#!/bin/bash
SCRIPTDIR=`dirname $(readlink -f $0)`
SCRIPTNAME=`basename $(readlink -f $0)`

function usage(){
printf "Utilisation du script avec des options :\n"
printf "\t-t THESAURUS_NAME\t : Le nom du thésaurus\n" 
printf "\t-s SERVICE_PRODUCTEUR\t : Le numéro du service producteur\n"
printf "\t-u WWWURL\t\t : L'adresse fqdn d'accès au site.\n"
printf "\t-c SSLDIR\t\t : Le répertoire des certificats SSL\n"
printf "\t-h\t\t\t : Affiche ce message.\n"
printf "Exemple : ./${SCRIPTNAME} -t GeoRégion -s 15 -u www.georegion.fr -c /home/ssl\n"
printf "./${SCRIPTNAME} doit être lancé avec aucune option OU avec les 4 options\n"
}
error_exit()
{
echo "Annulation du script : $1" && exit 1
}

case $# in
    0) MODE=interactive;;
    8) MODE=noninteractive;;
    *) usage && exit 1;;
esac

OPTS=$( getopt -q -o t:,s:,u:,c: -- "$@" )
[ $? != 0 ] && usage && exit 1
eval set -- "$OPTS"

while true ; do
	case "$1" in
		-t) THESAURUS_NAME="$2"
			shift 2;;
		-s) SERVICE_PRODUCTEUR="$2"
			shift 2;;
		-u) WWWURL="$2"
			shift 2;;
		-c) SSLDIR="$2"
			shift 2;;
		--) shift; break;;
	esac
done

check()
{
case "$2" in
    var) [ -z $1 ] && error_exit "$3";;
    file) [ -f $1 ] || error_exit "Le fichier $1 n'existe pas";;
    dir) [ -d $1 ] || error_exit "Le répertoire $1 n'existe pas";;
esac
}
if [ $MODE == "noninteractive" ]; then
    for param in THESAURUS_NAME SERVICE_PRODUCTEUR WWWURL SSLDIR; do
        check "${!param}" "var" "Variable $param vide"
    done
fi



check "${SCRIPTDIR}" "dir"
OLDIR="${SCRIPTDIR}/old"
check "$OLDDIR" "dir"
YEAR=`date '+%Y'`
DATE=`date '+%y%m%d'`
NUM="4.0.1"
NUM_old="3.4.17"
VERSIONTXT="/home/sites/prodigeadminsite/version.txt"
CONF1="/home/sites/prodigefrontcarto/app_conf.php"
CONF2="/home/sites/prodigecatalogue/PRRA/parametrage.php"
PATCH_NAME="update_prodige${NUM}_21092016.tar.gz"
PATCH_TAG="prodige_${NUM}"
PATCH_DESC="Patch correctif prodige_${NUM}
- migration vers Prodige 4.
"

for dir in \
${SCRIPTDIR}/prodigeadmincarto \
${SCRIPTDIR}/prodigeadminsite \
${SCRIPTDIR}/prodigecatalogue \
${SCRIPTDIR}/prodigefrontcarto \
${SCRIPTDIR}/prodigetelecarto \
${SCRIPTDIR}/prodigedatacarto \
${SCRIPTDIR}/cas \
${SCRIPTDIR}/vendor \
${SCRIPTDIR}/prodigegeosource \
${SCRIPTDIR}/tasks \
;do check "$dir" "dir";done
for file in \
${SCRIPTDIR}/editables_parameters.yml \
${SCRIPTDIR}/cas/etc/cas/cas.properties \
${SCRIPTDIR}/cas/WEB-INF/classes/services/prodige4-proxy.json \
${SCRIPTDIR}/cas/etc/cas/log4j2.xml \
${SCRIPTDIR}/prodigegeosource/WEB-INF/config-overrides-prod.xml \
${SCRIPTDIR}/prodigegeosource/WEB-INF/config-db/jdbc.properties \
${SCRIPTDIR}/prodigegeosource/WEB-INF/config-security/config-security.properties \
${SCRIPTDIR}/prodigeadmincarto/carmenwsback/app/config/parameters.yml \
${SCRIPTDIR}/prodigegeosource/WEB-INF/classes/log4j.xml \
${SCRIPTDIR}/conf/cas.xml \
${SCRIPTDIR}/conf/geonetwork.xml \
${SCRIPTDIR}/conf/geonetwork_log4j.xml \
${SCRIPTDIR}/conf/cas_log4j2.xml \
${SCRIPTDIR}/conf/server.xml \
${SCRIPTDIR}/conf/prodige4.0.conf \
${SCRIPTDIR}/conf/alkante.ldif \
${SCRIPTDIR}/conf/import_init.ldif \
${SCRIPTDIR}/conf/0.conf \
${SCRIPTDIR}/rights.sh \
${SCRIPTDIR}/uprod.sh \
;do check "$file" file ;done
which rsync > /dev/null 2>&1
[ $? -eq "0" ]  ||  error_exit "rsync n'est pas installé"
which pwgen > /dev/null 2>&1
[ $? -eq "0" ]  ||  error_exit "pwgen n'est pas installé"
LDAP_DOMAIN="alkante.domprodige"
LDAP_ORGA="Prodige4"
LDAP_PWD=`pwgen -s 10 1`
check "$LDAP_PWD" "var" "Le mot de passe LDAP n'a pu être généré"
SSL_PWD="$LDAP_PWD"
PHPCLI_DEFAULT_PWD=`pwgen -s 10 1`
check "$PHPCLI_DEFAULT_PWD" "var" "Le mot de passe PHPCLI_DEFAULT_PWD n'a pu être généré"


echo
echo "Mise à jour Prodige version NUM $NUM"
check "${VERSIONTXT}" "file"
[ "`egrep  "prodige_$NUM$" "${VERSIONTXT}"`" ] && error_exit "Version Prodige déjà égale à $NUM"
[ "`egrep  "prodige_$NUM_old$" "${VERSIONTXT}"`" ] || error_exit "Prodige est en version inférieure à $NUM_old"


get_dom()
{
# WWWURL= www-domain.tld ou www.domain.tld ou www-xyz.domain.tld ou www.xyz.domain.tld
ctl_saisie "$WWWURL" "^www[\.-].*\..*$"
# DNS_SUFFIX= -domain.tld ou .domain.tld ou -xyz.domain.tld ou .xyz.domain.tld
DNS_SUFFIX=`echo "$WWWURL" | sed 's/^www//'`
# DOMAIN= domain.tld ou xyz.domain.tld
DOMAIN=`echo $DNS_SUFFIX | sed 's/^-\?[a-z0-9]*\.//'`
# DNS_PREFIX_SEP = . ou -
DNS_PREFIX_SEP=`echo $WWWURL | sed 's/\(www\)\([\.-]\).*$/\2/'`
}

ctl_saisie()
{
echo $1 | grep -Eq "$2" ; [ $? -eq 0 ] || error_exit "Format saisi \"$1\" : incorrect"
}

if [ $MODE == "interactive" ]; then
    echo "Indiquer le nom du thésaurus ?"
    read THESAURUS_NAME
    echo
    echo "Indiquer le service producteur geosource (entier numérique) ?"
    read SERVICE_PRODUCTEUR
    ctl_saisie "$SERVICE_PRODUCTEUR" "^[0-9]*$"
    echo
    echo "Indiquer l'url qui sera utilisée pour accéder au site prodige ?
Elle peut être sous une et une seule des formes suivantes:
- www.domaine.fr      => vous devez fournir un certificat SSL wildcard pour      *.domaine.fr
- www-test.domaine.fr => vous devez fournir un certificat SSL wildcard pour      *.domaine.fr
- www.test.domaine.fr => vous devez fournir un certificat SSL wildcard pour *.test.domaine.fr
Merci de saisir l'url en commençant par www. ou par www-
"
    read WWWURL
    echo
    get_dom
    echo "L'accès au site sera https://$WWWURL
Pour cela vous devez fournir un certificat wildcard pour *.$DOMAIN"
    echo "Indiquer le répertoire où se trouvent les fichiers du certificat SSL (wildcard.prodige4.crt, wildcard.prodige4.key et CA_intermediate.pem) ?"
    read SSLDIR
    echo
fi

check "$SSLDIR" "dir"
for cert in wildcard.prodige4.crt wildcard.prodige4.key CA_intermediate.pem; do
   check "$SSLDIR/$cert" "file"
done
openssl verify -verbose -CAfile "$SSLDIR/CA_intermediate.pem"  "$SSLDIR/wildcard.prodige4.crt"
[ $? -eq 0 ] || error_exit "Erreur dans la vérification de la chaine des CA"
(openssl x509 -noout -modulus -in "$SSLDIR/wildcard.prodige4.crt"| openssl md5 ; openssl rsa -noout -modulus -in "$SSLDIR/wildcard.prodige4.key" | openssl md5) | uniq
[ $? -eq 0 ] || error_exit "Erreur $SSLDIR/wildcard.prodige4.crt ne correspond pas à $SSLDIR/wildcard.prodige4.key"
echo

ctl_saisie "$SERVICE_PRODUCTEUR" "^[0-9]*$"
get_dom
check "${CONF1}" "file"
url=`awk -F "\"" '/PRODIGE_VERIFY_RIGHTS_URL/ { print $4 }' "${CONF1}" |awk -F "/" '{ print $3 }'`
check "${CONF2}" "file"
user_bdd=`awk '/define\(\"PRO_CATALOGUE_CONNEXION/ { print $3 }' "${CONF2}"|sed "s/user=//"`
pass_bdd=`awk '/define\(\"PRO_CATALOGUE_CONNEXION/ { print $5 }' "${CONF2}"|sed "s/password=//"|sed 's/\".*//'`
DB=`awk '/define\(\"PRO_CATALOGUE_CONNEXION/ { print $4 }' "${CONF2}"|sed "s/dbname=//"`
user_internet=`awk -F "\"" '/define\(\"PRO_USER_INTERNET_USR_ID/ { print $4 }' "${CONF2}"`
for param in user_bdd pass_bdd DB user_internet url; do
     check "${!param}" "var" "Variable $param vide"
done

OLDDNS_SUFFIX=`echo $url | sed 's/^catalogue//
s/^www//'`
check "${OLDDNS_SUFFIX}" "var" "Variable OLDDNS_SUFFIX vide"
echo "OLDDNS_SUFFIX=$OLDDNS_SUFFIX"
echo "DNS_SUFFIX=$DNS_SUFFIX"
if [[ "$OLDDNS_SUFFIX" != "$DNS_SUFFIX" ]]
   then 
      echo "Vous souhaitez utiliser l'adresse $WWWURL, or jusqu'ici votre plate-forme a utilisé l'adresse $url.
Vous devez au préalable executer le script qui va transformer votre plate-forme de $url vers $WWWURL. (ETAPE5_optionnal_rename_url.sh)
Puis relancez à nouveau le script actuel"
      exit
fi

echo "
Le script est prêt pour lancer la migration avec les paramètres suivants:
- Le nom du thésaurus est : $THESAURUS_NAME
- Le service producteur geosource est : $SERVICE_PRODUCTEUR
- L'accès au site sera https://$WWWURL avec un certificat wildcard pour *.$DOMAIN présent dans $SSLDIR
- Paramètres postgres: bases PRODIGE et $DB  / user=$user_bdd pass=$pass_bdd
- Mot de passe admin LDAP : $LDAP_PWD
- Mot de passe java SSL : $SSL_PWD
- Mot de passe cli admin : $PHPCLI_DEFAULT_PWD"
read -p "Pour démarrer la migration, tapez sur la touche entrée!"
echo

chmod +r -R ${SCRIPTDIR}/
sed -i "s/^127.0.0.1.*$/127.0.0.1 localhost www$DNS_SUFFIX catalogue$DNS_SUFFIX adminsite$DNS_SUFFIX telecarto$DNS_SUFFIX datacarto$DNS_SUFFIX admincarto$DNS_SUFFIX mapserv$DNS_SUFFIX carto$DNS_SUFFIX/" /etc/hosts
echo "localhost:5432:PRODIGE:$user_bdd:$pass_bdd" > /root/.pgpass
echo "localhost:5432:$DB:$user_bdd:$pass_bdd" >> /root/.pgpass
chmod 600 /root/.pgpass

echo "max_locks_per_transaction = 128" >> /etc/postgresql/9.4/main/postgresql.conf
/etc/init.d/postgresql restart
/etc/init.d/tomcat7 stop
/etc/init.d/apache2 stop

echo
echo "Initialisation de l'annuaire LDAP"
echo "########################################################"
export DEBIAN_FRONTEND=noninteractive
sudo debconf-set-selections <<EOF
slapd slapd/internal/generated_adminpw password $LDAP_PWD
slapd slapd/password2 password $LDAP_PWD
slapd slapd/internal/adminpw password $LDAP_PWD
slapd slapd/password1 password $LDAP_PWD
slapd slapd/domain string $LDAP_DOMAIN
slapd shared/organization string $LDAP_ORGA
EOF
apt-get install -qqy slapd ldap-utils
echo
echo -e "\tImport du schéma alkante.ldif"
ldapadd -Q -Y EXTERNAL -H ldapi:/// -f ${SCRIPTDIR}/conf/alkante.ldif
sed -i 's|^SLAPD_SERVICES=.*$|SLAPD_SERVICES="ldap://127.0.0.1:389/ ldapi://127.0.0.1:389/"|' /etc/default/slapd
/etc/init.d/slapd restart
echo
echo -e "\tImport de import_init.ldif"
PHPCLI_DEFAULT_PWD_SSHA=`slappasswd -h {SSHA} -s "${PHPCLI_DEFAULT_PWD}" | sed 's|/|\\\/|g'`
sed -i "s/#PHPCLI_DEFAULT_PWD_SSHA#/${PHPCLI_DEFAULT_PWD_SSHA}/" ${SCRIPTDIR}/conf/import_init.ldif
ldapadd -x -H ldap://localhost/ -D "cn=admin,dc=alkante,dc=domprodige" -w "$LDAP_PWD" -f ${SCRIPTDIR}/conf/import_init.ldif 
echo


echo "Mise en place des sources"
echo "########################################################"
echo -e "\tArchivage de /home/sites/prodige* vers $OLDIR/ ..."
mv /home/sites/prodige* $OLDIR/
cd ${SCRIPTDIR}/
for dir in prodigeadmincarto prodigeadminsite prodigecatalogue prodigefrontcarto prodigetelecarto prodigedatacarto cas vendor prodigegeosource
  do
    echo -e "\tInstallation de /home/sites/$dir ..."
    rsync -a $dir /home/sites/
  done
chmod -R 770 /home/sites
chown -R www-data: /home/sites
echo

echo "Reprise d'anciennes données"
echo "########################################################"
echo -e "\tRécupération de data/config/codelist..."
rsync -a --ignore-existing $OLDIR/prodigegeosource/WEB-INF/data/config/codelist/* /home/sites/prodigegeosource/WEB-INF/data/config/codelist/
echo -e "\tRécupération de data/data/metadata_data..."
rsync -a $OLDIR/prodigegeosource/WEB-INF/data/data/metadata_data /home/sites/prodigegeosource/WEB-INF/data/data/
echo -e "\tRécupération de data/data/resources/images..."
cp -a $OLDIR/prodigegeosource/WEB-INF/data/data/resources/images /home/sites/prodigegeosource/WEB-INF/data/data/resources/
echo -e "\tRécupération de version.txt..."
cp $OLDIR/prodigegeosource/WEB-INF/version.txt /home/sites/prodigegeosource/WEB-INF/version.txt 
echo -e "\tRécupération de PRRA/ressources..."
rsync -a $OLDIR/prodigecatalogue/PRRA/ressources/ /home/sites/prodigecatalogue/web/upload/ressources/
echo -e "\tRécupération de PRRA/prodige.*.key..."
mkdir -p /home/prodige/automate
cp $OLDIR/prodigeadmincarto/PRRA/prodige.*.key /home/prodige/automate/
echo

echo "Edition des fichiers de configuration des sites"
echo "########################################################"
cp ${SCRIPTDIR}/favicon.ico /home/sites/
cp ${SCRIPTDIR}/logo-client.png /home/sites/
cp ${SCRIPTDIR}/editables_parameters.yml /home/sites/
cd /home/sites
for conffile in \
editables_parameters.yml \
cas/etc/cas/cas.properties \
cas/WEB-INF/classes/services/prodige4-proxy.json \
prodigeadmincarto/carmenwsback/app/config/parameters.yml \
prodigegeosource/WEB-INF/config-overrides-prod.xml \
prodigegeosource/WEB-INF/config-db/jdbc.properties \
prodigegeosource/WEB-INF/config-security/config-security.properties \
;do 
echo -e "\t$conffile"
sed -i "s/#DOMAIN#/$DNS_SUFFIX/g
s/#DB_PRODIGE_NAME#/PRODIGE/g
s/#DB_PRODIGE_USER#/$user_bdd/g
s/#DB_PRODIGE_PWD#/$pass_bdd/g
s/#DB_CATALOGUE_NAME#/$DB/g
s/#DB_CATALOGUE_USER#/$user_bdd/g
s/#DB_CATALOGUE_PWD#/$pass_bdd/g
s/#PHPCLI_DEFAULT_LOGIN#/admincli/g
s/#PHPCLI_DEFAULT_PWD#/${PHPCLI_DEFAULT_PWD}/g
s/#LDAP_PWD#/$LDAP_PWD/g
s/#PATH_CACERT_PEM#/\/etc\/apache2\/ssl-cert\/CA_intermediate.pem/g" $conffile
done

echo -e "\tprodigeadmincarto/carmenwsback/app/config/parameters.yml"
sed -i "s/dns_url_prefix_sep.*$/dns_url_prefix_sep: \"$DNS_PREFIX_SEP\"/" /home/sites/prodigeadmincarto/carmenwsback/app/config/parameters.yml 
echo

echo "Création des lien symboliques"
echo "########################################################"
for dir in prodigeadminsite prodigecatalogue prodigefrontcarto prodigetelecarto prodigedatacarto
  do
    echo -e "\t$dir"
    cd /home/sites
    cd $dir/vendor
    [ -d alkante ] && rm -rf alkante
    [ -d prodige ] && rm -rf prodige
    ln -s ../../vendor/alkante
    ln -s ../../vendor/prodige
  done
for dir in prodigeadmincarto/carmenwsback prodigeadmincarto/carmenwsmapserv
  do
    echo -e "\t$dir"
    cd /home/sites
    cd $dir/vendor
    [ -d alkante ] && rm -rf alkante
    [ -d prodige ] && rm -rf prodige
    ln -s ../../../vendor/alkante
    ln -s ../../../vendor/prodige
  done

echo "Installation des favicon.ico"
echo "########################################################"
for ico in \
/home/sites/cas/favicon.ico \
/home/sites/prodigegeosource/images/logos/favicon.ico \
/home/sites/prodigeadmincarto/carmenwsback/web/favicon.ico \
/home/sites/prodigeadminsite/web/favicon.ico \
/home/sites/prodigecatalogue/web/favicon.ico \
/home/sites/prodigedatacarto/web/favicon.ico \
/home/sites/prodigefrontcarto/web/favicon.ico \
/home/sites/prodigetelecarto/web/favicon.ico \
;do
ln -sf /home/sites/favicon.ico "${ico}"
done
echo
echo "Installation des logos"
echo "########################################################"
ln -s /home/sites/logo-client.png /home/sites/cas/images/logo-client.png
ln -s /home/sites/logo-client.png /home/sites/vendor/prodige/prodige/Prodige/ProdigeBundle/Resources/public/images/logo-client.png
echo

echo "Suppression de fichiers geonetwork posant problème"
echo "########################################################"
rm -f /home/sites/prodigegeosource/WEB-INF/data/config/schema_plugins/iso19139/schematron/schematron-rules-inspire-strict.sch
rm -f /home/sites/prodigegeosource/WEB-INF/data/config/schema_plugins/iso19139/schematron/schematron-rules-inspire-strict.xsl
rm -f /home/sites/prodigegeosource/WEB-INF/data/config/schema_plugins/iso19139/schematron/schematron-rules-inspire-sds.sch
rm -f /home/sites/prodigegeosource/WEB-INF/data/config/schema_plugins/iso19139/schematron/schematron-rules-inspire-sds.xsl
rm -f /home/sites/prodigegeosource/WEB-INF/data/config/schema_plugins/iso19139/schematron/schematron-rules-inspire-strict.disabled.sch
rm -f /home/sites/prodigegeosource/WEB-INF/data/config/schema_plugins/iso19139/schematron/schematron-rules-inspire-strict.disabled.xsl
rm -f /home/sites/prodigegeosource/WEB-INF/data/config/schema_plugins/iso19139/schematron/schematron-rules-inspire-sds.disabled.sch
rm -f /home/sites/prodigegeosource/WEB-INF/data/config/schema_plugins/iso19139/schematron/schematron-rules-inspire-sds.disabled.xsl
rm -f /home/sites/prodigegeosource/WEB-INF/data/config/schema_plugins/iso19139/schematron/schematron-rules-inspire.disabled.sch
rm -f /home/sites/prodigegeosource/WEB-INF/data/config/schema_plugins/iso19139/schematron/schematron-rules-inspire.disabled.xsl
echo


echo "Edition des fichiers de configuration tomcat"
echo "########################################################"
cp ${SCRIPTDIR}/conf/geonetwork_log4j.xml /home/sites/prodigegeosource/WEB-INF/classes/log4j.xml
cp ${SCRIPTDIR}/conf/geonetwork.xml /etc/tomcat7/Catalina/localhost/
chmod +r /etc/tomcat7/Catalina/localhost/*.xml
cp ${SCRIPTDIR}/conf/server.xml /etc/tomcat7/
sed -i "s/#SSL_PWD#/$SSL_PWD/g" /etc/tomcat7/server.xml
cp ${SCRIPTDIR}/conf/jetty-ssl.xml /etc/jetty8/
sed -i "s/#SSL_PWD#/$SSL_PWD/g" /etc/jetty8/jetty-ssl.xml
cp ${SCRIPTDIR}/conf/cas_log4j2.xml /home/sites/cas/WEB-INF/classes/log4j2.xml
chmod 750 /var/log/jetty8
chown www-data.adm /var/log/jetty8
echo



echo "Configuration des virtualhosts Apache"
echo "########################################################"
cp $OLDIR/etc/apache2/sites-available/*.conf /etc/apache2/sites-available/
[ -f /etc/apache2/sites-available/prodige4.0.conf ] && rm /etc/apache2/sites-available/prodige4.0.conf
cp  ${SCRIPTDIR}/conf/prodige4.0.conf /etc/apache2/sites-available/
sed -i "s/#DOMAIN#/$DNS_SUFFIX/g" /etc/apache2/sites-available/prodige4.0.conf
a2ensite prodige4.0.conf
[ -f /etc/apache2/sites-available/0.conf ] && rm /etc/apache2/sites-available/0.conf
cp  ${SCRIPTDIR}/conf/0.conf /etc/apache2/sites-available/
sed -i "s/#DOMAIN#/$DNS_SUFFIX/g" /etc/apache2/sites-available/0.conf
[ -d /home/sites/nothing ] || mkdir -p /home/sites/nothing
a2ensite 0.conf
echo


echo "Installation des certificats SSL"
echo "########################################################"
cd /etc/apache2
[ -d ssl-cert ] || mkdir ssl-cert
cd ssl-cert
[ -d ssl.crt ] || mkdir ssl.crt
[ -d ssl.key ] || mkdir ssl.key
cp "$SSLDIR/wildcard.prodige4.crt" ssl.crt/wildcard.prodige4.crt.$YEAR
cd ssl.crt && ln -s wildcard.prodige4.crt.$YEAR wildcard.prodige4.crt
cd ..
cp "$SSLDIR/wildcard.prodige4.key" ssl.key/wildcard.prodige4.key.$YEAR
cd ssl.key && ln -s wildcard.prodige4.key.$YEAR wildcard.prodige4.key
cd ..
cp "$SSLDIR/CA_intermediate.pem" CA_intermediate.pem.$YEAR
ln -s CA_intermediate.pem.$YEAR CA_intermediate.pem
chmod 644 CA_intermediate.pem.$YEAR

#conversion ssl pour keystore tomcat
mkdir /etc/tomcat7/keystore
openssl pkcs12 -export -name wildcard.prodige4 -in "$SSLDIR/wildcard.prodige4.crt" -inkey "$SSLDIR/wildcard.prodige4.key" -out /etc/tomcat7/keystore/wildcard.prodige4.p12.$YEAR -password pass:$SSL_PWD
keytool -importkeystore -destkeystore /etc/tomcat7/keystore/prodige.keystore -srckeystore /etc/tomcat7/keystore/wildcard.prodige4.p12.$YEAR -srcstoretype pkcs12 -srcalias wildcard.prodige4 -srcstorepass $SSL_PWD -deststorepass $SSL_PWD -noprompt
keytool -import -trustcacerts  -keystore /etc/ssl/certs/java/cacerts -storepass changeit -noprompt -file "$SSLDIR/CA_intermediate.pem" -alias wildcard.prodige4CA
echo

echo "Mise à jour de la base PRODIGE, consulter ${SCRIPTDIR}/update_${NUM}_PRODIGE.log"
echo "########################################################"
echo -e "\t\tinit_structure" | tee -a "${SCRIPTDIR}/update_${NUM}_PRODIGE.log"
psql -U $user_bdd -d PRODIGE -h localhost < /home/sites/prodigeadmincarto/carmenwsback/src/Carmen/ApiBundle/Resources/sql/init_structure.sql >> "${SCRIPTDIR}/update_${NUM}_PRODIGE.log" 2>&1
echo -e "\t\tinit_data" | tee -a "${SCRIPTDIR}/update_${NUM}_PRODIGE.log"
psql -U $user_bdd -d PRODIGE -h localhost < /home/sites/prodigeadmincarto/carmenwsback/src/Carmen/ApiBundle/Resources/sql/init_data.sql >> "${SCRIPTDIR}/update_${NUM}_PRODIGE.log" 2>&1
echo -e "\t\tmigrate_prodige" | tee -a "${SCRIPTDIR}/update_${NUM}_PRODIGE.log"
psql -U $user_bdd -d PRODIGE -h localhost < /home/sites/prodigeadmincarto/carmenwsback/src/Carmen/ApiBundle/Resources/sql/migrate.sql >> "${SCRIPTDIR}/update_${NUM}_PRODIGE.log" 2>&1

echo -e "\t\tlex... patientez... cette opération peut être longue" | tee -a "${SCRIPTDIR}/update_${NUM}_PRODIGE.log"
dns_tmp=`echo $DNS_SUFFIX | sed 's/^[\.-]//'`
sed -i "s/prodige4.alkante.com/$dns_tmp/" ${SCRIPTDIR}/files/lex.sql
psql -U $user_bdd -d PRODIGE -h localhost < ${SCRIPTDIR}/files/lex.sql >> "${SCRIPTDIR}/update_${NUM}_PRODIGE.log" 2>&1
echo


/etc/init.d/apache2 restart
#clean geosource index
/etc/init.d/tomcat7 stop
rm -rf /home/sites/prodigegeosource/WEB-INF/data/index/*
/etc/init.d/tomcat7 start
/etc/init.d/jetty8 restart
echo "Attente 60 secondes du redemarrage complet de geosource" ; sleep 60
echo

echo "Changement de protocole http -> https"
echo "########################################################"
sudo -u postgres psql -d $DB -c "update public.settings set value='https' where name='system/server/protocol';"
for i in www catalogue adminsite telecarto datacarto admincarto carto; do
   echo "Updating $i"
   sudo -u postgres psql -d $DB -c "update public.metadata set data = replace(data, 'http://${i}${DNS_SUFFIX}', 'https://${i}${DNS_SUFFIX}');"
done
sudo -u postgres psql -d $DB -c "update public.settings set value='https://www$DNS_SUFFIX/geonetwork/srv/' where name='metadata/resourceIdentifierPrefix';" 
echo "Updating mapfiles"
find /home/prodige/cartes/ \( -name "*.map" -o -name "*.xml" \) -print0 |xargs -0 sed -i "s/http:\/\/\(\(www\|catalogue\|adminsite\|telecarto\|datacarto\|admincarto\|carto\)$DNS_SUFFIX\)/https:\/\/\1/g"
find /home/prodige/cartes -type f -name "*.map" -exec sed -i '/LOG "log_itx.txt"/d' {} \;
[ -f /home/prodige/cartes/Publication/ms_error.txt ] && rm /home/prodige/cartes/Publication/ms_error.txt
echo

echo "Migration carmen:migration, consulter ${SCRIPTDIR}/migration_carmen.log"
echo "########################################################"
#mv /home/prodige/cartes /home/
mkdir /home/prodige/owscontext /home/prodige/QUEUE
chown www-data: /home/prodige/owscontext /home/prodige/QUEUE
chmod 770 /home/prodige/owscontext /home/prodige/QUEUE
cd /home/sites/prodigeadmincarto/carmenwsback/
php app/console carmen:migration $user_bdd $pass_bdd localhost 5432 PRODIGE > ${SCRIPTDIR}/migration_carmen.log 2>&1
echo


echo "Migration adminsite:migration, consulter ${SCRIPTDIR}/migration_adminsite.log"
echo "########################################################"

cd /home/sites/prodigeadminsite/
php app/console adminsite:migrate_mapcache > ${SCRIPTDIR}/migration_adminsite.log 2>&1
echo

echo "Initialisation de la base $DB, consulter ${SCRIPTDIR}/update_${NUM}_${DB}.log"
echo "########################################################"
psql -U $user_bdd -d $DB -h localhost < /home/sites/prodigecatalogue/src/ProdigeCatalogue/AdminBundle/Resources/sql/init_structure.sql > "${SCRIPTDIR}/update_${NUM}_${DB}_init.log" 2>&1
/etc/init.d/tomcat7 restart
echo "Attente 60 secondes du redemarrage complet de geosource" ; sleep 60
echo


#modif user si email/nom/prenom vide
psql -tA -P pager=off -U $user_bdd -d $DB -h localhost -c "set search_path to catalogue;update utilisateur set usr_email = 'NEANT' where usr_email = '' OR usr_email IS NULL;"
psql -tA -P pager=off -U $user_bdd -d $DB -h localhost -c "set search_path to catalogue;update utilisateur set usr_prenom = 'NEANT' where usr_prenom = '' OR usr_prenom IS NULL;"
psql -tA -P pager=off -U $user_bdd -d $DB -h localhost -c "set search_path to catalogue;update utilisateur set usr_nom = 'NEANT' where usr_nom = '' OR usr_nom IS NULL;"
echo


echo "Migration prodige:migration"
echo "########################################################"
cd /home/sites/prodigecatalogue/
app/console prodige:migration $SERVICE_PRODUCTEUR > ${SCRIPTDIR}/migration_catalogue.log 2>&1
echo


echo "Mise à jour de la base $DB, consulter ${SCRIPTDIR}/update_${NUM}_${DB}.log"
echo "########################################################"
psql -U $user_bdd -d $DB -h localhost < /home/sites/prodigecatalogue/src/ProdigeCatalogue/AdminBundle/Resources/sql/migrate_structure.sql >> "${SCRIPTDIR}/update_${NUM}_${DB}.log" 2>&1



cd /home/sites/prodigefrontcarto/web/IHM
[ -d IHM ] || mkdir IHM
cd IHM
ln -s /home/prodige/cartes/IHM/IHM cartes
#chown ftpuser.www-data -R cartes
#cd /home/sites/prodigefrontcarto/web
#ln -s /home/prodige/mapimage mapimage
#chown ftpuser.www-data -R mapimage
echo "Inscription des tâches planifiées"
echo "########################################################"
mkdir -p /home/tasks/log
mv ${SCRIPTDIR}/tasks/* /home/tasks/


echo "
#tasks prodige4.0
00 03           * * *   www-data    cd /home/sites/prodigeadminsite/ && /usr/bin/php app/console adminsite:generate_mapcache > /dev/null 2>&1
00 23           * * *   www-data    cd /home/sites/prodigeadmincarto/carmenwsback && /usr/bin/php app/console carmen:importdata > /dev/null 2>&1
00 00           * * *   www-data    cd /home/sites/prodigecatalogue/ && /usr/bin/php app/console prodige:password:lockexpired > /dev/null 2>&1
00,30 *         * * *   www-data    cd /home/sites/prodigeadminsite/ && /usr/bin/php app/console adminsite:populate_session_logs > /dev/null 2>&1
00 01         * * 6   www-data    cd /home/sites/prodigefrontcarto &&  /usr/bin/php app/console prodigefrontcarto:alertedition > /dev/null 2>&1
*/5 *           * * *   root        /home/tasks/queue_download.sh
30 00           * * *   root        /home/tasks/clean.sh
#end tasks prodige4.0
" >> /etc/crontab
echo


echo "Génération du fichiers version.txt"
echo "########################################################"
echo "Génération du fichier version.txt"
VERSIONTXT="/home/sites/prodigecatalogue/web/version.txt"
echo -e "Patch ${PATCH_NAME} (${PATCH_TAG}) applied `date`\n${PATCH_DESC}\n-----------------------------------------------" > ./current_version.txt
[ -f "${VERSIONTXT}" ] || touch "${VERSIONTXT}"
ed -s  "${VERSIONTXT}" <<< $'0 r ./current_version.txt\n,w'
rm ./current_version.txt


sed -i 's/checkpoint_segments = 100/#checkpoint_segments = 3/' /etc/postgresql/9.4/main/postgresql.conf
sed -i '/^max_locks_per_transaction = 128/d' /etc/postgresql/9.4/main/postgresql.conf
/etc/init.d/postgresql restart
/etc/init.d/tomcat7 restart
rm /root/.pgpass
rm /tmp/layerfieldvalues*

#rights.sh
cp ${SCRIPTDIR}/rights.sh /home/
cp ${SCRIPTDIR}/uprod.sh /home/sites/
cd /home/sites
bash ./uprod.sh
bash /home/rights.sh &

echo
echo "Fin de la mise à jour"
echo

exit 0
