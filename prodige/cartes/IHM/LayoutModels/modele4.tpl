{strip}
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html >
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="ModelDescription" content="Légende à gauche, carte de siuation au dessus"/>
    <title>{$owsTitle}</title>
    <link rel="stylesheet" type="text/css" href="{$path_to_IHM}JavaScript/ext-5.0.1/build/packages/ext-theme-crisp/build/resources/ext-theme-crisp-all-debug.css" />
    <link rel="stylesheet" type="text/css" href="{$path_to_IHM}css/print.css" />
    <link rel="stylesheet" type="text/css" href="{$path_to_IHM}css/consultation.css" />
</head>
<body>
{$print}
<table border="0"  cellpadding="0" cellspacing="0">
<tr>
  <td colspan="3" class="title" align="center">
  {if $title_alias != '' && $title==''}
     <b>{$title_alias}</b>
   {else}
     {$title}
   {/if}
   </td>
</tr>  
<tr>
  <td colspan="3" class="comment" align="left">
  {if $comment_alias != '' && $comment==''}
      <b>{$comment_alias}</b>
    {else}
      {$comment}
    {/if}
  </td>
</tr>
<tr>
  <td valign="top">
  {if $keymap_alias != '' && $keymap==''}
       <b>{$keymap_alias}</b>
   {else}
     {$keymap}
   {/if}
  </td>
  <td rowspan="2"></td>
  <td rowspan="2" align="left" valign="top">
   {if $map_alias != '' && $map==''}
     <b>{$map_alias}</b>
    {else}
      {$map}
    {/if}
  </td>
</tr>  
<tr>
  <td class="x-tree-root-ct x-tree-lines" valign="top">
  {if $legend_alias != '' && $legend==''}
    <b>{$legend_alias}</b>
    {else}
      {$legend}
    {/if}
  </td>
</tr>
<tr>
  <td></td>
  <td class="copyright" colspan="2">
  {if $copyright_alias != '' && $copyright==''}
      <b>{$copyright_alias}</b>
    {else}
      {$copyright}
    {/if}
  </td>
</tr>
</table>
</body>
</html>
{/strip}