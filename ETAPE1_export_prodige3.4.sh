#!/bin/bash
# repertoire d'execution du script actuel :
SCRIPTDIR=`dirname "$(readlink -f "$0")"`
DATE=`date '+%y%m%d-%H%M%S'`
LOG="$SCRIPTDIR/ETAPE1_export_${DATE}.log"
OLDIR="$SCRIPTDIR/old"

echo "Vérification du numéro de version de prodige3.4"
if [ "`grep 3.4.17 /home/sites/prodigeadminsite/version.txt`" ] 
  then echo " 3.4.17 OK"
  else echo " ERREUR : Prodige n'est pas en version 3.4.17" ; exit
fi


echo "
#################################"
echo -e "Démarrage de l'export des données de Prodige 3.4 localement.
Le script va :
* Effectuer une copie de /etc pour backup.
* Effectuer une copie des cartes
* Effectuer le dump des bases de l'application Prodige UNIQUEMENT (effectuez vous même la sauvegarde des éventuelles bases tierces)
Attention, cela peut nécessiter plusieurs Go d'espace disque dans le répertoire $OLDIR/"

read -p "Appuyez sur Entrée pour continuer
#################################"
echo

mkdir -p "${OLDIR}/postgres" 2> /dev/null
[ ! -d "${OLDIR}" ] && echo "Erreur, ${OLDIR} n'existe pas" && exit

DB=`awk '/define\(\"PRO_CATALOGUE_CONNEXION/ { print $4 }' /home/sites/prodigecatalogue/PRRA/parametrage.php|sed "s/dbname=//"`
[ -z "${DB}" ] && echo "Impossible de déterminer le nom de la base CATALOGUE" && exit
nDB=`sudo -i -u postgres psql -lt | awk '/'${DB}'/ { print $1}' | egrep -c "^${DB}$"`
[ $nDB -ne 1 ] && echo "La base postgres ${DB} n'existe pas" && exit
nPRODIGE=`sudo -i -u postgres psql -lt | awk '/PRODIGE/ { print $1}' | egrep -c "^PRODIGE$"`
[ $nPRODIGE -ne 1 ] && echo "La base postgres PRODIGE n'existe pas" && exit

read -p "

Si tout est OK, démarrage de l'export.
Appuyer sur Entrée pour continuer ou contrôle-C pour arrêter"

echo
/etc/init.d/apache2 stop
/etc/init.d/tomcat7 stop
/etc/init.d/postgresql restart
/etc/init.d/tomcat7 start
/etc/init.d/apache2 start
echo

echo "Backup config /etc vers le répertoire ${OLDIR}/etc"
echo "************************************************"
cp -a /etc "${OLDIR}/"
#desactivation taches planifiees
sed -i '/\/home\/tasks/d' /etc/crontab
sed -i '/\/home\/sites/d' /etc/crontab
echo

echo "Backup *.map *.xml ... vers le répertoire ${OLDIR}/cartes"
echo "************************************************"
mkdir "${OLDIR}/cartes"
rsync -a -m --include="+ */" --include="+ *.map" --include="+ *.xml" --include="+ *.sym" --include="+ Publication/etc/*" --include="- *" /home/prodige/cartes/ ${OLDIR}/cartes/
echo

echo "Dump local de la base ${DB} vers ${OLDIR}/postgres/"
echo "************************************************"
su - postgres -c "pg_dump -C -E UTF8 -Fc ${DB}" > ${OLDIR}/postgres/${DB}.dmp
echo "Dump local de la base PRODIGE vers ${OLDIR}/postgres/"
echo "************************************************"
su - postgres -c "pg_dump -C -E UTF8 -Fc PRODIGE" > ${OLDIR}/postgres/PRODIGE.dmp
echo "Dump local des privilèges Postgres vers ${OLDIR}/postgres/"
su - postgres -c "pg_dumpall --globals-only" > ${OLDIR}/postgres/privileges.dmp
echo

echo "Fin de l'export `date`"

