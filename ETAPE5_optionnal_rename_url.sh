#!/bin/bash
#Changement de nom de domaine pour une migration prodige 3.4 (avec sous sans alkanet) vers prodige 4.0
#Le script demande le nouveau domaine et doit être jour si ETAPE5 le demande

DATE=`date '+%y%m%d'`
DB=`awk '/define\(\"PRO_CATALOGUE_CONNEXION/ { print $4 }' /home/sites/prodigecatalogue/PRRA/parametrage.php|sed "s/dbname=//"`

CONF1="/home/sites/prodigefrontcarto/app_conf.php"
url=`awk -F "\"" '/PRODIGE_VERIFY_RIGHTS_URL/ { print $4 }' "${CONF1}" |awk -F "/" '{ print $3 }'`
OLDDNS_SUFFIX=`echo $url | sed 's/^catalogue//
s/^www//'`


get_dom()
{
ctl_saisie "$WWWURL" "^www[\.-].*\..*$"
DNS_SUFFIX=`echo "$WWWURL" | sed 's/^www//'`
DOMAIN=`echo $DNS_SUFFIX | sed 's/^-\?[a-z0-9]*\.//'`
DNS_PREFIX_SEP=`echo $WWWURL | sed 's/\(www\)\([\.-]\).*$/\2/'`
}

ctl_saisie()
{
echo $1 | grep -Eq "$2" ; [ $? -eq 0 ] || { echo "Format saisi \"$1\" : incorrect"; exit; }
}

echo "Indiquer la nouvelle url qui sera utilisée pour accéder au site prodige ?
Elle peut être sous une et une seule des formes suivantes:
- www.aaaa.fr        => vous devez fournir un certificat SSL wildcard pour        *.aaaa.fr
- www-bbb.aaaa.fr => vous devez fournir un certificat SSL wildcard pour        *.aaaa.fr
- www.bbb.aaaa.fr => vous devez fournir un certificat SSL wildcard pour *.bbb.aaaa.fr
Merci de saisir l'url en commen ant par www. ou par www-"
read WWWURL
get_dom
read -p "Vous avez choisi de remplacer l'adresse d'accès au site www$OLDDNS_SUFFIX par www$DNS_SUFFIX.
Appuyez sur la touche entrée pour procéder au changement: "
echo
/etc/init.d/postgresql restart

prodige_data()
{
echo "Mise a jour configuration des sites"
find /home/sites/ -maxdepth 3 -type f -name "*.php" |xargs sed -i "s|$OLDDNS_SUFFIX|$DNS_SUFFIX|g"
find /home/sites/ -type f -name "*.ows" -print0 |xargs -0 -r sed -i "s|$OLDDNS_SUFFIX|$DNS_SUFFIX|g"
find /home/prodige/ -type f -name "*.ows" -print0 |xargs -0 -r sed -i "s|$OLDDNS_SUFFIX|$DNS_SUFFIX|g"
[ -f /home/sites/prodigefrontcarto/IHM/metadata/cartes/index.html ] && sed -i "s|$OLDDNS_SUFFIX|$DNS_SUFFIX|g" /home/sites/prodigefrontcarto/IHM/metadata/cartes/index.html
[ -f /home/sites/prodigefrontcarto/templates_c/current_context.xml ] && sed -i "s|$OLDDNS_SUFFIX|$DNS_SUFFIX|g" /home/sites/prodigefrontcarto/templates_c/current_context.xml
echo "Mise a jour configuration des cartes"
#find /home/prodige/cartes/ -name "*.map" |xargs sed -i "s|$OLDDNS_SUFFIX|$DNS_SUFFIX|"
#find /home/prodige/cartes/ -name "*.xml" |xargs sed -i "s|$OLDDNS_SUFFIX|$DNS_SUFFIX|"
#find /home/prodige/cartes/ \( -name "*.map" -o -name "*.xml" \) -print0 |xargs -0 sed -i "s/\.${OLDDNS_SUFFIX}/${DNS_SUFFIX}/g"
find /home/prodige/cartes/ \( -name "*.map" -o -name "*.xml" \) -print0 |xargs -0 sed -i "s/${OLDDNS_SUFFIX}/${DNS_SUFFIX}/g"
echo
}
prodige_base()
{
echo "Mise à jour bases postgres"
sudo -u postgres psql -d $DB -c "update catalogue.acces_serveur set accs_adresse = 'carto$DNS_SUFFIX', accs_adresse_admin='admincarto$DNS_SUFFIX', accs_adresse_tele='telecarto$DNS_SUFFIX', accs_adresse_data='datacarto$DNS_SUFFIX';"
#sudo -u postgres psql -d $DB -c "update public.metadata set data = replace(data, '$OLDDNS_SUFFIX', '$DNS_SUFFIX');"
for i in www catalogue adminsite telecarto datacarto admincarto carto; do
   sudo -u postgres psql -d $DB -c "update public.metadata set data = replace(data, '${i}${OLDDNS_SUFFIX}', '${i}${DNS_SUFFIX}');"
done
sudo -u postgres psql -d $DB -c "update public.settings set value='www$DNS_SUFFIX' where name='system/server/host';"
sudo -u postgres psql -d $DB -c "update public.settings set value='https://www$DNS_SUFFIX/geonetwork/srv/' where name='metadata/resourceIdentifierPrefix';"            |  
sudo -u postgres psql -d PRODIGE -c "update parametrage.prodige_settings set prodige_settings_value = 'www$DNS_SUFFIX' where prodige_settings_constant = 'PRO_SITE_URL';"
echo
}

alkanet_base()                                                                                                                                                                                                                                    
{                                                                                                                                                                                                                                            
echo "Mise à jour base alkanet"
sudo -u postgres psql -d $DB -c "update alkanet.GEDIT_01_CONTENU set CONTENU_FR=REPLACE(CONTENU_FR, '$OLDDNS_SUFFIX', '$DNS_SUFFIX'), CONTENU_EN=REPLACE(CONTENU_EN, '$OLDDNS_SUFFIX', '$DNS_SUFFIX');"
sudo -u postgres psql -d $DB -c "update alkanet.GEDIT_01_PAGE set PAGE_URL_REDIRECTION=REPLACE(PAGE_URL_REDIRECTION, '$OLDDNS_SUFFIX', '$DNS_SUFFIX');"
sudo -u postgres psql -d $DB -c "update alkanet.IEDIT_DATA set DATA_DESC_LONG_FR=REPLACE(DATA_DESC_LONG_FR, '$OLDDNS_SUFFIX', '$DNS_SUFFIX'), DATA_DESC_LONG_EN=REPLACE(DATA_DESC_LONG_EN, '$OLDDNS_SUFFIX', '$DNS_SUFFIX'), DATA_URL_FR=REPLACE(DATA_URL_FR, '$OLDDNS_SUFFIX', '$DNS_SUFFIX'), DATA_URL_EN=REPLACE(DATA_URL_EN, '$OLDDNS_SUFFIX', '$DNS_SUFFIX');"
echo
}
alkanet_data()                                                                                                                                                                                                                                    
{
echo "Mise à jour MnoGoSearch"
sed -i "s|^Server http://www$OLDDNS_SUFFIX/|Server http://www$DNS_SUFFIX/|" /home/sites/alkanet/search/indexer.conf
echo "Vous pouvez reindexer mnogosearch tout de suite avec la commande suivante"
echo "/home/sites/alkanet/search/mnogosearch_index.sh"
echo "Ou attendre la tache planifiee cette nuit"
echo
}

alkanet()                                                                                                                                                                                                                                    
{
#[ -f /etc/apache2/sites-enabled/alkanet.conf ] && { alkanet_base; alkanet_data; }
[ -d  /home/sites/alkanet ] && { alkanet_base; alkanet_data; }
}

prodige()                                                                                                                                                                                                                                    
{
    prodige_base
    prodige_data
}

prodige
alkanet
sed -i "s/^127.0.0.1.*$/127.0.0.1 localhost www$DNS_SUFFIX catalogue$DNS_SUFFIX adminsite$DNS_SUFFIX telecarto$DNS_SUFFIX datacarto$DNS_SUFFIX admincarto$DNS_SUFFIX mapserv$DNS_SUFFIX carto$DNS_SUFFIX/" /etc/hosts
echo "Fin du script"
exit

