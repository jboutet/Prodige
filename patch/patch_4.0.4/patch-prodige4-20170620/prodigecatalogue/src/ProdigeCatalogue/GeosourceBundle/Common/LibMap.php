<?php
namespace ProdigeCatalogue\GeosourceBundle\Common;

use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * librairie permettant d'effectuer des manipulations sur des objets Map
 * @author Alkante
 */
class LibMap {

    /**
     *
     * @var \Symfony\Component\DependencyInjection\Container
     */
    static private $container;
    
    /**
     * @var \Doctrine\DBAL\Connection
     */
    private static $conn;
    

    public static function initContainer($container) {
        self::$container = $container;
    }
    /**
     * @param \Doctrine\DBAL\Connection $conn
     */
    public static function setConnection(\Doctrine\DBAL\Connection $conn) {
        self::$conn = $conn;
    }

    /**
    * brief ajoute une couche dans un mapfile
    * @param oMap objet Mapfile
    * @param MapPath chemin du mapfile
    * @param paramètres de la couche ajoutée
    * @param tabMetadata tableau des métadonnées du service
    * @param action wfs ou wms
    */
    public static function AddLayerToMap($params, $catalogueURL) {
        if( !self::$container ) throw new \Exception('container is not initialized, call LibMap:initContainer');
        //global PRO_SITE_URL;
        //global $PRO_GEONETWORK_DIRECTORY;
        //global $PRO_MAPFILE_PATH;
        $oLayer = array();
        
        $bResult = null;
            if(!is_null($params)) {

                   
                    if(count($params) > 0) {
                        switch($params["type"]) {
                            case "RASTER_PRODIGE":

                                $layer_name = self::neutralisationChaine($params["layerTitle"]);
                                
                                $oLayer["layerType"] = "RASTER";
                                
                                $oLayer["msLayerType"]="RASTER";
                                $oLayer["msGeometryType"]="RASTER";
                                
                                $rasterfile = PRO_MAPFILE_PATH.$params["layerTable"];
                                if ( strtolower(pathinfo($rasterfile, PATHINFO_EXTENSION))=="shp" ){
                                    $oLayer["msGeometryType"]="TILEINDEX";
                                }
                                $oLayer["msLayerData"] = $rasterfile;
                                $oLayer["msLayerGrayLevel"]=false;
                                
                                $oLayer["layerTitle"] = $params["layerTitle"];
                                $oLayer["layerName"] = $layer_name;
                                $oLayer["msLayerName"] =  "";
                                $oLayer["layerIdentifier"] =  $layer_name;
                                

                                $oLayer["layerProjectionEpsg"] = PRO_IMPORT_EPSG;
                                //default values
                                
                                
                                $oLayer["msClasses"] = array();
                                $oLayer["msClasses"][0]= array();
                                $oLayer["msClasses"][0]["name"] = $oLayer["layerTitle"];
                                $oLayer["msClasses"][0]["title"] = $oLayer["layerTitle"];
                                $oLayer["msClasses"][0]["styles"] = array();
                                $oLayer["msClasses"][0]["styles"]["color"] = "#FFFFFF";
                                $oLayer["msClasses"][0]["styles"]["symbol"] = "Carre";
                                $oLayer["msClasses"][0]["styles"]["size"] = "1";
                            break;

                            case "VECTOR_PRODIGE":
                                    $layer_name = self::neutralisationChaine($params["layerTitle"]);
                                    
                                    $oLayer["layerTitle"] = $params["layerTitle"];
                                    $oLayer["layerName"] = $layer_name;
                                    $oLayer["msLayerName"] =  "";
                                    $oLayer["layerIdentifier"] =  $layer_name;
                                    $conn_prodige = self::$conn;
                                    $tabInfoLayer = array();
                                    $desc = $conn_prodige->fetchAll("SELECT f_table_schema, f_geometry_column, srid, type from geometry_columns where f_table_name = :table",
                                    array("table" => strtolower($params["layerTable"])));
                                    foreach($desc as $table){
                                        //try {
                                            $geomInfo = $conn_prodige->fetchAll(
                                               "select public.st_srid(".$table["f_geometry_column"].") as srid, public.geometrytype(".$table["f_geometry_column"].") as type
                                                from ".$table["f_table_schema"].".".$params["layerTable"]." limit 1"
                                            );
                                            if ( !empty($geomInfo) ){
                                                $tabInfoLayer[] =  array_merge($table, $geomInfo[0]);
                                            }else{
                                                $tabInfoLayer[] = $table;
                                            }
                                       //} catch(\Exception $exception){};
                                    }
                                    if(empty($tabInfoLayer)){
                                        throw new \Exception('la ressource n\'existe pas ');
                                        return false;
                                    }
                                    $oLayer["msClasses"] = array();
                                    $oLayer["msClasses"][0]= array();
                                    $oLayer["msClasses"][0]["name"] = $oLayer["layerTitle"];
                                    $oLayer["msClasses"][0]["title"] = $oLayer["layerTitle"];
                                    $oLayer["msClasses"][0]["styles"] = array();
                                    
                                    foreach ($tabInfoLayer as $val){
                                        $oLayer["layerProjectionEpsg"] = $val["srid"];
                                        $oLayer["msGeometryType"] = $val["type"];
                                        switch($val["type"]) {
                                          case "POINT" :
                                          case "MULTIPOINT" :
                                              $oLayer["msLayerType"] ="POINT";
                                              $oLayer["msClasses"][0]["styles"]["color"] = "#FF0000";
                                              $oLayer["msClasses"][0]["styles"]["symbol"] = "Cercle";
                                              $oLayer["msClasses"][0]["styles"]["size"] = "5";
                                              break;
                                          case "LINESTRING":
                                          case "MULTILINESTRING" :
                                              $oLayer["msLayerType"] ="LINE";
                                              $oLayer["msClasses"][0]["styles"]["color"] = "#4B4B4B";
                                              $oLayer["msClasses"][0]["styles"]["symbol"] = "Continue";
                                              $oLayer["msClasses"][0]["styles"]["size"] = "2";
                                              break;
                                          case "MULTIPOLYGON" :
                                          case "POLYGON" :
                                          case "GEOMETRY" :
                                              $oLayer["msLayerType"] ="POLYGON";
                                              $oLayer["msClasses"][0]["styles"]["color"] = "#808080";
                                              $oLayer["msClasses"][0]["styles"]["outlinecolor"] = "#4B4B4B";
                                              $oLayer["msClasses"][0]["styles"]["symbol"] = "Carre";
                                              $oLayer["msClasses"][0]["styles"]["size"] = "1";
                                              break;
                                          default :
                                              return false;
                                        }
                                    
                                        $oLayer["msLayerPgSchema"] = $val["f_table_schema"];
                                        $oLayer["msLayerPgTable"] = $params["layerTable"];
                                        $oLayer["msLayerPgGeometryField"] = $val["f_geometry_column"];
                                        $oLayer["msLayerPgIdField"] = "gid";
                                        $oLayer["msLayerPgProjection"] =  $val["srid"];
                                      
                                        //perimetreInfo can be =1 if there is no territorial restriction
                                        if(isset($params["perimetreTable"]) && $params["perimetreTable"]!="" && $params["perimetreInfo"]!="" && $params["perimetreInfo"]!="1"){
                                            $strPerimetresIds = "";
                                            foreach($params["perimetreInfo"] as $id => $perimetre){
                                                //$tabCouple[]=array($perimetre[0], $perimetre[2]);
                                                $strPerimetresIds.= "'".$perimetre[0]."',";
                                            }
                                            $strPerimetresIds = substr($strPerimetresIds, 0, -1);
                                            
                                            $data = "the_geom from (select a.* from ".$params["layerTable"]." a INNER JOIN ".$params["perimetreTable"].
                                                    " b ON (a.the_geom && b.the_geom AND st_intersects(a.the_geom, b.the_geom)) where b.".
                                                    $params["perimetreInfo"][0][2]." in (".$strPerimetresIds.")) as foo using unique gid using srid=".$val["srid"];
                                            $oLayer["msLayerData"] = $data;
                                        }
                                        
                                        
                                        
                                        
                                    
                                    }
                                    
                                    $oLayer["layerType"] ="POSTGIS";
                                    $oLayer["msLayerConnectionType"] = "POSTGIS";
                                     
                                    //default values
                                    
                                    

                            break;

                            case "WMS_PRODIGE":
                                
                                    $layer_name = self::neutralisationChaine($params["layerTitle"]);
                                    $oLayer["layerVisible"] = "true";
                                    $oLayer["layerType"] = "WMS";

                                    $oLayer["msLayerType"]="RASTER";
                                    $oLayer["msGeometryType"]="RASTER";
                                    
                                    $oLayer["layerTitle"] = $params["layerTitle"];
                                    $oLayer["layerName"] = $layer_name;
                                    $oLayer["msLayerName"] =  "";
                                    $oLayer["layerIdentifier"] =  $layer_name;
                                    
                                    $oLayer["layerServerUrl"] = $params["wmsUrl"];
                                    $oLayer["layerServerVersion"]= $params["wmsVersion"];
                                    $oLayer["layerWxsname"]= $params["wmsName"];
                                    $oLayer["layerProjectionEpsg"] = str_ireplace("epsg:","", $params["wmsSrs"]);
                                    $oLayer["wmsSrs"] = str_ireplace("epsg:","", $params["wmsSrs"]);
                                    $oLayer["layerOutputformat"] = $params["wmsFormat"];

                                    $oLayer["msClasses"] = array();
                                    $oLayer["msClasses"][0]= array();
                                    $oLayer["msClasses"][0]["name"] = $oLayer["layerTitle"];
                                    $oLayer["msClasses"][0]["title"] = $oLayer["layerTitle"];
                                    $oLayer["msClasses"][0]["styles"] = array();
                                    $oLayer["msClasses"][0]["styles"]["color"] = "#FFFFFF";
                                    $oLayer["msClasses"][0]["styles"]["symbol"] = "Carre";
                                    $oLayer["msClasses"][0]["styles"]["size"] = "1";
                                    
                                    
                 /*                   
                        {
   "mapId":3823,
   "layerAnalyseType":1,
   "layerTitle":"GEOFLA\u00ae, yc Dom",
   "layerName":"GEOFLAycDom",
   "layerFieldUrl":"",
   "layerMetadataUuid":"",
   "hrefLayerMetadataUuid":"",
   "layerMetadataFile":"",
   "hrefLayerMetadataFile":"",
   "layerServerUrl":"http://carto.atlasante.fr/cgi-bin/mapserv?",
   "layerServerVersion":"1.3.0",
   "layerWxsname":"GEOFLAycDom",
   "projection":null,
   "layerOutputformat":"image/png",
   "layerOpacity":100,
   "layerLegend":true,
   "layerLegendScale":false,
   "layerDownloadable":false,
   "layerWms":false,
   "layerWfs":false,
   "layerAtom":false,
   "layerVisible":true,
   "layerHaslabel":false,
   "layerIdentifier":"GEOFLAycDom",
   "layerMsname":"",
   "msLayerType":"RASTER",
   "msGeometryType":"RASTER",
   "layerType":"WMS",
   "msLayerConnectionName":"Atlasant\u00e9",
   "msLayerConnectionType":"WMS",
   "wmsTitle":"GEOFLA\u00ae, yc Dom",
   "wmsSrs":"2154",
   "geosourceLayerMetadata":{
      "needsUpdate":false,
      "publish":false,
      "uuid":"",
      "cswTitle":"",
      "cswAbstract":"",
      "cswInspiretheme":"",
      "id":"Carmen.Model.GeosourceLayerMetadata-7",
      "cswKeywords":""
   },
   "layerProjectionEpsg":"2154",
   "layerMinscale":50,
   "layerMaxscale":120000000,
   "fields":[

   ],
   "msClasses":[
      {
         "expression":null,
         "name":"GEOFLA\u00ae, yc Dom",
         "title":"GEOFLA\u00ae, yc Dom",
         "status":"ON",
         "msClassLegend":true,
         "styles":{
            "color":"#000000",
            "outlinecolor":null,
            "outlinewidth":null,
            "symbol":"Carre",
            "width":null,
            "size":"1",
            "minsize":null,
            "maxsize":null,
            "angle":null,
            "pattern":null
         },
         "id":"Carmen.Model.MsClass-7"
      }
   ],
   "metadata":{
      "geoide_wms_srs":"",
      "geoide_wfs_srs":"",
      "id":"Carmen.Model.msMetadataLayer-7"
   }
}*/
                                    
                                break;
                            case "WFS_PRODIGE":
/*{
   "mapId":3843,
   "layerAnalyseType":1,
   "layerTitle":"P\u00e9rim\u00e8tre de la carte g\u00e9ologique 50 000e - harmonis\u00e9",
   "layerName":"SCAN_H_GEOL50_PERIM",
   "layerFieldUrl":"",
   "layerMetadataUuid":"",
   "hrefLayerMetadataUuid":"",
   "layerMetadataFile":"",
   "hrefLayerMetadataFile":"",
   "layerServerUrl":"http://geoservices.brgm.fr/geologie",
   "layerServerVersion":"1.0.0",
   "layerWxsname":"SCAN_H_GEOL50_PERIMETRE",
   "projection":null,
   "layerOutputformat":"",
   "layerOpacity":100,
   "layerLegend":true,
   "layerLegendScale":false,
   "layerDownloadable":false,
   "layerWms":false,
   "layerWfs":false,
   "layerAtom":false,
   "layerVisible":true,
   "layerHaslabel":false,
   "layerIdentifier":"SCAN_H_GEOL50_PERIM",
   "layerMsname":"",
   "msGeometryType":"POINT",
   "msLayerType":"POINT",
   "msClassItem":null,
   "msClassDistribution":"SAMEAMPLITUDE",
   "msProcessingChartSize":null,
   "msProcessingChartSizeRange":null,
   "msLabelItem":"",
   "layerType":"WFS",
   "msLayerConnectionName":"BRGM \u2013 G\u00e9ologie, Hydrog\u00e9ologie et Gravim\u00e9trie",
   "msLayerConnectionType":"WFS",
   "wfsTitle":"P\u00e9rim\u00e8tre de la carte g\u00e9ologique 50 000e - harmonis\u00e9",
   "wfsSrs":"4326",
   "geosourceLayerMetadata":{
      "needsUpdate":false,
      "publish":false,
      "uuid":"",
      "cswTitle":"",
      "cswAbstract":"",
      "cswInspiretheme":"",
      "id":"Carmen.Model.GeosourceLayerMetadata-8",
      "cswKeywords":""
   },
   "layerProjectionEpsg":"4326",
   "layerMinscale":50,
   "layerMaxscale":120000000,
   "fields":[

   ],
   "msClasses":[
      {
         "expression":null,
         "name":"P\u00e9rim\u00e8tre de la carte g\u00e9ologique 50 000e - harmonis\u00e9",
         "title":"P\u00e9rim\u00e8tre de la carte g\u00e9ologique 50 000e - harmonis\u00e9",
         "status":"ON",
         "msClassLegend":true,
         "styles":{
            "color":"#000000",
            "outlinecolor":null,
            "outlinewidth":null,
            "symbol":"Cercle",
            "width":null,
            "size":8,
            "minsize":null,
            "maxsize":null,
            "angle":null,
            "pattern":null
         },
         "id":"Carmen.Model.MsClass-8"
      }
   ],
   "metadata":{
      "geoide_wms_srs":"",
      "geoide_wfs_srs":"",
      "id":"Carmen.Model.msMetadataLayer-8"
   },
   "msProcessingChartType":null,
   "msLabel":null
}*/
                                $getFeatureUrl = $params["wfsUrl"].
                                                 (strpos($params["wfsUrl"], "?")==false ? "?" : "&").
                                                 "REQUEST=GETFEATURE".
                                                 "&MAXFEATURES=10".
                                                 "&VERSION=".$params["wfsVersion"].
                                                 "&TYPENAME=".$params["wfsName"];
                                $tmp_gmlContents = file_get_contents($getFeatureUrl);
                                
                                $gml_feature_type = LibMap::getGMLFeatureType($tmp_gmlContents);
                                if(preg_match("/point/i", $gml_feature_type)) {
                                    $type = "POINT";
                                }
                                else if(preg_match("/line/i", $gml_feature_type)) {
                                    $type = "LINE";
                                }
                                else if(preg_match("/polygon/i", $gml_feature_type)) {
                                    $type = "POLYGON";
                                }
                                else {
                                    $type = "POINT";
                                }
                                $oLayer["msGeometryType"] = $type;
                                $oLayer["msLayerType"] = $type;
                                
                                $layer_name = self::neutralisationChaine($params["layerTitle"]);
                                
                                $oLayer["layerVisible"] = "true";
                                $oLayer["layerType"] = "WFS";
                                
                                $oLayer["layerTitle"] = $params["layerTitle"];
                                $oLayer["layerName"] = $layer_name;
                                $oLayer["msLayerName"] =  "";
                                $oLayer["layerIdentifier"] =  $layer_name;
                                
                                
                                //"msLayerConnectionName":"BRGM \u2013 G\u00e9ologie, Hydrog\u00e9ologie et Gravim\u00e9trie",
                                $oLayer["msLayerConnectionType"] = "WFS";
                                 
                                $oLayer["layerServerUrl"] = $params["wfsUrl"];
                                $oLayer["layerServerVersion"]= $params["wfsVersion"];
                                $oLayer["layerWxsname"]= $params["wfsName"];
                                
                                $oLayer["layerProjectionEpsg"] = str_ireplace("epsg:","", $params["wfsSrs"]);
                                $oLayer["wfsSrs"] = str_ireplace("epsg:","", $params["wfsSrs"]);
                                                                
                                $oLayer["msClasses"] = array();
                                $oLayer["msClasses"][0]= array();
                                $oLayer["msClasses"][0]["name"] = $oLayer["layerTitle"];
                                $oLayer["msClasses"][0]["title"] = $oLayer["layerTitle"];
                                $oLayer["msClasses"][0]["styles"] = array();
                                
                                switch($type) {
                                  case "POINT" :
                                      $oLayer["msClasses"][0]["styles"]["color"] = "#FF0000";
                                      $oLayer["msClasses"][0]["styles"]["symbol"] = "Cercle";
                                      $oLayer["msClasses"][0]["styles"]["size"] = "5";
                                      break;
                                  case "LINE":
                                      $oLayer["msClasses"][0]["styles"]["color"] = "#4B4B4B";
                                      $oLayer["msClasses"][0]["styles"]["symbol"] = "Continue";
                                      $oLayer["msClasses"][0]["styles"]["size"] = "2";
                                      break;
                                  case "POLYGON" :
                                      $oLayer["msClasses"][0]["styles"]["color"] = "#808080";
                                      $oLayer["msClasses"][0]["styles"]["outlinecolor"] = "#4B4B4B";
                                      $oLayer["msClasses"][0]["styles"]["symbol"] = "Carre";
                                      $oLayer["msClasses"][0]["styles"]["size"] = "1";
                                      break;
                                  default :
                                      return false;
                                }
                                
                                //default values
                                
                               
                                

                            break;
                        }
                        
                        //common parameters
                        $oLayer["layerVisible"] = "true";
                        $oLayer["layerOpacity"] = 99;
                        $oLayer["layerLegend"] = true;
                        $oLayer["layerMinscale"] =50;
                        $oLayer["layerMaxscale"] = 12000000;
                        $oLayer["layerAnalyseType"] = 1;
                        $oLayer["layerMetadataUuid"] =$params["uuid"];
                        $oLayer["layerMetadataFile"] = self::$container->getParameter("PRO_GEONETWORK_URLBASE").
                                                       "/srv/fre/catalog.search#/metadata/".$params["uuid"];
                        
                        
                        //$p_oLayer->setMetaData("GI_ARBO_ORDRE", 1);
                    return $oLayer;
                     
            }
        }
    }

    /**
     * @param $oMap Mapscript Object
     * @param $directory path to the mapfile
     * @param mapfile name of the mapfile
     * @return unknown_type
     */
    public static function createLegendIcon($p_oMap, $directory, $mapfile, $path){
        $legendBase = $directory."/IHM/LEGEND/".$path;
        @mkdir($legendBase, 0770);
        $mapName = str_replace(".map", "", $mapfile);

        for($i=0; $i<$p_oMap->numlayers;$i++) {
            $l_msLayer = $p_oMap->getLayer($i);

            $UrlImgLegPre  = $legendBase."/".$mapName."_".$l_msLayer->name."_";

            $UrlImgLegPost = ".jpeg";
            $iCountClass = $l_msLayer->numclasses;
            if($iCountClass>0) {

                for($j=0;$j<$iCountClass;$j++) {
                    $classCouche = $l_msLayer->getClass($j);
                    //image
                    $l_msIcon = $classCouche->createLegendIcon(20, 10);

                    $l_msIcon->saveImage($UrlImgLegPre."class".$j.$UrlImgLegPost);
                }
            }
        }
    }

    /**
     * Returns the gml feature type of the features in the gml file
     * @param $gml_file
     * @return
     */
    public static function getGMLFeatureType($gmlContents){
        $geometry_type = "unknown";

        $gml = new \DOMDocument('1.0', 'iso-8859-1');
        $gml->loadXML($gmlContents);
        $geometry_list = $gml->getElementsByTagName("msGeometry");
        if($geometry_list->length > 0) {
            //we take only the first item, because all the geometries are supposed to be the same
            //in the gml layer
            $geometry  = $geometry_list->item(0);
            if ($geometry->hasChildNodes()){
                $geometry_children = $geometry->childNodes;
                for ($i = 0; $i < $geometry_children->length; $i++)
                {
                    if ($geometry_children->item($i)->prefix == "gml"){
                        $geometry_type = $geometry_children->item($i)->localName;
                    }
                }
            }
        }
        return $geometry_type;
    }

    /**
     * brief Supprime les caractères spèciaux
     * @param $string
     * @return unknown_type
     */
    public static function neutralisationChaine($string) {
        $rslt = stripslashes($string);
        $rslt = self::removeAccents($rslt);
        $rslt = str_replace("'", "", $rslt);
        $rslt = str_replace("\"", "", $rslt);
        $rslt = str_replace('(', '', $rslt);
        $rslt = str_replace(')', '', $rslt);
        $rslt = str_replace(' ', '', $rslt);
        return $rslt;
    }
    
    /**
     * Retire tous les accents ou lettres accolées de la chaine en maintenant les lettres
     * Retourne la chaine transformée
     * @param string $str     chaine à traiter
     * @param string $charset encodage de la chaine, =utf-8 par défaut
     * @return string
     */
    protected static function removeAccents($str, $charset='utf-8')
    {
        $str = htmlentities($str, ENT_NOQUOTES, $charset);
    
        $str = preg_replace('#&([A-za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
        $str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str); // pour les ligatures e.g. '&oelig;'
        $str = preg_replace('#&[^;]+;#', '', $str); // supprime les autres caractères
    
        return $str;
    }
}