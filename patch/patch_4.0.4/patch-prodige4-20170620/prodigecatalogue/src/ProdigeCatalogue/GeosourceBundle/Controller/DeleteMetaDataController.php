<?php
namespace ProdigeCatalogue\GeosourceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\JsonResponse;
use Prodige\ProdigeBundle\Common\Util;
use Prodige\ProdigeBundle\Controller\BaseController;
use Prodige\ProdigeBundle\Controller\User;
use Prodige\ProdigeBundle\DAOProxy\DAO;
use Prodige\ProdigeBundle\Common\DBManager\ViewFactory;

 
/**
 * Service du suppression complet d'une fiche de métadonnée (suppr prodige, geosource, admincarto)
 *
 * @Route("/geosource")
 */
class DeleteMetaDataController extends BaseController
{

    /**
     *
     * @see prodige3.4/prodigecatalogue/PRRA/administration_carto_Action.php
     * @see prodige3.4/prodigeadmincarto/PRRA/Synchronisation_cartes.php
     * @see prodige3.4/prodigeadmincarto/PRRA/Synchronisation_couches.php 
     * @Secure(roles="ROLE_USER")
     * @Route("/canDeleteMetaData/{uuid}", name="catalogue_geosource_canDeleteMetaData", options={"expose"=true})
     */
    public function canDeleteMetaDataAction(Request $request, $uuid)
    {
        $return = array("maps"=>array(), "joins"=>array());
        $CATALOGUE = $this->getCatalogueConnection("catalogue,public");
        $PRODIGE = $this->getProdigeConnection("public");
        
        $layers = $CATALOGUE->executeQuery("select distinct couchd_emplacement_stockage, couchd_type_stockage from COUCHE_SDOM where UUID = :uuid", array("uuid"=>$uuid));
        foreach($layers as $layer){
            $maps = $this->forward('JoinBundle:LayerGetMaps:layerGetMaps', array(), array(
                "layerTable" => $layer["couchd_emplacement_stockage"],
                "coucheType" => $layer["couchd_type_stockage"],
            ));
            $maps = $maps->getContent();
            $maps = substr($maps, 1, -1);
            $maps = json_decode($maps, true);
            $return["maps"] = array_merge($return["maps"], $maps);
            
            $joins = $PRODIGE->fetchAll("select view_name from information_schema.view_table_usage where table_name=:table_name", array("table_name"=>strtolower($layer["couchd_emplacement_stockage"])));
            foreach($joins as $i=>$join){
                $joins[$i] = $join["view_name"];
            }
            if ( $joins ) $return["joins"] = array_merge($return["joins"], array_values($joins));
        }
        
        return new JsonResponse($return);
    }

    /**
     *
     * @see prodige3.4/prodigecatalogue/PRRA/administration_carto_Action.php
     * @see prodige3.4/prodigeadmincarto/PRRA/Synchronisation_cartes.php
     * @see prodige3.4/prodigeadmincarto/PRRA/Synchronisation_couches.php 
     * @Secure(roles="ROLE_USER")
     * @Route("/deleteMetaData/{uuid}", name="catalogue_geosource_deleteMetaData", options={"expose"=true})
     */
    public function deleteMetaDataAction(Request $request, $uuid = NULL)
    {
        $prodigeConnection = $this->getProdigeConnection('carmen');
        //$prodigeConnection->beginTransaction();
        
        $conn = $this->getCatalogueConnection('catalogue');
        //$conn->beginTransaction();
        
        $gntw = new \Prodige\ProdigeBundle\Services\GeonetworkInterface(PRO_GEONETWORK_URLBASE, 'srv/fre/', array(
            CURLOPT_HEADER => false
        ));
        if ($uuid == NULL) {
            $metadataId = $request->query->get('metadataId', 0);
            $coucheId = $request->query->get('coucheId', NULL);
            $stockageCarteId = $request->query->get('stockageCarteId', NULL);
            $mode = $request->query->get('mode', "");
        } else {
            // carte ou couche ?
            $results = $conn->fetchAll("SELECT m.id, fm.fmeta_fk_couche_donnees FROM public.metadata m LEFT JOIN fiche_metadonnees fm ON fm.fmeta_id::text=m.id::text WHERE m.uuid = :uuid", array(
                'uuid' => $uuid
            ));
            $metadataId = $coucheId = $stockageCarteId = NULL;
            foreach ($results as $result) {
                $metadataId = $result['id'];
                $coucheId = $result['fmeta_fk_couche_donnees'];
                break;
            }
            //carte_projet
            if (is_null($coucheId)) {
                $results = $conn->fetchAll("SELECT cartp_fk_stockage_carte FROM STOCKAGE_CARTE ".
                                           "left join CARTE_PROJET on CARTE_PROJET.cartp_fk_stockage_carte = ". 
                                           "STOCKAGE_CARTE.pk_stockage_carte LEFT JOIN FICHE_METADONNEES on CARTE_PROJET.cartp_fk_fiche_metadonnees = ".
                                           "FICHE_METADONNEES.pk_fiche_metadonnees LEFT JOIN public.metadata on metadata.id = FICHE_METADONNEES.fmeta_id::bigint ".
                                           " WHERE metadata.uuid= :uuid", 
                array(
                    'uuid' => $uuid
                ));
                foreach ($results as $result) {
                    $stockageCarteId = $result['cartp_fk_stockage_carte'];
                    break;
                }
            }
        }
        
        // var_dump('metadataId : '.$metadataId, 'coucheId : '.$coucheId, 'stockageCarteId : '.$stockageCarteId); die();
        
        $msg = "";
        $success = true;
        
        
        //cartes, cartes perso
        if (! is_null($stockageCarteId)) {
            
            $dao = new DAO($conn);
            if ($dao) {
                // construction de l'url de suppression des données
                $deldataurl = '';
                $query = 'SELECT STKCARD_PATH, CARTP_FORMAT, METADATA.ID, PK_CARTE_PROJET, CARTP_WMS FROM CARTE_PROJET LEFT JOIN STOCKAGE_CARTE on CARTE_PROJET.cartp_fk_stockage_carte =  
                    STOCKAGE_CARTE.pk_stockage_carte LEFT JOIN FICHE_METADONNEES on CARTE_PROJET.cartp_fk_fiche_metadonnees =
                    FICHE_METADONNEES.pk_fiche_metadonnees LEFT JOIN public.metadata on metadata.id = FICHE_METADONNEES.fmeta_id::bigint WHERE PK_STOCKAGE_CARTE = :id';
                $rs = $dao->BuildResultSet($query, array('id' => $stockageCarteId));
                $rs->First();
                $metadataId = $rs->Read(2);
                $pk_carte_projet = $rs->Read(3);
                $cartp_wms = $rs->Read(4);
                
                if (! $rs->EOF()) {
                    //delete admincarto
                    $bSuccess = $this->delCarte($rs->Read(1), $rs->Read(0));
                }
                // delete metadata and catalogue info
                try {
                    if ($cartp_wms!=0){
                        //suppression service WMS associé
                        $this->forward('GeosourceBundle:LayerAddToWebService:mapAddToWebService', array("uuid"=>$uuid), array("carteId"=>$pk_carte_projet));
                    }

                    $query = 'DELETE FROM STOCKAGE_CARTE WHERE PK_STOCKAGE_CARTE = :id';
                    $dao->Execute($query, array('id' => $stockageCarteId));
                    $query = 'DELETE FROM CARTE_PROJET WHERE PK_CARTE_PROJET=:pk_carte_projet';
                    $dao->Execute($query, array('pk_carte_projet' => $pk_carte_projet));
                    
                    if($metadataId!=""){
                        $query = 'DELETE FROM FICHE_METADONNEES WHERE FMETA_ID = :metadataId';
                        $dao->Execute($query, array('metadataId' => $metadataId));
                    } 

                        
                } catch (\Exception $ex) {
                    $msg .= $ex->getMessage().'Erreur lors de la suppression de la fiche de carte ' . $stockageCarteId;
                    $success = false;
                }
                
            }
        }
        
        //couches
        if (! is_null($coucheId)) {
            $dao = new DAO($conn);
            if ($dao) {
                
                $query = 'SELECT ACCES_SERVEUR.ACCS_ADRESSE_ADMIN, COUCHE_DONNEES.COUCHD_TYPE_STOCKAGE, COUCHE_DONNEES.COUCHD_EMPLACEMENT_STOCKAGE FROM COUCHE_DONNEES JOIN ACCES_SERVEUR ON COUCHE_DONNEES.COUCHD_FK_ACCES_SERVER = ACCES_SERVEUR.PK_ACCES_SERVEUR WHERE COUCHE_DONNEES.PK_COUCHE_DONNEES = :coucheId';
                $rs = $dao->buildResultSet($query, array(
                    'coucheId' => $coucheId
                ));
                $rs->First();
                if (! $rs->EOF()) {
                    // $deldataurl = (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"]=="on" ? "https" : "http").'://' . $rs->Read(0);
                    //if ($rs->Read(1) == "-3") { // tables
                        /*if ($mode == "majic") {
                            // $deldataurl .= '/HTML_MAJIC/deleteData.php?metadata_id='.$metadataId;
                            $this->rrmdir(PRO_MAPFILE_PATH . "majic/" . $metadata_id);
                        }
                    } else {
                        // $deldataurl .= '/PRRA/Synchronisation_couches.php?action=del&type=' . $rs->Read(1) . '&couche=' . base64_encode($rs->Read(2));
                        $this->delCouche($rs->Read(1), $rs->Read(2));
                    }*/
                    $this->delCouche($request, $rs->Read(1), $rs->Read(2));
                    
                }
                
                // suppression de la métadonnée et informations liées
                $query = 'DELETE FROM FICHE_METADONNEES WHERE FMETA_ID = :FMETA_ID';
                if ($dao->Execute($query, array('FMETA_ID' => $metadataId))) {
                    $query = 'DELETE FROM COUCHE_DONNEES WHERE PK_COUCHE_DONNEES = :PK_COUCHE_DONNEES';
                    if (!$dao->Execute($query, array( 'PK_COUCHE_DONNEES' => $coucheId))) {
                        $msg .= 'Erreur lors de la suppression de la fiche de couche ' . $coucheId;
                        $success = false;
                    }
                } else {
                    $msg .= 'Erreur lors de la suppression de la fiche de couche ' . $coucheId;
                    $success = false;
                }
            }
        }
        
        if ( $success ){
            try {
                
                $uuidParent = 0;
                if ( !is_null($coucheId) ){
                    // recuperation de l'uuid pour supresseion du tag SV_ServiceIdentifiaction dans la metadonnées de service
                    $tabResultSerie = $this->getDataAndUuidByMetadataId($metadataId);
                    $uuid = $tabResultSerie[1];
                    // on recherche et recupere le uuid de la fiche de l'ensemble de serie parente, si elle existe pour suppression des l'arbo des zip Atom
                    
                    $reponse = $gntw->getMetadataRelations($uuid, "parent", "json");
                    $this->getLogger()->info(__METHOD__, (array)$gntw->getLastQuery());
                    if ($reponse != "null") {
                        $reponse = json_decode($reponse, true);
                        if($reponse){
                            if ( isset($reponse["parent"]) && is_array($reponse["parent"]) && !empty($reponse["parent"]) ){
                                $uuidParent = $reponse["parent"][0]["id"];
                            }
                        }
                    }
                }
                
                $reponse = $gntw->getMetadataRelations($uuid, "fcats", "json");
                $this->getLogger()->info(__METHOD__, (array)$gntw->getLastQuery());
                if ($reponse != "null") {
                    $reponse = json_decode($reponse, true);
                    if($reponse){
                        if ( isset($reponse["fcats"]) && is_array($reponse["fcats"]) && !empty($reponse["fcats"]) ){
                            $fcat = $reponse["fcats"][0]["id"];
                            $fcat = $conn->fetchColumn("select id from public.metadata where uuid=:uuid", array("uuid"=>$fcat));
                            $reponse = $gntw->get('md.delete?_content_type=json&id=' . $fcat);
                            $this->getLogger()->info(__METHOD__, (array)$gntw->getLastQuery());
                        }
                    }
                }
                
                
                
                // bug transaction prodige/geonetwork, commiter prodige avant d'appeler le service geonetwork
                // @TODO incohérence si le service geonetwork échoue alors que l'on a déjà commité la partie prodige ! 
                //$conn->commit();
                //$prodigeConnection->commit();
                
                $reponse = $gntw->get('md.delete?_content_type=json&id=' . $metadataId);
                $this->getLogger()->info(__METHOD__, (array)$gntw->getLastQuery());

                // suppression de la tags SV_ServiceIdentifiaction dans la met de service
                $this->delTagOperateFromMetaService($metadataId, $uuid);
                // suppression de l'arborescence de téléchargement correspondant au flux ATOM
                // file_get_contents(PRO_CATALOGUE_URLBASE."Services/updateArbo.php?uuid=".$uuid."&uuidparent=".$uuid_parent);
                $uuidParent && $response = $this->forward('ProdigeBundle:UpdateArboController:updateArbo', array(
                    'r_uuid' => $uuid,
                    'r_uuidparent' => $uuidParent
                ));
            } catch (\Exception $ex) {
                throw $ex;
                $msg .= 'Erreur lors de la suppression de la fiche de service ' . $metadataId;
                $success = false;
            }
        } else {
            //$conn->rollBack();
            //$prodigeConnection->rollBack();
            $msg .= 'Erreur lors de la suppression de la métadonnée';
        }
        
        return new Response(json_encode(array(
            "success" => $success,
            "msg" => $msg
        )));
        // echo json_encode(array("success" => $success, "msg" => $msg));
        // exit();
        // break;
    }

    /**
     *
     * @param type $type            
     * @param type $carte            
     */
    private function delCarte($type, $carte)
    {
        switch ($type) {
            case 0: // carte projet
            case 2: // carte perso
                
                if ($carte != NULL) {
                    $map_id = "";
                    $connection = $this->getProdigeConnection('carmen');
                    $tabMaps = $connection->fetchAll("select map_id from map where map_file = :mapfile", array(
                        "mapfile" => substr($carte, 0, -4)//suppression .map
                    ));
                    foreach ($tabMaps as $val) {
                        $map_id = $val['map_id'];
                    }
                    if ($map_id != "") {
                        $acces_adress_admin = rtrim($this->getParameter('PRODIGE_URL_ADMINCARTO'), "/");
                        $carmenEditMapAction = $acces_adress_admin . "/api/map/delete/" . $map_id;
                        $ticket = \Alk\Common\CasBundle\Security\Authentication\Provider\CasProvider::getPGT($carmenEditMapAction);
                        $jsonResp = $this->curl($carmenEditMapAction, 'DELETE', array(
                            'ticket' => $ticket
                        ), array());
                        
                        $jsonObj = json_decode($jsonResp);
                        if($jsonObj && $jsonObj->status && $jsonObj->status="200" ){
                            return true;
                        }else{
                            return false;
                        }
                        /*if (is_file(PRO_MAPFILE_PATH . $carte))
                            @unlink(PRO_MAPFILE_PATH . $carte);*/
                    }
                }
                break;
            case 1:
                // carte statique
                if (is_file(PRO_MAPFILE_STATIC_PATH . $carte))
                    @unlink(PRO_MAPFILE_STATIC_PATH . $carte);
                break;
        }
    }

    /**
     *
     * @param type $type            
     * @param type $couche            
     */
    private function delCouche($request, $type, $couche)
    {
        $prodigeConnection = $this->getProdigeConnection("carmen");
        $dao = new DAO($prodigeConnection, 'public');
        
        switch ($type) {
            case 0:
                // couche raster
                break;
            case 1: // couche vecteur
            case - 3: // table
               
                if ($dao) {
                    $rs = $dao->buildResultSet('SELECT TABLENAME FROM PG_TABLES WHERE TABLENAME = :TABLENAME', array(
                        'TABLENAME' => $couche
                    ));
                    if ($rs->GetNbRows() > 0) {
                        $query = 'DROP TABLE "' . $couche . '" cascade';
                        if ($dao->Execute($query)) {
                            /*$query = 'DELETE FROM GEOMETRY_COLUMNS WHERE F_TABLE_NAME = :F_TABLE_NAME';
                            if ($dao->Execute($query, array(
                                'F_TABLE_NAME' => $couche
                            ))) {*/
                                $query = 'DROP SEQUENCE IF EXISTS "seq_' . $couche . '"';
                                $dao->Execute($query);
                            //}
                        }
                    }
                }
                
                // ConnectionFactory::CloseConnection();
                
                break;
            case - 4: // vues
                     // $couche = pg_escape_string($couche);
                     // $AdminPath = "Administration/";
                     // require_once($AdminPath."DAO/DAO/DAO.php");
                     // require_once($AdminPath."DAO/ConnectionFactory/ConnectionFactory.php");
                     // require_once("../HTML_JOIN/lib/viewFactory.class.php");
                     
                // ConnectionFactory::BeginTransaction();
                     // $dao = new DAO();
                if ($dao) {
                    $rs2 = $dao->buildResultSet('select pk_view, view_name from parametrage.prodige_view_info where view_name=:view_name', array(
                        'view_name' => ViewFactory::$INITIAL_VIEW_PREFIX . '_' . $couche
                    ));
                    if ($rs2->GetNbRows() > 0) {
                        $rs2->First();
                        $pk_view = $rs2->Read(0);
                        $view_composite_name = $rs2->Read(1);
                        $queries = array();
                        $queries[] = "DELETE FROM parametrage.prodige_computed_field_info WHERE pk_view=:pk_view;";
                        $queries[] = "DELETE FROM parametrage.prodige_join_info WHERE pk_view=:pk_view;";
                        $queries[] = "DELETE FROM parametrage.prodige_view_info WHERE pk_view=:pk_view;";
                        //$queries[] = "DELETE FROM geometry_columns WHERE f_table_name=:view_composite_name and f_table_schema='public';";
                        //$queries[] = "DELETE FROM geometry_columns WHERE f_table_name=:couche and f_table_schema='public';";
                        $queries[] = "DELETE FROM parametrage.prodige_view_composite_info WHERE pk_view=:pk_view;";
                        // $queries[] = "DROP VIEW IF EXISTS \"".$couche."\";";
                        $queries[] = "DROP VIEW IF EXISTS \"".$couche."\";";
                        // $queries[] = "DROP VIEW IF EXISTS \"".$view_composite_name."\";";
                        $queries[] = "DROP VIEW IF EXISTS \"".$view_composite_name."\";";
                        foreach ($queries as $strSQL) {
                            if ($dao->Execute($strSQL, array(
                                'pk_view' => $pk_view
                            ))){
                                
                            } 
                            
                        }
                    }
                }
                // ConnectionFactory::CloseConnection();
                break;
        }
        //delete layer from maps
        $this->delLayersFromMap($request, $type, $couche, false);
        
        
    }
    
    /**
     * delete layers from map
     * @param string $couche Le nom de la couche
     * @param string $type   Le type de couche
     * @return array
     * 
     * @Secure(roles="ROLE_USER")
     * @Route("/dellayersfrommap/{type}", name="catalogue_geosource_dellayersfrommap", options={"expose"=true}, requirements={"type"})
     */
    public function delLayersFromMap(Request $request, $type, $couche=null, $returnResponse=true)
    {
        $couche = $couche ?: $request->get("couche", null);
        $acces_adress_admin = rtrim($this->getParameter('PRODIGE_URL_ADMINCARTO'), "/");
        $prodigeConnection = $this->getProdigeConnection("carmen");
        
        // suppression de la couche de toutes les cartes

        $tabLayersToRemove = $this->getMapfilesForLayer($this->getRequest(), $couche, $type, false);
        
        foreach($tabLayersToRemove as $mapfile => $layers){

        //foreach($layers as $key => $layername){
                $layername = implode(",",$layers);
                $tabMaps = $prodigeConnection->fetchAll("select map.map_id from map inner join layer on layer.map_id = map.map_id where map_file = :mapfile".
                                             " and layer_msname in (:layer_msname) and published_id is null", array ("layer_msname" => $layername,"mapfile" =>$mapfile) );
                foreach ($tabMaps as $val){
                    $map_id = $val["map_id"];
                    //start edition
                    $carmenEditMapAction = $acces_adress_admin."/api/map/edit/".$map_id;
                    $ticket = \Alk\Common\CasBundle\Security\Authentication\Provider\CasProvider::getPGT($carmenEditMapAction);
                    $jsonResp = $this->curl($carmenEditMapAction, 'GET', array('ticket'=>$ticket),  array());
                    $jsonObj = json_decode($jsonResp);

                    if($jsonObj &&  $jsonObj->success && $jsonObj->map && $jsonObj->map->mapId  ){
                        $map_id_edition =  $jsonObj->map->mapId;
                        
                        $tabLayersEditMap = $prodigeConnection->fetchAll("select layer_id from map inner join layer on layer.map_id = map.map_id where map.map_id = :map_id".
                            " and layer_msname in (:layer_msname)", array ("layer_msname" => $layername,"map_id" =>$map_id_edition) );
                        
                        foreach ($tabLayersEditMap as $layerInfo){ 
                            $layer_id_edition = $layerInfo["layer_id"];
                            $carmenDeleteMapAction = $acces_adress_admin . "/api/layer/".$map_id_edition."/delete/" . $layer_id_edition;
                            $ticket = \Alk\Common\CasBundle\Security\Authentication\Provider\CasProvider::getPGT($carmenDeleteMapAction);
                            $jsonResp = $this->curl($carmenDeleteMapAction, 'DELETE', array(
                                'ticket' => $ticket
                            ), array());
                            $jsonObj = json_decode($jsonResp);
                            if($jsonObj && $jsonObj->status && $jsonObj->status="200" ){
                            
                            }else{
                                error_log("error while deleting layer ".$layer_id." from map ".$map_id);
                            }
                        }
                        //save map
                        $carmenSaveMapAction = $acces_adress_admin."/api/map/publish/".$map_id."/".$map_id_edition;
                        $ticket = \Alk\Common\CasBundle\Security\Authentication\Provider\CasProvider::getPGT($carmenSaveMapAction);
                        $jsonResp = $this->curl($carmenSaveMapAction, 'POST',  array('ticket'=>$ticket),   array("mapModel"=>"false", "mapFile" => $mapfile));
                        $jsonObj = json_decode($jsonResp);
                        if($jsonObj && $jsonObj->success && $jsonObj->map){

                        }else{
                            error_log("error while saving map ".$layer_id." from map ".$map_id);
                        }
                    }
                //}
                
            }
        }
        //since all mapfiles are not adminstrated in database, delete mannualy
        $tabLayersToRemove = $this->getMapfilesForLayer($this->getRequest(), $couche, $type, false);

        foreach($tabLayersToRemove as $mapfile => $layers){
            $file = PRO_MAPFILE_PATH.$mapfile.".map";
            try{
                $oMap = @ms_newMapObj($file);
            }catch(\Exception $exception){
                continue;
            }
            $bHasOne = false;
            for($i = 0; $i < $oMap->numlayers; $i ++) {
                $oLayer = $oMap->getLayer ( $i );
                foreach($layers as $key => $name ){
                    if ($oLayer->name ==  $name) {
                              $oLayer->set ( "status", MS_DELETE );
                              $bHasOne = true;
                    }
                }
            }
            error_log($bHasOne);
            if ($bHasOne) {
                $oMap->save ( $file );
            }
        }
        
        return new JsonResponse(array(
            "sucess"=>true,
            "action" => "Suppression de la couche des cartes qui la contiennent."));
    }

    /**
     * 
     * Parcours les mapfiles contenant une couche
     * @param string $couche Le nom de la couche
     * @param string $type   Le type de couche
     * @return array
     * 
     * 
     * @Secure(roles="ROLE_USER")
     * @Route("/mapfilesforlayer/{couche}/{type}", name="catalogue_geosource_mapfilesforlayer", options={"expose"=true}, requirements={"type","couche"})
     */
    public function getMapfilesForLayer(Request $request, $couche, $type, $returnResponse=true)
    {
        $tabLayersToRemove = $this->ScanDirectory(PRO_MAPFILE_PATH, $couche, $type);
        $tabLayersToRemove = array_merge($tabLayersToRemove, $this->ScanDirectory(PRO_MAPFILE_PATH . "layers", $couche, $type, true));
        $tabLayersToRemove = array_merge($tabLayersToRemove, $this->ScanDirectory(PRO_MAPFILE_PATH . "carteperso", $couche, $type, true));
        if ( $returnResponse ){
        	return new JsonResponse(array("mapfiles"=>$tabLayersToRemove));
        } else {
          return $tabLayersToRemove;
        }
    }

    /**
     * Parcours les mapfiles et applique la suppression de couche
     * 
     * @param $Directory chemin
     *            du répertoire de base
     * @param
     *            couche nom de la couche
     * @param
     *            type type de couche
     * @param
     *            bSubDir parcours recursif
     * @return unknown_type
     */
    private function ScanDirectory($Directory, $couche, $type, $bSubDir = false)
    {
    	  $Directory = rtrim($Directory,  "/");
        $MyDirectory = opendir($Directory) or die('Erreur');
        $tabLayersToRemove = array();
        while ($Entry = readdir($MyDirectory)) {
            if (is_dir($Directory . '/' . $Entry) && $Entry != '.' && $Entry != '..') {
                if ($bSubDir)
                    $tabLayersToRemove = array_merge($tabLayersToRemove, $this->ScanDirectory($Directory . '/' . $Entry, $couche, $type, $bSubDir));
            } else {
                if ($this->GetExtensionName($Entry) == "map") {
                	if ( strpos($Entry, 'temp_admin_')!==false ) continue;
                    try {
                        $oMap = @ms_newMapObj($Directory . "/" . $Entry);
                        if ($oMap) {
                            $tabLayers = $this->getLayersFromData($oMap, $couche, $type, $Directory . '/' . $Entry);
                            if(!empty($tabLayers)){
                                //remove .map and prefix to publication dir in array
                                $tabLayersToRemove[substr(str_replace(rtrim(PRO_MAPFILE_PATH, "/")."/", "", $Directory . "/" . $Entry), 0, -4)] = $tabLayers;
                            }
                        }
                    } catch(\Exception $ex) {
                        error_log("impossible d'ouvrir " . $Directory . "/" . $Entry);
                    }
                }
            }
        }
        return $tabLayersToRemove;
    }

    /**
     * brief : fonction retournant les contenus de data et uuid à partir de l'id metadata
     * 
     * @param $metadata_id :
     *            id de la fiche de meta de serie de donnees
     * @return : tableau avec en 0: le data; en 1 l'uuid
     *        
     */
    private function getDataAndUuidByMetadataId($metadata_id)
    {
        
        $tabResult = array();
        
        $conn = $this->getCatalogueConnection();
        $dao = new DAO($conn, 'public,catalogue');
        //$dao->beginTransaction();
        
        $strSql = "SELECT data, uuid from metadata where id = :id";
        $rs2 = $dao->buildResultSet($strSql, array(
            'id' => $metadata_id
        ));
        
        for ($rs2->First(); ! $rs2->EOF(); $rs2->Next()) {
            $tabResult[0] = $rs2->ReadHtml(0);
            $tabResult[1] = $rs2->Read(1);
        }
        return $tabResult;
    }


    /**
     * brief supprime une couche d'un service
     * 
     * @param oMap objet Mapserver
     * @param data nom de la couche
     * @param type stockage 0 raster 1 Postgis
     */
    private function getLayersFromData ($oMap, $data, $type_stokage, $mapfile)
    {
        $tabLayers = array();
        if ( !$data ) return $tabLayers;
        for ($i = 0; $i < $oMap->numlayers; $i ++) {
            $oLayer = $oMap->getLayer($i);
            switch ($type_stokage) {
                case 0:
                    if ($oLayer->data == PRO_MAPFILE_PATH . $data) {
                        //$oLayer->set("status", MS_DELETE);
                        $tabLayers[]=$oLayer->name;
                    }
                    break;
                case 1:
                case - 4:
                    //error_log($mapfile. " ". $oLayer->data." ".preg_match("/\b".$data."\b/i", $oLayer->data));
                    if (preg_match("/\b" . $data . "\b/i", $oLayer->data)) {
                        // if (strpos($oLayer->data, $data." as foo using unique gid") !==false || (strpos($oLayer->data, $data.") as foo using unique gid") !==false ))
                        //$oLayer->set("status", MS_DELETE);
                        $tabLayers[]=$oLayer->name;
                    }
                    break;
            }
        }
        return $tabLayers;
    }

    private function CSCarteLayers_UpdateLayersOrder($oMap)
    {
        // reorder the layers
        // reaffect the right order to the layers : GI_ARBO_ORDRE = $oMap->numlayers - index
        $bResult = null;
        
        if (isset($oMap) && ! empty($oMap)) {
            $list = array();
            $count = 0; // at the end of the loop = number of layers not deleted
            for ($i = 0; $i < $oMap->numlayers; $i ++) {
                $oLayer = $oMap->getLayer(($oMap->numlayers - 1) - $i);
                if (! is_null($oLayer) && ($oLayer->status != MS_DELETE)) {
                    $count ++;
                    $oLayer->setMetaData("GI_ARBO_ORDRE", $count);
                }
            }
            $bResult = $oMap;
        }
        return $bResult;
    }

    /**
     * brief : fonction retirant le tag <srv:operateOn sous srv:SV_ServiceIdentifiaction
     * dans la fiche de meta de service (id=-8) identifié par le uuid de la metadata
     * lors de la suppresion d'une fiche de métadonnee de serie de données
     * 
     * @param $metadata_id :
     *            id de la fiche de meta de serie de donnees
     */
    private function delTagOperateFromMetaService($metadata_id, $uuid = -1)
    {
        
        $metadata_data_Service = "";
        $tabResultService = $this->getDataAndUuidByMetadataId(PRO_DOWNLOAD_METADATA_ID);
        $metadata_data_Service = $tabResultService[0];
        
        /**
         * ***************************************
         * cas des métadonnées de service
         * ****************************************
         */
        $version = "1.0";
        $encoding = "UTF-8";
        $metadata_doc_service = new \DOMDocument($version, $encoding);
        // chargement de la métadonnée
        $entete = "<?xml version=\"" . $version . "\" encoding=\"" . $encoding . "\"?>\n";
        $metadata_data_Service = $entete . $metadata_data_Service;
        if (@$metadata_doc_service->loadXML($metadata_data_Service) !== false) {
            $xpath_service = new \DOMXpath($metadata_doc_service);
            $nodeToDelete = $xpath_service->query("/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/srv:operatesOn[@uuidref=\"" . $uuid . "\"]");
            if ($nodeToDelete->length > 0) {
                foreach ($nodeToDelete as $key => $node) {
                    foreach ($node->attributes as $attr) {
                        // $tabAttr[$key][$attr->localName] = $attr->nodeValue;
                        if ($attr->localName == "uuidref" && $attr->nodeValue == $uuid) {
                            $node->parentNode->removeChild($node);
                        }
                    }
                }
            }
        }
        // save
        $new_metadata_data = $metadata_doc_service->saveXML();
        $new_metadata_data = str_replace($entete, "", $new_metadata_data);
        
        $geonetwork = new \Prodige\ProdigeBundle\Services\GeonetworkInterface(PRO_GEONETWORK_URLBASE, 'srv/fre/');
        $urlUpdateData = "md.edit.save";
        $formData = array(
            "id"=> PRO_DOWNLOAD_METADATA_ID,
            "data" => $new_metadata_data
        );
        //send to geosource
        $geonetwork->post($urlUpdateData, $formData);
        
    }
    
    /*
     * GetExtensionName - Renvoie l'extension d'un fichier . $File (char): Nom du fichier . $Dot (bool): avec le point true/false
     */
    private function GetExtensionName($File, $Dot = false)
    {
        if ($Dot == true) {
            $Ext = strtolower(substr($File, strrpos($File, '.')));
        } else {
            $Ext = strtolower(substr($File, strrpos($File, '.') + 1));
        }
        return $Ext;
    }
}
