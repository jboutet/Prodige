<?php

namespace ProdigeCatalogue\GeosourceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\JsonResponse;

use ProdigeCatalogue\GeosourceBundle\Common\MetadataDesc;
use ProdigeCatalogue\GeosourceBundle\Common\ClassRasterInfo;

use Prodige\ProdigeBundle\Controller\BaseController;
use Prodige\ProdigeBundle\Controller\User;
use Prodige\ProdigeBundle\DAOProxy\DAO;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use ProdigeCatalogue\GeosourceBundle\Common\LibMap;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Types\Type;

/**
 * @Route("/geosource")
 */
class PanierDownloadFrontalParametrageController extends BaseController {
    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/panierDownloadFrontalParametrage/{uuid}", name="catalogue_geosource_panierDownloadFrontalParametrage", options={"expose"=true })
     */
    public function catalogue_geosource_panierDownloadFrontalParametrageAction(Request $request, $uuid="") {

        
        unset($user);

        $user = User::GetUser();
        $userId = User::GetUserId();

        if(User::GetUserGeneric()) {
            $userEmail = "";
        } else {
            $userEmail = $user->GetEmail();
        }

        if(!is_null($userId))
            User::SetUserCookie($userId);
          
            if(!$user->HasTraitement('TELECHARGEMENT')) {
                //header('location:Erreurs/Telechargement.php');
                \Prodige\ProdigeBundle\Common\SecurityExceptions::throwAccessDeniedExceptionTelechargement();
                exit(0);
            }
            
            MetadataDesc::setConnection($this->getCatalogueConnection('catalogue'));
            MetadataDesc::setContainer($this->container);

            $m_debug = false;
            $m_styleCommunicationDisplay = "block";
            $m_styleCommunicationVisibility = "visible";
            $m_styleDim = "300";
            if(!$m_debug) {
                $m_styleCommunicationDisplay = "none";
                $m_styleCommunicationVisibility = "hidden";
                $m_styleDim = "0";
            }

            $m_MeatadatIdts = "";
            
            //param uuid present in URL
            if($uuid!=""){
                $tabMetadata = $this->getCatalogueConnection('public')->fetchAll("select id from metadata where metadata.uuid =:uuid",array ("uuid" => $uuid));
                foreach($tabMetadata as $key => $info){
                    $m_MeatadatIdts  = $info["id"];
                }
            }
            if($request->get("LAYERIDTS", false)) {
                $m_MeatadatIdts = $request->get("LAYERIDTS");
            }
            

            //Initialisation de la liste des territoires
            /*if(!isset($_POST["PRO_TERRITOIRES_TYPES"])&& !isset($_POST["territories"])){
             header("location:".PRO_CARTO_URLBASE."PRRA/panierDownloadFrontal_parametrage_sql.php?iMode=init&LAYERIDTS=".$m_MeatadatIdts);
             }else{
             $_PRO_TERRITOIRES_TYPES = array();
             $_PRO_TERRITOIRES_TYPES = $_POST["PRO_TERRITOIRES_TYPES"];
             }*/
            //declare global variables
            // global $_PRO_TERRITOIRES_TYPES;

            //get POST variables
            $territoire_array         =   $request->request->get("territories", array());
            $select_format            =   $request->request->get("SELECT_FORMAT", "");
            $select_proj              =   $request->request->get("SELECT_PROJ", "");
            $check_extract_territoire =   $request->request->get("check_extract_territoire", 0);
            $select_territoire_type   =   $request->request->get("SELECT_TERRITOIRE_TYPE", "");
            $extract_tolerance        =   $request->request->get("EXTRACT_TOLERANCE", 100);

            // Verification des droits de l'utilisateur sur les données
            $PreIdts = explode("|", $m_MeatadatIdts);
            $m_Idts_local   = array();  // tableau des identifiants de métadonnées téléchargeable en local
            $m_Idts_distant = array();  // tableau des identifiants de métadonnées téléchargeable à distance

            //config 
            $acces_adress_admin = $this->getParameter('PRODIGE_URL_ADMINCARTO');
            $url_srv_front = $this->getParameter('PRODIGE_URL_FRONTCARTO');
            $url_srv_tele  = $this->getParameter('PRODIGE_URL_TELECARTO');
            $acces_service_admin = 1;
            
            $tabRestrictTerr = array();
            if($PreIdts[0] != "") {

                //$dao = new DAO();
                $conn = $this->getCatalogueConnection('catalogue');
                $dao = new DAO($conn, 'catalogue');
                if($dao) {
                    for($i = 0; $i < count($PreIdts); $i++) {
                        if($PreIdts[$i]!=""){

                            /** vérification des droits sur la donnée **/
                            //0 la métadonnée est moissonnée, il s'agit d'un téléchargement direct
                            $query = 'SELECT isharvested, data FROM public.metadata where id =?';
                            $rs = $dao->BuildResultSet($query, array($PreIdts[$i]));
                            $rs->First();
                            $bAllow = ($rs->Read(0) == "y");
                            if($bAllow) {
                                array_push($m_Idts_distant, $PreIdts[$i]);
                                continue;
                            }
                            //1 si la donnée est publique, elle est libre d'accès
                            $query = 'SELECT couchd_download FROM couche_donnees WHERE pk_couche_donnees in (select fmeta_fk_couche_donnees from fiche_metadonnees where  FMETA_ID =?)';

                            $rs = $dao->BuildResultSet($query, array($PreIdts[$i]));
                            $rs->First();
                            $bAllow = $rs->Read(0);
                            if(!$bAllow) {
                                //2 vérification des droits par dom/sdom	et par objet
                                $query = 'SELECT DOM_NOM, SSDOM_NOM, PK_COUCHE_DONNEES FROM COUCHE_SDOM WHERE FMETA_ID =?';
                                $rs = $dao->BuildResultSet($query, array($PreIdts[$i]));
                                for($rs->First(); !$rs->EOF(); $rs->Next()) {
                                    $domaine = $rs->read(0);
                                    $sous_domaine = $rs->read(1);
                                    $objetCouche = $rs->read(2);
                                    //vérification de l'autorisation sur le domaine et sur l'objet
                                    $bAllow = $user->HasTraitement('TELECHARGEMENT', html_entity_decode($domaine, ENT_QUOTES, 'UTF-8'), html_entity_decode($sous_domaine, ENT_QUOTES, 'UTF-8'), PRO_TRT_TYPE_TELECHARGEMENT, $objetCouche, PRO_OBJET_TYPE_COUCHE);
                                    if($bAllow)
                                        break;
                                }
                                //3 vérification des restrictions de compétences
                                if(isset($objetCouche)){
                        				    $bAllow = $bAllow && $user->HasTraitementCompetence('TELECHARGEMENT', $objetCouche);
                            				//4 vérification des restricitions territoriales
                            				$trTerr = $user->GetTraitementTerritoire('TELECHARGEMENT', $objetCouche, $acces_adress_admin);
                            				if($trTerr===false){
                        				        $bAllow = false;
                            				}
                            				if(is_array($trTerr))  {
                              					//tableau des données restreintes territorialement avec leurs territoires
                               					$tabRestrictTerr[$PreIdts[$i]] = $trTerr;
                            				}
                        				}else{//identifiant inchohérent
                        				    $bAllow = false;
                        				}
                            }
                            if($bAllow) {
                                array_push($m_Idts_local, $PreIdts[$i]);
                            }
                        }
                        /** fin vérification des droits sur la donnée **/
                    }
                }
            }
            if(count($m_Idts_local) > 0) {
                $m_serveurs = MetadataDesc::GetServeurs($m_Idts_local);
            }
            else {
                $m_serveurs = array();
            }
            $m_serveur = "";
            $ecwAreaLimit = 1.8e308; //PHP max float value...
            $gTiffAreaLimit = 1.8e308; //PHP max float value...

            $tabTablename_vector  = array();
            $tabMetadataId_vector = array();
            $tabCoucheName_vector = array();

            $tabTablename_raster  = array();
            //$tabVrtPath_raster = array();
            $tabData_raster = array();
            $tabMetadataId_raster = array();
            $tabCoucheName_raster = array();

            $raster_srid = "";
            $raster_format = "";
            $is_raster_hr = false;

            $tabTablename_majic  = array();
            $tabMetadataId_majic = array();
            $tabCoucheName_majic = array();

            $tabTablename_table  = array();
            $tabMetadataId_table = array();
            $tabCoucheName_table = array();
            if(count($m_serveurs) > 0) {
                // a priori toute ses données sont localisées
                // sur un système où les serveurs ne varient pas...
                // $m_serveur contient les informations relatives aux différents serveurs
                $m_serveur = $m_serveurs[0];

                $m_Idts_vector = array();
                $m_Idts_raster = array();
                $m_Idts_majic = array();
                $m_Idts_table = array();

                foreach($m_Idts_local as $id) {
                    $meta_desc = new MetadataDesc($id);
                    switch($meta_desc->getType()) {
                        case MetadataDesc::$TYPE_STOCKAGE_POSTGIS :
                        case MetadataDesc::$TYPE_STOCKAGE_VUES :
                            array_push($m_Idts_vector,$id);
                            break;
                        case MetadataDesc::$TYPE_STOCKAGE_RASTER :
                            array_push($m_Idts_raster,$id);
                            break;
                        case MetadataDesc::$TYPE_STOCKAGE_MAJIC :
                            array_push($m_Idts_majic,$id);
                            break;
                        case MetadataDesc::$TYPE_STOCKAGE_TABLE :
                            array_push($m_Idts_table,$id);
                            break;
                        default : //majic files...
                            break;
                    }
                }

                // chaîne contenant le nom des tables séparée par "%"
                // chaîne contenant les identifiants des métadonnées séparée par "%"

                foreach($m_Idts_local as $id) {
                    $meta_desc = new MetadataDesc($id);
                    //if ( $meta_desc->GetTable() != "" ) {
                    switch($meta_desc->getType()) {
                        case MetadataDesc::$TYPE_STOCKAGE_POSTGIS :
                        case MetadataDesc::$TYPE_STOCKAGE_VUES :
                            $tabTablename_vector[]  = $meta_desc->GetTable();
                            $tabCoucheName_vector[$meta_desc->GetId()] = array("name"=> ($meta_desc->GetNom()), "server" => $meta_desc->GetSrv());
                            $tabMetadataId_vector[] = $id;
                            break;
                        case MetadataDesc::$TYPE_STOCKAGE_RASTER :
                            //$raster_info =  new RasterInfo($id);
                            ClassRasterInfo::setConnection($this->getCatalogueConnection('catalogue'));
                            $raster_info =  new ClassRasterInfo($id);
                            

                            //TODO obedel handle this better way
                            if(empty($raster_info->r_info))
                                //die("La couche raster n'a pas été décrite dans raster_info...\n");
                                exit();

                            $ecwAreaLimit = min($ecwAreaLimit, $raster_info->getMaxAreaForECW());
                            $gTiffAreaLimit = min($gTiffAreaLimit, $raster_info->getMaxAreaForGTIFF());
                            $vrtPath = $raster_info->GetVrtPath();

                            if($vrtPath != "") {
                                $tabTablename_raster[]  = $meta_desc->GetTable();
                                $dataLimit_raster = array(
                                    0 => $raster_info->GetInfo("ulx"),
                                    1 => $raster_info->GetInfo("uly"),
                                    2 => $raster_info->GetInfo("lrx"),
                                    3 => $raster_info->GetInfo("lry")
                                );

                                if(abs($raster_info->GetInfo("resx")) < RASTER_HR_RESOLUTION||abs($raster_info->GetInfo("resy")) < RASTER_HR_RESOLUTION) {
                                    $is_raster_hr = true;
                                }

                                $raster_srid= $raster_info->GetInfo("srid");
                                $raster_format= $raster_info->GetInfo("format");
                                $dataColorInterp = $raster_info->GetInfo("color_interp");
                                $tabData_raster[] =  $raster_info->GetVrtPath() . ":" . implode(',', $dataLimit_raster) .  ":" . $dataColorInterp;
                                $tabCoucheName_raster[$meta_desc->GetId()] = array("name"=> ($meta_desc->GetNom()), "server" => $meta_desc->GetSrv());
                                $tabMetadataId_raster[] = $id;
                            }
                            break;
                        case MetadataDesc::$TYPE_STOCKAGE_MAJIC ://majic files
                            $tabTablename_majic[]  = $meta_desc->GetTable();
                            $tabCoucheName_majic[$meta_desc->GetId()] = array("name"=> ($meta_desc->GetNom()), "server" => $meta_desc->GetSrv());
                            $tabMetadataId_majic[] = $id;

                            break;
                        case MetadataDesc::$TYPE_STOCKAGE_TABLE ://tables
                            $tabTablename_table[]  = $meta_desc->GetTable();
                            $tabCoucheName_table[$meta_desc->GetId()] = array("name"=> ($meta_desc->GetNom()), "server" => $meta_desc->GetSrv());
                            $tabMetadataId_table[] = $id;

                            break;
                    }
                    //}
                }
            }
            
            $fnStringify = function($array){
                return implode("%", array_map(function($v){return (is_array($v) ? implode("%", $v) : $v);}, $array));
            };
            $strTablename_vector  = $fnStringify($tabTablename_vector);
            $strMetadataId_vector = $fnStringify($tabMetadataId_vector);

            $strTablename_raster  = $fnStringify($tabTablename_raster);

            $strData_raster       = $fnStringify($tabData_raster);
            $strMetadataId_raster = $fnStringify($tabMetadataId_raster);

            $strTablename_majic   = $fnStringify($tabTablename_majic);
            $strMetadataId_majic  = $fnStringify($tabMetadataId_majic);

            $strTablename_table   = $fnStringify($tabTablename_table);
            $strMetadataId_table  = $fnStringify($tabMetadataId_table);

            $table_cb_id = "table_cb_territoire";

            $ar_permanent_parameters = array(
                                            "PRO_CSS_FILE"=> (defined("PRO_CSS_FILE") && PRO_CSS_FILE != "" ? PRO_CSS_FILE : ""),
                                            "PRO_TIMEOUT_TELE_DIRECT"=>(PRO_TIMEOUT_TELE_DIRECT !== null ? PRO_TIMEOUT_TELE_DIRECT : 30000),
                                            "url_srv_tele"=>($url_srv_tele !== null ? $url_srv_tele :$this->getParameter("PRODIGE_URL_TELECARTO") ),
                                            "URL_SVR_FRONT"=>($url_srv_front !== null ? $url_srv_front :$this->getParameter("PRODIGE_URL_FRONTCARTO")),
                                            "const_serveurCarto"=>($acces_adress_admin !== null ? $acces_adress_admin : $this->getParameter("PRODIGE_URL_ADMINCARTO")),
                                            "URL_SVR_ADMIN"=>($acces_adress_admin !== null ? $acces_adress_admin : $this->getParameter("PRODIGE_URL_FRONTCARTO")),
                                            "ACCS_SERVICE_ADMIN"=>($acces_service_admin !== null ? $acces_service_admin : "1" ),
                                            "URL_SVR_CATALOGUE"=>$request->server->get("HTTP_HOST"),
                                            "IDX"=>($acces_service_admin !== null ? $acces_service_admin : accs_service_admin),
                                            "ecwAreaLimit"=>$ecwAreaLimit,
                                            "gTiffAreaLimit"=>$gTiffAreaLimit,
                                            "providerUrl"=>$this->generateUrl('prodige_getAreasFromDB'),
                                            "service_idx"=>($acces_service_admin !== null ? $acces_service_admin : accs_service_admin),
                                            "table_cb_id"=>$table_cb_id
                                        );

            $ar_template_parameters = array(
                "userId"=>$userId,
                "userEmail"=>$userEmail,
                "extract_tolerance"=>$extract_tolerance,
                "m_serveur"=>$m_serveur,
                "m_serveurs"=>$m_serveurs,
            );
            $template_name = array();
            if($m_serveur != "" || count($m_Idts_distant) > 0) {
                if(isset($m_Idts_vector) && count($m_Idts_vector) > 0) {
                    $template_name[] = "panierDownloadFrontal_parametrage_vector_template";
                    $ar_template_parameters = array_merge($ar_template_parameters, array(
                                                  "tabTablename_vector"=>$tabTablename_vector,
                                                  "strMetadataId_vector"=>$strMetadataId_vector,
                                                  "tabCoucheName_vector"=>$tabCoucheName_vector,
                                                  "tabMetadataId_vector"=>$tabMetadataId_vector,
                                                  "tabRestrictTerr"=>$tabRestrictTerr,
                                                  "m_Idts_table"=>$m_Idts_table,
                                                  "m_Idts_vector"=>$m_Idts_vector,
                                                  "check_extract_territoire"=>$check_extract_territoire,
                                              ));
                }
                if(isset($m_Idts_raster) && count($m_Idts_raster) > 0) {
                    $template_name[] = "panierDownloadFrontal_parametrage_raster_template"; 
                    $ar_template_parameters = array_merge($ar_template_parameters, array(
                                                  "strData_raster"=>$strData_raster,
                                                  "strMetadataId_raster"=>$strMetadataId_raster,
                                                  "tabCoucheName_raster"=>$tabCoucheName_raster,
                                                  "raster_format"=>$raster_format,
                                                  "is_raster_hr"=>$is_raster_hr,
                                                  "raster_srid"=>$raster_srid,
                                                  "m_Idts_raster"=>$m_Idts_raster,
                                              ));
                }
                if(isset($m_Idts_majic) && count($m_Idts_majic) > 0) {
                    $template_name[] = "panierDownloadFrontal_parametrage_magic_template";
                    $ar_template_parameters = array_merge($ar_template_parameters, array(
                                                  "m_Idts_majic"=>$m_Idts_majic,
                                                  "strMetadataId_majic"=>$strMetadataId_majic,
                                                  "tabCoucheName_majic"=>$tabCoucheName_majic,
                                              ));
                }
                if(isset($m_Idts_table) && count($m_Idts_table) > 0) {
                    $template_name[] = "panierDownloadFrontal_parametrage_table_template";
                    $ar_template_parameters = array_merge($ar_template_parameters, array(
                                                  "strTablename_table"=>$strTablename_table,
                                                  "strMetadataId_table"=>$strMetadataId_table,
                                                  "tabCoucheName_table"=>$tabCoucheName_table,
                                                  "m_Idts_table"=>$m_Idts_table,
                                              ));
                }
                if(isset($m_Idts_distant) && count($m_Idts_distant) > 0) {
                    $template_name[] = "panierDownloadFrontal_parametrage_distant_template";
                    $ar_template_parameters = array_merge($ar_template_parameters, array(
                                                  "m_Idts_distant"=>$m_Idts_distant,
                                              ));
                }
                $ar_template_parameters["template_name"] = $template_name;
            } else {
                $template_name[] = "empty_template";
                $ar_template_parameters = array("template_name"=>$template_name);
            }

            $ar_all_parameters = array_merge($ar_permanent_parameters, $ar_template_parameters);

            return $this->render('GeosourceBundle:Default:panierDownloadFrontal_parametrage_template.html.twig', $ar_all_parameters);
    }
    
    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/downloadLayersGetMapfile/{modele}", name="catalogue_geosource_downloadlayers_getmapfile", options={"expose"=true })
     */
    public function catalogueGeosourceDownloadLayersGetMapfileAction(Request $request, $modele) {
        $user_id = User::GetUserId();
        $table_schema = "public";
        $CATALOGUE = $this->getCatalogueConnection('catalogue');
        $PRODIGE = $this->getProdigeConnection("carmen,".$table_schema);
        
        $map_file = preg_replace("!^modele_|.map$!", "", $modele);
        list($mapId, $srid) = $PRODIGE->fetchArray("select map_id, map_projection_epsg from carmen.map where map_file=:map_file and published_id is null", array("map_file"=>$map_file));
        
        $metadatas =  $request->request->get("metadata", array());
        
        $extents = array(); 
        $classes = array(); 
        $params = array();
        $paramtypes = array();
        foreach ($metadatas as $metadata){
            $sources = $CATALOGUE->fetchAll("select pk_couche_donnees, couchd_emplacement_stockage, couchd_type_stockage, raster_info.* ".
                    " from fiche_metadonnees ".
                    " inner join couche_donnees on (pk_couche_donnees=fmeta_fk_couche_donnees) ".
                    " left join raster_info on (pk_couche_donnees=raster_info.id) ".
                    " where fmeta_id=:fmeta_id", array("fmeta_id" => $metadata["id"]));
            
            if ( !is_numeric(key($sources)) ) $sources = array($sources);
            
            foreach($sources as $source){
                $pk_couche_donnees = $source["pk_couche_donnees"];
                $couchd_emplacement_stockage = $source["couchd_emplacement_stockage"];
                $name = $metadata["name"].(count($sources)>1 ? " [".$couchd_emplacement_stockage."]" : "");
                $gid = preg_replace("![/.]!", "_", $couchd_emplacement_stockage);
                
                switch($source['couchd_type_stockage']){
                    case MetadataDesc::$TYPE_STOCKAGE_POSTGIS :
                        $couchd_emplacement_stockage = strtolower($couchd_emplacement_stockage);
                        $tableExists = $PRODIGE->fetchColumn("select true from information_schema.tables where table_schema=:table_schema and table_name=:table_name".
                                                " union select true from information_schema.views where table_schema=:table_schema and table_name=:table_name", array("table_name"=>$couchd_emplacement_stockage, "table_schema"=>$table_schema));
                        
                        if ( $tableExists ){
                            
                            
                            $extents[] = "select '".$PRODIGE->fetchColumn(
                                    "select ".
                                    "  public.st_setsrid(public.st_extent(public.st_transform(the_geom, ".$srid.")), ".$srid.")::geometry as the_geom ".
                                    " from ".$table_schema.".".$couchd_emplacement_stockage
                                )."'::geometry as the_geom".
                                ", '".str_replace("'", "''", $name)."'::text as name ".
                                ", '".str_replace("'", "''", $gid)."'::text as gid ";

                            $outlinecolor = $this->getRandomColor();
                            $classes[] = array( 
                                "name" => $name,
                                "title" => $name,
                                "expression" => "/^".preg_quote($gid, "/")."$/",
                                "styles" => array(
                                    "outlinecolor" => $outlinecolor,
                                    "outlinesize" => 2,
                                    "outlinewidth" => 2,
                                    "symbol" => "Carre",
                                    "size" => "1"
                                )
                            );
                            
                        }
                    break;
                    case MetadataDesc::$TYPE_STOCKAGE_RASTER :
                        $extentSource = array("ulx"=>$source["ulx"], "uly"=>$source["uly"], "lrx"=>$source["lrx"], "lry"=>$source["lry"]);
                        $extentSource = array_diff($extentSource, array(" ", "", null));
                        if ( count($extentSource)!=4 ) continue;
                        $extentCoords = array(
                            implode(" ", array($extentSource["ulx"], $extentSource["uly"])),
                            implode(" ", array($extentSource["lrx"], $extentSource["uly"])),
                            implode(" ", array($extentSource["lrx"], $extentSource["lry"])),
                            implode(" ", array($extentSource["ulx"], $extentSource["lry"])),
                            implode(" ", array($extentSource["ulx"], $extentSource["uly"])),
                        );
                        $extentCoords = array_diff($extentCoords, array(" ", ""));
                        if ( count($extentCoords)!=4 ) continue;
                        $extentCoords = implode(", ", $extentCoords);
                        $extents[] = "select public.st_transform(st_geomfromtext ('POLYGON((".$extentCoords."))', ".$source["srid"]."), ".$srid.")::geometry as the_geom".
                                  ", '".str_replace("'", "''", $name)."'::text as name ".
                                  ", '".str_replace("'", "''", $gid)."'::text as gid ";
                          
                        $outlinecolor = $this->getRandomColor();
                        $classes[] = array( 
                            "name" => $name,
                            "title" => $name,
                            "expression" => "/^".preg_quote($gid, "/")."$/",
                            "styles" => array(
                                "outlinecolor" => $outlinecolor,
                                "outlinesize" => 2,
                                "outlinewidth" => 2,
                                "symbol" => "Carre",
                                "size" => "1"
                            )
                        );
                            
                    break;
                }
            } 
        }
        
        $acces_adress_admin = rtrim($this->getParameter('PRODIGE_URL_ADMINCARTO'), "/");;
        
        $admincartoUrl = $acces_adress_admin."/api/map/edit/".$mapId."/resume";
        
        $jsonObj = $this->curlAdminCartoJson($admincartoUrl);
        
        if($jsonObj &&  $jsonObj->success && $jsonObj->map && $jsonObj->map->mapId ){
            if  (!empty($extents) ){
                $edit_map_id =  $jsonObj->map->mapId;
                
                $modele = "local_data/TMP".$user_id."_".$map_file;
                $PRODIGE->executeQuery("update carmen.map set published_id=:map_id where map_id=:map_id", array("map_id"=>$edit_map_id, "map_file"=>$modele));
                
                $view_name = "v_layers_extent_".$user_id;
                $PRODIGE->executeQuery("drop view if exists public.".$view_name);
                $PRODIGE->executeQuery("create or replace view public.".$view_name." as select * from ( ".implode("\n union \n", $extents)." ) extents", $params, $paramtypes);
                $oLayer = array();
                $oLayer["mapId"] = $edit_map_id;
                $oLayer["layerType"] ="POSTGIS";
                $oLayer["msLayerConnectionType"] = "POSTGIS";
                $oLayer["msLayerType"] ="POLYGON";
                
                $oLayer["layerTitle"] = "Emprise des couches";
                $oLayer["layerTable"] = $view_name;
                $oLayer["msClasses"]  = array_values($classes);
            
                $layer_name = LibMap::neutralisationChaine($oLayer["layerTitle"]);
                
                $oLayer["layerName"] = $layer_name;
                $oLayer["msLayerName"] =  $layer_name;
                $oLayer["layerIdentifier"] =  $layer_name;
                
                $oLayer["layerAnalyseType"] = 2;
                $oLayer["msLayerPgSchema"] = $table_schema;
                $oLayer["msLayerPgTable"] = $view_name;
                $oLayer["msLayerPgGeometryField"] = "the_geom";
                $oLayer["msLayerPgIdField"] = "gid";
                $oLayer["msClassItem"] =  "gid";
                
                $oLayer["layerProjectionEpsg"] = $srid;
                $oLayer["msLayerPgProjection"] =  $srid;
                
                $oLayer["layerVisible"] = "true";
                $oLayer["layerOpacity"] = 99;
                $oLayer["layerLegend"] = true;
               
                
                //add Extent Layer
                $carmenAddLayerService = $acces_adress_admin."/api/layer/".$edit_map_id."/rest";
                $this->curlAdminCartoJson($carmenAddLayerService, array(), $oLayer);
                
                //publish the map
                $carmenAddLayerService = $acces_adress_admin."/api/map/publish/".$edit_map_id."/".$edit_map_id."";
                $this->curlAdminCartoJson($carmenAddLayerService, array(), array("mapModel"=>"false", "mapFile"=>$modele));
                $modele .= ".map";
            }
        } else {
            throw new \Exception("Echec de lecture du mapfile modèle");
        }
        return new JsonResponse(array("success"=>true, "mapfile"=>$modele));
    }
    
    protected function curlAdminCartoJson($url, array $getParameters=array(), array $postParameters=array(), $method="GET"){
        if ( !empty($getParameters) ){
            $url .= (strpos($url, "?")===false ? "?" : "&").http_build_query($getParameters);
        }
        
        $ticket = \Alk\Common\CasBundle\Security\Authentication\Provider\CasProvider::getPGT($url);
        $jsonResp = $this->curl($url, (!empty($postParameters) ? 'POST' : $method), ($ticket ? array('ticket'=>$ticket) : array()),  $postParameters);
        $jsonObj = json_decode($jsonResp);
        
        return $jsonObj;
    }
    
    protected function getRandomColor(){
        mt_srand ();
        $num = mt_rand ( 0, 0xffffff );
        return '#'. sprintf ( "%06x" , $num );
    }
}
