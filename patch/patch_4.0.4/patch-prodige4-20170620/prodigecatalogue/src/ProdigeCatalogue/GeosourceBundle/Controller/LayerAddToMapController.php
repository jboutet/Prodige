<?php

namespace ProdigeCatalogue\GeosourceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use ProdigeCatalogue\GeosourceBundle\Common\LibMap;

use Prodige\ProdigeBundle\Controller\BaseController;
use Prodige\ProdigeBundle\Controller\User;
use Prodige\ProdigeBundle\Common\Util;
use Prodige\ProdigeBundle\DAOProxy\DAO;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
* Page de consultation des couches présentes sur le serveur wms
* @author Alkante
*/

/**
 * @Route("/geosource")
 */
class LayerAddToMapController extends BaseController {
    
    
    /**
     * Add layer to temporary mapfile
     * @Secure(roles="ROLE_USER")
     * @Route("/layerAddToTempmap", name="catalogue_geosource_layerAddToTempmap", options={"expose"=true})
     */
    public function layerAddToTempmapAction(Request $request){
        
        LibMap::initContainer($this->container);
        
        $map = ($request->get("map", false) ? $request->get("map") : ($request->get("model", false) ? $request->get("model"): ""));
        $data = $request->get("DATA");
        $title = $request->get("title");
        $metadataId = $request->get("metadataId");
        $type_stokage = $request->get("TYPE_STOCKAGE");
        $mapUrl = urldecode($request->get("mapUrl"));
        $layerMapFile = urldecode($request->get("layerMapFile"));
        $path = $request->get("path", "layers/");

        
        
        $perimetre_table = $request->get("perimetre_table", "");
        $perimetre_info = $request->get("perimetre_info", "");
        
        $acces_adress_admin = rtrim($this->getParameter('PRODIGE_URL_ADMINCARTO'), "/");;

        $conn = $this->getProdigeConnection();
        
        $bReplaceData =false;
        //first load model mapfile
        
        if($map!=""){
            //1 create new temporary_map from model
            $carmenEditMapAction = $acces_adress_admin."/api/map/edit/".$map."/resume";
            
            $ticket = \Alk\Common\CasBundle\Security\Authentication\Provider\CasProvider::getPGT($carmenEditMapAction);
            $jsonResp = $this->curl($carmenEditMapAction, 'GET', array('ticket'=>$ticket),  array());
            $jsonObj = json_decode($jsonResp);
            if($jsonObj &&  $jsonObj->success && $jsonObj->map && $jsonObj->map->mapId  ){
                $map_id =  $jsonObj->map->mapId;
                //2 add layers
                //$bAddLayer = $this->addLayerToMap($map_id, $layer, $uuid);
                $LayerAddParams = array();
            
                //for case data is multiple
                $tabDatas = explode(";", $data);
                $tabTitles = explode(";", $title);
                $tabMetadataId = explode(";", $metadataId);
                
                //rights on territoire
                $tabPerimetreInfo = array();
                if($perimetre_table!="" && $perimetre_info!=""){
                    //pas d'outil contexte
                    $tabPerimetreInfo = explode(";", $perimetre_info);
                    //$oMap->setMetadata("TOOL_CONTEXT", "OFF");
                }
                $connection = $this->getCatalogueConnection("catalogue,public");
                
                //get correspondance id/uuid
                $tabUuid = $connection->fetchAll("select id, uuid from metadata where metadata.id in(:id)",array ("id" => $tabMetadataId), array("id"=>$connection::PARAM_INT_ARRAY));
                foreach($tabUuid as $key => $info){
                    $tabMatch[$info["id"]] =  $info["uuid"];
                }
                
                foreach($tabDatas as $key => $data){
                    $title = $tabTitles[$key];
                    if($data!=""){
                        $LayerAddParams["uuid"] = $tabMatch[$tabMetadataId[$key]];
                        switch($type_stokage){
                          case 0 :
                              $LayerAddParams["type"] = "RASTER_PRODIGE";
                              $LayerAddParams["layerTitle"] = $tabTitles[$key];
                              $LayerAddParams["layerTable"] = $data;
                              break;
                              
                          case 1 : //couches
                          case -4 : //vues   
                              $LayerAddParams["type"] = "VECTOR_PRODIGE";
                              $LayerAddParams["layerTitle"] = $title;
                              $LayerAddParams["layerTable"] = $data;
                              $LayerAddParams["perimetreTable"] = ($perimetre_table!="" ? $perimetre_table : '');
                              $LayerAddParams["perimetreInfo"] = (isset($tabPerimetreInfo[$key]) ? unserialize(base64_decode($tabPerimetreInfo[$key])) : '');
                              
                              if(isset($LayerAddParams["perimetreTable"]) && $LayerAddParams["perimetreTable"]!="" && $LayerAddParams["perimetreInfo"]!="" && $LayerAddParams["perimetreInfo"]!="1"){
                                  $strPerimetresIds = "";
                                  foreach($LayerAddParams["perimetreInfo"] as $id => $perimetre){
                                      $strPerimetresIds.= "'".$perimetre[0]."',";
                                  }
                                  $strPerimetresIds = substr($strPerimetresIds, 0, -1);
                                  //TODO check map projection ?
                                  $extent = $conn->fetchAll("select st_xmin(st_extent(the_geom)) as xmin, st_ymin(st_extent(the_geom)) as ymin, ".
                                                            "st_xmax(st_extent(the_geom)) as xmax, st_ymax(st_extent(the_geom)) as ymax from ".
                                                            $LayerAddParams["perimetreTable"]." where ".
                                                            $LayerAddParams["perimetreInfo"][0][2]." in (".$strPerimetresIds.") ");
                                  
                                  foreach ($extent as $val)
                                  {
                                      $extent = $val["xmin"]. ",". $val["ymin"].",".$val["xmax"].",".$val["ymax"];
                                      //complete URL with extent parmam
                                      $mapUrl.= (strpos($mapUrl, "?")===false ? "?extent=".$extent : "&extent=".$extent);
                                  }
                                  //take layeridentifier to modify data in mapfile
                                  $layer_msname = $conn->fetchColumn("select layer_msname from carmen.layer where map_id=:map_id and layer_metadata_uuid=:uuid ", array( "map_id" => $map_id, "uuid"=>$LayerAddParams["uuid"]));
                                  if ( $layer_msname!==false ){
                                      /*$carmenURL = $acces_adress_admin."/api/layer/".$map_id."/delete/".$layer_id;
                                      $ticket = \Alk\Common\CasBundle\Security\Authentication\Provider\CasProvider::getPGT($carmenURL);
                                      $jsonResp = $this->curl($carmenURL, 'GET', array('ticket'=>$ticket),  array());*/
                                      $bReplaceData = true;
                                  }
                                  
                              }
                              
                              break;
                
                          case 2:
                              $LayerAddParams["type"] = "WMS_PRODIGE";
                              $LayerAddParams["wmsUrl"] = $request->get("connection");
                              $LayerAddParams["wmsVersion"] = $request->get("version");
                              $LayerAddParams["wmsFormat"] = $request->get("format");
                              $LayerAddParams["wmsSrs"] = $request->get("srs");
                              
                              $LayerAddParams["layerTitle"] = $tabTitles[$key];
                              $LayerAddParams["wmsName"] = $data;
                              break;
                
                          case 3:
                              $LayerAddParams["type"] = "WFS_PRODIGE";
                              $LayerAddParams["wfsUrl"] = $request->get("connection");
                              $LayerAddParams["wfsVersion"] = $request->get("version");
                              $LayerAddParams["wfsFormat"] = $request->get("format");
                              $LayerAddParams["wfsSrs"] = $request->get("srs");
        
                              $LayerAddParams["layerTitle"] = $tabTitles[$key];
                              $LayerAddParams["wfsName"] = $data;
                
                              break;
                        }
                    }
                    //dynamically add Layer
                    if(!$bReplaceData){
                        LibMap::setConnection($conn);
                        
                        //TODO take $oLayer from default representation if exists
                        $oLayer = LibMap::AddLayerToMap($LayerAddParams,$this->getParameter("PRODIGE_URL_CATALOGUE") );
                        if(!$oLayer){
                            throw new \Exception("Echec lors du chargement de la donnée");
                        }
                        
                        $carmenAddLayerService = $this->getParameter('PRODIGE_URL_ADMINCARTO')."/api/layer/".$map_id."/rest";
                        
                        $ticket = \Alk\Common\CasBundle\Security\Authentication\Provider\CasProvider::getPGT($carmenAddLayerService);
                        $response = $this->curl($carmenAddLayerService, 'POST', array('ticket'=>$ticket),   $oLayer);
                        $responseObj = json_decode($response);
                    }
                }
                //3 change map parameters
                $prodigeConnection = $this->getProdigeConnection("carmen");
                //update map_wmsmetadata_uuid to null since it's only temporary
                $prodigeConnection->executeUpdate("update carmen.map set published_id =:map_id, map_title =:title, map_wmsmetadata_uuid=null where map_id=:map_id",
                    array("map_id" => $map_id,
                          "title"  => $tabTitles[0]));
            
                //4 save/publish Map
                $carmenSaveMapAction = $acces_adress_admin."/api/map/publish/".$map_id."/".$map_id;
                $ticket = \Alk\Common\CasBundle\Security\Authentication\Provider\CasProvider::getPGT($carmenSaveMapAction);
                $jsonResp = $this->curl($carmenSaveMapAction, 'POST',  array('ticket'=>$ticket),   array("mapModel"=>"false", "mapFile" => substr($path.$layerMapFile, 0, -4)));
                
                $jsonObj = json_decode($jsonResp);
                if($jsonObj && $jsonObj->success && $jsonObj->map){
                    
                    //perimetreInfo can be =1 if there is no territorial restriction
                    if($bReplaceData && isset($LayerAddParams["perimetreTable"]) && $LayerAddParams["perimetreTable"]!="" && $LayerAddParams["perimetreInfo"]!="" && $LayerAddParams["perimetreInfo"]!="1"){
                        $strPerimetresIds = "";
                        foreach($LayerAddParams["perimetreInfo"] as $id => $perimetre){
                            //$tabCouple[]=array($perimetre[0], $perimetre[2]);
                            $strPerimetresIds.= "'".$perimetre[0]."',";
                        }
                        $strPerimetresIds = substr($strPerimetresIds, 0, -1);
                        $srid= PRO_IMPORT_EPSG;
                        $desc = $prodigeConnection->fetchAll("SELECT f_table_schema, f_geometry_column, srid from public.geometry_columns where f_table_name = :table",
                            array("table" => strtolower($LayerAddParams["layerTable"])));
                        foreach($desc as $table){
                            try {
                                $geomInfo = $prodigeConnection->fetchAll(
                                    "select public.st_srid(".$table["f_geometry_column"].") as srid, public.geometrytype(".$table["f_geometry_column"].") as type
                                                from ".$table["f_table_schema"].".".$LayerAddParams["layerTable"]." limit 1"
                                );
                                if (!empty($geomInfo) )$srid= $geomInfo[0]["srid"];
                                
                            } catch(\Exception $exception){};
                        }
                        
                        $data = "the_geom from (select a.* from ".$LayerAddParams["layerTable"]." a INNER JOIN ".$LayerAddParams["perimetreTable"].
                        " b ON (a.the_geom && b.the_geom AND st_intersects(a.the_geom, b.the_geom)) where b.".
                        $LayerAddParams["perimetreInfo"][0][2]." in (".$strPerimetresIds.")) as foo using unique gid using srid=".$srid;
                        $file = PRO_MAPFILE_PATH.$path.$layerMapFile;
                    
                        //try{
                            $oMap = @ms_newMapObj($file);
                            $oLayer = $oMap->getLayerByName ($layer_msname);
                            $oLayer->set("data", $data);
                            $oMap->save($file);
                        /*}catch(\Exception $exception){
                            
                        }*/
                        
                    
                    }
                    //redirect to map
                    return $this->redirect($mapUrl);
                }
                throw new \Exception("Echec lors du chargement du modèle");
            }else{
                throw new \Exception("Echec lors du chargement du modèle");
            }
        } else { //sélection d'un layer
            return $this->chooseModel($request);
        }
        
    }
    
    
    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/layerAddToMap/{uuid}", name="catalogue_geosource_layerAddToMap", options={"expose"=true})
     */
    public function layerAddToMapAction(Request $request) {

       //loop on layers for ressource
        $CATALOGUE = $this->getCatalogueConnection("catalogue,public");
        $PRODIGE = $this->getProdigeConnection('carmen');
        LibMap::initContainer($this->container);
        $acces_adress_admin = rtrim($this->getParameter('PRODIGE_URL_ADMINCARTO'), "/");;
        $model = $request->get("model");
        
        $uuid = $request->get("uuid");
        $mapUuid = $request->get("mapUuid");
        $prefixMapfile = $request->get("prefixMapfile");
        
        if($prefixMapfile==""){
            $prefixMapfile = "layers/";
        }
        //mapUuid can be disconnected from catalogue uuid, for WMS representation or carteperso
        if($mapUuid==""){
            $mapUuid = $uuid;
        }
        
        //first detect if map exists in carmen database
        $tabMaps = $PRODIGE->fetchAll("select map_id, map_file from carmen.map where map_wmsmetadata_uuid = :uuid and published_id is null", array ("uuid" => $mapUuid));
        $map_id = "";
        foreach ($tabMaps as $val)
        {
            $map_id = $val['map_id'];
            $map_file = PRO_MAPFILE_PATH."/".$val['map_file'].".map";
            
            $layersMap = array();
            $mapObj = ms_newMapObj($map_file);
            for($i=0; $i<$mapObj->numlayers; $i++){
                $oLayer = $mapObj->getLayer($i);
                if ( $oLayer->connectiontype == MS_POSTGIS ){
                    $matches = array();
                    preg_match("!from\s+(\w+\.)?\b(\w+)\b!is", $oLayer->data, $matches);
                    $layersMap[$oLayer->name] = $matches[2];
                } else {
                    $layersMap[$oLayer->name] = $oLayer->tileindex ?: $oLayer->data;
                }
            }
            
            $carmenURL = $acces_adress_admin."/api/map/edit/".$map_id."/resume";
            $ticket = \Alk\Common\CasBundle\Security\Authentication\Provider\CasProvider::getPGT($carmenURL);
            $jsonResp = $this->curl($carmenURL, 'GET', array('ticket'=>$ticket),  array());
            
            $jsonObj = json_decode($jsonResp);
            $edit_map_id = null;
            $mapModel = false;
            $mapFile = $val['map_file'];
            if($jsonObj &&  $jsonObj->success && $jsonObj->map && $jsonObj->map->mapId  ){
                $edit_map_id =  $jsonObj->map->mapId;
                $mapModel = $jsonObj->map->mapModel;
            }
            if ( !$edit_map_id ) continue;
            
            
            $layers = $CATALOGUE->executeQuery("select  xpath('//gmd:identificationInfo/gmd:MD_DataIdentification/gmd:citation/gmd:CI_Citation/gmd:title/gco:CharacterString/text()'::text, ".
                                               "('<?xml version=\"1.0\" encoding=\"utf-8\"?>'::text || metadata.data)::xml, ".
                                                "ARRAY[ARRAY['gmd'::text, 'http://www.isotc211.org/2005/gmd'::text], ". 
                                                "ARRAY['gco'::text, 'http://www.isotc211.org/2005/gco'::text]])::text AS \"metadataTitle\", ". 
                                                "couchd_nom as title,  couchd_nom, couchd_type_stockage, couchd_emplacement_stockage ".
                                                " FROM couche_donnees ".
                                                " INNER JOIN fiche_metadonnees ON couche_donnees.pk_couche_donnees = fiche_metadonnees.fmeta_fk_couche_donnees".
                                                " INNER JOIN metadata ON fiche_metadonnees.fmeta_id::text = metadata.id::text where metadata.uuid =:uuid", 
                                                array("uuid" => $uuid));
            $layersRename = array();
            //Recherches des couches de la métadonnée à AJOUTER ou à METTRE À JOUR le titre
            foreach($layers as $layer){
                $couchd_type_stockage = $layer["couchd_type_stockage"];
                $couchd_emplacement_stockage = $layer["couchd_emplacement_stockage"];
                if ( $couchd_type_stockage==0 ){//raster
                    $alls = "%".implode("%", $layersMap)."%";
                    $matches = array();
                    if ( preg_match("!%([^%]*?".$couchd_emplacement_stockage.")%!", $alls, $matches) ){
                        if ( ($layer_msname = array_search($matches[1], $layersMap))!==false ){
                            $layer_id = $PRODIGE->fetchColumn("update carmen.layer set layer_title=:layer_title where map_id=:map_id and layer_msname=:layer_msname", array("map_id" => $edit_map_id, "layer_msname"=>$layer_msname, "layer_title"=>$layer["couchd_nom"]));
                            unset($layersMap[$layer_msname]);
                            continue;
                        }
                    }
                    $this->addLayerToMap($edit_map_id, $layer, $uuid);
                } else if ( $couchd_type_stockage==1 || $couchd_type_stockage==-4 ){//vector
                    if ( ($layer_msname = array_search($couchd_emplacement_stockage, $layersMap))!==false ){
                        $layer_id = $PRODIGE->fetchColumn("update carmen.layer set layer_title=:layer_title where map_id=:map_id and layer_msname=:layer_msname", array("map_id" => $edit_map_id, "layer_msname"=>$layer_msname, "layer_title"=>$layer["couchd_nom"]));
                        unset($layersMap[$layer_msname]);
                    }
                    else {
                        $this->addLayerToMap($edit_map_id, $layer, $uuid);
                    }
                } else {// what to do ? 
                    $layersMap = array();
                    break;
                }
            }
            
            //SUPPRESSION des anciennes couches de la métadonnée
            foreach($layersMap as $layer_msname=>$layer_data){
                $layer_id = $PRODIGE->fetchColumn("select layer_id from carmen.layer where map_id=:map_id and layer_metadata_uuid=:uuid and layer_msname=:layer_msname", array("uuid" => $uuid, "map_id" => $edit_map_id, "layer_msname"=>$layer_msname));
        
                if ( $layer_id!==false ){
                    $carmenURL = $acces_adress_admin."/api/layer/".$edit_map_id."/delete/".$layer_id;
                    $ticket = \Alk\Common\CasBundle\Security\Authentication\Provider\CasProvider::getPGT($carmenURL);
                    $jsonResp = $this->curl($carmenURL, 'GET', array('ticket'=>$ticket),  array());
                }
            }
            
            //4 save/publish Map
            $carmenURL = $acces_adress_admin."/api/map/publish/".$map_id."/".$edit_map_id;
            $ticket = \Alk\Common\CasBundle\Security\Authentication\Provider\CasProvider::getPGT($carmenURL);
            $posts = array("mapModel"=> $mapModel,
                           "mapFile" => $mapFile);
            $jsonResp = $this->curl($carmenURL, 'POST', array('ticket'=>$ticket), $posts);
            $jsonObj = json_decode($jsonResp);

            if($jsonObj && $jsonObj->success && $jsonObj->map){
                //redirect to map identifier
                return $this->redirect($acces_adress_admin."/edit_map/".$mapUuid);
            }
        }
        
        
        #############################################################################
        //add layer to model and redirect to map created
        if($model!=""){

            $tabLayers = $CATALOGUE->fetchAll("select  xpath('//gmd:identificationInfo/gmd:MD_DataIdentification/gmd:citation/gmd:CI_Citation/gmd:title/gco:CharacterString/text()'::text, ".
                                               "('<?xml version=\"1.0\" encoding=\"utf-8\"?>'::text || metadata.data)::xml, ".
                                                "ARRAY[ARRAY['gmd'::text, 'http://www.isotc211.org/2005/gmd'::text], ". 
                                                "ARRAY['gco'::text, 'http://www.isotc211.org/2005/gco'::text]])::text AS \"metadataTitle\", ". 
                                                " couchd_nom as title, couchd_type_stockage, couchd_emplacement_stockage FROM couche_donnees ".
                                                " INNER JOIN fiche_metadonnees ON couche_donnees.pk_couche_donnees = fiche_metadonnees.fmeta_fk_couche_donnees".
                                                " INNER JOIN metadata ON fiche_metadonnees.fmeta_id::text = metadata.id::text where metadata.uuid =:uuid",array ("uuid" => $uuid));


            if(!empty($tabLayers)){
                //1 create new temporary_map from model
                $carmenEditMapAction = $acces_adress_admin."/api/map/edit/".$model."/resume";
                $ticket = \Alk\Common\CasBundle\Security\Authentication\Provider\CasProvider::getPGT($carmenEditMapAction);
                $jsonResp = $this->curl($carmenEditMapAction, 'GET', array('ticket'=>$ticket),  array());
                
                $jsonObj = json_decode($jsonResp);
                if($jsonObj &&  $jsonObj->success && $jsonObj->map && $jsonObj->map->mapId  ){
                    $map_id =  $jsonObj->map->mapId;
                    
                    //2 add layers
                    foreach ($tabLayers as $layer)
                    {
                        $couchd_emplacement_stockage = $layer['couchd_emplacement_stockage'];
                        $metadataTitle = str_replace(array("{", "\"", "}"), "", $layer['metadataTitle']);
                        $this->addLayerToMap($map_id, $layer, $uuid);
                    }
                    
                    //3 change map parameters
                    $PRODIGE->executeUpdate("update carmen.map set published_id =:map_id, map_title =:title,  map_wmsmetadata_uuid=:uuid where map_id=:map_id",
                        array("map_id" => $map_id,
                              "uuid"   => $mapUuid,
                              "title"  => $metadataTitle));
                    
                    //4 save/publish Map
                    $carmenSaveMapAction = $acces_adress_admin."/api/map/publish/".$map_id."/".$map_id;
                    $ticket = \Alk\Common\CasBundle\Security\Authentication\Provider\CasProvider::getPGT($carmenSaveMapAction);
                    $posts = array("mapModel"=>"false",
                                   "mapFile" => $prefixMapfile.pathinfo($couchd_emplacement_stockage, PATHINFO_FILENAME ));
                    

                    $jsonResp = $this->curl($carmenSaveMapAction, 'POST', array('ticket'=>$ticket), $posts);
                    $jsonObj = json_decode($jsonResp);
                    if($jsonObj && $jsonObj->success && $jsonObj->map){
                        //redirect to map
                        return $this->redirect($acces_adress_admin."/edit_map/".$mapUuid);
                    }
                }else{
                    throw new \Exception("Echec lors du chargement du modèle");
                }
                
            }else{
                throw new \Exception("Echec lors du chargement des données dans le modèle");
            }
            
        }else{
            //choose model
            return $this->chooseModel($request);
        }
    }
    
    
    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/covisualisation", name="catalogue_geosource_convisualisation", options={"expose"=true})
     */
    public function covisualisationAction(Request $request) {
        
        $CATALOGUE = $this->getCatalogueConnection('catalogue,public');
        $PRODIGE = $this->getProdigeConnection('carmen,public');
        
        LibMap::initContainer($this->container);
        LibMap::setConnection($PRODIGE);
        
        $acces_adress_admin = rtrim($this->getParameter('PRODIGE_URL_ADMINCARTO'), "/");
        $acces_adress_front = rtrim($this->getParameter('PRODIGE_URL_FRONTCARTO'), "/");
        
        $user = User::GetUser();
        
        if(!$user->HasTraitement('NAVIGATION')) {
            \Prodige\ProdigeBundle\Common\SecurityExceptions::throwAccessDeniedException();
            exit(0);
        }
    
        $tabError = array();
    
        // récupération des paramètres postés
        $data     = $request->request->get("data", array());
        $mapName  = $request->request->get("mapName", "Co-visualisation du panier");
        $mapModel = $request->request->get("model", "");
        $mapFileTarget = "TMP_".time().".map";
        $mapUrl = $acces_adress_front."/1/layers/".$mapFileTarget;
        
        if($mapModel == "") { //interface de choix de modèle
            return $this->chooseModel($request);
        } else { //ajout des layers au modèle
            //1 create new temporary_map from model
            $carmenEditMapAction = $acces_adress_admin."/api/map/edit/".$mapModel."/resume";
            $ticket = \Alk\Common\CasBundle\Security\Authentication\Provider\CasProvider::getPGT($carmenEditMapAction);
            $jsonResp = $this->curl($carmenEditMapAction, 'GET', array('ticket'=>$ticket),  array());
            
            $jsonObj = json_decode($jsonResp);
            
            if($jsonObj &&  $jsonObj->success && $jsonObj->map && $jsonObj->map->mapId  ){
                $map_id =  $jsonObj->map->mapId;

                if(count($data) > 0) {
        
                    $dao = new DAO($CATALOGUE, 'catalogue');
                    // traitement des données
                    // chaque donnée est traitée et ajoutée en tant que couche au mapfile target
                    for($i=0; $i<count($data); $i++ ) {
                        
                        $metadata_id = $data[$i]['id'];
                        $metadata_uuid = $CATALOGUE->fetchColumn("select uuid from public.metadata where id=:id", array("id"=>$metadata_id));
        
                        // donnée non moissonnée
                        if($data[$i]["isHarvested"] == "n") {
                            
                            
                            $query = 'SELECT couchd_emplacement_stockage, couchd_nom, couchd_type_stockage, uuid'.
                                ' FROM couche_donnees '.
                                ' LEFT JOIN acces_serveur ON couche_donnees.couchd_fk_acces_server = acces_serveur.pk_acces_serveur '.
                                ' LEFT JOIN fiche_metadonnees ON fiche_metadonnees.fmeta_fk_couche_donnees  = couche_donnees.pk_couche_donnees '.
                                ' INNER JOIN public.metadata ON fiche_metadonnees.fmeta_id::text = metadata.id::text '.
                                
                                ' where fmeta_id = ?';
                            $rs = $dao->BuildResultSet($query, array($metadata_id));
       
                            // métadonnée existante en local
                            if($rs->GetNbRows() > 0) {
                                for($rs->First(); !$rs->EOF(); $rs->Next()){
                                    $LayerAddParams = array();
                                    $couchd_emplacement_stockage = $rs->Read(0);
                                    $layerTitle = html_entity_decode($rs->read(1), ENT_QUOTES, 'UTF-8');
                                    $couchd_type_stockage = $rs->Read(2);
                                    $uuid = $rs->Read(3);
                                    
                                    switch($couchd_type_stockage ) {
                                      case 1 ://table vecteur
                                      case -4 ://vue de jointure
                                          //vector prodige
                                          $LayerAddParams["type"] = "VECTOR_PRODIGE";
                                          $layerMapFile = $couchd_emplacement_stockage;
                                          break;
                                          //raster prodige
                                      case 0 :
                                          $LayerAddParams["type"] = "RASTER_PRODIGE";
                                          //if raster, take $couchd_emplacement_stockage and delete extension
                                          $layerMapFile = substr($couchd_emplacement_stockage, 0, strrpos($couchd_emplacement_stockage, "."));
                                          //get file name
                                          $layerMapFile = substr($layerMapFile, strrpos($layerMapFile, "/") + 1, strlen($layerMapFile));
                                          break;
                                      default :
                                          $layerMapFile = $couchd_emplacement_stockage;
                                          break;
                                    }
                                    $layerMapFile .= ".map";
            
                                    //vérification du caractère de visualisation libre de la donnée
                                    $query = 'SELECT couchd_wfs, couchd_wms FROM couche_donnees WHERE pk_couche_donnees in (select fmeta_fk_couche_donnees from fiche_metadonnees where  FMETA_ID =?)';
                                    $rsVisu = $dao->BuildResultSet($query, array($metadata_id));
                                    $rsVisu->First();
                                    $bFreeVisu = $rsVisu->Read(0) || $rsVisu->Read(1);
                                    //vérification des droits sur un des domaines de la donnée
                                    $bAllow = false;
                                    $query = "SELECT DOM_NOM, SSDOM_NOM, PK_COUCHE_DONNEES  FROM COUCHE_SDOM WHERE FMETA_ID =?";
                                    $rsRight = $dao->BuildResultSet($query, array($metadata_id));
            
                                    for($rsRight->First(); !$rsRight->EOF(); $rsRight->Next()) {
                                        $domaine = $rsRight->read(0);
                                        $sous_domaine = $rsRight->read(1);
                                        $objetCouche = $rsRight->read(2);
                                        //vérification de l'autorisation sur le domaine et sur l'objet
                                        $bAllow = $user->HasTraitement('NAVIGATION', html_entity_decode($domaine, ENT_QUOTES, 'UTF-8'), html_entity_decode($sous_domaine, ENT_QUOTES, 'UTF-8'), PRO_TRT_TYPE_CONSULTATION, $objetCouche, PRO_OBJET_TYPE_COUCHE);
                                        if($bAllow) {
                                            break;
                                        }
                                    }
            
                                    //vérification des restricitions territoriales
                                    $tabTerr = $user->GetTraitementTerritoire('NAVIGATION', $objetCouche);
                                    if($tabTerr === false) {
                                        $bAllow = false;
                                    }
            
                                    if($bFreeVisu || $bAllow) {
                                        

                                        //tableau des données restreintes territorialement avec leurs territoires
                                        
                                        // Recherche et ajout à la carte de la représentation par défaut de la métadonnée sauf pour les restrictions territoriales
                                        if(is_array($tabTerr) && count($tabTerr) > 0) {
                                            //ajout des paramètres de perimetre
                                            $LayerAddParams["perimetreTable"] = 'prodige_perimetre';
                                            $LayerAddParams["perimetreInfo"] = $tabTerr;
                                            
                                        }else{
                                            $stmt = $PRODIGE->executeQuery("select layer.layer_id, layer.map_id from carmen.layer inner join carmen.map on (map.map_id=layer.map_id and map.map_wmsmetadata_uuid=layer.layer_metadata_uuid and map.published_id is null) where layer_metadata_uuid=:uuid", array("uuid"=>$metadata_uuid));
                                            if ( $stmt->rowCount()>0 ){
                                                $from_map_id = null;
                                                $ar_layer_ids = array();
                                                foreach($stmt as $row){
                                                    $ar_layer_ids[] = $row["layer_id"];
                                                    $from_map_id = $row["map_id"];
                                                }
                                                //duplicate layer to map using carmen service
                                                $carmenAddLayerService = $this->getParameter('PRODIGE_URL_ADMINCARTO')."/api/map/copyLayersAndFieldsTables/".$map_id."/".$from_map_id."/add";
                                                $ticket = \Alk\Common\CasBundle\Security\Authentication\Provider\CasProvider::getPGT($carmenAddLayerService);
                                                $this->curl($carmenAddLayerService, 'POST', array("ticket"=>$ticket),  compact("ar_layer_ids"));
                                            
                                                continue;
                                            }
                                        }
                                        
                                        

                                        $LayerAddParams["layerTitle"] = $layerTitle." [".basename($couchd_emplacement_stockage)."]";
                                        $LayerAddParams["layerTable"] = $couchd_emplacement_stockage;
                                        $LayerAddParams["uuid"] = $uuid;
                                
                                        
                                        $oLayer = LibMap::AddLayerToMap($LayerAddParams,$this->getParameter("PRODIGE_URL_CATALOGUE") );
                                        if(!$oLayer){
                                            continue;
                                            throw new \Exception("problem loading layer in model");
                                        }
                                        //add layer to map using carmen service
                                        $carmenAddLayerService = $this->getParameter('PRODIGE_URL_ADMINCARTO')."/api/layer/".$map_id."/rest";
                
                                        $ticket = \Alk\Common\CasBundle\Security\Authentication\Provider\CasProvider::getPGT($carmenAddLayerService);
                                        $this->curl($carmenAddLayerService, 'POST', array("ticket"=>$ticket),  $oLayer);
            
                                    } else {
                                        $tabError[] = "Vous ne disposez pas des droits suffisants sur la donnée ".$layerTitle;
                                    }
                                }
                            } else {
                                // métadonnée non existante en local
                                $tabError[] = "La donnée ".$layerTitle." n'existe pas";
                            }
                        } elseif($data[$i]["isHarvested"] == "y") { // donnée moissonnée
                            $LayerAddParams = array();
                            $protocol   = (isset($data[$i]["protocol"]) && $data[$i]["protocol"] != ""     ? $data[$i]["protocol"]   : "");
                            $connection = (isset($data[$i]["connection"]) && $data[$i]["connection"] != "" ? $data[$i]["connection"] : "");
                            $layer      = (isset($data[$i]["layer"]) && $data[$i]["layer"] != ""           ? $data[$i]["layer"]      : "");
        
                            // cas récupération des informations dans la métadonnée de service
                            if($protocol == "" || $connection == "" || $layer == "") {
                                //Fonctionnement par métadonnée de service associée à la métadonnée de donnée
                                $uuidData = $data[$i]["uuidData"];
                                $uuidService = $data[$i]["uuidService"];
                                $tabUUidService = explode("|", $uuidService);
                                $struuidService = "";
                                foreach($tabUUidService as $uuid) {
                                    if($uuid != "") {
                                        $struuidService.="'".$uuid."',";
                                    }
                                }
                                //service de recherche des données WMS/WFS associées au service
                                $struuidService = substr($struuidService, 0, -1);
                                $strSql = "select data from public.metadata where metadata.uuid in (?)";
                                $rs2 = $dao->BuildResultSet($strSql, array($struuidService));
                                $tablayer = array();
                                $tabConnection = array();
                                $tabprotocol = array();
                                for($rs2->First(); !$rs2->EOF(); $rs2->Next()) {
                                    $dataXML = $rs2->Read(0);
                                    $version  = "1.0";
                                    $encoding = "UTF-8";
                                    $metadata_doc = new \DOMDocument($version, $encoding);
                                    //chargement de la métadonnée
                                    $entete = "<?xml version=\"".$version."\" encoding=\"".$encoding."\"?>\n";
                                    $metadata_data = $entete.$dataXML;
                                    $metadata_data = str_replace("&", "&amp;", $metadata_data);
                                    //echo $metadata_data;
                                    $metadata_doc->loadXML(($metadata_data));
                                    $xpath = new \DOMXPath($metadata_doc);
                                    $linkedUUid = $xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/srv:coupledResource/srv:SV_CoupledResource/srv:identifier/gco:CharacterString");
                                    foreach($linkedUUid as $node) {
                                        if($node->nodeValue == $uuidData) {
                                            $layerNode = $node->parentNode->parentNode->getElementsByTagName("ScopedName");
                                            $tablayer[] = $layerNode->item(0)->nodeValue;
                                            break;
                                        }
                                    }
                                    $linkedUrl = $xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/srv:containsOperations/srv:SV_OperationMetadata/srv:operationName/gco:CharacterString");
                                    foreach($linkedUrl as $node) {
                                        if($node->nodeValue == "DescribeFeatureType" || $node->nodeValue == "GetMap" ) {
                                            $UrlService = $xpath->query("srv:connectPoint/gmd:CI_OnlineResource/gmd:linkage/gmd:URL", $node->parentNode->parentNode);
                                            $tabConnection[] = $UrlService->item(0)->nodeValue;
                                            $tabprotocol[] = $node->nodeValue;
                                            break;
                                        }
                                    }
        
                                }
                                if(count($tablayer) == 1) {
                                    $layer = $tablayer[0];
                                    $connection = $tabConnection[0];
                                    $protocol = $tabprotocol[0];
                                } else {
                                    for($j=0; $j<count($tablayer); $j++) {
                                        $layer = $tablayer[$j];
                                        $connection = $tabConnection[$j];
                                        $protocol = $tabprotocol[$j];
                                        //priorité au service WMS associé vis à vis du service WFS, sinon on prend le dernier service associé
                                        if($tabprotocol[$j] == "GetMap") {
                                            break;
                                        }
                                    }
                                }
                            }
                            // fin cas récupération des informations dans la métadonnée de service
        

                            global $PRO_IMPORT_EPSG;
                            $layerTitle = ($data[$i]["layerTitle"]);
                            $LayerAddParams["layerTitle"] = $layerTitle;
                            $LayerAddParams["uuid"] = $metadata_uuid;
                            if($protocol == "GetMap") {
                                $LayerAddParams["wmsVersion"] = "1.1.1";
                                $LayerAddParams["type"] = "WMS_PRODIGE";
                                $LayerAddParams["wmsUrl"] = $connection;
                                $LayerAddParams["wmsFormat"] = "image/png";
                                $LayerAddParams["wmsSrs"] = $PRO_IMPORT_EPSG;
                                $LayerAddParams["wmsName"] = $layer;
                                
                            } else {
                                $LayerAddParams["wfsVersion"] =  "1.0.0";
                                $LayerAddParams["type"] = "WFS_PRODIGE";
                                $LayerAddParams["wfsUrl"] = $connection;
                                $LayerAddParams["wfsFormat"] = "image/png";
                                $LayerAddParams["wfsSrs"] = $PRO_IMPORT_EPSG;
                                $LayerAddParams["wfsName"] = $layer;
                            }
                            
                            
                            $oLayer = LibMap::AddLayerToMap($LayerAddParams,$this->getParameter("PRODIGE_URL_CATALOGUE") );
                            if(!$oLayer){
                                continue;
                                throw new \Exception("problem loading layer in model");
                            }
                            //add layer to map using carmen service
                            $carmenAddLayerService = $this->getParameter('PRODIGE_URL_ADMINCARTO')."/api/layer/".$map_id."/rest";
                            
                            $ticket = \Alk\Common\CasBundle\Security\Authentication\Provider\CasProvider::getPGT($carmenAddLayerService);
                            $this->curl($carmenAddLayerService, 'POST', array("ticket"=>$ticket),  $oLayer);

                        } else { // moissonnage non défini
                            $tabError[] = "Impossible de déterminer le type de moissonnage pour la donnée ".$layerTitle;
                        }
       
                    }
                    
                    //3 change map parameters
                    $prodigeConnection = $this->getProdigeConnection("carmen");
                    $prodigeConnection->executeUpdate("update carmen.map set published_id =:map_id, map_title =:title where map_id=:map_id",
                        array("map_id" => $map_id,
                              "title"  => "Covisualisation des données du panier"));
                    
                    //4 save/publish Map
                    $carmenSaveMapAction = $acces_adress_admin."/api/map/publish/".$map_id."/".$map_id;
                    $ticket = \Alk\Common\CasBundle\Security\Authentication\Provider\CasProvider::getPGT($carmenSaveMapAction);
                    $jsonResp = $this->curl($carmenSaveMapAction, 'POST', array("ticket"=> $ticket),   array("mapModel"=>"false", "mapFile" => substr("layers/".$mapFileTarget, 0, -4)));
                    $jsonObj = json_decode($jsonResp);
                    if($jsonObj && $jsonObj->success && $jsonObj->map){
                        //redirect to map
                        return $this->redirect($mapUrl);
                    }
                } else {
                    // erreur de paramétrage
                    $tabError[] = "Paramètres manquants ou mal construits";
                }
            }
    
            // affichage des erreurs
            if(count($tabError) > 0) {
                $html = "";
                $html .= "<html>";
                $html .= " <head>";
                $html .= "   <title>Erreurs : ".$mapName."</title>";
                $html .= " </head>";
                $html .= " <body>";
                $html .= "   <h2>Les erreurs suivantes sont apparues lors de la tentative de construction de la carte</h2>";
                $html .= "   <ul>";
                for($i=0; $i<count($tabError); $i++) {
                    $html .= "     <li>".$tabError[$i]."</li>";
                }
                $html .= "   </ul>";
                $html .= "   <div align=\"center\">";
                if($mapUrl) {
                    $html .= "<button onclick=\"document.location.href='".$mapUrl."'\">Continuer</button>&nbsp;";
                }
                $html .= "<button onclick=\"window.close()\">Fermer</button></div>";
                $html .= " </body>";
                $html .= "</html>";
                return new Response($html);
            }
            else {
                // redirige vers la visualisation
                return $this->redirect($mapUrl);
            }
        }
    }
    
    
    /**
     * choose model and POST form with request values to itself with additionnal param model
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function chooseModel(Request $request){
        
        $strCss1 = '<link rel="stylesheet" type="text/css" href="/bundles/geosource/css/Administration.css">';
        $strCss2 = '<link rel="stylesheet" type="text/css" href="/Scripts/ext3.0/resources/css/ext-all.css">';
        
        $strJs1 = '<script  src="/Scripts/ext3.0/adapter/ext/ext-base.js"> </script>';
        $strJs2 = '<script  src="/Scripts/ext3.0/ext-all.js"> </script>';
        $strJs3 = '<script  src="/Scripts/ext3.0/miframe-debug.js"> </script>';
        $strJs4 = '<script  src="/Scripts/extjs_overload.js"> </script>';
        $strJs4 .= '<script  src="/bundles/geosource/js/interface.js"> </script>';
        
        $strHtml = "
                    <!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">
                    <html>
                        <head>
                        <title> </title>
                        <META HTTP-EQUIV=\"Content-Type\" CONTENT=\"text/html; charset=UTF-8\">
                        <!-- IE specific : forcing IE9 Document model because ext 3.0 incompatibility with IE10 -->
                        <META HTTP-EQUIV=\"X-UA-Compatible\" CONTENT=\"IE=9\"/>
        
                        <!-- <link rel='icon' href='../public/images/favicon.ico' type='image/x-icon'/>-->
                        <!-- <link rel='stylesheet' type='text/css' href='/bundles/geosource/css/Administration.css'/>-->
                        ".$strCss1."
                        <!-- ** CSS ** -->
                        <!-- base library -->
                        <!-- <link rel='stylesheet' type='text/css' href='/Scripts/ext3.0/resources/css/ext-all.css' /> -->
                        ".$strCss2."
                        <!-- overrides to base library -->
        
                        <!-- ** Javascript ** -->
                        <!-- ExtJS library: base/adapter -->
                        <!-- <script type='text/javascript' src='/Scripts/ext3.0/adapter/ext/ext-base.js'></script>
                        <script type='text/javascript' src='/Scripts/ext3.0/ext-all.js'></script>  -->
                        ".$strJs1.$strJs2."
                        <!-- ExtJS library: all widgets -->
                        ".$strJs3.$strJs4."
                        </head>
                        <body>
                            <div class=\"ChoixModele\">
                                <p>La carte est ouverte pour la premi&egrave;re fois.
                                <br> Il est possible de choisir un mod&egrave;le pour fond de plan.</p><br>
                                <form  id=\"choix_modele\" name=\"choix_modele\" action=\"".$request->getRequestUri()."\" method=\"POST\">
                                    <input type='hidden' name=\"model\" id=\"model\" value=\"\">
                        ";
                        
                        foreach($request->request->all() as $key => $value){
                            if(!(is_array($value))){
                                $strHtml .="<input type='hidden' name=\"".$key."\" value=\"".$value."\">";
                            }
                            //$strHtml .="<input type='hidden' name=\"".$key."\" value=\"".$value."\">";
                        }
                        //TODO treat specific data case as generic  array
                        if(isset($request->request->all()["data"])) {
                            foreach($request->request->all()["data"] as $key => $value){
                                foreach($value as $key2 => $value2) {
                                    $strHtml .="<input type='hidden' name='data[".$key."][".$key2."]' value='".$value2."'>";
                                }
                            }
                        }
                        
                        foreach($request->query->all() as $key => $value){
                            $strHtml .="<input type='hidden' name=\"".$key."\" value=\"".$value."\">";
                        }
                        $strHtml .="<input type=\"button\" value=\"Parcourir les cartes modèles...\" onclick=\"LoadModelMaps('".$request->getSchemeAndHttpHost().$this->generateUrl('catalogue_geosource_tSAjoutDeCarte_MapModelList')."')\">
                                </form>
                            </div>
                        </body>
                    </html>
                    ";
        return new Response($strHtml);
    }
    
    /**
     * call Carmen service adding layer to model map
     * @param integer $map_id map identifier
     * @param array $layer layer parameters
     * @param uuid identifier
     */
    protected function addLayerToMap($map_id, $layer, $uuid){

        //create layer obj
        $oLayer =  array();
        
        $conn = $this->getProdigeConnection();
        $LayerAddParams = array();
        $LayerAddParams["layerTitle"] = str_replace(array("{", "\"", "}"), "", $layer['title']); 
        $LayerAddParams["layerTable"] = $layer['couchd_emplacement_stockage'];
        $LayerAddParams["type"] = ($layer["couchd_type_stockage"]==1 || $layer["couchd_type_stockage"]==-4 ? "VECTOR_PRODIGE" : "RASTER_PRODIGE");
        $LayerAddParams["uuid"] = $uuid;
        //dynamically add Layer
        LibMap::setConnection($conn);
        
        //TODO take $oLayer from default representation if exists
        $oLayer = LibMap::AddLayerToMap($LayerAddParams,$this->getParameter("PRODIGE_URL_CATALOGUE") );
        if(!$oLayer){
            throw new \Exception("problem loading layer in model");
        }
        //post to Carmen
        $carmenAddLayerService = $this->getParameter('PRODIGE_URL_ADMINCARTO')."/api/layer/".$map_id."/rest";
        $ticket = \Alk\Common\CasBundle\Security\Authentication\Provider\CasProvider::getPGT($carmenAddLayerService);
        
        $response = $this->curl($carmenAddLayerService, 'POST', array("ticket"=>$ticket),  $oLayer);
        
        $responseObj = json_decode($response);
        return $responseObj->status;
    }


    /**
     * écrit un message
     * @param $strMsg
     * @return nothing
     */
    protected function displayMsg($strMsg) {

        $strHtml = "
                    <!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">
                    <html>
                    <head>
                        <title>Représentation par défaut</title>
                        <META HTTP-EQUIV=\"Content-Type\" CONTENT=\"text/html; charset=UTF-8\">
                        <link rel='stylesheet' type='text/css' href=\"../public/css/administration_carto.php\">
                    </head>
                    <body>";
        $strHtml .= "<div class=\"errormsg\">
                    ".$strMsg.
                    "</div></body></html>";
        return $strHtml;
    }
    
    
    
    
}
