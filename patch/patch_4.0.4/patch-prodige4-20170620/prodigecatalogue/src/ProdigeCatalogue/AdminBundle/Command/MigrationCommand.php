<?php

namespace ProdigeCatalogue\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Prodige\ProdigeBundle\Controller\BaseController;
use Prodige\ProdigeBundle\Services\LdapUtils;
use ProdigeCatalogue\AdminBundle\Services\ThesaurusSerializer;

class MigrationCommand extends ContainerAwareCommand {

    /**
     * Configures the command definition
     */
    protected function configure() {
        $this->setName('prodige:migration')
            ->setDescription('Migration from version 3.4.x to 4.0')
            ->addArgument('service_producteur', InputArgument::REQUIRED, 'identifiant de la rubrique service producteur de la plateforme')
        ;
    }

    /**
     * Executes the command
     */
    protected function execute(InputInterface $input, OutputInterface $output) {
        // call the prodige.configreader service
        $this->getContainer()->get('prodige.configreader');

        $output->setDecorated(true);
        $conn = $this->getCatalogueConnection('public,catalogue');
        $service_producteur = $input->getArgument('service_producteur');
        $tabRubric = $conn->fetchAll("select rubric_id from rubric_param where rubric_id=:id", array(
            "id" => $service_producteur
        ));

        if (empty($tabRubric)) {
            $output->writeln("<error>ERROR : aucun service producteur valide fourni</error>");
            return;
        }

        $urlGeonetwork = $this->getContainer()->getParameter('PRO_GEONETWORK_URLBASE');
        $servlet = ""; // "geonetwork";

        $thesaurus_name = $this->getContainer()->getParameter("THESAURUS_NAME");

        $this->migrationDbIntoLdap($input, $output);
        $this->migrateRights($input, $output, $service_producteur);
        $output->writeln('<info>création du thesaurus des domaines</info>');

        ThesaurusSerializer::getStandaloneInstance($conn, array(
            CURLOPT_HEADER => true,
            CURLOPT_NOBODY => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false
            ), $this->getContainer()->getParameter("PRO_GEONETWORK_URLBASE"), $thesaurus_name)->generateDocument(false);
        
        //copy thesaurus file since in migration upload won't work    
        if(file_exists("/tmp/".$thesaurus_name.".rdf")){
            copy ("/tmp/".$thesaurus_name.".rdf" , 
            $this->getContainer()->getParameter("PATH_COMMUN")."/prodigegeosource/WEB-INF/data/config/codelist/external/thesauri/theme/".$thesaurus_name.".rdf");
        }
            
        $output->writeln('<info>Migration des métadonnées sur les mots-clés domaines/sous-domaines</info>');
        $this->migrateMetadataSdom($input, $output, $urlGeonetwork, $servlet, $thesaurus_name);
        $output->writeln('<info>Migration des URL</info>');
        $this->migrateURL($input, $output);
        
    }
    /**
     * Migration des URLS
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    private function migrateURL(InputInterface $input, OutputInterface $output) {
    
        $urlCatalogue = $this->getContainer()->getParameter("PRODIGE_URL_CATALOGUE");
        
        //must be always www URL
        $urlWWW = str_replace("/geonetwork/", "", $this->getContainer()->getParameter("PRO_GEONETWORK_URLBASE"));
        $strSql =  "update metadata set data =replace(data, '".$urlCatalogue."/PRRA/panierDownloadFrontal_parametrage.php', '".$urlCatalogue."/geosource/panierDownloadFrontalParametrage') where isharvested='n';
                    update metadata set data =replace(data, '".$urlWWW."/PRRA/panierDownloadFrontal_parametrage.php', '".$urlCatalogue."/geosource/panierDownloadFrontalParametrage') where isharvested='n';
                    update metadata set data =replace(data, '".$urlCatalogue."/catalogue/panierDownloadFrontal_parametrage.php', '".$urlCatalogue."/geosource/panierDownloadFrontalParametrage') where isharvested='n';
                    update metadata set data =replace(data, '".$urlWWW."/catalogue/panierDownloadFrontal_parametrage.php', '".$urlCatalogue."/geosource/panierDownloadFrontalParametrage') where isharvested='n';
                    update metadata set data =replace(data, '".$urlCatalogue."/PRRA/atomfeed/atomdataset.php?uuid=', '".$urlCatalogue."/rss/atomfeed/atomdataset/') where isharvested='n';
                    update metadata set data =replace(data, '".$urlWWW."/PRRA/atomfeed/atomdataset.php?uuid=', '".$urlCatalogue."/rss/atomfeed/atomdataset/') where isharvested='n';
                    update metadata set data =replace(data, '".$urlCatalogue."/catalogue/atomfeed/atomdataset.php?uuid=', '".$urlCatalogue."/rss/atomfeed/atomdataset/') where isharvested='n';
                    update metadata set data =replace(data, '".$urlWWW."/catalogue/atomfeed/atomdataset.php?uuid=', '".$urlCatalogue."/rss/atomfeed/atomdataset/') where isharvested='n';
                    update metadata set data =replace(data, 'services/GetContext/index.php?id=', 'frontcarto/context/getOws/') where isharvested='n';
                    update metadata set data =replace(data, 'services/GetPDF/index.php?id=', 'frontcarto/export/getPdf?id=') where isharvested='n';
                    update metadata set data =replace(data, '".$urlCatalogue."/PRRA/atomfeed/topatom.php', '".$urlCatalogue."/rss/atomfeed/topatom') where isharvested='n';
                    update metadata set data =replace(data, '".$urlWWW."/PRRA/atomfeed/topatom.php', '".$urlCatalogue."/rss/atomfeed/topatom') where isharvested='n';
                    update metadata set data =replace(data, '".$urlCatalogue."/catalogue/atomfeed/topatom.php', '".$urlCatalogue."/rss/atomfeed/topatom') where isharvested='n';
                    update metadata set data =replace(data, '".$urlWWW."/catalogue/atomfeed/topatom.php', '".$urlCatalogue."/rss/atomfeed/topatom') where isharvested='n';
                    update metadata set data =replace(data, '".$urlCatalogue."/PRRA/consultation.php', '".$urlCatalogue."/geosource/consultation') where isharvested='n';
                    update metadata set data =replace(data, '".$urlWWW."/PRRA/consultation.php', '".$urlCatalogue."/geosource/consultation') where isharvested='n';
                    update metadata set data =replace(data, '".$urlCatalogue."/catalogue/consultation.php', '".$urlCatalogue."/geosource/consultation') where isharvested='n';
                    update metadata set data =replace(data, '".$urlWWW."/catalogue/consultation.php', '".$urlCatalogue."/geosource/consultation') where isharvested='n';";
        
        $this->getCatalogueConnection('public')->executeUpdate($strSql);
        
    }
    /**
     * Migration de la base utilisateur et groupe de catalogue vers ldap
     * 
     * @param InputInterface $input            
     * @param OutputInterface $output            
     */
    private function migrationDbIntoLdap(InputInterface $input, OutputInterface $output) {
        // Init LDAP
        $host = $this->getContainer()->getParameter("ldap_host");
        $port = $this->getContainer()->getParameter("ldap_port");
        $dnAdmin = $this->getContainer()->getParameter("ldap_search_dn");
        $password = $this->getContainer()->getParameter("ldap_password");
        $baseDn = $this->getContainer()->getParameter("ldap_base_dn");
        $ldapUtils = LdapUtils::getInstance($host, $port, $dnAdmin, $password, $baseDn);

        $output->writeln('<info>Import des sous-domaine dans le LDAP</info>');
        $this->migrateSousDomaineFromDbToLdap($input, $output, $ldapUtils);
        $output->writeln('<info>Import des utilisateurs dans le LDAP</info>');
        $this->migrateUsersFromDbIntoLdap($input, $output, $ldapUtils);
    }

    /**
     * Migration des utilisateurs vers la base LDAP en utilisant le type alkPerson
     * 
     * @param InputInterface $input            
     * @param OutputInterface $output            
     * @param LdapUtils $ldapUtils            
     */
    private function migrateUsersFromDbIntoLdap(InputInterface $input, OutputInterface $output, LdapUtils $ldapUtils) {
        $conn = $this->getCatalogueConnection('catalogue');
        $sql = "SELECT CONCAT(usr_prenom, ' ', usr_nom) as cn, "
            . "usr_email as mail, "
            . "usr_id as uid ,"
            . "usr_id as o ,"
            . "usr_nom as sn , "
            . "usr_description as description , "
            . "usr_prenom as gn , "
            . "pk_utilisateur as uidnumber , "
            . "CONCAT('{md5}', usr_password) as userpassword , "
            . "usr_telephone as telephonenumber "
            . " from utilisateur";

        $tabUsers = $this->decode_data($conn->fetchAll($sql));
        foreach ($tabUsers as $key => $tabUser) {
            $tabUser['alkmemberofgroup'] = $this->fetchSousDomaineFromUserPk($input, $output, $tabUser['uidnumber']);
            $tabUser['alkmemberofprofile'] = $this->fetchProfileFromUserPk($input, $output, $tabUser['uidnumber']);
            if ($ldapUtils->userExist($tabUser['uid'])) {
                $output->writeln("<info>L'utilisateur {$tabUser['uid']} existe déjà, mise à jour </info>");
                $ldapUtils->editUser($tabUser);
            } else {
                $output->writeln("<info>Création utilisateur {$tabUser['uid']} </info>");
                $ldapUtils->addUser($tabUser);
            }
        }
    }

    /**
     * Retourne une liste de sous-groupes associés a notre utilisateur
     * 
     * @param InputInterface $input            
     * @param OutputInterface $output            
     * @param int $pkUtilisateur            
     * @return array
     */
    private function fetchSousDomaineFromUserPk(InputInterface $input, OutputInterface $output, $pkUtilisateur) {
        $conn = $this->getCatalogueConnection('catalogue');
        $sql = "select sd.ssdom_id as alkMemberOfGroup " . "from administrateurs_sous_domaine as asd, sous_domaine as sd " . "where pk_utilisateur = :uid and asd.pk_sous_domaine = sd.pk_sous_domaine";
        $tabSDPerUser = $this->decode_data($conn->fetchAll($sql, array(
                "uid" => $pkUtilisateur
        )));
        $tab = [];
        foreach ($tabSDPerUser as $val) {
            $tab[] = $val['alkmemberofgroup'];
        }

        return $tab;
    }

    /**
     * Retourne le traitement d'un utilisateur :
     * - Administrator si l'utilisateur à le traitements_utilisateur Administration
     * - Reviewer si l'utilisateur à le traitement_utilisateur CMS
     * - sinon RegisteredUser
     * 
     * @param InputInterface $input            
     * @param OutputInterface $output            
     * @param int $pkUtilisateur            
     * @return string
     */
    private function fetchProfileFromUserPk(InputInterface $input, OutputInterface $output, $pkUtilisateur) {
        $conn = $this->getCatalogueConnection('catalogue');
        // Par défaut un utilisateur est RegisteredUser
        $result = 'RegisteredUser';
        // Si l'utilisateur est Reviewer on le met
        $sql = "select 'Reviewer' as traitement from traitements_utilisateur where pk_utilisateur = :pk and trt_id='CMS'";
        $tabTraitement = $conn->fetchAll($sql, array(
            "pk" => $pkUtilisateur
        ));
        foreach ($tabTraitement as $val) {
            $result = $val['traitement'];
        }

        $sql = "select 'Administrator' as traitement from traitements_utilisateur where pk_utilisateur = :pk and trt_id='ADMINISTRATION'";
        $tabTraitement = $conn->fetchAll($sql, array(
            "pk" => $pkUtilisateur
        ));
        foreach ($tabTraitement as $val) {
            $result = $val['traitement'];
        }

        return $result;
    }

    /**
     * Retourne une liste d'uid associés à notre utilisateur
     * 
     * @param InputInterface $input            
     * @param OutputInterface $output            
     * @param type $pkSSDomaine            
     */
    private function fetchUsersFromSousDomainePk(InputInterface $input, OutputInterface $output, $pkSSDomaine) {
        $conn = $this->getCatalogueConnection('catalogue');
        $sql = "select asd.usr_id as alkmember "
                . "from administrateurs_sous_domaine as asd, sous_domaine as sd "
                . "where sd.pk_sous_domaine = :ssDomId and asd.pk_sous_domaine = sd.pk_sous_domaine";
        $tabUserPerSD = $this->decode_data($conn->fetchAll($sql, array(
                "ssDomId" => $pkSSDomaine
        )));
        $tab = [];
        foreach ($tabUserPerSD as $val) {
            if ($val['alkmember'] != null) {
                $tab[] = $val['alkmember'];
            }
        }

        return $tab;
    }

    /**
     * Migration des sous-groupe vers la base LDAP en utilisant le type alkOUGroup
     * 
     * @param InputInterface $input            
     * @param OutputInterface $output            
     * @param LdapUtils $ldapUtils            
     */
    private function migrateSousDomaineFromDbToLdap(InputInterface $input, OutputInterface $output, LdapUtils $ldapUtils) {
        $conn = $this->getCatalogueConnection('catalogue');
        $sql = "SELECT ssdom_id as ou, " . "ssdom_nom as alkprofilename , " . "ssdom_description as description ," . "pk_sous_domaine as gidnumber " . " from sous_domaine";

        $tabSousDomaines = $this->decode_data($conn->fetchAll($sql));

        foreach ($tabSousDomaines as $key => $tabSousDomaine) {
            $tabSousDomaine['alkmember'] = $this->fetchUsersFromSousDomainePk($input, $output, $tabSousDomaine['gidnumber']);
            if ($ldapUtils->groupExist($tabSousDomaine['ou'])) {
                $output->writeln("<info>Le sous-domaine {$tabSousDomaine['ou']} existe déjà, mise à jour </info>");
                $ldapUtils->editGroup($tabSousDomaine);
            } else {
                $output->writeln("<info>Création du sous-domaine {$tabSousDomaine['ou']}</info>");
                $ldapUtils->addGroup($tabSousDomaine);
            }
        }
    }

    private function decode_data($data) {
        if (is_object($data) || is_array($data)) {
            array_walk_recursive($data, function (&$data) {
                $data = html_entity_decode($data, ENT_QUOTES, "UTF-8");
            });
        } else {
            $data = html_entity_decode($data, ENT_QUOTES, "UTF-8");
        }
        return $data;
    }

    /**
     * *****************************************************************************************************
     * 
     * @abstract supprime un tag Dom/sDom
     * @param doc DOMDocument du XML de la m�tadonn�e
     * @param domSdom_name nom du domaine ou du sous-domaine
     * @param newDomSdom_name nom du nouveau domaine ou sous-domaine
     * @return true si la m�tadonn�e a �t� mis � jour, false sinon
     *         *****************************************************************************************************
     */
    private function del_domsdom(&$doc, $domsdomArray) {
        $bUpdate = false;
        $xpath = new \DOMXpath($doc);

        // serach the data
        $keyword_list = @$xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/fra:FRA_DataIdentification/gmd:descriptiveKeywords/gmd:MD_Keywords");
        if (!$keyword_list || $keyword_list->length < 1) {
            // ISO 19139 metadatas
            $keyword_list = @$xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:descriptiveKeywords/gmd:MD_Keywords");
            if (!$keyword_list || $keyword_list->length < 1) {
                // services data
                $keyword_list = @$xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/gmd:descriptiveKeywords/gmd:MD_Keywords");
            }
        }
        if ($keyword_list && $keyword_list->length > 0) {
            for ($j = 0; $j < $keyword_list->length; $j ++) {
                $bSdom = false;
                $bdom = false;
                $bHasOtherKeywords = false;
                $keywordType = $keyword_list->item($j)->getElementsByTagName('MD_KeywordTypeCode');
                if ($keywordType->length > 0 && $keywordType->item(0)->hasAttribute("codeListValue") && $keywordType->item(0)->getAttribute("codeListValue") == "theme") {
                    $items = $keyword_list->item($j)->getElementsByTagName('CharacterString');
                    // error_log("matches : $items->length");
                    for ($i = 0; $i < $items->length; $i ++) {
                        // error_log($items->item($i)->nodeValue);
                        if (in_array($items->item($i)->nodeValue, $domsdomArray[0])) {
                            $bSdom = true;
                            // error_log("suppr sdom");
                            $sdomItem = $i;
                        } elseif (in_array($items->item($i)->nodeValue, $domsdomArray[1])) {
                            $bdom = true;
                            // error_log("suppr dom");
                            $domItem = $i;
                        } else {
                            // error_log("other");
                            $bHasOtherKeywords = true;
                        }
                    }
                    if ($bdom && $bSdom && !$bHasOtherKeywords) {
                        // suppression du noeud parent si n�cessaire
                        // error_log("delete bloc");
                        $itemToDelete = $keyword_list->item($j)->parentNode;
                        $itemToDelete->parentNode->removeChild($itemToDelete);
                    } else {
                        // suppression des noeuds strictement n�cessaires
                        if ($bdom)
                            $items->item($domItem)->parentNode->parentNode->removeChild($items->item($domItem)->parentNode);
                        if ($bSdom)
                            $items->item($sdomItem)->parentNode->parentNode->removeChild($items->item($sdomItem)->parentNode);
                    }
                }
            }
        }
    }

    /**
     * *****************************************************************************************************
     * @brief ajoute un tag Dom/sDom
     * tente de l'ajouter avant les tags resourceConstraints pour conserver la conformit� des m�tadonn�es
     * sinon, l'ajoute � la fin du tag XX_XXXIdentification
     *
     * @param
     *            doc DOMDocument du XML de la m�tadonn�e
     * @param
     *            newDomSdom_name nom du nouveau domaine ou sous-domaine
     *            *****************************************************************************************************
     */
    private function add_domsdom(&$doc, $thesaurus_link, $urlGeonetwork, $servlet, $thesaurus_name) {
        $bUpdate = false;

        $thesaurus_date = date("Y-m-d");

        $xmlFragment = "<gmd:identificationInfo xmlns:gmd=\"http://www.isotc211.org/2005/gmd\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" ><gmd:descriptiveKeywords xlink:href=\"" .
                        $urlGeonetwork . "/srv/fre/xml.keyword.get?thesaurus=external.theme." . $thesaurus_name . "&amp;amp;id=" . $thesaurus_link . "&amp;amp;multiple=true&amp;amp;lang=fre\" xlink:show=\"replace\" /></gmd:identificationInfo>";

        $orgDoc = new \DOMDocument();
        $orgDoc->loadXML($xmlFragment);

        $nodeToImport = $orgDoc->getElementsByTagName("descriptiveKeywords")->item(0);
        $xpath = new \DOMXpath($doc);
        // search the data
        $identification = @$xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/fra:FRA_DataIdentification");
        if (!$identification || $identification->length < 1) {
            // ISO 19139 metadata
            $identification = @$xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification");
            if (!$identification || $identification->length < 1) {
                // service metadata
                $identification = @$xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification");
            }
        }
        if ($identification && $identification->length > 0) {
            $newNodeDesc = $doc->importNode($nodeToImport, true);
            $ressourceConstraints = $identification->item(0)->getElementsByTagName("resourceConstraints");
            if ($ressourceConstraints && $ressourceConstraints->length > 0) {
                $identification->item(0)->insertBefore($newNodeDesc, $ressourceConstraints->item(0));
            } else {
                $identification->item(0)->appendChild($newNodeDesc);
            }
        }
    }

    /**
     * Migrate Rights from catalogue to geosource
     * 
     * @param InputInterface $input            
     * @param OutputInterface $output            
     */
    protected function migrateRights(InputInterface $input, OutputInterface $output, $service_producteur) {
        $conn = $this->getCatalogueConnection('public,catalogue');

        // 0 nettoyage base de données
        $tabIdNegative = array(
            - 7,
            - 6,
            - 3,
            - 2,
            - 1
        );
        $tabOldNew=array();
        $output->writeln('<info>nettoyage de la base geosource (identifiants négatifs)</info>');
        foreach ($tabIdNegative as $key => $value) {
            $tabSequence = $conn->fetchAll("SELECT nextval('hibernate_sequence') as sequence");
            if (!empty($tabSequence)) {
                $sql = "delete from operationallowed where metadataid=:oldid";
                $this->getCatalogueConnection('public')->executeUpdate($sql, array(
                    "oldid" => $value
                ));
                $sql = "delete from metadatacateg where metadataid=:oldid";
                $this->getCatalogueConnection('public')->executeUpdate($sql, array(
                    "oldid" => $value
                ));
                $sql = "delete from validation where metadataid=:oldid";
                $this->getCatalogueConnection('public')->executeUpdate($sql, array(
                    "oldid" => $value
                ));
                $sql = "update metadata set id =:newid,  	istemplate='y' where id =:oldid";
                $this->getCatalogueConnection('public')->executeUpdate($sql, array(
                    "newid" => $tabSequence[0]["sequence"],
                    "oldid" => $value
                ));
                
                $sql = "update metadata set istemplate='y' where id in (1,2,3);";
                $this->getCatalogueConnection('public')->executeUpdate($sql);
                
                $tabOldNew[$value]=$tabSequence[0]["sequence"];
            }
        }
        //renseignement des titles des models
        $this->migrateMetadataModel($input, $output, $tabOldNew);
        
        // on fixe owner des metadata sur compte admin geosource

        $output->writeln('<info>nettoyage de la base geosource (droits) </info>');
        $tabUpdate = array();
        $tabUpdate[] = "update metadata set owner = 1, groupowner = 1;";

        $tabUpdate[] = "delete from usergroups;";
        $tabUpdate[] = "delete from metadatastatus;";

        // on garde un compte admin et on passe en administrateur
        $tabUpdate[] = "update users set profile = 0 where id=1;";
        $tabUpdate[] = "delete from email where user_id <>1;";
        $tabUpdate[] = "delete from useraddress where userid <>1;";

        $tabUpdate[] = "delete from users where id <>1;";

        // delete groups except GUEST ?, All
        // suppression des droits à l'exception des droits de l'internaute
        $tabUpdate[] = "delete from groupsdes where iddes not in (-1, 0, 1); ";
        $tabUpdate[] = "delete from operationallowed where groupid not in (-1, 0, 1); ";
        $tabUpdate[] = "delete from groups where id not in (-1, 0, 1);";

        foreach ($tabUpdate as $key => $value) {
            $this->getCatalogueConnection('public')->executeUpdate($value);
        }

        $tabUpdate = array();

        // 1 Migration des groupes de geosource à partir des sous-domaines PRODIGE
        // on ne peut pas prendre sdom_nom, problème de doublons sur 32 caractères
        $output->writeln('<info>création des groupes geosource</info>');
        
        $sql = "select  pk_sous_domaine, substr(ssdom_id,0,32) as ssdom_id, ssdom_description, substr(sous_domaine.ssdom_nom,0,96) as ssdom_nom from catalogue.sous_domaine ";
        
        $tabSdom = $conn->fetchAll($sql);
        foreach($tabSdom as $key => $infos){
            $sql = "insert into public.groups (id, name, description) values (?, ?, ?)";
            $this->getCatalogueConnection('public,catalogue')->executeUpdate($sql,array( 
                $infos["pk_sous_domaine"],
                html_entity_decode($infos["ssdom_id"],ENT_QUOTES, 'UTF-8'),
                html_entity_decode($infos["ssdom_description"],ENT_QUOTES, 'UTF-8')          
                )
            );
            $sql = "insert into public.groupsdes (iddes,	langid, 	label) " . "select ? ".
                   " , languages.id, ?  from  languages ;";
            $this->getCatalogueConnection('public,catalogue')->executeUpdate($sql,array(
                $infos["pk_sous_domaine"],
                html_entity_decode($infos["ssdom_nom"],ENT_QUOTES, 'UTF-8')
                )
            );
        }
        
        
        
        

        $tabUpdate = array();

        // 2 Migration des utilisateurs de Prodige dans gesource
        /*
         * Profiles : 0 => administrateur 1 => administrateur utilisateur 2 => relecteur 3 => editeur 4 => utilisateur enregistré 5 => guest ? 6 => monitor ?
         */
        
        // par défaut profile 4 : utilisateur enregistré
        $tabUpdate[] = "insert into public.users(id, username,  password, surname,  name,  profile, organisation, kind, security, authtype, lastlogindate, nodeid)" . "select pk_utilisateur, usr_id, '', substr(usr_nom, 0, 32), substr(usr_prenom, 0, 32), 4, usr_service, NULL, '', 'LDAP', '', 'srv' from catalogue.utilisateur where pk_utilisateur <>1;";

        // si droit CMS => relecteur
        $tabUpdate[] = "update public.users set profile = 2 from traitements_utilisateur where pk_utilisateur = users.id and trt_id='CMS'";
        // si droit ADMIN => administrateur
        $tabUpdate[] = "update public.users set profile = 0 from traitements_utilisateur where pk_utilisateur = users.id and trt_id='ADMINISTRATION'";


        $output->writeln('<info>création des utilisateurs geosource</info>');
        foreach ($tabUpdate as $key => $value) {
            $this->getCatalogueConnection('public,catalogue')->executeUpdate($value);
        }

        $tabUpdate = array();

        // 3 Association groupes / utilisateurs / privilèges (relecteur) => tous les administrateurs de sous-domaine sont relecteurs sur les groupes
        $tabUpdate[] = "insert into usergroups (userid, groupid, profile) select pk_utilisateur, pk_sous_domaine, 2 from administrateurs_sous_domaine where usr_id is not null;";

        // intégration des droits métadonnées
        $tabUpdate[] = "insert into operationallowed (groupid, metadataid, operationid) select pk_sous_domaine , fmeta_id::bigint, 2 from couche_sdom where fmeta_id is not null".
                       " and fmeta_id::bigint in (select id from metadata)";
        $tabUpdate[] = "insert into operationallowed (groupid, metadataid, operationid) select pk_sous_domaine , fmeta_id::bigint, 2 from cartes_sdom where fmeta_id is not null".
                       " and fmeta_id::bigint in (select id from metadata)";

        $output->writeln('<info>migration des droits d\'édition</info>');
        foreach ($tabUpdate as $key => $value) {
            $this->getCatalogueConnection('public,catalogue')->executeUpdate($value);
        }

        // groupowner des métadonnées
        $output->writeln('<info>migration des groupowner</info>');
        $sql = "update metadata set groupowner = couche_sdom.pk_sous_domaine from couche_sdom  where metadata.id = couche_sdom.fmeta_id::bigint " . "and dom_coherence = :rubric_id ";
        $this->getCatalogueConnection('public,catalogue')->executeUpdate($sql, array(
            "rubric_id" => $service_producteur
        ));

        $sql = "update metadata set groupowner = cartes_sdom.pk_sous_domaine from cartes_sdom  where metadata.id = cartes_sdom.fmeta_id::bigint " . "and dom_coherence = :rubric_id";
        $this->getCatalogueConnection('public,catalogue')->executeUpdate($sql, array(
            "rubric_id" => $service_producteur
        ));

        // owner des métadonnées, mise à jour à partir des administrateurs du sous-domaine des métadonnées puis en priorité celles du service producteur
        $output->writeln('<info>migration des owner</info>');
        $tabUpdate = array();
        $tabUpdate[] = "update metadata set owner =  pk_utilisateur from couche_sdom, administrateurs_sous_domaine " . " where metadata.id = couche_sdom.fmeta_id::bigint and couche_sdom.pk_sous_domaine = administrateurs_sous_domaine.pk_sous_domaine and  usr_id is not null;";
        $tabUpdate[] = "update metadata set owner =  pk_utilisateur from cartes_sdom, administrateurs_sous_domaine " . " where metadata.id = cartes_sdom.fmeta_id::bigint and cartes_sdom.pk_sous_domaine = administrateurs_sous_domaine.pk_sous_domaine and usr_id is not null;";

        foreach ($tabUpdate as $key => $value) {
            $this->getCatalogueConnection('public,catalogue')->executeUpdate($value);
        }

        $tabUpdate = array();
        $tabUpdate[] = "update metadata set owner =  pk_utilisateur from cartes_sdom, administrateurs_sous_domaine " . " where metadata.id = cartes_sdom.fmeta_id::bigint and cartes_sdom.pk_sous_domaine = administrateurs_sous_domaine.pk_sous_domaine and usr_id is not null and dom_coherence = :rubric_id";
        $tabUpdate[] = "update metadata set owner =  pk_utilisateur from couche_sdom, administrateurs_sous_domaine " . " where metadata.id = couche_sdom.fmeta_id::bigint and couche_sdom.pk_sous_domaine = administrateurs_sous_domaine.pk_sous_domaine and usr_id is not null and dom_coherence = :rubric_id";
        foreach ($tabUpdate as $key => $value) {
            $this->getCatalogueConnection('public,catalogue')->executeUpdate($sql, array(
                "rubric_id" => $service_producteur
            ));
        }
        
    }

    /**
     * Migrate domaines in thesaurus
     */
    protected function migrateMetadataSdom(InputInterface $input, OutputInterface $output, $urlGeonetwork, $servlet, $thesaurus_name) {
        // todo change metadata in XML from thesaurus
        $conn = $this->getCatalogueConnection('public,catalogue');
        $em = $this->getManager();
        $sql = "SELECT dom_nom, ssdom_nom from dom_sdom ";

        $tabDomaines = $conn->fetchAll($sql);

        $tabDomSdom = array();
        // tableau des dom/sdom possibles
        foreach ($tabDomaines as $key => $tabInfo) {
            $dom_nom = $tabInfo["dom_nom"];
            $sdom_nom = $tabInfo["ssdom_nom"];
            $tabDomSdom[0][] = html_entity_decode($dom_nom, ENT_QUOTES, 'UTF-8');
            $tabDomSdom[1][] = html_entity_decode($sdom_nom, ENT_QUOTES, 'UTF-8');
        }

        $sql = "select metadata.uuid, id, data, dom_nom, pk_sous_domaine from couche_sdom, metadata where metadata.id = couche_sdom.fmeta_id::bigint " . "union select metadata.uuid, id, data, dom_nom, pk_sous_domaine from cartes_sdom, metadata where metadata.id = cartes_sdom.fmeta_id::bigint ";
        $tabMetadata = $conn->fetchAll($sql);
        $lastid = - 99999;
        $bFirst = true;

        foreach ($tabMetadata as $key => $tabInfo) {
            $newTabMetadata[$tabInfo["id"]][] = $tabInfo;
        }

        foreach ($newTabMetadata as $id => $tabInfo) {
            // chargement XML
            // au premier sdom, destruction des dom_sdom
            $data = $tabInfo[0]["data"];
            $uuid = $tabInfo[0]["uuid"];
            $version = "1.0";
            $encoding = "UTF-8";
            $metadata_doc = new \DOMDocument($version, $encoding);
            $entete = "<?xml version=\"" . $version . "\" encoding=\"" . $encoding . "\"?>\n";
            $metadata_data = $entete . $data;

            // $metadata_data = str_replace("&", "&amp;", $metadata_data);
            if (@$metadata_doc->loadXML(($metadata_data))) {
                $this->del_domsdom($metadata_doc, $tabDomSdom);

                // ajout des nouveaux sous domaines
                $strThesauruslink = "";
                foreach ($tabInfo as $identifiant => $tabDomaineSdomaine) {
                    $pk_sous_domaine = $tabDomaineSdomaine["pk_sous_domaine"];
                    $strThesauruslink .= $urlGeonetwork . "/thesaurus/theme/domaine%23sousdomaine_" . $pk_sous_domaine . ",";
                }
                $strThesauruslink = substr($strThesauruslink, 0, - 1);

                $this->add_domsdom($metadata_doc, $strThesauruslink, $urlGeonetwork, $servlet, $thesaurus_name);
                
                $this->migrateXmlinks($metadata_doc);
                $this->migrateGraphicOverview($metadata_doc, $uuid, $urlGeonetwork);
                
                if (isset($metadata_doc)) {
                    $new_metadata_data = $metadata_doc->saveXML();

                    // file_put_contents ("/home/devperso/bfontaine/metadata.xml", $new_metadata_data );die();
                    $new_metadata_data = str_replace($entete, "", $new_metadata_data);

                    $strUpdate = "update metadata set data=:data where id=:id";
                    // echo $new_metadata_data."\n\n";
                    // echo $id."\n\n";

                    $conn = $this->getCatalogueConnection('public');
                    // file_put_contents ( "/home/devperso/bfontaine/metadata" . ".xml", $new_metadata_data );

                    $conn->executeQuery($strUpdate, array(
                        "data" => $new_metadata_data,
                        "id" => $id
                    ));
                }
            } else {
                $output->writeln("<error>ERROR : impossible de charger le XML" . $id . "</error>");
            }
        }
    }
    
    /**
     * add desciption to xlinks
     * @param unknown $doc
     */
    protected function migrateXmlinks(&$doc){
        
        $xpath = new \DOMXpath($doc);
        $linkedUrl = @$xpath->query("/gmd:MD_Metadata/gmd:distributionInfo/gmd:MD_Distribution/gmd:transferOptions/gmd:MD_DigitalTransferOptions/gmd:onLine/gmd:CI_OnlineResource");
        if ($linkedUrl && $linkedUrl->length > 0) {
            foreach ($linkedUrl as $key => $node) {
                $description = $node->getElementsByTagName('description');
                if ($description->length == 0 || ($description->item(0) && $description->item(0)->hasChildNodes())){
                    //take it from name which should exists
                    $name = $node->getElementsByTagName('name');
                    if ($name->length > 0 && $name->item(0)->hasChildNodes() && $name->item(0)->firstChild->nodeValue!=""){
                        if ($name->item(0)->getElementsByTagName("CharacterString")->length>0){
                            $nameValue = $name->item(0)->getElementsByTagName("CharacterString")->item(0)->nodeValue;
                            if ($description->length == 0){
                                $xmlFragment = "<gmd:CI_OnlineResource 
                                                xmlns:gco=\"http://www.isotc211.org/2005/gco\" xmlns:gmd=\"http://www.isotc211.org/2005/gmd\" >
                                    <gmd:description><gco:CharacterString>".$nameValue."</gco:CharacterString></gmd:description>
                                </gmd:CI_OnlineResource>";
                                $orgDoc = new \DOMDocument();
                                $orgDoc->loadXML($xmlFragment);
                                $nodeToImport = $orgDoc->getElementsByTagName("description")->item(0);
                                $newNodeDesc = $doc->importNode($nodeToImport, true);
                                $name->item(0)->parentNode->appendChild($newNodeDesc);
                            }else{
                                if($description->item(0)->getElementsByTagName("CharacterString")->length>0 && $description->item(0)->getElementsByTagName("CharacterString")->item(0)->nodeValue==""){
                                    $description->item(0)->getElementsByTagName("CharacterString")->item(0)->nodeValue = $nameValue;
                                }
                            }
                        }
                    }                
                }
            }
        }
        
        
    }
    
    /**
     * migrate graphicOverview
     * @param unknown $doc
     */
    protected function migrateGraphicOverview(&$doc, $uuid, $urlGeonetwork){
    
        $xpath = new \DOMXpath($doc);
        $imageFile = @$xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:graphicOverview/gmd:MD_BrowseGraphic/gmd:fileName/gco:CharacterString");
        if ($imageFile && $imageFile->length > 0) {
            foreach ($imageFile as $key => $node) {
                $imgfile = $node->nodeValue;
                if (filter_var($imgfile, FILTER_VALIDATE_URL) === false) {
                    $node->nodeValue = $urlGeonetwork."/srv/api/records/".$uuid."/attachments/".$imgfile;
                }
            }
        }
    
    
    }
    /**
     * Migrate domaines in thesaurus
     */
    protected function migrateMetadataModel(InputInterface $input, OutputInterface $output, $tabOldNew) {
        // todo change metadata in XML from thesaurus
        $conn = $this->getCatalogueConnection('public,catalogue');
        $em = $this->getManager();
        $tabModels = array("2" => "métadonnée de série de données standard",
                           $tabOldNew["-2"]=> "métadonnée de série de données tabulaire",
                           $tabOldNew["-3"]=> "métadonnée de série de données par jointure",
                           "1"=> "métadonnée d'ensemble de séries de données",
                           "3" => "métadonnées de cartes",
                           $tabOldNew["-1"] => "métadonnées de données MAJIC",
                           $tabOldNew["-6"] => "métadonnées de service");
                           
        $sql = "SELECT id, data from metadata where id in (".implode(",", array_keys($tabModels)).")";
    
        $tabData = $conn->fetchAll($sql);
    
        // tableau des dom/sdom possibles
        foreach ($tabData as $key => $tabInfo) {
            $data = $tabInfo["data"];
            $id   = $tabInfo["id"];
            
            $version = "1.0";
            $encoding = "UTF-8";
            $metadata_doc = new \DOMDocument($version, $encoding);
            $entete = "<?xml version=\"" . $version . "\" encoding=\"" . $encoding . "\"?>\n";
            $metadata_data = $entete . $data;
    
            // $metadata_data = str_replace("&", "&amp;", $metadata_data);
            if (@$metadata_doc->loadXML(($metadata_data))) {
                
                $xpath = new \DOMXpath($metadata_doc);
                $title = @$xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/*/gmd:citation/gmd:CI_Citation/gmd:title/gco:CharacterString");
                if ($title && $title->length > 0) {
                    if($title->item(0)->nodeValue == null || $title->item(0)->nodeValue==""){
                        $title->item(0)->nodeValue = $tabModels[$id];
                    }
                }
                if (isset($metadata_doc)) {
                    $new_metadata_data = $metadata_doc->saveXML();
    
                    // file_put_contents ("/home/devperso/bfontaine/metadata.xml", $new_metadata_data );die();
                    $new_metadata_data = str_replace($entete, "", $new_metadata_data);
    
                    $strUpdate = "update metadata set data=:data where id=:id";
                    // echo $new_metadata_data."\n\n";
                    // echo $id."\n\n";
    
                    $conn = $this->getCatalogueConnection('public');
                    // file_put_contents ( "/home/devperso/bfontaine/metadata" . ".xml", $new_metadata_data );
    
                    $conn->executeQuery($strUpdate, array(
                        "data" => $new_metadata_data,
                        "id" => $id
                    ));
                }
            } else {
                $output->writeln("<error>ERROR : impossible de charger le XML" . $id . "</error>");
            }
        }
    }
    

    /**
     * Returns the default doctrine entity manager.
     *
     * @return \Doctrine\ORM\EntityManager Doctrine entity manager.
     */
    protected function getManager() {
        return $this->getContainer()->get('doctrine')->getManager();
    }

    /**
     * Get Doctrine connection
     * 
     * @param string $connection_name        	
     * @param string $schema        	
     * @return \Doctrine\DBAL\Connection
     */
    protected function getConnection($connection_name, $schema = "public") {
        $conn = $this->getContainer()->get("doctrine")->getConnection($connection_name);
        $conn->exec('set search_path to ' . $schema);

        return $conn;
    }

    /**
     *
     * @param string $schema        	
     * @return \Doctrine\DBAL\Connection
     */
    protected function getProdigeConnection($schema = "public") {
        return $this->getConnection(BaseController::CONNECTION_PRODIGE, $schema);
    }

    /**
     *
     * @param string $schema        	
     * @return \Doctrine\DBAL\Connection
     */
    protected function getCatalogueConnection($schema = "public") {
        return $this->getConnection(BaseController::CONNECTION_CATALOGUE, $schema);
    }

}
