<?php

use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;
use Prodige\ProdigeBundle\DAOProxy\DAO;
use ProdigeCatalogue\AdminBundle\Common\Modules\BO\UtilisateurVO;
use ProdigeCatalogue\AdminBundle\Common\Components\EditForm\EditForm;
use ProdigeCatalogue\AdminBundle\Common\AccessRights\AccessRights;

$profil = (isset($_POST["EXPORT_PROFIL"]) ? $_POST["EXPORT_PROFIL"] : "-1");
$type =  (isset($_POST["EXPORT_TYPE"]) ? $_POST["EXPORT_TYPE"] : "csv");
$dao = new DAO($conn);
if ($dao)
{ 
    $sqlStmt = "select distinct usr_id, usr_nom, usr_prenom, usr_email, usr_telephone, usr_telephone2, usr_service from utilisateur left join grp_regroupe_usr on utilisateur.pk_utilisateur = grp_regroupe_usr.grpusr_fk_utilisateur";
    $params = array();
    if($profil!="-1"){
      $params = array("profil"=>$profil);
      $sqlStmt.= " where grpusr_fk_groupe_profil = :profil";
    }
    if($type=="csv"){
      exportCSV($dao, $sqlStmt, $params);
    }else{
      exportLDIF($dao, $sqlStmt, $params);
    }
        
}
  
  
function exportCSV(DAO $dao, $sqlStmt, $params){
  $i =0;
  $strLine = "";
  $strHeader = "";
  $rs = $dao->getConnection()->executeQuery($sqlStmt, $params);
  foreach($rs as $row){
    foreach($row as $key => $value){
      //firstLine
      if(!is_numeric($key))
      {
        if($i==0){
          $strHeader.=$key.",";
        }
        $strLine.=$value.",";
      }
    }
    $strLine = substr($strLine, 0, -1)."\n\r";
    $i++;
  }
  $strHeader = substr($strHeader, 0, -1)."\n\r";
  header("Content-Type: application/csv-tab-delimited-table");
  header("Content-disposition: filename=export_user.csv");
  echo $strHeader.$strLine;
}  


function exportLDIF(DAO $dao, $sqlStmt, $params){
  $i =0;
  $strLine = "";
  $strHeader = "";
  $rs_select=$dao->BuildResultSet($sqlStmt, $params);
  for($rs_select->First();!$rs_select->EOF();$rs_select->Next()){
    $strLine.="dn: mail=".$rs_select->Read(3)."\n";
    $strLine.="uid: ".$rs_select->Read(0)."\n";     
    $strLine.="givenName: ".$rs_select->Read(2)."\n"; 
    $strLine.="mail: ".$rs_select->Read(3)."\n";  
    if($rs_select->Read(6)!=""){
      $strLine.="departmentNumber: ".$rs_select->Read(6)."\n";  
    }
    if($rs_select->Read(4)!=""){
      $strLine.="telephoneNumber: ".$rs_select->Read(4)."\n";  
    }
    $strLine.="sn: ".$rs_select->Read(1)."\n"; 
    $strLine.="\n";
  }
  header("Content-Type: application/octet-stream;");
  header("Content-disposition: filename=export_user.ldif");
  echo $strLine;
}


  
?>