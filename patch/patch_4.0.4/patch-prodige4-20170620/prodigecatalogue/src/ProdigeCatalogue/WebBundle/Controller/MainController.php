<?php

namespace ProdigeCatalogue\WebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Prodige\ProdigeBundle\Controller\BaseController;
use ProdigeCatalogue\AdminBundle\Common\Synd\AlkFSyndXmlWriter;
use ProdigeCatalogue\AdminBundle\Common\Synd\AlkFSyndXmlItem;
use Prodige\ProdigeBundle\DAOProxy\DAO;
use Prodige\ProdigeBundle\Controller\User;
use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;

/**
 * @author alkante
 */
class MainController extends BaseController {

    /**
     * @Route("/", name="catalogue_web_main", options={"expose"=true})
     * 
     * Affiche la page d'accueil de prodige4
     */
    public function mainAction()
    {
        return $this->redirect($this->container->getParameter('PRO_GEONETWORK_URLBASE'));
        /*$params = array(
            'prodige_version'   => $this->container->getParameter('PRODIGE_VERSION'),
            'prodige_title'     => defined('PRO_PROJECT_TITLE') ? PRO_PROJECT_TITLE : 'Prodige',
            'prodige_subtitle'  => defined('PRO_PROJECT_SUBTITLE') ? PRO_PROJECT_SUBTITLE : '',
            'prodige_catalog'   => defined('PRO_CATALOGUE_RESUME_TEXTE') ? PRO_CATALOGUE_RESUME_TEXTE : 'Catalogue',
            'prodige_site_name' => isset($GLOBALS['GEONETWORK_SETTINGS']['system/site/name']) ? $GLOBALS['GEONETWORK_SETTINGS']['system/site/name'] : '',
            'geonetwork_url'    => $this->container->getParameter('PRO_GEONETWORK_URLBASE'),
        );
        return $this->render('WebBundle:Default:accueil.html.twig', $params);*/
    }
}
