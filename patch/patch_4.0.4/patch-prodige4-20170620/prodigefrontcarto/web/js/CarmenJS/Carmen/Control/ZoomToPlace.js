 
/**
 * Class: Carmen.Control.ZoomToPlace
 */

Carmen.Control.ZoomToPlace = new OpenLayers.Class(OpenLayers.Control, {

  extentArea: "",

  zoomToPlacePanel : Ext.create('Ext.form.Panel', {
    collapsible: true,
    title: 'Zones prédéfinies',
    itemCls: 'areasClass',
    width: '100%',
    buttons: [{
      id: 'btn_resetToPlace',
      text: 'Réinitialiser',
      type : 'reset'
    }, '->', {
      id: 'btn_zoomToPlace',
      text: 'Localiser'
    }],
     tabConfig: {
      title: 'Zones prédéfinies',
      tooltip: 'Localisation par zones prédéfinies'
    },
    border: true
  }),

  combos : [],


  setMap: function(map) {
    OpenLayers.Control.prototype.setMap.apply(this, arguments);
    for (var i=0; i<this.configs.length; i++) {
    	// building combo
    	var combo = this.buildCombo(this.configs[i], i);
    	combo['config'] = this.configs[i];
      // adding listeners
      combo.addListener('change', function(cmb, newVal, oldVal) {});
      // select listeners depends if the combo is linked or not       
      if (this.configs[i].linkedCombo && typeof(this.combos[this.configs[i].linkedCombo-1])!="undefined") {
        combo.disable();
        /*combo.addListener('select', 
          function(cmb, record, index) {
            this.extentArea = record.data.extent;
          }, 
          this);*/
        // add listener to to parent combo
        // @vlc correction bug
        //this.combos[this.configs[i].linkedCombo-1].addListener('select', 
         this.combos[this.configs[i].linkedCombo-1].addListener('select', 
          Carmen.Control.ZoomToPlace.buildListenerLink(combo), 
          this);
          
      } else {
      /*	combo.addListener('select', 
          function(cmb, record, index) {
            this.extentArea = record.data.extent;
            this.scale = cmb['config'].providerBaseParams.fieldEchelle;
          }, 
          this);*/
      }

    	this.combos.push(combo);
      this.zoomToPlacePanel.add(combo);
    }

    /*var btn = this.zoomToPlacePanel.buttons[0];
    btn.addListener('click', 
      function () {
        var tabExtent = this.extentArea.split(",");
        var lonlat = new OpenLayers.LonLat(parseFloat(tabExtent[0]),parseFloat(tabExtent[1]));
        if(parseFloat(tabExtent[0]) == parseFloat(tabExtent[2]) && parseFloat(tabExtent[1]) == parseFloat(tabExtent[3])){
	        this.map.setCenter(lonlat);
	        this.map.zoomToScale(this.scale);
        }else{
        	var minScale = this.scale;
        	this.map.zoomToSuitableExtent(
        	  new OpenLayers.Bounds(tabExtent[0],
        			                tabExtent[1],
        			                tabExtent[2],
        			                tabExtent[3]
        	  ),minScale);
        }     	
      },
      this);
  */
    /*var navPanel = this.map.app.ui.getNavigationPanel();
    navPanel.add(this.zoomToPlacePanel);
    this.map.app.ui.doLayout();*/
    
    this.map.app.ui.addToLocatorPanel(this.zoomToPlacePanel, 0, true);
    //this.buildPanel(this.config);
    Ext.getCmp("btn_resetToPlace").setHandler(
        function(event, toolEl, panelHeader) {
        	this.zoomToPlacePanel.reset();
        },
        this
    );
    Ext.getCmp("btn_zoomToPlace").setHandler(
        function(event, toolEl, panelHeader) {
           if(this.extentArea==""){
             Ext.MessageBox.alert("Localiser", "Merci de sélectionner une zone prédéfinie");
           }else{
             var tabExtent = this.extentArea.split(",");
             var lonlat = new OpenLayers.LonLat(parseFloat(tabExtent[0]),parseFloat(tabExtent[1]));
              if(parseFloat(tabExtent[0]) == parseFloat(tabExtent[2]) && parseFloat(tabExtent[1]) == parseFloat(tabExtent[3])){
                //temporary fix (need to add scale config in admincarto
                if(this.scale==0){
                  this.scale = 1000;
                }  
                this.map.setCenter(lonlat);
                this.map.zoomToScale(this.scale);
              }else{
                var minScale = this.scale;
                this.map.zoomToSuitableExtent(
                  new OpenLayers.Bounds(tabExtent[0],
                                    tabExtent[1],
                                    tabExtent[2],
                                    tabExtent[3]
                  ),minScale);
              }     	
          }
        },
        this
      );
    

    // hack to resize combo length... cos couldn't find 
    // the way to force with iitial config
   /* for (var i=0; i<this.combos.length; i++) {
    	this.combos[i].getEl().dom.size=18;
    }*/
    
  },

  
  buildCombo : function(config, number) {
    
    var combo = Ext.create('Ext.form.ComboBox',{ 
       listConfig: {
        loadingText: 'Chargement',
        getInnerTpl: function() { return '<tpl for="."><div ext:qtip="{name}" class="x-combo-list-item">{name}</div></tpl>'; }
       },
       emptyText: config.labelText,
       displayField: 'name',
       valueField: 'name',
       typeAhead: true,
       typeAheadDelay: 250,
       forceSelection: true,
       queryMode: 'local',
       triggerAction: 'all',
       editable: true,
       minChars: 1,
       queryDelay: 250,
       width: '100%',
       xtype: 'combo',
       id: 'combo' + number,
       store: Carmen.Control.ZoomToPlace.buildStore(config,number),
       listeners:{select:{fn:function(combo, value) {
                             this.extentArea = value[0].data.extent;
                             this.scale = config.providerBaseParams.fieldEchelle;
                           }},
                  scope: this
                       }
     });
     if (!config.linkedCombo)
       combo.getStore().load();
     return combo;
   },
  
  
  CLASS_NAME: "Carmen.Control.ZoomToPlace"

});



// Todo: Should be passed as a config object, at least the reader...
Carmen.Control.ZoomToPlace.buildStore = function(controlConfig, number) {
  var params = OpenLayers.Util.extend(
      { queryMode: 'getData', 
        queryParams_pk: number},
      controlConfig.providerBaseParams);
          
     var store = new Ext.data.Store({
       proxy: {
         type : 'ajax',
         url: controlConfig.providerUrl,
         extraParams: params,
         reader: {
           type : 'json',
           idproperty: 'id',
           totalProperty: 'totalCount',
           rootProperty: 'Names'
         },
       },
       fields : 
         [ { name: 'extent'},
           { name: 'id' },
           { name: 'name' }
         ]
     });
     return store;

};

Carmen.Control.ZoomToPlace.buildListenerLink = function(cbSlave) {
  return function(cmb, record, index) {
    console.log(cbSlave);
    var linkFieldName = cbSlave.config.linkedFieldName;
    var newBaseParams = OpenLayers.Util.extend({} , cbSlave.getStore().proxy.extraParams);
    newBaseParams = OpenLayers.Util.extend(newBaseParams, cbSlave.config.providerLinkParams);
    
    newBaseParams[linkFieldName] = record[0].data.id;  
	  
    cbSlave.getStore().proxy.extraParams = newBaseParams;
    cbSlave.clearValue();
    
    cbSlave.getStore().load();
    cbSlave.enable();
    this.extentArea = record[0].data.extent;
  };
};
