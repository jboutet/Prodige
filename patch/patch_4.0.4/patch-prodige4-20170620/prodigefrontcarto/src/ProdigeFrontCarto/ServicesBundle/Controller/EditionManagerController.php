<?php

namespace ProdigeFrontCarto\ServicesBundle\Controller;

use Prodige\ProdigeBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use ProdigeFrontCarto\HelpersBundle\MapfileToXML\CarmenMapfileToOWC;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Doctrine\DBAL\Types\Type;

use Prodige\ProdigeBundle\Controller\User;
use Prodige\ProdigeBundle\Common\Mail\AlkMail;
use Doctrine\DBAL\Connection;


/**
 * @Route("/editionmanager", options={"expose"=true}, requirements={"mapfile"=".+"})
 */
class EditionManagerController extends BaseController 
{
    const DEBUG_MAIL = false;
    const DEBUG_MAILTO = "m.artigny@alkante.com";
    const CALCUL_AUTOMATIQUE = "CALCUL_AUTOMATIQUE";
    /**
     * @var \Doctrine\DBAL\Connection
     */
    protected $PRODIGE;
    /**
     * @var \Doctrine\DBAL\Connection
     */
    protected $CATALOGUE;
    protected $editionLayer;
    protected $editionLayername;
    protected $editionTablename;
    
    protected $snapLayername;
    protected $snapTablename;
    protected $projection;
    
    protected $mapfile;
    protected $mapObj;
    
    protected $user_id;
    protected $msg_template_for_alert_user;
    protected $pk_couche_donnees;
    protected $territoire = array("filter_table"=>false, "filter_field"=>false, "filter_values"=>array());
    
    /**
    * 
    * @param Request $request
    */
    public function _initialize(Request $request, $mapfile=null, $editionLayername=null, $snapLayername=null)
    {
        $this->PRODIGE = $this->getProdigeConnection("public");
        $this->CATALOGUE = $this->getCatalogueConnection("catalogue");
        
        defined("PARAM_INT_ARRAY") || define("PARAM_INT_ARRAY", \Doctrine\DBAL\Connection::PARAM_INT_ARRAY );
        defined("PARAM_STR_ARRAY") || define("PARAM_STR_ARRAY", \Doctrine\DBAL\Connection::PARAM_STR_ARRAY );
        defined("PARAM_INTEGER")   || define("PARAM_INTEGER", Type::INTEGER);
        defined("PARAM_STRING")    || define("PARAM_STRING", Type::STRING);
        
        $this->editionLayername = $editionLayername;
        $this->snapLayername = ($snapLayername==="null" ? null : $snapLayername);
        $this->mapfile = $mapfile;
    
        if($this->mapfile) {
            $this->mapObj = ms_newMapObj($this->mapfile);
        }
        
        if($this->editionLayername && $this->mapfile) {
            $this->editionLayer = @$this->mapObj->getLayerByName($this->editionLayername);
            $this->editionTablename = $this->_getTableNameFromDataStr($this->editionLayer->data);
            
            $query =  "SELECT st_srid(the_geom) FROM {$this->editionTablename} limit 1";
            $params = array("editionTablename"=>$this->editionTablename);
            $this->projection = $this->PRODIGE->fetchColumn($query, $params);
            if ( $this->projection===false ){
                $table_name = explode(".", $this->editionTablename); $table_name = end($table_name);
                $query =  "SELECT srid FROM geometry_columns where f_table_name = :editionTablename";
                $params = array("editionTablename"=>$table_name);
                $this->projection = $this->PRODIGE->fetchColumn($query, $params);
            }
        }
        if($this->snapLayername && $this->mapfile) {
            $snapLayer = @$this->mapObj->getLayerByName($this->snapLayername);
            $this->snapTablename = $this->_getTableNameFromDataStr($snapLayer->data);
        }
        
        $user = User::GetUser();
        $this->user_id = User::getUserId();
        $this->territoire = array("filter_table"=>false, "filter_field"=>false, "filter_values"=>array());
        
        if ( $this->editionLayername ){
            $this->pk_couche_donnees = $this->CATALOGUE->fetchColumn("select pk_couche_donnees from couche_donnees where couchd_emplacement_stockage=:couchd_emplacement_stockage", array("couchd_emplacement_stockage"=>$this->editionTablename));
            if ( $this->pk_couche_donnees ){
                //ajout d'informations sur les restrictions territoriales
                //vérification des restrictions territoriales
                $territoires = $user->GetTraitementTerritoire('EDITION', $this->pk_couche_donnees);
                //$trTerr= 1 : pas de restriction, $trTerr=1 : restriction mais aucun droit => pas de droit d'édition, sinon tableau des territoires autorisés
                if (is_array($territoires)) {
                    $this->territoire["filter_table"] = 'prodige_perimetre';
                    $this->territoire["filter_field"] = $territoires[0][2];
                    //on suppose que le filtre est identique sur tous les périmètres
                    foreach ($territoires as $value) {
                        $this->territoire["filter_values"][] = $value[0];
                    }
                }
                
                

                $request->query->add($this->territoire);
                $strSql1 = "SELECT couchd_alert_msg_id FROM couche_donnees WHERE pk_couche_donnees = :pk_couche_donnees";
                $this->msg_template_for_alert_user = $this->CATALOGUE->fetchColumn($strSql1, array("pk_couche_donnees"=>$this->pk_couche_donnees));
            }
        }
    }

    /**
    * @Route("/allenumsvaluesbynames/{enums_names}", name="editionmanager_allenumsvaluesbynames", requirements={})
    */
    public function allEnumsValuesByNamesAction(Request $request, $enums_names){
        $this->_initialize($request);
        
        $query =  "select n.nspname as enum_schema, t.typname as enum_name, string_agg(e.enumlabel, ', ') as enum_value 
                from pg_type t 
                join pg_enum e on t.oid = e.enumtypid join pg_catalog.pg_namespace n ON n.oid = t.typnamespace 
                WHERE t.typname in (:enums_names) 
                group by enum_schema, enum_name";
        $params = array("enums_names"=>explode(",", $enums_names));
        $types = array("enums_names"=> PARAM_STR_ARRAY); 
        
        $resultArray = array();
        $stmt = $this->PRODIGE->executeQuery($query, $params, $types);
        foreach($stmt as $row){
            $field = array();
            $field['enum_name'] = $row['enum_name'];
            $field['enum_value'] = $row['enum_value'];
    
            $resultArray[] = $field;
        }
        return new JsonResponse(array(
            "success" => !empty($resultArray),
            "enums_values"=>$resultArray
        ));
    }
    
    /**
    * @Route("/allprevioussessions/{editionLayername}/{mapfile}", name="editionmanager_allprevioussessions")
    */
    public function allPreviousSessionsAction(Request $request, $editionLayername, $mapfile){
        $this->_initialize($request, $mapfile, $editionLayername);
    
        $query = "SELECT DISTINCT table_name FROM information_schema.tables WHERE table_name LIKE :table_name||'\\_%'";
        $table_name = explode(".", $this->editionTablename); $table_name = end($table_name);
        $params = array("table_name"=>$table_name);
        
        $displaySession = array();
        $valueSession = array();
        $sessions_store = array();
        $stmt = $this->PRODIGE->executeQuery($query, $params);
        foreach($stmt as $row){
            $table_name = $row['table_name'];
            $explodeSession = explode("_", $table_name);
            $sessions_store[] = array(
                'displaySession' => ($displaySession[] = date('Y-m-d H:i:s', intval($explodeSession[count($explodeSession) - 1]) )),
                'valueSession'   => ($valueSession[]   = $table_name)
            );
        }
        
        array_multisort($displaySession, SORT_LOCALE_STRING , SORT_DESC, $valueSession, SORT_LOCALE_STRING , $sessions_store);
    
        return new JsonResponse($sessions_store);
    }
    
    
    /**
    * @Route("/checkfeature/{editionLayername}/{snapLayername}/{mapfile}", name="editionmanager_checkfeature")
    */
    public function checkFeatureAction(Request $request, $editionLayername, $snapLayername, $mapfile){
        $this->_initialize($request, $mapfile, $editionLayername, $snapLayername);
    
        $gid              = $request->get("gid", null);
        $geometry         = $request->get("geometry", null);
        $constraintType   = $request->get("constraintType", "Point");
        $empriseTablename = $request->get("emprise_tablename", "");
        $empriseId        = $request->get("emprise_id", -1);
    
        if ( !$geometry ){
            return new JsonResponse(array(
                "check" => true,
               // "msg" => "Paramètre 'geometry' manquant"
            ));
        }
    
        
        $res = array(
            "check" => true,
            "constraintType" => $constraintType,
            "snapTablename" => null,
            "geometry" => $geometry
        );
        if ( $this->snapTablename ){
            $constraintParams = array("extentTableName" => $empriseTablename, "extentId" => $empriseId);
            if ( !$this->_checkConstraint($geometry, $this->snapTablename, "isvalid") ) {
                // test de validité de la géométrie
                $res = array(
                    "check" => false,
                    "msg" => "La géométrie n'est pas valide",
                    "gid" => $gid
                );
            }
            else if ( $empriseTablename && !$this->_checkConstraint($geometry, $this->snapTablename, "extent", $constraintParams) ) {
                // test d'inclusion dans l'emprise de numérisation
                $res = array(
                    "check" => false,
                    "msg" => "La géométrie n'est pas totalement incluse dans l'emprise de numérisation",
                    "gid" => $gid
                );
            }
            else if ( !$this->_checkConstraint($geometry, $this->snapTablename, $constraintType) ) {
                $res = array(
                    "check" => false,
                    "msg" => "La géométrie ne s'appuie pas sur le référentiel",
                    "gid" => $gid
                );
            
            }
        }
        if ( !$this->_checkConstraint($geometry, $this->snapTablename, "territoire") ) {
            $res = array(
                "check" => false,
                "msg" => "La géométrie est hors du territoire de compétences",
                "gid" => $gid,
                "reject" => true
            );
        
        }
        
        return new JsonResponse($res);
        
    }
        
    /**
    * @Route("/checklock/{editionLayername}/{mapfile}", name="editionmanager_checklock")
    */
    public function checkLockAction(Request $request, $editionLayername, $mapfile){
        $this->_initialize($request, $mapfile, $editionLayername);
        $extentStr = $request->get("extent", "");
              
        $isView = $request->get("isView", false);
        $isView = ($isView == "true") ? true : false;
    
        //retrieving extent from extent table
          
        $geomExtent = $this->_extentStrToGeomStr($extentStr);
    
        $createLocked = $this->_checkLockOnTable($this->editionTablename, $isView);
        $createLocked = $this->_checkLockOnTable($this->editionTablename, $isView, "_edit_datemaj", "timestamp");
        return new JsonResponse(array("success" => $createLocked, "geomExtent" => $geomExtent));
    }
    
    /**
        * @Route("/editstate/{state}/{editionLayername}/{mapfile}", name="editionmanager_editstate", requirements={"state"="new|modified|deleted"})
        */
    public function editStateAction(Request $request, $state, $editionLayername, $mapfile){
        $this->_initialize($request, $mapfile, $editionLayername);
        
        $nbr_of_add       = $request->get("nbr_of_add", "");
        $workingTablename = $request->get("workingTablename", "");
        $gid              = $request->get("gid", "");
        $geometry         = $request->get("geometry", "");
        $constraintType   = $request->get("constraintType", "Point");
        
        //$is_valid = $request->get("is_valid", "true")=="true";
        //if(($workingTablename == "") && ($gid == "") && ($geometry == "")) {
        if(($workingTablename == "") && ($geometry == "")) {
            trigger_error("Missing or wrong parameters in EditionManager [" . "editstate" . "] call.");
        }
        
        $isView = $request->get("isView", false);
        $isView = ($isView == "true") ? true : false;
    
        try {
            $gid = $this->_updateTempFeature($this->editionTablename, $nbr_of_add, $workingTablename, $gid, $geometry, $state, $isView);
            return new JsonResponse(array("success"=>true, "gid"=>$gid));
        }catch(\Exception $exception){
            return new JsonResponse(array("success"=>false, "gid"=>$gid, "message"=>$exception->getMessage()));
        }
    }

    /**
    * @Route("/endedition/{editionLayername}/{mapfile}", name="editionmanager_endedition")
    */
    public function endEditionAction(Request $request, $editionLayername, $mapfile){
        
        $workingTablename = $request->get("workingTablename", "");
        $sessionState = $request->get("sessionState", false);
        $filter_table = $request->get("filter_table", "");
        $filter_field = $request->get("filter_field", "");
        $filter_values = $request->get("filter_values", "");
        $gid =  $request->get("gid", "");
        $saveChanges = $request->get("saveChanges", "false")=="true" ;
        $donnee =  $request->get("donnee", "");
        $isView = $request->get("isView", false);
        $isView = ($isView == "true") ? true : false;
        
        $this->_initialize($request, $mapfile, $editionLayername);
      
        if ($editionLayername == "" || $workingTablename == "") {
            $res = array(
                "success" => true,
                "saveChanges" => false,
                "gids" => array(),
                "tablename" => $this->editionTablename,
                "nbrOfSentMails" => 0
            );
            return new JsonResponse($res);
            throw new \Exception("Missing or wrong parameters in EditionManager [" . "endedition" . "] call.");
        }
    
        $test = true;
        $gids = array();
        $nbrOfSendMails = 0;
        if ($saveChanges) {
            $column_name = "state";
            $sessionState = ($sessionState == "true") ? true : false;
            $gids = $this->_bringBackUpdatesToTable($this->editionTablename, $workingTablename, $sessionState, $gid, $column_name, $isView);
            
            if($gids && count($gids) > 0) {
                //$alerts = json_decode($alerts);
                $nbrOfSendMails = $this->_alertUsersForModifiedObjects($request, $this->editionTablename, $workingTablename, $gids, $donnee);
            }
        }
        $test = $this->_removeLockOnTable($this->editionTablename, $workingTablename, $isView);
        $test = $test && $this->_dropTempTable($workingTablename);
        $res = array(
            "success" => $test,
            "saveChanges" => $saveChanges,
            "gids" => implode(',', (array)$gids),
            "tablename" => $this->editionTablename,
            "nbrOfSentMails" => $nbrOfSendMails
        );
        return new JsonResponse($res);
    }
    

    /**
    * @Route("/getblockedfields/{editionLayername}/{mapfile}", name="editionmanager_getblockedfields")
    */
    public function getBlockedFieldsAction(Request $request, $editionLayername, $mapfile){
        $this->_initialize($request, $mapfile, $editionLayername);
        
        $ar_values_not_updates = array();
        $schema = "parametrage";
    
        $table_view_info = "prodige_view_info";
        $query = "SELECT pk_view FROM {$schema}.{$table_view_info} WHERE view_name = :view_name";
        $params = array("view_name"=>$this->editionTablename);
        $pk_view = $this->PRODIGE->fetchColumn($query, $params);
        if ( !$pk_view ) throw new \Exception("Error while accessing param table.");
        
        $ar_values_not_updates[] = "c_gid";
    
        $table_join_info = "prodige_join_info";
        $query = "SELECT table_name, table_fields, join_criteria FROM ".$schema.".".$table_join_info." WHERE pk_view = :pk_view ORDER BY join_pos";
        $stmt = $this->PRODIGE->executeQuery($query, array("pk_view"=>$pk_view));
        $pos_join = 1;
        foreach($stmt as $row){
            $ar_values_not_updates[] = "t".$pos_join."_gid";
            $value_to_explode = $row["join_criteria"];
            $value_to_explode = str_replace(array( '[', ']' ), '', $value_to_explode);
            $ar_values_to_push = explode('=', $value_to_explode);
            foreach($ar_values_to_push as $value) {
                $ar_values_not_updates[] = trim($value);
            }
            $pos_join++;
        }
        $ar_values_not_updates = array_values(array_unique($ar_values_not_updates));
        $res = array(
        "success" => !empty($ar_values_not_updates),
        "values_not_updates" => (empty($ar_values_not_updates) ? null : $ar_values_not_updates)
        );
        return new JsonResponse($res);
    }


    /**
    * @Route("/setinformation/{editionLayername}/{workingTablename}/{mapfile}", name="editionmanager_setinformation")
    */
    public function setInformationAction(Request $request, $editionLayername, $workingTablename, $mapfile){
        $this->_initialize($request, $mapfile, $editionLayername);
        $fieldsToModifiy = $request->get("fieldsToModifiy", "[]");
        $fieldsToModifiy = json_decode($fieldsToModifiy, true);
        $geometry = $request->get("geometry", null);
    
        $gid = $this->_setInformationInTempTableForEdition($this->editionTablename, $workingTablename, $fieldsToModifiy);
    
        if ( $gid!==false ){
            $gid = $this->_updateTempFeature($this->editionTablename, 0, $workingTablename, $gid, $geometry, "modified", false);
        }
        $res = array(
            "setInformation" => $gid!==false
        );
        return new JsonResponse($res);
    }
    
    /**
    * @Route("/gettablecolumns/{editionLayername}/{mapfile}", name="editionmanager_gettablecolumns")
    */
    public function getTableColumnsAction(Request $request, $editionLayername, $mapfile){
        $this->_initialize($request, $mapfile, $editionLayername);
        $Types = $this->_getAllDataType($this->editionTablename);
        return new JsonResponse(array("columns"=>$Types));
    }
    
    /**
    * @Route("/saveattributes/{editionLayername}/{mapfile}", name="editionmanager_saveattributes")
    */
    public function saveAttributesAction(Request $request, $editionLayername, $mapfile){
        $this->_initialize($request, $mapfile, $editionLayername);
        $fieldsToModifiy = $request->get("fieldsToModifiy", "[]");
        $fieldsToModifiy = json_decode($fieldsToModifiy, true);
    
        $gid = $this->_setInformationInTempTableForEdition($this->editionTablename, $this->editionTablename, $fieldsToModifiy);
        return new JsonResponse(array("saveAttributes"=>$gid!==false));
    }
    
    /**
    * @Route("/startedition/{editionLayername}/{mapfile}", name="editionmanager_startedition")
    */
    public function startEditionAction(Request $request, $editionLayername, $mapfile){
        $maxSize = 1000;
        $snapLayername = $request->get("snapLayername", null);
        $this->_initialize($request, $mapfile, $editionLayername, $snapLayername);
        
        $extentStr = $request->get("extent", "");
        //retrieving extent from extent table
        $geomExtentStr = $this->_extentStrToGeomStr($extentStr);
    
        $isView = $request->get("isView", false);
        $isView = ($isView == "true") ? true : false;
    
        $editionTableSession = $request->get("editionTableSession", "");
        if($editionTableSession == "") {
            $editionTempTableName = $this->_createTempTableForEdition($this->editionTablename, $geomExtentStr, $this->user_id, $isView);
        }
        else {
            $editionTempTableName = $editionTableSession;
        }
          
        $this->_putLockOnTable($this->editionTablename, $editionTempTableName, $this->user_id, $isView);
        $editionCount = $this->_getCountExport($editionTempTableName, CARMEN_PATH_CACHE, $this->user_id);
        if ( $editionCount>$maxSize ){
            $this->_dropTempTable($editionTempTableName);
            $res = array(
                "edition" => false,
                "workingTablename" => null,
                "editionData" => null,
                "snapData" => null,
                "size" => $editionCount,
                "featureTypeEdition" => null,
                "allColumnsTypesForWorkingTableName" => null
            );
            return new JsonResponse($res);
        }
        $editionFilename = $this->_exportTableToFile($editionTempTableName, CARMEN_PATH_CACHE, $this->user_id);
    
        $editionData = json_decode($this->_dataFromfile($editionFilename), true);
        @unlink(explode('.', $editionFilename)[0]);
        @unlink($editionFilename);
        $editionFilenameUrl = CARMEN_URL_CACHE . basename($editionFilename);
    
        // Geometry type
        // work here : Benoist
        $featureTypeEdition = 'polygon';
    
        if($this->editionLayer->type==MS_LAYER_POINT)
            $featureTypeEdition = 'point';
        else if ($this->editionLayer->type==MS_LAYER_LINE)
            $featureTypeEdition = 'path';
    
    
        $snapData = null;
        $snapCount = 0;
    
        if($this->snapLayername != "") {
            $snapLayer = @$this->mapObj->getLayerByName($this->snapLayername);
            if($snapLayer) {
                $snapTablename = $this->_getTableNameFromDataStr($snapLayer->data);
                
                // mettre extentstr à null pour ne pas filtrer la couche snapping sur l'emprise de la carte courante,
                // mais récupérer l'ensemble des données dans l'emprise référencée par empriseId
                $snapTempTableName = $this->_createTempTableForEdition($snapTablename, $geomExtentStr, $this->user_id, $isView);
                
                $snapCount = $this->_getCountExport($snapTempTableName, CARMEN_PATH_CACHE, $this->user_id, "", -1, null);
                if ( $snapCount>$maxSize ){
                    $this->_dropTempTable($editionTempTableName);
                    $this->_dropTempTable($snapTempTableName);
                    $res = array(
                        "edition" => false,
                        "workingTablename" => null,
                        "editionData" => null,
                        "snapData" => null,
                        "size" => $snapCount,
                        "featureTypeEdition" => null,
                        "allColumnsTypesForWorkingTableName" => null
                    );
                    return new JsonResponse($res);
                }
                $snapFilename = $this->_exportTableToFile($snapTempTableName, CARMEN_PATH_CACHE, $this->user_id, "", -1, null);
                $snapFilenameSize = filesize($snapFilename);
                $snapData = json_decode($this->_dataFromfile($snapFilename), true);
                if ( $snapTempTableName != $snapTablename ) $test = $this->_dropTempTable($snapTempTableName);
                @unlink(explode('.', $snapFilename)[0]);
                @unlink($snapFilename);
                
            }
        }
        $totalsize = $editionCount + $snapCount;
    
        if($totalsize < $maxSize) {
            $Types = $this->_getAllDataType($this->editionTablename);
            $res = array(
                "edition" => true,
                "workingTablename" => $editionTempTableName,
                "editionData" => $editionData,
                "snapData" => $snapData,
                "size" => $totalsize,
                "featureTypeEdition" => $featureTypeEdition,
                "allColumnsTypesForWorkingTableName" => $Types
            );
        }
        else {
            $this->_dropTempTable($editionTempTableName);
            $res = array(
                "edition" => false,
                "workingTablename" => null,
                "editionData" => null,
                "snapData" => $snapData,
                "size" => $totalsize,
                "featureTypeEdition" => null,
                "allColumnsTypesForWorkingTableName" => null
            );
        }
    
        return new JsonResponse($res);
    }

    
    /**
    * @Route("/periodic/alertedition/{frequency}", name="editionmanager_periodicalertedition", defaults={"frequency"="1 weeks"})
    */
    public function periodicAlertEditionAction($frequency = "1 weeks"){
        $CATALOGUE = $this->getCatalogueConnection('catalogue');
        $PRODIGE = $this->getProdigeConnection('public');
    
        if ( self::DEBUG_MAIL ){
            $frequency = "1 days";
        }
        
        // call the prodige.configreader service
        $this->get('prodige.configreader');
        
            
        // init config
        $constantes = $CATALOGUE->fetchAll("select * from prodige_settings");
        foreach($constantes as $constante){
            if ( !defined($constante["prodige_settings_constant"]) ){
                define($constante["prodige_settings_constant"], $constante["prodige_settings_value"]);
            }
        }
        
        
        $couches = $CATALOGUE->fetchAll("select distinct pk_couche_donnees, couchd_nom, couchd_emplacement_stockage, couchd_alert_msg_id, uuid ".
                    " from couche_donnees ".
                    " inner join fiche_metadonnees on (fmeta_fk_couche_donnees=pk_couche_donnees) ".
                    " inner join public.metadata on (fmeta_id=metadata.id::text) ".
                    " inner join usr_alerte_perimetre_edition on (usralert_fk_couchedonnees=pk_couche_donnees)");
        
        $byUser = array();
        foreach($couches as $couche) {
            $pk_couche_donnees = $couche["pk_couche_donnees"];
            $couchd_nom = $couche["couchd_nom"];
            $couchd_emplacement_stockage = $couche["couchd_emplacement_stockage"];
            $couchd_alert_msg_id = $couche["couchd_alert_msg_id"];
            $uuid = $couche["uuid"];
            
            $hasEdition = $PRODIGE->fetchColumn("select true from ".$couchd_emplacement_stockage." where _edit_datemaj::date>(now()::date - '{$frequency}'::interval)");
            
            if ( !$hasEdition ) continue;
            
            $zonages = $CATALOGUE->fetchAll("select distinct couchd_emplacement_stockage, zonage_field_id, perimetre_code, pk_perimetre_id, usralert_fk_utilisateur, usr_email, usr_nom, usr_prenom".
                    " from usr_alerte_perimetre_edition ".
                    " inner join perimetre on (usralert_fk_perimetre=pk_perimetre_id) ".
                    " inner join zonage on (perimetre_zonage_id=pk_zonage_id) ".
                    " inner join couche_donnees on (zonage_fk_couche_donnees=pk_couche_donnees) ".
                    " inner join utilisateur on (usralert_fk_utilisateur=pk_utilisateur) ".
                    " where usralert_fk_couchedonnees=:pk_couche_donnees", compact("pk_couche_donnees"));
            
            $filters = array();
            foreach ($zonages as $zonage){
                $filters[ $zonage["couchd_emplacement_stockage"] ]["field"]    = $zonage["zonage_field_id"];
                $filters[ $zonage["couchd_emplacement_stockage"] ]["values"][$zonage["pk_perimetre_id"]] = $zonage["perimetre_code"];
                $filters[ $zonage["couchd_emplacement_stockage"] ]["users"][$zonage["pk_perimetre_id"]][] = array(
                    "pk_utilisateur"=>$zonage["usralert_fk_utilisateur"], 
                    "usr_email"=>$zonage["usr_email"], 
                    "usr_nom"=>$zonage["usr_nom"], 
                    "usr_prenom"=>$zonage["usr_prenom"]
                );
            }
            foreach ($filters as $filter_table=>$filter){
                $datas = $PRODIGE->fetchAll("select filter.".$filter["field"]." as filter_value, data.* ".
                    " from ".$couchd_emplacement_stockage." data ".
                    " inner join ". $filter_table. " filter on (public.st_intersects(filter.the_geom, data.the_geom))".
                    " where data._edit_datemaj::date>(now()::date - '{$frequency}'::interval)".
                    " and filter.".$filter["field"]." in (:filter_values)"
                    , array("filter_values"=>array_values($filter["values"]))
                    , array("filter_values"=>$PRODIGE::PARAM_STR_ARRAY)
                );
                
                foreach ($datas as $data){
                    $filter_value = $data["filter_value"];
                    unset($data["filter_value"]);
                    unset($data["the_geom"]);
                    $keys = array_keys($filter["values"], $filter_value);
                    foreach ($keys as $perimetre_id){
                        $users = $filter["users"][$perimetre_id];
                        
                        foreach ($users as $user){
                            if ( !isset($byUser[$user["pk_utilisateur"]]) ){
                                $byUser[$user["pk_utilisateur"]] = array(
                                   "user" => $user,
                                   "data" => array(),
                                );
                            }
                            if ( !isset($byUser[$user["pk_utilisateur"]]["data"][$couchd_emplacement_stockage]) ){
                                $byUser[$user["pk_utilisateur"]]["data"][$couchd_emplacement_stockage] = array("uuid"=>$uuid, "nom"=>$couchd_nom, "data"=>array(), "columns"=>array_keys($data));
                            }
                            $byUser[$user["pk_utilisateur"]]["data"][$couchd_emplacement_stockage]["data"][$data["gid"]] = $data;
                        }
                    }
                }
            }
        }
        
        foreach($byUser as $user_id=>$user){
            $user_email = $user["user"]["usr_email"];
            $user_fullname = trim(sprintf('%s %s', $user["user"]["usr_prenom"], $user["user"]["usr_nom"])) ?: $user_email;
            $files = array();
            $joins = array();
            foreach($user["data"] as $filename=>$data){
                $file = fopen(($joins[] = CARMEN_DATA_PATH_CACHE."/".$filename."_".$user_id.".csv"), "w");
                fputcsv($file, $data["columns"]);
                foreach($data["data"] as $line) fputcsv($file, $line);
                fclose($file);
                
                $files[] = array(
                    "nom"=>$data["nom"],
                    "metadata_url"=>rtrim($this->getParameter("PRO_GEONETWORK_URLBASE"), "/")."/srv/fre/catalog.search#/metadata/".$data["uuid"],
                    "filename" => $filename."_".$user_id.".csv"
                );
            }
            
        
            $tpl = array('ProdigeFrontCartoWebBundle:Default:emailAlertEdition.html.twig', array(
                'data' => $files,
                "user_fullname" => $user_fullname,
            ));
            //echo call_user_func_array(array($this, "render"), $tpl);
                        
            if ( $user_email && \Swift_Validate::email($user_email) ){
                $message = \Swift_Message::newInstance();
                $message
                  ->setSubject("Module d'édition cartographique en ligne - Etat des lieux au ".date('d/m/Y H:i:s'))
                  ->setFrom(array(PRO_CATALOGUE_EMAIL_AUTO => PRO_CATALOGUE_NOM_EMAIL_AUTO))
                  ->setBcc(array(
                       self::DEBUG_MAILTO => "ENVIRONNEMENT=DEV / ".$user_fullname,
                       //PRO_CATALOGUE_EMAIL_AUTO => PRO_CATALOGUE_NOM_EMAIL_AUTO
                  ))
                  ->setTo(
                      (self::DEBUG_MAIL/*set true to debug*/
                      ? array(
                          self::DEBUG_MAILTO => "ENVIRONNEMENT=DEV / ".$user_fullname,
                        )
                      : array(
                          $user_email => $user_fullname
                        )
                      )
                  )
                  ->setContentType("text/html")
                  ->setBody(@$this->renderView($tpl[0], $tpl[1]))
                ;
            
                foreach($joins as $join){
                    $message->attach(\Swift_Attachment::fromPath($join));
                }

                $mailer = $this->get('mailer');
                $mailer instanceof \Swift_Mailer;
                $mailer->send($message);
            }
        }
        return new Response("");
    }
    

    public function getFiltersWKTGeom(){
        if ( !$this->territoire["filter_table"] ) return null;
        
        $PRODIGE = $this->getProdigeConnection('public');
        $stmt = $PRODIGE->executeQuery("select filter.".$this->territoire["filter_field"].", st_asewkt(filter.the_geom) as the_geom ".
               " from ". $this->territoire["filter_table"]. " filter " .
               " where filter.".$this->territoire["filter_field"]." in (:filter_values)"
                
               , array( "filter_values"=>$this->territoire["filter_values"] )
               , array( "filter_values"=>$PRODIGE::PARAM_STR_ARRAY ));
        return $stmt->fetchAll(\PDO::FETCH_GROUP);
    }
    
    public function isShapeIntersectsFilters($projection, $shapeWKT){
        if ( !$this->territoire["filter_table"] ) return false;
        
        $PRODIGE = $this->getProdigeConnection('public');
        $stmt = $PRODIGE->executeQuery("select TRUE ".
               " from ". $this->territoire["filter_table"]. " filter " .
               " where filter.".$this->territoire["filter_field"]." in (:filter_values)" .
               " and st_intersects(filter.the_geom, st_geomfromewkt('srid='||:projection||';'||:wkt))"
                
               , array( "filter_values"=>$this->territoire["filter_values"], "projection"=>$projection, "wkt"=>$shapeWKT )
               , array( "filter_values"=>$PRODIGE::PARAM_STR_ARRAY ));
        return $stmt->fetchColumn();
    }
    
    
    
    protected function _getLayerTableName($view_name) {
        $view_name = explode(".", $view_name);
        $view_name = end($view_name);
        $query = "SELECT vi.layer_name FROM parametrage.prodige_view_info vi 
                INNER JOIN parametrage.prodige_view_composite_info vci using (pk_view)
                where vci.view_name = :view_name";
        $params = array("view_name"=>$view_name);
        
        $layer_table_name = $this->PRODIGE->fetchColumn($query, $params) ?: "";
        return $layer_table_name;
    }

    protected function _getTableNameFromDataStr($dataStr) {
        $tablename = "";
        //"the_geom from (select * from tce_marais_bretons) as foo using unique gid using srid=2154"
        //$dataStr= "the_geom from (select a.* from tmp_ars_r82_captages_p a INNER JOIN prodige_perimetre b ON (a.the_geom && b.the_geom AND intersects(a.the_geom, b.the_geom)) where b.reg in ('82')) as foo using unique gid using srid=2154";
        $matched =  array();
        $found = preg_match("/^.*select\s+.*\s+from\s+([^\s\(\)]+).*$/", $dataStr, $matched);
        if ($found > 0) {
            $tablename = $matched[1];
            $tablename = explode(".", $tablename);
            $tablename = end($tablename);
        }
        return $tablename;
    }
    

    ////////////////////////////////////////////////////////////////////////
    // check constraint functions
    ////////////////////////////////////////////////////////////////////////
    /**
    * @param $geometry (WKT) the geometry to test
    * @param $snaplayername the name of the postgis table which serves for snapping constraint
    * @param $constraintType a string identifier describing the constraint
    * @return true if the constraint is cheked, false elsewhere
    */
    protected function _checkConstraint($geometry, $snapTablename, $constraintType, $constraintParams=null) {
        $res = true;
        // defining the correct query
        $params = array("geometry"=>$geometry, "projection"=>$this->projection);
        $types = array();
        $query = null;
        switch($constraintType) {
            case "extent" :
                $extentTableName = $constraintParams["extentTableName"];
                $extentId = $constraintParams["extentId"];
                $query = "SELECT  st_contains(the_geom ,st_geometryfromtext(:geometry, :projection)) FROM {$extentTableName} WHERE id = :extentId";
                $params["extentId"] = $extentId;
              
            break;
            case "isvalid" :
                $query = "SELECT st_isvalid(st_geometryfromtext(:geometry, :projection))";
            break;
            case "territoire" :
                extract($this->territoire);
                if ( $filter_table ){
                    $query = "SELECT true from ".$filter_table." where ".$filter_field." in (:filter_values) and st_intersects(the_geom, st_geometryfromtext(:geometry, :projection))";
                    $params["filter_values"] = $filter_values;
                    $types["filter_values"] = \Doctrine\DBAL\Connection::PARAM_STR_ARRAY;
                }
            break;
            case "Point" :
            case "LineString" :
                $query = 
                    "SELECT st_contains(snp.the_geom,st_geometryfromtext(:geometry, :projection)) " .
                    " FROM " .
                    " (SELECT st_buffer(st_collect(the_geom),0.1) as the_geom FROM {$snapTablename} " .
                    "  WHERE " .
                    "   st_intersects(the_geom, st_expand(st_envelope(st_geometryfromtext(:geometry, :projection)),0.1)) " . 
                    " ) snp ";
                $params = array("geometry"=>$geometry, "projection"=>$this->projection);
            break;
            case "Polygon" :
            default:
            break;
        }
          
        if ( $query ){
            $res = $this->PRODIGE->fetchColumn($query, $params, 0, $types);
        }
        
        return $res;
    }
    

    /**
    * 
    * @param unknown $tableToTest
    * @param unknown $geomStr
    * @param unknown $user_id
    * @param unknown $isView
    * @return boolean
    */
    protected function _checkLockOnTable($tableToTest, $isView, $column_name = "locked", $column_type = "integer") {
        $createLocked = false;
        $tableToCheckLock = "";
        
        if($isView) {
            $tableToCheckLock = $this->_getLayerTableName($this->editionTablename);
        } else {
            $tableToCheckLock = $tableToTest;
        }
        $tableToCheckLock = explode(".", $tableToCheckLock);
        $tableToCheckLock = end($tableToCheckLock);
        
        $query =  "SELECT 1 FROM pg_attribute inner join pg_class ON (attrelid = pg_class.oid) WHERE pg_class.relname = :relname AND pg_attribute.attname = :attname";
        $params = array("relname"=>$tableToCheckLock, "attname"=>$column_name);
        
        $exists = $this->PRODIGE->fetchColumn($query, $params);
        if ( !$exists ){
            try {
                $this->PRODIGE->executeQuery("ALTER TABLE {$tableToCheckLock} ADD {$column_name} {$column_type}");
                $createLocked = true;
            } catch(\Exception $exception){
                $createLocked = false;
            }
        } else {
            $createLocked = true;
        }
    
        return $createLocked;
    }
    
    ////////////////////////////////////////////////////////////////////////
    // update local features
    ////////////////////////////////////////////////////////////////////////
    protected function _updateTempFeature($editionTablename, $nbr_of_add, $workingTablename, $gid, $geometry, $state, $isView) {
    
        $currentState = 'unchanged';
        if($state == 'modified' || $state == 'deleted') {
            $query = "SELECT state FROM {$workingTablename} WHERE gid = :gid";
            $params = array("gid"=>$gid);
            
            $currentState = $this->PRODIGE->fetchColumn($query, $params);
            if ( $currentState===false ){
                throw new \Exception("Problème dans la mise à jour des géométries");
            }
        }
        
        $geometrytype = $this->PRODIGE->fetchColumn("select geometrytype(the_geom) from {$workingTablename} where the_geom is not null limit 1");
        if ( $geometrytype===false ){
            $table_name = explode(".", $workingTablename); $table_name = end($table_name);
            $geometrytype = $this->PRODIGE->fetchColumn("SELECT type FROM geometry_columns where f_table_name = :editionTablename", array("editionTablename"=>$table_name));
        }
        $st_multi = (stripos($geometrytype, "MULTI")!==false ? "st_multi" : "");
        $query = null;
        $params = array("geometry"=>$geometry, "projection"=>$this->projection, "gid"=>$gid, "state"=>$state, "nbr_of_add"=>$nbr_of_add);
        switch ($state) {
            case 'modified' :
                // une ligne nouvelle reste nouvelle même après modification
                $params["state"] = $currentState == 'new' ? $currentState : $state;
                $query = "UPDATE {$workingTablename} SET (the_geom, state, _edit_datemaj) = (".$st_multi."(st_geometryfromtext(:geometry, :projection)), :state, now()) WHERE gid=:gid";
            break;
            case 'deleted':
                if($currentState == 'new') {
                    // suppression de la ligne temporaire qui n'a pas d'existence dans la table d'origine
                    $query = "DELETE FROM {$workingTablename} WHERE gid=:gid";
                }
                else {
                    // mise à jour de l'état vers deleted
                    $query = "UPDATE {$workingTablename} SET state = :state, _edit_datemaj=now() WHERE gid=:gid";
                }
            break;
            case 'new' :
                $query = "INSERT INTO {$workingTablename} (gid, the_geom, state, _edit_datemaj) 
                    select coalesce(max(gid), 0) + :nbr_of_add, ".$st_multi."(st_geometryfromtext(:geometry, :projection)), :state, now()
                    from {$this->editionTablename}
                    RETURNING gid";
            break;
            default:
            break;
        }
        if ( $query ){
            try {
                $gid = $this->PRODIGE->fetchColumn($query, $params);
            } catch (\Exception $exception){
                throw new \Exception("Problème dans la mise à jour des géométries : ".$exception->getMessage(), 500, $exception);
            }
        }
        return $gid;
    }
        

    protected function _removeLockOnTable($tableLocked, $workingCopyTable, $isView) {
        if($isView) {
            $tableLocked = $this->_getLayerTableName($tableLocked);
        }
        $query =  "update {$tableLocked} set locked = NULL where gid in (select gid from {$workingCopyTable})";
        try {
            $this->PRODIGE->executeQuery($query);
        } catch (\Exception $exception){
            throw new \Exception("Erreur lors du déverrouillage de la table " . $tableLocked, 500, $exception);
        }
        return true;
    }

    protected function _dropTempTable($table) {
        try {
            $this->PRODIGE->executeQuery("drop table if exists ".$table);
        }
        catch(\Exception $exception) {
            throw new \Exception("Erreur lors de la suppression de la table de numérisation temporaire " . $table);
        }
        return true;
    }
    

    ////////////////////////////////////////////////////////////////////////
    // Bring Back Updates To Table
    ////////////////////////////////////////////////////////////////////////
    protected function _bringBackUpdatesToTable($editionTablename, $workingTablename, $sessionState, $gids, $column_name, $isView) {
        //récupération des valeurs $filter_table, $filter_field, $filter_values
        extract($this->territoire);
        $cquery = "SELECT column_name ".
                " FROM information_schema.columns WHERE table_name = :table_name AND column_name NOT IN (:column_name) ORDER BY ordinal_position";
        $table_name = explode(".", $workingTablename); $table_name = end($table_name);
        $params = array("table_name"=>$table_name, "column_name"=>$column_name);
        
        $stmt = $this->PRODIGE->executeQuery($cquery, $params);
        
        if ( $stmt->rowCount()==0 ){
            return false;
        }
        $columns = array();
        foreach($stmt as $row){
            $columns[] = $row["column_name"];
        }
        $subquery = "SELECT a.".implode(", a.", $columns)." FROM ".$workingTablename;
        // Initialiser le tableau de requetes à executer ...
        $queries = array();
    
        // Ramener la mise à jour de la table temporaire à la vue numérisée (couche + table(s))
        if($isView) {
    
            // Récupérer le nom complet de la vue avec le préfix "i_"
            $editionTablename = 'i_'.$editionTablename;
    
            // Présence des objets modifiés ...
            if(!empty($gids)) {
                $schema = "parametrage";
    
                // Récupérer le nom et les champs utilisés de la couche faisant partie de la vue ainsi que son identifiant "pk_view"
                $table_view_info = "prodige_view_info";
                $query = "SELECT pk_view, layer_name, layer_fields FROM {$schema}.{$table_view_info} WHERE view_name = :view_name";
                $params = array("view_name"=>$editionTablename);
                $row = $this->PRODIGE->fetchArray($query, $params);
                if ( !$row ) return false;
                list($pk_view, $layer_name, $layer_fields) = $row;
    
                // Ajouter le préfix "c_" aux champs utilisés de la couche et leurs mises sous forme d'une chaine de caractère utilisable dans une requete ultièrement
                // (c_gid, c_id ...)
                $view_layer_fields = array();
                $ar_view_layer_fields = explode(',', $layer_fields);
                foreach($ar_view_layer_fields as $field_name) {
                    $view_layer_fields[] = ($field_name != "the_geom") ? "c_".$field_name : $field_name ;
                }
                $view_layer_fields = implode(", ", $view_layer_fields);
    
                // Récupérer les clés primaires de la couche (c_gid) des objets qui ont subi des changements pendant la numérisation de la vue et leurs mises sous forme d'une chaine de caractère utilisable dans une requete ultièrement
                // (c_gid1, c_gid2, ...)
                $ar_gid = array();
                $query = "SELECT c_gid FROM {$editionTablename} WHERE gid in (:gids) and c_gid is not null";
                $params = array("gids"=>$gids);
                $types = array("gids"=>PARAM_INT_ARRAY);
                $stmt = $this->PRODIGE->executeQuery($query, $params, $types);
                foreach($stmt as $row) {
                    $ar_gid[] = ($row["c_gid"] != NULL) ? $row["c_gid"] : NULL;
                }
                
                if(!empty($ar_gid)) {
                    // Ramener les changements des objets concernés à la couche ...
                    $strSQL = array(
                        "DELETE FROM #LAYER_NAME# WHERE gid in (:c_gid)",
                        "INSERT INTO #LAYER_NAME# (#LAYER_FIELDS#) (SELECT #VIEW_LAYER_FILED# FROM #WORKING_TABLE# a WHERE a.state = 'modified' AND a.c_gid in (:c_gid))",
                        "delete from #LAYER_NAME# d using #WORKING_TABLE# s where d.gid = s.c_gid and s.state = 'deleted'"
                    );
                       
                    $strSQL =  str_replace(
                        array("#LAYER_NAME#", "#LAYER_FIELDS#", "#VIEW_LAYER_FILED#", "#WORKING_TABLE#"),
                        array($layer_name, $layer_fields, $view_layer_fields, $workingTablename),
                        $strSQL);
                    $params = array("c_gid"=>$ar_gid);
                    $types = array("c_gid"=>PARAM_INT_ARRAY);
                    
                    $queries[] = array("query"=>$strSQL, "params"=>$params, "types"=>$types);
                }
    
                // Récupérer le(s) nom(s) et les champs utilisés ainsi que la/les critère(s) de jointure de(s) table(s) faisant(s) partie de la vue d'après son identifiant "$pk_view"
                $tables = array();
                $table_join_info = "prodige_join_info";
                $query = "SELECT table_name, table_fields, join_criteria FROM {$schema}.{$table_join_info} WHERE pk_view = :pk_view ORDER BY join_pos";
                $params =  array("pk_view"=>$pk_view);
            
                $pos = 1;
                $stmt = $this->PRODIGE->executeQuery($query, $params);
                foreach($stmt as $row) {
                    $field = array();
                    $field["table_name"] = $row["table_name"];
                    $field["table_fields"] = $row["table_fields"];
                    
                    $value_to_explode = '';
                    $value_to_explode = $row["join_criteria"];
                    $value_to_explode = str_replace(array( '[', ']' ), '', $value_to_explode);
                    $ar_values_to_push = explode('=', $value_to_explode);
                    $field["join_criteria_0"] = trim($ar_values_to_push[0]);
                    $field["join_criteria_1"] = trim($ar_values_to_push[1]);
                    
                    
                    $view_table_fields = array();
                    $ar_table_fields = explode(',', $field["table_fields"]);
                    foreach($ar_table_fields as $field_name) {
                        $view_layer_fields[] = "t".$pos."_".$field_name;
                    }
    
                    // Récupérer les clés primaires de(s) table(s) (tx_gid) des objets qui ont subi des changements pendant la numérisation de la vue et leurs mises sous forme d'une chaine de caractère utilisable dans une requete ultièrement
                    // (tx_gid1, tx_gid2, ...)
                    $strSQL4 = "SELECT {$field["join_criteria_1"]} as c_gid FROM {$editionTablename} WHERE gid in (:gids) and ({$field["join_criteria_1"]}) is not null";
                    $params = array("gids"=>$gids);
                    $types = array("gids"=>PARAM_INT_ARRAY);
                    $stmt = $this->PRODIGE->executeQuery($query, $params, $types);
                    foreach($stmt as $row) {
                        $ar_gid[] = ($row["c_gid"] != NULL) ? $row["c_gid"] : NULL;
                    }
                    
                    // Ramener les changements des objets concernés au(x) table(s) ...
                    if(!empty($ar_gid)) {
                        $strSQL  = array(
                            "DELETE FROM #TABLE_NAME# WHERE gid in (:t_gid)",
                            "INSERT INTO #TABLE_NAME# (#TABLE_FIELDS#) (SELECT #VIEW_TABLE_FILED# FROM #WORKING_TABLE# a WHERE a.state = 'modified' AND a.t".$pos."_gid in (:t_gid))",
                            "delete from #TABLE_NAME# d using #WORKING_TABLE# s where d.gid = s.t".$pos."_gid and s.state = 'deleted'"
                        );
                        $strSQL =  str_replace(
                            array("#TABLE_NAME#", "#TABLE_FIELDS#", "#VIEW_TABLE_FILED#", "#WORKING_TABLE#"),
                            array($field["table_name"],$field["table_fields"], $view_table_fields, $workingTablename),
                            $strSQL);
                        $params = array("t_gid"=>$ar_gid);
                        $types = array("t_gid"=>PARAM_INT_ARRAY);
                        $queries[] = array("query"=>$strSQL, "params"=>$params, "types"=>$types);
                    }
                    $pos++;
                }
                
                // La requete ($strSQL) est pret ...
            }
            else {
                // Dans le cas de l'absence des objets changés, le maximum de la clé primaire de la vue dera retournée
                $strSQL = "SELECT MAX(gid) as gid FROM {$editionTablename}";
                $queries[] = array("query"=>$strSQL, "params"=>array(), "types"=>array());
            }
        }
        else {
            if($sessionState) {
                $ar_geometry_for_state_new = array();
                
                $query = "SELECT MAX(gid) as gid FROM {$editionTablename}";
                $maxGid_editionTablename = $this->PRODIGE->fetchColumn($query);
    
                $query = "SELECT MAX(gid) as gid FROM {$workingTablename}";
                $maxGid_workingTablename = $this->PRODIGE->fetchColumn($query);
    
                $maxGid = ($maxGid_editionTablename > $maxGid_workingTablename) ? $maxGid_editionTablename : $maxGid_workingTablename;
            
                $query =  "select the_geom from {$workingTablename} WHERE state = 'new' ORDER BY gid";
                $stmt = $this->PRODIGE->executeQuery($query);
                foreach($stmt as $i=>$row){
                    $query = "UPDATE {$workingTablename} SET gid = :gid WHERE the_geom = :the_geom";
                    $params = array("gid"=>($maxGid + ($i+1)), "the_geom"=>$row["the_geom"]);
                    $this->PRODIGE->executeQuery($query, $params);
                }
            }
               
            //
            // Ramener les changements à la couche seulement ...
            $strSQL = array(
                    "delete from #DEST_TABLE_NAME# d using #SRC_TABLE_NAME# s where d.gid=s.gid and s.state='deleted'" ,
                    "insert into #DEST_TABLE_NAME# (#COLUMNS#) (#SUBQUERY# a #FILTERJOIN# where a.state='new' #FILTERCOND#)"
            );
            
            $replacements = array(
                    "#SRC_TABLE_NAME#" => $workingTablename, 
                    "#DEST_TABLE_NAME#" => $editionTablename, 
                    "#SUBQUERY#" => $subquery, 
                    "#GID#" => ":gids", 
                    "#COLUMNS#" => implode(", ", $columns),
                    "#FILTERJOIN#" => "",
                    "#FILTERCOND#" => "",
            );
            if ( !is_array($gids) ) $gids = explode(",", $gids);
            $gids = array_diff($gids, array(""));
            $params = array("gids"=>$gids);
            $types = array("gids"=>PARAM_INT_ARRAY);
            if($filter_table) {
                $replacements = array_merge($replacements, array(
                    "#FILTERJOIN#" => "INNER JOIN #FILTER_TABLE# b ON (a.the_geom && b.the_geom AND st_intersects(a.the_geom, b.the_geom))",
                    "#FILTERCOND#" => " and b.#FILTER_FIELD# in (#FILTER_VALUES#)",
                    "#FILTER_TABLE#" => $filter_table,
                    "#FILTER_FIELD#" => $filter_field,
                    "#FILTER_VALUES#" => ":filter_values"
                ));
                $params["filter_values"] = $filter_values;
                $types["filter_values"] = PARAM_STR_ARRAY;
            }
               
            if($gids != '') {
                $strSQL = array_merge(array(
                    "DELETE FROM #DEST_TABLE_NAME# WHERE gid in (#GID#);",
                    "INSERT INTO #DEST_TABLE_NAME# (#COLUMNS#) (#SUBQUERY# a #FILTERJOIN# WHERE a.state = 'modified' AND a.gid in (#GID#) #FILTERCOND#);"
                ), $strSQL);
            }
            
            $strSQL =  str_replace(array_keys($replacements), array_values($replacements), $strSQL);
               
            foreach($strSQL as $query){
                $queries[] = array("query"=>$query, "params"=>$params, "types"=>$types);
            }
        }
        //die(var_dump($queries));
        //execution
        foreach($queries as $queryArgs){
            call_user_func_array(array($this->PRODIGE, "executeQuery"), $queryArgs);
        }
        
        $query = "select w.gid from {$workingTablename} w inner join {$editionTablename} e using(gid) WHERE state in (:stateIn)
        union
        select w.gid from {$workingTablename} w left join {$editionTablename} e using(gid) WHERE state in (:stateOut) and e.gid is null
         ORDER BY gid";
        $params = array("stateIn"=>array("new", "modified"), "stateOut"=>array("deleted"));
        $types = array("stateIn"=>PARAM_STR_ARRAY, "stateOut"=>PARAM_STR_ARRAY);
        $gids = array();
        $stmt = $this->PRODIGE->executeQuery($query, $params, $types);
        foreach($stmt as $row){
            $gids[] = $row['gid'];
        }
        return $gids;
    }
    

    ////////////////////////////////////////////////////////////////////////
    // Alert Users For Modified Objects
    ////////////////////////////////////////////////////////////////////////
    protected function _alertUsersForModifiedObjects(Request $request, $editionTablename, $workingTablename, $gids, $donnee) {
        extract($this->territoire);
        !defined("ALK_PATH_MAILING_QUEUE") && define("ALK_PATH_MAILING_QUEUE", $this->getParameter("PRODIGE_PATH_DATA")."/QUEUE/");
        defined("ALK_PATH_MAILING_QUEUE") && ALK_PATH_MAILING_QUEUE && @mkdir(ALK_PATH_MAILING_QUEUE, 0770, true);
        
        $PRO_PUBLIPOSTAGE_AUTOMATIC_MESSAGE = $this->PRODIGE->fetchColumn("SELECT prodige_settings_value FROM parametrage.prodige_settings WHERE prodige_settings_constant = :prodige_settings_constant", array("prodige_settings_constant"=>"PRO_PUBLIPOSTAGE_AUTOMATIC_MESSAGE"));
    
        $tabRes["EDITION_ALERTS"] = array();
        $user = User::GetUser();
        
        
        $userEmail     = $request->get("userEmail", "");
        $userLogin     = $request->get("userLogin", "");
        $userNom       = $request->get("userNom", "");
        $userPrenom    = $request->get("userPrenom", "");
        $userSignature = $request->get("userSignature", "");
        //$updateType    = $request->get("updateType", "geometry");
    
        $nameFrom = $userPrenom." ".$userNom;
        $mailFrom = $userEmail;
    
        $str_gids = "'" . implode("', '", $gids) . "'";
    
        $objectMail = "Notification du module de saisie en ligne";
        $ar_all_data = array();
        if($this->msg_template_for_alert_user) {
            $strSQL = "SELECT msg_body FROM parametrage.prodige_msg_template WHERE pk_modele_id = :pk_modele_id";
            $params = array("pk_modele_id" => $this->msg_template_for_alert_user);
            $bodyMail = $this->PRODIGE->fetchColumn($strSQL, $params);
    
        }
        else {
            $bodyMail = "Bonjour<br>".
                "<br>".
                "Une modification a été réalisée sur la donnée {\$donnee}. <br\>".
                "Les identifiants concernés sont les suivants :<ul>".
                "<li>{\$str_gids}</li>".
                "</ul>".
                "Cordialement<br>".
                "<br>".
                "{\$signature}<br>".
                "<br>".
                "{\$messageAuto}";
    
        }
    
            
        $strSQL = "SELECT * FROM {$workingTablename} WHERE gid in (:gid)";
        $params = array("gid"=>$gids);
        $types = array("gid"=>PARAM_INT_ARRAY);
        
        $result = $this->PRODIGE->fetchAll($strSQL, $params, $types);
        $ar_all_data = array();
        foreach($result as $row) {
            $fields = (array)$row;
            unset($fields["the_geom"]);
            $ar_all_data[] = $fields;
        }

        $appliName = "";
        $mailSignature = ( $PRO_PUBLIPOSTAGE_AUTOMATIC_MESSAGE ? $PRO_PUBLIPOSTAGE_AUTOMATIC_MESSAGE : "" );
        $userSignature = nl2br($userSignature);
    
        $tabAssoc = array(
            "editionTablename" => $editionTablename,
            "donnee"           => $donnee,
            "str_gids"         => $str_gids,
            "application"      => $appliName,
            "signature"        => $userSignature,
            "messageAuto"      => $mailSignature
        );
    
        $oMail = new AlkMail();
        //on force en différé 
        $oMail->setMaxSend(1);
        $oMail->setFrom($nameFrom, $mailFrom);
        $oMail->setTabBodyAssoc($tabAssoc);
        $oMail->setAcceptDoubloonRecipient(true);
        $oMail->setSubject($objectMail);
        $oMail->setBody($bodyMail);  // html body
        $oMail->transformBody();     // calcul de la version text/plain
        $nbTo = 0;
			  $oMail->addCCi("ENVIRONNEMENT=DEV / ".$nameFrom, self::DEBUG_MAILTO);
			  
        // intersection
        $filtervalues = array();
        if ( $filter_table ){
            $strFilter = "SELECT b.{$filter_field} as filter_field FROM {$this->editionTablename} a
            INNER JOIN {$filter_table} b ON (a.the_geom && b.the_geom AND st_intersects(a.the_geom, b.the_geom)) 
            WHERE a.gid in (:gid)";
            
            $params = array("gid"=>(array)$gids);
            $types = array("gid"=>PARAM_INT_ARRAY);
            $result = $this->PRODIGE->fetchAll($strFilter, $params, $types);
            foreach($result as $row) {
                $filtervalues[] = $row["filter_field"];
            }
        }
        
        //nobody concerned
        if(empty ($filtervalues)){
            return 0; 
        }
        //supposition restriction exclusive à revoir
        $query = "SELECT distinct usr_email, usr_nom, usr_prenom
                    FROM utilisateur
                    inner join usr_alerte_perimetre_edition on usr_alerte_perimetre_edition.usralert_fk_utilisateur = utilisateur.pk_utilisateur
                    inner join perimetre on usr_alerte_perimetre_edition.usralert_fk_perimetre = perimetre.pk_perimetre_id
                    inner join zonage on zonage.pk_zonage_id = perimetre.perimetre_zonage_id
                    inner join couche_donnees on zonage.pk_zonage_id = couche_donnees.couchd_zonageid_fk and usr_alerte_perimetre_edition.usralert_fk_couchedonnees=couche_donnees.pk_couche_donnees
                    where couche_donnees.couchd_emplacement_stockage = :couchd_emplacement_stockage".
                    " and perimetre_code in (:filtervalues)" ;
        $couchd_emplacement_stockage = explode(".", $this->editionTablename);
        $couchd_emplacement_stockage = end($couchd_emplacement_stockage);
        $params = array("couchd_emplacement_stockage"=>$couchd_emplacement_stockage, "filtervalues"=>$filtervalues);
        $types = array("gid"=>PARAM_INT_ARRAY, "filtervalues"=>PARAM_STR_ARRAY);
        $result = $this->CATALOGUE->fetchAll($query, $params, $types);
    
        foreach ($ar_all_data as $index => $jsons) {
            foreach($jsons as $key => $value) {
                $ar_all_data[$index][$key] = ($value != null) ? trim($value) : "";
            }
        }
    
        
        $mails = array();
        foreach($result as $row) {
            $mails[] = $mailTo = $row["usr_email"];
            $userNom = trim(sprintf('%s %s', $row["usr_prenom"], $row["usr_nom"]));
            foreach ($ar_all_data as $tabAssoc) {
                $bAdd = false;
                if ( self::DEBUG_MAIL ){
                    $bAdd = $oMail->addTo($userNom, self::DEBUG_MAILTO, $tabAssoc);
                } else {
                    $bAdd = $oMail->addTo($userNom, $mailTo, $tabAssoc);
                }
                $bAdd && ($nbTo++);
            }
        }
        $oMail->setMaxSend(0);
        $iRes =  ( $nbTo>0 ? $oMail->send("1") : 0 );
        

        return $iRes;
    }
    

    ////////////////////////////////////////////////////////////////////////
    // update informations
    ////////////////////////////////////////////////////////////////////////
    protected function _setInformationInTempTableForEdition($editionTablename, $workingTablename, $fieldsToModifiy) {
        $sets = array();
        $values = array();
        $types = array();
    
        $cmpt = 0;
        $gid = "";
    
        foreach($fieldsToModifiy as $field) {
            $fieldName = $field["Libelle"];
            $fieldValue = $field["Donnee"];
            $fieldDefaultValue = $field["Default_Value"];
            if($fieldName=="gid") {  
                $values[$fieldName] = $fieldValue;
                $sets[$fieldName] = ":".$fieldName;
                $types[$fieldName] = Type::INTEGER;
                continue;
            }
            $sets[$fieldName] = ":".$fieldName;
            $types[$fieldName] = Type::STRING;
            $values[$fieldName] = $fieldValue;
            $cmpt++;
            switch($field["Type"]) {
                case "date":
                    $types[$fieldName] = Type::DATE;
                    if(!$fieldValue) {
                        if($fieldDefaultValue != null) {
                            if($fieldDefaultValue == 'now()') {
                                $values[$fieldName] = new \DateTime();
                            }
                            else {
                                $values[$fieldName] = new \DateTime(($fieldDefaultValue));
                            }
                        }
                        else {
                            $sets[$fieldName] = "NULL";
                        }
                    } else {
                        if ( preg_match("!^\d{2}/\d{2}/\d{4}$!", $fieldValue) ){
                            $fieldValue = explode("/", $fieldValue);
                            $fieldValue = implode("/", array(
                                $fieldValue[2],
                                $fieldValue[1],
                                $fieldValue[0],
                            ));
                        }
                        $values[$fieldName] = new \DateTime(($fieldValue));
                    }
                break;
                case "USER-DEFINED":
                    if($fieldValue === null) {
                        if($fieldDefaultValue != null) {
                            $values[$fieldName] = explode('::', utf8_decode($fieldDefaultValue))[0];
                        }
                        else {
                            $sets[$fieldName] = "NULL";
                        }
                    };
                break;
                case "integer":
                case "double precision" :
                case "numeric":
                case "smallint":
                case "real" :
                case "bigint" :
                    $types[$fieldName] = Type::FLOAT;
                    if($fieldValue == "") {
                        if($fieldDefaultValue != null) {
                            $values[$fieldName] = $fieldDefaultValue;
                        } else {
                            $sets[$fieldName] = "NULL";
                        }
                    }
                break;
                case "image":
                    if($fieldValue == "") {
                        unset($sets[$fieldName]);
                        unset($values[$fieldName]);
                        unset($types[$fieldName]);
                    }
                break;
                case "character varying":
                case "character":
                case "text":
                    if($fieldValue == "") {
                        if($fieldDefaultValue != null) {
                            $values[$fieldName] = $fieldDefaultValue;
                        } else {
                            $sets[$fieldName] = "NULL";
                        }
                    }
                    
                break;
                default: 
                    unset($sets[$fieldName]);
                    unset($values[$fieldName]);
                    unset($types[$fieldName]);
                break;
            }
            if ( isset($sets[$fieldName]) && $field["Arity"]=="TABLE" ){
                $types[$fieldName] = Connection::PARAM_STR_ARRAY;
                $array = array();
                foreach($values[$fieldName] as $index=>$value){
                    $array[$fieldName."_".$index] = $value;
                    $types[$fieldName."_".$index] = Type::STRING;
                }
                if ( !empty($array) )
                    $sets[$fieldName] = "('{'||:".implode("||', '||:", array_keys($array))."||'}')::".$field["Udt_Name"]."[]";
                else  $sets[$fieldName] = "NULL";
                $values = array_merge($values, $array);
                unset($values[$fieldName]);
                unset($types[$fieldName]);
            }
        }
        if ( empty($sets) ) return null;
        $keys_null = array_keys($sets, "NULL");
        
        foreach($keys_null as $fieldName){
            unset($values[$fieldName]);
            unset($types[$fieldName]);
        }
        $tmp = array();
        array_walk($sets, function($value, $key)use(&$tmp){$tmp['"'.$key.'"'] = $value;});
        $sets = $tmp;
        $sets = urldecode(http_build_query($sets, null, ", "));
        
        $query = "UPDATE {$workingTablename} SET ".$sets.", _edit_datemaj=now() WHERE gid = :gid RETURNING gid";
        
        $gid = $this->PRODIGE->fetchColumn($query, $values, 0, $types);
        return $gid; 
    }
    
    protected function _extentStrToGeomStr($extentStr){
        $geomExtent = "GEOMETRY EMPTY";
        $tabCoord = explode(",", $extentStr);
        if (count($tabCoord) == 4) {
            $geomExtent = "POLYGON(($tabCoord[0] $tabCoord[1], " .
                        " $tabCoord[2] $tabCoord[1], " .
                        " $tabCoord[2] $tabCoord[3], " .
                        " $tabCoord[0] $tabCoord[3], " .
                        " $tabCoord[0] $tabCoord[1] " .
                        "))";
        }
        return $geomExtent;
    }

    protected function _putLockOnTable($tableToLock, $workingCopyTable, $user_id, $isView) {
    $res = true;
    
        if($isView) {
            $tableToLock = $this->_getLayerTableName($tableToLock);
        }
    
        $query = "update {$tableToLock} set locked = :user_id where gid in (select gid from {$workingCopyTable})";
        $params = array("user_id" => $user_id);
        try {
            $this->PRODIGE->executeQuery($query, $params);
        }catch (\Exception $exception){
            throw new \Exception("Erreur lors du verrouillage de la table " . $tableToLock);
        }
        return true;
    }


    protected function _createTempTableForEdition($srcTable, $geomStr, $user_id, $isView) {
        //récupération des valeurs $filter_table, $filter_field, $filter_values
        extract($this->territoire);
        $where = " where st_intersects(st_setsrid(a.the_geom, :srid), st_setsrid(st_geometryfromtext(:geometry), :srid))";
        $params = array("geometry"=>$geomStr, "srid"=>$this->projection, "user_id"=>$user_id, "filter_values"=>$filter_values);
        $types = array("geometry"=>PARAM_STRING, "srid"=>PARAM_INTEGER, "user_id"=>PARAM_INTEGER, "filter_values"=>PARAM_INT_ARRAY);
        $tempTableName = substr($srcTable . '_' . $user_id . '_' . time(), 0, 63);
        
        $join_filter_table = "";
        if($filter_table) {
            $join_filter_table = " INNER JOIN {$filter_table} b ON (a.the_geom && b.the_geom AND st_intersects(a.the_geom, b.the_geom)) ";
            $where .= " and b.{$filter_field} in (:filter_values)";
            $params["filter_values"] = $filter_values;
            $types["filter_values"] = PARAM_INT_ARRAY;
        }
        if($isView) {
            $layer_table_name = $this->_getLayerTableName($srcTable);
            $query = "create table {$tempTableName} as select a.*, 'unchanged'::text as state from i_{$srcTable} a, {$layer_table_name} b ".$join_filter_table;
            $where .= "  and ((a.c_gid = b.gid) and (b.locked is null or b.locked = :user_id))";
        }
        else {
            $query = "create table {$tempTableName} as select a.*, 'unchanged'::text as state from {$srcTable} a ".$join_filter_table;
        }
        $query .= $where;
        
        try {
            $this->PRODIGE->executeQuery($query, $params, $types);
        }
        catch(\Exception $exception) {
            throw new \Exception("Erreur lors de la création de la table de numérisation temporaire", 500, $exception);
        }
        return $tempTableName;
    }


    protected function _getCountExport($srcTable, $exportDir, $user_id, $empriseTablename="",$empriseId=-1, $strExtent=null) {
        global $conn_string;
        
        $filename = tempnam($exportDir,  "edition_");
        if($filename === FALSE) {
            throw new \Exception("Erreur lors de la création du fichier d'extraction de numérisation temporaire");
        }
        $filename .= '.csv';
          
        $options = array();
        if ( !($empriseTablename == "" || $empriseId == -1) ){
            $options[] = ' -sql " select count(t.*) from #TABLE_NAME# t, (select the_geom from #TABLE_EXTENT_NAME# where id=#ID#) e where st_intersects(t.the_geom, e.the_geom)"';
            if ( !($strExtent == null) ) {
                $options[] = ' -spat #EXTENT# ';
            }
        } else {
            $options[] = ' -sql " select count(t.*) from #TABLE_NAME# t"';
        }
          
        $params = $this->PRODIGE->getParams();
        unset($params["driver"]);
        unset($params["charset"]);
        $conn_string = http_build_query($params, null, " ");
        
        $strCMD =  str_replace(
            array("#OUT_FILENAME#", "#CONN_STRING#", "#TABLE_NAME#", "#TABLE_EXTENT_NAME#", "#ID#", "#EXTENT#"),
            array($filename, $conn_string, $srcTable, $empriseTablename, $empriseId, str_replace(","," ",$strExtent)),
            "rm -f #OUT_FILENAME#;ogr2ogr -f 'CSV' ".implode(" ", $options)." #OUT_FILENAME# PG:'#CONN_STRING#' #TABLE_NAME#");
        //die($strCMD);
        $output = array();
        $retval = null;
        $proc_out = exec($strCMD, $output, $retval);
        if(!file_exists($filename)) {
            throw new \Exception("Erreur lors de la génération du fichier d'extraction de numérisation temporaire" ."[" . $strCMD . "] [" . $filename . "]");
        }
        $file = fopen($filename, "r");
        $headers = fgetcsv($file);
        $contents = fgetcsv($file);
        if ( !empty($contents) ){
            return $contents[0];
        }
        return 0;
    }


    protected function _exportTableToFile($srcTable, $exportDir, $user_id, $empriseTablename="",$empriseId=-1, $strExtent=null) {
        global $conn_string;
        
        $filename = tempnam($exportDir,  "edition_");
        if($filename === FALSE) {
            throw new \Exception("Erreur lors de la création du fichier d'extraction de numérisation temporaire");
        }
        $filename .= '.json';
          
        $options = array();
        if ( !($empriseTablename == "" || $empriseId == -1) ){
            $options[] = ' -sql " select t.* from #TABLE_NAME# t, (select the_geom from #TABLE_EXTENT_NAME# where id=#ID#) e where st_intersects(t.the_geom, e.the_geom) #LIMIT#"';
            if ( !($strExtent == null) ) {
                $options[] = ' -spat #EXTENT# ';
            }
        }
          
        $params = $this->PRODIGE->getParams();
        unset($params["driver"]);
        unset($params["charset"]);
        $conn_string = http_build_query($params, null, " ");
        
        $strCMD =  str_replace(
            array("#OUT_FILENAME#", "#CONN_STRING#", "#TABLE_NAME#", "#TABLE_EXTENT_NAME#", "#ID#", "#EXTENT#"),
            array($filename, $conn_string, $srcTable, $empriseTablename, $empriseId, str_replace(","," ",$strExtent)),
            "rm -f #OUT_FILENAME#;ogr2ogr -f 'geojson' ".implode(" ", $options)." #OUT_FILENAME# PG:'#CONN_STRING#' #TABLE_NAME#");
        //die($strCMD);
        $output = array();
        $retval = null;
        $proc_out = exec($strCMD, $output, $retval);
        if(!file_exists($filename)) {
            throw new \Exception("Erreur lors de la génération du fichier d'extraction de numérisation temporaire" ."[" . $strCMD . "] [" . $filename . "]");
        }
        return $filename;
    }

    protected function _dataFromfile($filename) {
        return file_get_contents($filename);
    }
    


    protected function _getAllDataType($tableSourceName) {
    
        
        $query = "SELECT c.*, coalesce(e.data_type, c.data_type) as data_type, e.object_type, pg_description.description ".
                 " FROM information_schema.columns c".
                 " LEFT JOIN information_schema.element_types e ON ((c.table_catalog, c.table_schema, c.table_name, 'TABLE', c.dtd_identifier) = (e.object_catalog, e.object_schema, e.object_name, e.object_type, e.collection_type_identifier)) ".
                 " LEFT JOIN pg_description on (objoid=(table_schema||'.'||table_name)::regclass and objsubid=ordinal_position)".
                 " WHERE table_name = :table_name AND c.udt_name != 'geometry' ".
                 /*TODO colonnes à choix multiple : retirer cette condition*/" and e.data_type is null".
                 " ORDER BY c.ordinal_position";
        $tableSourceName = explode(".", $tableSourceName);
        $tableSourceName = end($tableSourceName);
        $params = array('table_name'=>$tableSourceName);
        $stmt = $this->PRODIGE->executeQuery($query, $params);
        $res = array();
        foreach($stmt as $row){
            if ( $row["description"] && stripos($row["description"], self::CALCUL_AUTOMATIQUE)!==false )
                continue;
            
            $field = array();
            $field['column_name'] = utf8_encode($row['column_name']);
            $field['data_type'] = $row['data_type'];
            $field['column_default'] = $row['column_default'];
            $field['udt_name'] = preg_replace("!^_!", "", $row['udt_name']);
            $field['character_maximum_length'] = $row['character_maximum_length'] ?: $row['numeric_precision'];
            $field['is_nullable'] = $row['is_nullable'];
            $field['domain_name'] = $row['domain_name'];
            $field["object_type"] = $row['object_type'];
            
            
            $res[] = $field;
        }
        
        return $res;
    }
    


    
    /**
     * @Route("/upload/picture/{post_name}", name="catalogue_web_uploadtemp", options={"expose"=true})
     */

    /**
    * @Route("/upload/picture/{post_name}/{editionLayername}/{workingTablename}/{mapfile}", name="editionmanager_uploadpicture", requirements={})
    */
    public function uploadPictureAction(Request $request, $post_name, $editionLayername, $workingTablename, $mapfile)
    {
        $this->_initialize($request, $mapfile, $editionLayername);
        $tablename = explode(".", $this->editionTablename);
        $tablename = end($tablename);
        
        $subpath = '/ressources/'.$tablename.'/';
        $uploaddir = $this->getParameter("PRODIGE_PATH_DATA").'/cartes/IHM'.$subpath;
        @mkdir($uploaddir, 0770, true);
        
        $filename = $post_name."_".basename($_FILES[$post_name]['name']);
        $uploadfile = $uploaddir . $filename;
        $urlfile = "/IHM/IHM/cartes".$subpath. $filename;
        // Check if image file is a actual image or fake image
        $success = move_uploaded_file($_FILES[$post_name]['tmp_name'], $uploadfile);
        return new JsonResponse(array("success"=>$success, "file"=>$urlfile, "name"=>basename($uploadfile)));
    }
    
    protected function uploadPicture($filename) {}
}

class FONCTIONS {
    


protected function computeExtentFromDb($tablename, $rowId, $strExtentToIntersect) {
  global $dbconn;
  global $projection;

  $res = "";

  $strSQL = "select st_astext(the_geom) as geom_str, st_area(the_geom) as geom_area from "  . 
  "(select st_intersection(the_geom, st_geomfromtext(:geometry, :projection)) as the_geom from {$tablename} where id=:rowId) t";
  $params = array("rowId"=>$rowId, "geometry" => $strExtentToIntersect, "projection"=>$projection);
  $result = pg_query($dbconn, $strSQL);
  if ($result) {
    // recupération des identifiants des nouveaux objets
    while ($row = pg_fetch_array($result)) {
    $res['geom'] = $row['geom_str'];
    $res['area'] = $row['geom_area'];
    }
  }

  return $res;
}





////////////////////////////////////////////////////////////////////////
// get all user's sessions
////////////////////////////////////////////////////////////////////////
protected function getAllSessionsForuserId($editionTablename) {

    global $dbconn;

    $strSQL = "SELECT DISTINCT TABLE_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME LIKE :table_name||'_%'";
    $table_name = explode(".", $this->editionTablename); $table_name = end($table_name);
    $params = array("table_name"=>$table_name);
    
    //die($strSQL);
    $result = pg_query($dbconn, $strSQL);
    if(!$result) {
        trigger_error("Error lors de la mise à jour des informations");
    }
    else {
        $res = array();
        while ($row = pg_fetch_assoc($result)) {
            $res[] = $row['table_name'];
        }
        $sessions_store = array();
        for($i=0 ; $i<count($res) ; $i++) {
            $explodeSession = explode("_", $res[$i]);
            $field = array();
            //TODO control user_id : if($explodeSession[count($explodeSession) - 2] == $user_id)
            //if($explodeSession[count($explodeSession) - 2] == $user_id) {
                $field['displaySession'] = date('Y-m-d H:i:s', intval($explodeSession[count($explodeSession) - 1]) );
                $field['valueSession'] = $res[$i];
                array_push($sessions_store, $field);
            //}

        }
    }

    $displaySession = array();
    $valueSession = array();
    foreach ($sessions_store as $key => $row) {
        $displaySession[$key]  = $row['displaySession'];
        $valueSession[$key] = $row['valueSession'];
    }

    array_multisort($displaySession, SORT_LOCALE_STRING , $valueSession, SORT_LOCALE_STRING , $sessions_store);

    return $sessions_store;
}


    }
