<?php

  function  CNCarteMetadata_set($p_oMap)
  {
    //pour modifier le texte par defaut des métadonneés --> cf. COMMON/SPEC/CARTE/CSCarteMetadata.php
    $metadataText = "";
    $metadataText = $p_oMap->getMetaData("COPYRIGHT");
    if (empty($metadataText)) $metadataText = "";
    if (function_exists('CSCarteMetadata_set')) $CsMetadataText = CSCarteMetadata_set($p_oMap);
    if (!empty($CsMetadataText)) $metadataText = $CsMetadataText;
    return $metadataText;
  }

  function  CNCarteMetadata_position($p_mapHeight, $p_mapWidth, $p_imgHeight, $p_imgWidth)
  {
    //pour modifier la localisation par defaut des métadonneés --> cf. COMMON/SPEC/CARTE/CSCarteMetadata.php
    $position = array();
    $position[0] = 5;                 //LEFT
    $position[1] = $p_mapHeight - $p_imgHeight - 5;   //TOP

    if (function_exists('CSCarteMetadata_position')) $CsPosition = CSCarteMetadata_position($p_mapHeight, $p_mapWidth, $p_imgHeight, $p_imgWidth);
    if (!empty($CsPosition)) $position = $CsPosition;
    return $position;
  }

  function  CNCarteMetadata_bgDebord()
  {
    //pour modifier la localisation par defaut des métadonneés --> cf. COMMON/SPEC/CARTE/CSCarteMetadata.php
    $bgDebord = array();
    $bgDebord[0] = 0;                 //HORIZONTAL
    $bgDebord[1] = 0;                 //VERTICALE

    if (function_exists('CSCarteMetadata_bgDebord')) $CsBgDebord = CSCarteMetadata_bgDebord();
    if (!empty($CsBgDebord)) $bgDebord = $CsBgDebord;
    return $bgDebord;
  }

  function  CNCarteMetadata_font()
  {
    //pour modifier la FONT par defaut des métadonneés --> cf. COMMON/SPEC/CARTE/CSCarteMetadata.php
    $font = "fritqat.ttf";
    if (function_exists('CSCarteMetadata_font')) $CsFont = CSCarteMetadata_font();
    if (!empty($CsFont)) $font = $CsFont;
    return $font;
  }

  function  CNCarteMetadata_fontSize()
  {
    //pour modifier la taille de la FONT par defaut des métadonneés --> cf. COMMON/SPEC/CARTE/CSCarteMetadata.php
    $fontSize = 12;
    if (function_exists('CSCarteMetadata_fontSize')) $CsFontSize = CSCarteMetadata_fontSize();
    if (!empty($CsFontSize)) $fontSize = $CsFontSize;
    return $fontSize;
  }

  function  CNCarteMetadata_color()
  {
    //pour modifier l'envronnement par defaut des métadonneés --> cf. COMMON/SPEC/CARTE/CSCarteMetadata.php
    $color = array("0","0","0");
    if (function_exists('CSCarteMetadata_color')) $CsColor = CSCarteMetadata_color();
    if (!empty($CsColor)) $color = $CsColor;
    return $color;
  }
  function  CNCarteMetadata_bgColor()
  {
    //pour modifier l'envronnement par defaut des métadonneés --> cf. COMMON/SPEC/CARTE/CSCarteMetadata.php
    if (function_exists('CSCarteMetadata_bgColor')) $color = CSCarteMetadata_bgColor();
    return $color;
  }
  function  CNCarteMetadata_bgTransp()
  {
    //pour modifier l'envronnement par defaut des métadonneés --> cf. COMMON/SPEC/CARTE/CSCarteMetadata.php
    $transparence = 0;
    if (function_exists('CSCarteMetadata_bgTransp')) $CsTransparence = CSCarteMetadata_bgTransp();
    if (!empty($CsTransparence)) $transparence = $CsTransparence ;
    return $transparence;
  }

  function  CNCarteMetadata_add($p_oMap, $urlMap, $disableAlpha = false)
  {
      $urlMap = realpath($urlMap);
      return CNCarteMetadata_addwithfont($p_oMap, $urlMap, $disableAlpha);
  }

  function CNCarteMetadata_CompletePathOfFontFile($p_oMap, $text_fontfile)
  {
    // Chemin de la police TrueType
    $font = $text_fontfile;
    // Chemin de la carte
    $mapPath = $p_oMap->mappath;
    
    if (!empty($mapPath))
      $mapPath .= "/";
    if (strstr($text_fontfile, "/") == FALSE)
    {
      $fontpath = $p_oMap->fontsetfilename;
      if (substr($fontpath, 0, 2) == "./") $fontpath = substr($fontpath, 2);
      $fontpath = dirname($fontpath);
      if (!empty($fontpath))
        $fontpath .= "/";
      $font = $mapPath.$fontpath.$text_fontfile;
    }
    return $font;
  }


  function  CNCarteMetadata_addwithfont($p_oMap, $urlMap, $disableAlpha = false)
  {

    //ajout de text dans la carte
    if (function_exists('CSCarteMetadata_addSpecifiqueText'))
      $p_oMap = CSCarteMetadata_addSpecifiqueText($p_oMap, $urlMap, $disableAlpha);

    return $p_oMap;
  }


  function  CNCarteMetadata_addText($p_oMap, $urlMap, $text, $text_fontfile, $text_fontsize, $text_left, $text_top, $cadre_left, $cadre_top, $cadre_H, $cadre_L, $text_bgColor, $text_color, $transparence, $disableAlpha = false)
  {
    $bResult = null;
    if (($urlMap) && ($p_oMap) && ($text_fontfile))
    {
      if ($text != "")
      {
        // Chemin de l'image à marquer
        $imgname = $urlMap;
        if (strstr($urlMap, $p_oMap->web->imagepath) == FALSE) $imgname = $p_oMap->web->imagepath.substr($urlMap, strlen($p_oMap->web->imageurl));

        // Modification de l'image
        $im = CNCarteMetadata_getRscFromImg($p_oMap, $imgname);
        if ($im){
          if (!empty($text_bgColor))
          {
            if ($disableAlpha)
              imagefilledrectangle($im, $cadre_left, $cadre_top, $cadre_left+$cadre_L, $cadre_top+$cadre_H, imagecolorallocate($im, $text_bgColor[0], $text_bgColor[1], $text_bgColor[2]));
            else
              imagefilledrectangle($im, $cadre_left, $cadre_top, $cadre_left+$cadre_L, $cadre_top+$cadre_H, imagecolorallocatealpha($im, $text_bgColor[0], $text_bgColor[1], $text_bgColor[2], $transparence));
          }
          imageTtFtext($im, $text_fontsize, 0, $text_left, $text_top, imagecolorallocate($im, $text_color[0], $text_color[1], $text_color[2]), $text_fontfile, $text);
          CNCarteMetadata_saveRscFromImg($im, $imgname, $disableAlpha);
          if (isset($im))
            imagedestroy($im);
          $bResult = $p_oMap;
        }
      }
      else{
        $bResult = $p_oMap;
      }
    }

    if(empty($bResult)){
      CNSerializeErreur_open();
      CNSerializeErreur_write("Impossible de mettre en place les métadonnées");
      CNSerializeErreur_close();
    }
    return $bResult;
  }

  function CNCarteMetadata_getRscFromImg($p_oMap, $imgName){
  	$img = null;
    switch(strrchr($imgName, ".")){
      case ".png":
        $img = @imageCreateFromPng($imgName);
        break;
      case ".gif":
        $img = @imageCreateFromGif($imgName);
        break;
      case ".jpg":
      case ".jpeg":
        $img = @imageCreateFromJpeg($imgName);
        break;
    }
    return $img;

  }

  function CNCarteMetadata_saveRscFromImg($img, $imgName, $disableAlpha = false)
  {
    switch(strrchr($imgName, "."))
    {
      case ".png":
        if (!$disableAlpha){
          @imagesavealpha($img, true);
        }
        @imagePng($img, $imgName);
        break;
      case ".gif":
        @imageGif($img, $imgName);
        break;
      case ".jpg":
      case ".jpeg":
        @imageJpeg($img, $imgName);
        break;
    }
  }
  
  
  //fonction spécifique d'ecriture des métadonnées
  //return == chaine          --> remplacement des métadonnées du fichier MAP par la chaine
  //return == null          --> prise en compte des métadonnées du fichier MAP: Balise "COPYRIGHT"
  //return == " "           --> affiche aucun copyright mm si il existe dans le fichier MAP
  /*function  CSCarteMetadata_set($p_oMap)
  {
    $CsMetadataText = " ";
    return $CsMetadataText;
  }*/

  //fonction spécifique de localisation sur la carte des métadonnées des métadonnées
  //return == ARRAY(2)        --> remplacement de la localisation des métadonnées
  //return == null          --> prise en compte de la ocalisation par defaut des métadonnées (en bas à gauche avec marge de 5px)
  function  CSCarteMetadata_position($p_mapHeight, $p_mapWidth, $p_imgHeight, $p_imgWidth)
  {
    $position = array();
    //$position[0] = LEFT;
    //$position[1] = TOP;
    $position[0] = $p_mapWidth - $p_imgWidth - 15;
    $position[1] = $p_mapHeight - $p_imgHeight - 8;

    return $position;
    //return null;
  }

  //fonction spécifique de definition ddebord du cadre de fond pour les metadonnées
  //return == ARRAY(2)        --> remplacement des debords du cadre des métadonnées
  //return == null          --> prise en compte des debords du cadre par defaut des métadonnées (0 et 0)
  function  CSCarteMetadata_bgDebord()
  {
    $bgDebord = array();
    $bgDebord[0] = 10;                  //HORIZONTAL
    $bgDebord[1] = 3;                 //VERTICALE

    return $bgDebord;
    //return null;
  }

  //fonction spécifique pour la font des métadonnées
  //return == chaine          --> remplacement de la font des métadonnées
  //return == null          --> prise en compte de la font par defaut des métadonnées: "fritqat.ttf"
  function  CSCarteMetadata_font()
  {
    //return "ARIAL.TTF";
    return null;
  }

  //fonction spécifique pour la taille de la font des métadonnées
  //return == entier          --> remplacement de la taille de la font des métadonnées
  //return == null          --> prise en compte de la taille de la FONT par defaut des métadonnées: "12"
  function  CSCarteMetadata_fontSize()
  {
    return 7;
    //return null;
  }

  //fonction spécifique pour la couleur des métadonnées
  //return == COLOR           --> remplacement de la couleur des métadonnées
  //return == null          --> prise en compte de la couleur par defaut des métadonnées: "imagecolorallocate($im, 0,0,0)"
  function  CSCarteMetadata_color()
  {
    return array("1","0","0");
    //return null;
  }

  //fonction spécifique pour la couleur de fond des métadonnées
  //return == COLOR           --> remplacement de la couleur de fond des métadonnées
  //return == null          --> prise en compte de la couleur de fond par defaut des métadonnées: "aucune"
  function  CSCarteMetadata_bgColor()
  {
    return array("255","255","255");
    //return null;
  }

  //fonction spécifique pour la transparence de lacouleur  de fond des métadonnées
  //return == de 0 à 127          --> remplacement de la couleur de fond des métadonnées
  //return == null          --> prise en compte de la transparence par defaut des métadonnées: "aucune (0)"
  function  CSCarteMetadata_bgTransp()
  {
    return 8;
    //return null;
  }




















  //---------------------
  //function lancée apres l'ajout des metadonnées
  function  CSCarteMetadata_addSpecifiqueText($p_oMap, $urlMap, $disableAlpha = false)
  {
    //ajout de deux ligne de texte en haut agauche centréES par rapport a un rectangle
    //CSCarteMetadata_addScaleNumerique($p_oMap, $urlMap);
    //ajout de l'echelle dans la carte
    if (function_exists('CSCarteMetadata_MEDD_ajout'))
    {
      CSCarteMetadata_MEDD_ajout($p_oMap, $urlMap, $disableAlpha);
    }

    return $p_oMap;
  }


  function CSCarteMetadata_addExemple($p_oMap, $urlMap){
    //exemple pour ajouté deux ligne de texte en haut agauche centré par rapport a un rectangle

    $text_fontsize = 9;
    $text_bgColor = array("255", "255", "255");
    $text_cadreDebordH = 5;
    $text_cadreDebordV = 5;
    $text_Debord = 10;
    $font = CNCarteMetadata_CompletePathOfFontFile($p_oMap, "ARIAL.TTF");

    $text2 = " Dernière mise à jour: 24/11/05 ";
    $text1 = "Site de démonstration du MEDD [ version 0.16 ]";


    $bbox1 = imagettfbbox($text_fontsize, 0, $font, $text1);
    $tmpTxt1_left   = 0-min($bbox1[0],$bbox1[2],$bbox1[4],$bbox1[6]);
    $tmpTxt1_top  = 0-min($bbox1[1],$bbox1[3],$bbox1[5],$bbox1[7]);
    $tmpTxt1_width  = max($bbox1[0],$bbox1[2],$bbox1[4],$bbox1[6]) - min($bbox1[0],$bbox1[2],$bbox1[4],$bbox1[6]);
    $tmpTxt1_height = max($bbox1[1],$bbox1[3],$bbox1[5],$bbox1[7]) - min($bbox1[1],$bbox1[3],$bbox1[5],$bbox1[7]);

    $bbox2 = imagettfbbox($text_fontsize, 0, $font, $text2);
    $tmpTxt2_left   = 0-min($bbox2[0],$bbox2[2],$bbox2[4],$bbox2[6]);
    $tmpTxt2_top  = 0-min($bbox2[1],$bbox2[3],$bbox2[5],$bbox2[7]);
    $tmpTxt2_width  = max($bbox2[0],$bbox2[2],$bbox2[4],$bbox2[6]) - min($bbox2[0],$bbox2[2],$bbox2[4],$bbox2[6]);
    $tmpTxt2_height = max($bbox2[1],$bbox2[3],$bbox2[5],$bbox2[7]) - min($bbox2[1],$bbox2[3],$bbox2[5],$bbox2[7]);

    if ($tmpTxt1_width < $tmpTxt2_width){
      $largTxt = $tmpTxt2_width;
      $text_left1 = $text_Debord  + $tmpTxt1_left + ($tmpTxt2_width-$tmpTxt1_width)/2;
      $text_left2 = $text_Debord  + $tmpTxt2_left;
    }
    else {
      $largTxt = $tmpTxt1_width;
      $text_left1 = $text_Debord  + $tmpTxt1_left;
      $text_left2 = $text_Debord  + $tmpTxt2_left + ($tmpTxt1_width-$tmpTxt2_width)/2;
    }
    $text_top1 = $text_Debord  + $tmpTxt1_top;
    $text_top2 = $text_top1 + $tmpTxt1_height;

    $cadre_left = $text_Debord  - $text_cadreDebordH;
    $cadre_top  = $text_top1 - $tmpTxt1_top - $text_cadreDebordV;
    $cadre_larg = $largTxt + 2 * $text_cadreDebordH;
    $cadre_haut = $tmpTxt1_height + $tmpTxt2_height + 2 * $text_cadreDebordV;

    CNCarteMetadata_addText($p_oMap, $urlMap, $text1, $font, $text_fontsize, $text_left1, $text_top1, $cadre_left,  $cadre_top, $cadre_haut,  $cadre_larg,  $text_bgColor,  array("0", "0", "255"),8);
    CNCarteMetadata_addText($p_oMap, $urlMap, $text2, $font, $text_fontsize, $text_left2, $text_top2, 0,      0,      0,        0,        null,     array("255", "0", "0"), 0);
  }

  function CSCarteMetadata_addScaleInMap($p_oMap, $urlMap){
    $scalebar = $p_oMap->scalebar;
    $scalebar->set('width', $p_oMap->width/4);
      $m_oImageScale = $p_oMap->drawScaleBar();
    $urlScale = $m_oImageScale->saveWebImage();

    // Chemin de l'image à marquer
    $imgname1 = $urlMap;
    if (strstr($urlMap, $p_oMap->web->imagepath) == FALSE) $imgname1 = $p_oMap->web->imagepath.substr($urlMap, strlen($p_oMap->web->imageurl));
    $imgname2 = $urlScale;
    if (strstr($urlScale, $p_oMap->web->imagepath) == FALSE) $imgname2 = $p_oMap->web->imagepath.substr($urlScale, strlen($p_oMap->web->imageurl));

    $im1 = CNCarteMetadata_getRscFromImg($p_oMap, $imgname1);
    $im2 = CNCarteMetadata_getRscFromImg($p_oMap, $imgname2);

    if ($im1){
      $sizeScale = getimagesize ( $imgname2 );
      imagecopymerge ( $im1, $im2, 5, $p_oMap->height - $sizeScale[1] - 5, 0, 0, $sizeScale[0], $sizeScale[1], 60 );
      CNCarteMetadata_saveRscFromImg($im1, $imgname1);
    }

    //echelle numerique

    $text_fontsize = 8;
    $text_bgColor = array("255", "255", "255");
    $text_cadreDebordH = 5;
    $text_cadreDebordV = 5;
    $text_Debord = 10;
    $font = CNCarteMetadata_CompletePathOfFontFile($p_oMap, "ARIAL.TTF");


    $scale = intval($p_oMap->scale);
    $scale = strrev($scale);
    $temp = "";
    while (strlen($scale)>3){
      $temp = $temp." ".substr($scale, 0, 3);
      $scale = substr($scale, 3, strlen($scale));
    }
    $temp = $temp." ".$scale;
    $scale = strrev($temp);

    $text1 = "Echelle: 1 / ".$scale;

    $bbox1 = imagettfbbox($text_fontsize, 0, $font, $text1);
    $tmpTxt1_left   = 0-min($bbox1[0],$bbox1[2],$bbox1[4],$bbox1[6]);
    $tmpTxt1_top  = 0-min($bbox1[1],$bbox1[3],$bbox1[5],$bbox1[7]);
    $tmpTxt1_width  = max($bbox1[0],$bbox1[2],$bbox1[4],$bbox1[6]) - min($bbox1[0],$bbox1[2],$bbox1[4],$bbox1[6]);
    $tmpTxt1_height = max($bbox1[1],$bbox1[3],$bbox1[5],$bbox1[7]) - min($bbox1[1],$bbox1[3],$bbox1[5],$bbox1[7]);

    /*
    CNCarteMetadata_addText(
        $p_oMap,
        $urlMap,
        $text,
        $text_fontfile,
        $text_fontsize,
        $text_left,
        $text_top,
        $cadre_left,
        $cadre_top,
        $cadre_H,
        $cadre_L,
        $text_bgColor,
        $text_color,
        $transparence)
    */
    CNCarteMetadata_addText(
        $p_oMap,
        $urlMap,
        $text1,
        $font,
        $text_fontsize,
        5 + $sizeScale[0],
        $p_oMap->height - $sizeScale[1] + $tmpTxt1_height,
        5 + $sizeScale[0],
        $p_oMap->height - $sizeScale[1]-5,
        $sizeScale[1]-1,
        $tmpTxt1_width,
        $text_bgColor,
        array("0", "0", "0"),
        40
      );


    if (isset($im1))  imagedestroy($im1);
    if (isset($im2))  imagedestroy($im2);

    return $p_oMap;
  }
  
  function CSCarteMetadata_MEDD_ajout($p_oMap, $urlMap, $disableAlpha = false)
  {
    $l_bAdd = false;
    $l_bAddMetadata = false;
    $l_bAddScaleAsImage = false;
    $l_bAddScaleAsTexte = false;
    $l_bAddLogo = false;
    $rapport = 1.0 * $p_oMap->resolution / 96;

    /*$l_bAdd = ($p_oMap->getMetaData("ADDINCRUSTATIONS") != "0");
    if (!$l_bAdd)
      return;*/
    
    $mapPath = $p_oMap->mappath;
    if (!empty($mapPath)) $mapPath .= "/";
    
    $l_bAddLogo = ($p_oMap->getMetaData("LOGO") != "0");
    $imgLogo = $p_oMap->getMetaData("LOGOPATH");
    if (!empty($imgLogo) && $l_bAddLogo)
    {
      $imgLogo = $mapPath.$imgLogo;
      if (!is_file($imgLogo))
        $l_bAddLogo = false;
    } else {
        $l_bAddLogo = false;
    }

    $l_bAddMetadata = ($p_oMap->getMetaData("COPYRIGHT") != "0");
    $metadata_text = "";
    if ($l_bAddMetadata)
    {
      $metadata_text = $p_oMap->getMetaData("COPYRIGHTTXT");
      if (is_null($metadata_text))
        $metadata_text = "";
      if ($metadata_text == "")
        $l_bAddMetadata = false;
    }

    $l_bAddScaleAsImage = ($p_oMap->getMetaData("SCALEIMG") != "0");
    if (($p_oMap->getMetaData("SCALETXT") != "0") && ($rapport == 1.0))
      $l_bAddScaleAsTexte = true;
    else
      $l_bAddScaleAsTexte = false;


    $fontMeddAlias = $p_oMap->getMetaData("FONT");
    $fontMedd = CSCarteMetadata_MEDD_getFontFileByAlias($mapPath, $p_oMap, $fontMeddAlias);
    if ($fontMedd == "")
    {
      $l_bAddScaleAsTexte = false;
      $l_bAddMetadata = false;
    }


    $bgColorParam = $p_oMap->getMetaData("BACKGROUNDIMAGECOLOR");
    if (empty($bgColorParam))
      $bgColorParam = "FFFFFF00";
    if (strlen($bgColorParam) == 6)
      $bgColorParam .= "00";
    $bgTransparency = hexdec(substr($bgColorParam, 6, 2))/2 ;
    $bgTransparency = 127 - $bgTransparency ;
    //if ($bgTransparency > 100) $bgTransparency = 100;
    $bgTransparency = 100 * $bgTransparency  / 127;

    $fontColorParam = $p_oMap->getMetaData("FONTCOLOR");
    if (empty($fontColorParam))  $fontColorParam = "00000000";
    if (strlen($fontColorParam) == 6) $fontColorParam .= "00";

    $fontSize = $p_oMap->getMetaData("FONTSIZE");
    if (empty($fontSize))
      $fontSize = 8;
    $fontSize = round($rapport * $fontSize);

    $scalebar = $p_oMap->scalebar;
    //$scalebar->set('width', $p_oMap->width / 4);
    //$scalebar->set('height', round($scalebar->height * $rapport));
    $labelScaleBar = $scalebar->label;
    //$labelScaleBar->set('type', MS_TRUETYPE);
    //$labelScaleBar->set('font', $fontMedd);
    /*switch($labelScaleBar->type)
    {
      case MS_BITMAP:
        if ($rapport != 1)
          $labelScaleBar->set('size', MS_GIANT);
        break;
      default:
        $labelScaleBar->set('size', round($labelScaleBar->size * $rapport));
        break;
    }*/
    $labelScaleBar->color->setRGB(hexdec(substr($fontColorParam, 0, 2)), hexdec(substr($fontColorParam, 2, 2)), hexdec(substr($fontColorParam, 4, 2)));
    $scalebar->color->setRGB(hexdec(substr($fontColorParam, 0, 2)), hexdec(substr($fontColorParam, 2, 2)), hexdec(substr($fontColorParam, 4, 2)));
    $scalebar->backgroundcolor->setRGB(hexdec(substr($bgColorParam, 0, 2)), hexdec(substr($bgColorParam, 2, 2)), hexdec(substr($bgColorParam, 4, 2)));
    $scalebar->imagecolor->setRGB(hexdec(substr($bgColorParam, 0, 2)), hexdec(substr($bgColorParam, 2, 2)), hexdec(substr($bgColorParam, 4, 2)));

    $m_oImageScale = $p_oMap->drawScaleBar();
    $urlScale = $m_oImageScale->saveWebImage();

    // Chemin de l'image à marquer
    $imgCarte = $urlMap;
    if (strstr($urlMap, $p_oMap->web->imagepath) == FALSE) 
    	$imgCarte = $p_oMap->web->imagepath."/".substr($urlMap, strlen($p_oMap->web->imageurl)); 
    $imgEchelle = $urlScale;
    if (strstr($urlScale, $p_oMap->web->imagepath) == FALSE)
      $imgEchelle = $p_oMap->web->imagepath."/".substr($urlScale, strlen($p_oMap->web->imageurl));


    /*
    //AJOUT du LOGO
    $sizeLogo = array(0, 0, 3);
    if ($l_bAddLogo)
    {
      $sizeLogo = getimagesize($imgLogo);
      $p_posLeft = 5;
      $p_posTop = $p_oMap->height - $sizeLogo[1] - 5;
      CSCarteMetadata_MEDD_addImageInMap($p_oMap, $urlMap, $imgCarte, $imgLogo, $p_posLeft, $p_posTop, 100);
    }

    //AJOUT de l'ECHELLE en IMAGE
    $sizeScale = array(0, 0, 3);
    if ($l_bAddScaleAsImage)
    {
      $sizeScale = getimagesize($imgEchelle);
      $p_posTop = $p_oMap->height - $sizeScale[1] - 5;
      $p_posLeft = 10 + $sizeLogo[0];
        CSCarteMetadata_MEDD_addImageInMap($p_oMap, $urlMap, $imgCarte, $imgEchelle, $p_posLeft, $p_posTop, $bgTransparency);
    }

    //AJOUT de l'ECHELLE en TEXTE
    if ($l_bAddScaleAsImage)
      $p_posLeft = $p_posLeft + $sizeScale[0];
    else
    {
      if ($l_bAddLogo)
        $p_posLeft = 5 + $p_posLeft + $sizeLogo[0];
    }
    $p_posTop = $p_oMap->height - $sizeScale[1] - 5;
    $p_txtheight = $sizeScale[1] - $sizeScale[3];
    $MonTexte = CSCarteMetadata_MEDD_getScale($p_oMap);
    if ($l_bAddScaleAsTexte) CSCarteMetadata_MEDD_addTexteInMap($p_oMap, $urlMap, $imgEchelle, $MonTexte, $p_posLeft, $p_posTop, $p_txtheight, $fontMedd, $bgColorParam, $fontColorParam, $fontSize);

    //AJOUT du COPYRIGHT
    $p_posLeft = 400;
    $p_posTop = 100;
    $p_txtheight = $sizeScale[1] - $sizeScale[3];
    $MonTexte = $metadata_text;
    if ($l_bAddMetadata) CSCarteMetadata_MEDD_addCopyrightInMap($p_oMap, $urlMap, $imgEchelle, $MonTexte, $fontMedd, $bgColorParam, $fontColorParam, $fontSize, 0);
    */


    //AJOUT de l'ECHELLE en IMAGE
    $sizeScale = array(0, 0, 3);
    $p_posLeft = round(10 * $rapport);

    if ($l_bAddScaleAsImage)
    {
      $sizeScale = getimagesize($imgEchelle);
      //$p_posTop = $p_oMap->height - round(($sizeScale[1] + 5) * $rapport);
      $p_posTop = $p_oMap->height - ($sizeScale[1] + 5);
      $resolution = $p_oMap->resolution;
      //$p_oMap->set('resolution', 96);
        CSCarteMetadata_MEDD_addImageInMap($p_oMap, $urlMap, $imgCarte, $imgEchelle, $p_posLeft, $p_posTop, $bgTransparency, $disableAlpha);
        //$p_oMap->set('resolution', $resolution);
    }

    //AJOUT de l'ECHELLE en TEXTE
    $sizeLogo = array(0, 0, 3);
    if ($l_bAddScaleAsImage)
      $p_posLeft = $p_posLeft + $sizeScale[0];
    else
    {
      if ($l_bAddLogo)
      {
        $sizeLogo = getimagesize($imgLogo);
        $p_posLeft = 5 + $p_posLeft + $sizeLogo[0];
      }
    }
    $p_posTop = $p_oMap->height - $sizeScale[1] - 5;
    $p_txtheight = $sizeScale[1];
    $MonTexte = CSCarteMetadata_MEDD_getScale($p_oMap);
    if ($l_bAddScaleAsTexte)
      CSCarteMetadata_MEDD_addTexteInMap($p_oMap, $urlMap, $imgEchelle, $MonTexte, $p_posLeft, $p_posTop, $p_txtheight, $fontMedd, $bgColorParam, $fontColorParam, $fontSize, $disableAlpha);

    //AJOUT du COPYRIGHT
    //$l_bAddLogo = true;
    if ($l_bAddLogo)
    {
      $sizeLogo = getimagesize($imgLogo);
      $widthLogo = round($sizeLogo[0] * $rapport);
    }
    else
    {
      $widthLogo = 0;
    }
    $p_posLeft = 300;
    $p_posTop = 100;
    $p_txtheight = $sizeScale[1];
    $MonTexte = $metadata_text;
    if ($l_bAddMetadata)
      CSCarteMetadata_MEDD_addCopyrightInMap($p_oMap, $urlMap, $imgEchelle, $MonTexte, $fontMedd, $bgColorParam, $fontColorParam, $fontSize, $widthLogo, $disableAlpha);


    //AJOUT du LOGO
    $sizeLogo = array(0, 0, 3);
    if ($l_bAddLogo)
    {
      $sizeLogo = getimagesize($imgLogo);
      $p_posLeft = $p_oMap->width  - round($sizeLogo[0] * $rapport);
      $p_posTop = $p_oMap->height - round(($sizeLogo[1] + 5) * $rapport);
      CSCarteMetadata_MEDD_addImageInMap($p_oMap, $urlMap, $imgCarte, $imgLogo, $p_posLeft, $p_posTop, 100, $disableAlpha);
    }
  }



  function CSCarteMetadata_MEDD_addImageInMap($p_oMap, $urlMap, $imgname1, $imgname2, $p_posLeft, $p_posTop, $p_Transparency, $disableAlpha = false)
  {
    $rapport = 1.0 * $p_oMap->resolution / 96;
    $im1 = CNCarteMetadata_getRscFromImg($p_oMap, $imgname1);
    $im2 = CNCarteMetadata_getRscFromImg($p_oMap, $imgname2);

    if (!is_null($im1) && (!is_null($im2)))
    {
      $sizeScale = getimagesize ( $imgname2 );
      if ($rapport == 1.0){
      	@imagecopymerge ( $im1, $im2, $p_posLeft, $p_posTop, 0, 0, $sizeScale[0], $sizeScale[1], $p_Transparency );
      }else{
      	
        @imagecopyresampled ( $im1, $im2, $p_posLeft, $p_posTop, 0, 0, round($sizeScale[0] * $rapport), round($sizeScale[1] * $rapport), $sizeScale[0], $sizeScale[1] );
      }
      CNCarteMetadata_saveRscFromImg($im1, $imgname1, $disableAlpha);
    }
    if (isset($im1))  @imagedestroy($im1);
    if (isset($im2))  @imagedestroy($im2);
    
    return $p_oMap;
  }

  function CSCarteMetadata_MEDD_addTexteInMap($p_oMap, $urlMap, $imgname2, $p_sMontexte, $p_posLeft, $p_posTop, $p_txtheight, $p_fontMedd, $p_bgColorParam, $p_fontColorParam, $p_fontSize, $disableAlpha = false)
  {

    //echelle numerique
    $text_fontsize = $p_fontSize;
    $text_cadreDebordH = 5;
    $text_cadreDebordV = 5;
    $text_Debord = 10;
    $font = CNCarteMetadata_CompletePathOfFontFile($p_oMap, $p_fontMedd);


    $bbox1 = imagettfbbox($text_fontsize, 0, $font, $p_sMontexte);
    $tmpTxt1_left   = 0-min($bbox1[0],$bbox1[2],$bbox1[4],$bbox1[6]);
    $tmpTxt1_top  = 0-min($bbox1[1],$bbox1[3],$bbox1[5],$bbox1[7]);
    $tmpTxt1_width  = max($bbox1[0],$bbox1[2],$bbox1[4],$bbox1[6]) - min($bbox1[0],$bbox1[2],$bbox1[4],$bbox1[6]);
    $tmpTxt1_height = max($bbox1[1],$bbox1[3],$bbox1[5],$bbox1[7]) - min($bbox1[1],$bbox1[3],$bbox1[5],$bbox1[7]);

    // Modification de l'image
    $imgname = $urlMap;
    if (strstr($urlMap, $p_oMap->web->imagepath) == FALSE)
      $imgname = $p_oMap->web->imagepath.substr($urlMap, strlen($p_oMap->web->imageurl));
    $im = CNCarteMetadata_getRscFromImg($p_oMap, $imgname, $disableAlpha);
    if ($im){

      $fontcolorbg = imagecolorallocatealpha($im, hexdec(substr($p_bgColorParam, 0, 2)), hexdec(substr($p_bgColorParam, 2, 2)), hexdec(substr($p_bgColorParam, 4, 2)), hexdec(substr($p_bgColorParam, 6, 2))/2);
      $fontcolortxt= imagecolorallocatealpha($im, hexdec(substr($p_fontColorParam, 0, 2)), hexdec(substr($p_fontColorParam, 2, 2)), hexdec(substr($p_fontColorParam, 4, 2)), hexdec(substr($p_fontColorParam, 6, 2))/2);

      imagefilledrectangle(
        $im,
        $p_posLeft,                     /*  cadre_left    */
        $p_posTop,                      /*  cadre_top   */
        $p_posLeft+$tmpTxt1_width,              /*  cadre_right   */
        $p_posTop+$p_txtheight - 1,             /*  cadre_bottom  */
        $fontcolorbg                    /*  color     */
      );
      imageTtFtext(
        $im,
        $text_fontsize,                   /*  Font SIZE   */
        0,                          /*            */
        $p_posLeft,                     /*  Tetxt LEFT    */
        $p_posTop + ($p_txtheight+$tmpTxt1_height)/2,     /*  Text TOP    */
        $fontcolortxt,                    /*  Text Color    */
        $font,                        /*  Font file   */
        $p_sMontexte                    /*  Texte     */
      );
      CNCarteMetadata_saveRscFromImg($im, $imgname, $disableAlpha);
      if (isset($im))
        imagedestroy($im);
      $bResult = $p_oMap;
    }
    return $p_oMap;
  }

  function CSCarteMetadata_MEDD_addCopyrightInMap($p_oMap, $urlMap, $imgname2, $p_sMontexte, $p_fontMedd, $p_bgColor, $p_fontColor, $p_fontSize, $p_widthLogo, $disableAlpha = false)
  {

    $rapport = 1.0 * $p_oMap->resolution / 96;
    //echelle numerique
    $text_fontsize = $p_fontSize;
    $text_bgColor = $p_bgColor;
    $text_cadreDebordH = round(5 * $rapport);
    $text_cadreDebordV = round(5 * $rapport);
    $text_Debord = round(10 * $rapport);
    $font = CNCarteMetadata_CompletePathOfFontFile($p_oMap, $p_fontMedd);


    $bbox1 = imagettfbbox($text_fontsize, 0, $font, $p_sMontexte);
    $tmpTxt1_left   = 0-min($bbox1[0],$bbox1[2],$bbox1[4],$bbox1[6]);
    $tmpTxt1_top  = 0-min($bbox1[1],$bbox1[3],$bbox1[5],$bbox1[7]);
    $tmpTxt1_width  = max($bbox1[0],$bbox1[2],$bbox1[4],$bbox1[6]) - min($bbox1[0],$bbox1[2],$bbox1[4],$bbox1[6]);
    $tmpTxt1_height = max($bbox1[1],$bbox1[3],$bbox1[5],$bbox1[7]) - min($bbox1[1],$bbox1[3],$bbox1[5],$bbox1[7]);

  // Modification de l'image
    $imgname = $urlMap;
    if (strstr($urlMap, $p_oMap->web->imagepath) == FALSE)
      $imgname = $p_oMap->web->imagepath.substr($urlMap, strlen($p_oMap->web->imageurl));
    $im = CNCarteMetadata_getRscFromImg($p_oMap, $imgname, $disableAlpha);
    if ($im){

      $fontcolorbg = imagecolorallocatealpha($im, hexdec(substr($p_bgColor, 0, 2)), hexdec(substr($p_bgColor, 2, 2)), hexdec(substr($p_bgColor, 4, 2)), hexdec(substr($p_bgColor, 6, 2))/2);
      $fontcolortxt= imagecolorallocatealpha($im, hexdec(substr($p_fontColor, 0, 2)), hexdec(substr($p_fontColor, 2, 2)), hexdec(substr($p_fontColor, 4, 2)), hexdec(substr($p_fontColor, 6, 2))/2);

      imagefilledrectangle(
        $im,
        $p_oMap->width - $tmpTxt1_width - $text_Debord - $text_cadreDebordH - $p_widthLogo,     /*  cadre_left    */
        $p_oMap->height - $tmpTxt1_height - $text_Debord - $text_cadreDebordV,          /*  cadre_top   */
        $p_oMap->width  - $text_cadreDebordH - $p_widthLogo,            /*  cadre_right   */
        $p_oMap->height - $text_cadreDebordV,                   /*  cadre_bottom  */
        $fontcolorbg                        /*  color     */
      );
      imageTtFtext(
        $im,
        $text_fontsize,                   /*  Font SIZE   */
        0,                          /*            */
        $p_oMap->width - $tmpTxt1_width - $text_Debord - $p_widthLogo,/*  Tetxt LEFT    */
        $p_oMap->height - $tmpTxt1_height,          /*  Text TOP    */
        $fontcolortxt,                    /*  Text Color    */
        $font,                        /*  Font file   */
        $p_sMontexte                    /*  Texte     */
      );
      CNCarteMetadata_saveRscFromImg($im, $imgname, $disableAlpha);
      if (isset($im))
        imagedestroy($im);
      $bResult = $p_oMap;
    }
    return $p_oMap;
  }

  function CSCarteMetadata_MEDD_getScale($p_oMap){
    
    $scale = intval(getScale($p_oMap->width, $p_oMap->extent->maxx,$p_oMap->extent->minx));
    
    $scale = strrev($scale);
    $temp = "";
    while (strlen($scale)>3){
      $temp = $temp." ".substr($scale, 0, 3);
      $scale = substr($scale, 3, strlen($scale));
    }
    $temp = $temp." ".$scale;
    $scale = strrev($temp);

    $temp = " Echelle: 1 / ".$scale;


    return $temp;
  }

function CSCarteMetadata_MEDD_getFontFileByAlias($mapPath, $oMap, $font = NULL)
{
  $fontlistfile = $mapPath.$oMap->fontsetfilename;
  $fontfile = "";
  $fd = fopen($fontlistfile, "r");
  if ($fd !== FALSE)
  {
    while (!feof($fd) && (empty($fontfile)))
    {
      $line = trim(fgets($fd));
      $pos = strpos($line, " ");
      if (is_null($font) || ($font == ""))
      {
        $font = substr($line, 0, $pos);
        $fontfile = trim(substr($line, $pos));
      }
      else
      {
        if (($pos >= 0) && (substr($line, 0, $pos) == $font))
          $fontfile = trim(substr($line, $pos));
      }
    }
    fclose($fd);
  }
  return $fontfile ;
}

function CSCarteMetadata_MEDD_hasRaster($oMap)
{
  $find = false;
  if (($oMap) && !is_null($oMap))
  {
    for ($i = 0; ($i < $oMap->numlayers) && !$find; $i++)
    {
      $oLayer = $oMap->getLayer($i);
      if ($oLayer && ($oLayer->status == MS_ON) &&
         (
          (intval($oLayer->maxscale < 0) || (intval($oLayer->maxscale) >= intval($oMap->scale))) &&
          (intval($oLayer->minscale < 0) || (intval($oLayer->minscale) <= intval($oMap->scale)))
         )
        )
      {
        switch($oLayer->type)
        {
          case MS_LAYER_RASTER:
          case MS_LAYER_TILEINDEX:
            $find = true;
            break;
          default:
            if ($oLayer->connectiontype == MS_WMS)
              $find = true;
            break;
        }
      }
    }
  }
  return $find;
}

function getScale($carte_width, $EXTENT_MAXX, $EXTENT_MINX) {
  $factor = 1;//getFactor($media);
  $MapWidth = $carte_width * 0.0254 / 96 / $factor;
  $largX = $EXTENT_MAXX - $EXTENT_MINX;
  $scale = $largX / $MapWidth;
  return $scale;
}

function  CSCarteMetadata_MEDD_adjust4Resolution($oMap)
{
  if (($oMap) && !is_null($oMap))
  {
    $rapport = 1.0 * $oMap->resolution / 96;
    for ($i = 0; $i < $oMap->numlayers; $i++)
    {
      $oLayer = $oMap->getLayer($i);
      if ($oLayer && ($oLayer->status == MS_ON))
      {
        for ($j = 0; $j < $oLayer->numclasses; $j++)
        {
          $oClass = $oLayer->getClass($j);
          if ($oClass)
          {
            if ($oClass->label)
              $oClass->label->set('size', ceil(doubleval($oClass->label->size) * $rapport));
            for ($k = 0; $k < $oClass->numstyles; $k++)
            {
              $oStyle = $oClass->getStyle($k);
              if ($oStyle)
              {
                $oStyle->set('size', ceil(doubleval($oStyle->size) * $rapport));
                $oStyle->set('minsize', ceil(doubleval($oStyle->minsize) * $rapport));
                $oStyle->set('maxsize', ceil(doubleval($oStyle->maxsize) * $rapport));
              }
            }
          }
        }
      }
    }
  }
  return $oMap;
}

?>
