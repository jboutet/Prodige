<?php
namespace ProdigeFrontCarto\HelpersBundle\MapfileToXML;
/*licence/ 

Module écrit, supporté par la société Alkante SAS <alkante@alkante.com>


Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site http://www.cecill.info.

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
termes.

/licence*/


/**
 * @package 
 * @author obedel
 * @class CarmenMapfileToOWC
 * @brief Class returning an XML "context" file derived from a carmen mapfile 
 */

error_reporting(E_ALL);

// Class parameters
define("MAPPING_CARMEN_CSV", realpath(dirname(__FILE__))."/lib/tagmapping_carmen.csv");
define("CARMEN_NS_URI", "http://www.carto.ecologie.gouv.fr/");
define("CARMEN_NS_PREFIX", "carmen");

// Carmen Constants
define("CARMEN_LAYER_POS", "GI_ARBO_ORDRE");
define("CARMEN_LAYER_GRP", "GI_ARBO_GROUP");
define("CARMEN_DISPLAY_CLASS", "CLASS_LEGENDE");
define("CARMEN_LAYER_TITLE", "LAYER_TITLE");

define("CARMEN_OWS_LAYER_SYMBOLSCALEDENOM", "layerSettings_symbolScaleDenom");
define("CARMEN_OWS_LAYER_METADATA_FILE", "layerSettings_MetadataFile");
define("CARMEN_OWS_MAP_METADATA_LOGOPATH", "mapSettings_logoPath");
define("CARMEN_OWS_MAP_METADATA_BACKGROUND_TRANSPARENCY", "mapSettings_backgroundTransparency");

define("REGEXP_GRP_STATE", "/([^\/]*)\[([+-])\]/"); // match a syntax "group_name[group_open]"

// Context merging policy constants
define("CARMEN_CTX_MERGING_FROM_MAP",0);
define("CARMEN_CTX_MERGING_FROM_CTX",1);
define("CARMEN_CTX_MERGING_FROM_MIX",2);

// Context merging default config files
define("CARMEN_CTX_MERGING_POLICY_FILE","lib/context_merging_policy_prodige.csv");
define("CARMEN_CTX_MERGING_CONFIG_FILE","lib/context_merging_cfg_prodige.csv");


// Miscellaneous constants
define("CON_CGI", 0);
define("CON_WMS", 1);


class CarmenMapfileToOWC extends MapfileToOWC {

	private $mdataMapping_map = array();  	// metadata mapping for the map section mdataMapping_map[CARMEN MAPFILE TAG] = "Context XML node name" 
	private $mdataMapping_layer = array();	// idem for the layer section
	private $mdataMapping_class = array();	// idem for the class section
		
	private $layerTree = NULL;  	
	
	/**
	 * 
	 * Constructor
	 * @param $mapfile : path to the mapfile
	 * @param $generate_mdata :  flag to generate or not metadatas of the map and the layer in the context file  
	 * @param $cgi_url : url of the CGI mapserver engine interpreting the mapfile
	 * @param $base_url : base url for element refered in the mapfile other than data (keymap image...)
	 * 
	 */
	function __construct($mapfile, $cgi_url="http://server/mapserv", $mapfile2 = "", $base_url="http://server/mapfile_ref/",
	$extent=null, $group=null, $layer=null, $object=null) 
	{
		parent::__construct($mapfile, $cgi_url, true, $mapfile2, $base_url, $extent, $group, $layer, $object);
		
		if ($extent!=null || $group!=null || $layer!=null || $object !=null) 
      $this->initialExtent = $this->defineMapObj($extent, $group, $layer, $object);
      
    $this->initialExtent = $this->initialExtent == NULL ? $this->map->extent : $this->initialExtent;
	}

	
 /**
   * @brief : update layers status according to contextual parameters and 
   * 	return an initial extent defined from parameter extent or calculated 
   * 	from parameter object 
   * @param extent : initial extent encoded as 'minx,miny,maxx,maxy"
   * @param group : ';' separated list of layer groups that should be set to ON   
   * @param layer : ';' separated list of layers that should be set to ON
   * @param object : encoded string ("layer;field;value") describing object(s) 
   * which enclosing bounding box will serve as initial extent view on the map
   *	@return the initial extent view. 
   */
  private function defineMapObj($extent, $group, $layer, $object)
  {
    $oExtentMax = ms_newRectObj();
    $oExtentMax->setExtent($this->map->extent->minx, 
     $this->map->extent->miny,
     $this->map->extent->maxx,
     $this->map->extent->maxy);
    $oExtentInitial = NULL;
    if($extent!=""){
      $oExtentInitial = shapeStrToRectObj($extent);
    }
    else
    {
      if ($object!="") {
        //request
        $selExtentLayer = ms_newRectObj();
        $selExtentLayer->setExtent(MAX_DOUBLE, MAX_DOUBLE, MIN_DOUBLE, MIN_DOUBLE);
        $tabParamQuery = explode(";", $object);
        $qstring = $tabParamQuery[1] . "=" . '"' . $tabParamQuery[2] . '"';
        for ($i = 0; $i < $this->map->numlayers; $i++){
          $msLayer = $this->map->getLayer($i);
          $layerTitle = $msLayer->getMetadata(CARMEN_LAYER_TITLE);
          if(strcmp(trim($layerTitle), mapfileEncode(trim($tabParamQuery[0])))==0){
           // activate queried layer
           $msLayer->set("status", MS_ON); 
           switch($msLayer->connectiontype) {
              case MS_SHAPEFILE :
              case MS_TILED_SHAPEFILE :
                // redefining the connection as an ogr connection
                // to allow complex query expressions via DATA sql restrictions
                $datasetName = datasetNameFromFilename(OWSEncode($msLayer->data));
                $resSetConnection = @$msLayer->set("connection", mapfileEncode(forceShapefileExtension(OWSEncode($msLayer->data))));
                @$msLayer->set("connectiontype", MS_OGR);
                $sqlStr = 'SELECT * FROM ' . $datasetName . ' WHERE ' . stripslashes($qstring);
                @$msLayer->set("data", mapfileEncode($sqlStr));
                $queryMode = QM_OGR_FILTER;
                $res = (@$msLayer->open()==MS_SUCCESS);
                $debugQstring = $sqlStr . ' ' . $msLayer->connection;
                break;
              case MS_OGR :
                // defining or completing the DATA sql restrictions
                $datasetName = datasetNameFromFilename(OWSEncode($msLayer->connection));
                $sqlStr = 'SELECT * FROM ' . $datasetName . ' WHERE ' . stripslashes($qstring);
                @$msLayer->set("data", mapfileEncode($sqlStr));
                
                //echo mapfileEncode($sqlStr) . "\n";
                
                $queryMode = QM_OGR_FILTER;
                $res = (@$msLayer->open()==MS_SUCCESS);
                // todo : look if there is already a restriction
                // but not simple to handle cos LEFT JOIN and so on...
                // if (isset($msLayer->data) && strlen(($msLayer->data))>0)
                $debugQstring = $sqlStr;
                break;
              case MS_POSTGIS :
                $queryMode = QM_MAPSERVER_QUERY;
                $qstring = $tabParamQuery[1]. '::text' . '=' . '\'' . $tabParamQuery[2] . '\'';
                $res = (@$msLayer->queryByAttributes(mapfileEncode($tabParamQuery[1]), mapfileEncode($qstring) ,MS_MULTIPLE)==MS_SUCCESS);
                if ($res)
                  $res = (@$msLayer->open()==MS_SUCCESS);
                $debugQstring = $qstring;
                break;
              default:
                // general case : handling query with mapscript queryByAttributes
                $queryMode = QM_MAPSERVER_QUERY;
                $qstring = $tabParamQuery[2];
                $res = (@$msLayer->queryByAttributes(mapfileEncode($tabParamQuery[1]), mapfileEncode($qstring) ,MS_MULTIPLE)==MS_SUCCESS);
                if ($res)
                 $res = (@$msLayer->open()==MS_SUCCESS);
                $debugQstring = $qstring;
                break;
            }
            if ($res){
              //echo " " . $res . "\n"; 
              $count=0;
              switch($queryMode) {
                case QM_OGR_FILTER:
                  $status = @$msLayer->whichShapes($this->map->extent);
                  //print_r($this->map->extent);
                  //echo "layer status " . $msLayer->status . "\n";
                  //echo "query status " . $status . "\n";
                  //print_r($msLayer->getExtent());
                  $count=0;
                  while ($shape = @$msLayer->nextShape())
                  {
                    
                    //print_r($selExtentLayer);
                    //print_r($feat->bounds);
                    
                    $selExtentLayer = mergeRectObj($selExtentLayer, $shape->bounds);
                    //print_r($selExtentLayer);
                    $shape->free();
                    $count++;
                  }
                  //echo $count . "\n";
                  break;
                case QM_MAPSERVER_QUERY:
                  $count = @$msLayer->getNumResults();
                  for ($j=0;$j<$count;$j++) {
                    $resObj = @$msLayer->getResult($j);
                    $feat = @$msLayer->getShape($resObj);
                    $selExtentLayer = mergeRectObj($selExtentLayer, $feat->bounds);
                    $feat->free();
                  }
                  break;
              }
              if ($count>0) {
                $oExtentInitial = $selExtentLayer;
              }
              $msLayer->close();
            }
          }
        }
      }
    }
    
    
    if ($group!="" || $layer!="") {
      $tabGroups = explode(";", mapfileEncode($group));
      $tabLayers = explode(";", mapfileEncode($layer));
      for ($i = 0; $i < $this->map->numlayers; $i++){
        $msLayer = $this->map->getLayer($i);
        $giArboGroup = $msLayer->getMetadata(CARMEN_LAYER_GRP);
        $layerTitle = $msLayer->getMetadata(CARMEN_LAYER_TITLE);
        if ($group != "") {
          foreach($tabGroups as $key => $value) {
	          //$group = trim(stripslashes(urldecode($group)));
            //echo $group . "    ";
	          //echo $giArboGroup;
	          //echo '<br/>';
	          if(strpos($giArboGroup, trim($value))!==false) {
	            $msLayer->set("status", MS_ON);
            }
          }
        }
        if($layer!="") {
          foreach($tabLayers as $key => $value){
            //$value = trim(stripslashes(urldecode($value)));
            //echo $value;
            //echo $layerTitle . "   ";
            //echo '<br/>';
            if (strcmp(trim($layerTitle),trim($value))==0) {
              $msLayer->set("status", MS_ON);
            }
          }
        }
      }
    }
    return $oExtentInitial;
  }
	
	
	
	
	
	
	
	////////////////////////////////////////////////
	// Metadata part
  ///////////////////////////////////////////////
	
  protected function readMetadata() {
    parent::readMetadata();
    $this->readMetadataMappings();
  }
    
  
  protected function referenceMapToXML()
  {
    $m_ref = $this->map->reference;
    //$n_refMap = $this->xml_doc->createElementNS(CARMEN_NS_URI, CARMEN_NS_PREFIX . "ReferenceMap");
    $n_refMap = $this->xml_doc->createElement("ReferenceMap");
    
    // interesting to get format of the image, but not provided by mapfile...
    //$n_refMap->setAttribute("format", )
    $n_refMap->setAttribute("width", OWSEncode($m_ref->width));
    $n_refMap->setAttribute("height", OWSEncode($m_ref->height));
    
    // adding bounding box info for ref Map
    $n_bb = $this->xml_doc->createElement("BoundingBox");
    //$n_bb = $doc->createElementNS(OWS_URI, "BoundingBox");
    $proj_def = OWSEncode($this->map->getProjection());
    $n_bb->setAttribute("crs", $proj_def);

    //$n_lc = $n_bb->getElementsBytagNameNS(OWS_URI, "LowerCorner")->item(0);
    $n_lc = $this->xml_doc->createElement("LowerCorner");
    $lc_str = OWSEncode($m_ref->extent->minx) . " " . OWSEncode($m_ref->extent->miny);
    $n_lc->nodeValue = $lc_str;

    //$n_uc = $n_bb->getElementsBytagNameNS(OWS_URI, "UpperCorner")->item(0);
    $n_uc = $this->xml_doc->createElement("UpperCorner");
    $uc_str = OWSEncode($m_ref->extent->maxx) . " " . OWSEncode($m_ref->extent->maxy);
    $n_uc->nodeValue = $uc_str;
    
    $n_bb->appendChild($n_lc);
    $n_bb->appendChild($n_uc);
    $n_refMap->appendChild($n_bb);
    
    // now give info about the url of the ref image
    $n_url = $this->xml_doc->createElement("OnlineResource");
    //$n_url = $this->xml_doc->createElementNS(OWS_URI, "OnlineResource");
    //echo $this->mapfile_base_url . " --- " . $m_ref->image;
    $n_url->setAttribute("href", $this->mapfile_base_url . OWSEncode(str_replace("../", "", $m_ref->image)));
    $n_refMap->appendChild($n_url);
    
    return $n_refMap;
  }
    
    
    
  /**
   * @return a XML Node encoding map metadata
   */
  protected function MapMetadataToXML()
  {
    $n_extension = parent::MapMetadataToXML();

    // adding reference map image url to extension section
    if ($this->map->reference!=null) {
      $n_extension->appendChild($this->referenceMapToXML());
    }

    // adding full url path in some local mapfile references
    // TODO : find a way to handle path in a better way
    $ln_mdata = $n_extension->getElementsByTagName(CARMEN_OWS_MAP_METADATA_LOGOPATH);  
    if ($ln_mdata->length>0) {
      $mdata_base_url = $this->mapfile_base_url;
      $mdata_filename = $ln_mdata->item(0)->nodeValue;
      $n_extension->removeChild($ln_mdata->item(0));
      
      // recuperation des parametres width, height, format
      $format_logo = "";
      $width_logo =10;
      $height_logo =10;
            
      $urlImageLogo = $mdata_base_url.urldecode($mdata_filename); 
      
      $tabName = array();
      $tabName = explode(".", $mdata_filename);
      if(is_array($tabName) && !empty($tabName)){
        $format_logo = $tabName[1];
      }
      
      $size = @getimagesize($urlImageLogo);
      list($width_logo, $height_logo) = $size;

      // remplacement de mapsettings_logoPath par LogoURL 
      $version  = "1.0";
      $encoding = "UTF-8";
      $doc = new \DOMDocument($version, $encoding);      
      $strXml = "<LogoURL width=\"".$width_logo."\" height=\"".$height_logo."\" format=\"".$format_logo."\"   xmlns:xlink=\"http://www.w3.org/1999/xlink\">
                    <OnlineResource xlink:type=\"simple\" xlink:href=\"".$urlImageLogo."\"/>   
                  </LogoURL>";
      $tagLogoURL = new \DOMDocument;
      $tagLogoURL->loadXML($strXml);
      $nodeToImport = $tagLogoURL->getElementsByTagName("LogoURL")->item(0);

      $tagExtension = $n_extension->parentNode->getElementsByTagName("Extension");      
      if($tagExtension && $tagExtension->length>0){
        $newNodeDesc = $this->xml_doc->importNode($nodeToImport, true);
        $tagExtension->item(0)->parentNode->appendChild($newNodeDesc);
      }
      //$n_mdata = $this->xml_doc->createElement(CARMEN_OWS_MAP_METADATA_LOGOPATH, $mdata_base_url . './' . $mdata_filename);
      //$n_extension->appendChild($n_mdata);

    }
        
    // adding backroung transparency metadata
    $bgd_transparency = $this->map->outputformat->transparent;
    $n_mdata = $this->xml_doc->createElement(CARMEN_OWS_MAP_METADATA_BACKGROUND_TRANSPARENCY, $bgd_transparency ? "ON" : "OFF");
    $n_extension->appendChild($n_mdata);
    
    // adding layer tree organisation to extension section
    $this->buildLayerTree();  
    $this->buildProviderGroups();

    $n_layerTree = $this->layerTree->toXML($this->xml_doc);
    $n_extension->appendChild($n_layerTree);
    return $n_extension;
  }
    
    
    protected function createMetadataNode($name, $value, $section) {
  		$new_name="";
    	switch($section) {
    		case MS_SECTION_WEB : 
    			if (array_key_exists($name, $this->mdataMapping_map)) {
    				$new_name = $this->mdataMapping_map[$name];
    			}
    		break;
  		    case MS_SECTION_LAYER : 
    			if (array_key_exists($name, $this->mdataMapping_layer)) {
    				$new_name = $this->mdataMapping_layer[$name];
    			}
    		break;
  		    case MS_SECTION_CLASS : 
    			if (array_key_exists($name, $this->mdataMapping_class)) {
    				$new_name = $this->mdataMapping_class[$name];
    			}
    		break;
  		    default:
  		    break;
    	}
		
    	// test if the metadata is a carmen metadata 
    	if (strlen($new_name)!=0) {
    		//$new_name = CARMEN_NS_PREFIX .":" . $new_name;
    		//$n_mdata = $this->xml_doc->createElementNS(CARMEN_NS_URI, $new_name, CarmenMapfileToOWC::latin2utf8($value));
    		$n_mdata = $this->xml_doc->createElement(OWSEncode($new_name), rawurlencode(OWSEncode($value)));
    		//$n_mdata = $this->xml_doc->createElement($new_name, $value);
    	} 
    	else {
    		$n_mdata = parent::createMetadataNode($name, $value, $section);
    	}
    	
    	return $n_mdata;
  	}
  	
  	
 	/**
   *
   * @param $layer : an object layer (PHP Mapscript)
   * @return a XML Node encoding layer metadatas
   */
  
  	protected function LayerMetadataToXML($layer)
  {
    $n_extension = parent::LayerMetadataToXML($layer);
    $n_symbolScaleDenom = $this->xml_doc->createElement(CARMEN_OWS_LAYER_SYMBOLSCALEDENOM, $layer->symbolscaledenom);
    $n_extension->appendChild($n_symbolScaleDenom);
    
    // adding full url path in some local mapfile references
    // TODO : find a way to handle path in a better way
    $ln_mdataFile = $n_extension->getElementsByTagName(CARMEN_OWS_LAYER_METADATA_FILE);  
    if ($ln_mdataFile->length>0) {
      //if url is complete don't take in account local reference
      //TODO RegExp
      if (strrpos(urldecode($ln_mdataFile->item(0)->nodeValue), "http://")===false && strrpos(urldecode($ln_mdataFile->item(0)->nodeValue), "https://")===false) {
        $mdataFile_url = str_replace('IHM/IHM','IHM/metadata', $this->mapfile_base_url);
        $localPath = $ln_mdataFile->item(0)->nodeValue;
        $n_extension->removeChild($ln_mdataFile->item(0));
        $n_mdataFile = $this->xml_doc->createElement(CARMEN_OWS_LAYER_METADATA_FILE, $mdataFile_url . 'Publication/' . $localPath);
        $n_extension->appendChild($n_mdataFile);
      }
    }

    return $n_extension;
  }

  
  protected function LayerToXML($layer) {
    $n_layer = parent::LayerToXML($layer);
    return $n_layer;
  } 
	
	
  //////////////////////////////////////////
  // MISCELLANEOUS
  //////////////////////////////////////////
  
  private function readMetadataMappings() 
  {
  	// read metadata mapping file in th array tab
  	$tab = file(MAPPING_CARMEN_CSV, FILE_IGNORE_NEW_LINES + FILE_SKIP_EMPTY_LINES);
  	if ($tab != FALSE) {
		// fetch each line of the file 
  		for ($i=1; $i<count($tab); $i++) {
  			$row = explode(',', str_replace("\"", "", $tab[$i]));
  			// builds the different mapping tables (Map Metadata, Layer Metadata, Class Metadata)
  			switch($row[0]) {
  				case "WEB" :
					$this->mdataMapping_map[$row[1]] = $row[2];
  					break;
  				case "LAYER" :
					$this->mdataMapping_layer[$row[1]] = $row[2];  					
  					break;
  				case "CLASS" :
  					$this->mdataMapping_class[$row[1]] = $row[2];
  					break;
  				default:
  					throw new Exception('Unknown mapfile section');
  					break; 
  			} // end switch
  		} // end for
  	} // end if
  }  	

  /**
   * Computes the functionnal group attached to each layer 
   * 
   */
  
  private function buildLayerTree() {

  	$pos2grp = array();   // (CARMEN_LAYER_POS => CARMEN_LAYER_GRP) 
  	$pos2layer = array(); // (CARMEN_LAYER_POS => php mapscript layer object)
  	$pos2idx = array(); // (CARMEN_LAYER_POS => layer index in the mapfile)
    $rootLayers = array();
  	for ($i=0; $i<$this->map->numlayers; $i++) {
  		$olayer = $this->map->getLayer($i);
  		$mdata_pos = OWSEncode($olayer->getMetaData(CARMEN_LAYER_POS));
  		$mdata_grp = OWSEncode($olayer->getMetaData(CARMEN_LAYER_GRP));
      if (strlen($mdata_pos)>0) {
        $pos = intval($mdata_pos);
        $pos2layer[$pos] = $olayer;
        $pos2grp[$pos] = $mdata_grp; 
        $pos2idx[$pos] = $i;        
      }
      else
        $rootLayers[] = $olayer;
  	}
  	
  	for ($i=1; $i<=$this->map->numlayers; $i++) {
  	  if (!isset($pos2layer[$i])) {
  	    $layer = array_pop($rootLayers);
  	    $pos2layer[$i] = $layer;
  	    $pos2grp[$i] = ""; 
        $pos2idx[$i] = $i;        
  	  } 
  	}
  	
  	// layer should be inserted in the ascending order
  	$this->layerTree = new CarmenNode("legend", NODE_OPEN);
   	for ($i=1; $i<=count($pos2layer); $i++) {
  		if (array_key_exists($i,$pos2grp)) {
  		 $this->layerTree->addLeafFromCarmenStr($pos2grp[$i], $pos2layer[$i], $pos2idx[$i]);//, $pos2idx[$i]);
  		}		
  	}
  }

  // build "Provider groups" from the layer tree
  private function buildProviderGroups() {
    	$providerId = 0;
  		$tabProviderGrp= array();
//  	$root->prettyPrint();
  		$this->buildProviderGroups_aux($this->layerTree->children, $providerId, $tabProviderGrp);
//  	echo "\n";
//  	$root->prettyPrint();
  	
  }
  
  // build "Provider groups" from the layer tree (auxiliary function)
  private function buildProviderGroups_aux(&$nodeList, &$providerGrpId, &$tabProviderGrp) {
  	$layerSrc = "";
  	$layerType="";
  	$previousLayerInOneGroup = false;
	// check if all node of the list are of the same type
  	for ($i=0;$i<count($nodeList); $i++) {
  		$node = &$nodeList[$i];
  		// a node group does not belong to the current group
  		if ($node->type==NODE_GROUP) {
			  // we make a new group only if this node was not the first met 
  			//echo ($node->name . "    i " . $i . " providerGrpId " . $providerGrpId); 
  		  $providerGrpId = ($i>0) ? $providerGrpId + 1 : $providerGrpId;  			
  		  //echo ($node->name . "    i " . $i . " providerGrpId " . $providerGrpId);
  			// builds group(s) from this node
  			$this->buildProviderGroups_aux($node->children, $providerGrpId, $tabProviderGrp);
  		} 
  		else {
  			$olayer = &$node->misc["layer"];
  			$this->getLayerConnectionInfo($olayer, $layerType2, $layerSrc2);
        
  			if (($i>0) && 
  			  // check if the current layer does not have the same properties than the other of the group
  			  (!($layerType2==$layerType && strcmp($layerSrc2, $layerSrc)==0) 
          // or if the layer is a WMS-C layer
  			  || ($layerType2==CON_WMS &&  strcmp(OWSEncode($olayer->getMetadata("LAYER_TILED")),"ON")==0)
  			  // or if the layer is a direct child of the root node
  			  || ($node->depth==1)
  			  // or if the layer is a WMS with a transparency value set
  			  || ($layerType2==CON_WMS && $olayer->opacity!=100)
  			  // or if the previous layer has been marked to be a one group layer
  			  || $previousLayerInOneGroup)
  				) {
  				// if no, we make a new group
  				$providerGrpId++;
  				$previousLayerInOneGroup = ($layerType2==CON_WMS && 
  				 ($olayer->opacity!=100 || strcmp(OWSEncode($olayer->getMetadata("LAYER_TILED")),"ON")==0)); 
  			}
  			//echo "name : " . OWSEncode($olayer->getMetadata("LAYER_TITLE")) . "(" . $olayer->name . ") type : " . $layerType2 . " opacity : " . $olayer->opacity . " group : " . $providerGrpId . " \n";
  			
  			$layerType = $layerType2;
  			$layerSrc = $layerSrc2;
  			
  			$tabProviderGrp[$providerGrpId][] = &$olayer;
  			//$node->misc["pos"] = $node->misc["pos"] . "___ " . $providerGrpId;
  			$node->misc["providerGrpId"] = $providerGrpId;
  		}
  	}
  	//$providerGrpId++;
  }
  
  // get layer connection type and and a string about its provider
  public function getLayerConnectionInfo($layer, &$connection, &$provider) {
    // Possible Layer connectionType : MS_INLINE, MS_SHAPEFILE, MS_TILED_SHAPEFILE, MS_SDE, MS_OGR, MS_TILED_OGR, MS_POSTGIS, MS_WMS, MS_ORACLESPATIAL, MS_WFS, MS_GRATICULE, MS_MYGIS, MS_RASTER, MS_PLUGIN
	switch ($layer->connectiontype) {
  	case MS_WMS:
  		$connection = CON_WMS;
  		$provider = OWSEncode($layer->getMetadata("wms_onlineresource"));
  		break;
	  default:
		  $connection = CON_CGI;
		  $provider = $this->mapfile;
  	  break;
  	}
  }
  
////////////////////////////////////////////////////////////////////////
// Context merging
////////////////////////////////////////////////////////////////////////  
  private function buildMergingPolicyMapping($mergingPolicyFileName) {
    $mergingPolicyMapping = array();
    // read merging policy file in array 
  	$tab = file($mergingPolicyFileName, FILE_IGNORE_NEW_LINES + FILE_SKIP_EMPTY_LINES);
  	if ($tab != FALSE) {
		// fetch each line of the file 
  		for ($i=1; $i<count($tab); $i++) {
  			$row = explode(',', str_replace("\"", "", $tab[$i]));
  			switch($row[0]) {
  				case "WEB" :
					$mergingPolicyMapping[$this->mdataMapping_map[$row[1]]] = $row[2];
  					break;
  				case "LAYER" :
					$mergingPolicyMapping[$this->mdataMapping_layer[$row[1]]] = $row[2];
  					break;
  				case "CLASS" :
  					$mergingPolicyMapping[$this->mdataMapping_class[$row[1]]] = $row[2];
  					break;
  				case "OTHER" :
					$mergingPolicyMapping[$row[1]] = $row[2];
					break;
  				default:
  					throw new Exception('Unknown merging policy section');
  					break; 
  			} // end switch
  		} // end for
  	} // end if
  	return $mergingPolicyMapping;
  }
  
  private function loadMergingConfigMapping($mergingConfigFileName) {
    $mergingConfigMapping = array();
    // read merging policy file in array 
  	$tab = file($mergingConfigFileName, FILE_IGNORE_NEW_LINES + FILE_SKIP_EMPTY_LINES);
  	if ($tab != FALSE) {
		// fetch each line of the file 
  		for ($i=1; $i<count($tab); $i++) {
  			$row = explode(',', str_replace("\"", "", $tab[$i]));
  			// associate merging elt cfg with xpath node location
			$mergingConfigMapping[$row[0]] = $row[1];	
  		} // end for
  	} // end if
  	return $mergingConfigMapping;
  }
  
  public function mergeWithContext($xmlContextStr, $xmlMapStr, $mergingPolicyFileName="", $mergingConfigFileName="") {
  	$tempname = tempnam(sys_get_temp_dir(), "prodigeFrontcarto_mergeWithContext");
  	$tempfile = fopen($tempname, "w");
  	
    $res = null;
    if ($mergingPolicyFileName=="")
      $mergingPolicyFileName = realpath(dirname(__FILE__)) . '/' . CARMEN_CTX_MERGING_POLICY_FILE;
    if ($mergingConfigFileName=="")
      $mergingConfigFileName = realpath(dirname(__FILE__)) . '/' . CARMEN_CTX_MERGING_CONFIG_FILE;
    
    $mergingPolicyMapping = $this->buildMergingPolicyMapping($mergingPolicyFileName);
    $mergingConfigMapping = $this->loadMergingConfigMapping($mergingConfigFileName);

    $context_doc = @\DOMDocument::loadXML($xmlContextStr);
    
    // checking if current mapfile is coherent with the mapfile the context is generated from
    $context_mapfile = $this->getXmlNodeValue($context_doc, "mapSettings_mapfile");
    if ($context_mapfile && basename($context_mapfile)!= basename($this->mapfile, ".map")) {
      $err_msg = "Le contexte ne semble pas être associé à la carte courante.";
      //echo $err_msg . "<br/>\n";
      trigger_error($err_msg);
      return null;
    }
    
    // loading original map context xml doc
    //$xmlMapStr = $this->buildXML();
    
      
    // removing namespace declaration from the ows context file cos making xpath not working well
    // maybe  an internal bug of php domXpath or a wrong use from myself
    $xmlMapStr = preg_replace('/<ViewContext[^<]*>/', '<ViewContext>', $xmlMapStr);
    $map_doc = @\DOMDocument::loadXML($xmlMapStr);
    $res1	= $map_doc->saveXML();
    
    fwrite($tempfile, "==== SAVE 1 ====\n".$res1);
    
    
    // and xpath associated docs
    $map_xpath_doc = new \DOMXPath($map_doc);
    $context_xpath_doc = new \DOMXPath($context_doc);
    
    
    // merging map metatdata nodes
    foreach ($mergingPolicyMapping as $nodeName => $policy) {
      // retrieving context and map nodes...
      $context_node = null;
      $map_node = null;
      //echo $nodeName . " start<br/>\n";
      if (isset($mergingConfigMapping[$nodeName])) {
        $xpathNodeLocation = $mergingConfigMapping[$nodeName];
        //echo $xpathNodeLocation . "<br/>\n";
        $map_node = $this->getXmlNodeFromXpath($map_xpath_doc, $xpathNodeLocation, null);
        //var_dump($map_node);
        $context_node = $this->getXmlNodeFromXpath($context_xpath_doc, $xpathNodeLocation, null);
        //var_dump($context_node);
      }
      else {
        $context_node = $this->getXmlNode($context_doc, $nodeName, null);
        $map_node = $this->getXmlNode($map_doc, $nodeName, null);
      }
      // and applying strategy
      if ($context_node!=null && $map_node!=null) {
        // node by node replacement strategy
        //echo $policy;
        switch ($policy) {
          case CARMEN_CTX_MERGING_FROM_MAP :
            // overiding context configuration with map configuration
            $this->replaceXmlNode($context_doc, $context_node, $map_node);		
            break;
          case CARMEN_CTX_MERGING_FROM_MIX :
            // applying specific policy
            $this->applyMergingStrategy($nodeName, $context_node, $map_node, $context_xpath_doc, $map_xpath_doc);
            break;
          case CARMEN_CTX_MERGING_FROM_CTX :
          default :
            // keeping context configuration
            break;
        }
        
      }
      else {
        // global treatement strategy 
        if ($policy==CARMEN_CTX_MERGING_FROM_MIX) {
          $this->applyMergingStrategy($nodeName);
        }
      }
      //echo $nodeName . " end<br/>\n";
    }
    
    //reinsert namespace...
    $n_context = $context_doc->getElementsBytagName("ViewContext")->item(0);
    $n_context->setAttribute("xmlns", "http://www.opengeospatial.net/context");
    $n_context->setAttribute("xmlns:gml", "http://www.opengis.net/gml");
    $n_context->setAttribute("xmlns:kml", "http://www.opengis.net/kml/2.2");
    $n_context->setAttribute("xmlns:ogc", "http://www.opengis.net/ogc");
    $n_context->setAttribute("xmlns:ows", "http://www.opengis.net/ows");
    $n_context->setAttribute("xmlns:sld", "http://www.opengis.net/sld");
    $n_context->setAttribute("xmlns:xlink", "http://www.w3.org/1999/xlink");
    $n_context->setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
    $n_context->setAttribute("xsi:schemaLocation", "http://www.opengeospatial.net/context context.xsd");
    
    
    $res = $context_doc->saveXML();
    
    fwrite($tempfile, "==== SAVE 2 ====\n".$res);
    fclose($tempfile);
    
    return $res;
  } 

  private function applyMergingStrategy($strategyName, $c_node=null, 
	$m_node=null, $context_xpath_doc=null, $map_xpath_doc=null) {
	  //echo "Applying specific merging strategy " . $strategyName . "<br/>\n";
	  switch($strategyName) {
		case "SGY_LAYER_MERGE" :
			// removing layers and groups from context that are no more present in map
			// updating layers and group from context that are still present in map
			// adding new layer from map at the end of the layer tree of the context
			//echo "Lauching mergeLayerTreeStrategy1<br/>\n";
			$c_nodeToBeReindexed = $this->mergeLayerTreeStrategy1($c_node, $m_node);
			
			// retieving layer ressources list nodes
			$c_layerListRoot = $this->getXmlNodeFromXpath($context_xpath_doc, '/ViewContext/LayerList');
			$m_layerListRoot = $this->getXmlNodeFromXpath($map_xpath_doc, '/ViewContext/LayerList');
			
			// replacing context layer list by map one so that index in layer tree become coherent with list
			// and add layer added by user in context at the end of the new layer list
			// and finally update index of layer added by user wrt thier poistion in the new layer list
			$this->mergeLayerTreeStrategy2($c_layerListRoot, $m_layerListRoot,$c_nodeToBeReindexed);
			
			// need reindexing the providerGroupId in the merged treenode cos 
			// new layer may have been introduced in the map
			$initialProviderGroupId=0;
      $previousProviderGroupId=null;
			$this->reindexProviderGroupId($c_node, $initialProviderGroupId, $previousProviderGroupId);
		break;
		default:
			//nothing done
		break;
	  }
	   
  }	


  private function mergeLayerTreeStrategy1($c_node, $m_node) {
	$toBeReIndexed = array();	
	//if ($c_node->nodeName=="layerTreeNode") {
		$c_children = $c_node->childNodes;
		$m_children = $m_node->childNodes;
		$replacements = array();
		$removals = array();
    $pendingMapNodeToBeRemoved = array();
		for ($i=0; $i<$c_children->length; $i++) {
			$c = $c_children->item($i);
			if ($c->attributes->getNamedItem("type")->nodeValue=="layer" &&
				$c->attributes->getNamedItem("addedByUser") && 
				$c->attributes->getNamedItem("addedByUser")->nodeValue=="1") {
				// layer node added by user
				// should be kept in context but reordered cos the number of layers 
				// in map may have changed
				//echo "node " . $c->attributes->getNamedItem("name")->nodeValue . " is to be reindexed <br/>\n";
				array_push($toBeReIndexed, $c);
			}
			else {
				if ($c->attributes->getNamedItem("type")->nodeValue=="layer") {
					// layer node
					$c_name = $c->attributes->getNamedItem("name")->nodeValue;
					// checking if the layer from context is still present in map
					$m = $this->getLayerTreeNodeFromNodeList($m_children, $c_name);
					if ($m!=null) {
						// layer node present in map
						// replacing the context node by the map node
						// storing replacement to be done
						array_push($replacements, array("c" => $c, "m" =>$m));
					}
					else {
						// layer node no more present in map
						// node should be removed
						array_push($removals, $c);
					}
				}
				else {
					if ($c->attributes->getNamedItem("type")->nodeValue=="group") {
						// group node
						$c_name = $c->attributes->getNamedItem("name")->nodeValue;
						$m = $this->getLayerTreeNodeFromNodeList($m_children, $c_name, "group");
						if ($m!=null) {
							// group node present in map
							// merging deeper
							$toBeReIndexed = array_merge($toBeReIndexed, $this->mergeLayerTreeStrategy1($c,$m));
              array_push($pendingMapNodeToBeRemoved, $m);
						}
						else {
							// group node no more present in map
							// node should be removed
							array_push($removals, $c);
						}
					}
				}
			}
		}
		
		// making replacements
		for ($i=0;$i<count($replacements);$i++) {
			$replace = $replacements[$i];
			// replacing node from context by node from map
			//echo "Replacing " . $replace["c"]->attributes->getNamedItem("name")->nodeValue . " By " . $replace["m"]->attributes->getNamedItem("name")->nodeValue . "<br/>\n";
			$this->replaceXmlNode($replace["c"]->ownerDocument, $replace["c"], $replace["m"]);
			// removing $m from the map layerTree
			//echo "Removing " . $replace["m"]->attributes->getNamedItem("name")->nodeValue . " From M <br/>\n";
		  if($replace["m"]->parentNode)
			  $replace["m"]->parentNode->removeChild($replace["m"]);
		}
    
    // removing group map nodes that have already been transfered to context
    for ($i=0;$i<count($pendingMapNodeToBeRemoved);$i++) {
      $m = $pendingMapNodeToBeRemoved[$i];
      // removing $m from the map layerTree
      //echo "Removing " . $m->attributes->getNamedItem("name")->nodeValue . " From M <br/>\n";
			$m->parentNode->removeChild($m);
    }		
    
    // making removals
		for ($i=0;$i<count($removals);$i++) {
			$c = $removals[$i];
			// removing $c from the context layerTree
			//echo "Removing " . $c->attributes->getNamedItem("name")->nodeValue . " From C <br/>\n";
			$c->parentNode->removeChild($c);
		}
		
		// adding new map nodes to context layer tree 
		$m_children = $m_node->childNodes;
		for ($i=0;$i<$m_children->length;$i++) {
			$m = $m_children->item($i);
			$newNode = $c_node->ownerDocument->importNode($m, true);
			//echo "adding " . $newNode->attributes->getNamedItem("name")->nodeValue . " To C <br/>\n";
			$c_node->appendChild($newNode);
		}
    
	//}
	return $toBeReIndexed;
  }
  
  
  
  private function mergeLayerTreeStrategy2($c_layerListRoot, $m_layerListRoot, $c_nodeToBeReindexed) {
    //var_dump($c_nodeToBeReindexed);
    $m_layerCount = $m_layerListRoot->childNodes->length;
    // retrieving layer nodes added by user
    for ($i=0; $i<count($c_nodeToBeReindexed); $i++) {
      $c_node = $c_nodeToBeReindexed[count($c_nodeToBeReindexed) - ($i+1)];
      $idx = intval($c_node->attributes->getNamedItem("layerIdx")->nodeValue);
      if ($idx <= $c_layerListRoot->childNodes->length) {
        // retrieving layer node
        $c_layerNode = $c_layerListRoot->childNodes->item($idx);
        
        //echo "Context node " . $c_node->attributes->getNamedItem("name")->nodeValue . "[" . $idx . "] is being reindexed<br/>\n";
        // adding it at the end of the map layer node list
        $newLayerNode = $m_layerListRoot->ownerDocument->importNode($c_layerNode, true);
        //echo "new node " . $newLayerNode->attributes->getNamedItem("name")->nodeValue . "[" . $c_layerNode->attributes->getNamedItem("name")->nodeValue . "] is being inserted<br/>\n";
        $m_layerListRoot->appendChild($newLayerNode);
        // updating the index of the corresponding node in the layerTree
        $c_node->attributes->getNamedItem("layerIdx")->nodeValue=$m_layerCount-1;
        $m_layerCount++;
      }
    }

    
    $m_layerCount = $m_layerListRoot->childNodes->length;
    //echo $m_layerListRoot->childNodes->length;
    //var_dump($m_layerListRoot);
    // keeping layer visibility state from context layer config
    for($i=0; $i<$m_layerCount; $i++) {
      $m_layerNode = $m_layerListRoot->childNodes->item($i);
      if ($m_layerNode->hasAttributes()) {
        $m_layerName = $m_layerNode->attributes->getNamedItem("name")->nodeValue;
        //echo $m_layerName .'<br/>';
        $c_layerNode = $this->getLayerTreeNodeFromNodeList($c_layerListRoot->childNodes, $m_layerName);
        if ($c_layerNode!=null) {
          $c_hidden_value =  $c_layerNode->attributes->getNamedItem("hidden")->nodeValue;
          //echo $c_hidden_value .'<br/>';
          $m_layerNode->attributes->getNamedItem("hidden")->nodeValue = $c_hidden_value;
        }
      }
    }

    // replacing context layer list by map layer list
    $this->replaceXmlNode($c_layerListRoot->ownerDocument, $c_layerListRoot, $m_layerListRoot);
  }
  
  private function reindexProviderGroupId($n_layerTreeNode, &$currentProviderId, &$previousProviderGroupId) {
	$n_children = $n_layerTreeNode->childNodes;
	for ($i=0; $i<$n_children->length; $i++) {
		$n = $n_children->item($i);
		if ($n->attributes && $n->attributes->getNamedItem("type")->nodeValue=="layer" &&
			$n->attributes->getNamedItem("providerGrpId")) {
			// layers added by user have each a different providerGroupId		
			if ($n->attributes->getNamedItem("addedByUser") &&
				$n->attributes->getNamedItem("addedByUser")->nodeValue=="1") {
				
				$n->attributes->getNamedItem("providerGrpId")->nodeValue=$currentProviderId;
				$currentProviderId++;
			}
			else {
				// in case of map existing layers
				// reindex so that layers that have the same providerGroupId 
				// keep the same next providerGroupId
				$layerProviderGroupId = $n->attributes->getNamedItem("providerGrpId")->nodeValue;
				if ($previousProviderGroupId==null) 
					$previousProviderGroupId = $layerProviderGroupId;
				if ($layerProviderGroupId!=$previousProviderGroupId) 
					$currentProviderId++;
				$n->attributes->getNamedItem("providerGrpId")->nodeValue=$currentProviderId;
			}
		}
		else if ($n->attributes && $n->attributes->getNamedItem("type")->nodeValue=="group") {
			$this->reindexProviderGroupId($n, $currentProviderId, $previousProviderGroupId);
		}
	}
  }
	
  private function getLayerTreeNodeFromNodeList($nodeList, $nodeName, $nodeType = "layer") {
	  $res=null;
	  $i=0;
	  while ($i<$nodeList->length && $res==null) {
		$n = $nodeList->item($i);
		if ($n->attributes && $n->attributes->getNamedItem("type")== null) {
      if ($n->attributes->getNamedItem("name")->nodeValue== $nodeName)  {
        $res= $n;
      }
    }
    else {
      if ($n->attributes && $n->attributes->getNamedItem("type")->nodeValue== $nodeType && 
         $n->attributes->getNamedItem("name")->nodeValue== $nodeName)
        $res= $n;
    }
		$i++;
	  }
	  
	  return $res;
  }

  
  private function getXmlNode($doc, $nodeName, $default=null) {
	$res=null;
	$nodeList = $doc->getElementsByTagName($nodeName);
	if ($nodeList->length>0 && $nodeList->item(0)!=null) {
		$res = $nodeList->item(0);
	}
	return $res;
  }
  
  private function getXmlNodeValue($doc, $nodeName, $default=null) {
	$node = $this->getXmlNode($doc, $nodeName,null);
	return $node==null ? $default : $node->nodeValue;
  }
  
  private function getXmlNodeFromXpath($xpathDoc, $xpathQuery, $default=null) {
	$res=null;
	$nodeList = $xpathDoc->query($xpathQuery);
	if ($nodeList->length>0 && $nodeList->item(0)!=null) {
		$res = $nodeList->item(0);
	}
	return $res;
  }
  
  private function replaceXmlNode($doc, $oldNode, $newNode) {
	if ($oldNode!=null) {
		$parent = $oldNode->parentNode;
		if ($parent==null)
			trigger_error("[replaceXmlNode] cannot replace the root node.");
		else {
			$newNode = $doc->importNode($newNode, true);
			$parent->replaceChild($newNode, $oldNode);
		}
	}
  }
  
  // test function for debugging only  
  public function test() {
  	$this->buildLayerTree();  
  	$this->buildProviderGroups();
  	$this->layerTree->prettyPrint();    	
  }


}

