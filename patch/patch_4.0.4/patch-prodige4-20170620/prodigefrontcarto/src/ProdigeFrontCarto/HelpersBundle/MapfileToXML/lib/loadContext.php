<?php
/**
 *
 * @param $strJsContext string json de context
 * @param $mapfile : path to mapfile
 * @param width : pixel width of the map to create
 * @param height : pixel width of the map to create
 * @param mapWidth : pixel width of the navigator map
 * @param exportType : 0 conservation de l'échelle, 1 conservation de l'emprise
 * @return unknown_type
 */
function loadContext($strJsContext, $mapfile, $width, $height, $mapWidth, $exportType=0){
  
  $oMap = @ms_newMapObj($mapfile);
  if (is_null($oMap))
  return false;
  //first of All set print size
  $oMap->setSize($width, $height);
  if($strJsContext=="")
    return $oMap;
  //$strJsContext = stripslashes($strJsContext);
  
  $oContext = json_decode($strJsContext, false);
  
  if(!$oContext)  
    return $oMap;
  
  $xmin = $oContext->ViewContext->General->BoundingBox->attributes->minx;
  $ymin = $oContext->ViewContext->General->BoundingBox->attributes->miny;
  $xmax = $oContext->ViewContext->General->BoundingBox->attributes->maxx;
  $ymax = $oContext->ViewContext->General->BoundingBox->attributes->maxy;
  
  if($exportType==1){//emprise conservée en largeur, adaptation en hauteur, là où les variations sont moins importantes
    
    //calcul du centre
    $Xcenter = ($xmax + $xmin)/2;
    $Ycenter = ($ymax + $ymin)/2;
    $ratio = ($xmax - $xmin)/$width ;

    $ymin = $Ycenter-$ratio*$height/2;
    $ymax = $Ycenter+$ratio*$height/2;
     
  }
  else {//echelle conservée
    ///calcul du ratio en largeur
    $ratio = ($xmax - $xmin)/$mapWidth ;
    //calcul du centre
    $Xcenter = ($xmax + $xmin)/2;
    $Ycenter = ($ymax + $ymin)/2;
    //affectation des extents
    $xmin = $Xcenter-$ratio*$width/2;
    $xmax = $Xcenter+$ratio*$width/2;
    $ymin = $Ycenter-$ratio*$height/2;
    $ymax = $Ycenter+$ratio*$height/2;
  }

  $metadata = $oContext->ViewContext->General->Extension;
  foreach($metadata as $key=>$value){
      if ( is_object($value) ) continue; 
      if ( is_array($value) ) continue; 
      if ( in_array($key, array("MaxBoundingBox")) ){
          continue;
      }
      $key = str_replace(array("mapSettings_"), "", $key);
      $key = str_replace(array("tool_"), "", $key);
      $key = strtoupper($key);
      $value = urldecode($value);
      $oMap->setMetadata($key, $value); 
  }
  
  //load Extent
  
  $oMap->setExtent($xmin,$ymin, $xmax, $ymax);

  //load Layers params
  $Layerlist = $oContext->ViewContext->LayerList->Layer;
  $tabDrawingLayer = array();
  foreach($Layerlist as $idLayer => $oLayer){
    //set LayersOrder
    $layerName = $oLayer->attributes->name;
    $tabDrawingLayer[]= $layerName;
    $oMapservLayer = @$oMap->getLayerByName($layerName);
    if(!$oMapservLayer){
        
      // WMS layer added by user...
      if (isset($oLayer->Extension->wms_layer)) {
        $oMapservLayer = ms_newLayerObj($oMap);
        $oMapservLayer->set("name", "layer".$idLayer);
        $oMapservLayer->set("type", MS_LAYER_RASTER);
        $oMapservLayer->setConnectionType(MS_WMS);
        $oMapservLayer->set("connection", $oLayer->Extension->wms_onlineresource);
        $oMapservLayer->set("status", MS_ON);
        $oMapservLayer->setMetaData("wms_srs", $oLayer->Extension->wms_srs);
        $oMapservLayer->setMetaData("wms_name", $oLayer->Name);
        //$oMapservLayer->setMetaData("wms_title", $params[3]);
        $oMapservLayer->setMetaData("wms_format", $oLayer->Extension->wms_format);
        $oMapservLayer->setMetaData("wms_onlineresource",  $oLayer->Extension->wms_onlineresource);
        $oMapservLayer->setMetaData("wms_server_version", "1.1.1");
      }
      // KML Layer drawn by user...
      else if (isset($oLayer->Document)) {
        $tmp_kml_filename = tempnam(CARMEN_PATH_CACHE, "kml_") . ".kml";
        $oMapservLayer = drawKmlLayer($oMap, 
          $tmp_kml_filename, 
          $oLayer->Document, MS_SHAPE_POLYGON, 
          $oLayer->Extension->layerSettings_olStyle);
      }
      else { //WFS added Layer
      	//var_dump($oLayer->Server);
        $mapUrl = $oLayer->Server->OnLineResource->attributes->url;
        $parsedUrl = parse_url($mapUrl);
        $var  = explode('&', $parsedUrl["query"]);
        foreach($var as $val)
        {
          $x = explode('=', $val);
          $arr[$x[0]] = $x[1];
        }
        if(file_exists($arr["map"])){
          $oMapCopy = ms_newMapObj($arr["map"]);
          if($oMapCopy){
            $oLayerCopy = $oMapCopy->getLayerByName($oLayer->attributes->name);
            $oMapservLayer = ms_newLayerObj($oMap, $oLayerCopy);
            // need to redefine data path in an absolute way
            $newDataLocation = dirname($arr["map"]) . "/" . $oMapservLayer->data;
            $oMapservLayer->set("data", $newDataLocation);
          }
        }
      }
    }
    else {
      // in case of WMS-C, tuiled server url is replaced by no tuiled reference server 
      // cos mapserver does not handled tuiled server
      if ($oMapservLayer->getMetaData("WMTS_WMS_ONLINERESOURCE")!="") {
        $onlineRessource = rawurldecode($oMapservLayer->getMetaData("WMTS_WMS_ONLINERESOURCE"));
        $oMapservLayer->set("connection", $onlineRessource);
        $oMapservLayer->setMetaData("wms_onlineresource", $onlineRessource);
        $wmsLayer = rawurldecode($oMapservLayer->getMetaData("WMTS_WMS_LAYER"));
        $oMapservLayer->setMetaData("wms_name", $wmsLayer);
      }
      if (isset($oLayer->Extension->wmsc_wms_onlineresource)) {
        $onlineRessource = rawurldecode($oLayer->Extension->wmsc_wms_onlineresource);
        $oMapservLayer->set("connection", $onlineRessource);
        $oMapservLayer->setMetaData("wms_onlineresource", $onlineRessource);
      }
      if (isset($oLayer->Extension->wmsc_wms_layer)) {
        $wmsLayer = rawurldecode($oLayer->Extension->wmsc_wms_layer);
        $oMapservLayer->setMetaData("wms_name", $wmsLayer);
      }
    }
   //turn status off/on
    if($oLayer->attributes->hidden == 1)
      $oMapservLayer->set("status", MS_OFF);
    else
      $oMapservLayer->set("status", MS_ON);
  }
  $oMap->setlayersdrawingorder($tabDrawingLayer);
  
  $layerTree = $oContext->ViewContext->General->Extension->LayerTree;
  $asArray = function($tree)use(&$asArray){
      if ( is_object($tree) ){
          $temp = array();
          foreach(get_object_vars($tree) as $var_name=>$var_value){
              $temp[$var_name] = $asArray($var_value);
          }
          $tree = $temp;
      }
      else if ( is_array($tree) ){
          foreach($tree as $var_name=>$var_value){
              $tree[$var_name] = $asArray($var_value);
          }
      }
      return $tree;
  };
  $applyOpacity = function (&$opacities, $tree) use(&$applyOpacity) {
      foreach ($tree as $key=>$node){
          if ( isset($node["attributes"]) && $node["attributes"]["type"]=="group" && isset($node["LayerTreeNode"]) ){
              $applyOpacity($opacities, $node["LayerTreeNode"]);
          }
          if ( isset($node["attributes"]) && $node["attributes"]["type"]=="layer" ){
              $opacities[$node["attributes"]["mapName"]] = $node["attributes"]["opacity"]*100;
          }
      }
  };
  $layerTree = $asArray($layerTree);

  $opacities = array();
  $applyOpacity($opacities, $layerTree);
  
  $oMap->setlayersdrawingorder((array_keys($opacities)));
  $status = array();
  for($iLayer=0; $iLayer<$oMap->numlayers; $iLayer++){
      $oLayer = $oMap->getLayer($iLayer);
      if ( $oLayer ) {
          $status[$oLayer->name] = $oLayer->status;
          $oLayer->set('status', MS_DELETE);
      }
  }
  $tempMapfile = str_replace(".map", "_".uniqid("tmp").".map", $mapfile);
  $oMap->save($tempMapfile);
  
  $oMapTemp = ms_newMapObj($tempMapfile);
  foreach (array_reverse($opacities, true) as $layerName=>$layerOpacity){
      $oLayer = $oMap->getLayerByName($layerName);
      if ( $oLayer ){
          $oLayerTemp = ms_newLayerObj($oMapTemp, $oLayer);
          if ( $oLayerTemp ){
              $oLayerTemp->set("status", $status[$oLayer->name]);
              $oLayerTemp->set("opacity", $layerOpacity);
          }
      }
  }
  //$oMap->save(CARMEN_PATH_CACHE . "/test.map");
  //$oMapTemp->save($tempMapfile);
  @unlink($tempMapfile);
  $oMap = $oMapTemp;
  
  $index = 0;
  
//   foreach (array_reverse($opacities, true) as $layerName=>$layerOpacity){
//       $oLayer = $oMap->getLayerByName($layerName);
//       if ( $oLayer ){
// //           $layerIndex = $oLayer->index;
// //           while( $layerIndex < $index){
// //               $layerIndex++;
// //               $oMap->moveLayerUp($oLayer->index);
// //           }
// //           while( $layerIndex > $index){
// //               $layerIndex--;
// //               $oMap->moveLayerDown($oLayer->index);
// //           }
// //           $index++;
//           $oLayer->set("opacity", $layerOpacity);
//       }
//   }

  if(isset($oContext->ViewContext->General->Extension->Annotation)){
    $annot_config = $oContext->ViewContext->General->Extension->Annotation; 
    
    $annot_file_polygon = CARMEN_PATH_CACHE."/annotation_".rand(0,1000).".kml";
    drawKmlLayer($oMap, 
      $annot_file_polygon, 
      $annot_config[0], 
      MS_SHAPE_POLYGON, 
      $annot_config[3]);

    $annot_file_line = CARMEN_PATH_CACHE."/annotation_".rand(0,1000).".kml";
    drawKmlLayer($oMap, 
      $annot_file_line, 
      $annot_config[1], 
      MS_SHAPE_LINE, 
      $annot_config[3]);

    $annot_file_point = CARMEN_PATH_CACHE."/annotation_".rand(0,1000).".kml";
    drawKmlLayer($oMap, 
      $annot_file_point, 
      $annot_config[2], 
      MS_SHAPE_POINT, 
      $annot_config[3]);
  }
  
  if(isset($oContext->ViewContext->General->Extension->Label)){
    $label_config = $oContext->ViewContext->General->Extension->Label; 
    $tmp_kml_label_filename = tempnam(CARMEN_PATH_CACHE, "kml_label") . ".kml";
    drawKmlLayer($oMap, 
      $tmp_kml_label_filename, 
      $label_config->data, MS_LAYER_POINT, 
      $label_config->style);
  }
  
  //$oMap->save(CARMEN_PATH_CACHE . "/test.map");
  return $oMap;
}

/**
 *
 * @param $oMap
 * @param $annot_file
 * @param $kml
 * @param $type
 * @param $color
 * @return unknown_type
 */
function drawKmlLayer($oMap, $annot_file, $kml, $type, $styleList){
  
  if($kml == "")
    return;

  $fwrite = fopen($annot_file, "w");
  fwrite($fwrite, $kml);
  fclose($fwrite);
  //Insert Annotation Layers
  $olayer = ms_newLayerObj($oMap);
  $olayer->set("type",  $type);
  $olayer->setConnectionType(MS_OGR);
  $olayer->set("status", MS_ON);
  $olayer->set("name", "kml".$type);
  //TODO put the same as OpenLayer
  $olayer->set("opacity", 40);
  $olayer->set("connection", $annot_file);
  $olayer->set("labelcache","off");

  if (isset($styleList->filterRules->map) && 
    is_object($styleList->filterRules->map) && 
    count(get_object_vars($styleList->filterRules->map))>0) {
    $olayer->set("classitem", "description");
    $olayer->set("labelitem", "name");
    foreach(get_object_vars($styleList->filterRules->map) as $key => $style){
      $oClass = ms_newClassObj($olayer);
      //TODO Change No description available
      if($key=="0")
        $oClass->setExpression("No description available");
      else
        $oClass->setExpression($key);
      
      $oClass->set("name", $key);
      //if ($olayer->type!=MS_LAYER_ANNOTATION) {
        $p_oStyle = ms_newStyleObj($oClass);
        $colorRGBArray = hex2rgb($style->fillColor);
        $StrokeColorRGBArray = hex2rgb($style->strokeColor);
  
        //$olayer->set("transparency", round((1.0  - floatval($style->fillOpacity)) * 100));
        
        //$p_oStyle->set("opacity", round(floatval($style->fillOpacity)) * 100);
        $p_oStyle->set("size", max(10, intval($style->strokeWidth)));
        $p_oStyle->set("angle", "360");
        $p_oStyle->outlinecolor->setRGB($StrokeColorRGBArray["rouge"], $StrokeColorRGBArray["vert"], $StrokeColorRGBArray["bleu"]);
        $p_oStyle->color->setRGB($colorRGBArray["rouge"], $colorRGBArray["vert"], $colorRGBArray["bleu"]);
        //$p_oStyle->set("antialias", "true");
        switch($olayer->type){
          case MS_LAYER_POINT:
            $symboleName = isset($style->ms_symbolName) ? $style->ms_symbolName : "Cercle";
            $p_oStyle->set("symbolname", $symboleName);
            break;
          case MS_LAYER_LINE:
            $symboleName = isset($style->ms_symbolName) ? $style->ms_symbolName : "Continue";
            $p_oStyle->set("size", intval($style->strokeWidth));
            $p_oStyle->set("symbolname", $symboleName);
            break;
           case MS_LAYER_POLYGON:
            //$symboleName = isset($style->ms_symbolName) ? $style->ms_symbolName : "Carre";
            //$p_oStyle->set("symbolname", $symboleName);
            break;
          default:
            $symboleName = isset($style->ms_symbolName) ? $style->ms_symbolName : "Carre";
            $p_oStyle->set("symbolname", $symboleName);
            break;
        }
        
        /*$p_oStyleOutLine = ms_newStyleObj($oClass);
        $p_oStyleOutLine->set("size",2);
        $p_oStyleOutLine->set("angle", "360");
        $p_oStyleOutLine->outlinecolor->setRGB($StrokeColorRGBArray["rouge"], $StrokeColorRGBArray["vert"], $StrokeColorRGBArray["bleu"]);
        $p_oStyleOutLine->color->setRGB($colorRGBArray["rouge"], $colorRGBArray["vert"], $colorRGBArray["bleu"]);
        $p_oStyleOutLine->set("antialias", "true");
        $p_oStyleOutLine->set("symbolname", 'ms_area_empty');*/
      //}
      if (isset($style->label)) {
        
        $po_oLabel = new labelObj();
        $po_oLabel->set("font", $style->ms_fontName);
        $colorRGBArray = hex2rgb($style->fontColor);
        $po_oLabel->color->setRGB($colorRGBArray["rouge"], $colorRGBArray["vert"], $colorRGBArray["bleu"]);
        $po_oLabel->set("size", intval($style->fontSize));
        $po_oLabel->set("type", "TRUETYPE");
        $po_oLabel->set("position", MS_UC);
        $po_oLabel->set("offsety", "10");
        $po_oLabel->set("force", "true");
        $po_oLabel->shadowcolor->setRGB(255,255,255);
        $oClass->addLabel($po_oLabel);
        //$po_oLabel->set("position", ol2msPosition($style->labelAlign));
        //echo ol2msPosition($style->labelAlign) . "  ";
      }
      
      
    }
  }
  else {
    $style = $styleList;
    $oClass = ms_newClassObj($olayer);
    $oClass->set("name", "kmlClass");
    $p_oStyle = ms_newStyleObj($oClass);
    $colorRGBArray = hex2rgb($style->defaultStyle->fillColor);
    $StrokeColorRGBArray = hex2rgb($style->defaultStyle->strokeColor);
    $olayer->set("opacity", round((floatval($style->defaultStyle->fillOpacity)) * 100));
    
    $p_oStyle->set("size", max(10, intval($style->defaultStyle->strokeWidth)));
    $p_oStyle->set("angle", "360");
    $p_oStyle->outlinecolor->setRGB($StrokeColorRGBArray["rouge"], $StrokeColorRGBArray["vert"], $StrokeColorRGBArray["bleu"]);
    $p_oStyle->color->setRGB($colorRGBArray["rouge"], $colorRGBArray["vert"], $colorRGBArray["bleu"]);
    switch($olayer->type){
      case MS_LAYER_POINT:
        $p_oStyle->set("symbolname", "Cercle");
        break;
      case MS_LAYER_LINE:
        $p_oStyle->set("size", intval($style->defaultStyle->strokeWidth));
        $p_oStyle->set("symbolname", "Continue");
        break;
      default:
        $p_oStyle->set("symbolname", "Carre");
        break;
    }
  }
  return $olayer;

}

function hex2rgb($color)
{
  if(strlen($color) > 1)
  if($color[0] == '#')
  $color = substr($color, 1);

  if(strlen($color) == 6)
  list($r, $g, $b) = array(
  $color[0].$color[1],
  $color[2].$color[3],
  $color[4].$color[5]
  );
  elseif(strlen($color) == 3)
  list($r, $g, $b) = array(
  $color[0].$color[0],
  $color[1].$color[1],
  $color[2].$color[2]
  );
  else
  return false;

  return array(
    'rouge' => hexdec($r),
    'vert' => hexdec($g),
    'bleu' => hexdec($b)
  );
}


function monthFormat($mois){
  switch($mois) {
    case 'January': $mois = 'Janvier'; break;
    case 'February': $mois = 'Février'; break;
    case 'March': $mois = 'Mars'; break;
    case 'April': $mois = 'Avril'; break;
    case 'May': $mois = 'Mai'; break;
    case 'June': $mois = 'Juin'; break;
    case 'July': $mois = 'Juillet'; break;
    case 'August': $mois = 'Août'; break;
    case 'September': $mois = 'Septembre'; break;
    case 'October': $mois = 'Octobre'; break;
    case 'November': $mois = 'Novembre'; break;
    case 'December': $mois = 'Decembre'; break;
    default: $mois =''; break;
  }
  return $mois;
}


function ol2msPosition($ol_align) {
  switch ($ol_align) {
    case 'lt' : $ms_pos = 'ul'; break;
    case 'lb' : $ms_pos = 'cl'; break;
    case 'lm' : $ms_pos = 'll'; break;
    case 'ct' : $ms_pos = 'uc'; break;
    case 'cb' : $ms_pos = 'cc'; break;
    case 'cm' : $ms_pos = 'lc'; break;
    case 'rt' : $ms_pos = 'ur'; break;
    case 'rb' : $ms_pos = 'cr'; break;
    case 'rm' : $ms_pos = 'lr'; break;
    default: $ms_pos = 'cc'; break; 
  } 
  return $ms_pos;
}







function updateWfsDataSourceFromContext($strXmlContext) {
    
    $xml_doc = new DOMDocument();
    $xml_doc->loadXML($strXmlContext);
    $LayerNodes = $xml_doc->getElementsByTagName("Layer");
    
    for ($i=0; $i<$LayerNodes->length; $i++) {
      $layerNode = $LayerNodes->item($i);
      $layerTypes = $layerNode->getElementsByTagName("layerType");
      if ($layerTypes->length>0) {
        $layerType =$layerTypes->item(0);
        if ($layerType->nodeValue == "mapfile_wfs") {
          
          $wfs_mapfilePath = $layerNode->getElementsByTagName("layerSettings_mapfile")
            ->item(0)->nodeValue;
          $wfs_layername = $layerNode->getElementsByTagName("LAYER_TITLE")
            ->item(0)->nodeValue;
          
          //die("layerType found for layer " . $wfs_layername . " (" . $wfs_mapfilePath . ")");
          updateWfsDataSource($wfs_mapfilePath, $wfs_layername);
        }
      }
    }
}


function updateWfsDataSource($wfs_mapfile, $wfs_layer) {
  $oMap = ms_newMapObj($wfs_mapfile);
  $oLayer = $oMap->getLayerByName($wfs_layer);
  
  $wfs_connection = $oLayer->getMetadata("CMN_WFS_CONNECTION");
  $wfs_layername = $oLayer->getMetadata("CMN_WFS_LAYERNAME");
  $wfs_projection = $oLayer->getMetadata("CMN_WFS_PROJECTION");
  
  
  $shp_filename = dirname($wfs_mapfile) . "/" . $oLayer->data . ".shp"; 
  $shp_projection = $oLayer->getMetadata("CMN_WFS_MAP_PROJECTION");

  if (is_file($shp_filename)) {
    $stats = stat($shp_filename);
    $fileTime = $stats["mtime"];
    $currentTime  = time();
    if ($currentTime > $fileTime + CARMEN_WFS_VALIDITY) {
      
      unlink_shapefile($shp_filename);
      if (!remoteWfsToShape($oMap, 
        $wfs_connection, 
        $wfs_layername, 
        $wfs_projection, 
        $shp_filename, 
        $shp_projection, 
        $featureType))
        //trigger_error("updateWfsDataSource : Unable to update wfs datasource " . $wfs_layername);
        trigger_error("Mise à jour des données WFS impossible pour la couche " . $wfs_layername);
    }
  }
  else
    //trigger_error("updateWfsDataSource : Unable to open wfs datasource " . $shp_filename);
    trigger_error("Problème d'accès aux données WFS de la couche " . $wfs_layername);
}


function unlink_shapefile($shp_filename) {
  $shapepath = dirname($shp_filename);
  $shapename = basename($shp_filename, ".shp");
  
  unlink($shapepath . "/" . $shapename . ".shp");
  unlink($shapepath . "/" . $shapename . ".shx");
  unlink($shapepath . "/" . $shapename . ".dbf");
  unlink($shapepath . "/" . $shapename . ".prj");
}





function remoteWfsToShape($oMap_wfs_temp, $wfs_connection, $wfs_layername, $wfs_projection, $shp_filename, $shp_projection, &$featureType, $OGR2OGR_BIN = "ogr2ogr") {
  $res = true;
  // using mapserver to retrieve wfs layer as a gml file
  $oMap = clone $oMap_wfs_temp;
  $wfslayerObj = ms_newLayerObj($oMap);
  $wfslayerObj->set("name", "wfs_layer");
  $wfslayerObj->setConnectionType( MS_WFS);
  $wfslayerObj->set("connection", $wfs_connection);
  $wfslayerObj->set("status", MS_ON);
  $wfslayerObj->set("template", "CONSULTABLE");
  $wfslayerObj->setMetaData("wfs_request_method",  "GET");
  if (defined("CARMEN_WFS_MAXFEATURE"))
    $wfslayerObj->setMetaData("wfs_maxfeatures", CARMEN_WFS_MAXFEATURE);
  $wfslayerObj->setMetaData("wfs_typename",  $wfs_layername);
  $wfslayerObj->setMetaData("wfs_connectiontimeout", "300");//5minutes
  $wfslayerObj->setMetaData("wfs_version", "1.0.0");
  $wfslayerObj->setProjection(strtolower($wfs_projection));

  $gmlfilename = $wfslayerObj->executeWFSGetfeature();
  //$oMap->save("test.map");
  $gmlFile = new DOMDocument();
  $gmlFile->load($gmlfilename);

  $gmlStr = $gmlFile->saveXML();
  $gmlFile = new SimpleXMLElement($gmlStr);
  $result = $gmlFile->xpath('//gml:featureMember');
  if(count($result) == 0){
    return -1;
  }
  
  // parsing paremeters
  $shapepath = dirname($shp_filename);
  $shapename = basename($shp_filename, ".shp");
  
  // preparing for OGR translation
  $strFormatOut = "ESRI Shapefile";
  $strDataSource = $gmlfilename;
  $strDestDataSource = $shapepath;
  $strDestLayerName = $shapename;

  $ogr_cmd = $OGR2OGR_BIN . ' -f "' . $strFormatOut . 
    '" -t_srs ' . $shp_projection . 
    ' -s_srs ' . $wfs_projection .
    ' -nln ' . $strDestLayerName .
    ' ' . $strDestDataSource .
    ' ' . $strDataSource;

  system($ogr_cmd, $return);
  if ($return!=0) 
    trigger_error("remoteWfsToShape : Unable to perform ogr2ogr cmd : " . $ogr_cmd . "(error " . $return . ")");
    //trigger_error("Aucune données WFS n'a pu être récupérée pour l'étendue et la projection de la carte courante.");
  
  // OGR : registering formats
  OGRRegisterAll();
  
  // OGR : opening datasource
  $hSFDriver = NULL;
  $hDS = OGROpen($shp_filename, FALSE, $hSFDriver );
  if( $hDS == NULL ) 
    //trigger_error("remoteWfsToShape : Unable to open data source " . $shp_filename);
    trigger_error("Ouverture impossible de la couche WFS");
  
  //OGR : getting geometry type
  $hLayer = OGR_DS_GetLayer($hDS,0);
  $hFDefn = OGR_L_GetLayerDefn($hLayer);
  $geomType = OGR_FD_GetGeomType($hFDefn);
  $featureType = OGRgeomTypeToMSgeomType($geomType);  
  return $res;
}




// old version based only on php_ogr
// do not allow to reproject data because no way to create spatial reference system in php_ogr
// but kept here as a good example on how touse php_ogr
function remoteWfsToShape_old($oMap_wfs_temp, $wfs_connection, $wfs_layername, $wfs_projection, $shp_filename, $shp_projection, &$featureType) {
  // using mapserver to retrieve wfs layer as a gml file
  $oMap = $oMap_wfs_temp->clone();
  $wfslayerObj = ms_newLayerObj($oMap);
  $wfslayerObj->set("name", "wfs_layer");
  $wfslayerObj->setConnectionType( MS_WFS);
  $wfslayerObj->set("connection", $wfs_connection);
  $wfslayerObj->set("status", MS_ON);
  $wfslayerObj->set("template", "CONSULTABLE");

  $wfslayerObj->setMetaData("wfs_request_method",  "POST");
  $wfslayerObj->setMetaData("wfs_maxfeatures", "5000");
  $wfslayerObj->setMetaData("wfs_typename",  $wfs_layername);
  $wfslayerObj->setMetaData("wfs_connectiontimeout", "180");
  $wfslayerObj->setMetaData("wfs_version", "1.0.0");
  $wfslayerObj->setProjection("init=".strtolower($wfs_projection));

  $gmlfilename = $wfslayerObj->executeWFSGetfeature();
  
  // parsing paremeters
  $shapepath = dirname($shp_filename);
  $shapename = basename($shp_filename, ".shp");
  
  // preparing for OGR translation
  $strFormatOut = "ESRI Shapefile";
  $strDataSource = $gmlfilename;
  $strDestDataSource = $shapepath;
  $strDestLayerName = $shapename;
  
  // OGR : registering formats
  OGRRegisterAll();
  
  // OGR : opening datasource
  $hSFDriver = NULL;
  $hDS = OGROpen($strDataSource, FALSE, $hSFDriver );
  if( $hDS == NULL ) 
    trigger_error("remoteWfsToShape : Unable to open data source " . $strDataSource);
    
  // OGR : finding output driver 
  $hSFDriver = NULL;
  for( $iDriver = 0;
       $iDriver < OGRGetDriverCount() && $hSFDriver == NULL;
       $iDriver++ ) {
    if (!strcasecmp(OGR_DR_GetName(OGRGetDriver($iDriver)) , $strFormatOut))
      $hSFDriver = OGRGetDriver($iDriver);
  }
    
  if( $hSFDriver == NULL )
    trigger_error( "remoteWfsToShape : Unable to find output driver '" . $strFormatOut . "'");
  if( !OGR_Dr_TestCapability( $hSFDriver, ODrCCreateDataSource ) )
    trigger_error( "remoteWfsToShape : '" . $strFormatOut . "' driver does not support data source creation");

  // OGR : creating output data source
  $hODS = OGR_Dr_CreateDataSource( $hSFDriver, $strDestDataSource, NULL);
  if( $hODS == NULL )
    trigger_error( "remoteWfsToShape : Problems during output data source creation");
    
  // OGR : translating data
  $hLayer = OGR_DS_GetLayer($hDS, 0);

  
  $hSrcSRS = oUTM.SetWellKnownGeogCS( "WGS84" );
  
  $hDestSRS =   
  
  $res = remoteWfsToShape_translate( $hDS, $hLayer, $hODS, $strDestLayerName); 
  if (!$res)
    trigger_error( "remoteWfsToShape : Problems while translating data");

  //OGR : getting geometry type
  $hOLayer = OGR_DS_GetLayer($hODS,0);
  $hFDefn = OGR_L_GetLayerDefn($hOLayer);
  $geomType = OGR_FD_GetGeomType($hFDefn);
  $featureType = OGRgeomTypeToMSgeomType($geomType);  
    
  return $res;
}

/*
 * PHP OGR translate function
 * @param $hSrcDS : ogr src datasource
 * @param $hSrcLayer : ogr src layer
 * @param $hDstDS : ogr dest data source
 * @param $hDstLayerName : ogr dest layer name
 * @return TRUE if the translation could be performed, FALSE elsewhere 
 */ 

function remoteWfsToShape_translate( $hSrcDS, $hSrcLayer, $hDstDS , $hDstLayerName) {

  // OGR : creating destination layer 
  $hFDefn = OGR_L_GetLayerDefn($hSrcLayer);
  $hDstLayer = OGR_DS_CreateLayer( $hDstDS, $hDstLayerName,
                                     OGR_L_GetSpatialRef($hSrcLayer), 
                                     OGR_FD_GetGeomType($hFDefn),NULL );
  if ($hDstLayer==NULL) { 
    trigger_error("remoteWfsToShape_translate : Could not create destination layer");
    return false;
  }
  
  // OGR : adding fields
  for( $iField = 0; $iField < OGR_FD_GetFieldCount($hFDefn); $iField++ ) {
    if( OGR_L_CreateField( $hDstLayer, OGR_FD_GetFieldDefn( $hFDefn, $iField), 
      0 /*bApproOK*/ ) != OGRERR_NONE ) {
      trigger_error("remoteWfsToShape_translate : Unable to create output fields 
        in destination layer");
      return false;
    }
  }
  
  // OGR : transfer features...
  OGR_L_ResetReading($hSrcLayer);
  while( ($hFeature = OGR_L_GetNextFeature($hSrcLayer)) != NULL ) {
    $hDstFeature = OGR_F_Create( OGR_L_GetLayerDefn($hDstLayer) );
    if( OGR_F_SetFrom( $hDstFeature, $hFeature, FALSE /*bForgiving*/ ) 
        != OGRERR_NONE ) {
      trigger_error("remoteWfsToShape_translate : Unable to translate feature " . 
                      OGR_F_GetFID($hFeature) . " from layer " . OGR_FD_GetName($hFDefn));
      OGR_F_Destroy($hFeature);
      return false;
    }
    
    //$hDstFeature ogr_g_transform
    
    
    OGR_F_Destroy($hFeature);
    if( OGR_L_CreateFeature( $hDstLayer, $hDstFeature ) != OGRERR_NONE ) {
      trigger_error("remoteWfsToShape_translate : Unable to create feature " . 
                      OGR_F_GetFID($hDstFeature));
       return false;
    }
    OGR_F_Destroy($hDstFeature);
  }
  return true;
}

function OGRgeomTypeToMSgeomType($ogrGeomType) {
  switch($ogrGeomType) { 
    case wkbPoint:
    case wkbMultiPoint:
    case wkbPoint25D:
    case wkbMultiPoint25D:
      $res = MS_SHAPE_POINT;//"Point";//
      break;
    case wkbLineString:
    case wkbMultiLineString:
    case wkbLineString25D:
    case wkbMultiLineString25D:
      $res = MS_SHAPE_LINE;//"Line";//
      break;
    case wkbPolygon:
    case wkbMultiPolygon:
    case wkbPolygon25D:
    case wkbMultiPolygon25D: 
      $res = MS_SHAPE_POLYGON;//"Polygon";//
      break;    
    default:
      $res = MS_SHAPE_POINT;//"Point other";//
      break;  
  }
  return $res;
}


?>