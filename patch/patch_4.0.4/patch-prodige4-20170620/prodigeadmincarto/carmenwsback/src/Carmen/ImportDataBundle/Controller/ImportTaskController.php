<?php
namespace Carmen\ImportDataBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use JMS\SecurityExtraBundle\Annotation\Secure;

use ProdigeCatalogue\WebBundle\Common\Ressources\Ressources;
use Prodige\ProdigeBundle\Controller\BaseController;
use Carmen\ImportDataBundle\Controller\AbstractImportDataController;
use Symfony\Component\HttpFoundation\ParameterBag;
use Carmen\ApiBundle\Exception\ApiException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Prodige\ProdigeBundle\Services\GeonetworkInterface;

class ImportTaskController extends AbstractImportDataController
{
    const IMPORT_TYPE_SYNCHRO   = "synchronisation";
    const IMPORT_TYPE_AUTOMATE  = "automate";
    const IMPORT_TYPE_DIFFERE   = "import_differe";
    const IMPORT_TYPE_GEOCODAGE = "geocodage";
    
    protected $resultats = array();

    protected function addSuccess($flux, $data){
    	$user_id = $flux["user_id"];
    	$pk_tache = $flux["pk_tache_import_donnees"];
    	if ( !isset($this->resultats[$user_id]) )  $this->resultats[$user_id] = array("success"=>array(), "failure"=>array());
    	if ( !isset($this->resultats[$user_id]["success"][$pk_tache]) ) $this->resultats[$user_id]["success"][$pk_tache] = array();
    	$this->resultats[$user_id]["success"][$pk_tache] = array_merge($this->resultats[$user_id]["success"][$pk_tache], $flux);
    	$this->resultats[$user_id]["success"][$pk_tache]["imports"][$flux["import_table"]] = $data;
    }
    protected function addFailure($flux, $data){
    	$user_id = $flux["user_id"];
    	$pk_tache = $flux["pk_tache_import_donnees"];
    	$import_key = (isset($data["file_source"]) ? $data["file_source"] : $flux["import_table"]);
    	if ( !isset($this->resultats[$user_id]) )  $this->resultats[$user_id] = array("success"=>array(), "failure"=>array());
    	if ( !isset($this->resultats[$user_id]["failure"][$pk_tache]) ) $this->resultats[$user_id]["failure"][$pk_tache] = array();
    	$this->resultats[$user_id]["failure"][$pk_tache] = array_merge($this->resultats[$user_id]["failure"][$pk_tache], $flux);
    	$this->resultats[$user_id]["failure"][$pk_tache]["imports"][$import_key] = $data;
    	
    	unset($this->resultats[$user_id]["success"][$pk_tache]["imports"][$flux["import_table"]]);
    }
    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/prodige/task/import/{uuid}", name="prodige_task_import", options={"expose"=true}, defaults={"uuid"=null})
     */
    public function taskImportAction($uuid=null) {
        $this->curlUseDefaultAuthentication(true);
        
        $this->container->get('prodige.configreader');
        $PRODIGE = $this->getProdigeConnection(self::PRODIGE_SCHEMA_PUBLIC);
        $CATALOGUE = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE);
        
        $constantes = $CATALOGUE->fetchAll("select * from prodige_settings");
        foreach($constantes as $constante){
            if ( !defined($constante["prodige_settings_constant"]) ){
                define($constante["prodige_settings_constant"], $constante["prodige_settings_value"]);
            }
        }
        
        if ( $uuid==null )
            $this->readAutomateIni();
        
        $PRODIGE->isTransactionActive()   && $PRODIGE->commit();
        $CATALOGUE->isTransactionActive() && $CATALOGUE->commit();
        $where = "(task.import_date_execution is null ".
        		 " or ( ".
        		 "  task.import_frequency is not null".
        		 "  and task.import_date_execution::date<=(now()::date-(task.import_frequency||' days')::interval)  ".
        		 " ))";

        if ( $uuid!==null ){
        	$where .= " and uuid=:uuid"; 
        }
        $iOrder = 0;
        $orderBy = "(case when task.import_type=:IMPORT_TYPE_DIFFERE   then ".($iOrder++)." ".
        		            " when task.import_type=:IMPORT_TYPE_AUTOMATE  then ".($iOrder++)." ".
        		            " when task.import_type=:IMPORT_TYPE_SYNCHRO   then ".($iOrder++)." ".
        		            " when task.import_type=:IMPORT_TYPE_GEOCODAGE then ".($iOrder++)." ".
        		            " else ".($iOrder++)." end)";
        $executables = $CATALOGUE->executeQuery(
        		"select task.* ".
        		" from tache_import_donnees task ".
        		" where ".$where." order by ".$orderBy, 
        		array(
        			"uuid" => $uuid,
		        	"IMPORT_TYPE_SYNCHRO"   => self::IMPORT_TYPE_SYNCHRO,
		        	"IMPORT_TYPE_AUTOMATE"  => self::IMPORT_TYPE_AUTOMATE,
		        	"IMPORT_TYPE_DIFFERE"   => self::IMPORT_TYPE_DIFFERE,
		        	"IMPORT_TYPE_GEOCODAGE" => self::IMPORT_TYPE_GEOCODAGE,
		        )
        );
        
        
        $byUser = array();
        foreach ($executables as $flux){
        	$pk_tache_import_donnees = $flux["pk_tache_import_donnees"];
        	$import_type = $flux["import_type"];
        	$flux["original_uuid"] = $flux["uuid"];
        	$uuid = $flux["uuid"];
        	$user_id = $flux["user_id"];
        	$import_table = $flux["import_table"];
        	$import_data_type = $flux["import_data_type"];
        	$import_data_source = $flux["import_data_source"];
        	$flux["import_extra_params"] = (array)json_decode($flux["import_extra_params"], true);
        	
          $flux["geonet_user_id"] = $CATALOGUE->fetchColumn("select p.id from public.users p inner join catalogue.utilisateur c on (c.usr_id=p.username) where c.pk_utilisateur=:user_id", array("user_id"=>$flux["user_id"]));
          if ( $flux["geonet_user_id"]===false ) continue;
	        	
	        $sql = " select m.id ".
	               " from ".self::CATALOGUE_SCHEMA_PUBLIC.".metadata m ".
	               " where m.uuid= :uuid";
	        $flux["fmeta_id"] = $CATALOGUE->fetchColumn($sql, array("uuid"=>$uuid), 0);
          $namespaces = "ARRAY[ARRAY['gmd', 'http://www.isotc211.org/2005/gmd'], ARRAY['gco','http://www.isotc211.org/2005/gco']]";
          $flux["metadata_title"] = $CATALOGUE->fetchColumn("select array_to_string(xpath('/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:citation/gmd:CI_Citation/gmd:title/gco:CharacterString/text()'::text, data::xml, {$namespaces}), ' ') ".
                  " from ".self::CATALOGUE_SCHEMA_PUBLIC.".metadata m ".
                  " where m.uuid= :uuid", array("uuid"=>$uuid), 0);
        	
        	try {
        		
        		if ( $flux["pk_couche_donnees"] ){
        			$_fn_import_type = "ImportCouche";
        		} else {
        			$_fn_import_type = "Synchronisation";
        		}
        		//var_dump($flux, $_fn_import_type);
        	
        		$func = "pre".$_fn_import_type;
        		if ( method_exists($this, $func) ) $this->$func($flux);
        		
        		$func = "do".$_fn_import_type;
        		if ( method_exists($this, $func) ) $this->$func($flux);
        		
        		$func = "post".$_fn_import_type;
        		if ( method_exists($this, $func) ) $this->$func($flux);
        	}
        	catch (\Exception $exception){
		        $PRODIGE->isTransactionActive()   && $PRODIGE->rollBack();
		        $CATALOGUE->isTransactionActive() && $CATALOGUE->rollBack();
		        $resultat = ($exception instanceof ApiException ? $exception->getData() : array("result"=>array("success"=>"FAILURE", "import_status"=>$this->formatJsonError($exception->getMessage())."(".basename($exception->getFile())."[".$exception->getLine()."])")));
		        $this->addFailure($flux, $resultat);
        		//throw new \Exception($exception->getMessage().($exception instanceof ApiException ? print_r($exception->getData(), 1) : ""));
        	}
        	
//         	if ( !isset($byUser[$flux["user_id"]]) ) $byUser[$flux["user_id"]] = array();
//         	$byUser[$flux["user_id"]] = array_merge_recursive($byUser, $resultat); 
        }
        $successfull = array();
        foreach($this->resultats as $user_id=>$resultat){
					//Sends confirmation email with ticket & cgvs through swiftmailer
					$user = $CATALOGUE->fetchAssoc("select usr_nom, usr_prenom, usr_email from utilisateur where pk_utilisateur=:user_id", compact("user_id"));

					$user_fullname = implode(" ", array_diff(array($user["usr_prenom"], $user["usr_nom"]), array("", null)));
					$user_email = $user["usr_email"];
					
				  $hasImport = false;
				  foreach($resultat["success"] as $pk_tache=>$tables){
				    $successfull[] = $pk_tache;
				  	$hasImport = $hasImport || (isset($tables["imports"]) && !empty($tables["imports"]));
				  }
				  if ( !$hasImport )unset($resultat["success"]);
					
	        $tpl = array('CarmenImportDataBundle:Default:emailExecution.html.twig', array(
	          'resultat' => $resultat,
	        	"user_fullname" => $user_fullname,
	        	"GEONETWORK_URL" => rtrim($this->getParameter("PRO_GEONETWORK_URLBASE"), "/")
	        ));
	        //echo call_user_func_array(array($this, "render"), $tpl);
					
        
	        if ( $user_email && \Swift_Validate::email($user_email) ){
    					$message = \Swift_Message::newInstance()
    					  ->setSubject("Tâche planifiée d'import des données SIG - ".date('d/m/Y H:i:s'))
    					  ->setFrom(array(PRO_CATALOGUE_EMAIL_AUTO => PRO_CATALOGUE_NOM_EMAIL_AUTO))
    					  ->setBcc(array(
    					       "m.artigny@alkante.com" => "ENVIRONNEMENT=DEV / ".$user_fullname
    					  ))
    					  ->setTo(
    					  	(false/*set true to debug*/
    					  	? array(
    						  	"m.artigny@alkante.com" => "ENVIRONNEMENT=DEV / ".$user_fullname,
    // 						  	"v.legloahec@alkante.com" => "ENVIRONNEMENT=DEV / ".$user_fullname, 
    // 						  	"b.fontaine@alkante.com" => "ENVIRONNEMENT=DEV / ".$user_fullname,
    						    )
    					  	: array(
    					  	    $user_email => $user_fullname
    					  	  )
    					  	)
    					  )
    					  ->setContentType("text/html")
    					  ->setBody(@$this->renderView($tpl[0], $tpl[1]))
    					;
    
    					$mailer = $this->get('mailer');
    					$mailer instanceof \Swift_Mailer;
    					$mailer->send($message);
	        }
        }

        if ( $uuid!==null ){
        	
        } else {
            
        }
        if ( !empty($successfull)  )
            $CATALOGUE->executeQuery("delete from tache_import_donnees task where import_frequency is null and pk_tache_import_donnees in (".implode(", ", $successfull).")");
        //die(var_dump($byUser));
        if ( $uuid!==null ){
            $current = reset($this->resultats);
            return new JsonResponse($this->resultats, (!empty($current["failure"]) ? JsonResponse::HTTP_INTERNAL_SERVER_ERROR : JsonResponse::HTTP_OK));
        } else {
            return new JsonResponse(array());
        }
    }
    
    #######################################################################

    /**
     * Préparation de l'exécution dans le cas d'un import différé depuis le module d'import de données
     * @param array $flux
     * @throws \Exception
     */
    protected function preImportCouche(array $flux)
    {
        $PRODIGE = $this->getProdigeConnection(self::PRODIGE_SCHEMA_PUBLIC);
        $CATALOGUE = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE);
        
        !$PRODIGE->isTransactionActive()   && $PRODIGE->beginTransaction();
        !$CATALOGUE->isTransactionActive() && $CATALOGUE->beginTransaction();
    }
    /**
     * Exécution de l'import dans le cas d'un import différé depuis le module d'import de données
     * @param array $flux
     * @throws \Exception
     */
    protected function doImportCouche(array $flux)
    {
        $PRODIGE = $this->getProdigeConnection(self::PRODIGE_SCHEMA_PUBLIC);
        $CATALOGUE = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE);
        
        $layertype = $flux["import_data_type"];
        
        $request = new ParameterBag();
        $request->replace($flux["import_extra_params"]);
        $request->set("couchd_emplacement_stockage", $flux["import_table"]);
        $request->set("file_source", $flux["import_data_source"]);
        if ( isset($flux["pk_couche_donnees"]) )
            $request->set("pk_couche_donnees", $flux["pk_couche_donnees"]);
        
        try {
        	$resultat = array();
        	$resultat["file_source"] = $request->get("file_source");
	        switch ( $layertype ){
	        	case "VECTOR" :
	        	case "TABULAIRE" :
	        	case "MNT" :
	        	  if ( $request->get('import_action')=="update" && !$request->get('field_key') ){
	        	      $request->set('import_action', "replace");
	        	  }
	        		$resultat = array_merge($resultat, $this->doImportInPostgis($request, $flux["fmeta_id"], $layertype, false));
	        		
	        	break;
	        	case "RASTER" :
	        		$resultat = array_merge($resultat, $this->doImportRaster($request, $flux["fmeta_id"], $layertype, false));
	        	break;
	        }
	        $this->addSuccess($flux, $resultat);
        } catch (\Exception $exception){
	        $PRODIGE->isTransactionActive()   && $PRODIGE->rollBack();
	        $CATALOGUE->isTransactionActive() && $CATALOGUE->rollBack();
	        $resultat = array_merge($resultat, ($exception instanceof ApiException ? $exception->getData() : array("result"=>array("success"=>"FAILURE", "import_status"=>$exception->getMessage()))));
	        $this->addFailure($flux, $resultat);
        }
    }
    /**
     * Post-traitements de l'exécution dans le cas d'un import différé depuis le module d'import de données
     * @param array $flux
     * @throws \Exception
     */
    protected function postImportCouche(array $flux)
    {
        $PRODIGE = $this->getProdigeConnection(self::PRODIGE_SCHEMA_PUBLIC);
        $CATALOGUE = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE);
        
        $PRODIGE->isTransactionActive()   && $PRODIGE->commit();
        $CATALOGUE->isTransactionActive() && $CATALOGUE->commit();
    }
    
    #######################################################################
    
    /**
     * Préparation de l'exécution dans le cas d'un import suite à l'exécution de l'automate
     * @param array $flux
     * @throws \Exception
     */
    protected function readAutomateIni()
    {
        $defaults = $this->container->getParameter('jsdefaults', array());
        $PRODIGE = $this->getProdigeConnection(self::PRODIGE_SCHEMA_PUBLIC);
        $CATALOGUE = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE);
        
        $directory = PRO_ROOT_FILE_AUTOMATE;
        $files = array();
        exec("find ".$directory." -type f -name '*.ini' -not -iwholename '*/archives/*'", $files);
        
        !$CATALOGUE->isTransactionActive() && $CATALOGUE->beginTransaction();
        
        $type_stockage = array();
        $i=0;
        // traduction de type numérique de catalogue.couche_donnees en type text attendu par catalogue.tache_import_donnees
        foreach(self::$DATATYPE_TO_STOCKAGE as $stockage=>$datatype){
        	$type_stockage['(:couchd_type_stockage'.$i.', :import_data_type'.$i.')'] = array('couchd_type_stockage'.$i=>intval($stockage), 'import_data_type'.$i=>strtoupper($datatype));
        	$i++;
        }
        $headers = array("import_table", "import_data_source", "user_mail", "data_datemaj", "checksum");
        foreach($files as $fileIni){
        	$file = fopen($fileIni, "r");
        	while ( $csv = fgetcsv($file, null, '"') ){
        		if ( empty($csv) ) continue;
        		$csv = array_map("trim", $csv);
        		if ( count($csv)!=count($headers) ) continue;
        		$csv = array_combine($headers, $csv);
        		$csv["import_data_source"] = rtrim(dirname($fileIni), '/')."/".$csv["import_data_source"];
        		unset($csv["checksum"]);
        		unset($csv["data_datemaj"]);
        		
		        $data = array(
		        		"import_table" => ":import_table",
		        		"import_data_source" => ":import_data_source",
		        		"user_id" => "users.pk_utilisateur as user_id",
		        		"uuid" => "meta.uuid",
		        		"import_type" => "'automate' as import_type",
		        		"import_frequency" => " null as import_frequency",
		        		"import_data_type" => "datatype.import_data_type",
		        		"pk_couche_donnees" => "couche.pk_couche_donnees",
		        );
		        $inserts = $CATALOGUE->executeQuery(
		        		"insert into tache_import_donnees (".implode(", ", array_keys($data)).")".
		        		" select distinct on (usr_email) ".implode(", ", $data)." ".
		        		" from utilisateur users, couche_donnees couche ".
		        		" inner join fiche_metadonnees fmeta on (fmeta.fmeta_fk_couche_donnees=couche.pk_couche_donnees) ".
		        		" inner join public.metadata meta on (meta.id=fmeta.fmeta_id::int) ".
		        		" inner join (values".implode(', ', array_keys($type_stockage)).") as datatype (couchd_type_stockage, import_data_type) on (datatype.couchd_type_stockage::int=couche.couchd_type_stockage)".
		        		" where users.usr_email = :user_mail and couche.couchd_emplacement_stockage=:import_table".
		        		" returning *",
		        		array_merge($csv, array_reduce($type_stockage, "array_merge", array()))
		        );
        	
		        foreach($inserts as $tache){
		        	$layertype = strtoupper($tache["import_data_type"]);
		        	$couchd_type_stockage = array_search(strtolower($layertype), self::$DATATYPE_TO_STOCKAGE);
		        	$pk_couche_donnees = $tache["pk_couche_donnees"];
		        	$pk_tache_import_donnees = $tache["pk_tache_import_donnees"];
		        	$couchd_emplacement_stockage = $tache["import_table"];
		        	$layerfile = $tache["import_data_source"];
		        	$caracteristiques = $this->getGISFileProperties($pk_couche_donnees, $layerfile, $couchd_type_stockage, null, $layertype, $couchd_emplacement_stockage, $directory);
		        	$extraparams = array(
		          	"encoding_source"    => $caracteristiques["encoding"],
		          	"projection_cible"   => $defaults["PROJECTION"],
		          	"projection_source"  => 'auto',
		          	"import_action"      => 'replace',
		          	"field_key"          => '',
		          	"file_georeferenced" => false,
		          	"file_sheet"         => '',
	          	  "layerfields"        => array("drops"=>array(), "types"=>$caracteristiques["layerfields"], "xField"=>null, "yField"=>null),
		          	"couchd_type_stockage"  => $couchd_type_stockage,
		          	"pk_couche_donnees"  => $pk_couche_donnees,
		          );
		        	$extraparams = array_merge(
		        	   array(
		        	     "couchd_nom" => "Couche non définie",
		        	   ),
        	       $CATALOGUE->fetchAssoc("select * from couche_donnees where pk_couche_donnees=:pk_couche_donnees", array("pk_couche_donnees"=>$pk_couche_donnees)) ?: array(),
        	       $extraparams
		        	);
		        	
		        	$CATALOGUE->executeQuery(
		        	"update tache_import_donnees set import_extra_params=:import_extra_params where pk_tache_import_donnees=:pk_tache_import_donnees", array(
		        			"pk_tache_import_donnees" => $pk_tache_import_donnees,
		        			"import_extra_params" => json_encode($extraparams)
		        	));
		        }
        	}
        }
        $CATALOGUE->isTransactionActive() && $CATALOGUE->commit();
    }
    
    
    #######################################################################
    
    /**
     * Préparation de l'exécution dans le cas d'un import de métadonnées moissonnées
     * @param array $flux
     * @throws \Exception
     */
    protected function importWFS(array &$flux)
    {
        $request = $this->getRequest();
        $fmeta_id = $flux["fmeta_id"];
        $uuid = $flux["uuid"];
        
        $PRODIGE = $this->getProdigeConnection(self::PRODIGE_SCHEMA_PUBLIC);
        $CATALOGUE = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE);
        
        !$CATALOGUE->isTransactionActive() && $CATALOGUE->beginTransaction();
        $url = $flux["import_data_source"];
        $urlquery = parse_url($flux["import_data_source"], PHP_URL_QUERY);
        $url = str_replace($urlquery, "", $url);
        $tmp = array(); parse_str($urlquery, $tmp); $urlquery = $tmp;
        $urlquery = array_merge($urlquery, array("request"=>"GetFeature"), $flux["import_extra_params"]);
        
        $url = rtrim($url, "?")."?".http_build_query($urlquery);
        
        if ( !$flux["pk_couche_donnees"] ){
            // ajout d'une nouvelle couche
            $flux["pk_couche_donnees"] = $pk_couche_donnees = $CATALOGUE->fetchColumn("select nextval('seq_couche_donnees'::regclass)", array(), 0);
            $couchd_nom = $flux["metadata_title"]." - Couche synchronisée";
            $couchd_type_stockage = 1;
            $couchd_emplacement_stockage = $flux["import_table"];
            $couchd_id = (string)$pk_couche_donnees;
            $couchd_visualisable = 0;
            $couchd_fk_acces_server = 1;
            $coucheFields = array("pk_couche_donnees", "couchd_id", "couchd_nom", "couchd_type_stockage", "couchd_emplacement_stockage", "couchd_visualisable", "couchd_fk_acces_server");
          
            //force value to 1
            $CATALOGUE->executeQuery("insert into couche_donnees (".implode(", ", $coucheFields).") values (:".implode(", :", $coucheFields).")", compact($coucheFields));
            $CATALOGUE->executeQuery("insert into fiche_metadonnees (fmeta_id, fmeta_fk_couche_donnees) values (:fmeta_id, :pk_couche_donnees)", compact("fmeta_id", "pk_couche_donnees"));
            if ( $flux["import_frequency"] )
                $CATALOGUE->executeQuery("update tache_import_donnees set pk_couche_donnees=:pk_couche_donnees where pk_tache_import_donnees=:pk_tache_import_donnees", $flux);
        }
        $data = array(
            "file_source" => $url,
            "couchd_type_stockage" => "1",
            "couchd_emplacement_stockage" => $flux["import_table"],
            "couchd_nom" => $flux["import_table"],
            "pk_couche_donnees" => $flux["pk_couche_donnees"]
        );
        $parameters = new ParameterBag();
        $parameters->replace($data);
        $result = array();
        $result["pk_couche_donnees"] = $flux["pk_couche_donnees"];
        $result["file_source"] = $url;
        $result["datatype"] = "WFS";
        $result = array_merge($result, $this->doImportInPostgis($parameters, $fmeta_id, "VECTOR"));
        return $result;
    }
    
    /**
     * Préparation de l'exécution dans le cas d'un import de métadonnées moissonnées
     * @param array $flux
     * @throws \Exception
     */
    protected function importATOM(array &$flux)
    {
        $request = $this->getRequest();
        $fmeta_id = $flux["fmeta_id"];
        $uuid = $flux["uuid"];
        
        $PRODIGE = $this->getProdigeConnection(self::PRODIGE_SCHEMA_PUBLIC);
        $CATALOGUE = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE);
        
        !$CATALOGUE->isTransactionActive() && $CATALOGUE->beginTransaction();
    
        $rootDir = $this->getMapfileDirectory()."/temp/";
        
        $filezip = $rootDir.$uuid.".zip";
        file_exists($filezip) && @unlink($filezip);
        $file = fopen($filezip, "a");
        
        $curl = curl_init($flux["import_data_source"]);
        curl_setopt_array($curl, array(
            CURLOPT_BINARYTRANSFER => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CONNECTTIMEOUT => 30,
            CURLOPT_FILE => $file, 
        ));
        curl_exec($curl);
        file_exists($filezip);
        
        $layertypes = array("VECTOR");
        $couches = array();
        foreach ($layertypes as $layertype){
            $couches = array_merge($couches, $this->analyseZipFile($filezip, $layertype));
        }
        
        $max_length = $PRODIGE->fetchColumn("show max_identifier_length");
        $import_table = rtrim($flux["import_table"], "_");
        
        foreach($couches as $couche){
        	try {
            $result = array();
            $couche_file = str_replace($rootDir.$uuid."/", "", $couche["file"]);
            if ( isset($flux["import_extra_params"][$couche_file]) ){
            	$dbdata = $flux["import_extra_params"][$couche_file];
            	if ( isset($dbdata["pk_couche_donnees"]) ) $pk_couche_donnees = $dbdata["pk_couche_donnees"];
            	$couche["couchd_nom"] = $dbdata["couchd_nom"] = $CATALOGUE->fetchColumn("select couchd_nom from couche_donnees where pk_couche_donnees=:pk_couche_donnees", array("pk_couche_donnees"=>$pk_couche_donnees));
            } else {
                $rs = $CATALOGUE->executeQuery(
                    "select cche.pk_couche_donnees ".
                    " from couche_donnees cche ".
                    " inner join fiche_metadonnees fmeta on (cche.pk_couche_donnees=fmeta.fmeta_fk_couche_donnees)".
                    " where fmeta.fmeta_id=:fmeta_id and cche.couchd_emplacement_stockage=:stockage"
                , array(
                    "fmeta_id" => $flux["fmeta_id"],
                    "stockage" => substr(strtolower($import_table."_".$couche["stockage"]), 0, $max_length)
                ));
                $pk_couche_donnees = $rs->fetchColumn(0);
                //création de la couche de donnée associée
                if ( !$pk_couche_donnees ){
                    $pk_couche_donnees = $CATALOGUE->fetchColumn("select nextval('seq_couche_donnees'::regclass)", array(), 0);
                    $couchd_emplacement_stockage = substr(strtolower($import_table."_".$couche["stockage"]), 0, $max_length);
                    list($unique, $table_exists) = array_values($this->isUniqueStorageAction($couchd_emplacement_stockage, $pk_couche_donnees));
                    $i = 1;
                    while ( !$unique && $i<100 ){
                        $couchd_emplacement_stockage = substr(strtolower($import_table."_".$couche["stockage"]), 0, $max_length-strlen("_".$i))."_".($i++);
                        list($unique, $table_exists) = array_values($this->isUniqueStorageAction($couchd_emplacement_stockage, $pk_couche_donnees));
                    }
                    $couche["stockage"] = $couchd_emplacement_stockage;
                    $couchd_id = (string)$pk_couche_donnees;
                    $dbdata = array(
                        "pk_couche_donnees" => $pk_couche_donnees,
                        "couchd_id" => $couchd_id,
                        "couchd_nom" => $couche["couchd_nom"],
                        "couchd_description" => "Couche synchronisée et initialisée le ".date("d/m/Y à H:i:s")." par lecture de l'adresse ".$flux["import_data_source"],
                        "couchd_type_stockage" => $couche["type_stockage"],
                        "couchd_emplacement_stockage" => $couchd_emplacement_stockage,
                        "couchd_fk_acces_server" => 1,
                    );
                    $couchedFields = array_keys($dbdata);
                        
                    $CATALOGUE->executeQuery("insert into couche_donnees (".implode(", ", $couchedFields).") values (:".implode(", :", $couchedFields).")", $dbdata);
                    $CATALOGUE->executeQuery("insert into fiche_metadonnees (fmeta_id, fmeta_fk_couche_donnees) values (:fmeta_id, :pk_couche_donnees)", compact("fmeta_id", "pk_couche_donnees"));
                        
                } else {
                    $dbdata = array(
                        "pk_couche_donnees" => $pk_couche_donnees,
                        "couchd_nom" => $couche["couchd_nom"],
                        "couchd_emplacement_stockage" => substr(strtolower($import_table."_".$couche["stockage"]), 0, $max_length),
                    );
                }
            }
            $data = array_merge($dbdata, array(
                "import_status" => "Configuration partielle par import ZIP",
                "file_source" => $couche["file"],
                "file_name" => str_replace($rootDir, "", $couche["file"]),
                "autoLoad" => true,
                "import_action" => "create",
            ));
            $flux["import_table"] = $data["couchd_emplacement_stockage"];
            
            // lecture des champs du fichier
            $structure = $this->getGISFileProperties($pk_couche_donnees, urlencode(str_replace($rootDir, "", $couche["file"])), $couche["type_stockage"], null, "VECTOR", $dbdata["couchd_emplacement_stockage"], $rootDir);
            
            
            $structure["layerfields"] = (isset($structure["layerfields"]) ? $structure["layerfields"] : array());
            $is_georeferenced = false;
            $data["sheets"] = $structure["sheets"];
            foreach($structure["layerfields"] as $iField=>$field){
                $structure["layerfields"][$iField]["field_visible"] = true;
                $is_georeferenced = $is_georeferenced || (!$couche["georeferenced"] || in_array($field["field_datatype"], array("integer", "bigint", "float")));
            }
            $data["layerfields"] = array("types"=>$structure["layerfields"], "drops"=>array(), "xField"=>null, "yField"=>null);
            $data["file_georeferenced"] = $is_georeferenced;
            $data["encoding_source"] = $structure["encoding"] ?: "UTF-8";
        
            $parameters = new ParameterBag();
            $parameters->replace($data);
            $result["pk_couche_donnees"] = $data["pk_couche_donnees"];
            $result["datatype"] = "ATOM";
            $result["file_source"] = str_replace($rootDir.$uuid."/", $flux["import_data_source"]." :: ", $data["file_source"]);
            $result = array_merge($result, $this->doImportInPostgis($parameters, $fmeta_id, $layertype));
            $this->addSuccess($flux, $result);
        	} catch (\Exception $exception){
		        $resultat = array_merge($result, ($exception instanceof ApiException ? $exception->getData() : array("result"=>array("success"=>"FAILURE", "import_status"=>$exception->getMessage()."<br>(".basename($exception->getFile())."[".$exception->getLine()."])"))));
		        $this->addFailure($flux, $resultat);
	        }
        }
        
       // @exec("rm -rf ".$rootDir.$uuid."*");
        
        return null;
    }
    
    /**
     * Préparation de l'exécution dans le cas d'un import de métadonnées moissonnées
     * @param array $flux
     * @throws \Exception
     */
    protected function preSynchronisation(array &$flux)
    {
        $PRODIGE = $this->getProdigeConnection(self::PRODIGE_SCHEMA_PUBLIC);
        $CATALOGUE = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE);
        
        $fmeta_id = $flux["fmeta_id"];
        $uuid = $flux["uuid"];
        $origin_uuid = $uuid;
    
        $geonetwork = $this->getGeonetworkInterface();
        $groups = $geonetwork->get("/api/0.1/users/".$flux["geonet_user_id"]."/groups", false, array(CURLOPT_HTTPHEADER=>array('Accept:application/json, text/plain, */*')));
        
        $groups = (is_string($groups) ? json_decode($groups, true) : $groups);
        if ( !is_array($groups) ) $groups = (empty($groups) ? array() : array($groups));
        
        $pk_sous_domaine = false;
        $group_id = $geonetwork->getMetadataGroupOwner($CATALOGUE, $fmeta_id);
        foreach ($groups as $group){
            if ( !is_array($group) ) continue;
            if ( in_array($group["id"]["profile"], array("Editor", "Administrator")) ){
                $pk_sous_domaine = $CATALOGUE->fetchColumn("select pk_sous_domaine from sous_domaine where ssdom_id=:ssdom_id", array("ssdom_id"=>$group["group"]["name"]));
                if ( $pk_sous_domaine!==false ){
                    $group_id = $group["id"]["groupId"];
                    break;
                }
            }
        }
        
        if ( !$flux["import_frequency"] ){
            $relatedid = null;
            $relatedUuid = null; 
            /*
            * Validation de la transaction puis redirection vers la duplication de la fiche geonetwork (qui génÃ¨rera la fiche_metadonnees)
            */
            try{
                $errormsg = "Echec de la duplication / Geosource indisponible";
                $json = $geonetwork->createMetadata($fmeta_id, $group_id, "n", "n", "n");
                $json_init = $json;
                $json = (is_string($json) ? json_decode($json) : $json);
                if( $json &&  $json->id  ){
                    $flux["fmeta_id"] = $json->id;
                    
                    $query = "select uuid, data from public.metadata where id=:id";
                    list($new_uuid, $data) = $CATALOGUE->fetchArray($query, array("id"=>$flux["fmeta_id"]));
                    $flux['uuid'] = $new_uuid;
                    
                    // change the metadata owner and group
                    $CATALOGUE->executeQuery("update public.metadata set owner=:user_id, groupowner=:group_id where id=:id", array("id"=>$flux["fmeta_id"], "user_id"=>$flux["geonet_user_id"], "group_id"=>$group_id));

                    $CATALOGUE->isTransactionActive() && $CATALOGUE->commit();

                    // update the xml definition of the metadata 
                    $version  = "1.0";
                    $encoding = "UTF-8";
                    $entete   = "<?xml version=\"".$version."\" encoding=\"".$encoding."\"?>\n";
                    $data = str_replace("&", "&amp;", $data);
                    $data = $entete.$data;
                    $doc = new \DOMDocument($version, $encoding);
                    if($doc->loadXML($data)) {
                    	  $xpath = new \DOMXPath($doc);
                        // modifie le nom
                        $nodes = $xpath->query('//gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:citation/gmd:CI_Citation/gmd:title/gco:CharacterString');
                        if ( $nodes->length>0 ){
                            $node = $nodes->item(0);
                            $flux["metadata_title"] = $node->nodeValue = "Copie figée au ".date('d/m/Y')." de ".$node->nodeValue;
                        }
                        
                        // modifie la date
                        $nodes = $xpath->query('//gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:citation/gmd:CI_Citation/gmd:date/gmd:CI_Date/gmd:date/gco:DateTime');
                        if ( $nodes->length>0 ){
                            $node = $nodes->item(0);
                            $node->nodeValue = date('Y-m-d\TH:i:s');
                        }
                        
                        // modifie la qualité
                        $nodes = $xpath->query('//gmd:MD_Metadata/gmd:dataQualityInfo/gmd:DQ_DataQuality/gmd:lineage/gmd:LI_Lineage/gmd:statement/gco:CharacterString');
                        if ( $nodes->length>0 ){
                            $node = $nodes->item(0);
                            $node->nodeValue .= "\n\n- URL de la métadonnée de la ressource copiée : ".rtrim(PRO_GEONETWORK_URLBASE, "/")."/srv/fre/catalog.search#/metadata/".$origin_uuid.
                                                "\n- URI de la ressource : ".$origin_uuid.
                                                "\n- Date de la copie : ".date('d/m/Y');
                        }
                        
                        // empty these nodes
                        $deletedNodes = array(
                            '//gmd:MD_Metadata/gmd:fileIdentifier/gco:CharacterString',
                            '//gmd:MD_Metadata/gmd:parentIdentifier/gco:CharacterString',
                            '//gmd:MD_Metadata/gmd:contact/gmd:CI_ResponsibleParty/gmd:organisationName/gco:CharacterString',
                            '//gmd:MD_Metadata/gmd:contact/gmd:CI_ResponsibleParty/gmd:contactInfo/gmd:CI_Contact/gmd:address/gmd:CI_Address/gmd:electronicMailAddress/gco:CharacterString',
                            '//gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:citation/gmd:CI_Citation/gmd:identifier/gmd:MD_Identifier/gmd:code/gco:CharacterString',
                            '//gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:pointOfContact/gmd:CI_ResponsibleParty/gmd:organisationName/gco:CharacterString',
                            '//gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:pointOfContact/gmd:CI_ResponsibleParty/gmd:contactInfo/gmd:CI_Contact/gmd:address/gmd:CI_Address/gmd:electronicMailAddress/gco:CharacterString',
                            '//gmd:MD_Metadata/gmd:distributionInfo/gmd:MD_Distribution/gmd:transferOptions/gmd:MD_DigitalTransferOptions/gmd:onLine/gmd:CI_OnlineResource/gmd:linkage/gmd:URL',
                            '//gmd:MD_Metadata/gmd:distributionInfo/gmd:MD_Distribution/gmd:transferOptions/gmd:MD_DigitalTransferOptions/gmd:onLine/gmd:CI_OnlineResource/gmd:protocol/gco:CharacterString',
                        );
                        foreach($deletedNodes as $path){
                            $nodes = $xpath->query($path);
                            for($i=0; $i<$nodes->length; $i++){
                                $nodes->item($i)->nodeValue = "";
                            }
                        }
                        
                        //save
                        $newData = $doc->saveXML();
                        $geonetwork->editMetadata($flux["fmeta_id"], $newData);
                        
                        // affect the domain/subdomain to the metadata
                        if ( $pk_sous_domaine ){
                            $this->updateDomSDomMetadata($flux["fmeta_id"], array($pk_sous_domaine));
                        }
                    } else {
                        throw new \Exception("Impossible de lire la métadonnée {$uuid}");
                    }
                    
                } else {
                    throw new \Exception("Impossible de dupliquer la métadonnée {$uuid}");
                }
            }catch(\Exception $exception){throw new \Exception($errormsg, 500, $exception);}
        } else {
            // change the metadata owner and group
            $CATALOGUE->executeQuery("update public.metadata set isharvested='n' where uuid=:uuid", array("uuid"=>$flux["uuid"]));
            $geonetwork->editMetadata($flux["fmeta_id"], $CATALOGUE->fetchColumn("select data from public.metadata where uuid=:uuid", array("uuid"=>$flux["uuid"])));
        
            // affect the domain/subdomain to the metadata
            if ( $pk_sous_domaine ){
                $this->updateDomSDomMetadata($flux["fmeta_id"], array($pk_sous_domaine));
            }
            
        }
    }
    
    protected function _(array &$flux)
    {
    	$namepspaces = array(
    		"gmd" => 'http://www.isotc211.org/2005/gmd'
    	);
    	$sql_namespaces = array();
    	foreach($namepspaces as $alias=>$path){
    		$sql_namespaces[] = "{".$alias.",".$path."}";
    	}
    	$sql_namespaces = "{".implode(",", $sql_namespaces)."}";
    	$nodeDataset = '//gmd:MD_Metadata/gmd:hierarchyLevel/gmd:MD_ScopeCode[@codeListValue="dataset"]';
    	$nodeVector = '//gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:spatialRepresentationType/gmd:MD_SpatialRepresentationTypeCode[@codeListValue="vector"]';
    	$nodeTitle = '//gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:citation/gmd:CI_Citation/gmd:title/gco:CharacterString';
        $PRODIGE = $this->getProdigeConnection(self::PRODIGE_SCHEMA_PUBLIC);
        $CATALOGUE = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE);
        
        $fmeta_id = $flux["fmeta_id"];
        $uuid = $flux["uuid"];
        
        !$CATALOGUE->isTransactionActive() && $CATALOGUE->beginTransaction();
        if ( !$flux["import_frequency"] ){
            $geonetwork = new GeonetworkInterface();
            $group_id = $geonetwork->getMetadataGroupOwner($CATALOGUE, $fmeta_id);
                
            $relatedid = null;
            $relatedUuid = null; 
            /*
            * Validation de la transaction puis redirection vers la duplication de la fiche geonetwork (qui génÃ¨rera la fiche_metadonnees)
            */
            try{
            	//Template de métadonnée de type dataset+vector
            	$where = array(
            		"istemplate='y'", 
            		"schemaid='iso19139'",
            		"xpath_exists(:nodeDataset, data::xml, :sql_namespaces ::text[][])",
            		"xpath_exists(:nodeVector , data::xml, :sql_namespaces ::text[][])",
            	);
            	$template_id = $CATALOGUE->fetchColumn("select id from public.metadata where ".implode(" and ", $where), compact('nodeDataset', 'nodeVector', 'sql_namespaces'));
            	if ( !$template_id ){
            		throw new \Exception("Impossible de trouver le template de métadonnée pour les séries de données vectorielles.");
            	}
            
                $errormsg = "Echec de la duplication / Geosource indisponible";
                $json = $geonetwork->createMetadata($template_id, $group_id, "n", "n", "n");
                $json = (is_string($json) ? json_decode($json) : $json);
                if( $json &&  $json->id  ){
                    $metadata_id = $json->id;
                    //définit cette nouvelle métadonnée comme métadonnée enfant de l'ensemble de série de donnée
                    $geonetwork->metadataProcessing("parent-add", $metadata_id, array("parentUuid"=>$uuid));
			              $parent_data = $CATALOGUE->fetchColumn("select data from public.metadata where id=:id", array("id"=>$fmeta_id));
			              $data = $CATALOGUE->fetchColumn("select data from public.metadata where id=:id", array("id"=>$metadata_id));
			              
			              $doc = new \DOMDocument();
			              $doc->loadXML($parent_data);
			              $parent_data = $doc;
			              $xpath = new \DOMXPath($parent_data);
			              $parentNodes = $xpath->query($nodeTitle);
			              
			              $doc = new \DOMDocument();
			              $doc->loadXML($data);
			              $data = $doc;
			              $xpath = new \DOMXPath($data);
			              $nodes = $xpath->query($nodeTitle);
			              
										if (!is_null($nodes)) {
										  foreach ($nodes as $i=>$node) {
										  	$parentNode = $parentNodes->item($i);
										  	if ($parentNode){
										  		$node->nodeValue = $parentNode->nodeValue;
										  	}
										  }
										}
										$geonetwork->editMetadata($metadata_id, $data->saveXML(), true);
			              
			            	if ( !$template_id ){
			            		throw new \Exception("Impossible de trouver le template de métadonnée pour les séries de données vectorielles.");
			            	}
                    $flux["fmeta_id"] = $metadata_id;
                    $query = "select uuid from public.metadata where id=:id";
                    $flux["uuid"] = $CATALOGUE->fetchColumn($query, array("id"=>$metadata_id));
                } else {
                    throw new \Exception("Impossible de dupliquer la métadonnée {$uuid}");
                }
            }catch(\Exception $exception){
                if ( isset($errormsg) ) 
                    throw new \Exception($errormsg, $exception); 
                else throw $exception;
            }
        }
    }
    /**
     * Exécution de l'import dans le cas d'un import de métadonnées moissonnées
     * @param array $flux
     * @throws \Exception
     */
    protected function doSynchronisation(array &$flux)
    {
        $PRODIGE = $this->getProdigeConnection(self::PRODIGE_SCHEMA_PUBLIC);
        $CATALOGUE = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE);
        
        $fmeta_id = $flux["fmeta_id"];
        $uuid = $flux["uuid"];
        
        !$CATALOGUE->isTransactionActive() && $CATALOGUE->beginTransaction();
        
        try {
	        if ( $flux["import_data_type"]=="WFS" ){
	        	$resultat = $this->importWFS($flux);
	        } 
	        else {
	        	$resultat = $this->importATOM($flux);
	        }
	        if ( isset($resultat) ){
	        	$this->addSuccess($flux, $resultat);
	        }
        } catch(\Exception $exception){
	        $PRODIGE->isTransactionActive()   && $PRODIGE->rollBack();
	        $CATALOGUE->isTransactionActive() && $CATALOGUE->rollBack();
	        $resultat = ($exception instanceof ApiException ? $exception->getData() : array("result"=>array("success"=>"FAILURE", "import_status"=>$exception->getMessage())));
	        $this->addFailure($flux, $resultat);
        }
    }
    /**
     * Post-traitements de l'exécution dans le cas d'un import de métadonnées moissonnées
     * @param array $flux
     * @throws \Exception
     */
    protected function postSynchronisation(array $flux)
    {
        $PRODIGE = $this->getProdigeConnection(self::PRODIGE_SCHEMA_PUBLIC);
        $CATALOGUE = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE);
        
        $fmeta_id = $flux["fmeta_id"];
        $uuid = $flux["uuid"];
        
        !$CATALOGUE->isTransactionActive() && $CATALOGUE->beginTransaction();
        
        if ( $flux["import_frequency"] ){
        	$pk_tache = $flux["pk_tache_import_donnees"];
        	$extra_params = $flux["import_extra_params"];
        	foreach ($this->resultats as $resultats){
        		if ( !isset($resultats["success"]) ) continue;
        		if ( !isset($resultats["success"][$pk_tache]) ) continue;
        		if ( !isset($resultats["success"][$pk_tache]["imports"]) ) continue;
        		$imports = $resultats["success"][$pk_tache]["imports"];
        		foreach($imports as $import_table=>$import){
        			$extra_params[str_replace($flux["import_data_source"]." :: ", "", $import["file_source"])] = array(
        				"pk_couche_donnees" => $import["pk_couche_donnees"],
        				"couchd_emplacement_stockage" => $import_table
        			);
        		}
        	}
        	$CATALOGUE->executeQuery("update tache_import_donnees set import_date_execution=now(), import_extra_params=:import_extra_params where pk_tache_import_donnees=:pk_tache_import_donnees"
        	, array(
        		"pk_tache_import_donnees" => $pk_tache,
        		"import_extra_params" => json_encode($extra_params),
        	));
        }
        
	        $PRODIGE->isTransactionActive()   && $PRODIGE->commit();
	        $CATALOGUE->isTransactionActive() && $CATALOGUE->commit();
    }
    
}
