<?php

namespace Carmen\ImportDataBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use JMS\SecurityExtraBundle\Annotation\Secure;
use JMS\Serializer\SerializationContext;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;
use Prodige\ProdigeBundle\Controller\BaseController as ProdigeBaseController;
use Carmen\ApiBundle\Controller\BaseController as CarmenBaseController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Carmen\ApiBundle\Services\Helpers;

use Carmen\ImportDataBundle\Services\ImportInPostgis;
use Carmen\ImportDataBundle\Services\ImportRaster;
use Prodige\ProdigeBundle\Services\ConfigReader;
use Carmen\ImportDataBundle\Services\LayerChangeName;
use Doctrine\DBAL\Connection;
use Symfony\Component\HttpFoundation\ParameterBag;
use Carmen\ApiBundle\Exception\ApiException;
use Prodige\ProdigeBundle\Services\GeonetworkInterface;
use Prodige\ProdigeBundle\DAOProxy\DAO;
use Prodige\ProdigeBundle\Controller\FeatureCatalogueController;
use Prodige\ProdigeBundle\Controller\UpdateDomSdomController;
use Prodige\ProdigeBundle\Controller\UpdateArboController;

/**
 * @author alkante
 */
abstract class AbstractImportDataController extends CarmenBaseController
{
    const DEFAULT_TYPE_STOCKAGE = "1";//vector
    const PRODIGE_SCHEMA_PUBLIC      = "public"; 
    const PRODIGE_SCHEMA_LOCALDATA   = "local_data"; 
    const CATALOGUE_SCHEMA_CATALOGUE = "catalogue"; 
    const CATALOGUE_SCHEMA_PUBLIC    = "public"; 
    
    protected static $DATATYPE_TO_STOCKAGE = array(
        "0" => "raster",
        "1" => "vector",
        "01" => "mnt",
        "-1" => "multi",
        "-2" => "majic",
        "-3" => "tabulaire",
        "-4" => "join",
    );
    protected static $ADDITIONAL_RASTER_EXT = array("tfw", "tab");
    protected static $DATATYPE_EXTENSION = array(
		  "VECTOR" => array(
		  	'shp' => array('prj', 'dbf', 'shx', 'qpj'), 
		  	'mif' => array('mid'), 
		  	'tab' => array('id', 'map', 'dat', 'ind')
		  ),
		  "TABULAIRE" => array('csv','ods','xls','xlsx',),
		  "MNT" => array('asc','xyz',),
		  "SPREADSHEET" => array('ods','xls','xlsx',),
		  "RASTER" => array(
		  		'RASTER' => array('gtiff','tiff','tif','ecw'),
		  		'TILEINDEX' => array(
		  			'shp' => array('prj', 'dbf', 'shx', 'qpj'), 
		  			'tab' => array('id', 'map', 'dat', 'ind'), 
		  		)
		  )
    );
    protected static $REQUIRED_EXTENSION = array(
		  "VECTOR" => array(
		  	'shp' => array('prj', 'dbf', 'shx'), 
		  	'mif' => array('mid'), 
		  	'tab' => array('id', 'map', 'dat')
		  ),
		  "TABULAIRE" => array('csv','ods','xls','xlsx',),
		  "MNT" => array('asc','xyz',),
		  "SPREADSHEET" => array('ods','xls','xlsx',),
		  "RASTER" => array(
		  		'RASTER' => array('gtiff','tiff','tif','ecw'),
		  		'TILEINDEX' => array(
		  			'shp' => array('prj', 'dbf', 'shx'), 
		  			'tab' => array('id', 'map', 'dat'), 
		  		)
		  )
    );
    
    protected static $TABLE_STOCKAGES = array("1" => "public", "-3" => "public");

    protected function formatJsonSuccessCallback($callback){
        $args = array_slice(func_get_args(), 1);
        $response = call_user_func_array(array($this, "formatJsonSuccess"), $args);
        if ( $response instanceof Response ){
            if ( $callback ) return new Response($callback.'('.$response->getContent().')');
            return $response;
        }
        return $response;
    }
    protected function formatJsonErrorCallback($callback){
        $args = array_slice(func_get_args(), 1);
        $response = call_user_func_array(array($this, "formatJsonError"), $args);
        if ( $response instanceof Response ){
            if ( $callback ) return new Response($callback.'('.$response->getContent().')');
            return $response;
        }
        return $response;
    }
    
    protected function formatJsonError($status = 500, $error = "Internal Server Error", $description = "", array $extra = array())
    {
        $response = parent::formatJsonError($status, $error, $description, $extra);
        $content = $response->getContent();
        $PRODIGE = $this->getProdigeConnection(null);
        $CATALOGUE = $this->getCatalogueConnection(null);
        $content = str_replace($PRODIGE->getPassword(), "******", $content);
        $content = str_replace($CATALOGUE->getPassword(), "******", $content);
        $content = str_replace($PRODIGE->getUsername(), "******", $content);
        $content = str_replace($CATALOGUE->getUsername(), "******", $content);
        $response->setContent($content);
        return $response;
    }
    
    protected function formatJsonSuccess(array $extra = array(), $status = 200)
    {
        $response = parent::formatJsonSuccess($extra, $status);
        $content = $response->getContent();
        $PRODIGE = $this->getProdigeConnection(null);
        $CATALOGUE = $this->getCatalogueConnection(null);
        $content = str_replace($PRODIGE->getPassword(), "******", $content);
        $content = str_replace($CATALOGUE->getPassword(), "******", $content);
        $content = str_replace($PRODIGE->getUsername(), "******", $content);
        $content = str_replace($CATALOGUE->getUsername(), "******", $content);
        $response->setContent($content);
        return $response;
    }
    /**
     * Change search_path on  Doctrine connection
     * @param string $connection_name
     * @param string $schema
     * @return \Doctrine\DBAL\Connection
     */
    protected function setSearchPath(\Doctrine\DBAL\Connection $conn, $schema="public") {
        $conn->exec('set search_path to '.$schema);
        return $conn;
    }
    /**
     * Get Doctrine connection
     * @param string $connection_name
     * @param string $schema
     * @return \Doctrine\DBAL\Connection
     */
    protected function getConnection($connection_name, $schema="public") {
        $conn = $this->getDoctrine()->getConnection($connection_name);
        $schema && $conn->exec('set search_path to '.$schema);
        
        return $conn;
    }
    
    /**
     * @param string $schema
     * @return \Doctrine\DBAL\Connection
     */
    public function getProdigeConnection($schema="public") {
        return $this->getConnection(ProdigeBaseController::CONNECTION_PRODIGE, $schema);
    }
    
    /**
     * @param string $schema
     * @return \Doctrine\DBAL\Connection
     */
    public function getCatalogueConnection($schema="public") {
        return $this->getConnection(ProdigeBaseController::CONNECTION_CATALOGUE, $schema);
    }

    /**
     * @param \Doctrine\DBAL\Connection $db_connection
     * @param string|array $query The SQL query with or without comma (simple or multiple queries)
     * @param array  $params
     * @return \Doctrine\DBAL\Driver\Statement
     * @throws \Doctrine\DBAL\DBALException
     */
    public function curlToGeosource($route, array $parameters = array(), $method="GET")
    {
    	
    		$geonetwork = new GeonetworkInterface(PRO_GEONETWORK_URLBASE, 'srv/fre/');
    		
        $url_parameters = ($method=="GET" ? empty($parameters) ? "" : "?".http_build_query($parameters) : "");
        $post_parameters = ($method=="GET" ? array() : $parameters);
        
        $parameters = (empty($parameters) ? "" : "?".http_build_query($parameters));
        $url = $route.$url_parameters;
        
        if ( $method=="GET" ){
        	return $geonetwork->get($url);
        } else {
        	return $geonetwork->post($url, $post_parameters);
        }
    }

    /**
     * @param \Doctrine\DBAL\Connection $db_connection
     * @param string|array $query The SQL query with or without comma (simple or multiple queries)
     * @param array  $params
     * @return \Doctrine\DBAL\Driver\Statement
     * @throws \Doctrine\DBAL\DBALException
     */
    public function curlToCatalogue($route, array $parameters = array(), $method="GET")
    {
        $url_parameters = ($method=="GET" ? empty($parameters) ? "" : "?".http_build_query($parameters) : "");
        $post_parameters = ($method=="GET" ? array() : $parameters);
        $url = rtrim($this->getParameter("PRODIGE_URL_CATALOGUE"), '/').$route.$url_parameters;
        return $this->curl($url, $method, array(),  $post_parameters);
    }

    /**
     * Update Geosource Metadata information and create the attribute catalogue
     * @param integer $metadata_id
     * @param array   $metadata_fields
     * @param boolean $bUpdateArbo
     */
    public function duplicateGeosourceMetadata($uuid)
    {
        $CATALOGUE = $this->getCatalogueConnection("catalogue"); 
        $sql = " select m.id from ".self::CATALOGUE_SCHEMA_PUBLIC.".metadata m where m.uuid= :uuid";
        $fmeta_id = $CATALOGUE->fetchColumn($sql, array("uuid"=>$uuid));
        $geonetwork = $this->getGeonetworkInterface();
        $group_id = $geonetwork->getMetadataGroupOwner($CATALOGUE, $fmeta_id);
            
        $relatedid = null;
        $relatedUuid = null; 
        /*
        * Validation de la transaction puis redirection vers la duplication de la fiche geonetwork (qui génÃ¨rera la fiche_metadonnees)
        */
        try{
            $errormsg = "Echec de la duplication / Geosource indisponible";
            $json = $geonetwork->createMetadata($fmeta_id, $group_id);
        }catch(\Exception $exception){throw new \Exception($errormsg, $exception);}
        
        $json = (is_string($json) ? json_decode($json) : $json);
        if( $json &&  $json->id  ){
            $relatedid = $json->id;
            $query = "select uuid from public.metadata where id=:id";
            $relatedUuid = $CATALOGUE->fetchColumn($query, array("id"=>$relatedid));
        } else {
            throw new \Exception("Impossible de dupliquer la métadonnée {$uuid}");
        }
        try{
            $errormsg = "Echec de la duplication / Erreur d'affectation des domaines/sous-domaines";
            $query = 'INSERT INTO ssdom_dispose_metadata (uuid, ssdcouch_fk_sous_domaine)'.
                     ' SELECT :relatedUuid, ssdcouch_fk_sous_domaine FROM ssdom_dispose_metadata WHERE uuid = :uuid';
            $CATALOGUE->executeQuery($query, compact("relatedUuid", "uuid"));
        }catch(\Exception $exception){throw new \Exception($errormsg, $exception);}
        $bVerify = 0;
        
        $pk_couche_donnees = null;
        $query = 'SELECT pk_couche_donnees, couchd_emplacement_stockage,couchd_type_stockage,accs_adresse_admin '.
                 ' from couche_donnees couche '.
                 ' inner join fiche_metadonnees fmeta on (fmeta.fmeta_fk_couche_donnees=couche.pk_couche_donnees) '.
                 ' left join acces_serveur on (couchd_fk_acces_server=pk_acces_serveur)'.
                 ' where fmeta.fmeta_id = :fmeta_id';
        $couches = $CATALOGUE->executeQuery($query, compact("fmeta_id"));
        foreach($couches as $couche){
            $couchd_emplacement_stockage = $couche["couchd_emplacement_stockage"];
            $couchd_type_stockage = $couche["couchd_type_stockage"];
            $pk_couche_donnees = $couche["pk_couche_donnees"];
                        
            if ($couchd_type_stockage == 0){
                $getCopyName = function($file)use($CATALOGUE){
                    $namefile=basename($file);
                    list($tmp_namefile,$extension)=explode(".",$namefile);
                    $files = glob(dirname($file)."/".$tmp_namefile."_copy*.".$extension);
                    if ( empty($files) ){
                        $num = 1;
                    } else {
                        sort($files);
                        $last = array_pop($files);
                        list($origname,$suite)=explode("_copy",$last);
                        $num = intval($suite) ?: 0;
                        $num++;
                        if(dirname($file)!='.')
                            $newname=dirname($file)."/".$origname."_copy".$num.".".$extension;
                        else
                            $newname=$origname."_copy".$num.".".$extension;
                    }
                    return $newname;
                }; 
            }
            else{
                $getCopyName = function($table)use($CATALOGUE){
                    list($origname,$suite)=explode("_copy",$table);
                    $last = $CATALOGUE->fetchColumn("select max(couchd_emplacement_stockage) from couche_donnees where  WHERE couchd_emplacement_stockage like :table||'_copy%'", compact("table"));
                    if ( !$last ){
                        $num = 1;
                    } else {
                        list($origname,$suite)=explode("_copy",$last);
                        $num = intval($suite) ?: 0;
                        $num++;
                    }
                    $newname=$origname."_copy".$num;
                    return $newname;
                }; 
            }
            $copy_couchd_emplacement_stockage = $getCopyName($couchd_emplacement_stockage);
                        
            /*if ($bVerify == 0) {
                $accs_adresse = $couche["accs_adresse_admin"];
                
                $accs_adresse = (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"]=="on" ? "https" : "http").'://' . $accs_adresse . '/PRRA/Administration/Administration/Actions/ActionsDuplication.php';
                                
                if ($couchd_type_stockage == 0)
                    header('location:' .$accs_adresse . '?ACTION=couchefichierExist&SRCFILE=' . $couchd_emplacement_stockage . '&DESFILE=' . $copy_couchd_emplacement_stockage . '&id=' . $metadataId . '&group=2'./*'&domaine=' . $domaine . '&sousdomaine=' . $sousdomaine . '&coucheId=' . $coucheId . '&NOM=' . $_GET["NOM"]);
                else
                    header('Location:' .$accs_adresse . '?ACTION=coucheExist&SRCTABLE=' . $couchd_emplacement_stockage . '&DESTTABLE=' . $copy_couchd_emplacement_stockage . '&id=' . $metadataId . '&group=2'./*'&domaine=' . $domaine . '&sousdomaine=' . $sousdomaine . '&coucheId=' . $coucheId . '&NOM=' . $_GET["NOM"]);
                exit ();
            }*/
            
            $next_couche_donnees = $CATALOGUE->fetchColumn("SELECT NEXTVAL('SEQ_COUCHE_DONNEES')");
            /*
            * Duplication de la couche de donnees
            */
            try{
                $errormsg = "Echec de la duplication / Erreur de la duplication de la donnée";
                $paramsbind = array(
                    "pk_couche_donnees" => $next_couche_donnees,
                    "couchd_id" => $next_couche_donnees,
                    "couchd_emplacement_stockage" => $copy_couchd_emplacement_stockage,
                );
                $fields = array(
                    "couchd_nom",
                    "couchd_description",
                    "couchd_type_stockage",
                    "couchd_fk_acces_server"
                );
                $query = 'INSERT INTO couche_donnees (pk_couche_donnees, couchd_id, couchd_emplacement_stockage, '.implode(", ", $fields).') '.
                         ' SELECT :pk_couche_donnees, :couchd_id, :couchd_emplacement_stockage, '.implode(", ", $fields).
                         ' FROM couche_donnees '.
                         ' WHERE pk_couche_donnees = :from_pk_couche_donnees';
                $CATALOGUE->executeQuery($query, array_merge($paramsbind, array("from_pk_couche_donnees"=>$pk_couche_donnees)));
            }catch(\Exception $exception){throw new \Exception($errormsg, $exception);}
                        
            /*
            * Ajout des domaines sous-domaines
            */
            try{
                $errormsg = "Echec de la duplication / Erreur d'affectation des domaines/sous-domaines";
                $query = 'INSERT INTO ssdom_dispose_couche (ssdcouch_fk_couche_donnees, ssdcouch_fk_sous_domaine)'.
                         ' SELECT :to_couche_donnees, ssdcouch_fk_sous_domaine FROM ssdom_dispose_couche WHERE ssdcouch_fk_couche_donnees = :from_couche_donnees';
                $CATALOGUE->executeQuery($query, array("to_couche_donnees"=>$next_couche_donnees, "from_pk_couche_donnees"=>$pk_couche_donnees));
            }catch(\Exception $exception){throw new \Exception($errormsg, $exception);}
                        
            /*
            * Ajout de la relation donnnée/métadonnée
            */
            try{
                $errormsg = "Echec de la duplication / Erreur de création de la relation donnnée/métadonnée";
                $query = 'INSERT INTO FICHE_METADONNEES (FMETA_ID, FMETA_FK_COUCHE_DONNEES, STATUT) VALUES (:relatedId, :next_couche_donnees, 1)';
                $CATALOGUE->executeQuery($query, compact("next_couche_donnees", "relatedId"));
            }catch(\Exception $exception){throw new \Exception($errormsg, $exception);}
        }
    }

    /**
     * Update Geosource Metadata information and create the attribute catalogue
     * @param integer $metadata_id
     * @param array   $metadata_fields
     * @param boolean $bUpdateArbo
     */
    public function updateAtomArboMetadata($metadata_id, $metadata_fields=array(), $bUpdateArbo=false)
    {
        $PRODIGE = $this->getProdigeConnection(self::PRODIGE_SCHEMA_PUBLIC);
        ($bOpenProdige = $PRODIGE->isTransactionActive()) && $PRODIGE->commit();
        
        $CATALOGUE = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE);
        ($bOpenCatalogue = $CATALOGUE->isTransactionActive()) && $CATALOGUE->commit();
        
        
        $controller = new UpdateArboController();
        $controller->setContainer($this->container);
        $request = new Request();
        $request->query->replace(array(
            "metadata_id"=>$metadata_id,
        ));
        $response = $controller->updateArboAction($request);
        
        
        $bOpenProdige   && !$PRODIGE->isTransactionActive()   && $PRODIGE->beginTransaction();
        $bOpenCatalogue && !$CATALOGUE->isTransactionActive() && $CATALOGUE->beginTransaction();
        $PRODIGE = $this->getProdigeConnection(self::PRODIGE_SCHEMA_PUBLIC);
        $CATALOGUE = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE); 
    
    }
    
    public function deleteGeosourceFeatureCatalogue($metadata_id, $couchd_emplacement_stockage)
    {
        $PRODIGE = $this->getProdigeConnection(self::PRODIGE_SCHEMA_PUBLIC);
        ($bOpenProdige = $PRODIGE->isTransactionActive()) && $PRODIGE->commit();
        
        $CATALOGUE = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE);
        ($bOpenCatalogue = $CATALOGUE->isTransactionActive()) && $CATALOGUE->commit();
        
        $featureCatalogue = new FeatureCatalogueController();
        $featureCatalogue->setContainer($this->container);
        $request = new Request();
        $request->query->replace(array(
            "category" => "fcat",
            "metadata_id"=>$metadata_id,
            "fcat_fieldOfApplication" => $couchd_emplacement_stockage,
            "mode" => "delete",
        ));
        $response = $featureCatalogue->featureCatalogueAction($request);
                    
        $bOpenProdige   && !$PRODIGE->isTransactionActive()   && $PRODIGE->beginTransaction();
        $bOpenCatalogue && !$CATALOGUE->isTransactionActive() && $CATALOGUE->beginTransaction();
        $PRODIGE = $this->getProdigeConnection(self::PRODIGE_SCHEMA_PUBLIC);
        $CATALOGUE = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE); 
    }
    
    public function generateGeosourceFeatureCatalogue($metadata_id, $couchd_nom, $couchd_emplacement_stockage, array $couche_fields=array())
    {
        if ( empty($couche_fields) ) return;
        $PRODIGE = $this->getProdigeConnection(self::PRODIGE_SCHEMA_PUBLIC);
        ($bOpenProdige = $PRODIGE->isTransactionActive()) && $PRODIGE->commit();
        
        $CATALOGUE = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE); 
        ($bOpenCatalogue = $CATALOGUE->isTransactionActive()) && $CATALOGUE->commit();
    
        $namespaces = "ARRAY[ARRAY['gmd', 'http://www.isotc211.org/2005/gmd'], ARRAY['gco','http://www.isotc211.org/2005/gco']]"; 
        $sql_metadata_title = " array_to_string(xpath('/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:citation/gmd:CI_Citation/gmd:title/gco:CharacterString/text()'::text, data::xml, {$namespaces}), ' ') as metadata_title";
        if ( !$CATALOGUE->fetchColumn("select xml_is_well_formed(data::text) from ".self::CATALOGUE_SCHEMA_PUBLIC.".metadata where id=:metadata_id", compact("metadata_id")) ){
        	$sql_metadata_title = "null as metadata_title";
        }
        $metadata_title = $CATALOGUE->fetchColumn("select ".$sql_metadata_title." from ".self::CATALOGUE_SCHEMA_PUBLIC.".metadata where id=:metadata_id", compact("metadata_id"));
        
        
        $featureCatalogue = new FeatureCatalogueController();
        $featureCatalogue->setContainer($this->container);
        $request = new Request();
        $request->query->replace(array(
            "category" => "fcat",
            "metadata_id"=>$metadata_id,
            "fcat_MD_DataIdentification" => ($metadata_title ? $metadata_title." - " : "").$couchd_nom.' ['.$couchd_emplacement_stockage.']',
            "fcat_fieldOfApplication" => $couchd_emplacement_stockage,
            "mode" => "replace",
            "champs" => $couche_fields
        ));
        $response = $featureCatalogue->featureCatalogueAction($request);
        
        $bOpenProdige   && !$PRODIGE->isTransactionActive()   && $PRODIGE->beginTransaction();
        $bOpenCatalogue && !$CATALOGUE->isTransactionActive() && $CATALOGUE->beginTransaction();
        $PRODIGE = $this->getProdigeConnection(self::PRODIGE_SCHEMA_PUBLIC);
        $CATALOGUE = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE); 
    }

    /**
     * Update Geosource Metadata information and create the attribute catalogue
     * @param integer $metadata_id
     * @param array   $metadata_fields
     * @param boolean $bUpdateArbo
     */
    public function updateDomSDomMetadata($metadata_id, $sdom=array())
    {
        $PRODIGE = $this->getProdigeConnection(self::PRODIGE_SCHEMA_PUBLIC);
        ($bOpenProdige = $PRODIGE->isTransactionActive()) && $PRODIGE->commit();
        
        $CATALOGUE = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE);
        ($bOpenCatalogue = $CATALOGUE->isTransactionActive()) && $CATALOGUE->commit();
        
        //update representationType in metadata
        
        $updateDomSdom = new UpdateDomSdomController();
        $updateDomSdom->setContainer($this->container);
        $request = new Request();
        $request->query->replace(array(
            "id" => $metadata_id,
            "mode" => "couche",
            'sdom' => $sdom
        ));
        $response = $updateDomSdom->updateDomSdomAction($request);
        
        $bOpenProdige   && !$PRODIGE->isTransactionActive()   && $PRODIGE->beginTransaction();
        $bOpenCatalogue && !$CATALOGUE->isTransactionActive() && $CATALOGUE->beginTransaction();
        
    }

    /**
     * @param \Doctrine\DBAL\Connection $db_connection
     * @param string|array $query The SQL query with or without comma (simple or multiple queries)
     * @param array  $params
     * @return \Doctrine\DBAL\Driver\Statement
     * @throws \Doctrine\DBAL\DBALException
     */
    public function executeQuery(\Doctrine\DBAL\Connection $db_connection, $query, $params = array())
    {
        $stmt = null;
        $queries = (is_array($query) ? $query : explode(";", $query));
        foreach($queries as $query){
            $query = trim($query);
            if ( empty($query) ) continue;
            $query = preg_replace("!;$!", "", $query);
            $stmt= $db_connection->executeQuery($query, $params);
        }
        return $stmt;
    }
    
    protected function analyseRequest(ImportInPostgis $importeur)
    {
    	$file_sheet = $importeur->GetImportFeuille();
    	
			$bIsOk = true;
			
// 			// transform XLS to CSV file to use as input for ogr
// 			if ( strtolower($importeur->GetTypeFichier())==strtolower(ImportInPostgis::$EXT_XLS) ) {
// 				$bIsOk = $importeur->Xls2csv($file_sheet);
// 				$importeur->setm_fichier($importeur->getm_fichierShape());
// 			}
// 			// transform ODS to CSV file to use as input for ogr
// 			if ( strtolower($importeur->GetTypeFichier())==strtolower(ImportInPostgis::$EXT_ODS) ) {
// 				$bIsOk = $importeur->Ods2csv($file_sheet);
// 				$importeur->setm_fichier($importeur->getm_fichierShape());
// 			}
			$bCSVToDelete = 0;
			$couchd_visualisable = 1;
			$old_extension = "";
			if ( strtolower($importeur->GetTypeFichier())==strtolower(ImportInPostgis::$EXT_XYZ) ) {
				$importeur->setm_fichier(str_ireplace(".XYZ", ".csv", $importeur->GetImportFichier()));
				$bCSVToDelete = 1;
				$couchd_visualisable = 0;
				$old_extension = ".XYZ";
			}
			if ( strtolower($importeur->GetTypeFichier())==strtolower(ImportInPostgis::$EXT_ASC) ) {
				$importeur->setm_fichier(str_ireplace(".ASC", ".csv", $importeur->GetImportFichier()));
				$bCSVToDelete = 1;
				$couchd_visualisable = 0;
				$old_extension = ".ASC";
			}
			
			return array_values(compact('bIsOk', "couchd_visualisable", "bCSVToDelete", "old_extension"));
    }
    
    /**
     * Analyze the layer file to find the layerfields, the layer encoding, the Geometry, the file sheets 
     * @param integer $pk_couche_donnees            cf Table couche_donnees
     * @param string  $layerfile                    Relative path to the layer file
     * @param integer $couchd_type_stockage         cf Table couche_donnees
     * @param string  $layersheet                   Name of sheet to read or null if it is not found or available
     * @param string  $layertype                    The layer type (VECTOR|TABULAIRE|RASTER)
     * @param string  $couchd_emplacement_stockage  If necessary, specify the couchd_emplacement_stockage= the db table to create
     * @param string  $directory                    If necessary, specify the directory of the layer file
     * @throws Exception
     * @return array
     */
    protected function convertMNT($directory, $layerfile)
    {
			$xyz = array(); $asc = array();
			$layerfile = explode('|', $layerfile);
			
			if ( empty($layerfile) ) return $layerfile;
			
			if ( is_dir($directory.$layerfile[0]) ){
				$dir = $directory.$layerfile[0];
        $cmd = "find ".$dir." -type f -maxdepth 1 -iname '*.xyz' ";
        $xyz = array(); exec($cmd, $xyz);
        $cmd = "find ".$dir." -type f -maxdepth 1 -iname '*.asc' ";
        $asc = array(); exec($cmd, $asc);
			} else {
				foreach($layerfile as $index=>$file){
				  $layerfile[$index] = $file = str_replace("Publication/", "", $file);
					$extension = explode(".", $file);
					$extension = strtolower(end($extension));
					if ( $extension=="asc" ){
						$asc[] = $directory.$file;
					}
					if ( $extension=="xyz" ){
						$xyz[] = $directory.$file;
					}
				}
			}
			if ( empty($xyz) && empty($asc) ) return implode('|', $layerfile);
			
			$deletables = array(); 
			$files = array();
			if ( !empty($xyz) ){
				$files = $xyz;
			} 
			else if ( !empty($asc) ){
				//Cas de fichiers .asc, on fait la conversion en fichier xyz
				foreach ($asc as $file){
					$extension = explode(".", $file);
					$extension = strtolower(end($extension));
					$fileConvertedXYZ = str_ireplace(".".$extension, ".XYZ", $file);
					$res = array(); $return = "";
					$cmd = "gdal_translate -of XYZ '".$file."' '".$fileConvertedXYZ."'";
					exec($cmd." 2>&1", $res, $return);
					if($return){
						throw new \Exception("Problème lors de la conversion du fichier ".$file." à l'aide de gdal_translate.".implode("<br>", $res));
					}
					$files[] = $fileConvertedXYZ;
					$deletables[] = $fileConvertedXYZ;
				}
			}
			
			$tempFile = tempnam($directory."/temp", "mnt_concat_");
			@unlink($tempFile);
			$csvFile = $tempFile.".CSV";
			@unlink($csvFile);
			$HEADERS = array("separator"=>"\t", "columns"=>array("X", "Y", "Z"));
			$separators = array("\t", " ", ",");
			$cmds = array();
			foreach ($files as $file){
				$res = array();$return = "";
				foreach ($separators as $separator){
				  $fopen = fopen($file, "r");
					$headers = fgetcsv($fopen, null, $separator);
					$sheader = implode($separator, $headers);
				  fclose($fopen);
					if (is_array($headers) && count($headers)>1) {
						$HEADERS["separator"] = $separator;
						break;
					}
				}
				$hasHeader = array_reduce($headers, function($previous, $item){return $previous && !is_numeric($item);}, true);
				
				if ( $hasHeader ){
					$HEADERS["columns"] = array_merge($HEADERS["columns"], $headers);
				  $cmds[] = "egrep -v '".$sheader."' '".$file."' >> '".$csvFile."'";
				} else {
				  $cmds[] = "cat '".$file."' >> '".$csvFile."'";
				}
			}
			$HEADERS["columns"] = array_unique($HEADERS["columns"]);
			
			file_put_contents($csvFile, implode($HEADERS["separator"], $HEADERS["columns"])."\n");
			foreach ($cmds as $cmd){
				$res = array();$return = "";
				exec($cmd." 2>&1", $res, $return);
				if($return){
					throw new \Exception("Problème lors de la mise en relation des fichiers sélectionnés.");
				}
			}
			@array_map("unlink", $deletables);
			file_put_contents($tempFile.".filelist", $directory."/".implode("\n".$directory."/", $layerfile)); 
			return $csvFile;
    }
    
    /**
     * Analyze the layer file to find the layerfields, the layer encoding, the Geometry, the file sheets 
     * @param integer $pk_couche_donnees            cf Table couche_donnees
     * @param string  $layerfile                    Relative path to the layer file
     * @param integer $couchd_type_stockage         cf Table couche_donnees
     * @param string  $layersheet                   Name of sheet to read or null if it is not found or available
     * @param string  $layertype                    The layer type (VECTOR|TABULAIRE|RASTER)
     * @param string  $couchd_emplacement_stockage  If necessary, specify the couchd_emplacement_stockage= the db table to create
     * @param string  $directory                    If necessary, specify the directory of the layer file
     * @throws Exception
     * @return array
     */
    protected function getGISFileProperties($pk_couche_donnees, $layerfile, $couchd_type_stockage, $layersheet=null, $layertype="VECTOR", $couchd_emplacement_stockage=null, $directory=null)
    {
    		$tabDatatype = array(
	    		"int" => "int4",
	    		"int4" => "int4",
	    		"integer" => "int4",
	    		"numeric0" => "int4",
	    		"integer64" => "int4",
	    		"bigint" => "int4",
	    		"float" => "float",
	    		"real" => "float",
	    		"numeric" => "float",
	    		"double" => "float",
	    		"double precision" => "float",
	    		"string" => "text",
	    		"varchar" => "text",
	    		"character varying" => "text",
	    		"text" => "text",
	    		"date" => "date",
	    		"timestamp" => "date",
    		);
    	
    	
    		$CATALOGUE = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE);
    		$PRODIGE = $this->getProdigeConnection(self::PRODIGE_SCHEMA_PUBLIC);
    		
    		$extension = strtolower(pathinfo($layerfile, PATHINFO_EXTENSION));
    		$isSpreadSheet = in_array($extension, self::$DATATYPE_EXTENSION["SPREADSHEET"]);
    		
    	  $columns = array();
    	  try {
    	  	if ( !$couchd_emplacement_stockage && $pk_couche_donnees ){
	    	  	$couchd_emplacement_stockage = $CATALOGUE->fetchColumn(
	    	  			"select couchd_emplacement_stockage from couche_donnees where pk_couche_donnees=:pk_couche_donnees"
	    	  			, compact("pk_couche_donnees")
	    	  			, 0);
    	  	}
    	  
	        if ( $couchd_emplacement_stockage && array_key_exists($couchd_type_stockage, self::$TABLE_STOCKAGES) ){
	        	
	        	$querycolumns = $PRODIGE->executeQuery(
	        	           		"select column_name, data_type, numeric_scale ".
	        	           		" from information_schema.columns  ".
	        	           		" where table_schema=:table_schema and table_name=:table_name",
	        			       array("table_schema"=>self::$TABLE_STOCKAGES[$couchd_type_stockage], "table_name"=>$couchd_emplacement_stockage));
	        	foreach($querycolumns as $row){
	        		$data_type = strtolower($row["data_type"]);
	        		if ( $data_type=="numeric" && $row["numeric_scale"]==0 ){
	        			$data_type = "numeric0";
	        		}
	        		$columns[$row["column_name"]] = $tabDatatype[$data_type];
	        	}
	        } 
    	  } catch(\Exception $exception){}
    	  
    	  $get_encoding = "data";
    	  $original_layertype = $layertype;
    	  $_layertype = $layertype;
    	  if ( $_layertype =="TABULAIRE" ) $_layertype = "VECTOR";
    	  if ( $_layertype =="MULTI" ) $_layertype = "VECTOR";
    	  $directory = rtrim(($directory ?: $this->getMapfileDirectory()), '/').'/';
    	  $layerfile = urldecode($layerfile);
    	  $layerfile = preg_replace("!^Publication/!", "", $layerfile);
    	  
    	  if ( $layertype=="MNT" ){
    	  	$get_encoding = "headers";
    	  	$layerfile = $this->convertMNT($directory, $layerfile);
    	  	$_layertype = "VECTOR";
    	  	$layertype = "TABULAIRE";
    	  }
    	  
    	  
    	  if ( file_exists($layerfile) )
    	    $layerfile = str_replace(rtrim(realpath($directory), "/")."/", "", realpath($layerfile));
    	  else if (file_exists($directory.$layerfile))
    	    $layerfile = str_replace(rtrim(realpath($directory), "/")."/", "", realpath($directory.$layerfile));
    	  $layerfile = urlencode($layerfile);
    	  
    	  
        $parameters = array("layertype"=>$_layertype, "layerfile"=>$layerfile, "layersheet"=>$layersheet);
        $parameters = array_diff($parameters, array(null));
        $params = array_merge(array("directory"=>$directory), /*$request->query->all(), $request->request->all()*/ array());
        
        $carmen_ws_helpers_geometrytype = 
            str_replace("app_dev.php/", "", str_replace("/api/mapserver", "", 
                $this->generateUrl("carmen_ws_helpers_geometrytype", array_merge($parameters, array("encoding"=>"headers")), false)));
        
        $carmen_ws_helpers_getsheets = 
            str_replace("app_dev.php/", "", str_replace("/api/mapserver", "", 
                $this->generateUrl("carmen_ws_helpers_getsheets", $parameters, false)));
        
        
        $carmen_ws_helpers_layerfields = 
            str_replace("app_dev.php/", "", str_replace("/api/mapserver", "", 
                $this->generateUrl("carmen_ws_helpers_layerfields", array_merge($parameters, array("encoding"=>$get_encoding)), false)));
        
        $geometrytype = $this->callMapserverApi($carmen_ws_helpers_geometrytype, "GET", $params, array());
        $bGetFields = ( $isSpreadSheet ? !is_null($layersheet) : true );
        $properties = array("layerfields"=>array(), "nbFieldsShared"=>0, "columns"=>$columns, "sheets"=>array(), "encoding"=>"UTF-8");
            
        if ( $bGetFields ){
            try {
	        $fields       = $this->callMapserverApi($carmen_ws_helpers_layerfields, "GET", $params, array());
            }catch(ApiException $exception){
                $data = $exception->getData();
                if ( isset($data) && isset($data["results"]) ){
                    return $data["results"];
                }
            }
	        
	        if ( !$fields ) $fields = array();
	        if ( !isset($fields["fields_encoded"]) ) $fields["fields_encoded"] = array();
          if ( isset($fields["encoding"]) ) 
              	$properties["encoding"] = $fields["encoding"];
              
	        $properties["layerfields"] = $fields["fields_encoded"];
	        $nbFieldsShared = 0; 
	        
	        foreach ($properties["layerfields"] as $field_index=>$field){
	            $field["field_datatype"] = $tabDatatype[strtolower($field["field_datatype"])];
	            if ( $original_layertype=="MNT" ){
	            	$field["field_datatype"] = "float";
	            }
	            $field["field_index"] = $field_index;
	            $field["field_visible"] = (empty($columns) || (array_key_exists(strtolower($field["field_name"]), $columns) && $columns[strtolower($field["field_name"])]==$field["field_datatype"]));
	            if ( $field["field_visible"]===true && !empty($columns) ) $nbFieldsShared++;
	                
	            $properties["layerfields"][$field_index] = $field;
	        }

	        $properties["import_action"] = "update,replace";
	        if ( $nbFieldsShared==0 ){
	            	if ( empty($columns) ) $properties["import_action"] = "create";
	            	else $properties["import_action"] = "replace";
	        }
	        $properties["nbFieldsShared"] = $nbFieldsShared;
        } 
        
        if ( stripos($properties["encoding"], "UNKNOW"  )!==false ) $properties["encoding"] = "Latin-1";  
        if ( stripos($properties["encoding"], "ASCII"   )!==false ) $properties["encoding"] = "UTF-8"; 
        if ( stripos($properties["encoding"], "ISO-8859")!==false ) $properties["encoding"] = "Latin-1"; 
            
        $properties["columns"] = $columns;
        if ( $isSpreadSheet ){
          $sheets = $this->callMapserverApi($carmen_ws_helpers_getsheets, "GET", $params, array());
          $properties["sheets"] = $sheets["sheets"];
        }
        $result = array_merge($geometrytype, $properties);
        
        $result["layerfile"] = str_replace(realpath($directory."/"), "", $layerfile);
        
        return $result;
    }
    /**
     * Configure l'importeur dans Postgis. Récupère les champs du fichier au préalable
     * @param ImportInPostgis $importeur
     * @param unknown $table
     * @param unknown $fichier
     * @param unknown $metadata_id
     * @param string $feuille
     * @param string $fichierShape
     * @param string $data_encoding
     */
    protected function setImportInPostgisConfig(ImportInPostgis $importeur, $pk_couche_donnees, $layertype, $couchd_emplacement_stockage, $file_source, $fmeta_id = -1, $layersheet = null, $fichierShape = null, $data_encoding='UTF-8', $fields)
    {
    	//$properties = $this->getGISFileProperties($pk_couche_donnees, $file_source, $couchd_type_stockage, $layersheet, $layertype);
    	$importeur->setFileFields($fields);
    	$importeur->setDataExecution($couchd_emplacement_stockage, $file_source, $fmeta_id?:-1, $layersheet, $fichierShape, $data_encoding);
    	
    }
    
    /**
     * Get all fields in db table with unique values
     * @param ImportInPostgis $importeur
     * @return array
     */
    protected function getPGTableUniqueFields(ImportInPostgis $importeur)
    {
    	$champs = array();
			$pgChps = $importeur->GetChampsPostGIS();
			foreach ( $pgChps as $name => $type ){
				if ( strtolower($name) !="the_geom" && strtolower($name) !="gid" ) {
					if ($importeur->IsChampUnique($name)){
						$champs[utf8_encode($name)] = array("field_name"=>$name);
					}
				}
			}
			return array_values($champs);
    }
    
    /**
     * Supprime les domaines sous-domaines de la métadonnée à ses couches
     * @param Connection $CATALOGUE
     * @param int $fmeta_id
     * @param int $pk_couche_donnees
     */
    protected function detachDomaineSousDomaine(Connection $CATALOGUE,  $pk_couche_donnees)
    {
    	$params = array($pk_couche_donnees);
    	
    	!$CATALOGUE->isTransactionActive() && $CATALOGUE->beginTransaction();
    	$tabSql = array(
    		"delete from ssdom_dispose_couche where ssdcouch_fk_couche_donnees=:pk_couche_donnees"
    	);
    	try {
	    	foreach($tabSql as $query){
	    		$CATALOGUE->executeQuery($query, $params);
	    	}
	    	$CATALOGUE->isTransactionActive() && $CATALOGUE->commit();
    	} catch (\Exception $exception){ 
    		$CATALOGUE->isTransactionActive() && $CATALOGUE->rollBack();
    	}
    }
    
    /**
     * Affecte les domaines sous-domaines de la métadonnée à ses couches
     * @param Connection $CATALOGUE
     * @param int $fmeta_id
     * @param int $pk_couche_donnees
     */
    protected function attachDomaineSousDomaine(Connection $CATALOGUE,  $fmeta_id, $pk_couche_donnees, $closeTransaction=true)
    {
    	$params = compact("pk_couche_donnees", "fmeta_id");
    	
    	!$CATALOGUE->isTransactionActive() && $CATALOGUE->beginTransaction();
    	$tabSql = array(
    		"delete from ssdom_dispose_couche where ssdcouch_fk_couche_donnees=:pk_couche_donnees"
    	, "insert into ssdom_dispose_couche (ssdcouch_fk_couche_donnees, ssdcouch_fk_sous_domaine) ".
    		" select donnees.pk_couche_donnees, ssdcouch_fk_sous_domaine".
    		" from ssdom_dispose_metadata dommeta ".
    		" inner join public.metadata meta using (uuid) ".
    		" inner join fiche_metadonnees fmeta on (fmeta.fmeta_id=meta.id::text) ".
    		" inner join couche_donnees donnees on (donnees.pk_couche_donnees = fmeta.fmeta_fk_couche_donnees) ".
    		" where fmeta.fmeta_id=:fmeta_id ".
    		" and donnees.pk_couche_donnees=:pk_couche_donnees"
    	);
    	try {
	    	foreach($tabSql as $query){
	    		$CATALOGUE->executeQuery($query, $params);
	    	}
	    	$closeTransaction && $CATALOGUE->isTransactionActive() && $CATALOGUE->commit();
    	} catch (\Exception $exception){ 
    		$closeTransaction && $CATALOGUE->isTransactionActive() && $CATALOGUE->rollBack();
    	}
    }
    
    /**
     * Exectute the import of the data for an insertion into Postgis Database
     * @param Request $request
     * @param unknown $fmeta_id
     * @param string $layertype
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function doImportInPostgis(ParameterBag $parametersBag, $fmeta_id, $layertype="VECTOR", $closeTransaction=true, $schema=self::PRODIGE_SCHEMA_PUBLIC)
    {
        $PRODIGE = $this->getProdigeConnection(self::PRODIGE_SCHEMA_PUBLIC);
        $CATALOGUE = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE);
        
        $result = null;
        /**
         * extraction des paramètres 
         * @link ImportDataController::verifyIntegrityAction
         */
        {
            !$CATALOGUE->isTransactionActive() && $CATALOGUE->beginTransaction();
            !$PRODIGE->isTransactionActive() && $PRODIGE->beginTransaction();
          
            $defaults = $this->container->getParameter('jsdefaults', array());
            $l_layertype = strtolower($layertype);
            $typeCouche = $l_layertype;
            /*TODO*/
            $xField = $yField = $zField = "";
            $couchd_nom = "Nom inconnu";
            
            //extract all submitted values
            $requestValues = $parametersBag->all();
            extract($requestValues);
            
            $fields = $parametersBag->get('layerfields', array());
            if ( empty($fields) ) $fields = array("drops"=>array(), "types"=>array(), "xField"=>null, "yField"=>null);
            if ( !isset($fields["drops"]) ) $fields["drops"] = array();
            $strDropFields = implode(",", $fields["drops"]);
            $tabTypeFields = array();
            foreach($fields["types"] as $field){
                $tabTypeFields[($field["field_name"])] = $field["field_datatype"];
            }
            
            $import_action = $parametersBag->get('import_action', 'create');
            $encoding_source = $parametersBag->get('encoding_source', 'auto');
            $projection_source = $parametersBag->get('projection_source', 'auto');
            $projection_cible = $parametersBag->get('projection_cible', $defaults["PROJECTION"]);
            if ( $projection_source=="auto" ) {
                if ( $layertype=="MNT" ){
                    $projection_source = $projection_cible;
                } else {
                    $projection_source = -1;
                }
            }
            
            $couchd_emplacement_stockage = $parametersBag->get('couchd_emplacement_stockage', '');
            $couchd_type_stockage = intval(array_search(strtolower($layertype), self::$DATATYPE_TO_STOCKAGE));
            
            $file_georeferenced = $parametersBag->get('file_georeferenced', false);
            $file_source = $parametersBag->get('file_source', '');
            $file_sheet = $parametersBag->get('file_sheet', '');
            $field_key = $parametersBag->get('field_key', null);
            
            if ( $file_georeferenced ){
                $xField = ($fields["xField"]);
                $yField = ($fields["yField"]);
            }
            
            $pk_couche_donnees = $parametersBag->get('pk_couche_donnees', 0);
            $synchronisation_mapfiles = $parametersBag->get('synchronisation_mapfiles', false);
        }
    
        $importeur = null;
        
	      try {
          
            // IMPORT IN POSTGIS
            $importeur = new ImportInPostgis($this, $schema, $this->getParameter("CMD_OGR2OGR", "ogr2ogr"));
        
            //EXIT IF NO DATA SOURCE SUBMITTED
            if ( $file_source=="" ){ 
            	$date = $CATALOGUE->fetchColumn("select changedate from couche_donnees where pk_couche_donnees=:pk_couche_donnees", compact("pk_couche_donnees"));
            	if ( $date ) $result = "Importée le ".date("d/m/Y à H:i:s", strtotime($date));
            	else $result = "Import à faire";
            	$fmeta_id && $this->attachDomaineSousDomaine($CATALOGUE, $fmeta_id, $pk_couche_donnees, $closeTransaction);
            	
            	$couche_fields = $importeur->getFeatureCatalogFields($couchd_emplacement_stockage);
            	$this->generateGeosourceFeatureCatalogue($fmeta_id, $couchd_nom, $couchd_emplacement_stockage, $couche_fields);
            	$this->updateDomSDomMetadata($fmeta_id);
            	$this->updateAtomArboMetadata($fmeta_id);
            	return array("status"=>"NOTHING_TO_DO", "success"=>true, "result"=>array("import_status"=>$result, "pk_couche_donnees"=>$pk_couche_donnees));
            }
            

          $this->setImportInPostgisConfig($importeur, $pk_couche_donnees, $layertype, $couchd_emplacement_stockage, $file_source, $fmeta_id, $file_sheet, null, $encoding_source, $fields["types"]);
	        //$importeur->setDataExecution($couchd_emplacement_stockage, $file_source, $fmeta_id, $file_sheet, null, "LATIN1");
	        if ( !$importeur->IsExistTableStockage() ){  
	        	if ($import_action=="update") $import_action = "create";
	        }
	        $bIsOk = true;
	        
	        
	        // IMPORT IN DATABASE
	        switch($import_action){
	        	case "create" :
	        		
							if ( $bIsOk ) {
								if ( $xField == "" && $yField == "" && strtolower($importeur->GetTypeFichier())==strtolower(ImportInPostgis::$EXT_CSV) ) {
									$result = $importeur->CreatingDataFromTable($strDropFields, $typeCouche, $tabTypeFields);
								} else {
									$result	= $importeur->CreatingData($projection_source, $projection_cible, $xField, $yField, $zField, $strDropFields, $typeCouche, $tabTypeFields );
								}
							}
							
							//TODO : $res = json_decode(file_get_contents("http://".$PRO_SITE_URL."/PRRA/Services/setMetadataVisualisable.php?mode=".$bViusalisable."&metadata_id=".$metadata_id));
		          
							
	        	break;
	        	case "update" :
							if ( empty($field_key)  ) {
								throw new \Exception("Erreur: Aucune clé d'intégration n'est définie");
							} else {
								$result = array();
 					      $result[] = $importeur->ArchivingData();
								if ( $bIsOk ) {
									if ( $xField == "" && $yField == "" && strtolower($importeur->GetTypeFichier())==strtolower(ImportInPostgis::$EXT_CSV) ) {
										$result[] = $importeur->UpdatingDataFromTable($field_key, $strDropFields, $tabTypeFields);
									} else {
										$result[]	= $importeur->UpdatingData($field_key, $projection_source, $projection_cible, $xField, $yField, $zField, $strDropFields, $typeCouche, $tabTypeFields);
									}
								}
							}
	        	break;
	        	case "replace" :
							$result = array();
 					   // $result[] = $importeur->IntegritingData($xField, $yField, $zField, $strDropFields, $tabTypeFields);
	            $result[] = $importeur->ArchivingData();
							if ( $bIsOk ) {
								if ( $xField == "" && $yField == "" && strtolower($importeur->GetTypeFichier())==strtolower(ImportInPostgis::$EXT_CSV) ) {
									$result[] = $importeur->ReplacingDataFromTable($strDropFields, $typeCouche, $tabTypeFields);
								} else {
									$result[]	= $importeur->ReplacingData($projection_source, $projection_cible, $xField, $yField, $zField, $strDropFields, $typeCouche, $tabTypeFields);
								}
							}
	        	break;
	        }// FIN IMPORT IN DATABASE
	        
	        if ( $this->getEnv()!="dev" ){
	            if ( strpos(realpath(dirname($file_source))."/", realpath(rtrim($this->getMapfileDirectory(), "/")."/temp")."/")!==false ){ 
	                $importeur->DeletingFileTempo($file_source, strtolower($layertype));
	            }
	        }
	        
	        
					$PRODIGE->executeQuery("set search_path to public");
					$tab = explode(".", $couchd_emplacement_stockage);
					$PRODIGE->executeQuery("select populate_geometry_columns((:schemaname||'.'||:tablename)::regclass)", array("tablename"=>end($tab), "schemaname"=>$schema));
          $PRODIGE->isTransactionActive() && $PRODIGE->commit();
          
					//On met à jour l'information couchd_visualisable apres import de donnees (si 3D => 0, sinon =>1)
					if ( $pk_couche_donnees ){
						if ( $layertype=="MNT" ) $couchd_visualisable = "0";
						else $couchd_visualisable = "1";
						$CATALOGUE->executeQuery("update couche_donnees set changedate=now(), couchd_visualisable=:couchd_visualisable where pk_couche_donnees=:pk_couche_donnees", compact("couchd_visualisable", "pk_couche_donnees"));
					
        		$synchronisation_mapfiles && $synchronisation_mapfiles = $this->curlToCatalogue("/geosource/dellayersfrommap/".$couchd_type_stockage."/".$couchd_emplacement_stockage);
        		
        		
	        	if ( !is_bool($synchronisation_mapfiles) ){
	        		$synchronisation_mapfiles = json_decode($synchronisation_mapfiles, true);
	        		if ( $synchronisation_mapfiles ) {
	        			if ( $synchronisation_mapfiles["success"] ){
	        				$result[] = $synchronisation_mapfiles["action"];
	        				$CATALOGUE->executeQuery("update couche_donnees set couchd_wms=0, couchd_wfs=0 where pk_couche_donnees=:pk_couche_donnees", compact("couchd_visualisable", "pk_couche_donnees"));
						
	        			}
	        		}
	        	}        	
        		$fmeta_id && $this->attachDomaineSousDomaine($CATALOGUE, $fmeta_id, $pk_couche_donnees, $closeTransaction);
        		
						$closeTransaction && $CATALOGUE->isTransactionActive() && $CATALOGUE->commit();
						$closeTransaction && $PRODIGE->isTransactionActive() && $PRODIGE->commit();
						
						$couche_fields = $importeur->getFeatureCatalogFields();
						$this->generateGeosourceFeatureCatalogue($fmeta_id, $couchd_nom, $couchd_emplacement_stockage, $couche_fields);
	          $this->updateDomSDomMetadata($fmeta_id);
	          $this->updateAtomArboMetadata($fmeta_id);
	          
					} else {
						$CATALOGUE->isTransactionActive() && $CATALOGUE->commit();
					}
	      } 
	      catch(\Exception $exception){
            $PRODIGE->isTransactionActive () && $PRODIGE->rollBack ();
						$CATALOGUE->isTransactionActive() && $CATALOGUE->rollBack();
            
            $message = $exception->getMessage ();
            $message = "Une erreur s'est produite".(php_sapi_name()=="cli" ? "<br/>".$message."<br/>(".basename($exception->getFile())."[".$exception->getLine()."])" : "");
            
            throw new ApiException ( $message, array(
                "data" => explode("\n", str_replace( array("<br>", "<br/>"), array("\n", "\n"), $exception->getMessage())),
                "function" => $exception->getFile() . "(" . $exception->getLine() . ")",
                "stack" => explode("#", $exception->getTraceAsString()),
                "status" => "FAILURE",
                "details" => "<div class='error_details'>" . preg_replace("!Une\s+erreur\s+s'est\s+produite\s*[:.]\s*!i", "", $message) . "</div>",
                "result" => array("import_status" => $message),
                "success" => false 
            ), $exception->getCode(), $exception);
        }
	      
	      $import_status = $result;
        //$import_status = (is_array($result) ? array_map("utf8_encode", $result) : $result);
        $import_status = array_merge((array)$import_status, array("Importée le ".date("d/m/Y à H:i:s", strtotime($CATALOGUE->fetchColumn("select changedate from couche_donnees where pk_couche_donnees=:pk_couche_donnees", compact("pk_couche_donnees"))))) );
        $import_status = array_diff($import_status, array(null, ""));
        $import_status = implode("<br/>", $import_status);
        $import_action = ($import_action=="create" ? "update" : $import_action);
        $unique_fields = $this->getPGTableUniqueFields($importeur);
        list($couchd_emplacement_stockage, $couchd_wms, $couchd_wfs) = $CATALOGUE->fetchArray("select couchd_emplacement_stockage, couchd_wms, couchd_wfs from couche_donnees where pk_couche_donnees=:pk_couche_donnees", compact("pk_couche_donnees"));
        $file_source = "";
        $file_name = "";
        $layerfields = array();
	      return array("status"=>"SUCCESS", "success"=>true, "result"=>compact(
	      	"import_status", 
	      	"pk_couche_donnees", 
	      	"import_action", 
	      	"unique_fields", 
	      	"couchd_emplacement_stockage",
	      	"couchd_wms",
	      	"couchd_wfs",
	      	"file_source", 
	      	"file_name", 
	      	"layerfields"
	      ));
    }
    
    
    /**
     * Exectute the import of the data for an insertion into Postgis Database
     * @param Request $request
     * @param unknown $fmeta_id
     * @param string $layertype
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function doImportRaster(ParameterBag $parametersBag, $fmeta_id, $layertype, $closeTransaction=true)
    {
        $CATALOGUE = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE);
	      $dataDirectory = rtrim($this->getMapfileDirectory(), "/")."/";
	      
        $result = null;
        
        
        $delete_files = array();
        
        //extract all submitted values
        $requestValues = $parametersBag->all();
        extract($requestValues);

        $import_action = $parametersBag->get('import_action', 'create');
        
        
        $file_source = $parametersBag->get('file_source', '');
        
    
	        
        
        $temp_source = $file_source;
        $file_source = preg_replace("!^(/?)temp/!", "$1raster/", $file_source);
        @mkdir($dataDirectory.dirname($file_source), 0770, true);
        if ( $temp_source!=$file_source ){
            $extension = pathinfo($dataDirectory.$temp_source, PATHINFO_EXTENSION);
            $bCopy = true; 
            if ( strtolower($extension)=="shp" ){
                $extensions = array("shp", "dbf", "shx", "prj", 'qpj');
            } else {
                $extensions = array_merge(array($extension), self::$ADDITIONAL_RASTER_EXT);
            }
            $_temp_source = preg_replace("!".$extension."$!", "", $temp_source);
            $_file_source = preg_replace("!".$extension."$!", "", $file_source);
            $extensions = array_merge($extensions, array_map("strtolower", $extensions), array_map("strtoupper", $extensions));
            $extensions = array_unique($extensions);
            foreach ($extensions as $extension){
                if ( !file_exists($dataDirectory.$_temp_source.$extension) ) continue;
                $bCopy = $bCopy && @copy($dataDirectory.$_temp_source.$extension, $dataDirectory.$_file_source.$extension);
                $delete_files[] = $dataDirectory.$_temp_source.$extension;
            }
            
            if ( !$bCopy ){
                $file_source = $temp_source;
                $delete_files = array();
            }
        }
        
        $couchd_emplacement_stockage = preg_replace("!^Publication/!", "", $file_source);
        $file_source = preg_replace("!^Publication/!", "", $file_source);
        $couchd_type_stockage = array_search(strtolower($layertype), self::$DATATYPE_TO_STOCKAGE);
        $synchronisation_mapfiles = $parametersBag->get('synchronisation_mapfiles', false);
        
        
        try {
          !$CATALOGUE->isTransactionActive() && $CATALOGUE->beginTransaction();
          
    
	        $pk_couche_donnees = $parametersBag->get('pk_couche_donnees', 0);
	        $old_emplacement = $CATALOGUE->fetchColumn("select couchd_emplacement_stockage from couche_donnees where pk_couche_donnees=:pk_couche_donnees", compact("pk_couche_donnees"));
	        
	        $coucheFields = array("pk_couche_donnees", "couchd_id", "couchd_nom", "couchd_description", "couchd_type_stockage", "couchd_emplacement_stockage", "couchd_wms", "couchd_wfs");
	        
	        if ( !$pk_couche_donnees ){
	          // ajout d'une nouvelle couche
	        	$pk_couche_donnees = $CATALOGUE->fetchColumn("select nextval('seq_couche_donnees'::regclass)", array(), 0);
	        	$couchd_id = (string)$pk_couche_donnees;
	        	
	        	$couchd_visualisable = 1;
	        	$coucheFields[] = "couchd_visualisable";
	        	
	        	//force value to 1
	        	$couchd_fk_acces_server = 1;
	        	$coucheFields[] = "couchd_fk_acces_server";
	        	$CATALOGUE->executeQuery("insert into couche_donnees (".implode(", ", $coucheFields).") values (:".implode(", :", $coucheFields).")", compact($coucheFields));
	        	$CATALOGUE->executeQuery("insert into fiche_metadonnees (fmeta_id, fmeta_fk_couche_donnees) values (:fmeta_id, :pk_couche_donnees)", compact("fmeta_id", "pk_couche_donnees"));
	        	
	        } else {
	          // maj d'une couche existante
	        	$couchd_id = (string)$pk_couche_donnees;
	        	
	        	//force value to 1
	        	$couchd_fk_acces_server = 1;
	        	$coucheFields[] = "couchd_fk_acces_server";
	        	$set = array_combine($coucheFields, $coucheFields);
	        	$set = str_replace("=", "=:", http_build_query($set, null, ", "));
	        	$CATALOGUE->executeQuery("update couche_donnees set ".$set." where pk_couche_donnees=:pk_couche_donnees", compact($coucheFields));
	        }
        
	        if ( $file_source=="" || !file_exists($dataDirectory.$file_source) ){ 
	        	$date = $CATALOGUE->fetchColumn("select changedate from couche_donnees where pk_couche_donnees=:pk_couche_donnees and couchd_visualisable=1", compact("pk_couche_donnees"));
	        	if ( $date ) $result = "Importée le ".date("d/m/Y à H:i:s", strtotime($date));
	        	else $result = "Import à faire";
        	  $this->attachDomaineSousDomaine($CATALOGUE, $fmeta_id, $pk_couche_donnees, $closeTransaction);
	        	$closeTransaction && $CATALOGUE->isTransactionActive() && $CATALOGUE->commit();
	        	return array("status"=>"NOTHING_TO_DO", "success"=>true, "f"=>$file_source, "result"=>array("import_status"=>$result, "pk_couche_donnees"=>$pk_couche_donnees, "import_action"=>"update"));
	        }
	        
	        ImportRaster::initCommands($this->getParameter("CMD_OGR2OGR", "ogr2ogr"));
	        
	        $rasterInfo = ImportRaster::rasterToVRT($pk_couche_donnees, $dataDirectory, $dataDirectory.$file_source, $this->getParameter("PRODIGE_DEFAULT_EPSG", "2154"));
	        $rasterFields = array_keys($rasterInfo);
	        if ( $CATALOGUE->fetchColumn("select true from raster_info where id=:pk_couche_donnees", compact("pk_couche_donnees"))!==true ){
	        	$CATALOGUE->executeQuery("insert into raster_info (".implode(", ", $rasterFields).") values (:".implode(", :", $rasterFields).")", $rasterInfo);
	        } else {
	        	$set = array_combine($rasterFields, $rasterFields);
	        	$set = str_replace("=", "=:", http_build_query($set, null, ", "));
        	  $CATALOGUE->executeQuery("update raster_info set ".$set." where id=:id", $rasterInfo);
	        }
	        
	        if ( $old_emplacement && $old_emplacement!=$couchd_emplacement_stockage ){
	        	$layerChangeNameService = new LayerChangeName($CATALOGUE);
						$layerChangeNameService->ScanDirectory($dataDirectory, $old_emplacement, $couchd_emplacement_stockage); //Publication
						$layerChangeNameService->ScanDirectory($dataDirectory."/layers", $old_emplacement, $couchd_emplacement_stockage); //Publication/layers 
	        }
	        
					//On met à jour l'information couchd_visualisable apres import de donnees (si 3D => 0, sinon =>1)
					$couchd_visualisable = 1;
					$CATALOGUE->executeQuery("update couche_donnees set changedate=now(), couchd_visualisable=:couchd_visualisable where pk_couche_donnees=:pk_couche_donnees", compact("couchd_visualisable", "pk_couche_donnees"));
					
	        $this->attachDomaineSousDomaine($CATALOGUE, $fmeta_id, $pk_couche_donnees, $closeTransaction);
	        
          $closeTransaction && $CATALOGUE->isTransactionActive() && $CATALOGUE->commit();
          !$CATALOGUE->isTransactionActive() && $CATALOGUE->beginTransaction();
	        
	        $this->updateDomSDomMetadata($fmeta_id);
          
        	$synchronisation_mapfiles && $this->curlToCatalogue("/geosource/dellayersfrommap/".$couchd_type_stockage."/".$couchd_emplacement_stockage);
        	
          $closeTransaction && $CATALOGUE->isTransactionActive() && $CATALOGUE->commit();
	      } catch(\Exception $exception){
          $closeTransaction && $CATALOGUE->isTransactionActive() && $CATALOGUE->rollBack();
	      	//throw $exception;
	        $message = $exception->getMessage();
	        $message = "Une erreur s'est produite.".(php_sapi_name()=="cli" ? "<br/>".$message."<br/>(".basename($exception->getFile())."[".$exception->getLine()."])" : "");
	        
	      	throw new ApiException("Une erreur s'est produite", array(
	      			"data" => explode("\n", str_replace(array("<br>", "<br/>"), array("\n", "\n"), $exception->getMessage())),
	      			"function"=>$exception->getFile()."(".$exception->getLine().")", 
	      			"stack"=>explode("#", $exception->getTraceAsString()), 
	      			"status"=>"FAILURE", 
	      			"details" => "<div class='error_details'>" . preg_replace("!Une\s+erreur\s+s'est\s+produite\s*[:.]\s*!i", "", $message)."</div>",
	      			"result" => array("import_status"=>$message),
	      			"success"=>false
	      	));
	      }
    
	      @array_map("unlink", $delete_files);
        
				//On met à jour l'information couchd_visualisable apres import de donnees (si 3D => 0, sinon =>1)
				$CATALOGUE->executeQuery("update couche_donnees set changedate=now(), couchd_visualisable=:couchd_visualisable where pk_couche_donnees=:pk_couche_donnees", compact("couchd_visualisable", "pk_couche_donnees"));
				//On met à jour le status d'import
	      $CATALOGUE->executeQuery("update couche_donnees set changedate=now(), couchd_visualisable=:couchd_visualisable where pk_couche_donnees=:pk_couche_donnees", compact("couchd_visualisable", "pk_couche_donnees"));
	          
        $import_status = (is_array($result) ? array_map("utf8_encode", $result) : $result);
        $import_status = array_merge((array)$import_status, array("Importée le ".date("d/m/Y à H:i:s", strtotime($CATALOGUE->fetchColumn("select changedate from couche_donnees where pk_couche_donnees=:pk_couche_donnees", compact("pk_couche_donnees"))))) );
        $import_status = array_diff($import_status, array(null, ""));
        $import_status = implode("<br/>", $import_status);
        
        $import_action = "update";
	      return array("status"=>"SUCCESS", "success"=>true, "result"=>compact("import_status", "pk_couche_donnees", "import_action", "file_source"));
    }

    /**
     * Checks if the table name (couchd_emplacement_stockage) is unique and exists
     * @param string $couchd_emplacement_stockage
     * @param integer $pk_couche_donnees
     * @return array(unique, table_exists)
     */
    public function isUniqueStorageAction($couchd_emplacement_stockage, $pk_couche_donnees=-1)
    {
        $connection = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE);
        
        $sql = " select 1 ".
               " from couche_donnees cd ".
               " where cd.couchd_emplacement_stockage=:couchd_emplacement_stockage".
               " and cd.pk_couche_donnees <> :pk_couche_donnees";
        $exists = $connection->executeQuery($sql, compact("couchd_emplacement_stockage", "pk_couche_donnees"));
    
        $unique = ($exists->rowCount()==0);
        $table_exists = true;
        if ( $unique ){
	        $connection = $this->getProdigeConnection();
        	$table_schema = self::PRODIGE_SCHEMA_PUBLIC;
	        $sql = " select 1 ".
	               " from information_schema.tables  ".
	               " where table_name=:couchd_emplacement_stockage".
	               " and table_schema = :table_schema";
	        $exists = $connection->executeQuery($sql, compact("couchd_emplacement_stockage", "table_schema"));
	        $table_exists = ($exists->rowCount()>0);
        }
        return compact("unique", "table_exists");
    }
    /**
     * Get all (all depth) GIS files extracted from the ZIP file according to layertype (VECTOR|RASTER|TABULAIRE) and only if all required files by extension are present
     * @param string $filezip    The full path to the ZIP file
     * @param string $layertype  VECTOR|RASTER|TABULAIRE
     * @throws \Exception
     * @return array
     */
    protected function analyseZipFile($filezip, $layertype){
	      $tableMaxLength = 60; 
	      $DATATYPE_TO_STOCKAGE = array_change_key_case(array_flip(self::$DATATYPE_TO_STOCKAGE), CASE_UPPER);
	      $DATATYPE_TO_STOCKAGE["MULTI"] = $DATATYPE_TO_STOCKAGE["VECTOR"];
	      if ( !file_exists($filezip) ){
	      	throw new \Exception("L'archive ".basename($filezip)." n'existe pas");
	      }
		    $extractTo = str_replace(".".pathinfo($filezip, PATHINFO_EXTENSION), "", $filezip);
		    
	    	$zip = new \ZipArchive();
	    	$res = $zip->open($filezip);
	      if ($res === TRUE) {
	      	$bOk = $zip->extractTo($extractTo);
	      	$zip->close();
	      	if ( !$bOk ) throw new \Exception("Impossible d'extraire l'archive ".basename($filezip));
	      } else {
	      	$oClass = new \ReflectionClass("\\ZipArchive");
          $cst = $oClass->getConstants();
	      	throw new \Exception("Impossible d'ouvrir l'archive ".($filezip)." [".$res."={".print_r($cst, 1)."}]");
	      }
	      $insert_files = array();
	      
		    $ext_georeferenced = array();
	      switch($layertype){
	      	case "VECTOR" :
		      	$ext_georeferenced = self::$DATATYPE_EXTENSION["TABULAIRE"];
	      	case "MULTI" :
		      	$ext_vectoriel = array_keys(self::$DATATYPE_EXTENSION["VECTOR"]);
		      	//recherche dans le répertoire de tous les fichiers (en profondeur) ayant une des extensions 
		      	$cmd = "find ".$extractTo." -iname '*.".implode("' -or -iname '*.", array_merge($ext_vectoriel, $ext_georeferenced))."' ";
		      	$files = array();
		      	exec($cmd, $files);
		      	
		      	foreach($files as $file){
		      		$georeferenced = false;
		      		$pathinfo = pathinfo($file);
		      		$extension = strtolower($pathinfo["extension"]);
		      		$no_extension_file = str_ireplace(".".$extension, "", $file);
		      		$inserted_file = array(
		      			"couchd_nom" => $pathinfo["filename"],
		      			"file" => $file,
		      			"stockage" => strtolower(substr(preg_replace("!\W!", "_", $pathinfo["filename"]), 0, $tableMaxLength)),
		      			"type_stockage" => $DATATYPE_TO_STOCKAGE[$layertype],
		      			"extension" => $pathinfo["extension"],
		      			"no_extension_file" => $no_extension_file,
		      			"required_extension" => array(),
		      			"georeferenced" => false,
		      		);
		      		if ( in_array($extension, $ext_vectoriel) ){
		      			// fichier vectoriel => des fichiers associés sont obligatoires
		      			$inserted_file["required_extension"] = array_merge(self::$REQUIRED_EXTENSION["VECTOR"][$extension], array($extension));
		      		} else {
		      			// fichier georeferencé
		      			$inserted_file["georeferenced"] = true;
		      		}
		      		$insert_files[$pathinfo["filename"]] = $inserted_file;
		      	}
		      	
		      break;
	      	case "MNT" :
		      	$ext_georeferenced = self::$DATATYPE_EXTENSION["MNT"];
		      	//recherche dans le répertoire de tous les fichiers (en profondeur) ayant une des extensions 
		      	$cmd = "find ".$extractTo." -iname '*.".implode("' -or -iname '*.", $ext_georeferenced)."' ";
		      	$files = array();
		      	exec($cmd, $files);
		      	foreach($files as $file){
		      		$georeferenced = false;
		      		$pathinfo = pathinfo($file);
		      		$extension = strtolower($pathinfo["extension"]);
		      		$no_extension_file = str_ireplace(".".$extension, "", $file);
		      		$insert_files[$pathinfo["filename"]] = array(
		      			"couchd_nom" => $pathinfo["filename"],
		      			"file" => $file,
		      			"stockage" => strtolower(substr(preg_replace("!\W!", "_", $pathinfo["filename"]), 0, $tableMaxLength)),
		      			"type_stockage" => $DATATYPE_TO_STOCKAGE[$layertype],
		      			"extension" => $pathinfo["extension"],
		      			"no_extension_file" => $no_extension_file,
		      			"required_extension" => array(),
		      			"georeferenced" => true,
		      		);
		      	}
		      break;
	      	case "TABULAIRE" :
		      	$ext_georeferenced = self::$DATATYPE_EXTENSION["TABULAIRE"];
		      	//recherche dans le répertoire de tous les fichiers (en profondeur) ayant une des extensions 
		      	$cmd = "find ".$extractTo." -iname '*.".implode("' -or -iname '*.", $ext_georeferenced)."' ";
		      	$files = array();
		      	exec($cmd, $files);
		      	foreach($files as $file){
		      		$georeferenced = false;
		      		$pathinfo = pathinfo($file);
		      		$extension = strtolower($pathinfo["extension"]);
		      		$no_extension_file = str_ireplace(".".$extension, "", $file);
		      		$insert_files[$pathinfo["filename"]] = array(
		      			"couchd_nom" => $pathinfo["filename"],
		      			"file" => $file,
		      			"stockage" => strtolower(substr(preg_replace("!\W!", "_", $pathinfo["filename"]), 0, $tableMaxLength)),
		      			"type_stockage" => $DATATYPE_TO_STOCKAGE[$layertype],
		      			"extension" => $pathinfo["extension"],
		      			"no_extension_file" => $no_extension_file,
		      			"required_extension" => array(),
		      			"georeferenced" => false,
		      		);
		      	}
		      break;
	      	case "RASTER" :
	      		//TODO
		      	$ext_raster = self::$DATATYPE_EXTENSION["RASTER"]["RASTER"];
		      	$ext_tileindex = array_keys(self::$DATATYPE_EXTENSION["RASTER"]["TILEINDEX"]);
		      	//recherche dans le répertoire de tous les fichiers (en profondeur) ayant une des extensions 
		      	$cmd = "find ".$extractTo." -iname '*.".implode("' -or -iname '*.", array_merge($ext_raster, $ext_tileindex))."' ";
		      	$files = array();
		      	exec($cmd, $files);
		      	foreach($files as $file){
		      		$georeferenced = false;
		      		$pathinfo = pathinfo($file);
		      		$extension = strtolower($pathinfo["extension"]);
		      		$no_extension_file = str_ireplace(".".$extension, "", $file);
		      		$inserted_file = array(
		      			"couchd_nom" => $pathinfo["filename"],
		      			"file" => $file,
		      			"stockage" => $pathinfo["filename"],
		      			"type_stockage" => $DATATYPE_TO_STOCKAGE[$layertype],
		      			"extension" => $pathinfo["extension"],
		      			"no_extension_file" => $no_extension_file,
		      			"required_extension" => array(),
		      			"georeferenced" => false,
		      		);
		      		if ( in_array($extension, $ext_tileindex) ){
		      			// fichier vectoriel => des fichiers associés sont obligatoires
		      			// fichier georeferencé
		      			$inserted_file["georeferenced"] = true;
		      			$inserted_file["stockage"] = substr(preg_replace("!\W!", "_", $pathinfo["filename"]), 0, $tableMaxLength);
		      			$inserted_file["required_extension"] = array_merge(self::$REQUIRED_EXTENSION["RASTER"]["TILEINDEX"][$extension], array($extension));
		      		}
		      		
		      		$insert_files[$pathinfo["filename"]] = $inserted_file;
		      	}
		      break;
	      
	      }
	      ksort($insert_files);
	      $tmp = array();
	      foreach ($insert_files as $inserted_file){
	      	$no_extension_file = $inserted_file["no_extension_file"];
      		$required_extension = $inserted_file["required_extension"];
	      	if ( !empty($required_extension) ){
      			$searchAll = glob($no_extension_file.".*");
      			$required_exists = true;
      			$searchAll = array_map("strtolower", $searchAll);
      			foreach ($required_extension as $extension){
      				$required_exists = $required_exists && in_array(strtolower($no_extension_file.".".$extension), $searchAll);
      			}
      			if ( !$required_exists ) continue;//pas d'ajout du fichier en base s'il manque un des fichiers requis
	      	}
	      	$tmp[] = $inserted_file;
	      }
	      $insert_files = $tmp;
	      return $insert_files;
	  }
	  

	  /**
	   * generate the Raster Tileindex for all available files in directory
	   * @param $extension string
	   * @param $shapefile string
	   * @param $directory string 
	   */
    public function generateRasterTileindexAction($extension, $shapefile, $directory)
    {
        $defaults = $this->container->getParameter('jsdefaults', array());
        $extensionInit = $extension;
        $rootDir = realpath($this->getMapfileDirectory())."/";
        $extractTo = rtrim(realpath($rootDir.$directory), "/")."/";
        $toDirectory = preg_replace("!".$rootDir."temp/!", $rootDir."raster/", $extractTo);
        @mkdir($toDirectory, 0770, true);
        
        
        if ( !file_exists($extractTo) || !is_dir($extractTo) ) 
            return $this->formatJsonError(404, "Le répertoire {$directory} n'existe pas."); 
        
        $selectedFiles = array();
        $selectedExtension = null;
        if ( $extension=="_first_" ){
            $extensionInit = "";
            $extensions = self::$DATATYPE_EXTENSION["RASTER"]["RASTER"];
            $extensions = array_merge(array_map("strtoupper", $extensions), array_map("strtolower", $extensions));
            foreach($extensions as $extension){
                $selectedFiles = glob($extractTo."*.".$extension);
                if ( !empty($selectedFiles) ){
                    $selectedExtension = $extension;
                    break;
                }
            }
        } else {
            $selectedFiles = glob($extractTo."*.".$extension);
            if ( !empty($selectedFiles) ){
                $selectedExtension = $extension;
            }
        }
        
        if ( is_null($selectedExtension) ) 
            return $this->formatJsonError(404, "Aucun fichier Raster ".($extensionInit ? "d'extension ".$extensionInit : "")." dans le répertoire {$directory}"); 
        
        $files = array_combine($selectedFiles, $selectedFiles);
        
        if ( $extractTo!=$toDirectory ){
            $files = array();
            foreach ($selectedFiles as $file){
                $newfile = str_replace($extractTo, $toDirectory, dirname($file)."/").preg_replace("![^\w.]!", '_', basename($file));
                if ( rename($file, $newfile) )
                    $files[$file] = $newfile;
                
            }
        }
        $rm = "rm -rf ".$toDirectory.$shapefile.".sh* ".$toDirectory.$shapefile.".prj ".$toDirectory.$shapefile.".dbf ".$toDirectory.$shapefile.".qpj ";
        
        $shapefilePath = $toDirectory.$shapefile.".shp";
        
        $logFile = dirname($shapefilePath)."/gdaltindex.log";
        
        $cmd = $rm." ; gdaltindex -t_srs EPSG:".$defaults["PROJECTION"]." ".$shapefilePath." ".implode(" ", array_map('escapeshellarg', $files))." 2> ".$logFile;
        $messages = array();
        $last = exec($cmd, $messages, $return);
        
        $success = $selectedFiles;
        $failures = array();
        $logs = trim(file_get_contents($logFile));
        if ( $logs=="" ){
            @unlink($logFile);
            $logFile = null;
        } else {
            foreach($files as $oldfile=>$newfile){
                if ( strpos($logs, $newfile)!==false ){
                    $failures[] = $oldfile; 
                    unset($success[array_search($oldfile, $success)]);
                }
            }
        }
        if ( $return ){//there is an error
            foreach($files as $old=>$new){
                if ( file_exists($new) && !file_exists($old) ) copy($old, $new);
            }
            return $this->formatJsonError(404, "La génération du fichier d'index n'a pas réussi : <br/>[{$cmd}]".implode("<br/>", $messages)); 
        }
        
        $shapefile = str_replace($rootDir, "", $shapefilePath);
        $success = str_replace($rootDir, "/Publication/", $success);
        $failures = str_replace($rootDir, "/Publication/", $failures);
        $logs = str_replace($rootDir, "/Publication/", $logs);
        return $this->formatJsonSuccess(array("cmd"=>$cmd, "logs"=>nl2br($logs), "file_source"=>$shapefile, "success"=>$success, "failures"=>$failures, "rastersused"=>$files)); 
       
    }
}