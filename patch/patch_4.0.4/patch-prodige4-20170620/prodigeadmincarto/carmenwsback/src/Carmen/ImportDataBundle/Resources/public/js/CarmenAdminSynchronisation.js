Ext.define('Carmen.model.SynchronisationSources', {
    extend: 'Ext.data.Model',
    fields: [{
        name: 'title',
        type: 'string'
    }, {
        name: 'description',
        type: 'string'
    }, {
        name: 'import_data_source',
        type: 'string'
    }, {
        name: 'import_data_type',
        type: 'string'
    }, {
        name: 'type',
        type: 'string'
    }, {
        name: 'content-type',
        type: 'string'
    }, {
        name: 'is_selected',
        type: 'boolean',
        defaultValue : false
    }]
}); 
/**
 * This example is an advanced tree example. It illustrates:
 *
 * - Multiple headers
 * - Preloading of nodes with a single AJAX request
 * - Header hiding, showing, reordering and resizing
 * - useArrows configuration
 * - Keyboard Navigation
 * - Discontiguous selection by holding the CTRL key
 * - Using custom iconCls
 * - singleExpand has been set to true
 */
Ext.define('Carmen.view.tree.SynchronisationSources', {
    extend: 'Ext.tree.Panel',
    
    requires: [
        'Ext.data.*',
        'Ext.grid.*',
        'Ext.tree.*'
        //'Ext.ux.CheckColumn'
    ],    
    xtype: 'tree-grid',
    
    reserveScrollbar: true,
    
    title: 'Sources possibles de téléchargement des données SIG issues des liens de la métadonnée',
    minHeight: 370,
    useArrows: false,
    rootVisible: false,
    rowLines : true,   
    border : true,
    initComponent: function() {
    	var me = this;
      this.width = '100%';
      
      Ext.apply(this, {
        rootVisible: false,
        viewConfig : {emptyText : 'Aucune source de téléchargement', deferEmptyText : false, loadMask :true},
        store: new Ext.data.TreeStore({
        	autoLoad : true,
          model: Carmen.model.SynchronisationSources,
          proxy: {
            type: 'ajax',
            url: document.location.href+'/js'
          }
        }),
        
        columns: {
        	defaults : {align:'center',padding:'5 0 5 0', flex:0.5},
        	items : [{
        		align : undefined,
            xtype: 'treecolumn', //this is so we know which column will show the tree
            text: 'Source',
            flex: 2,
            sortable: true,
            dataIndex: 'title',
            renderer : function(value, meta, record){
            	var tpl = {
            		'{title}' : record.get('title') || record.get('description'),
            		'<i>{description}</i>' : record.get('description')
            	};
            	var content = [], values = [];
            	Ext.iterate(tpl, function(item, value){
            		if ( !Ext.isEmpty(value) && values.indexOf(value)==-1 ){
            			content.push(item);
            			values.push(value);
            			tpl[item.replace(/.*\{(\w+)\}.*/, '$1')] = value;
            		}
            	});
              return new Ext.XTemplate(content.join('<br/>')).apply(tpl);
            }
          },{
            text: 'Type de lien de métadonnée',
            sortable: true,
            dataIndex: 'type'
          },{
            text: 'Type de contenu',
            sortable: true,
            dataIndex: 'content-type'
          },{
            text: 'Ouvrir le lien',
            flex:0.3,
            sortable: true,
            dataIndex: 'import_data_source',
            xtype : 'widgetcolumn',
            defaultBindProperty : 'value',
            widget : {
              defaultBindProperty : 'value', setValue : function(v){this.value=v;},
            	xtype : 'button',
            	text : 'Consulter',
            	handler : function(btn){
            		window.open(btn.value)
            	}
//            }
//            renderer : function(value){
//            	return '<a href="'+value+'" target="_blank">Consulter</a>';
            }
          },{
            text: 'Lien de synchronisation', 
            flex:0.5,
            
            sortable: true,
            dataIndex: 'import_data_source',
            xtype : 'widgetcolumn',
            onWidgetAttach : function(column, widget, record){
            	if ( record.synchronisation_radio &&  record.synchronisation_radio.id!=widget.id ) {
            	  record.synchronisation_radio.destroy();
            	  
            	}
            	record.synchronisation_radio = widget;
            },
            widget : {
              defaultBindProperty : 'inputValue', setInputValue : function(v){this.inputValue=v;},
            	name : 'synchronisation',
            	//bind : {inputValue : '{value}'},
            	xtype : 'radio',
            	listeners : {
            		afterrender : function(radio){
            			var record = radio.getWidgetRecord();
            			var selected = me.getStore().findRecord('is_selected', true) || me.getStore().getAt(0);
            			if ( selected==record ){
            				radio.setValue(true);
            			}
                  radio.el.setVisibilityMode(Ext.dom.Element.VISIBILITY);
            			radio.setVisible( record.get('leaf')==true );
            		},
            		change : function(radio, checked){
            			var selModel = me.getSelectionModel();
            			if ( !selModel ) return;
            			selModel[(checked ? 'select' : 'deselect')]([radio.getWidgetRecord()])
            		}
            	}
            }
          }]
        }
      });
      this.callParent();
    }
});


Ext.onReady(function() {
	var labelWidth = 350;
	Ext.tip.QuickTipManager.init();
	
	var form = Ext.create('Ext.form.Panel', {
		submitForm : function(mode){
      if ( !this.isValid() ) return false; 
      var selection = this.down('tree-grid').getSelectionModel().getSelection();
      if ( !selection || selection.length==0 ){
        Ext.Msg.alert('Erreur de saisie', 'Veuillez sélectionner un source de donnée pour la synchronisation des données.');
        return false;
      }
      this.mask((mode=="execute" ? "Enregistrement et exécution immédiate en cours..." : "Enregistrement en cours..."));
      var record = selection[0];
      var submitData = Ext.apply(this.getForm().getFieldValues(), record.data);
      Ext.Ajax.request({
      	timeout :  24*60*60*1000,
        url : Routing.generate('prodige_synchronisation_save', {mode : mode, uuid : Carmen.metadata.uuid}),
        method : 'POST',
        jsonData : submitData,
        scope : this,
        success : function(){
          this.unmask();
          Ext.Msg.alert((mode=="execute" ? "Enregistrement et exécution immédiate réussie" : "Enregistrement réussi"), "L'enregistement"+(mode=="execute" ? " et l'exécution immédiate de l'import ont " : " a ")+"réussi.");
        },
        failure : function(status){
          this.unmask();
          if (status.timedout) {
            Ext.Msg.alert((mode=="execute" ? "Erreur d'exécution" : "Erreur d'enregistrement"), "Le temps maximum d'exécution est dépassé");
          }
        }
      })
		},
    width : '100%',
    layout : {type : 'vbox', pack:'top', align:'left'},
    defaults : {labelWidth : labelWidth, labelSeparator : '&nbsp;&nbsp;', labelAlign : 'right'},
    items : [{
      xtype : 'displayfield',
      fieldLabel : 'Identifiant',
      name : 'uuid',
      value : (Carmen.metadata ? Carmen.metadata.uuid : null)
    }, {
      xtype : 'displayfield',
      fieldLabel : 'Nom de la métadonnée',
      name : 'metadata_title',
      value : (Carmen.metadata ? Carmen.metadata.metadata_title : null)
    },{ 
      xtype : 'panel',
      border : true,
      width : '100%',
      title : 'Configuration de la tâche de synchronisation',
      padding : '10 10 10 10',
      items : [{
        xtype : 'panel',
        margin : '10 10 0 10',
        defaults : {labelWidth : labelWidth, labelSeparator : '&nbsp;&nbsp;', labelAlign : 'right'},
        layout : 'form',
        items : [{
          margin : '10 10 0 10',
        	xtype : 'fieldcontainer',
          fieldLabel : "Table d'import",
        	layout:{type:'table', tableAttrs : {width:'100%'}},
        	items : [{
        	  width:'100%',
            xtype : 'textfield',
            maxLength : 1024,
            name : 'import_table',
            vtype : 'identifier',
            allowBlank : false
          }, {
          	xtype : 'displayfield',
          	width : 400,
          	value : "&nbsp;(préfixe des tables créées en cas d'import de données multiples)"
          }]
        },{
          margin : '10 10 0 10',
          xtype : 'radiogroup',
          columns: 1,
          fieldLabel : 'Fréquence de synchronisation',
          items : [{
          	xtype : 'radio',
          	name : 'type_frequency',
          	inputValue : 'fixed',
            value : true,
          	boxLabel : 'Copie figée : Une seule synchronisation'
          }, {
          	layout : {type : 'hbox', pack:'top', align:'left'},
          	xtype : 'fieldcontainer',
          	items : [{
          		xtype : 'radio',
              inputValue : 'repeted',
              name : 'type_frequency',
          		boxLabel : 'Copie vivante : Périodique tous les &nbsp;',
          		listeners : {
          			change : function(radio, checked){
          				radio.nextSibling('numberfield').allowBlank =  !checked;
          				radio.nextSibling('numberfield').setValue((checked ? 1 : null));
                  radio.nextSibling('numberfield').validate();
          			}
          		}
          	}, {
              name : 'import_frequency',
              minValue : 1,
          		xtype : 'numberfield'
          	}, {xtype : 'displayfield', value:'&nbsp;jour(s)'}]
          }]
        }]
      }, {
        xtype : 'tree-grid'
      }]
  	}],
    buttonAlign : 'center',
    buttons : [{
      text : 'Fermer',
      handler : function(){window.close()}        
    },'->', {
      text : 'Enregistrer',
      tooltip : "Ajouter la configuration à la tâche de synchronisation des données",
      tooltipType : 'title',
      handler : function(){
        form.submitForm("save");
      }
    },{
      text : 'Enregistrer et exécuter',
      tooltip : "Ajouter la configuration à la tâche de synchronisation des données et Lancement d'une première exécution",
      tooltipType : 'title',
      handler : function(){
      	form.submitForm("execute");
      }
    }
    ]
  });
  form.loadRecord(Ext.create('Ext.data.Model', Carmen.task));
  Ext.create('Ext.container.Viewport', {
    layout : 'fit',
    renderTo : 'main',
    items : [form]
  });
});
