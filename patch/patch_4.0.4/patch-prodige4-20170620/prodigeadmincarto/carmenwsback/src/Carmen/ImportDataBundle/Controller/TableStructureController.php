<?php

namespace Carmen\ImportDataBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\JsonResponse;

use Prodige\ProdigeBundle\Controller\BaseController as ProdigeBaseController;
use Carmen\ApiBundle\Controller\BaseController as CarmenBaseController;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

use Prodige\ProdigeBundle\Common\Util;
use Doctrine\DBAL\Types\Type;

/**
 * @Route("/importdata/table_structure")
 */
class TableStructureController extends AbstractImportDataController {
    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/index/{metadata_id}/{table_name}", name="carmen_tablestructure_index", options={"expose"=true}, defaults={"table_name"=null}, requirements={"metadata_id"})
     */
    public function indexAction(Request $request, $metadata_id, $table_name=null) {
        $configReader = $this->get('prodige.configreader');
        global $PRO_IMPORT_EPSG;
        $CATALOGUE = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE);
        
        if ( !$table_name )
            $table_name = $CATALOGUE->fetchColumn("select couchd_emplacement_stockage from catalogue.couche_donnees inner join catalogue.fiche_metadonnees on (fmeta_fk_couche_donnees=pk_couche_donnees) where fmeta_id=:fmeta_id", array("fmeta_id"=>$metadata_id));
        
                
        $url_back = $request->get("url_back", null);
        $ar_parameters = array("url_back"=>$url_back, "table_name"=>$table_name, "metadata_id"=>$metadata_id, "PRODIGE_DEFAULT_EPSG" =>$PRO_IMPORT_EPSG );

        return $this->render('CarmenImportDataBundle:Default:tableStructureManager.html.twig', $ar_parameters);

    }
    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/services/{action}", name="carmen_tablestructure_services", options={"expose"=true}, requirements={"action"})
     */
    public function servicesAction(Request $request, $action) {
        if(isset($action)) {
            $action .= 'Action';
            if ( method_exists($this, $action) ) return $this->$action($request);
        } 
        return new JsonResponse(array('error'=>"Fonction [{$action}] non définie"));
    }
    

    protected function transactionalQueries(\Doctrine\DBAL\Connection $connection, array $queries, array $params=array(), array $types=array()) 
    {
        try {
            $connection->beginTransaction();
            foreach($queries as $query){
                $connection->executeQuery($query, $params, $types);
            }
            $connection->commit();
            return true;
        }catch(\Exception $exception){
            $connection->rollBack();
            throw $exception;
        }
        return false;
    }
    

    protected function createTableAction(Request $request) 
    {
        $PRODIGE   = $this->getProdigeConnection(self::PRODIGE_SCHEMA_PUBLIC);
        $CATALOGUE = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE);
        $metadata_id      = $fmeta_id = $request->get("metadata_id", null);
        $tableName        = strtolower($request->get("tableName", null));
        $geometryType     = $request->get("geometryType", null);
        $projectionsValue = $request->get("projectionsValue", null);
    
        $createTable = false;
        if(isset($tableName) && isset($geometryType) && isset($projectionsValue) && isset($metadata_id)) {
            $schema = self::PRODIGE_SCHEMA_PUBLIC;
            try {
                $queries = array(
                    "DROP TABLE IF EXISTS ".$tableName,
                    "DROP SEQUENCE IF EXISTS ".$tableName."_gid_seq",
                    "DELETE FROM public.geometry_columns WHERE f_table_name = :f_table_name",
                    "CREATE SEQUENCE ".$tableName."_gid_seq  START 1",
                    "CREATE TABLE ".$schema.".".$tableName." (gid bigint DEFAULT nextval('".$tableName."_gid_seq'::regclass) NOT NULL PRIMARY KEY, the_geom geometry(".$geometryType.",".$projectionsValue.") NOT NULL)",
                    "CREATE INDEX idx_gist_base_prodige_".$tableName."_geometry ON ".$tableName." USING gist (the_geom)",
                    "INSERT INTO public.geometry_columns (f_table_catalog, f_table_schema, f_table_name, f_geometry_column, coord_dimension, srid, type) VALUES (:f_table_catalog, :f_table_schema, :f_table_name, :f_geometry_column, :coord_dimension, :srid, :type)",
                );
                $params = array(
                    "f_table_catalog" => "",
                    "f_table_schema" => $schema,
                    "f_table_name" => $tableName,
                    "f_geometry_column" => "the_geom",
                    "coord_dimension" => 2,
                    "srid" => $projectionsValue,
                    "type" => $geometryType,
                );
                
                
                $pk_couche_donnees = $CATALOGUE->fetchColumn("select pk_couche_donnees from catalogue.couche_donnees inner join catalogue.fiche_metadonnees on (fmeta_fk_couche_donnees=pk_couche_donnees) where fmeta_id=:fmeta_id and couchd_emplacement_stockage=:couchd_emplacement_stockage", array("fmeta_id"=>$metadata_id, "couchd_emplacement_stockage"=>$tableName));
                $addCoucheDonnees = false;
                if ( !$pk_couche_donnees ){
                    $pk_couche_donnees = $CATALOGUE->fetchColumn("select nextval('seq_couche_donnees'::regclass)", array(), 0);
                    $couchd_emplacement_stockage = $tableName;
                    list($unique, $table_exists) = array_values($this->isUniqueStorageAction($couchd_emplacement_stockage, $pk_couche_donnees));
                    $i = 1;
                    while ( !$unique ){
                      $couchd_emplacement_stockage = $tableName."_".($i++);
                      list($unique, $table_exists) = array_values($this->isUniqueStorageAction($couchd_emplacement_stockage, $pk_couche_donnees));
                    }
                    $queries = str_replace($tableName, $couchd_emplacement_stockage, $queries);
                    $params = str_replace($tableName, $couchd_emplacement_stockage, $params);
                    $tableName = $couchd_emplacement_stockage;
                    $addCoucheDonnees = true;
                }
                $createTable = $this->transactionalQueries($PRODIGE, $queries, $params);

                if ( $addCoucheDonnees ){
                    $coucheFields = array(
                    "pk_couche_donnees" => $pk_couche_donnees, 
                    "couchd_id" => (string)$pk_couche_donnees, 
                    "couchd_nom" => "Structure d'édition libre", 
                    "couchd_description" => "Structure d'édition libre", 
                    "couchd_type_stockage" => self::DEFAULT_TYPE_STOCKAGE, 
                    "couchd_emplacement_stockage" => $tableName,
                    "couchd_visualisable" => 1,
                    "couchd_wms" => 0, 
                    "couchd_wfs" => 0,
                    "couchd_fk_acces_server" => 1,
                    );
                    $queries = array(
                      "insert into catalogue.couche_donnees (".implode(", ", array_keys($coucheFields)).") values (:".implode(", :",  array_keys($coucheFields)).")",
                      "insert into catalogue.fiche_metadonnees (fmeta_id, fmeta_fk_couche_donnees) values (:fmeta_id, :pk_couche_donnees)", 
                    );
                    $params = array_merge($params, 
                      $coucheFields,
                      compact("fmeta_id", "pk_couche_donnees")
                    );
                    
                    $createTable = $createTable && $this->transactionalQueries($CATALOGUE, $queries, $params);
                    
                    $this->attachDomaineSousDomaine($CATALOGUE, $fmeta_id, $pk_couche_donnees);
                }
            }catch(\Exception $exception){throw $exception;
                return new JsonResponse(array("error" => "Erreur : ".$exception->getMessage()));
            }
        }
        return new JsonResponse(array( "createTable" => $createTable, "tableName"=>$tableName ));
    }

    protected function dropTableAction(Request $request) 
    {
        $metadata_id       = $request->get("metadata_id", null);
        $tableName         = strtolower($request->get("tableName", null));
        $dropTable = false;

        $CATALOGUE = $this->getCatalogueConnection("catalogue");
        if((isset($tableName)) && ($tableName != "")) {
            $schema = self::PRODIGE_SCHEMA_PUBLIC;

            try {
                list($pk_fiche_metadonnees, $pk_couche_donnees) = $CATALOGUE->fetchArray("select pk_fiche_metadonnees, pk_couche_donnees from catalogue.couche_donnees inner join catalogue.fiche_metadonnees on (fmeta_fk_couche_donnees=pk_couche_donnees) where fmeta_id=:fmeta_id and couchd_emplacement_stockage=:couchd_emplacement_stockage", array("fmeta_id"=>$metadata_id, "couchd_emplacement_stockage"=>$tableName));
                if ( $pk_fiche_metadonnees && $pk_couche_donnees){
                    $queries = array(
                        "delete from catalogue.fiche_metadonnees where pk_fiche_metadonnees=:pk_fiche_metadonnees", 
                        "delete from catalogue.couche_donnees where pk_couche_donnees=:pk_couche_donnees", 
                    );
                    $params = compact("pk_fiche_metadonnees", "pk_couche_donnees");
                    $dropTable = $this->transactionalQueries($CATALOGUE, $queries, $params);
                }
                $queries = array(
                    "DROP TABLE IF EXISTS ".$tableName, 
                    "DROP SEQUENCE IF EXISTS ".$tableName."_gid_seq",
                );
                $params = array();
                $dropTable = $this->transactionalQueries($this->getProdigeConnection("public"), $queries, $params);
            }catch(\Exception $exception){
                return new JsonResponse(array("error" => "Erreur : ".$exception->getMessage()));
            }
        }
        
        return new JsonResponse(array( "dropTable" => $dropTable ));
        
    }

    protected function alterTableAction(Request $request) 
    {
        $schema = self::PRODIGE_SCHEMA_PUBLIC;
    
        $PRODIGE = $this->getProdigeConnection($schema);
        $tableName        = strtolower($request->get("tableName", null));
        $fieldsToAdd      = $request->get("fieldsToAdd", null);
        $alterTable = false;
    
        if(isset($fieldsToAdd) && isset($tableName)) {
            $queries = array();
            
            $fieldsToAddJSON = json_decode($fieldsToAdd);
            
    
            $alters = array();
            foreach($fieldsToAddJSON as $cmpt=>$value) {
                
                $strSQL = "";
                if($cmpt < 2) {
                    continue;
                }
                else {
                    if($value->Type == 'enum') {
                        if($value->EnumName != "") {
                            $strSQL.= " ADD \"".$value->FieldName."\" \"".$value->EnumName."\"" ;
                            if($value->Mandatory) {
                                $strSQL.= " NOT NULL";
                            }
                            if($value->DefaultValue != "") {
                                $strSQL.= " DEFAULT '".$PRODIGE->convertToDatabaseValue(trim($value->DefaultValue), Type::STRING)."'";
                            }
                        } else continue;
                    }
                    else {
                        $strSQL.= " ADD \"".$value->FieldName."\" ".$value->Type."" ;
                        if($value->Type=='varchar') {
                            if($value->VarcharLength != "") {
                                $strSQL.= "(".$value->VarcharLength.")";
                            }
                            if($value->Mandatory) {
                                $strSQL.= " NOT NULL";
                            }
                            if($value->DefaultValue != "") {
                                $strSQL.= " DEFAULT '".$PRODIGE->convertToDatabaseValue($value->DefaultValue, Type::STRING)."'";
                            }
                        }
                        elseif($value->Type=='date') {
                            if($value->Mandatory) {
                                $strSQL.= " NOT NULL";
                            }
                            if($value->DefaultValue != "") {
                                $strSQL.= " DEFAULT '".$PRODIGE->convertToDatabaseValue($value->DefaultValue, Type::STRING)."'";
                            }
                        }
                        elseif(($value->Type=='bigint') || ($value->Type=='real')) {
                            if($value->Mandatory) {
                                $strSQL.= " NOT NULL";
                            }
                            if($value->DefaultValue !== "") {
                                $strSQL.= " DEFAULT ".$PRODIGE->convertToDatabaseValue($value->DefaultValue, ($value->Type=='bigint' ? Type::BIGINT : Type::FLOAT));
                            }
                        }
                        else continue;
                    }
    
                    $alters[] = $strSQL;
                }
            }
            if ( !empty($alters) ){
                $queries[] = "ALTER TABLE ".$schema.".".$tableName." ".implode(", ", $alters);
            }
            foreach($fieldsToAddJSON as $i=>$value) {
                if($value->Comment != '') {
                    $queries[] = " COMMENT ON COLUMN ".$schema.".".$tableName.".\"".$value->FieldName."\" IS '".$PRODIGE->convertToDatabaseValue(trim($value->Comment), Type::STRING)."'";
                }
            }
            try {
                $alterTable = $this->transactionalQueries($this->getProdigeConnection("public"), $queries);
            }catch(\Exception $exception){
                return new JsonResponse(array("error" => "Erreur : ".$exception->getMessage()));
            }
            
        }
        return new JsonResponse(compact( "alterTable" ));
    }

    protected function getAllEnumAction(Request $request) {
    
        $enums = array();
        
        try{
            $PRODIGE = $this->getProdigeConnection(self::PRODIGE_SCHEMA_PUBLIC);
            $strSQL = "select n.nspname as enum_schema, t.typname as enum_name, string_agg(e.enumlabel, ', ') as enum_value from pg_type t join pg_enum e on t.oid = e.enumtypid join pg_catalog.pg_namespace n ON n.oid = t.typnamespace group by enum_schema, enum_name";
            $enums = $PRODIGE->fetchAll($strSQL);
        }catch(\Exception $exception){
            return new JsonResponse(array("error" => "Erreur : ".$exception->getMessage()));
        }
        return new JsonResponse(array(
            "getAllEnum" => true,
            "allEnums" => $enums ?: array()
        ));
    }

    protected function createEnumAction(Request $request) {
    
        $schema = self::PRODIGE_SCHEMA_PUBLIC;
        $PRODIGE = $this->getProdigeConnection($schema);
        
        $alterEnum =  false;
        $createEnum = false;
    
        $enum = $request->get("enum", null);
        $enumValuesToAdd = json_decode($request->get("enumValuesToAdd", null));
    
        if(isset($enumValuesToAdd) && isset($enum) && !empty($enumValuesToAdd)) {
            try{
                $enumValuesToAdd = array_map(function($value) use($PRODIGE) {return "'".$PRODIGE->convertToDatabaseValue(trim($value), Type::STRING)."'";}, $enumValuesToAdd);
                $strSQL = "CREATE TYPE \"". $enum."\" AS ENUM (".implode(", ", $enumValuesToAdd).")";
                $createEnum = $this->transactionalQueries($this->getProdigeConnection("public"), array($strSQL));
            }catch(\Exception $exception){
                return new JsonResponse(array("error" => "Erreur : ".$exception->getMessage()));
            }
        }
        return new JsonResponse(compact( "createEnum", "alterEnum" ));
    }

    protected function alterEnumAction(Request $request) {
        $schema = self::PRODIGE_SCHEMA_PUBLIC;
        $PRODIGE = $this->getProdigeConnection($schema);
        
        $alterEnum =  false;
        $createEnum = false;
    
        $enum = $request->get("enum", null);
        $enumValuesToAdd = json_decode($request->get("enumValuesToAdd", null));
    
        if(isset($enumValuesToAdd) && isset($enum) && !empty($enumValuesToAdd)) {
            try{
                $enumValuesToAdd = array_map(function($value) use($PRODIGE) {return "'".$PRODIGE->convertToDatabaseValue(trim($value), Type::STRING)."'";}, $enumValuesToAdd);
                $queries = array();
                foreach($enumValuesToAdd as $value){
                    $queries[] = "ALTER TYPE \"". $enum."\" ADD VALUE IF NOT EXISTS ".$value;
                }
                foreach($queries as $query){
                   $PRODIGE->executeQuery($query);
                }
                $alterEnum = true;
            }catch(\Exception $exception){
                return new JsonResponse(array("error" => "Erreur : ".$exception->getMessage()));
            }
        }
        return new JsonResponse(compact( "createEnum", "alterEnum" ));
    }

    protected function deleteEnumAction(Request $request) {

        $enum = $request->get("enum", null);
        $deleteEnum = false;
    
        if(isset($enum)) {
            try{
                $deleteEnum = $this->transactionalQueries($this->getProdigeConnection("public"), array("DROP TYPE \"".$enum."\""), array());
            }catch(\Exception $exception){
                return new JsonResponse(array("error" => "Erreur : ".$exception->getMessage()));
            }
        }
        return new JsonResponse(compact( "deleteEnum" ));
    }

}
