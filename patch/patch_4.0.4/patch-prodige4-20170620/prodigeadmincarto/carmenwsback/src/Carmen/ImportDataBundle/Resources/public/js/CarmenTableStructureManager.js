/**
 *
 */

// Globals
var fieldName_global = '';
var json_enums_global = '';
var enum_name_global = '';
var backUrl_global = '';

function init(metadata_id, tableName, backUrl) {
	_metadata_id = metadata_id;
  backUrl_global = backUrl;
  fCreateFormPanelStep1(metadata_id, tableName, !!tableName);
}

//-----------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------

function fCreateFormPanelStep1(metadata_id, tableName, isReadOnly) {

  var cmb_geometryType_store = Ext.create('Ext.data.Store', {
    fields : ['text', 'value'],
    data : [{
      'text' : 'Point',
      'value' : 'MULTIPOINT'
    }, {
      'text' : 'Ligne',
      'value' : 'MULTILINESTRING'
    }, {
      'text' : 'Polygone',
      'value' : 'MULTIPOLYGON'
    }]
  });

  var cmb_projectionsValues_store = Ext.create('Ext.data.JsonStore', {
    fields : [{name:'text', mapping:'display'}, {name:'value', mapping:'epsg'}],
    proxy : {
      type : 'ajax',
      url : Routing.generate('prodige_getProjections')
    },
    autoLoad : true
  });

  var formPanelStep1 = Ext.create('Ext.form.Panel', {
    id : 'CreationFormPanelStep1_id',
    frame : true,
    title : "Création d'une donnée (étape 1)",
    //labelWidth : 150,
    width : '50%',
    x : '50%',
    align : 'center',
    heigth : 150,
    bodyPadding : 10,
    renderTo : 'form_panel_to_render', // id for a html div
    fieldDefaults : {
      labelWidth : 200,
      width : '100%',
      labelAlign : 'right',
      labelSeparator : ''
    },
    items : [ {
      id : 'txt_TableName_id',
      xtype : 'textfield',
      name : 'TableName_name',
      fieldLabel : 'Nom de la table',
      value : (tableName) ? tableName : '',
      minLength : 3,
      maxLength : 55,
      vtype : 'alphanum',
      readOnly : isReadOnly ? true : false
    }, {
      id : 'nbr_FieldNumber_id',
      xtype : 'numberfield',
      name : 'FieldNumber_name',
      fieldLabel : 'Nombre de champs attributaires',
      value : 1,
      minValue : 1,
      maxValue : 99
    }, {
      id : 'cmb_GeometryType_id',
      xtype : 'combo',
      name : 'GeometryType_name',
      fieldLabel : 'Type de la géométrie',
      store : cmb_geometryType_store,
      queryMode : 'local', //important
      allowBlank : false,
      editable : false,
      valueField : 'value',
      displayField : 'text',
      value : 'POLYGON'
    }, {
      id : 'cmb_Projection_id',
      xtype : 'combo',
      name : 'Projection_name',
      fieldLabel : 'Projection',
      store : cmb_projectionsValues_store,
      value : PRODIGE_DEFAULT_EPSG,
      queryMode : 'local', //important
      allowBlank : false,
      editable : false,
      valueField : 'value',
      displayField : 'text'
    }],
    buttons : (isReadOnly ? [{
      text : 'Supprimer la table',
      handler : function() {
        Ext.Msg.confirm('Suppression de la table', 'Confirmez-vous la suppression de la table ?', function(btn){
          if ( btn!="yes" ) return;
          dropTable(Ext.getCmp('txt_TableName_id').getValue(), function(response) {
            var response = Ext.decode(response.responseText);
            //console.log('response is : ', response);
            if (response.dropTable) {
              document.location.reload() 
            } else {
              Ext.Msg.alert('Echec', "La suppression n'a pas réussi").setIcon(Ext.Msg.ERROR);
            }
          })
        })
      }
    }, '->'] : []).concat([{
      id : 'btn_NextStep_id',
      text : 'Etape suivante',
      handler : function() {
        if ((Ext.getCmp('txt_TableName_id').getValue().trim() !== '')
            && (Ext.getCmp('nbr_FieldNumber_id').getValue() > 0)
            && (Ext.getCmp('nbr_FieldNumber_id').getValue() < 100)
            && (Ext.getCmp('cmb_GeometryType_id').getValue() != null)
            && (Ext.getCmp('cmb_Projection_id').getValue() != null)
        ) {
          fieldName_global = Ext.getCmp('txt_TableName_id').getValue();
          var numberOfFields = Ext.getCmp('nbr_FieldNumber_id').getValue();
          var doNext = function(){
            Ext.Ajax.request({
              url: Routing.generate('carmen_tablestructure_services', {action: 'createTable'}),
              method : 'POST',
              params : {
                metadata_id : metadata_id,
                tableName : fieldName_global,
                geometryType : Ext.getCmp('cmb_GeometryType_id') .getValue(),
                projectionsValue : Ext.getCmp('cmb_Projection_id').getValue()
              },
              success : function(response) {
                var response = Ext.decode(response.responseText);
                //console.log('response is : ', response);
                if (response.createTable) {
                  formPanelStep1.destroy();
                  fCreateFormPanelStep2(response.tableName, numberOfFields);
                } else {
                  Ext.Msg.alert('Erreur', "La table n'a pas été créée").setIcon(Ext.Msg.ERROR);
                }
              },
              failure : function(response) {
                Ext.Msg.alert('Erreur', "La table n'a pas été créée").setIcon(Ext.Msg.ERROR);
              }
            });
          };
          
        	if ( isReadOnly ){
        		Ext.Msg.confirm('Confirmation', 'La table existe déjà et sera recréée (suppression puis création) si vous poursuivez.<br>Confirmez-vous la réinitialisation de la structure de table ?', 
        		function(btn){
        			if ( btn!="yes" ) return;
        			doNext();
        		})
        	} else {
        		doNext();
        	}
        } else {
          Ext.Msg.alert('Attention', 'Attention aux valeurs saisies ...');
        }
      }
    }, {
      text : 'Fermer',
      handler : function() {
        formPanelStep1.destroy();
        backUrl_global && (document.location.href = backUrl_global);
        !backUrl_global && window.close() 
      }
    }])
  });
}

//-----------------------------------------------------------------------------------------------------------------------------
var gPanelStep2_store = Ext.create('Ext.data.Store', {
  fields : ['FieldName', 'Type', 'VarcharLength', 'Enum', 'DefaultValue', 'Comment', 'Mandatory'],
  data : [{
    'FieldName' : 'gid',
    'Type' : 'real',
    'VarcharLength' : '',
    'Enum' : '',
    'DefaultValue' : '',
    'Comment' : '',
    'Mandatory' : 'Oui'
  }, {
    'FieldName' : 'the_geom',
    'Type' : 'geometry',
    'VarcharLength' : '',
    'Enum' : '',
    'DefaultValue' : '',
    'Comment' : '',
    'Mandatory' : 'Oui'
  }]
});

var enum_store_step2  = Ext.create('Ext.data.Store', { fields : ['text', 'value'], data : [] });

var enum_values_store = Ext.create('Ext.data.Store', { fields : ['text', 'value'], data : [] });

function fGetAllEnum(enum_store) {

  json_enums_global = '';

  Ext.Ajax.request({
    url: Routing.generate('carmen_tablestructure_services', {action: 'getAllEnum'}),
    method : 'GET',
    success : function(response) {
      json_enums_global = Ext.decode(response.responseText);
      if (json_enums_global.getAllEnum) {
        var data = json_enums_global.allEnums;
        for (var i = 0; i < data.length; i++) {
          enum_store.add({
            'text' : data[i]['enum_name'],
            'value' : data[i]['enum_name']
          });
        }
      } else {
        console.log('problem in getAllEnum');
      }

    },
    failure : function(response) {
      console.log('Failure : action = getAllEnum - response is : ', response.responseText);
    }
  });
}

function fRefreshEnumStore(enum_store) {
  json_enums_global = '';
  enum_store.removeAll();
  fGetAllEnum(enum_store);
}

function fRefreshEnumValuesStore(enum_name, values_store) {

  var data = json_enums_global.allEnums;
  if (data.length > 0) {
    var str_enum_values = '';
    var arr_enum_values = [];
    for (var i = 0; i < data.length; i++) {
      if (data[i]['enum_name'] == enum_name) {
        str_enum_values = data[i]['enum_value'];
        break;
      }
    }
    arr_enum_values = str_enum_values.split(',');
    for (var i = 0; i < arr_enum_values.length; i++) {
      values_store.add({
        'text' : arr_enum_values[i],
        'value' : arr_enum_values[i]
      });
    }
  }
  return arr_enum_values;
}

function fIsValide(gPanelStore) {
  var fieldsNameArray = [];
  for (var i = 0; i < gPanelStore.data.length; i++) {
    if (fieldsNameArray.indexOf(gPanelStore.getAt(i).data['FieldName']) == -1) {
      fieldsNameArray.push(gPanelStore.getAt(i).data['FieldName']);
    }
  }
  return (fieldsNameArray.length == gPanelStore.data.length) ? true : false;
}

function fCreateFormPanelStep2(fieldName, nbrOfField) {

  for (var i = 0; i < nbrOfField; i++) {
    gPanelStep2_store.add({
      'FieldName' : '',
      'Type' : '',
      'VarcharLength' : '',
      'Enum' : '',
      'DefaultValue' : '',
      'Comment' : '',
      'Mandatory' : ''
    });
  }

  var combo_store = Ext.create('Ext.data.Store', {
    fields : ['text', 'value'],
    data : [{
      'text' : 'Nombre entier (bigint)',
      'value' : 'bigint'
    }, {
      'text' : 'Nombre décimal (real)',
      'value' : 'real'
    }, {
      'text' : 'Texte (varchar)',
      'value' : 'varchar'
    }, {
      'text' : 'Date (date)',
      'value' : 'date'
    }, {
      'text' : 'Enumération (enum)',
      'value' : 'enum'
    }]
  });

  fRefreshEnumStore(enum_store_step2);
  
  var onWidgetAttach = function(column, widget, record){
    var isReadOnly = ['gid', 'the_geom'].indexOf(record.get('FieldName')) != -1;
    if (isReadOnly) {
      var readonly = widget.down('[name=readonly]');
      readonly.setVisible(true);
      readonly.setValue(record.get(column.dataIndex));
      Ext.each(widget.query(':not([name=readonly])'), function(cmp) {
        cmp.setVisible(false);
      });
      return;
    }
    // TODO
    // begin 
    widget.items.each(function(cmp) {
      cmp.on('change', function() {
        record.set(column.dataIndex, widget.getValue());
        record.commit();
      });
    }); // end
  };

  var formPanelStep2 = Ext.create('Ext.form.Panel', {
    id : 'CreationFormPanelStep2_id',
    frame : true,
    title : "Création d'une donnée (étape 2)",
    width : 'auto',
    //heigth: 550,
    bodyPadding : 10,
    renderTo : 'form_panel_to_render', // id for a html div
    items : [{
      // Label
      id : 'lbl_TableName_id',
      xtype : 'label',
      html : 'Nom de la table : ' + fieldName + '</br>'
    }, {
      // Label
      id : 'lbl_NumberOfField_id',
      xtype : 'label',
      html : '</br>Nombre de champs attributaires : ' + nbrOfField + '</br>'
    }, {
      //gridPanel
      xtype : 'gridpanel',
      id : 'GridPanelStep2_id',
      //height: 535,
      store : gPanelStep2_store,
      viewConfig : {stripeRows:true,forceFit : true, autoExpandColumn : 'Comment_id'}, autoExpandColumn : 'Comment_id',
      columnLines : true,
      columns : {
      	defaults  : {width : '14%', align : 'center'},
        items :[{
          id : 'FieldName_id',
          header : 'Nom champ',
          sortable : true,
          dataIndex : 'FieldName',
          xtype : 'widgetcolumn',
          onWidgetAttach : onWidgetAttach,
          widget : {
            fieldDefaults : {width : "100%"},
            xtype : 'fieldcontainer',
            defaultBindProperty : 'value',
            value : null,
            setValue : function(value) {
              var activeField = this.down('[name=varchar]');
              if (activeField && activeField.isVisible()) {
                activeField.setVisible(true);
                activeField.setValue(this.value);
              }
            },
            getValue : function(value) {
              var activeField = this.down('[name=varchar]');
              if (activeField && activeField.isVisible()) {
                return this.value = activeField.getValue();
              }
              var readonlyField = this.down('[name=readonly]');
              if (readonlyField && readonlyField.isVisible()) {
                return this.value = readonlyField.getValue();
              }
              return this.value;
            },
            items : [{
              xtype : 'displayfield',
              name : 'readonly',
              hidden : true
            }, {
              xtype : 'textfield',
              name : 'varchar',
              allowBlank : false,
              maxLength : 63,
              vtype : 'alphanum'
            }]
          }
        }, {
        	align : 'center',
          id : 'Type_id',
          header : 'Type',
          sortable : true,
          dataIndex : 'Type',
          xtype : 'widgetcolumn',
          onWidgetAttach : onWidgetAttach,
          widget : {
            fieldDefaults : {width : "100%"},
            xtype : 'fieldcontainer',
            defaultBindProperty : 'value',
            value : null,
            setValue : function(value) {
              var activeField = this.down('[name=combo]');
              if (activeField) {
                activeField.setValue(this.value);
                activeField.setVisible(true);
              }
            },
            getValue : function(value) {
              var activeField = this.down('[name=combo]');
              if (activeField && activeField.isVisible()) {
                return this.value = activeField.getValue();
              }
              var readonlyField = this.down('[name=readonly]');
              if (readonlyField && readonlyField.isVisible()) {
                return this.value = readonlyField.getValue();
              }
              return this.value;
            },
            items : [{
              xtype : 'displayfield',
              name : 'readonly',
              hidden : true
            }, {
              xtype : 'combo',
              name : 'combo',
              store : combo_store,
              queryMode : 'local', //important
              valueField : 'value',
              displayField : 'text',
              hidden : true
            }]
          }
        }, {
          id : 'VarcharLength_id',
          header : 'Longueur de texte',
          sortable : true,
          dataIndex : 'VarcharLength',
          xtype : 'widgetcolumn',
          onWidgetAttach : onWidgetAttach,
          widget : {
            fieldDefaults : {width : "100%"},
            xtype : 'fieldcontainer',
            defaultBindProperty : 'value',
            value : null,
            setValue : function(value) {
              var record = this.getWidgetRecord();
              if (!record) {
                return;
              }
              var currentType = this.getWidgetRecord().get('Type');
              var isNotEnumMode = ['readonly', 'real', 'bigint', 'geometry', 'date', 'varchar'].indexOf(currentType) != -1;
              if (isNotEnumMode) {
                var activeField = this.down('[name=' + currentType + ']');
                if (activeField) {
                  activeField.setValue(this.value);
                  activeField.setVisible(true);
                }
                var inactiveFields = this.query(':not([name=' + currentType + '])');
                Ext.each(inactiveFields, function(inactiveField) {
                  inactiveField.setVisible(false);
                });
              }
            },
            getValue : function(value) {
              var currentType = this.getWidgetRecord().get('Type');
              var activeField = this.down('[name=' + currentType + ']');
              if (activeField && activeField.isVisible()) {
                return this.value = activeField.getValue();
              }
              var readonlyField = this.down('[name=readonly]');
              if (readonlyField && readonlyField.isVisible()) {
                return this.value = readonlyField.getValue();
              }
              return this.value;
            },
            items : [{
              xtype : 'displayfield',
              name : 'readonly',
              hidden : true
            }, {
              xtype : 'numberfield',
              name : 'varchar',
              minValue : 1,
              maxValue : 1000,
              hidden : true
            }]
          }
        }, {
          id : 'Enum_id',
          header : 'Enumération',
          dataIndex : 'Enum',
          xtype : 'widgetcolumn',
          onWidgetAttach : onWidgetAttach,
          widget : {
            fieldDefaults : {width : "100%"},
            xtype : 'fieldcontainer',
            defaultBindProperty : 'value',
            value : null,
            setValue : function(value) {
              var record = this.getWidgetRecord();
              if (!record) {
                return;
              }
              var currentType = this.getWidgetRecord().get('Type');
              var activeField = this.down('[name=' + currentType + ']');
              if (activeField) {
                activeField.setValue(this.value);
                activeField.setVisible(true);
              }
              var inactiveFields = this.query(':not([name=' + currentType + '])');
              Ext.each(inactiveFields, function(inactiveField) {
                inactiveField.setVisible(false);
              });
            },
            getValue : function(value) {
              var currentType = this.getWidgetRecord().get('Type');
              var activeField = this.down('[name=' + currentType + ']');
              if (activeField && activeField.isVisible()) {
                return this.value = activeField.getValue();
              }
              var readonlyField = this.down('[name=readonly]');
              if (readonlyField && readonlyField.isVisible()) {
                return this.value = readonlyField.getValue();
              }
              return this.value;
            },
            items : [{
              xtype : 'displayfield',
              name : 'readonly',
              hidden : true
            }, {
              triggers : {
                edit: {
                  cls : 'prodige-trigger',
                  bodyTpl : '<i class="fa fa-edit" title="Editer l\'énumération"></i>',
                  handler: function() {
                    if ( !this.getValue() ) return;
                    var values = fRefreshEnumValuesStore(this.getValue(), enum_values_store);
                    fCreateFormPanelStep3(this.getValue(), values);
                  }
                },
                add: {
                  cls : 'prodige-trigger',
                  bodyTpl : '<i class="fa fa-plus" title="Créer une énumération"></i>',
                  handler: function() {
                    fCreateFormPanelStep3('', [] );
                  }
                }
              },
              xtype : 'combo',
              name : 'enum',
              store : enum_store_step2,
              queryMode : 'local', //important
              valueField : 'value',
              displayField : 'text',
              hidden : true,
              listeners : {
                change : function(combo){
                  enum_values_store.removeAll();
                  combo.getValue() && fRefreshEnumValuesStore(combo.getValue(), enum_values_store);
                }
              }
            }]
          }
        }, {
          id : 'DefaultValue_id',
          header : 'Valeur par défaut',
          sortable : true,
          dataIndex : 'DefaultValue',
          xtype : 'widgetcolumn',
          onWidgetAttach : onWidgetAttach,
          widget : {
            fieldDefaults : {width : "100%"},
            xtype : 'fieldcontainer',
            defaultBindProperty : 'value',
            value : null,
            setValue : function(value) {
              var record = this.getWidgetRecord();
              if (!record) {
                return;
              }
              var currentType = this.getWidgetRecord().get('Type');
              var activeField = this.down('[name=' + currentType + ']');
              if (activeField) {
                activeField.setValue(this.value);
                activeField.setVisible(true);
              }
              var inactiveFields = this.query(':not([name=' + currentType + '])');
              Ext.each(inactiveFields, function(inactiveField) {
                inactiveField.setVisible(false);
              });
            },
            getValue : function(value) {
              var currentType = this.getWidgetRecord().get('Type');
              var activeField = this.down('[name=' + currentType + ']');
              if (activeField && activeField.isVisible()) {
                return this.value = activeField.getValue();
              }
              var readonlyField = this.down('[name=readonly]');
              if (readonlyField && readonlyField.isVisible()) {
                return this.value = readonlyField.getValue();
              }
              return this.value;
            },
            items : [{
              xtype : 'displayfield',
              name : 'readonly',
              hidden : true
            }, {
              xtype : 'textfield',
              name : 'varchar',
              hidden : true
            }, {
              xtype : 'numberfield',
              name : 'real',
              minValue : 0,
              hidden : true
            }, {
              xtype : 'numberfield',
              name : 'bigint',
              minValue : 0,
              hidden : true
            }, {
              xtype : 'datefield',
              name : 'date',
              hidden : true
            }, {
              xtype : 'combo',
              name : 'enum',
              store : enum_values_store,
              queryMode : 'local', //important
              valueField : 'value',
              displayField : 'text',
              hidden : true
            }]
          }
        }, {
          id : 'Comment_id',
          header : 'Commentaire',
          sortable : true,
          dataIndex : 'Comment',
          xtype : 'widgetcolumn',
          onWidgetAttach : onWidgetAttach,
          width : "20%",
          widget : {
            fieldDefaults : {width : "100%"},
            xtype : 'fieldcontainer',
            defaultBindProperty : 'value',
            value : null,
            setValue : function(value) {
              var record = this.getWidgetRecord();
              if (!record) {
                return;
              }
              var currentType = this.getWidgetRecord().get('Type');
              if (currentType) {
                var activeField = this.down('[name=varchar]');
                if (activeField) {
                  activeField.setValue(this.value);
                  activeField.setVisible(true);
                }
                var inactiveFields = this.query(':not([name=varchar])');
                Ext.each(inactiveFields, function(inactiveField) {
                  inactiveField.setVisible(false);
                });
              }
            },
            getValue : function(value) {
              var activeField = this.down('[name=varchar]');
              if (activeField && activeField.isVisible()) {
                return this.value = activeField.getValue();
              }
              var readonlyField = this.down('[name=readonly]');
              if (readonlyField && readonlyField.isVisible()) {
                return this.value = readonlyField.getValue();
              }
              return this.value;
            },
            items : [{
              xtype : 'displayfield',
              name : 'readonly',
              hidden : true
            }, {
              xtype : 'textfield',
              name : 'varchar',
              hidden : true
            }]
          }
        }, {
          width : "9%",
          id : 'Mandatory_id',
          header : 'Obligatoire',
          sortable : true,
          dataIndex : 'Mandatory',
          xtype : 'widgetcolumn',
          onWidgetAttach : onWidgetAttach,
          widget : {
            fieldDefaults : {width : "auto"},
            xtype : 'fieldcontainer',
            defaultBindProperty : 'value',
            value : null,
            setValue : function(value) {
              var record = this.getWidgetRecord();
              if (!record) {
                return;
              }
              var currentType = this.getWidgetRecord().get('Type');
              if (currentType) {
                var activeField = this.down('[name=check]');
                if (activeField) {
                  activeField.setValue(this.value);
                  activeField.setVisible(true);
                }
                var inactiveFields = this.query(':not([name=check])');
                Ext.each(inactiveFields, function(inactiveField) {
                  inactiveField.setVisible(false);
                });
              }
  
            },
            getValue : function(value) {
              var activeField = this.down('[name=check]');
              if (activeField && activeField.isVisible()) {
                return this.value = activeField.getValue();
              }
              var readonlyField = this.down('[name=readonly]');
              if (readonlyField && readonlyField.isVisible()) {
                return this.value = readonlyField.getValue();
              }
              return this.value;
            },
            items : [{
              xtype : 'displayfield',
              name : 'readonly',
              hidden : true
            }, {
              xtype : 'checkbox',
              name : 'check',
              boxLabel : 'Oui',
              hidden : true
            }]
          }
        }]
      }
    }],
    buttons : [{
      text : 'Supprimer la table',
      handler : function() {
      	Ext.Msg.confirm('Suppression de la table', 'Confirmez-vous la suppression de la table ?', function(btn){
      		if ( btn!="yes" ) return;
      	  dropTable(fieldName_global, function(response) {
            var response = Ext.decode(response.responseText);
            //console.log('response is : ', response);
            if (response.dropTable) {
              formPanelStep2.destroy();
              gPanelStep2_store.removeAll();
              //fCreateFormPanelStep1('', false);
              backUrl_global && (document.location.href = backUrl_global);
              !backUrl_global && window.close() 
            } else {
              Ext.Msg.alert('Echec', "La suppression n'a pas réussi").setIcon(Ext.Msg.ERROR);
            }
          })
      	})
      }
    }, '->', {
      id : 'btn_SaveStep2_id',
      text : 'Enregistrer',
      handler : function() {
        //
        if (fIsValide(gPanelStep2_store)) {
          //
          var fieldsToAddArray = [];
          for (var i = 0; i < gPanelStep2_store.data.length; i++) {
            fieldsToAddArray.push({
              'FieldName'     : gPanelStep2_store.getAt(i).data['FieldName'],
              'Type'          : gPanelStep2_store.getAt(i).data['Type'],
              'VarcharLength' : gPanelStep2_store.getAt(i).data['VarcharLength'],
              'EnumName'      : gPanelStep2_store.getAt(i).data['Enum'],
              'DefaultValue'  : gPanelStep2_store.getAt(i).data['DefaultValue'],
              'Comment'       : gPanelStep2_store.getAt(i).data['Comment'],
              'Mandatory'     : gPanelStep2_store.getAt(i).data['Mandatory']
            });
          }
          //console.log('fieldsToAddArray is : ', fieldsToAddArray);
          Ext.Ajax.request({
            url: Routing.generate('carmen_tablestructure_services', {action: 'alterTable'}),
            method : 'POST',
            params : {
              tableName : fieldName_global,
              fieldsToAdd : JSON.stringify(fieldsToAddArray)
            },
            success : function(response) {
              var response = Ext.decode(response.responseText);
              //console.log('response is : ', response);
              if (response.alterTable) {
                //Ext.getCmp('btn_SaveStep2_id').setDisabled(true);
                backUrl_global && (document.location.href = backUrl_global);
                !backUrl_global && Ext.Msg.alert('Réussi', "L'ajout des champs à la table a réussi").setIcon(Ext.Msg.INFO);
                Ext.getCmp('btn_SaveStep2_id').setDisabled(true);
              } else {
                Ext.Msg.alert('Echec', "L'ajout des champs à la table n'a pas réussi").setIcon(Ext.Msg.ERROR);
              }
              
            },
            failure : function(response) {
              Ext.Msg.alert('Echec', "L'ajout des champs à la table n'a pas réussi").setIcon(Ext.Msg.ERROR);
            }
          });
        } else {
          Ext.Msg.alert('Attention', 'Les noms des champs doivent être rempli et leurs noms vont être unique').setIcon(Ext.Msg.ERROR);
        }
      }
    }, {
      id : 'btn_CancelStep2_id',
      text : 'Fermer',
      handler : function() {
        formPanelStep2.destroy();
        backUrl_global && (document.location.href = backUrl_global);
        !backUrl_global && window.close() 
      }
    }]
  });
}

function dropTable(fieldName_global, callback){
  Ext.Ajax.request({
    url: Routing.generate('carmen_tablestructure_services', {action: 'dropTable'}),
    method : 'POST',
    params : {
      metadata_id : _metadata_id,
      tableName : fieldName_global
    },
    success : callback,
    failure : function(response) {
      Ext.Msg.alert('Echec', "La suppression n'a pas réussi").setIcon(Ext.Msg.ERROR);
    }
  });
}

// -----------------------------------------------------------------------------------------------------------------------------

function fCreateFormPanelStep3(enum_name, enums_values) {

  var enum_store_step3 = Ext.create('Ext.data.Store', { fields : ['Value', 'DeleteAction'] });

  if ((typeof enums_values !== 'undefined' && enums_values && enums_values.constructor === Array)) {
    for (var i = 0; i < enums_values.length; i++) {
      enum_store_step3.add({
        'Value' : enums_values[i],
        'DeleteAction' : 'readOnly'
      });
    }
  }

  var columnWidth = 250;
  var formPanelEnum = Ext.create('Ext.form.Panel', {
    id : 'CreationFormPanelEnum_id',
    frame : true,
    //labelWidth : 150,
    width : 400,
    heigth : 300,
    bodyPadding : 10,
    fieldDefaults : {
      labelAlign : 'right'
    },
    items : [{
      id : 'txt_EnumName_id',
      xtype : (enum_name !== '' ? 'displayfield' : 'textfield'),
      name : 'EnumName_name',
      fieldLabel : 'Nom de la liste',
      value : enum_name
    }, {
      //gridPanel
      xtype : 'gridpanel',
      id : 'GridPanelEnum_id',
      columnLines : true,
      viewConfig : {stripeRows : true},
      border : true,
      //width: 550,
      height : 250,
      store : enum_store_step3,
      hideHeaders : true,
      tbar : [{
        xtype : 'displayfield',
        name : 'enum_store_step3_value',
        value : 'Valeur',
        width : columnWidth
      }, {
        xtype : 'button',
        text : 'Ajouter',
        handler : function(btn) {
          enum_store_step3.add({
            'Value' : '',
            'DeleteAction' : 'Supprimer'
          });
        }
      }],
      columns : [{
        id : 'Value_id',
        width : columnWidth,
        dataIndex : 'Value',
        xtype : 'widgetcolumn',
        onWidgetAttach : function(column, widget, record) {
          if (record.get('DeleteAction') == 'readOnly') {
            var readOnly = widget.down('[name=readOnly]');
            readOnly.setValue(record.get('Value'));
            readOnly.setVisible(true);
            Ext.each(widget.query(':not([name=readOnly])'), function(cmp) {
              cmp.setVisible(false);
            });
            return;
          }
          //
          // TODO
          // begin
          widget.items.each(function(cmp) {
            cmp.on('change', function() {
              record.set('Value', widget.getValue());
              record.commit();
            });
          }); // end
        },
        widget : {
          xtype : 'fieldcontainer',
          defaultBindProperty : 'value',
          value : null,
          setValue : function(value) {
            var activeField = this.down('[name=varchar]');
            if (activeField) {
              activeField.setValue(this.value);
              activeField.setVisible(true);
            }
            var inactiveFields = this.query(':not([name=varchar])');
            Ext.each(inactiveFields, function(inactiveField) {
              inactiveField.setVisible(false);
            });
          },
          getValue : function(value) {
            var activeField = this.down('[name=varchar]');
            if (activeField && activeField.isVisible()) {
              return this.value = activeField.getValue();
            }
            var readonlyField = this.down('[name=readonly]');
            if (readonlyField && readonlyField.isVisible()) {
              return this.value = readonlyField.getValue();
            }
            return this.value;
          },
          items : [{
            xtype : 'displayfield',
            name : 'readOnly',
            width : columnWidth-20,
            hidden : true
          }, {
            xtype : 'textfield',
            name : 'varchar',
            width : columnWidth-20,
            minLength : 3,
            maxLength : 100,
            value : 'Valeur',
            vtype : 'alphanum',
            hidden : true
          }]
        }
      }, {
        id : 'DeleteAction_id',
        dataIndex : 'DeleteAction',
        xtype : 'widgetcolumn',
        onWidgetAttach : function(column, widget, record) {
          if (record.get('DeleteAction') == 'readOnly') {
            widget.setVisible(false);
          } else {
            widget.setVisible(true);
          }
          return;
        },
        widget : {
          xtype : 'button',
          text : 'Supprimer',
          handler : function(btn) {
            Ext.Msg.confirm(
            "Suppression d'une énumération", 
            "Confirmez-vous la suppression de l'énumération "+enum_name+" ?" +
            "<br><br>(Note : Cette action échouera si l'énumération est utilisée pour un champ)", 
            function(btn){btn=="yes" && enum_store_step3.removeAt(enum_store_step3.data.length - 1, 1)});
          }
        }
      }]
    }],
    buttons : [{
      id : 'btn_SaveEnum_id',
      text : 'Enregistrer',
      handler : function() {
        var enumValuesToAdd = [];
        for (var i = 0; i < enum_store_step3.data.length; i++) {
          if (enum_store_step3.getAt(i).data['DeleteAction'] != 'readOnly') {
            enumValuesToAdd.push(enum_store_step3.getAt(i).data['Value']);
          }
        }
        Ext.Ajax.request({
          url: Routing.generate('carmen_tablestructure_services', {action: (enum_name_global !== '' ? 'alterEnum' : 'createEnum')}),
          method : 'POST',
          params : {
            enumValuesToAdd : JSON.stringify(enumValuesToAdd),
            enum : Ext.getCmp('txt_EnumName_id').getValue()
          },
          success : function(response) {
            var response = Ext.decode(response.responseText);
            //console.log('response is : ', response);
            if (response.createEnum || response.alterEnum) {
              enum_name_global = '';
              fRefreshEnumStore(enum_store_step2);
              popupEnum.close();
            } else {
              Ext.Msg.alert('Echec', 'Echec ...').setIcon(Ext.Msg.ERROR);
            }
            
          },
          failure : function(response) {
            console.log('Failure - create/alter enumeration - response is : ', response.responseText);
          }
        });
      }
    }, {
      id : 'btn_DeleteEnum_id',
      text : 'Supprimer',
      handler : function() {
        var enum_name =  Ext.getCmp('txt_EnumName_id').getValue();
        Ext.Msg.confirm(
        "Suppression d'une énumération", 
        "Confirmez-vous la suppression de l'énumération "+enum_name+" ?" +
        "<br><br>(Note : Cette action échouera si l'énumération est utilisée pour un champ)", 
        function(btn){
          if ( btn!="yes" ) return;
          
          Ext.Ajax.request({
            url: Routing.generate('carmen_tablestructure_services', {action: 'deleteEnum'}),
            method : 'POST',
            params : {
              enum : enum_name
            },
            success : function(response) {
              var response = Ext.decode(response.responseText);
              //console.log('response is : ', response);
              if (response.deleteEnum) {
                enum_name_global = '';
                fRefreshEnumStore(enum_store_step2);
                enum_values_store.removeAll();
                enum_store_step3.removeAll();
                popupEnum.close();
              } else {
                Ext.Msg.alert('Echec', "Echec dans la suppression de l'énumération").setIcon(Ext.Msg.ERROR);
              }
            },
            failure : function(response) {
              console.log('Failure - delete enumeration - response is : ', response.responseText);
            }
          });
        });
          
      }
    }, {
      id : 'btn_Close_id',
      text : 'Fermer',
      width : 85,
      handler : function() {
        //fRefreshComboStore(combo_store);
        enum_name_global = '';
        enum_store_step3.removeAll();
        popupEnum.close();
      }
    }]
  });

  var popupEnum = Ext.create('Ext.window.Window', {
    title : 'Gestion de la liste de valeurs',
    width : formPanelEnum.width,
    height : formPanelEnum.height,
    items : formPanelEnum,
    listeners : {
      close : function(){
        enum_values_store.reload();
      }
    }
  });
  popupEnum.show();
}
//-----------------------------------------------------------------------------------------------------------------------------