<?php
/**
 * Ecrit dans le log, la statistique d'envoi de mail placé dans la queue
 */


/**
 * Write statistique in csv file
 */
function writeCSVFile($filename, $row) {
    $exists = ( file_exists($filename) && is_file($filename) ? true : false );
    $file = fopen($filename, 'a+');
    if( !$file ) {
        return;
    }
    if( !$exists ) {
        fwrite($file, '"Date/Heure","Adresse IP","Nom","Prénom","Login","Donnée concernée","Nombre de destinataires du message"'."\n");
    }
    $line = "";
    $sep = "";
    foreach($row as $value) {
        $line .= $sep.'"'.str_replace('"', '', $value).'"';
        $sep = ',';
    }
    fwrite($file, $line."\n");
    fclose($file);
}
    
/**
 * adds a log when a mail is sent
 * @param string $adrIP      ip address
 * @param string $name       name of user who sends the mail
 * @param string $given      given name of user who sends the mail
 * @param string $login      login of user who sends the mail
 * @param string $layerName  name of layer concerned by the mail
 * @param int    $nbTo       number of recipient
 */
function addLog($adrIP, $name, $given, $login, $layerName, $nbTo) {
    $dir = __DIR__;
    $i = 0;
    $bOk = false;
    while ( $dir && is_dir($dir) &&  $dir!="/" && $i++<50 && !($bOk=( is_dir($dir."/../app") )) ){
        $dir = realpath($dir."/..");
    }
    if ( !$bOk ) return;
    $logFilename = $dir."/../app/logs/statistiques.csv";
    $row = array(date("d/m/Y H:i:s"), $adrIP, $name, $given, $login, $layerName, $nbTo);
    writeCSVFile($logFilename, $row);
}

/**
 * Return in array, list of params with (key=>value)
 * @param array $argv
 * @return array
 */
function getArguments($args) {
    $res = array();
    foreach($args as $arg) {
      if( preg_match('/--([^=]+)=(.*)/', $arg, $matches) ) {
        $res[$matches[1]] = $matches[2];
      } 
    }
  return $res;
}

$tabArgs = getArguments($argv);
$tabParams = array();
$tabKeys = array('ip', 'userName', 'userGiven', 'userLogin', 'tableName', 'nbTo');
foreach($tabKeys as $key) {
    if( array_key_exists($key, $tabArgs) ) {
        $tabParams[] = $tabArgs[$key];
    }
}
if( count($tabParams) == count($tabKeys) ) {
    // ajout du log
    call_user_func_array("addLog", $tabParams);
} else {
    addLog("localhost", "Serveur", "Prodige", "Erreur", "Impossible d'ajouter la statistique suite à un envoi de mailing différé.", "0");
}

// sortie avec succès
exit(0);
