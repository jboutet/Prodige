#!/bin/bash
SCRIPTDIR=`dirname $(readlink -f $0)`
[ -d "$SCRIPTDIR" ] || { echo "Fin du script : $SCRIPTDIR n'existe pas"; exit; }
OWNER="ftpuser"
id $OWNER > /dev/null 2>&1
[ $? -ne 0 ] && OWNER="www-data"
APACHE="www-data"

rights()
{
    if [ -d "${1}" ]
    then
        echo "Chowning ${1}"
        find "${1}" -print0 | xargs -r -0 chown ${2}.${3}
        if [ "${4}" == "ro" ]; then
            echo "Chmoding ${1} directories ${4}"
            find "${1}" -type d -print0 | xargs -r -0 chmod 750
            echo "Chmoding ${1} files ${4}"
            find "${1}" -type f -print0 | xargs -r -0 chmod 640
        fi
        if [ "${4}" == "rw" ]; then
            echo "Chmoding ${1} directories ${4}"
            find "${1}" -type d -print0 | xargs -r -0 chmod 770
            echo "Chmoding ${1} files ${4}"
            find "${1}" -type f -print0 | xargs -r -0 chmod 660
        fi
    fi
}

chown ${OWNER}.${APACHE} "${SCRIPTDIR}/sites"
chmod 750 "${SCRIPTDIR}/sites"

for vhost in prodigecatalogue prodigefrontcarto prodigeadmincarto/carmenwsback prodigeadmincarto/carmenwsmapserv prodigedatacarto prodigetelecarto prodigeadminsite; do
        rights "${SCRIPTDIR}/sites/$vhost" "${OWNER}" "${APACHE}" "ro"
        rights "${SCRIPTDIR}/sites/$vhost/app/cache" "${OWNER}" "${APACHE}" "rw"
        rights "${SCRIPTDIR}/sites/$vhost/app/logs" "${OWNER}" "${APACHE}" "rw"
        rights "${SCRIPTDIR}/sites/$vhost/web/upload" "${OWNER}" "${APACHE}" "rw"
done

rights "${SCRIPTDIR}/sites/prodigecatalogue/web/thumbnails"  "${OWNER}" "${APACHE}" "rw"
rights "${SCRIPTDIR}/sites/prodigetelecarto/web/DOWNLOAD"  "${OWNER}" "${APACHE}" "rw"

#error.log
for vhost in prodigedatacarto prodigecatalogue prodigefrontcarto prodigeadminsite prodigetelecarto; do
   chmod -R 770 "${SCRIPTDIR}/sites/$vhost/app"
done

#rights "${SCRIPTDIR}/sites/prodigeadminsite" "${OWNER}" "${APACHE}" "ro"
#chmod    770 /home/sites/prodigeadminsite/services/epsg
rights "${SCRIPTDIR}/sites/cas" "${OWNER}" "${APACHE}" "ro"
rights "${SCRIPTDIR}/sites/vendor" "${OWNER}" "${APACHE}" "ro"

rights "${SCRIPTDIR}/sites/prodigegeosource" "${OWNER}" "${APACHE}" "ro"
[ -d /home/sites/prodigegeosource/xml/schemas ] && chmod -R 770 /home/sites/prodigegeosource/xml/schemas
chmod    770 /home/sites/prodigegeosource/WEB-INF/*
rights "${SCRIPTDIR}/sites/prodigegeosource/WEB-INF/data" "${OWNER}" "${APACHE}" "rw"

chown ${OWNER}.${APACHE} "${SCRIPTDIR}/prodige"
rights "${SCRIPTDIR}/prodige" "${OWNER}" "${APACHE}" "rw"

#specifique
for respire in /home/sites/respire; do
  if [ -d $respire ]; then  
    rights "$respire" "${OWNER}" "${APACHE}" "ro"
    rights "$respire/upload" "${OWNER}" "${APACHE}" "rw"
    rights "$respire/alkanet/upload" "${OWNER}" "${APACHE}" "rw"
    [ -f $respire/alkanet/scripts/respire/task/sendMailFromQueue.sh ] && chmod  600 $respire/alkanet/scripts/respire/task/sendMailFromQueue.sh
    find $respire/alkanet/services/xhtml2odt/ -type f -name "xhtml2odt.*" -print0 | xargs -r -0 chmod 750
    chmod  750 $respire/alkanet/scripts/alkanet/cgi-bin/alkanet_upload.cgi
  fi
done


chown www-data: /home/tasks/*.sh
chmod 770 /home/tasks/*.sh
chown -R www-data: /home/tasks/log
chmod -R 770 /home/tasks/log
chown root: /home/tasks/clean.sh
chmod 700 /home/tasks/clean.sh

chmod 750 "${SCRIPTDIR}/rights.sh"
