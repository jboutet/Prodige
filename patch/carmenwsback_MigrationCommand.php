<?php
namespace Carmen\ApiBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Carmen\ApiBundle\Services\Helpers;
use Carmen\OwsContextBundle\Controller\OwsContextController;
use Carmen\OwsContextBundle\Controller\MapController;
use Carmen\ApiBundle\Entity\LexAnalyseType;

use Prodige\ProdigeBundle\Controller\BaseController;
use Carmen\ApiBundle\Entity\Map;

class MigrationCommand extends ContainerAwareCommand
{
    const ACCOUNT_DIRECTORY = "cartes";
    const ACCOUNT_ID = 1;
    const ACCOUNT_DNS_ID = 1;
    /**
     * List of patterns that correspond to metadata kept
     * @var array
     */
    protected $metadataPatterns = array(
        "mapObj" => array(
            "^wms_",
            "^wms_",
            "geoide_wfs_srs",
            "geoide_wms_srs",
            "^wfs_",
            "PS_g3_SECURITE_PWD",
        ),
        "layerObj" => array(
            "^wms_",
            "^wfs_",
            "geoide_wfs_srs",
            "geoide_wms_srs",
        		"^gml_include_items",
        		"^ows_include_items",
            "PS_g3_SECURITE_PWD",
        ),
    );
    
    /**
     * recursive search with pattern
     * @param unknown $pattern
     */
    protected function glob_recursive($pattern, $flags = 0)
    {
        $files = glob($pattern, $flags);
       
        foreach (glob(dirname($pattern).'/*', GLOB_ONLYDIR|GLOB_NOSORT) as $dir)
        {
            $files = array_merge($files, $this->glob_recursive($dir.'/'.basename($pattern), $flags));
        }
       
        return $files;
    }
    
    /**
     * Configures the command definition
     */
    protected function configure()
    {
        $this
            ->setName('carmen:migration')
            ->setDescription('Migration of directories from version 3.4 to 4.0')
            /*->addArgument(
                'src_directory',
                InputArgument::REQUIRED,
                'directory to migrate')*/
            /*)->addArgument(
                'account_name',
                InputArgument::REQUIRED,
                'account name'
            )->addArgument(
                'account_id',
                InputArgument::REQUIRED,
                'account id'
            )->addArgument(
                'user_email',
                InputArgument::REQUIRED,
                'user email (cas connection)'
            )->addArgument(
                'account_dns_id',
                InputArgument::REQUIRED,
                'dns identifier (must be present in dns table)'
            )->addArgument(
                'account_geonetworkurl',
                InputArgument::OPTIONAL,
                'geonetwork URL for account'*/
            ->addArgument(
                'account_userpostgres',
                InputArgument::OPTIONAL,
                'user postgres for account database'
            )->addArgument(
                'account_passwordpostgres',
                InputArgument::OPTIONAL,
                'password postgres for account database'
            )->addArgument(
                'account_hostpostgres',
                InputArgument::OPTIONAL,
                'host postgres for account database'
            )->addArgument(
                'account_portpostgres',
                InputArgument::OPTIONAL,
                'port postgres for account database'
            )->addArgument(
                'account_dbpostgres',
                InputArgument::OPTIONAL,
                'database postgres for account'
            )->addArgument(
                'migrate_action',
                InputArgument::OPTIONAL,
                'Specific migrate action'
            );
    }

    /**
     * Executes the command
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->setDecorated(true);
        
        //collect id/uuid correspondance
        $catalog_conn = $this->getConnection(BaseController::CONNECTION_CATALOGUE);
        $conn = $this->getConnection(BaseController::CONNECTION_PRODIGE);
        $em   = $this->getManager();
        
        
        $migrate_action = $input->getArgument('migrate_action');
        if ( $migrate_action && method_exists($this, $migrate_action) ){
            $args = null;
            switch(strtolower($migrate_action)){
                case "migratewxs" : 
                    $args = array(
                        $this->getAccountDirectory(self::ACCOUNT_DIRECTORY)."/Publication", 
                        $em->getRepository("CarmenApiBundle:Account")->find(self::ACCOUNT_ID), 
                        $output
                    ); break; 
            }
            if ( is_array($args) ){
                call_user_func_array(array($this, $migrate_action), $args);
                $output->writeln("<info>Migrate Action {$migrate_action} is done !</info>");
                return;
            }
            $output->writeln("<error>ERROR : Unavailable migrate Action {$migrate_action}</error>");
            return;
        }
        
        $sql = "SELECT id, uuid from public.metadata where isharvested='n'";
        $tabMeta = $catalog_conn->fetchAll ( $sql );
        $GLOBALS["tabMetadata"] = array();
        foreach($tabMeta as $key => $tabInfo){
            $GLOBALS["tabMetadata"][$tabInfo["id"]] = $tabInfo["uuid"];
        }
        
        $conn = $this->getConnection(BaseController::CONNECTION_PRODIGE);
        
        $em   = $this->getManager();
        //Create a constant for each analyse type to link to their ID
        $analyses = $em->getRepository("CarmenApiBundle:LexAnalyseType")->findAll();
        foreach($analyses as $analyse){
        		$analyse instanceof LexAnalyseType;
            $code_field = $analyse->getAnalyseTypeCode();
            $id_field = $analyse->getAnalyseTypeId();
            if ( !defined("BD_ANALYSETYPE_".$code_field) ){
                define("BD_ANALYSETYPE_".$code_field, $id_field);
            }
        }
        //Create a constant for each tool to link to his ID
        $tools = $em->getRepository("CarmenApiBundle:LexTool")->findAll();
        foreach($tools as $tool){
        		$tool instanceof LexTool;
            $code_field = strtoupper($tool->getToolIdentifier());
            $id_field = $tool->getToolId();
            if ( !defined("BD_TOOL_".$code_field) ){
                define("BD_TOOL_".$code_field, $id_field);
            }
        }
        
        //$directory = $input->getArgument("src_directory");///home/devperso/bfontaine/prodige4/cartes";
        //PRODIGE Parameters
        $account_name = self::ACCOUNT_DIRECTORY;
        $account_id = self::ACCOUNT_ID;
        //TODO insert into lexDNS config URL plateforme before command
        $account_dns_id = self::ACCOUNT_DNS_ID;
        //TODO REQUETE SQL SELECT populate_geometry_columns();  
        //TODO insert into account SGBD params
        /*if(!file_exists($directory)){
            $output->writeln("<error>ERROR : no source directory or source directory does not exist</error>");
            return;
        }*/
        
                    
        
        if ($account_name!="" && $account_name!="") {
            
            $accountDns = $em->getRepository('CarmenApiBundle:LexDns')->find($account_dns_id);
            if ($accountDns){
                $this->migrateDir( $accountDns, $input, $output);
            }else{
                $output->writeln("<error>ERROR : account dns must be identifier from lex_dns table. Insert data in lex_dns table first</error>");
            }
            
        } else {
            $output->writeln("<error>ERROR : account name and id can't be empty</error>");
        }

        
    }
    
    protected function getAccountDirectory($account_path){
       $config =  $this->getContainer()->getParameter('carmen_config', array());
       return $config['mapserver']['root_data'] . "/" . $account_path;
    }
    
   
    /**
     * 
     * @param unknown $directory
     */
    protected function migrateDir( $accountDns, $input, OutputInterface $output)
    {
        //create generic account
        $account_name = self::ACCOUNT_DIRECTORY;
        $account_id = self::ACCOUNT_ID;
        $userGenericEmail = "prodige";
        /*$account_name = $input->getArgument('account_name');
        $account_id = $input->getArgument('account_id');*/
        $account_userpostgres = $input->getArgument('account_userpostgres');
        $account_passwordpostgres = $input->getArgument('account_passwordpostgres');
        $account_hostpostgres = $input->getArgument('account_hostpostgres');
        $account_portpostgres = $input->getArgument('account_portpostgres');
        $account_dbpostgres = $input->getArgument('account_dbpostgres');
        //$account_geonetworkurl = $input->getArgument('account_geonetworkurl');
        
        
        $config =  $this->getContainer()->getParameter('carmen_config', array());
        
        if(file_exists($config['mapserver']['root_data'])){
            //copy rep to data directory
            //$output->writeln('<info>copying data</info>');
            //$account_directory = $directory;
            $account_path = $account_name;//basename($directory);
            //exec ("cp -R ".$directory." ".$config['mapserver']['root_data']);
            $account_directory = $this->getAccountDirectory($account_path);
            
            
            
            if (file_exists($account_directory)){
                //script to migrate from Mapserver 5.X to mapserver 7.0
            		$output->writeln('<info>remove TMP_ files </info>');
            		$mask = $account_directory."/Publication/TMP_*.map";
            		array_map( "unlink", glob( $mask ) );
            		
            		
                exec("bash ".realpath(dirname(__FILE__))."/migrate.sh ".$account_directory);
                //create acount in database
                $account = new \Carmen\ApiBundle\Entity\Account();
                $account->setAccountName($account_name)
                ->setAccountId($account_id)
                ->setAccountPath($account_path)
                ->setAccountDbpostgres($account_dbpostgres)
                ->setAccountUserpostgres($account_userpostgres)
                ->setAccountPortpostgres($account_portpostgres)
                ->setAccountHostpostgres($account_hostpostgres)
                ->setAccountPasswordpostgres($account_passwordpostgres)
                ->setAccountDns($accountDns)
                ->setAccountGeonetworkurl("");
                
                $conn = $this->getConnection();
                $em   = $this->getManager();
                $em->getConnection()->getConfiguration()->setSQLLogger(null);
                gc_collect_cycles();
        
                $em->persist($account);
                $em->flush();
                
                if( $account ) {
                    
                    $output->writeln('<info>account (id='.$account->getAccountId().') was successfuly created !</info>');
                    $output->writeln('');
                    
                    //create automatic user
                    //TODO gérer autrement identification ? VLC
                    $user = new \Carmen\ApiBundle\Entity\Users();
                    $user->setAccount($account)
                         ->setUserEmail($userGenericEmail);
                    $em->persist($user);
                    $em->flush();
                    //migrate conteact info

                    //treat Publication directory
                    $dirPublication = $account_directory."/Publication";
                    //NOT IN PRODIGE
                    //$this->migrateContact($dirPublication, $user, $output);
                    
                    
                    $this->migrateWXS($dirPublication, $account, $output);
                    
                    //open model files and save it
                    $filepath_Modeles = $dirPublication."/Modeles_Administration.xml";
                    $tabModels = array();
                    if ( file_exists($filepath_Modeles)){
                        $docDOM_WMS = new \DOMDocument();
                        $docDOM_WMS->load($filepath_Modeles);
                         
                        $elements = $docDOM_WMS->getElementsByTagName("MODELE");
                    
                        foreach ($elements as $element ){
                            $modelId = $element->getAttribute("ID");
                            $modelName = $element->getAttribute("NAME");
                            $tabModels[$modelId] = $modelName;
                        }
                    }
                    
                    if (file_exists($dirPublication)) {
                        $MAPFILES = $this->glob_recursive($dirPublication."/*.map");
                        //array_diff(glob($dirPublication."/*.map"), glob($dirPublication."/TMP_*.map"), glob($dirPublication."/tmp_*.map"));
                        foreach($MAPFILES as $mapfile) {
                        	  //$Entry = basename($mapfile);
                            // convert mapfiles and do not treat wms_*.map as mapObj
                            if ( basename($mapfile)!="wms.map" && basename($mapfile)!="wfs.map"){
                                $this->migrateMap($dirPublication, $mapfile, $account, $output, $tabModels);
                            }
                            //}
                            //else {
                                /*$file = $dirPublication.'/'.$Entry;
                                try {            
                                    $output->writeln('');
                                    $output->writeln("treating WMS-Mapfile ".$file);
                                	
                                    ms_ResetErrorList();
                                    $mapObj = ms_newMapObj($file);
                                    $mapObj = $this->addAditionnalInfo($mapObj);
                                    
                                    $output->write('<info>Remove included symbols  : </info>');
                                    for($i=0; $i<$mapObj->getNumSymbols(); $i++){
                                        $oSymbol = $mapObj->getSymbolObjectById($i);
                                        $oSymbol->set("inmapfile", MS_FALSE);
                                    }
                                    if ( !$mapObj->symbolsetfilename ){
                                        if ( file_exists($dirPublication . "/". str_replace("wms_", "", $Entry))){
                                            $realMap = ms_newMapObj($dirPublication . "/". str_replace("wms_", "", $Entry));
                                            if($realMap->symbolsetfilename){
                                                $mapObj->setSymbolset($realMap->symbolsetfilename);
                                            }
                                        }elseif ( file_exists($dirPublication . "/" .($symbol = "./etc/symbolsWMS.sym") ))
                                            $mapObj->setSymbolset($symbol);
                                        elseif ( file_exists($dirPublication . "/" .($symbol = "./etc/symbols.sym") ))
                                            $mapObj->setSymbolset($symbol);
                                    }
                                    $output->writeln('<info>Done !</info>');
                                    
                                    $output->write('<info>Remove unneeded metadata : </info>');
                                    $key = null;
                                    $removed = array();
                                    while ( $key = $mapObj->metadata->nextkey($key) ){
                                        if ( !preg_match("!". implode("|", $this->metadataPatterns["mapObj"]) ."!", $key) ){
                                            $removed[] = $key;
                                        }
                                    }
                                    foreach($removed as $indice => $value)
                                    	$mapObj->removeMetadata($value);
                                    
                                    /*if ( !empty($removed) )
                                     $output->writeln("  From mapObj  = [".implode(", ", $removed)."]");*/
                                    /*
                                    for($i = 0; $i<$mapObj->numlayers; $i++) {
                                        $oLayer = $mapObj->getLayer($i);
                                        $key = null;
                                        $removed = array();
                                        while ( $key = $oLayer->metadata->nextkey($key) ){
                                            if ( !preg_match("!". implode("|", $this->metadataPatterns["layerObj"]) ."!", $key) ){
                                                $removed[] = $key;
                                            }
                                        }
                                        foreach($removed as $indice => $value)
                                            $oLayer->removeMetadata($value);
                                         
                                        /*if ( !empty($removed) )
                                         $output->writeln("  From layerObj ".$oLayer->name." = [".implode(", ", $removed)."]");*/
                                    	/* 
                                    }
                                    
                                    $output->writeln('<info>Done !</info>');
                                    $mapObj->save($file);
                                    
                                    ms_ResetErrorList();
                                    $mapObj = ms_newMapObj($file);
                                    $mapObj->save($file);
                                    
                                    
                                } catch (\Exception $ex) { 
                                    $output->writeln('<error>'.$file.' not treated, corrupted for Mapserver 7.0 !</error>');
                                    $strError = $ex->getMessage();
                                    $error = ms_GetErrorObj();
                                    while($error && $error->code != MS_NOERR)
                                    {
                                        $strError .= sprintf("\n  Error in %s: %s", $error->routine, $error->message);
                                        $error = $error->next();
                                    }
                                    $output->writeln('  <comment>'.$strError."\n".'</comment>');
                                    continue;
                                }*/
                            //}
                        }
                    } else {
                        $output->writeln("<error> ".$dirPublication." directory does not exist</error>");
                    }
                }else{
                   $output->writeln("<error> Account has not been created </error>");
                }
                exec ("cp ".realpath(dirname(__FILE__))."/../Resources/__TEMPLATES__/CARMEN/{account_name}/Publication/template.map " .$account_directory. "/Publication");
                //Rebuild XML file
                $this->getContainer()->get('carmen.helpers')->buildServicesXML();

                $this->removeXMLFiles($account_directory."/Publication", $output);
            }else{
                $output->writeln("<error> Problem while copying directory </error>");
            }
        } else{
            $output->writeln("<error> Wrong parameter destination directory does not exist </error>");
        }  
    }
    
    /**
     * add info not in Carmen v2
     * @param unknown $oMap
     */
    protected function addAditionnalInfo(&$oMap, $dirMapfile, $dirPublication){
            
        //if not etc dir exist, create symlink
        if(!file_exists($dirMapfile."/etc")){
            symlink($dirPublication."/etc", $dirMapfile."/etc");
        }
        if(strpos($oMap->symbolsetfilename, "../")!==false){
            $oMap->setSymbolSet("./etc/".basename($oMap->symbolsetfilename));
        }
        if(strpos($oMap->fontsetfilename, "../")!==false){
            $oMap->setFontSet("./etc/".basename($oMap->fontsetfilename));
        }
        
        //add VALIDATION PATTERN for CGI request 
        $strValidation = 'WEB
                          VALIDATION
                            "queryfile" "[a-z]+"
                          END
                          END';

        $oMap->web->updateFromString($strValidation);

        $projection = $oMap->getProjection();
        for($j = 0; $j<$oMap->numlayers; $j++) {
            $oLayerObj = $oMap->getLayer($j);

            //assign map projection if layer projection is not set
            if($oLayerObj->getProjection()==""){
                $oLayerObj->setProjection($projection);
            }
            
            $oLayerObj->set('toleranceunits', MS_PIXELS);
            //$oLayer->set('template', 'GML');
        
            if ($oLayerObj->type == MS_LAYER_POLYGON){
                $oLayerObj->set('tolerance', 0);
            }else{
                $oLayerObj->set('tolerance', 5);
            }
			//if class status is off (not visible), remove name and title (not in legend)
            //complex algorithme to update fromstring all classes of LAYER since updateFromstring don't update object totaly but add in object
            for($i = 0; $i<$oMap->numlayers; $i++) {
	            $oLayer = $oMap->getLayer($i);
	            $iNumClasses = $oLayer->numclasses;
	            for ($iClass=0; $iClass<$iNumClasses; $iClass++ ){
	                $oClass = $oLayer->getClass(0);
					//update styles first
                    if($oLayer->type == MS_LAYER_POLYGON){
                        for($style=0; $style<$oClass->numstyles; $style++){
                            $oStyle = $oClass->getStyle($style);
                            if ($oStyle->symbolname == "Carre"){
                                //put size to 1, otherwize grid in image returned
                                $oStyle->set("size", 1);
                            }
                        }
                    }
                    $tabFinalClass = array();
                    $strClass =  $oClass->convertToString();
                    $tabFinalClass = $tabClassLines = explode("\n", $strClass);
                    if($oClass->status ==MS_OFF){
                        $tabFinalClass = array();
                        foreach($tabClassLines as $key => $value){
                            //remove CLASS NAME AND TITLE, mapscript can't do it 
                            if(strpos($value, "NAME")==false && strpos($value, "TITLE")==false){
                                array_push($tabFinalClass, $value);
                            }
                        }
                    }
					          $encoding = $oLayer->getMetadata("encoding");
                    if($encoding!=""){
                        array_push($tabFinalClass, "ENCODING ".$encoding);
                    }
                    $strFinalClass =  "LAYER ".implode("\n", $tabFinalClass)." END";
                    $oLayer->removeClass(0);
                    $oLayer->updateFromString($strFinalClass);
                }
            }
        }
    }
    /**
     * remove unused files in CarmenV3
     * @param unknown $dirAdherent
     */
		protected function removeXMLFiles($dirAdherent, OutputInterface $output)
    {
        /*$files = glob($dirAdherent."/*.xml");
        //realpath of excluded files
        $keep  =  array_map('realpath', array($dirAdherent."/ServeursWFS_Consultation.xml",
                                      $dirAdherent."/ServeursWMS_Consultation.xml",
                                      //specific PRODIGE exclude Projections for the moment
                                      $dirAdherent."/Projections_Administration.xml",
                                      $dirAdherent."/Modeles_Administration.xml"));
        $files = array_diff($files, $keep);*/
        $files = array($dirAdherent."/ServeursWMS_Consultation.xml",
                       $dirAdherent."/ServeursWMSC_Administration.xml",
                       $dirAdherent."/Controles.xml",
                       $dirAdherent."/type_modele.xml",
                       $dirAdherent."/ServeursWFS_Administration.xml",
                       $dirAdherent."/ServeursWMS_Administration.xml",
                       $dirAdherent."/ServeursWMTS_Administration.xml");
        
        $deleted = array();
        foreach($files as $file){
            if ( unlink($file)!==false) $deleted[] = $file;
        }
        $output->writeln('<info>Remove unneeded XML files : '.implode(', ', array_map('basename', $deleted)).'</info>');
    }
    
    /**
     * 
     * @param unknown $file
     */
    protected function migrateMap($dirPublication, $file, $account, OutputInterface $output, $tabModels)
    {
        $config =  $this->getContainer()->getParameter('carmen_config', array());
        try {            
            ms_ResetErrorList();
            $oMap = ms_newMapObj($file);
			
			//migrate map
            $oMap->setMetadata("wms_enable_request", "*");
            
        } catch (\Exception $ex) { 
              $output->writeln('<error>'.$file.' not treated, corrupted for Mapserver 7.0 !</error>');
               $strError = $ex->getMessage();
               $error = ms_GetErrorObj();
              while($error && $error->code != MS_NOERR)
              {
                $strError .= sprintf("\n  Error in %s: %s", $error->routine, $error->message);
                $error = $error->next();
              }
              $output->writeln('  <comment>'.$strError."\n".'</comment>');
              return;
        }
        gc_enable();
        $em   = $this->getManager();
        $em->clear();
        $conn = $this->getConnection();
        
        $em->getConnection()->getConfiguration()->setSQLLogger(null);
        gc_collect_cycles();
        //create MAP in database 
        $map_file = str_replace($dirPublication."/", "",  $file);
        //without extension .map
        $map_file = substr($map_file, 0, -4);
        
        $output->writeln('');
        $output->writeln('treating '.$file);
        $map_title = $oMap->name;
        
        //get map_summary info from wms generated file
        $map_summary = "";
        //NOT USED IN PRODIGE
        /*if(file_exists($wmsfile)){
            try {            
               $oMapWms = ms_newMapObj($wmsfile);
               //migrate map
               $map_summary = $oMapWms->getMetadata("wms_abstract");
            } catch (\Exception $ex) { 
               $output->writeln('<error>wms file : '.$wmsfile.' not treated, corrupted for Mapserver 7.0 !</error>');
            }
                 
        }*/
        $map_catalogable = FALSE;//($oMap->getMetadata("CATALOGABLE")=="ON");
        $map_password = "";//$oMap->getMetadata("PS_g3_SECURITE_PWD");
        $map_model = FALSE;
        
        
        if(in_array($map_file.".map", array_keys($tabModels))){
	        
//          if($oMap->numlayers>0){
	        	$map_model = TRUE;
	        	//rename file
	        	@unlink($file);
	        	$file = $dirPublication."/modele_".$map_file.".map";
	        	
	/*        }else{
	        	//remove file and stop migration for map
	        	@unlink($file);
	        	return;	        	
	        }*/
        }
        
//         $map_bgcolor = $oMap->imagecolor->toHex();
//         $map_bgcolor = str_replace('#', '', $map_bgcolor);
//         $map_bgcolor = substr($map_bgcolor, 0, 6);
        
        $map_extent_xmin = $oMap->extent->minx;
        $map_extent_ymin = $oMap->extent->miny;
        $map_extent_xmax = $oMap->extent->maxx;
        $map_extent_ymax = $oMap->extent->maxy;
        $projection="";
        if($oMap->getProjection()){
            $map_projection_epsg = substr($oMap->getProjection(),11); // taking only the ID after the 11-char string "init=epsg:"
            
            if(is_numeric($map_projection_epsg)){
            	$projection = $em->getRepository('CarmenApiBundle:LexProjection')->find($map_projection_epsg);
            }
            //force map to accept wms request in map projection
            $oMap->setMetadata("wms_srs", "EPSG:".$map_projection_epsg);
        }                
        $map_minscale = intval($oMap->web->minscaledenom);
        $map_maxscale = intval($oMap->web->maxscaledenom);
        

        //NOT USED IN PRODIGE
        $map_wfsmetadata_uuid = "";//$oMap->getMetadata("metadata_wfs_uuid");
        $map_atommetadata_uuid = "";//$oMap->getMetadata("metadata_atom_uuid");
        
        //Use of this column to collect map uuid 
        $map_wmsmetadata_uuid = "";//$oMap->getMetadata("metadata_wms_uuid");
        $catalog_conn = $this->getCatalogueConnection("catalogue,public");
        
        //layers
        if(strpos($map_file, "layers/")!==false){
           $sql = "SELECT metadata.uuid from metadata  inner join fiche_metadonnees ". 
                  " ON metadata.id = fiche_metadonnees.fmeta_id::bigint ".
                  " inner join couche_donnees ON couche_donnees.pk_couche_donnees = fiche_metadonnees.fmeta_fk_couche_donnees ".
                  " where couchd_emplacement_stockage =:stkcard_path";
           $stkcard_path = basename($map_file);
           $tabUUID = $catalog_conn->fetchAll ( $sql , array ("stkcard_path" => $stkcard_path));
           
           if(count($tabUUID)==0){
               //try it with like for raster case
               $sql = "SELECT metadata.uuid from metadata  inner join fiche_metadonnees ".
                   " ON metadata.id = fiche_metadonnees.fmeta_id::bigint ".
                   " inner join couche_donnees ON couche_donnees.pk_couche_donnees = fiche_metadonnees.fmeta_fk_couche_donnees ".
                   " where couchd_emplacement_stockage like :stkcard_path";
               $stkcard_path = basename($map_file);
               $tabUUID = $catalog_conn->fetchAll ( $sql , array ("stkcard_path" => "%".$stkcard_path."%"));
               //ambiguous data, throw warning
               if(count($tabUUID)>1){
                   $output->writeln('<info>map (mapfile='.$map_file.') ambiguous recognition in catalog, verify uuid in database !</info>');
               }
           }
           
        }else{
        //map
          $sql = "select uuid from stockage_carte ".
                " inner join carte_projet on carte_projet.cartp_fk_stockage_carte = stockage_carte.pk_stockage_carte  ".
                " inner join fiche_metadonnees ON carte_projet.cartp_fk_fiche_metadonnees = fiche_metadonnees.pk_fiche_metadonnees ".
                " inner join metadata ON metadata.id = fiche_metadonnees.fmeta_id::bigint ".
                " where stkcard_path= :stkcard_path";
          $stkcard_path = $map_file.".map";
          $tabUUID = $catalog_conn->fetchAll ( $sql , array ("stkcard_path" => $stkcard_path));
        }
        
        foreach ($tabUUID as $val)
        {
           $map_wmsmetadata_uuid  = $val['uuid'];
        }
        if($map_wmsmetadata_uuid==""){
          $map_wmsmetadata_uuid = uniqid();
          $output->writeln('<info>map (mapfile='.$map_file.') not recognized in catalog, generate uuid !</info>');
        }
        $mapRefmapExtentXmin = $oMap->reference->extent->minx;
        $mapRefmapExtentXmax = $oMap->reference->extent->maxx;
        $mapRefmapExtentYmin = $oMap->reference->extent->miny;
        $mapRefmapExtentYmax = $oMap->reference->extent->maxy;
        $mapRefmapImage=$oMap->reference->image;
                 
        
        //echo "memory usage". memory_get_usage() . "\n";
        
        // example : create a new map, set some values, persist to database and then remove it !
        $databaseMap = new \Carmen\ApiBundle\Entity\Map();
        $databaseMap->setMapFile($map_file)
        ->setMapTitle($map_title)
        ->setMapSummary($map_summary)
        ->setMapCatalogable($map_catalogable)
        ->setMapPassword($map_password)
        ->setMapModel($map_model)
        ->setMapExtentXmin($map_extent_xmin)
        ->setMapExtentYmin($map_extent_ymin)
        ->setMapExtentXmax($map_extent_xmax)
        ->setMapExtentYmax($map_extent_ymax)
        ->setMapMinScale($map_minscale)
        ->setMapMaxScale($map_maxscale)
        ->setMapAtommetadataUuid($map_atommetadata_uuid)
        ->setMapWfsmetadataUuid($map_wfsmetadata_uuid)
        ->setMapWmsmetadataUuid($map_wmsmetadata_uuid)
        ->setMapRefmapExtentXmax($mapRefmapExtentXmax)
        ->setMapRefmapExtentXmin($mapRefmapExtentXmin)
        ->setMapRefmapExtentYmax($mapRefmapExtentYmax)
        ->setMapRefmapExtentYmin($mapRefmapExtentYmin)
        ->setMapRefmapImage($mapRefmapImage)
        //->setMapBgcolor($map_bgcolor)
     //   ->setMapDatepublication($map_date_publication)
        // published ID ? user? 
        ->setAccount($em->getRepository('CarmenApiBundle:Account')->find($account->getAccountId()));

        if($projection!="")
            $databaseMap->setMapProjectionEpsg($projection);

        // persist entity to database
        $em->persist($databaseMap);
        // tells doctrine to write in database now (will throw an exception if entity cannot be saved in database)
        $em->flush();

      
        $this->migrateMapUIAndUIModel($oMap, $databaseMap, $output);

       $correspondanceBetweenNumLayersAndDatabaseID_array = array();
       for($i = $oMap->numlayers; $i>0 ; $i--) {
       		$databaseIDofNewLayer = $this->migrateLayer($dirPublication, $oMap->getLayer($i-1), $databaseMap, $output);
       		array_push($correspondanceBetweenNumLayersAndDatabaseID_array,$databaseIDofNewLayer);
       }
       $correspondanceBetweenNumLayersAndDatabaseID_array = array_reverse($correspondanceBetweenNumLayersAndDatabaseID_array);

       $this->migrateMapTool($oMap, $databaseMap, $output);
       ////NOT USED IN PRODIGE$this->migrateMapFavoriteArea($oMap, $databaseMap, $output);
       $this->migrateMapLocator($oMap, $databaseMap, $output);

       $this->migrateGroups($oMap, $databaseMap, $correspondanceBetweenNumLayersAndDatabaseID_array, $output);            
       //NOT USED IN PRODIGE $this->migrateMap_keyword($oMap, $databaseMap, $output);
         
        // try to fetch this map from the database, just the check if it's been correctly persisted
        $em->flush();
        $mapId = $databaseMap->getMapId();
        $em->detach($databaseMap);
        $databaseMap = $em->getRepository('CarmenApiBundle:Map')->find($mapId);
        $databaseMap instanceof Map;
        
        if( $databaseMap ) {    
            $output->writeln('<info>map (id='.$databaseMap->getMapId().') was successfuly created and persited to database !</info>');
        } else {
            $output->writeln('<error>map creation failed !</error>');
        }
        
        

        //specific mapfile traitment
        $this->addAditionnalInfo($oMap, dirname($file), $dirPublication);
        $output->write('<info>Remove unneeded metadata in mapfile '.basename($file).' : </info>');
        $key = null;
        $removed = array();
        while ( $key = $oMap->metadata->nextkey($key) ){
            if ( !preg_match("!". implode("|", $this->metadataPatterns["mapObj"]) ."!", $key) ){
                $removed[] = $key;
            }
        }
        foreach($removed as $indice => $value)
            $oMap->removeMetadata($value);
        
        /*if ( !empty($removed) )
            $output->writeln("  From mapObj  = [".implode(", ", $removed)."]");*/

        for($i = 0; $i<$oMap->numlayers; $i++) {
            $oLayer = $oMap->getLayer($i); 
            $key = null;
            $removed = array();
            while ( $key = $oLayer->metadata->nextkey($key) ){
                if ( !preg_match("!". implode("|", $this->metadataPatterns["layerObj"]) ."!", $key) ){
                    $removed[] = $key;
                }
            }
            foreach($removed as $indice => $value)
                  $oLayer->removeMetadata($value);
            
            /*if ( !empty($removed) )
                $output->writeln("  From layerObj ".$oLayer->name." = [".implode(", ", $removed)."]");*/
            
        }           
        //save Mapfile
        
        $oMap->save($file); 
        
        $output->writeln('<info>Done !</info>');
        
        
        try{
            $em->clear();
            //generate final owsContext
            $output->write('<info>Generate final owsContext for mapfile '.basename($file).' : </info>');
            $owsContext = $this->getContainer()->get('carmen.owscontext');
            $owsContext instanceof OwsContextController;
            $config =  $this->getContainer()->getParameter('carmen_config', array());
            $prefix = ($databaseMap->getMapModel() ? "modele_" : "");
            $owsContext->generateContextAction($account->getAccountId(), $account->getAccountId(), $databaseMap->getMapId(), $prefix.$databaseMap->getMapFile(), $config['owscontext']['publication_dir'], false, true);
            
            $output->writeln('<info> Done!</info>');
        } catch (\Exception $exception){$output->writeln('<error> ERROR : '.$exception->getMessage().'!</error>');}
        // END
        ///////////////////////////////////////////////////
        
        $output->writeln('');
            
  
    }
    
    
    protected function migrateWXS ($dirPublication, $databaseAccount, OutputInterface $output) {
    
	    try {
	    	$types = array(
	        "WMS"  => 1, 
	    	  "WFS"  => 2, 
	    	  "WMTS" => 3, 
	    	  "WMSC" => 4,
	    	);
	    	
	    	foreach ($types as $type_name=>$type_id){
	    	    $filepathXML = $dirPublication."/Serveurs".$type_name."_Administration.xml";
	    	    $this->migrateWXSFromFilepath($filepathXML, $databaseAccount, $type_id, $output);
	    	}
	    	
    	} catch (\Exception $ex) {
    		$output->writeln('<error>Error while migrating WXS : '. $ex.' !</error>');
    	}
    }

    
    
    protected function migrateWXSFromFilePath($filepath, $databaseAccount, $wxsType_id, OutputInterface $output) {

    	if ( !file_exists($filepath) ) return;
    	$em   = $this->getManager();
    	 
    	try {
    		
    		$wxsType = $em->getRepository('CarmenApiBundle:LexWxsType')->find($wxsType_id);
    		$wxsRank = 0 ;
    		
	    	$doc = new \DOMDocument();
	    	if(@$doc->load($filepath)){
		    	$elements = $doc->getElementsByTagName("SERVEUR");    	
		    	
		    	foreach ($elements as $element ){
		    		$wxsName = $element->getAttribute("NOM");
		    		$wxsUrl = $element->getAttribute("URL");
	          if ( $wxsName=="" ) $wxsName = $wxsUrl;
	          
		    		$databaseWxs = new \Carmen\ApiBundle\Entity\Wxs();
		    		$databaseWxs->setAccount($databaseAccount)
		    		->setWxsRank($wxsRank)
		    		->setWxsName($wxsName)
		    		->setWxsUrl($wxsUrl)
		    		->setWxsType($wxsType)
		    		;
		    		
		    		$em->persist($databaseWxs);
		    		$em->flush();
		    	
		    		$wxsRank++;
		    	}
		    	$output->writeln('<info>Migrating servers '.basename($filepath).' ['.$wxsRank.' data inserted] : Done !</info>');
	    	}else{
	    		$output->writeln('<error>Error while migrating WXS : '. $filepath.' invalid XML file !</error>');
	    	}
	    	
    	} catch (\Exception $ex) {
	    	$output->writeln('<error>Error while migrating WXS : '. $ex.' !</error>');
    	}
    }
    

    
    protected function migrateContact ($dirPublication, $databaseUser, OutputInterface $output) {
        $em   = $this->getManager();
        
        try {
/*
            <contact>
            <contactorganization>Onema</contactorganization>
            <contactperson>Equipe Information GÃ©ographique</contactperson>
            <contactposition></contactposition>
            <address>Le "Nadar" Hall C.  5, square FÃ©lix Nadar</address>
            <postcode>94300 </postcode>
            <city>Vincennes</city>
            <stateorprovince></stateorprovince>
            <country></country>
            <mailaddress>sig@onema.fr</mailaddress>
            <telephone></telephone>
            <fax></fax>
            </contact>
  */          
            $filepath = $dirPublication."/contact.xml";
            if ( !file_exists($filepath) ) return;
            $docDOM = new \DOMDocument();
    	    	if($docDOM->load($filepath)){
        	    	
    	    	    $contactorganization = $docDOM->getElementsByTagName("contactorganization")->item(0)->nodeValue;
    	    	    $contactperson = $docDOM->getElementsByTagName("contactperson")->item(0)->nodeValue;
    	    	    $contactposition = $docDOM->getElementsByTagName("contactposition")->item(0)->nodeValue;
    	    	    $address = $docDOM->getElementsByTagName("address")->item(0)->nodeValue;
    	    	    $postcode = $docDOM->getElementsByTagName("postcode")->item(0)->nodeValue;
    	    	    $city = $docDOM->getElementsByTagName("city")->item(0)->nodeValue;
    	    	    $stateorprovince = $docDOM->getElementsByTagName("stateorprovince")->item(0)->nodeValue;
    	    	    $country = $docDOM->getElementsByTagName("country")->item(0)->nodeValue;
    	    	    $mailaddress = $docDOM->getElementsByTagName("mailaddress")->item(0)->nodeValue;
    	    	    $telephone = $docDOM->getElementsByTagName("telephone")->item(0)->nodeValue;
    	    	    $fax = $docDOM->getElementsByTagName("fax")->item(0)->nodeValue;
    	    	    
       	    		$contact = new \Carmen\ApiBundle\Entity\Contact();
    	    		  $contact->setUser($databaseUser)
            	    		  ->setContactAddress($address)
            	    		  ->setContactCity($city)
            	    		  ->setContactCountry($country)
            	    		  ->setContactEmail($mailaddress)
            	    		  ->setContactFax($fax)
            	    		  ->setContactName($contactperson)
            	    		  ->setContactOrganization($contactorganization)
            	    		  ->setContactPhone($telephone)
            	    		  ->setContactPosition($contactposition)
            	    		  ->setContactUnit($stateorprovince)
            	    		  ->setContactZipcode($postcode);
    	    		  
      	    		$em->persist($contact);
        	    	$em->flush();
    	    	}
    
        } catch (\Exception $ex) {
            $output->writeln('<error>Error while migrating Contact : '. $ex.' !</error>');
        }
    }
    
    
    
    
    

    protected function migrateFields($dirPublication, &$oLayer, $databaseLayer, OutputInterface $output) {
        $em   = $this->getManager();
        $em->getConnection()->getConfiguration()->setSQLLogger(null);
        gc_collect_cycles();
        $correspondaceArray_fieldtypeIDandName = array(
            "TXT"=>"1",
            "URL" => "2",
            "IMG" => "3",
            "AAAAMMJJ" => "4",
            "AAAAJJMM" => "5",
            "MMJJAAAA" => "6",
            "JJMMAAAA" => "7"
        );
        $new_gml_include_items = array();
        $gml_include_items = $oLayer->getMetadata("geoide_gml_include_items");
        $tabItems = explode((strpos($gml_include_items, ",")!== false ? "," : " "), $gml_include_items);
         
        try {
            $fieldsActions = array("resume"=>array(), "minifiche"=>array(), "fiche"=>array());
            // get data from layer object
             
            $data_resume_string = $oLayer->getMetadata("RESUME");
            $data_resume_array = array();
            if($data_resume_string){
                $data_resume_array = explode(";", $data_resume_string);
                $tmp = array();
                foreach ($data_resume_array as $field){
                    if ( empty($field) ) continue;
                    $field = explode("|", $field);
                    $tmp[] = $field[0];
                }
                $fieldsActions["resume"] = $tmp;
            }
             
            $data_minifiche_string = $oLayer->getMetadata("MINIFICHE");
            $data_minifiche_array = array();
            if($data_minifiche_string){
                $data_minifiche_array = explode(";", $data_minifiche_string);
                $tmp = array();
                foreach ($data_minifiche_array as $field){
                    if ( empty($field) ) continue;
                    $field = explode("|", $field);
                    $tmp[] = $field[0];
                }
                $fieldsActions["minifiche"] = $tmp;
            }
             
            $data_fiche_string = $oLayer->getMetadata("FICHE");
            $data_fiche_array = array();
            if($data_fiche_string){
                $data_fiche_array = explode(";", $data_fiche_string);
                $tmp = array();
                foreach ($data_fiche_array as $field){
                    if ( empty($field) ) continue;
                    $field = explode("|", $field);
                    $tmp[] = $field[0];
                }
                $fieldsActions["fiche"] = $tmp;
            }
             
            $all_fields_array = array_values(array_unique(array_merge($data_fiche_array, $data_minifiche_array, $data_resume_array)));
             
            $layerFields = array();
             
            // detecting data throw ogr driver for vector data
            $data = array();
            if($oLayer->connectiontype == MS_OGR) {
                if($oLayer->connection != "" && file_exists($dirPublication . "/" . $oLayer->connection))
                    $data = array(
                        "connection" => $dirPublication . "/" . $oLayer->connection,
                        "index" => 0
                    );
            } elseif($oLayer->connectiontype == MS_SHAPEFILE) {
                if($oLayer->data != "" && file_exists($dirPublication . "/" . $oLayer->data))
                    $data = array(
                        "connection" => $dirPublication . "/" . $oLayer->data,
                        "index" => 0
                    );
            } elseif($oLayer->connectiontype == MS_WFS) {
                if($oLayer->connection != "")
                    $data = array(
                        "connection" => "WFS:" . $oLayer->connection,
                        "index" => $oLayer->getMetadata("wfs_typename")
                    );
            } elseif($oLayer->connectiontype == MS_POSTGIS) {
                 
                $schema = preg_replace('!.+from\s+("?(\w+)"?\.)?"?(\w+)"?.+!i', "$2", $oLayer->data);
                $table = preg_replace('!.+from\s+("?(\w+)"?\.)?"?(\w+)"?.+!i', "$3", $oLayer->data);
                $layerName = $table;
                $layerData = "PG:" . $oLayer->connection .($schema ? " active_schema=" . $schema : "");
                $data = array(
                    "connection" => $layerData,
                    "index" => $table
                );
            } else {
                $output->writeln('<error>problem whis layer detection  "' . $oLayer->connectiontype . '" :  ' . ' !</error>');
            }
             
            if(! empty($data)) {
                $getFieldsResult = $this->getFieldTypes($data["connection"], $data["index"], $output);
                $layerFieldsDef = $getFieldsResult[0];
                 
                // Encoding set to layer
                if($getFieldsResult[1] != "US-ASCII") {
                    $oLayer->setMetadata("encoding", $getFieldsResult[1]);
                    $databaseLayer->setLayerDataEncoding($getFieldsResult[1]);
                }
                // if classitem/labelitem found, set field name in data encoding
                /*
                 * if($oLayer->classitem!=""){
                * $oLayer->set("classitem", iconv("utf-8", $getFieldsResult[1], $oLayer->classitem));
                * }
                * if($oLayer->labelitem!=""){
                * $oLayer->set("labelitem", iconv("utf-8", $getFieldsResult[1], $oLayer->labelitem));
                * }
                */
                $em->persist($databaseLayer);
                if($layerFieldsDef)
                    $layerFields = array_keys($layerFieldsDef);
                 
                // warning log on field with accents
            		  $aValid = array('-', '_');
            		  for($i = 0; $i < count($layerFields); $i ++) {
            		      if(! ctype_alnum(str_replace($aValid, '', $layerFields[$i]))) {
            		          $output->writeln('<error>layer contains fields name with accents, not completely supported</error>');
            		          break;
            		      }
            		  }
            		  if ( stripos($getFieldsResult[1], "UNKNOWN")!==false ){
            		      $layerFields = array_map(function($v)use(&$layerFieldsDef){
        	               $vconv = mb_convert_encoding($v, "UTF-8");
        	               $layerFieldsDef[$vconv] = $layerFieldsDef[$v];
        	               return $vconv;
        	           }, $layerFields);
            		  }
    
            		  $rankFields = 0;
            		  foreach($all_fields_array as $fieldRank=>$fieldDef) {
                    $rankFields = $fieldRank;
                    $fieldDef = explode("|", $fieldDef);
    
                    $fieldName = $fieldDef[0];
    
                    if(in_array($fieldName, $layerFields)) { // only put in field if field is really a data field
                        $fieldAlias = $fieldDef[1];
    
                        $data_fieldType_name = $fieldDef[2];
                        $data_fieldType_id = $correspondaceArray_fieldtypeIDandName[$data_fieldType_name];
                        $fieldtype = $em->getRepository('CarmenApiBundle:LexFieldType')->find($data_fieldType_id);
    
                        $fieldUrl = isset($fieldDef[3]) ? $fieldDef[3] : "";
    
                        $fieldQueries = in_array($fieldName, $fieldsActions["fiche"]);
                        $fieldTooltips = in_array($fieldName, $fieldsActions["minifiche"]);
                        $fieldHeaders = in_array($fieldName, $fieldsActions["resume"]);
                        if ( $fieldQueries || $fieldTooltips ){
                            $new_gml_include_items[] = $fieldName;
                        }
    
                        $fieldOgc = ( $gml_include_items == "all" || in_array($fieldName, $tabItems) );
    
                        $databaseField = new \Carmen\ApiBundle\Entity\Field();
                        $fieldDataType = $em->getRepository('CarmenApiBundle:LexFieldDatatype')->find($layerFieldsDef[$fieldName]);
    
                        $databaseField
                        ->setLayer($databaseLayer)
                        ->setFieldRank($fieldRank)
                        ->setFieldName($fieldName)
                        ->setFieldAlias($fieldAlias)
                        ->setFieldType($fieldtype)
                        ->setFieldUrl($fieldUrl)
                        ->setFieldQueries($fieldQueries)
                        ->setFieldTooltips($fieldTooltips)
                        ->setFieldHeaders($fieldHeaders)
                        ->setFieldOgc($fieldOgc)
                        ->setFieldDatatype($fieldDataType)
                        ;
                        $databaseLayer->addFields($databaseField);
    
                        $layerFields = array_diff($layerFields, array($fieldName)); // delete entry from layer fields
                    }
                }
    
                //add fields not configured
                //for ($i = 0 ; $i < count($layerFields) ; $i++) {
                foreach ($layerFields as $i=> $value) {
                    if(isset($layerFields[$i])){//cause some keys have been deleted
    
                        $fieldName = $layerFields[$i];
                        $fieldAlias = $fieldName;
                        $data_fieldType_id = $correspondaceArray_fieldtypeIDandName["TXT"];
                        $fieldtype = $em->getRepository('CarmenApiBundle:LexFieldType')->find($data_fieldType_id);
                        	
                        $rankFields = $rankFields+1;
                        $fieldRank = $rankFields;
                        	
                        $fieldQueries = false ;
                        $fieldTooltips = false;
                        $fieldHeaders = false;
                        $fieldOgc = ($gml_include_items=="all" || in_array($fieldName, $tabItems));
                        	
                        $databaseField = new \Carmen\ApiBundle\Entity\Field();
    
                        $fieldDataType = $em->getRepository('CarmenApiBundle:LexFieldDatatype')->find($layerFieldsDef[$fieldName]);
                        $databaseField
                        ->setLayer($databaseLayer)
                        ->setFieldRank($fieldRank)
                        ->setFieldName($fieldName)
                        ->setFieldAlias($fieldAlias)
                        ->setFieldType($fieldtype)
                        ->setFieldQueries($fieldQueries)
                        ->setFieldTooltips($fieldTooltips)
                        ->setFieldHeaders($fieldHeaders)
                        ->setFieldOgc($fieldOgc)
                        ->setFieldDatatype($fieldDataType);
                        ;
                        $databaseLayer->addFields($databaseField);
                    }
                }
            }
             
            if ( empty($new_gml_include_items) ){
                if($oLayer->getMetadata("gml_include_items")!="")
                    $oLayer->removeMetadata('gml_include_items');
                if($oLayer->getMetadata("gml_include_items")!="")
                    $oLayer->removeMetadata('ows_include_items');
            } else {
                if ( count($new_gml_include_items)==count($databaseLayer->getFields()) ){
                    $oLayer->setMetadata('gml_include_items', 'all');
                    $oLayer->setMetadata('ows_include_items', 'all');
                } else {
                    $oLayer->setMetadata('gml_include_items', implode(",", $new_gml_include_items));
                    $oLayer->setMetadata('ows_include_items', implode(",", $new_gml_include_items));
                }
            }
    
    
             
             
        } catch (\Exception $ex) {
            $output->writeln('<error>problem with field detection (can\'t open layer named "'.$oLayer->name.'") !</error>');
            $output->writeln('<error>' . $ex->getMessage() . ' [' . $ex->getLine() . ']</error>');
        }
    }
    
    
    /**
     * Returns the types of the fields for the layer given
     * @param $strfilename
     * @return $FieldsTypes : array(field_name=>type of the field)
     */
    
    protected function getFieldTypes($strfilename, $layerIndex = 0, OutputInterface $output)
    {
        // always put your code in a try catch block to be sure to always return a JsonResponse
        try {
            //echo "memory usage before treat fields". memory_get_usage() . "\n";
            $strfilename = "\"".$strfilename."\"";
            if(!is_numeric($layerIndex))
                $strfilename.=" \"".$layerIndex."\"";
            $tempname = tempnam(sys_get_temp_dir(), "layerfieldvalues_");
            @unlink($tempname);
            $csvfile = $tempname.".csv";
            $detectEncodingFile = $tempname."_encoding.csv";
            $destCsvfile = $tempname."_UTF8.csv";
    
            $toEncoding = "UTF-8";
            $cmd = "ogrinfo -ro -al -so -nocount -noextent -nomd {$strfilename} > \"{$csvfile}\" 2> \"{$tempname}.log\"";
            $errors = array(); $return=null;
            $result = exec($cmd, $errors, $return);
            //echo $cmd."\n";
            $errors = array_merge($errors, array(file_get_contents("{$tempname}.log")));
            //echo $csvfile."\n";
            $cmd = "file -b --mime-encoding ".$csvfile." > ".$detectEncodingFile. " 2> \"{$tempname}_file.log\"";
            //echo $cmd."\n";
            $result = exec($cmd, $errors, $return);
            //echo $detectEncodingFile."\n";
            $resultEncoding="";
            if(file_exists($detectEncodingFile))
                $resultEncoding = strtoupper(str_replace("\n", '',file_get_contents($detectEncodingFile)));
            //impossible to detec file
            //echo $resultEncoding."\n";
            if($resultEncoding == "" || $resultEncoding =="BINARY" || !empty($errors[0])){
                throw new \Exception('Problem detecting fields, can\'t open layer '.$strfilename.' with ogrinfo');
            }
             
            $cmd = "iconv -f ".$resultEncoding. " -t ".$toEncoding ." ". $csvfile. " > ".$destCsvfile;
    
            //echo $cmd."\n";
    
            $result = exec($cmd, $errors, $return);
             
            $output->writeln('detect encoding... '.$resultEncoding);
    
            $startfield_pattern = "!^Layer SRS WKT!is";
            $field_pattern = "!^([^\s:]+)\s*:\s+([^\s:]+)!i";
    
            $items = array();
            $fieldListStart = false;
            $projectionCount = 0;
            if ( file_exists($destCsvfile) ){
                $file = fopen($destCsvfile, "r");
                while( $line = fgets($file) ){
                    if ( preg_match($startfield_pattern, $line) || $fieldListStart || $projectionCount>0 ){
                        $fieldListStart = true;
                        $projectionCount += substr_count($line, "[");
                        $projectionCount -= substr_count($line, "]");
                    }
                    if ( $fieldListStart && $projectionCount==0 &&  preg_match($field_pattern, $line) ){
                        preg_replace($field_pattern."e", '$items["$1"] = "$2";', $line);
                    }
                }
                foreach($items as $key => $value){
                    $items[$key] = $this->getCarmenType($value);
                }
                //print_r($items);
                //echo "memory usage after treat fields". memory_get_usage() . "\n";
                return array($items, $resultEncoding);
                 
            }
            @unlink($csvfile);
            @unlink($detectEncodingFile);
            @unlink($destCsvfile);
            @unlink($tempname);
            @unlink($tempname.".log");
            @unlink($tempname."_file.log");
    
        } catch(\Exception $ex ) {
            $output->writeln('<error>'.$ex->getMessage().'</error>');
            return array();
        }
    }
    
    /**
     * get Carmen types according to ogrtypes
     * @param unknown $ogrType
     * @return number
     */
    protected function getCarmenType($ogrType){
        // call the helper function
        $type = Helpers::getCarmenType($ogrType);
        if( null === $type ) {
            // TODO we should not be doing echo here
            echo '<error>problem ogr type not recognized "'.$ogrType.'" :  !</error>';
        }
        return $type;
    }
    
    
    
    
    
    /**
     *
     * @param unknown $oLayer mslayer obj
     * @param unknown $map doctrine map obj
     * @param OutputInterface $output
     */
    protected function migrateLayer($dirPublication, &$oLayer, $map, OutputInterface $output)
    {
        $conn = $this->getConnection();
        $em   = $this->getManager();
        //$em->clear();
        $em->getConnection()->getConfiguration()->setSQLLogger(null);
        gc_collect_cycles();
        //echo "memory usage before treat layer". memory_get_usage() . "\n";
        try {
            //create LAYER in database
            $layer_title = $oLayer->getMetadata("LAYER_TITLE");
            //this is layer name in wms file
            $layer_name = "";////NOT USED IN PRODIGE $oLayer->getMetadata("wms_name");
             
            //name not given in mapfile, attribute one
            if($layer_name==""){
                $layer_name = $oLayer->name;
            }
             
            /*if(file_exists($wmsfile)){
             try {
            $oMapWms = ms_newMapObj($wmsfile);
            //migrate WMS layer name
            if($oLayerWMS = $oMapWms->getLayerByName($layer_name)){
            //truncate 20 caracs
            $layer_name = substr($layer_name, 0, 20);
            $oLayerWMS->set("name", $layer_name);
            $oMapWms->save($wmsfile);
            }
            } catch (\Exception $ex) {
            //$output->writeln('<error>wms file : '.$wmsfile.' not treated, corrupted for Mapserver 7.0 !</error>');
            }
             
            }*/
             
             
            $layer_msname = $oLayer->name;
            $layer_id     = $oLayer->name;
            $output->writeln('treating layer '.$layer_title."(".$oLayer->name.")");
            $layer_wmsurl = "";
            $layer_type_id ="";
            $layerHaslabel = false;
            $layer_projection ="";
            $layerServerVersion = "";
            $layerWxsname ="";
            $layer_wmsformat="";
    
            $layer_tiled_layer       = "";
            $layer_tiled_server_url  = "";
            $layer_tiled_resolutions = "";
            $layer_wmts_matrixids    = "";
            $layer_wmts_tileorigin   = "";
            $layer_wmts_tileset      = "";
            $layer_wmts_style        = "";
            $layer_wmsc_boundingbox  = "";
    
    
            switch($oLayer->connectiontype){
              case MS_POSTGIS:
                  $layer_type_id = "POSTGIS";
    
                   
                  break;
              case MS_WMS :
                  $layer_type_id = "WMS";
                  if($oLayer->getMetadata("WMSC")=="YES"){
                      $layer_type_id = "WMSC";
                      $layer_tiled_layer       = $oLayer->getMetadata("WMSC_WMS_LAYER");
                      $layer_tiled_server_url  = $oLayer->getMetadata("WMSC_WMS_ONLINERESOURCE");
                      $layer_tiled_resolutions = $oLayer->getMetadata("WMSC_RESOLUTION");
                      $layer_wmsc_boundingbox  = $oLayer->getMetadata("WMSC_BOUNDINGBOX");
                  }
                  elseif($oLayer->getMetadata("WMTS")=="YES"){
                      $layer_type_id = "WMTS";
                      $layer_tiled_layer       = $oLayer->getMetadata("WMTS_WMS_LAYER");
                      $layer_tiled_server_url  = $oLayer->getMetadata("WMTS_WMS_ONLINERESOURCE");
                      $layer_tiled_resolutions = $oLayer->getMetadata("WMTS_RESOLUTIONS");
                      $layer_wmts_matrixids    = $oLayer->getMetadata("WMTS_MATRIXIDS");
                      $layer_wmts_tileorigin   = $oLayer->getMetadata("WMTS_TILEORIGIN");
                      $layer_wmts_tileset      = $oLayer->getMetadata("WMTS_TILESET");
                      $layer_wmts_style        = $oLayer->getMetadata("WMTS_STYLE");
                  }
                   
                  $layer_wmsurl = $oLayer->getMetadata("wms_onlineresource");
                  $layer_wmsformat = $oLayer->getMetadata("wms_format");
                  $layerWxsname = $oLayer->getMetadata("wms_name");
                  $layerServerVersion = $oLayer->getMetadata("wms_server_version");
                  $layer_wms_srs = $oLayer->getMetadata("wms_srs");
                  if($layer_wms_srs!=""){
                      $layer_projection_epsg = substr($layer_wms_srs,5); // taking only the ID after the 5char string "epsg:"
                      if(is_numeric($layer_projection_epsg))
                          $layer_projection = $em->getRepository('CarmenApiBundle:LexProjection')->find($layer_projection_epsg);
                  }
    
                  break;
              case MS_WFS :
                  $layer_type_id = "WFS";
                  $layer_wmsurl = $oLayer->connection;
                  $layerWxsname = $oLayer->getMetadata("wfs_typename");
                  $layer_wms_srs = $oLayer->getMetadata("wfs_srs");
                  if($layer_wms_srs!=""){
                      $layer_projection_epsg = substr($layer_wms_srs,5); // taking only the ID after the 5char string "epsg:"
                      if(is_numeric($layer_projection_epsg))
                          $layer_projection = $em->getRepository('CarmenApiBundle:LexProjection')->find($layer_projection_epsg);
                  }
                   
                  break;
            }
            if($layer_type_id==""){
                if ($oLayer->type == MS_LAYER_RASTER)
                    $layer_type_id = "RASTER";
                else
                    $layer_type_id = "VECTOR";
            }
            $layer_type = $em->getRepository('CarmenApiBundle:LexLayerType')->find($layer_type_id);
             
            //TODO vÃ©rifier si ce paramÃ¨tre est vraiment nÃ©cessaire et si on ne doit pas migrer cela au niveau du mapfile
            //Il est possible de supprimer du getlegendgraphic une classe Â«Â en commentantÂ Â» avec un # le paramÃ¨tre name, sans supprimer son affichage cartographique
            $layer_legend = ($oLayer->getMetadata("CLASS_LEGENDE")=="ON");
            $layer_legend_scale = ($oLayer->getMetadata("ECHELLE_LEGENDE")=="ON");
    
            /** @see MigrationCommand::execute() for the constants BD_ANALYSETYPE_xx */
            if($layer_type_id == "RASTER" || $layer_type_id =="WMS" || $layer_type_id =="WMSC" || $layer_type_id =="WMTS")
                $layer_analyse_type_id = BD_ANALYSETYPE_RASTER;
            else{
                //default values
                if($oLayer->numclasses<=1){
                    //Uniq symbol analysis
                    $layer_analyse_type_id = BD_ANALYSETYPE_UNIQSYMBOL;
                }else{
                    //uniq values analysis
                    $layer_analyse_type_id = BD_ANALYSETYPE_UNIQVALUE;
                }
                switch ($oLayer->getMetadata("TYPE_SYMBO")){
                  case "UNIQUE":
                      $layer_analyse_type_id = BD_ANALYSETYPE_UNIQSYMBOL;
                      break;
                  case "DISTINCT":
                      $layer_analyse_type_id = BD_ANALYSETYPE_UNIQVALUE;
                      break;
                  case "CLASSES":
                      $layer_analyse_type_id = BD_ANALYSETYPE_GRADUATECOLOR;
                      break;
                  case "PROPORTIONAL":
                      $layer_analyse_type_id = BD_ANALYSETYPE_PROPORTIONAL;
                      // only one class
                      $oClass = $oLayer->getClass(0);
                      $oLabel = $oClass->getLabel(0);
                      $oStyle = $oClass->getStyle(0);
                      //can't know if layer is polygon or point
                      $oStyle->setGeomTransform("centroid");
                      $oStyle->set("minsize", $oLabel->minsize);
                      $oStyle->set("maxsize", $oLabel->maxsize);
    
                      break;
                  case "GRADUEL":
                      $layer_analyse_type_id = BD_ANALYSETYPE_GRADUATESYMBOL;
                      break;
                  case "CHARTS":
                      $tabProcessing = $oLayer->getProcessing();
                      if(isset($tabProcessing["CHART_TYPE"]) && $tabProcessing["CHART_TYPE"] =="PIE")
                          $layer_analyse_type_id = BD_ANALYSETYPE_PIECHART;
                      else
                          //BAR
                          $layer_analyse_type_id = BD_ANALYSETYPE_BARCHART;
                      break;
                  default :
                      //$layer_analyse_type_id = BD_ANALYSETYPE_UNIQSYMBOL;
                      break;
                }
                if($oLayer->labelitem !=""){
                    $layerHaslabel=true;
                }
    
            }
    
    
            $layer_analyse = $em->getRepository('CarmenApiBundle:LexAnalyseType')->find($layer_analyse_type_id);
             
             
            $layer_field_url = $oLayer->getMetadata("FICHE_URLBASE");
            $layerMetadataFile = $oLayer->getMetadata("GI_METADATA_FILE");
    
            $uuid="";
            if($oLayer->getMetadata("GEONETWORK_METADATA_ID")!=""){
                //Prodige database layer
                if(isset($GLOBALS["tabMetadata"][$oLayer->getMetadata("GEONETWORK_METADATA_ID")])){
                    $uuid = $GLOBALS["tabMetadata"][$oLayer->getMetadata("GEONETWORK_METADATA_ID")];
                    $layerMetadataFile = $this->getContainer()->getParameter("PRO_GEONETWORK_URLBASE")."/srv/fre/catalog.search#/metadata/".$uuid;
                }
            }
    
            $layerMetadataUuid = $uuid;
            $layer_wms =  FALSE;////NOT USED IN PRODIGE$oLayer->getMetadata("WMS_STATUS")=="ON";
            $layer_wfs = FALSE;//NOT USED IN PRODIGE;$oLayer->getMetadata("WFS_STATUS")=="ON";
            $layer_atom = FALSE;////NOT USED IN PRODIGE $oLayer->getMetadata("TELECHARGEABLE")=="ON";
            $layer_downloadable = FALSE;//NOT USED IN PRODIGE $oLayer->getMetadata("TELECHARGEABLE")=="ON";
            $layer_opacity = $oLayer->opacity;
            $layer_minscale = intval(max(0, $oLayer->minscaledenom));
            $layer_maxscale = intval(max(0, $oLayer->maxscaledenom));
            $layer_visible = $oLayer->status;
    
    
            $layer = new \Carmen\ApiBundle\Entity\Layer();
    
            $layer->setLayerType($layer_type)
            ->setMap($map)
            ->setLayerTitle($layer_title)
            ->setLayerName($layer_name)
            ->setLayerMsname($layer_msname)
            ->setLayerLegend($layer_legend)
            ->setLayerLegendScale($layer_legend_scale)
            ->setLayerAnalyseType($layer_analyse)
            ->setLayerFieldUrl($layer_field_url)
            ->setLayerMetadataFile($layerMetadataFile)
            ->setLayerMetadataUuid($layerMetadataUuid)
            ->setLayerWms($layer_wms)
            ->setLayerWfs($layer_wfs)
            ->setLayerAtom($layer_atom)
            ->setLayerDownloadable($layer_downloadable)
            ->setLayerOpacity($layer_opacity)
            ->setLayerMinscale($layer_minscale)
            ->setLayerMaxscale($layer_maxscale)
            ->setLayerVisible($layer_visible)
            ->setLayerOutputformat($layer_wmsformat)
            ->setLayerHaslabel($layerHaslabel)
            ->setLayerIdentifier($layer_id)
    
            ->setLayerServerUrl($layer_wmsurl)
            ->setLayerServerVersion($layerServerVersion)
            ->setLayerWxsname($layerWxsname)
    
            ->setLayerTiledLayer($layer_tiled_layer)
            ->setLayerTiledServerUrl($layer_tiled_server_url)
            ->setLayerTiledResolutions($layer_tiled_resolutions)
            ->setLayerWmscBoundingbox($layer_wmsc_boundingbox)
            ->setLayerWmtsMatrixids($layer_wmts_matrixids)
            ->setLayerWmtsTileorigin($layer_wmts_tileorigin)
            ->setLayerWmtsTileset($layer_wmts_tileset)
            ->setLayerWmtsStyle($layer_wmts_style)
            ;
    
    
            if ($layer_projection!="")
                $layer->setLayerProjectionEpsg($layer_projection);
    
            //$output->writeln('treating layer called '.$layer->name);
             
    
            if(in_array($layer_type_id, array("VECTOR", "WFS", "POSTGIS")))
                $this->migrateFields($dirPublication, $oLayer, $layer, $output);
    
            // persist entity to database
            $em->persist($layer);
            $em->flush();
    
            return $layer->getLayerId();
    
    
            // FIN
            ///////////////////////////////////////////////////
    
    
        } catch (\Exception $ex) {
            $output->writeln('<error>problem while treating layer '.$oLayer->name.'  '. $ex.' !</error>');
    
        }
    }
    
    
    
    
    protected function migrateMapUIAndUIModel($oMap, $map, OutputInterface $output)
    {
        $em   = $this->getManager();
        $em->getConnection()->getConfiguration()->setSQLLogger(null);
        gc_collect_cycles();
    
        try {
            $mapUI_logo = ($oMap->getMetadata("ADDINCRUSTATIONS")==1 && $oMap->getMetadata("ADDLOGO")==1);
            $mapUI_logopath = ($oMap->getMetadata("ADDINCRUSTATIONS")==1 ? $oMap->getMetadata("LOGOPATH") : "");
            $mapUI_copyright_text = ($oMap->getMetadata("ADDINCRUSTATIONS")==1 ? trim($oMap->getMetadata("COPYRIGHT")): "");
            $mapUI_copyright = $oMap->getMetadata("ADDINCRUSTATIONS")==1 && !empty($mapUI_copyright_text) && ($oMap->getMetadata("ADDCOPYRIGHT")==1);
            $mapUI_copyright_font = ($oMap->getMetadata("ADDINCRUSTATIONS")==1 ? $oMap->getMetadata("FONT_COPYRIGHT") : "");
            $mapUI_copyright_font_color = ($oMap->getMetadata("ADDINCRUSTATIONS")==1 ? substr($oMap->getMetadata("FONT_COLOR"),0,6) : "");
            $bgtransparent = 0;
            if ($oMap->getMetadata("ADDINCRUSTATIONS")==1 && strlen($oMap->getMetadata("BACK_COLOR")) == 8){
                $bgtransparent = (int)floor(hexdec(substr($oMap->getMetadata("BACK_COLOR"), 6, 2)) / 255 * 100);
            }
            $mapUI_copyright_font_size = ($oMap->getMetadata("ADDINCRUSTATIONS")==1 ? $oMap->getMetadata("FONT_SIZE") : "");
            //BACK_COLOR on 8 caracters, take 6 first caracters
            $mapUI_copyright_background_color = ($oMap->getMetadata("ADDINCRUSTATIONS")==1 ? substr($oMap->getMetadata("BACK_COLOR"),0,6) : "");
            //TODO vérifier cette info n'existe pas à priori
            //$mapUI_copyright_background_transparency = ?
            $theme_color = $oMap->getMetadata("EXT_THEME_COLOR");
            $mapUI_color_id = $em->getRepository('CarmenApiBundle:LexColorId')->findOneBy(array("colorKey" => $theme_color));
            $mapUI_banner = ($oMap->getMetadata("BANDEAU")=="ON");
            $mapUI_banner_file = $oMap->getMetadata("BANNER_MODEL");
            $mapUI_legend = ($oMap->getMetadata("LEGENDE")=="ON");
            //TODO Ã  vérifier vis Ã  vis du code source existant
            $mapUI_legend_print = FALSE;//NOT USED IN PRODIGE($oMap->getMetadata("LEGENDE_PRINT")=="ON");
            $mapUI_keymap = ($oMap->getMetadata("VUE_GLOBALE")=="ON");
            $mapUI_focus = FALSE;//NOT USED IN PRODIGE($oMap->getMetadata("CHANGEMENT_RESOLUTION")=="ON");
            $mapUI_locate = ($oMap->getMetadata("LOCALISATION")=="ON");
    
    
            $database_mapUI = new \Carmen\ApiBundle\Entity\MapUi();
            $database_mapUI->setMap($map)
            ->setUiLogo($mapUI_logo)
            ->setUiLogoPath($mapUI_logopath)
            ->setUiCopyright($mapUI_copyright)
            ->setUiCopyrightText($mapUI_copyright_text)
            ->setUiCopyrightFont($mapUI_copyright_font)
            ->setUiCopyrightFontColor($mapUI_copyright_font_color)
            ->setUiCopyrightBackgroundColor($mapUI_copyright_background_color)
            ->setUiCopyrightTransparency($bgtransparent)
            ->setUiColor($mapUI_color_id)
            ->setUiBanner($mapUI_banner)
            ->setUiBannerFile($mapUI_banner_file)
            ->setUiLegend($mapUI_legend)
            ->setUiLegendPrint($mapUI_legend_print)
            ->setUiKeymap($mapUI_keymap)
            ->setUiFocus($mapUI_focus)
            ->setUiLocate($mapUI_locate)
            ->setUiCopyrightFontSize(intval($mapUI_copyright_font_size))
            ;
    
            $this->migrateUIModel ($oMap, $database_mapUI, $output);
            // persist entity to database
            $em->persist($database_mapUI);
            $em->flush();
    
        } catch (\Exception $ex) {
            $output->writeln('<error>problem while treating mapUI '.$oMap->name.'  '. $ex.' !</error>');
        }
    }
    
    
    
    protected function migrateUIModel ($oMap, $database_mapUI, OutputInterface $output) {
        $conn = $this->getConnection();
        $em   = $this->getManager();
        $em->getConnection()->getConfiguration()->setSQLLogger(null);
        gc_collect_cycles();
         
        try {
            $model_rawdata_string = $oMap->getMetadata("LAYOUT_MODEL");
            if ($model_rawdata_string)
                $model_rawdata_array = explode(";", $model_rawdata_string);
            else $model_rawdata_array = array();
    
            $database_mapUI instanceof \Carmen\ApiBundle\Entity\MapUi;
            $account = $database_mapUI->getMap()->getAccount();
             
            for ($i = 0 ; $i < count($model_rawdata_array) ; $i++) {
                //$thismodelinfo_array = explode (",", $model_rawdata_array[$i]);
    
                $modeInfo = explode ("@", $model_rawdata_array[$i]);
    
                $file = $modeInfo[0];
                $name = $modeInfo[1];
    
                $existingmodelFiles = $em->getRepository('CarmenApiBundle:AccountModel')->findBy(array('account'=>$account->getAccountId(), "accountModelFile" => $file));
    
                $suchAModelAlreadyExists = ( count($existingmodelFiles) > 0 );
                if($existingmodelFiles) {
                    $model = $existingmodelFiles[0];
                }
                else {
                    $model = new \Carmen\ApiBundle\Entity\AccountModel();
                    $model->setAccount($account)->setAccountModelFile($file)->setAccountModelName($name);
                    $em->persist($model);
                    $em->flush();
                }
    
    
                $modelRank = $i;
                 
                $database_uimodel = new \Carmen\ApiBundle\Entity\UiModel();
                $database_uimodel->setMapUi($database_mapUI)
                ->setModelRank($modelRank)
                ->setAccountModel($model);
                //->setModelFile($modelFile);
                //->setModelName($modelName); NB1 no more modelName in database architecture (and method setName doesn't exist anymore)
                $database_mapUI->addUiModels($database_uimodel);
                 
                //$em->persist($database_uimodel);
                //$em->flush();
    
    
    
            }
        } catch (\Exception $ex) {
            $output->writeln('<error>problem while treating uimodel of map "'.$oMap->name.'" :  '. $ex.' !</error>');
        }
    }
    
    
    //NOT USED IN PRODIGE
    protected function migrateMap_keyword ($oMap, $databaseMap, OutputInterface $output) {
    
        $conn = $this->getConnection();
        $em   = $this->getManager();
        $em->getConnection()->getConfiguration()->setSQLLogger(null);
        gc_collect_cycles();
        $em->persist($databaseMap);
         
         
        try {
    
            $keywords_string = $oMap->getMetadata("KEYWORDS");
            $keywords_array = explode(";", $keywords_string);
    
    
            foreach ($keywords_array as $thisKeyword_string) {
                 
                $thisKeyword_array = explode ("|", $thisKeyword_string);
    
                if ( count($thisKeyword_array) > 1) {
    
    
                    $thisKeyword_categoryName = $thisKeyword_array[0] ?: null;
                    $thisKeyword_name = $thisKeyword_array[1];
    
    
                    $collection_foundKeywords = $em->getRepository('CarmenApiBundle:Keyword')->findBy(array('keywordName'=>$thisKeyword_name));
                    $suchAKeywordAlreadyExists = ( count($collection_foundKeywords) > 0 );
                    if($suchAKeywordAlreadyExists) {
                        $databaseKeyword = $collection_foundKeywords[0];
    
                    }
                    else {
                        $databaseKeyword = new \Carmen\ApiBundle\Entity\Keyword();
                        $databaseKeyword->setKeywordName($thisKeyword_name);
                    }
    
    
                    $collection_foundCategories = $em->getRepository('CarmenApiBundle:LexCategoryKeyword')->findBy(array('categoryName'=>$thisKeyword_categoryName));
                    $suchACategorydAlreadyExists = ( count($collection_foundCategories)>0 );
                    if ($suchACategorydAlreadyExists) {
                        $databaseCategory = $collection_foundCategories[0];
                    }
                    else {
                        $databaseCategory = new \Carmen\ApiBundle\Entity\LexCategoryKeyword();
                        $databaseCategory->setCategoryName($thisKeyword_categoryName);
                    }
    
                    $databaseKeyword->setCategory($databaseCategory);
                    $databaseMap instanceof \Carmen\ApiBundle\Entity\Map;
                    $databaseMap->addKeywords($databaseKeyword);
    
                    $em->persist($databaseCategory);
                    $em->flush();
    
                    $em->persist($databaseKeyword);
                    $em->flush();
                }
    
            }
    
            //updating the map
            $em->persist($databaseMap);
            $em->flush();
    
    
    
        } catch (\Exception $ex) {
            $output->writeln('<error>problem while treating map_keywords of map "'.$oMap->name.'" :  '. $ex.' !</error>');
        }
         
    }
    
    
    
    
    
     
    protected function migrateMapTool ($oMap, $databaseMap, OutputInterface $output)
    {
        $conn = $this->getConnection();
        $em   = $this->getManager();
        $em->getConnection()->getConfiguration()->setSQLLogger(null);
        gc_collect_cycles();
    
        $tools = $em->createQuery("select lex.toolIdentifier, lex.toolId from CarmenApiBundle:LexTool lex")->getArrayResult();
        $tmp = array();
        foreach ($tools as $tool){
            $tmp[$tool['toolIdentifier']] = $tool["toolId"];
        }
        $tools = $tmp;
         
    
        try {
    
            $this->addValueToMapToolAttribute($databaseMap, BD_TOOL_PAN, $oMap->getMetadata("PANORAMIQUE"), "ON|simple|advanced", $oMap->getMetadata("PANORAMIQUE"));
            $this->addValueToMapToolAttribute($databaseMap, BD_TOOL_ZOOM, $oMap->getMetadata("ZOOM"), "ON|simple|advanced", $oMap->getMetadata("ZOOM"));
            $this->addValueToMapToolAttribute($databaseMap, BD_TOOL_FITALL, $oMap->getMetadata("FITALL"), "ON|simple|advanced", $oMap->getMetadata("FITALL"));
            $this->addValueToMapToolAttribute($databaseMap, BD_TOOL_MEASURE, $oMap->getMetadata("OUTIL_MESURE"), "ON|simple|advanced", $oMap->getMetadata("OUTIL_MESURE"));
            $this->addValueToMapToolAttribute($databaseMap, BD_TOOL_SYMBOLOGY, $oMap->getMetadata("PRESENTATION_SYMBOLOGIE_COUCHE"));
            $this->addValueToMapToolAttribute($databaseMap, BD_TOOL_CONTEXT, $oMap->getMetadata("TOOL_CONTEXT"), "ON|simple|advanced", $oMap->getMetadata("TOOL_CONTEXT"));
            $this->addValueToMapToolAttribute($databaseMap, BD_TOOL_GEOBOOKMARK, $oMap->getMetadata("TOOL_GEOBOOKMARK"));
            $this->addValueToMapToolAttribute($databaseMap, BD_TOOL_ADDLAYER, $oMap->getMetadata("AJOUT_COUCHE_OGC"));
            //$this->addValueToMapToolAttribute($databaseMap, 9, $oMap->getMetadata("TELECHARGEMENT"));
            $this->addValueToMapToolAttribute($databaseMap, BD_TOOL_EXPORTIMG, $oMap->getMetadata("SAUVEGARDE_CARTE_FORMAT_IMAGE"), "ON|simple|advanced", $oMap->getMetadata("SAUVEGARDE_CARTE_FORMAT_IMAGE"));
            $this->addValueToMapToolAttribute($databaseMap, BD_TOOL_EXPORTPDF, $oMap->getMetadata("IMPRESSION_PDF"), "ON|simple|advanced|predefinedModels|layoutModels", $oMap->getMetadata("IMPRESSION_PDF"));
            $this->addValueToMapToolAttribute($databaseMap, BD_TOOL_PRINT, $oMap->getMetadata("IMPRESSION_NAVIGATEUR"), "ON|simple|advanced", $oMap->getMetadata("IMPRESSION_NAVIGATEUR"));
            //$this->addValueToMapToolAttribute($databaseMap, 13, $oMap->getMetadata("TOOL_TOOLTIPS"));
            $this->addValueToMapToolAttribute($databaseMap, BD_TOOL_INFO, $oMap->getMetadata("INFORMATION"), "ON|simple|advanced", $oMap->getMetadata("INFORMATION"));
            $this->addValueToMapToolAttribute($databaseMap, BD_TOOL_BUFFER, $oMap->getMetadata("TOOL_BUFFER"), "ON|simple|advanced", $oMap->getMetadata("TOOL_BUFFER"));
            $this->addValueToMapToolAttribute($databaseMap, BD_TOOL_QUERYATTRIBUTES, $oMap->getMetadata("TOOL_REQUEST"), "ON|simple|advanced", $oMap->getMetadata("TOOL_QUERYATTRIBUTES"));
            $this->addValueToMapToolAttribute($databaseMap, BD_TOOL_SCALEIMG, $oMap->scalebar->status, MS_ON);
            $this->addValueToMapToolAttribute($databaseMap, BD_TOOL_SCALETXT, $oMap->getMetadata("ADDSCALE_ASTEXTE"), "0");
            $this->addValueToMapToolAttribute($databaseMap, BD_TOOL_SCALE, $oMap->getMetadata("ECHELLE"));
            $this->addValueToMapToolAttribute($databaseMap, BD_TOOL_ANNOTATION, $oMap->getMetadata("TOOL_ANNOTATION"), "ON|simple|advanced", $oMap->getMetadata("TOOL_ANNOTATION"));
            /*//NOT USED IN PRODIGE
             $this->addValueToMapToolAttribute($databaseMap, 20, $oMap->getMetadata("TOOL_POINT_ANNOTATION"));
            $this->addValueToMapToolAttribute($databaseMap, 21, $oMap->getMetadata("TOOL_LINE_ANNOTATION"));
            $this->addValueToMapToolAttribute($databaseMap, 22, $oMap->getMetadata("TOOL_POLYGON_ANNOTATION"));
            $this->addValueToMapToolAttribute($databaseMap, 23, $oMap->getMetadata("TOOL_TEXT_ANNOTATION"));
            $this->addValueToMapToolAttribute($databaseMap, 24, $oMap->getMetadata("TOOL_SNAPPING"));
            $this->addValueToMapToolAttribute($databaseMap, 25, $oMap->getMetadata("TOOL_MOVE_ANNOTATION"));
            $this->addValueToMapToolAttribute($databaseMap, 26, $oMap->getMetadata("TOOL_MODIFY_ANNOTATION"));
            $this->addValueToMapToolAttribute($databaseMap, 27, $oMap->getMetadata("TOOL_STYLES_ANNOTATION"));
            $this->addValueToMapToolAttribute($databaseMap, 28, $oMap->getMetadata("TOOL_ATTRIBUT_ANNOTATION"));
            $this->addValueToMapToolAttribute($databaseMap, 29, $oMap->getMetadata("TOOL_EXPORT_ANNOTATION"));
            $this->addValueToMapToolAttribute($databaseMap, 30, $oMap->getMetadata("TOOL_BUFFER_FROM_SELECTION_ANNOTATION"));
            $this->addValueToMapToolAttribute($databaseMap, 31, $oMap->getMetadata("TOOL_BUFFER_ALL_ANNOTATION"),"ON", $oMap->getMetadata("TOOL_BUFFER_ALL_VALUE_ANNOTATION"));
            $this->addValueToMapToolAttribute($databaseMap, 32, $oMap->getMetadata("TOOL_ANNOTATION"));*/
    
            /*if($oMap->getMetadata("TOOL_ATTRIBUT_VALUE_ANNOTATION")!=""){
             $mapAnnotationAttribute = new \Carmen\ApiBundle\Entity\MapAnnotationAttribute();
            $mapAnnotationAttribute->setMap($databaseMap);
            $mapAnnotationAttribute->setAttributeName($oMap->getMetadata("TOOL_ATTRIBUT_VALUE_ANNOTATION"));
            $mapAnnotationAttribute->setAttributeRank(0);
            // persist entity to database
            $em->persist($mapAnnotationAttribute);
            $em->flush();
            }*/
    
        } catch (\Exception $ex) {
            $output->writeln(' Error while migrating mapTool of map called "'.$oMap->name.': '. $ex.' !</error>');
    
    
        }
    }
    
    
    protected function migrateMapFavoriteArea($oMap, $databasemap, OutputInterface $output)
    {
        $conn = $this->getConnection();
        $em   = $this->getManager();
        $em->getConnection()->getConfiguration()->setSQLLogger(null);
        gc_collect_cycles();
    
        try {
    
            $favoriteAreaString = $oMap->getMetadata("zones_favorites");
    
            if ($favoriteAreaString != "") {
                $json = json_decode(str_replace("'", "\"", $favoriteAreaString));
                if($json){
                    foreach($json as $key => $area){
                        $favoriteArea = new \Carmen\ApiBundle\Entity\FavoriteArea();
                        //TODO convert in WGS84 ?
                        $lc = $area->BoundingBox->LowerCorner;
                        $tabLc = explode(" ", $lc);
    
                        $uc = $area->BoundingBox->UpperCorner;
                        $tabUc = explode(" ", $uc);
                        $favoriteArea->setMap($databasemap)
                        ->setAreaName($area->name)
                        ->setAreaXmin($tabLc[0])
                        ->setAreaYmin($tabLc[1])
                        ->setAreaXmax($tabUc[0])
                        ->setAreaYmax($tabUc[1]);
                        $em->persist($favoriteArea);
                        $em->flush();
                    }
    
                }
                 
            }
    
    
    
    
        } catch (\Exception $ex) {
            $output->writeln(' Error while migrating mapLocator of map called "'.$oMap->name.': '. $ex.' !</error>');
        }
    }
    
    
    
    protected function migrateMapLocator($oMap, $databasemap, OutputInterface $output)
    {
        $conn = $this->getConnection();
        $em   = $this->getManager();
        $em->getConnection()->getConfiguration()->setSQLLogger(null);
        gc_collect_cycles();
    
        try {
            $tabLocatorsObj = array();
            $mapLocator_localisation = $oMap->getMetadata("LOCALISATION");
    
            if ($mapLocator_localisation == "ON") {
                $mapLocator_localisation_config = $oMap->getMetadata("LOCALISATION_CONFIG");
                if($mapLocator_localisation_config!=""){
                    $array_configs = explode (";", $mapLocator_localisation_config);
                    $rank = 0;
                    foreach ($array_configs as $config) {
    
                        $mapLocator = new \Carmen\ApiBundle\Entity\MapLocator();
                        //$mapLocator->setMap($databasemap);
    
                        $array_config = explode("|", $config);
    
                        $mapLocator->setLocatorCriteriaRank($rank);
                        $rank ++;
    
                        $name = $array_config[0];
                        $mapLocator->setLocatorCriteriaName($name);
    
                        $layer_name = $array_config[1];
    
                        $layer = $em->getRepository('CarmenApiBundle:Layer')->findBy(array('map'=>$databasemap->getMapId(),'layerMsname'=>$layer_name) );
    
                        if($layer && count($layer)>0){
                            $layer_id = $layer[0]->getLayerId();
                            $mapLocator->setLocatorCriteriaLayerId($layer_id);
                        }
    
                        $locator_criteria_field_id = $array_config[2];
                        $mapLocator->setLocatorCriteriaFieldId($locator_criteria_field_id);
    
                        $locator_criteria_field_name = $array_config[3];
                        $mapLocator->setLocatorCriteriaFieldName($locator_criteria_field_name);
    
                        //TODO TREATMENT
                        /*if(isset($array_config[4])){
                         $locator_criteria_visibility = $array_config[4];
                        $mapLocator->setLocatorCriteriaVisibility($locator_criteria_visibility);
                        }*/
    
                        if (isset($array_config[5]) && isset($tabLocatorsObj[$array_config[5]])) {
                            $locator_criteria_related_field = $array_config[6];
                            $mapLocator->setLocatorCriteriaRelated($tabLocatorsObj[$array_config[5]]);
                            $mapLocator->setLocatorCriteriaRelatedField($locator_criteria_related_field);
                        }
                        //register mapLocatorObj to be identified by rank when whe add related criteria
                        $tabLocatorsObj[$rank] = $mapLocator;
                         
                        // persist entity to database
                        $em->persist($mapLocator);
                        $databasemap->addLocators($mapLocator);
                        $em->persist($databasemap);
                        $em->flush();
                    }
                }
                 
            }
    
    
    
    
        } catch (\Exception $ex) {
            $output->writeln(' Error while migrating mapLocator of map called "'.$oMap->name.': '. $ex.' !</error>');
        }
    }
    
    
    protected function addGroupToMapTree($thismaptree, $groupToAdd ) {
    	array_push($thismaptree, $groupToAdd);
    }
    
    protected function addElementToGroup($parentGroup, $elementToAdd ) {
    	$elementToAdd->node_parent = $parentGroup->node_id;
    }
    
    protected function arrayFromArboGroupeString ($thisstring){
    	$temp_string = rtrim($thisstring, "]");
    
    	$temp_array_allstring = explode ("[",$temp_string);
       	$temp_array_names = explode("|", $temp_array_allstring[0]);
       	
       	$thisNodeOpenIndicator = '-';
       	if(count($temp_array_allstring) > 1) {
       		$thisNodeOpenIndicator = $temp_array_allstring[1];
       	}

       	$thisLongname = $temp_array_names[0];
       	$thisShortname = '';
       	if(count($temp_array_names) > 1) {
       		$thisShortname = $temp_array_names[1];
       	}
       	
    	$arrayToReturn = array(
    	  "longname" => $thisLongname,
    	  "shortname" => $thisShortname,
    	  "node_id" => "", 
    	  "node_parent"=>"",
    	  "pos" => 0,
    	  "isLayer"=> FALSE,
    	  "nodeIsOpened" => ($thisNodeOpenIndicator == "+"),
    	  "subElements" => array()
    	);

    	return $arrayToReturn;
    }
    
    
    protected function compareFunction($a, $b){
        return $a['pos'] > $b['pos'];
    }
    protected function exploreAndMigrateGroup($group, $database_map, $parentNodeID) {	
    	
      
        
    	$em   = $this->getManager();
    	$em->getConnection()->getConfiguration()->setSQLLogger(null);
    	gc_collect_cycles();
    	//sorting all subelements of group by " (even group has a pos ; it is the pos of the last layer added in it)
    	$thisGroupIsSortedByPos = FALSE;
    
    	while (!$thisGroupIsSortedByPos && count($group["subElements"])>=2) {
    		$thisGroupIsSortedByPos = TRUE;
    		for ($j = 0; $j<count($group["subElements"])-2 ; $j++ ){
    			if ($group["subElements"][$j]["pos"]>$group["subElements"][$j+1]["pos"]){
    				$thisGroupIsSortedByPos = FALSE;
    				$temp = $group["subElements"][$j];
    				$group["subElements"][$j] = $group["subElements"][$j+1];
    				$group["subElements"][$j+1] = $temp;
    			}
    		}
    	}
    
    	//putting the right "order" (by ordering pos first) 
    	usort($group["subElements"],  array($this,'compareFunction'));
    	for ($k = 0; $k<count($group["subElements"]); $k++) {
    	    $group["subElements"][$k]["pos"] = $k;
    	}
    	
    	
    
        foreach($group["subElements"] as $element) {
        
        		$maptree_database = new \Carmen\ApiBundle\Entity\MapTree();
        		$maptree_database->setNodeParent($parentNodeID);
        		$maptree_database->setMap($database_map);
        		$maptree_database->setNodeOpened($element["nodeIsOpened"]);
        		$maptree_database->setNodeIsLayer($element["isLayer"]);
        		$maptree_database->setNodePos($element["pos"]);
        
        
        		$maptreeprint_database = new \Carmen\ApiBundle\Entity\MapTreePrint();
        		$maptreeprint_database->setNodeParent($parentNodeID);
        		$maptreeprint_database->setMap($database_map);
        		$maptreeprint_database->setNodeOpened($element["nodeIsOpened"]);
        		$maptreeprint_database->setNodeIsLayer($element["isLayer"]);
        		$maptreeprint_database->setNodePos($element["pos"]);
        		
        		if ($element["isLayer"]) {
        		    if(is_int($element["node_id"])){
            		    $maptree_database->setId($element["node_id"]);
            		    $em->persist($maptree_database);
            		    $em->flush();
            		     
            		    $maptreeprint_database->setId($element["node_id"]);
            		    $em->persist($maptreeprint_database);
            		    $em->flush();
        		    }
        		     
        		} else {// it is a group
        		     
        		    $group_database = new \Carmen\ApiBundle\Entity\MapGroup();
        		    $group_database->setMap($database_map);
        		    $group_database->setGroupName($element["longname"]);
        		    $group_database->setGroupIdentifier($element["shortname"]);
        		    $em->persist($group_database);
        		    $em->flush();
        		
        		    $thisGroupeDatabaseId = $group_database->getGroupId();
        		
        		    $maptree_database->setId($thisGroupeDatabaseId);
        		    $em->persist($maptree_database);
        		    $em->flush();
        		    	
        		    $maptreeprint_database->setId($thisGroupeDatabaseId);
        		    $em->persist($maptreeprint_database);
        		    $em->flush();
        		     
        		    $this->exploreAndMigrateGroup($element, $database_map, $thisGroupeDatabaseId);
        		}
    		}
		}
    




    protected function migrateGroups($oMap, $database_map, $correspondance_array, OutputInterface $output)
		{
		    $conn = $this->getConnection();
		    $em   = $this->getManager();
		    $em->getConnection()->getConfiguration()->setSQLLogger(null);
		    gc_collect_cycles();
		
		
		    try {
		        $numLayers = $oMap->numlayers;
		        $mapTree = array("root"=>array());
		
		        //construction of tree
		        for($i = 0; $i < $numLayers; $i ++) {
		
		            $thisLayer = $oMap->getLayer($i);
		            //si GI_ARBO_ORDRE n'est pas numeric, l'arbre risque d'être non totalement valide
		            $layer_pos = is_numeric($thisLayer->getMetadata("GI_ARBO_ORDRE")) ? $thisLayer->getMetadata("GI_ARBO_ORDRE") : 0;
		
		
		            $layerInGroup = !empty($thisLayer->getMetadata("GI_ARBO_GROUP"));
		
		            // we create the layer element
		            $newLayerElement = array (
		                "node_id" => $correspondance_array[$i],
		                "node_parent" => "",
		                "pos" => $layer_pos,
		                "nodeIsOpened" => FALSE,
		                "isLayer"=>TRUE,
		            );
		
		
		            if (!$layerInGroup) { // if the layer has no "group" specified in the map file
		                $mapTree["root"][] = $newLayerElement; //  must be added directly to the root
		                ksort($mapTree);
		            }
		            else { // else, the layer has a "group" specified
		
		                $groups = explode("/",$thisLayer->getMetadata("GI_ARBO_GROUP") ); // we get the info of groups for this layer
		                $nbGroups = count($groups); // get the number of groups (at least one because the case zero already treated)
		                $parentNode =& $mapTree["root"];
		                foreach($groups as $index=>$group){
		                    if ( $index==$nbGroups-1 )
		                        $parentNode[$group][] = $newLayerElement;
		                    else {
		                        if ( !isset($parentNode[$group]) ) $parentNode[$group] = array();
		                        $parentNode =& $parentNode[$group];
		                    }
		                }
		            }
		             
		        }
		
		        $fnGetLayerPos = function($tree) use (&$fnGetLayerPos) {
		            $layerPos = 100000;
		            foreach ($tree as $node){
		                if ( isset($node["isLayer"]) && $node["isLayer"] ){
		                    $layerPos = min($layerPos, $node["pos"]);
		                } else {
		                    $layerPos = min($layerPos, $fnGetLayerPos($node));
		                }
		            }
		            return $layerPos;
		        };
		        $fnReadTree = function(&$mapTreeNodes, $tree, $depth=0, $parent="") use(&$fnReadTree, $fnGetLayerPos){
		
		            foreach ($tree as $group=>$nodes){
		                if ( isset($nodes["isLayer"]) && $nodes["isLayer"] ){
		                    $layerPos = $nodes["pos"];
		                    $nodes["node_parent"] = $parent;
		
		                    $mapTreeNodes[$depth."_".$layerPos."_l"] = $nodes;
		                } else if ( $group!=="root" ) {
		                    $group = $this->arrayFromArboGroupeString($group);
		                    $layerPos = $fnGetLayerPos($nodes);
		                    $group["pos"] = $layerPos;
		                    $group["node_parent"] = $parent;
		                    $thisPkey = $depth."_".$layerPos."_g";
		                    $mapTreeNodes[$thisPkey] = $group;
		
		                    $fnReadTree($mapTreeNodes, $nodes, $depth+1, $thisPkey);
		                } else {
		                    $fnReadTree($mapTreeNodes, $nodes, $depth+1, "");
		                }
		            }
		
		        };
		
		        $tmp = array();
		        $fnReadTree($tmp, $mapTree);
		        uksort($tmp, function($keyA, $keyB){
		            $keyA = explode("_", $keyA);
		            $keyB = explode("_", $keyB);
		            if ( intval($keyA[0]) == intval($keyB[0]) ){
		                if ( intval($keyA[1]) == intval($keyB[1]) ){
		                    return ( $keyA[2] < $keyB[2] ? -1 : 1);
		                }
		                return ( intval($keyA[1]) < intval($keyB[1]) ? -1 : 1);
		            }
		            return ( intval($keyA[0]) < intval($keyB[0]) ? -1 : 1);
		        });
		        $mapTree = $tmp;
		         
		        // MAP TREE IS CONSTRUCTED
		        // NOW WE INSERT IT IN DATABASE
		         
		
		
		        //sorting all subelements of maptree by " (even group has a pos ; it is the pos of the last layer added in it)
		        /*$mapTreeRootIsSortedByPos = FALSE;
		         while (!$mapTreeRootIsSortedByPos) {
		        $mapTreeRootIsSortedByPos = TRUE;
		        for ($j = 0; $j<count($mapTree)-1 ; $j++ ){
		        if ($mapTree[$j]["pos"]>mapTree[$j+1]["pos"]){
		        $mapTreeRootIsSortedByPos = FALSE;
		        $temp = $mapTree[$j];
		        $mapTree[$j] = $mapTree[$j+1];
		        $mapTree[$j+1] = $temp;
		        }
		        }
		        }
		
		        //putting the right "ordre"
		        for ($k = 0; $k<count($mapTree); $k++) {
		        $mapTree[$k]["pos"] = $k;
		        }*/
		
		
		        // now the arborescence is done, we put it on the database
		        foreach ($mapTree as $key=>$mapTreeElement) {
		            $parentNodeID = (isset($mapTree[$mapTreeElement["node_parent"]]) ? $mapTree[$mapTreeElement["node_parent"]]["node_id"] : null); //TODO parentNode pour racine ?
		
		            $maptree_database = new \Carmen\ApiBundle\Entity\MapTree();
		            $maptree_database->setMap($database_map);
		            $maptree_database->setNodeParent($parentNodeID);
		            $maptree_database->setNodeIsLayer($mapTreeElement["isLayer"]);
		            $maptree_database->setNodeOpened($mapTreeElement["nodeIsOpened"]);
		            $maptree_database->setNodePos($mapTreeElement["pos"]);
		
		
		             
		            $maptreeprint_database = new \Carmen\ApiBundle\Entity\MapTreePrint();
		            $maptreeprint_database->setMap($database_map);
		            $maptreeprint_database->setNodeParent($parentNodeID);
		            $maptreeprint_database->setNodeIsLayer($mapTreeElement["isLayer"]);
		            $maptreeprint_database->setNodeOpened($mapTreeElement["nodeIsOpened"]);
		            $maptreeprint_database->setNodePos($mapTreeElement["pos"]);
		
		
		            if ($mapTreeElement["isLayer"]) {
		            $maptree_database->setId($mapTreeElement["node_id"]);
		            $em->persist($maptree_database);
		            $em->flush();
		
		            $maptreeprint_database->setId($mapTreeElement["node_id"]);
		            $em->persist($maptreeprint_database);
		            $em->flush();
		
		            } else {// it is a group
		
		            $group_database = new \Carmen\ApiBundle\Entity\MapGroup();
		            $group_database->setMap($database_map);
		            $group_database->setGroupName($mapTreeElement["longname"]);
		                $em->persist($group_database);
		                $em->flush();
		                 
		                $mapTree[$key]["node_id"] = $node_id = $group_database->getGroupId();
		
		                $maptree_database->setId($node_id);
		                $em->persist($maptree_database);
		                $em->flush();
		                 
		                $maptreeprint_database->setId($node_id);
		                $em->persist($maptreeprint_database);
		                $em->flush();
		
		                //$this->exploreAndMigrateGroup($mapTreeElement, $database_map, $node_id);
		            }
		            }
		            } catch (\Exception $ex) {
		            $output->writeln('<error>problem while treating maptree '.$oMap->name.'  '. $ex.' !</error>');
		            }
		            }
		
	    		
 
    
    
    
    
    
 
     /**
      * 
      * @param type $databaseMap
      * @param type $tool_id
      * @param type $value
      * @param type $allowed_value
      * @param type $toolValue
      */ 
    protected function addValueToMapToolAttribute($databaseMap, $tool_id, $value, $allowed_value = "ON", $toolValue="") {
      $tabAllowedValue = explode("|", $allowed_value);
    	if(in_array($value, $tabAllowedValue)) {
    		$em   = $this->getManager();
    		$tool = $em->getRepository('CarmenApiBundle:LexTool')->find($tool_id);
    		$mapTool = new \Carmen\ApiBundle\Entity\MapTool();
    		$mapTool->setMap($databaseMap);
    		$mapTool->setTool($tool);
        
    		if($toolValue!=""){
    			$mapTool->setMaptoolValue($toolValue);
    		}
    		// persist entity to database
    		$em->persist($mapTool);
    		$em->flush();
    	}
    }
    
    /**
     * GetExtensionName - Renvoie l'extension d'un fichier
     * @param $file string  Nom du fichier
     * @param $dot  boolean Avec le point, true/false
     */
    protected function GetExtensionName($file, $dot=false)
    {
        if ($dot == true) {
            $Ext = strtolower(substr($file, strrpos($file, '.')));
        } else{
            $Ext = strtolower(substr($file, strrpos($file, '.') + 1));
        }
        return $Ext;
    }
    
    /**
     * Returns the default doctrine entity manager.
     *
     * @return \Doctrine\ORM\EntityManager Doctrine entity manager.
     */
    protected function getManager()
    {
        return $this->getContainer()->get('doctrine')->getManager();
    }
    
    /**
     * Returns the default DBAL PDO Connection.
     *
     * @return \Doctrine\DBAL\Driver\PDOConnection
     */
   /* protected function getConnection()
    {
        return $this->getContainer()->get('doctrine')->getConnection();
    }*/
    
   /**
	 * Get Doctrine connection
	 * 
	 * @param string $connection_name        	
	 * @param string $schema        	
	 * @return \Doctrine\DBAL\Connection
	 */
  	protected function getConnection($connection_name =BaseController::CONNECTION_PRODIGE  , $schema = "carmen") {
      $conn = $this->getContainer ()->get ( "doctrine" )->getConnection ( $connection_name );
      $conn->exec ( 'set search_path to ' . $schema );

      return $conn;
	  }
    /**
     *
     * @param string $schema        	
     * @return \Doctrine\DBAL\Connection
     */
    protected function getCatalogueConnection($schema = "catalogue") {
      return $this->getConnection ( BaseController::CONNECTION_CATALOGUE, $schema );
    }
    
    
    
    
    

}