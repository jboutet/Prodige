#!/bin/bash

######################################################################################
##
## script d'update de l'application Prodige
## Mise à jour corrective de la version 4.0 de PRODIGE
## Un environnement ssh est utile si l'installation se déroule à distance
##
######################################################################################


SCRIPTDIR=`dirname $(readlink -f $0)`
DATE=`date '+%y%m%d'`
NUM="4.0.3"
NUMDATE="20170616"
NUM_old="4.0.2"
VERSIONTXT="/home/sites/prodigecatalogue/web/version.txt"
echo
echo "Mise à jour Prodige version $NUM"

PATCH_NAME="update_prodige${NUM}_$NUMDATE.tar.gz"
PATCH_TAG="prodige_${NUM}"
PATCH_DESC="Patch correctif prodige_${NUM}
- prodigeadmincarto : Correction Dictionaries.js
"

######################################################################################
error_exit()
{
echo "Annulation du script : $1" && exit 1
}
check()
{
case "$2" in
    var) [ -z $1 ] && error_exit "$3";;
    file) [ -f $1 ] || error_exit "Le fichier $1 n'existe pas";;
    dir) [ -d $1 ] || error_exit "Le répertoire $1 n'existe pas";;
esac
}
######################################################################################

requisites()
{
if [ "`egrep "^Patch correctif prodige_${NUM}$" $VERSIONTXT`" ]
  then echo "Version Prodige déjà égale à ${NUM} : fin du patch ";exit
fi
if [ "`egrep "^Patch correctif prodige_${NUM_old}$" $VERSIONTXT`" ]
  then echo "Version Prodige supérieure ou égale à ${NUM_old} : OK ";echo
  else echo "Prodige est en version inférieure à ${NUM_old}, fin du patch"; exit
fi
}

requisites_debian()
{
apt-get -qq update
if [ "$?" -ne 0 ] 
  then echo "Problème avec le mirroir Debian, fin du script" && exit
fi
}



get_vars()
{
# WWWURL= www-domain.tld ou www.domain.tld ou www-xyz.domain.tld ou www.xyz.domain.tld
WWWURL=`awk -F "\"" '/PRO_GEONETWORK_URLBASE/ { print $2 }' /home/sites/editables_parameters.yml | awk -F "/" '{ print $3 }'`
# DNS_SUFFIX= -domain.tld ou .domain.tld ou -xyz.domain.tld ou .xyz.domain.tld
DNS_SUFFIX=`awk -F "\"" '/PRODIGE_URL_ADMINSITE/ { print $2 }' /home/sites/editables_parameters.yml | sed 's|https://adminsite||'`
# DOMAIN= domain.tld ou xyz.domain.tld
DOMAIN=`echo $DNS_SUFFIX | sed 's/^-\?[a-z0-9]*\.//'`
# DNS_PREFIX_SEP = . ou -
DNS_PREFIX_SEP=`echo $WWWURL | sed 's/\(www\)\([\.-]\).*$/\2/'`
user_bdd=`awk '/catalogue_user/ { print $2 }' /home/sites/editables_parameters.yml`
pass_bdd=`awk '/catalogue_password/ { print $2 }' /home/sites/editables_parameters.yml`
#sur prodige CATALOGUE="CATALOGUE" / puis CATALOGUE=alk_respire 
DB=`awk '/catalogue_name/ { print $2 }' /home/sites/editables_parameters.yml`
SSL_PWD=`cat /home/sites/editables_parameters.yml| awk '/ldap_password/ { print $NF }'`
LDAP_PWD="${SSL_PWD}"
for param in DNS_SUFFIX DOMAIN DNS_PREFIX_SEP user_bdd pass_bdd DB SSL_PWD LDAP_PWD; do
     check "${!param}" "var" "Variable $param vide"
done
}

######################################################################################

copie_source()
{
echo "Copie des fichiers"
cd "${SCRIPTDIR}/patch-prodige4-$NUMDATE"
chown -R www-data.www-data prodige*
chmod -R 750 prodige*

for dir in `find -mindepth 1 -maxdepth 1 -type d | sed 's/^\.\///' | egrep "^prodige.+$|^cas|^vendor"`
  do
    echo -e "\tCopie de ${dir}"
    rsync -a ${dir} /home/sites/
  done
[ -d prodige ] && echo -e "\tCopie de prodige" && rsync -a prodige /home/
echo

}

patch_sql()
{
# todo automatiser avec tests existance fichiers
echo "Mise à jour des bases"
echo "localhost:5432:PRODIGE:${user_bdd}:${pass_bdd}" > /root/.pgpass
echo "localhost:5432:${DB}:${user_bdd}:${pass_bdd}" >> /root/.pgpass
chmod 600 /root/.pgpass
#echo -e "\t${DB}"
#psql -U ${user_bdd} -d ${DB} -h localhost < "/home/sites/prodigecatalogue/src/ProdigeCatalogue/AdminBundle/Resources/sql/upgrade_${NUM}.sql"
echo -e "\tPRODIGE"
psql -U ${user_bdd} -d PRODIGE -h localhost < "/home/sites/prodigeadmincarto/carmenwsback/src/Carmen/ApiBundle/Resources/sql/upgrade_${NUM}.sql"
rm /root/.pgpass
echo
}

migrate_php()
{
# todo automatiser avec tests existance fichiers
echo "Migration"
cd /home/sites/prodigecatalogue/ && app/console prodige_${NUM}:migration
cd /home/sites/prodigeadmincarto/ && app/console prodige_${NUM}:migration
echo
}

debian()
{
echo
}
######################################################################################

version()
{
# todo un seul fichier dans /home/sites/prodigecatalogue/web/version.txt
echo "Génération du fichier version.txt"
echo -e "Patch ${PATCH_NAME} (${PATCH_TAG}) applied `date`\n${PATCH_DESC}\n-----------------------------------------------" > ./current_version.txt
[ -f "${VERSIONTXT}" ] || touch "${VERSIONTXT}"
ed -s  "${VERSIONTXT}" <<< $'0 r ./current_version.txt\n,w'
rm ./current_version.txt
}

uprod()
{
#cd /home/sites
#bash ./uprod.sh
cd /home/sites/prodigeadmincarto/carmenwsback
bash bin/uprod.sh
}

rights()
{
[ -f /home/rights.sh ] && [ -z "`ps ax | grep "/home/rights.sh" | grep -v grep`" ] && /home/rights.sh > /dev/null 2>&1 &
}



requisites
#get_vars
#requisites_debian
#debian
copie_source
#patch_sql
#migrate_php
version
uprod
rights


[ -f /root/.pgpass ] && rm /root/.pgpass


echo
echo "Fin de la mise à jour"
echo

exit 0
