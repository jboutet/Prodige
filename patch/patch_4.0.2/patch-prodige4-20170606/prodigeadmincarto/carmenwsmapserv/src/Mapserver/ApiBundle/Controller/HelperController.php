<?php

namespace Mapserver\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Head;
use FOS\RestBundle\Controller\Annotations\Link;
use FOS\RestBundle\Controller\Annotations\Patch;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Unlink;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations\QueryParam; // GET params
use FOS\RestBundle\Controller\Annotations\RequestParam; // POST params
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\JsonResponse;
use Mapserver\ApiBundle\Exception\MapserverException;
use Mapserver\ApiBundle\Library\JenksGenerator;
/**
 * Helper to generate layer classes according to specified symbology 
 * @author alkante <support@alkante.com>
 * 
 * 
 * @Route("/helpers")
 */
class HelperController extends MapfileController 
{
    const GENERATION_DISTRIBUTION = "distribution";
    const GENERATION_SYMBOLOGY = "symbology";
    
    const GRADUATE_COLOR  = "COLOR";
    const GRADUATE_SYMBOL = "SYMBOL";
    
    const CLASSDISTRIBUTION_SAMEAMPLITUDE = 'SAMEAMPLITUDE';
    const CLASSDISTRIBUTION_SAMECOUNT     = 'SAMECOUNT';
    const CLASSDISTRIBUTION_HOMOGENEOUS   = 'HOMOGENEOUS';
    
    const SPREADSHEET_EXTENSIONS   = 'ODS,XLS,XLSX';
    
    /**
     * @param layerObj $layerObj   object layer from mapscript library
     * 
     * @throws MapserverException
     * @throws \Exception
     * @return unknown
     */
    protected function returnClasses(\layerObj $layerObj, array $distribution=array(), $bGetArray=false) 
    {
        $data = array();
        for ($iClass=0; $iClass<$layerObj->numclasses; $iClass++ ){
            $oClass = $layerObj->getClass($iClass);
            $style = $oClass->getStyle(0);
            $symbol = "Vide";
            switch($layerObj->type){
                case MS_LAYER_POINT : 
                    $symbol = "Cercle"; 
                    if ( !$style->size || $style->size<0 ) $style->set("size", 8);
                break;
                case MS_LAYER_LINE : 
                	  $symbol = "Carre"; 
                    if ( !$style->width || $style->width<0 ) $style->set("width", 1);
                break;
                case MS_LAYER_POLYGON: 
                    $symbol = "Carre"; 
                    if ( !$style->width || $style->width<0 ) $style->set("width", 8);
                break;
            }
            if ( !$style->symbolname ){
            $style->set("symbolname", $symbol);
            }
            
            $data[] = $this->convertClassObj($oClass, $iClass, true);
        }
        $result = array("classes"=>$data, "distribution"=>$distribution);
        return ($bGetArray ? $result : $this->formatJsonSuccess($result));
    }
    
    /**
     * Construct a symbology of type UNIQUE SYMBOL for the layer 
     * @Get("/{generation_type}/{mapfile}/{layer}/{analysetype}/{geometrytype}", requirements={"analysetype"="PIECHART|BARCHART", "generation_type"="symbology|distribution"})
     * 
     * @QueryParam(name="directory", description="REFERENCE DIRECTORY", nullable=false)
     *
     * @param string $mapfile   mapfile name without extension
     * @param string $layer     layer name
     * 
     * @ApiDoc(
     *   section="Layer",
     *   description="get Layer information",
     *   requirements={
     *     {
     *       "name"="file",
     *       "dataType"="text",
     *       "requirement"="\w/\d+",
     *       "description"="mapfile name without extension"
     *     },
     *     {
     *       "name"="layer",
     *       "dataType"="text",
     *       "requirement"="\w/\d+",
     *       "description"="layer name"
     *     },
     *   },
     * )
     */
    public function symbologyChart(Request $request, ParamFetcher $paramFetcher, $analysetype, $generation_type, $mapfile, $layer, $geometrytype=null)
    {
        //reset mapserver error (to avoid warnings due to precedent errors)
        ms_ResetErrorList();
        
        try {
            if ( $generation_type==self::GENERATION_DISTRIBUTION ) {
                return $this->formatJsonSuccess(array("distribution"=>array()));
            }
            
            $directory = $paramFetcher->get("directory"); 
            
            $mapObj = $this->getMapObj($directory, $mapfile);
            $layerObj = $this->getLayerObj($mapObj, $layer);
            if ( $geometrytype && defined("MS_LAYER_".$geometrytype)){
                $layerObj->set('type', constant("MS_LAYER_".$geometrytype));
            }
            
	    	    $msStyleSizes = $request->get('msStyleSize', array());
            $layerTitle = $layerObj->getMetadata("LAYER_TITLE") ?: $layerObj->name;
        
            while ( $layerObj->numclasses>0 ){
                $layerObj->removeClass($layerObj->numclasses-1);
            }
            $iClass = 0;
            foreach ($msStyleSizes as $iClass=>$msStyleSize){
                
                if ( $iClass<$layerObj->numclasses ){
                  $oClass = $layerObj->getClass($iClass);
                } else if ( $layerObj->numclasses>0 ) {
                    $oClass = ms_newClassObj($layerObj, $layerObj->getClass(0));
                } else {
                    $oClass = $this->getClassObj($mapObj, $layerObj, $iClass, false);
                }
                if ( !$oClass ){
                    throw new MapserverException(sprintf($this->t("Unable to add a class for layer %s in mapfile %s"), $layer, $mapfile));
                }
                
                $oClass->set("name", ($msStyleSize));
                $oClass->set("title", ($msStyleSize));
                
                $style = $oClass->getStyle(0);
                $style->setbinding(MS_STYLE_BINDING_SIZE, ($msStyleSize));
                
                $style->color->setHex($this->getRandomColor());
                $style->outlinecolor->setRGB(-1, -1, -1, -1);
                $style->set("outlinewidth", 1);
                
            }
            while ( $layerObj->numclasses>$iClass+1){
            	$layerObj->removeClass($layerObj->numclasses-1);
            }
            $mapObj->save($mapfile.".tmp");
            
            return $this->returnClasses($layerObj, array());
        } catch (\Exception $exception){
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, 'Internal Server Error', $exception->getMessage());
        }
    }
    
    
    /**
     * Construct a symbology of type UNIQUE SYMBOL for the layer 
     * @Get("/{generation_type}/{mapfile}/{layer}/{analysetype}/{geometrytype}", requirements={"analysetype"="UNIQSYMBOL|PROPORTIONAL", "generation_type"="symbology|distribution"})
     * 
     * @QueryParam(name="directory", description="REFERENCE DIRECTORY", nullable=false)
     *
     * @param string $mapfile   mapfile name without extension
     * @param string $layer     layer name
     * 
     * @ApiDoc(
     *   section="Layer",
     *   description="get Layer information",
     *   requirements={
     *     {
     *       "name"="file",
     *       "dataType"="text",
     *       "requirement"="\w/\d+",
     *       "description"="mapfile name without extension"
     *     },
     *     {
     *       "name"="layer",
     *       "dataType"="text",
     *       "requirement"="\w/\d+",
     *       "description"="layer name"
     *     },
     *   },
     * )
     */
    public function symbologyUniqsymbol(Request $request, ParamFetcher $paramFetcher, $analysetype, $generation_type, $mapfile, $layer, $geometrytype=null)
    {
        //reset mapserver error (to avoid warnings due to precedent errors)
        ms_ResetErrorList();
        
        try {
            if ( $generation_type==self::GENERATION_DISTRIBUTION ) {
                return $this->formatJsonSuccess(array("distribution"=>array()));
            }
            $directory = $paramFetcher->get("directory"); 
            $mapObj = $this->getMapObj($directory, $mapfile);
            $layerObj = $this->getLayerObj($mapObj, $layer);
            if ( $geometrytype && defined("MS_LAYER_".$geometrytype)){
                $layerObj->set('type', constant("MS_LAYER_".$geometrytype));
            }
            
            while ($layerObj->numclasses > 0){
                $layerObj->removeClass(0);
            }
            $classObj = $this->getClassObj($mapObj, $layerObj, 0, false);
            
            if ( !$classObj ){
                throw new MapserverException($this->_t(sprintf("Could not find a class in the layer named %s in mapfile %s", $layer, $mapfile)));
            }
            
            $layerTitle = $request->get('layerTitle', $layerObj->getMetadata("LAYER_TITLE") ?: $layerObj->name);
            $classObj->set("name", $classObj->name ?: $layerTitle);
            $classObj->set("title", ($layerTitle));
            
            //Create the first style
            if ( $classObj->numstyles == 0 ) {
                $styleObj = ms_newStyleObj($classObj);
                $styleObj->color->setRGB(0, 0, 0);
                $styleObj->set('size', 10);
                $styleObj->set('width', 10);
            }
            //Use the first style 
            else {
                $styleObj = $classObj->getStyle(0);
            }
            // Allow only one style
        
            $iStyle = 1;
            while ($classObj->numstyles > $iStyle){
                $classObj->deleteStyle($iStyle);
            }
           
            if ( !$styleObj->symbolname ){
            switch ($layerObj->type) {
                case MS_LAYER_CHART :
                case MS_LAYER_POINT :
	                    $styleObj->set('symbolname', "cercle");
                      $styleObj->set('size', 10);
                break;
                case MS_LAYER_LINE :
                    $styleObj->set('symbol', 0);
                      $styleObj->set('width', 10);
                break;
                case MS_LAYER_POLYGON :
	                    $styleObj->set('symbolname', "Carre");
                      $styleObj->set('size', 10);
                break;
            }
            }
            $returnDistribution = array(); 
	          $classitem = $request->get("classitem"); 
            if ( $analysetype=="PROPORTIONAL" && $classitem ){
	            $distribution = $this->getLayerFieldValues($mapObj, $layerObj, $classitem, true, true);
        
	            $minValue = null;
	            $maxValue = null;
	            foreach($distribution as $item){
	            	if ( !isset($minValue) ) $minValue = $item["field"];
	            	if ( !isset($maxValue) ) $maxValue = $item["field"];
	            	$minValue = min($minValue, $item["field"]);
	            	$maxValue = max($maxValue, $item["field"]);
	            }
	            $returnDistribution = array("minValue"=>$minValue, "maxValue"=>$maxValue);
            }
            return $this->returnClasses($layerObj, $returnDistribution);
        } catch (\Exception $exception){
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, 'Internal Server Error', $exception->getMessage());
        }
    }
    
    /**
     * Construct a symbology of type UNIQUE VALUE for the layer 
     * @Get("/{generation_type}/{mapfile}/{layer}/UNIQVALUE/{geometrytype}", requirements={"generation_type"="symbology|distribution"})
     * 
     * @QueryParam(name="directory", description="REFERENCE DIRECTORY", nullable=false)
     * @QueryParam(name="classitem", description="The FIELD used as classitem", nullable=false)
     *
     * @param string $mapfile   mapfile name without extension
     * @param string $layer     layer name
     * 
     * @ApiDoc(
     *   section="Layer",
     *   description="get Layer information",
     *   requirements={
     *     {
     *       "name"="file",
     *       "dataType"="text",
     *       "requirement"="\w/\d+",
     *       "description"="mapfile name without extension"
     *     },
     *     {
     *       "name"="layer",
     *       "dataType"="text",
     *       "requirement"="\w/\d+",
     *       "description"="layer name"
     *     },
     *   },
     * )
     */
    public function symbologyUniqvalue(Request $request, ParamFetcher $paramFetcher, $generation_type, $mapfile, $layer, $geometrytype=null)
    {
        //reset mapserver error (to avoid warnings due to precedent errors)
        ms_ResetErrorList();
        
        try {
            $directory = $paramFetcher->get("directory"); 
            $classitem = $paramFetcher->get("classitem"); 
            
            if ( !$classitem ){
                throw new MapserverException(sprintf($this->_t("No classitem defined for the layer %s in mapfile %s"), $layer, $mapfile));
            }
            $mapObj = $this->getMapObj($directory, $mapfile);
            $layerObj = $this->getLayerObj($mapObj, $layer);
            if ( $geometrytype && defined("MS_LAYER_".$geometrytype)){
                $layerObj->set('type', constant("MS_LAYER_".$geometrytype));
            }
            /*
              //cas particulier des symboles proportionnels, supression temporaire de centroid
              $bCentroid  = false;
              if (strpos($oLayer->data, "centroid")!==false){
                $oLayer->set("data", str_replace( "centroid(the_geom)", "the_geom", $oLayer->data));
                $bCentroid  = true;
              }
              //rétablissement du centroide
              if ($bCentroid)
                $oLayer->set("data", str_replace( "the_geom", "centroid(the_geom)", $oLayer->data));
              */
            
            $layerTitle = $layerObj->getMetadata("LAYER_TITLE") ?: $layerObj->name;
            $layerObj->set("classitem", $classitem);
            $distribution = $this->getLayerFieldValues($mapObj, $layerObj, $classitem, true, true);
            
            if ( $generation_type==self::GENERATION_DISTRIBUTION ) {
                return $this->formatJsonSuccess(array("distribution"=>$distribution));
            }
            while ( $layerObj->numclasses>0 ){
                $layerObj->removeClass($layerObj->numclasses-1);
            }
            $iClass = 0;
            foreach ($distribution as $iClass=>$distinct){
//                 if ( $iClass>self::NB_MAX_CLASSES ){
//                     break;
//                 }
                $fieldValue = $distinct["field"];
                $fieldCount = $distinct["count"];
                
                if ( $iClass<$layerObj->numclasses ){
                  $oClass = $layerObj->getClass($iClass);
                } else if ( $layerObj->numclasses>0 ) {
                    $oClass = ms_newClassObj($layerObj, $layerObj->getClass(0));
                } else {
                    $oClass = $this->getClassObj($mapObj, $layerObj, $iClass, false);
                }
                if ( !$oClass ){
                    throw new MapserverException(sprintf($this->t("Unable to add a class for layer %s in mapfile %s"), $layer, $mapfile));
                }
                
                $oClass->set("name", $fieldValue);
                $oClass->set("title", $fieldValue);
                $this->getLogger()->debug(__METHOD__, array("distinct"=>$distinct, "fieldValue"=>$fieldValue, "count"=>$fieldCount, "expr"=>"^".$fieldValue."$"));
                $oClass->setExpression("/^".$fieldValue."$/");
                
                $style = $oClass->getStyle(0);
                $style->color->setHex($this->getRandomColor());
                $style->outlinecolor->setRGB(-1, -1, -1, -1);
                $style->set("outlinewidth", 1);
                
            }
            while ( $layerObj->numclasses>$iClass+1){
            	$layerObj->removeClass($layerObj->numclasses-1);
            }
            $mapObj->save($mapfile.".tmp");
            
            return $this->returnClasses($layerObj, $distribution);
        } catch (\Exception $exception){
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, 'Internal Server Error', $exception->getMessage());
        }
    }
    
    /**
     * Construct a symbology of type GRADUATE COLOR for the layer 
     * @Get("/{generation_type}/{mapfile}/{layer}/GRADUATE{graduate_type}/{geometrytype}", requirements={"generation_type"="symbology|distribution", "graduate_type"="COLOR|SYMBOL"})
     * 
     * @QueryParam(name="directory", description="REFERENCE DIRECTORY", nullable=false)
     * @QueryParam(name="classitem", description="The FIELD used as classitem", nullable=false)
     * @QueryParam(name="classdistribution", description="The class distribution to apply", nullable=false)
     * @QueryParam(name="classcount", description="The class count to generate", nullable=false)
     *
     * @param string $mapfile   mapfile name without extension
     * @param string $layer     layer name
     * 
     * @ApiDoc(
     *   section="Layer",
     *   description="get Layer information",
     *   requirements={
     *     {
     *       "name"="file",
     *       "dataType"="text",
     *       "requirement"="\w/\d+",
     *       "description"="mapfile name without extension"
     *     },
     *     {
     *       "name"="layer",
     *       "dataType"="text",
     *       "requirement"="\w/\d+",
     *       "description"="layer name"
     *     },
     *   },
     * )
     */
    public function symbologyGraduate(Request $request, ParamFetcher $paramFetcher, $generation_type, $mapfile, $layer, $graduate_type, $geometrytype=null)
    {
        //reset mapserver error (to avoid warnings due to precedent errors)
        ms_ResetErrorList();
        
        try {
            $directory = $paramFetcher->get("directory"); 
            $classitem = $paramFetcher->get("classitem"); 
            $classdistribution = $paramFetcher->get("classdistribution"); 
            $classcount = $paramFetcher->get("classcount");
            
            if ( !$classitem ){
                throw new MapserverException(sprintf($this->_t("No classitem defined for the layer %s in mapfile %s"), $layer, $mapfile));
            }
            $mapObj = $this->getMapObj($directory, $mapfile);
            $layerObj = $this->getLayerObj($mapObj, $layer);
            if ( $geometrytype && defined("MS_LAYER_".$geometrytype)){
                $layerObj->set('type', constant("MS_LAYER_".$geometrytype));
            }
            
            $layerObj->set("classitem", $classitem);
            
            $distribution = $this->getLayerFieldValues($mapObj, $layerObj, $classitem, true, true);
            
            if ( $generation_type==self::GENERATION_DISTRIBUTION ) {
                return $this->formatJsonSuccess(array("distribution"=>array()));
            }
            
            $limitsMax = array();
            switch($classdistribution){
            	case self::CLASSDISTRIBUTION_SAMEAMPLITUDE :
            		$limitsMax = $this->symbologyGraduate_SameAmplitude($distribution, $classcount);
            	break;
            	case self::CLASSDISTRIBUTION_SAMECOUNT :
            		$limitsMax = $this->symbologyGraduate_SameCount($distribution, $classcount);
            	break;
            	case self::CLASSDISTRIBUTION_HOMOGENEOUS :
            		$limitsMax = $this->symbologyGraduate_Homogeneous($distribution, $classcount);
            	break;
            }
            
            $propertyDistribution = array();
            $suffixe = ( $graduate_type=="COLOR" ? "color" : "size");
            $minProperty = $request->get("classminsymbol".$suffixe, null);
            $maxProperty = $request->get("classmaxsymbol".$suffixe, null);
            
            $symbolname = $request->get("classsymbol", null);
        
            if ( $classcount>0 ){
	            if ( $graduate_type=="COLOR" ){
	            	$minProperty = $minProperty ?: $this->getRandomColor();
	            	$maxProperty = $maxProperty ?: $this->getRandomColor();
	            	$minProperty = $this->getRGB($minProperty);  
	            	$maxProperty = $this->getRGB($maxProperty); 
	            	list($minRed, $minGreen, $minBlue) = $minProperty;  
	            	list($maxRed, $maxGreen, $maxBlue) = $maxProperty;  
	            	$deltaRed   = ceil(($maxRed-$minRed    )/max(1, $classcount-1));
	            	$deltaGreen = ceil(($maxGreen-$minGreen)/max(1, $classcount-1));
	            	$deltaBlue  = ceil(($maxBlue-$minBlue  )/max(1, $classcount-1));
	            } else {
	            	$minProperty = $minProperty ?: 6;
	            	$maxProperty = $maxProperty ?: 6+($classcount*2);
	            	$delta = ceil(($maxProperty-$minProperty)/max(1, $classcount-1));
	            }
            }
            
            for ($iClass=0; $iClass<$classcount; $iClass++){
            	if ( $graduate_type=="COLOR" ){
            		$propertyDistribution[$iClass]["red"] = $minRed+($iClass*$deltaRed);
            		$propertyDistribution[$iClass]["green"] = $minGreen+($iClass*$deltaGreen);
            		$propertyDistribution[$iClass]["blue"] = $minBlue+($iClass*$deltaBlue);
            		$propertyDistribution[$iClass] = array_values($propertyDistribution[$iClass]);
            	} else {
            		$propertyDistribution[$iClass] = $minProperty+($iClass*$delta);
            	}
            }
            $propertyDistribution[0] = $minProperty;
            $propertyDistribution[$classcount-1] = $maxProperty;
        
            while ($layerObj->numclasses > 0){
                $layerObj->removeClass($layerObj->numclasses-1);
            }
            $previous = "";
            for ($iClass=0; $iClass<$classcount; $iClass++){
                if ( $iClass==min($classcount, count($limitsMax)) ) $limit = "";
                else $limit = $limitsMax[$iClass];
                
                if ( $iClass<$layerObj->numclasses ){
                  $oClass = $layerObj->getClass($iClass);
                } else if ( $layerObj->numclasses>0 ) {
                    $oClass = ms_newClassObj($layerObj, $layerObj->getClass(0));
                } else {
                    $oClass = $this->getClassObj($mapObj, $layerObj, $iClass, false);
                }
                if ( !$oClass ){
                    throw new MapserverException(sprintf($this->t("Unable to add a class for layer %s in mapfile %s"), $layer, $mapfile));
                }
                if ( $classcount>1 ){
	                switch ( $iClass ){
	                	case 0 :
	                		$expressionTitle = "< ".$limit;
	                		$expression = "[{$classitem}] < {$limit}";
	                	break;
	                	case $classcount-1 :
	                		$expressionTitle = ">= ".$previous;
	                		$expression = "[{$classitem}] >= {$previous}";
	                	break;
	                	default : 
	                		$expressionTitle = "de ".$previous." à <".$limit;
	                		$expression = "[{$classitem}] >= {$previous} AND [{$classitem}] < {$limit}";
	                	break;
	                }
                } else {
                	$expressionTitle = " = ".$limit;
	                $expression = "[{$classitem}] = {$limit}";
                }
                
                $oClass->set("name", "(".$expressionTitle.")");
                $oClass->set("title", "(".$expressionTitle.")");
                
                $oClass->setExpression($expression);
                
                $style = $oClass->getStyle(0);
                if ( $graduate_type=="COLOR" ){
	                $this->callFunction($style->color, "setRGB", $propertyDistribution[$iClass]);
                $style->set("width", 1);
// 	                $style->outlinecolor->setRGB(-1, -1, -1, -1);
// 	                $style->set("outlinewidth", 0);
                } else {
	                $style->color->setHex('#000000');
	                $style->set("size", $propertyDistribution[$iClass]);
                }
                if ( !isset($symbolname) ){
                    $symbolname = ($geometrytype=="POINT" ? "Cercle" : "Carre");
                }
						      if ( is_numeric($symbolname) ){
						          $this->callSetNotRequired($style, "symbol", intval($symbolname));
						      } else {
						          $this->callSetNotRequired($style, "symbolname", $symbolname);
						      }
                
                $previous = $limit;
            }
        
            while ( $layerObj->numclasses>$classcount){
            	$layerObj->removeClass($layerObj->numclasses-1);
            }
            
            //$classes = $this->returnClasses($layerObj, array(), true);
            
            return $this->returnClasses($layerObj, $distribution);
        } catch (\Exception $exception){
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, 'Internal Server Error', $exception->getMessage());
        }
    }
    
    /**
     * Create subdirectories if mapname contains directories
     * @param $dir : the dir where to create subdirectories
     * @param $mapname : the mapname in which subdirectories are written
     */
    protected function create_subdirectories($dir, $mapname){
       $MyDirectory = opendir($dir) or die('Erreur répertoire manquant');
       $tabSubDir = explode("/", $mapname);
       for ($i = 0; $i <count($tabSubDir)-1; $i++){
         @mkdir($dir."/".$tabSubDir[$i]);
         $dir.= $tabSubDir[$i];
       }
    }
    /**
     * Generate legend icons for mapfile
     * @Post("/generate_legend/{mapfile}")
     * 
     * @param string $mapfile   mapfile name without extension
     * @QueryParam(name="directory", description="REFERENCE DIRECTORY", nullable=false)
     */
    public function generateLegendIcons(Request $request, ParamFetcher $paramFetcher, $mapfile)
    {
      $directory = $paramFetcher->get("directory");
      $mapObj = $this->getMapObj($directory, $mapfile);
      /** legend icons generation **/
      $mapName = basename($mapfile, ".map");
      $legendBaseDir = $directory . "../IHM/LEGEND/";

      //delete existing icons
      $MyDirectory = opendir($legendBaseDir) or die('Erreur répertoire manquant');
      while($Entry = @readdir($MyDirectory)) {
        if(!is_dir($legendBaseDir.'/'.$Entry)&& $Entry != '.' && $Entry != '..') {
          if(substr($Entry, 0, strlen($mapName))==$mapName){
            if (substr_count($Entry, $mapName."_layer")>0){
              unlink($legendBaseDir.'/'.$Entry);
            }
          }
        }
      }
      $this->create_subdirectories($legendBaseDir, $mapName);
      
      $mapObj->outputformat->set('imagemode', MS_IMAGEMODE_RGB);
      $mapObj->outputformat->set("name", "jpeg");
      $mapObj->outputformat->set('transparent', MS_OFF);
      $mapObj->outputformat->set("mimetype", "image/jpeg");
      $mapObj->outputformat->set("driver", "GD/JPEG");
      $mapObj->outputformat->set("extension", "jpg");
      $mapObj->selectOutputFormat("jpeg");
      $mapObj->imagecolor->setRGB(-1, -1, -1);
      
      for ($i=0; $i<$mapObj->numlayers;$i++){
        $l_msLayer = $mapObj->getLayer( $i );

        $UrlImgLegPre  = $legendBaseDir.$mapName."_".$l_msLayer->name."_";

        $UrlImgLegPost = ".jpeg";
        $iCountClass = $l_msLayer->numclasses;
        if ($iCountClass>0) {
        //if proportional symbols        change annotations to points     
          //TODO proportionnal legend in PRODIGE 4 ?
          /*if ($l_msLayer->type == MS_LAYER_ANNOTATION && $l_msLayer->getMetadata("TYPE_SYMBO")=="PROPORTIONAL"){
            $l_msLayer->set("type", MS_LAYER_POINT);
            $oClass = $l_msLayer->getClass(0); 
          }*/
          for ($j=0;$j<$iCountClass;$j++){
            $classCouche = $l_msLayer->getClass($j);
            //image
            if ( $classCouche->numstyles >0 ){
              $oStyle = $classCouche->getStyle(0);
              if ($l_msLayer->type == MS_LAYER_POINT){
                $legend_size = $oStyle->size;
                $oStyle->set("size", min($legend_size, 10));
              }
            }
            //TODO proportionnal legend in PRODIGE 4 ?  
            /*if($l_msLayer->getMetadata("TYPE_SYMBO")=="PROPORTIONAL"){
              $style = $classCouche->getStyle(0);
              $style->set("size",30);

              $legend_size = $style->size;
              $legend_scale = (isset($_REQUEST["PN_MAP_SCALE"]) ? $_REQUEST["PN_MAP_SCALE"] : 1);
              $legend_symbolscaledenom = $l_msLayer->symbolscaledenom;
              $legend_value = ($legend_size * $legend_scale / $legend_symbolscaledenom);

              $styleInternal = ms_newStyleObj($classCouche, $style);
              $styleInternal->outlinecolor->setRGB(255,255,255);
              $styleInternal->set("offsety", 5.5);
              $styleInternal->set("size", intval(intval($style->size)/1.5));
              //L'attribut outlinewidth n'etant pas disponible dans cette version
              //on cree un deuxieme cercle pour epaissir le contour blanc du cercle central
              $styleInternal2 = ms_newStyleObj($oClass, $style);
              $styleInternal2->outlinecolor->setRGB(255,255,255);
              $styleInternal2->set("offsety", 5.5);
              $styleInternal2->set("size", intval(intval($style->size)/1.65));
              // On genere l'image utilisee dans la legende
              $l_msIcon = $classCouche->createLegendIcon(32,32);
              $classCouche->deleteStyle(1);

              /** On ajoute les valeurs de la legende 
              $l_msIcon->saveImage($UrlImgLegPre."class".$j.$UrlImgLegPost);

            }else{*/
              $l_msIcon = $classCouche->createLegendIcon(20, 10);
              $l_msIcon->saveImage($UrlImgLegPre."class".$j.$UrlImgLegPost);
            //} 
          }
        }
      }
      return new Response("OK");
    }
    
    /**
     * Generate a symbol icon for a type of geometry and current submitted configuration
     * @PUT("/generate/symbol/{geometrytype}/{symbolset}")
     * 
     * @RequestParam(name="directory", description="MAPFILE DIRECTORY", nullable=false)
     */
    public function generateClassSymbol(Request $request, ParamFetcher $paramFetcher, $geometrytype, $symbolset)
    {
        try {
            $symbols = array();
            $directory = $paramFetcher->get("directory");
            $mapfile = "template.map";
            $mapObj = $this->getMapObj($directory, $mapfile);
            
            $symbolset = urldecode($symbolset) ?: $mapObj->symbolsetfilename;
            if ( !file_exists($symbolset) && file_exists($mapObj->mappath.$symbolset) ){
               $symbolset = $mapObj->mappath.$symbolset;
            }
            $mapObj->setSymbolSet($symbolset);
            $symbol_prefix = pathinfo($symbolset, PATHINFO_FILENAME);
        
            $imagepath = $this->getParameter("path_image_directory")."/SYMBOLS/TEMP/";
            @mkdir($imagepath, 0770, true);
            $imageurl = $this->getParameter("url_image_directory")."/SYMBOLS/TEMP/";
            $mapObj->web->set("imagepath", $imagepath);
            $mapObj->web->set("imageurl" , $imageurl);
            
            $layerObj = $this->getLayerObj($mapObj, 0, false);
            
            $classObj = $this->getClassObj($mapObj, $layerObj, 0, false);
            $styleObj = $this->getStyleObj($mapObj, $layerObj, $classObj, 0);
            
            
            $geometrytype = "MS_LAYER_".$geometrytype;
            if ( defined($geometrytype) )
            	$geometrytype = constant($geometrytype);
            $wkt = null;
            $feature_width = $feature_height = "1000";
            $feature_xmin = $feature_ymin = "0";
            $defaultSymbol = "Carre";
            $fieldSize = "size";
            switch ( $geometrytype ){
            	case MS_LAYER_POINT :
                $defaultSymbol = "Cercle";
                $available_types = array(MS_SYMBOL_VECTOR, MS_SYMBOL_ELLIPSE, MS_SYMBOL_PIXMAP, MS_SYMBOL_TRUETYPE);
                
            		$wkt = "POINT( {$feature_width} {$feature_height} )";
            		$minsize = 5;
            	break;
            	case MS_LAYER_LINE :
            		$wkt = "LINESTRING(".
            		  "{$feature_xmin} {$feature_ymin}, ".
            		  "{$feature_width} {$feature_height}".
            		  ")";
            		$minsize = 1;
            		$fieldSize = "width";
            	break;
            	case MS_LAYER_POLYGON :
                $available_types = array(MS_SYMBOL_VECTOR, MS_SYMBOL_ELLIPSE, MS_SYMBOL_PIXMAP, MS_SYMBOL_TRUETYPE);
                
                $feature_xmax = $feature_xmin + $feature_width;
                $feature_ymax = $feature_ymin + $feature_height;
            		$wkt = "POLYGON((".
            		  "{$feature_xmin} {$feature_ymin}, ".
            		  "{$feature_xmin} {$feature_ymax}, ".
            		  "{$feature_xmax} {$feature_ymax}, ".
            		  "{$feature_xmax} {$feature_ymin},  ".
            		  "{$feature_xmin} {$feature_ymin}".
            		  "))";
            		$minsize = 9;
            	break;
            }
            $layerObj->set("type", $geometrytype);
            $layerObj->addFeature(ms_shapeObjFromWkt($wkt));
            
            $parameters = $request->request->all();
            $style =& $parameters["styles"];
            if ( !isset($style['symbol']) && $geometrytype!=MS_LAYER_LINE ){
              $style["symbol"] = $defaultSymbol;
              $style["size"] = "9";
            }
            $style["size"] = max($minsize, $style["size"], $style[$fieldSize]);
            $this->getLogger()->info(__METHOD__, $style);
            $parameters["styles"] = array_diff($parameters["styles"], array(""));
            $request->request->replace($parameters);
            $this->putClassInLayer($request->request, $mapObj, $layerObj, 0);
            $classObj = $this->getClassObj($mapObj, $layerObj, 0, true);
            $layerObj->set('status', MS_ON);
            $classObj->set('name', 'defaultName');

            $classJson = $this->convertClassObj($classObj, 0);
            $icon = $classJson["icon"];
            
            return $this->formatJsonSuccess(array("data"=>$icon));
        } catch (\Exception $exception){
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, 'Internal Server Error', $exception->getMessage());
        }
    }
    /**
     * Extract from layerfile, the geometry type and returns its value
     * @Get("/fields/{layertype}/{layerfile}/{encoding}/{layersheet}", requirements={"layertype"="VECTOR|WFS", "encoding"="data|headers"}, defaults={"layersheet"=null, "encoding"="data"})
     * 
     * @param string $layerfile   complet path of filename
     * 
     * @ApiDoc(
     *   section="Layer",
     *   description="get geometry type",
     *   requirements={
     *     {
     *       "name"="layerfile",
     *       "dataType"="text",
     *       "description"="complet path of filename"
     *     }
     *   },
     * )
     */
    public function getLayerFields(Request $request, $layertype, $layerfile, $encoding="data", $layersheet=null)
    {
        $path      = $request->query->get("directory", "");
        $layerfile = urldecode($layerfile);
      
        //$this->getLogger()->debug(__METHOD__, array("layerFile"=>$layerfile));
        try {
        	  $layer_connection = null;
        	  $layer_name = null;
            $results = array();
            switch($layertype){
                case "VECTOR" :
                case "TABULAIRE" :
                	  $layer_connection = '"'.realpath($path)."/".$layerfile.'"';
                	  $layer_name = pathinfo($layerfile, PATHINFO_FILENAME);
                break;
                case "WFS" :
                case "POSTGIS" :
                    list($layer_connection, $layer_name) = explode("@", $layerfile);
                break;
            }
            if ($layer_connection && $layer_name)
                $results = $this->_getLayerFields($layer_connection, $layer_name, $layersheet, ($encoding=="data"));
            return $this->formatJsonSuccess($results);
        } catch( \Exception $exception){
            //$this->getLogger()->debug(__METHOD__, array("err"=>$exception->getMessage()));
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, $exception->getMessage());
        }
    }
    
    /**
     * Returns the field values for the layer. Returns values distinct or not, with counts or not. 
     * @param string  $layerName
     * @param string  $layerData
     * @param string  $field
     * @param boolean $bDistinct
     * @param boolean $bCount
     * @throws MapserverException
     * @return array
     */
    public function getLayerFieldValues(\mapObj $mapObj, \layerObj $layerObj, $field, $bDistinct=false, $bCount=false)
    {
        $tempname = $this->getParameter("path_image_directory")."/layerfieldvalues_".uniqid();
        
        $csvfile = $tempname.".csv";
        $fieldsfile = $tempname."_fields.csv";
        $detectEncodingFile = $tempname."_encoding.csv"; 
        $destCsvfile = $tempname."_UTF8.csv";
        $toEncoding = "UTF-8";
        list($layerData, $layerName) = self::_getDataForPhpOGR($mapObj, $layerObj, true, true);
        $fieldO = $field;
        $field = utf8_decode($field);
       
       
        
        /**
         * Found the index of this field in all fields with encodingless
         */
        $fields = $this->_getLayerFields($layerData, $layerName); 
        $fields = $fields["fields"];
        
        $fieldIndex = 0;
        foreach($fields as $field_name=>$field_datatype){
            $fieldIndex++;
            if ( strtolower($field)==strtolower($field_name) ){
                break;
            }
            if ( strtolower($field)==utf8_decode(strtolower($field_name)) ){
                break;
            }
            if ( strtolower($field)==utf8_encode(strtolower($field_name)) ){
                break;
            }
        }
        
        
        /**
         * Get field values or counts
         */
        $sql = "select ".($bDistinct ? "DISTINCT" : "")." ".
               ($fieldIndex ? "*" : "\"".$field."\" as __FIELD__").
               ($bCount ? ", count(*) as __COUNT__" : "").
               " from \"".$layerName."\"".
               ($bCount ? " group by ".($fieldIndex ?: "__FIELD__") : "").
               " order by ".($fieldIndex ?: "__FIELD__");
        
        
        $cmds = array(
          "ogr2ogr -dialect sqlite -sql '{$sql}' -f \"CSV\" \"{$csvfile}\" {$layerData} 2> {$tempname}.log",
          "detectEncodingFile=`file -b --mime-encoding ".$csvfile."` ; iconv -f \${detectEncodingFile} -t ".$toEncoding ." ". $csvfile. " > ".$destCsvfile
        );
        
        foreach ($cmds as $cmd){
            $this->getLogger()->debug(__METHOD__, array("cmd"=>$cmd));
            $errors = array(); $return = 1;
            $result = exec($cmd, $errors, $return);
            if ( file_exists("{$tempname}.log") ){
                $errors = array_merge($errors, array(file_get_contents("{$tempname}.log")));
                @unlink("{$tempname}.log");
            }
            $this->getLogger()->debug(__METHOD__, array("result"=>$result, "return"=>$return, "errors"=>$errors));
            if ( !empty($errors) && strpos(implode($errors), "ERROR")!==false ) {
                throw new MapserverException(sprintf($this->_t("Unable to execute OGR query \"%s\" for layer named \"%s\" and field \"%s\" \n\n [COMMANDE=%s]\n%s"), $sql, $layerObj->name, $field, $cmd, implode("\n", $errors)));
            }
        }
        $csvfile = $destCsvfile;
                
        //var_dump(file_get_contents($csvfile));
        
        $data = array();
        if ( file_exists($csvfile) ){
            $file = fopen($csvfile, "r");
            $headers = array();
            if( $line = fgetcsv($file) ){
                $headers = array_map("strtolower", $line);
                if ( $fieldIndex ){
                  $headers = array("field");
                  if ( $bCount )
                    $headers[] = "count";
                }
                $headers = str_replace("__field__", "field", $headers);
                $headers = str_replace("__FIELDUTF8__", "field_utf8", $headers);
                $headers = str_replace("__count__", "count", $headers);
            }
            if ( !empty($headers) ){
                while( $line = fgetcsv($file) ){
                    if ( $fieldIndex ){
                      $line = array($line[$fieldIndex-1], array_pop($line));
                      if ( !$bCount ){
                        $line = array($line[0]);
                      }
                    }
                    $line = array_map("trim", $line);
                    $data[] = array_combine($headers, $line);
                }
            }
            //@unlink($csvfile);
        }
        //var_dump($data);
        @exec("rm -f {$tempname}.*");
        return $data;
    }
    
    /**
     * Construct a graduate distribution of data by method SAMEAMPLITUDE (the data intervals are equals)
     * @param array{field=>, count=>} $distribution
     * @param int $nbClass
     * @return array(limitMax)
     */
    protected function symbologyGraduate_SameAmplitude($distribution, $nbClass)
		{
			$classes = array();
			$limits = array();
			
			foreach ($distribution as $i=>$datarow)
			{
				$val = $datarow["field"];
				if ( !is_numeric($val) ){
					$val = 0;
				}
				$val = floatval($val);
				if ( $i==0 ){
					$maxVal = $val;
					$minVal = $val;
				} else {
					$maxVal = max($val, $maxVal);
					$minVal = min($val, $minVal);
				}
			}
			if ($maxVal != $minVal) {
				for ($iteration = 0; $iteration < $nbClass - 1; $iteration++)
					$classes[$iteration] = $minVal + ($iteration + 1) * (($maxVal - $minVal) / $nbClass);
			}
			
			for ($j = 0; $j < count($classes); $j++)
			{
				$classes[$j] = round($classes[$j], 2);
			}
			
			if ( empty($classes) ){
				$classes[] = $maxVal;
			}
			
			$this->getLogger()->debug(__METHOD__, array("nbClass"=>$nbClass, "minVal"=>$minVal, "maxVal"=>$maxVal, "classes"=>$classes));
			return $classes;
		}

    /**
     * Construct a graduate distribution of data by method SAMECOUNT (the number of data in intervals are equal)
     * @param array{field=>, count=>} $distribution
     * @param int $nbClass
     * @return array(limitMax)
     */
    protected function symbologyGraduate_SameCount($distribution, $nbClass)
		{
			$classes = array();
			
			$nbTotalData = 0;
			foreach ($distribution as $i=>$datarow)
			{
				$val = $datarow["field"];
				$nbTotalData += $datarow["count"];
				if ( !is_numeric($datarow["field"]) ){
					$datarow["field"] = 0;
				}
				$datarow["field"] = floatval($datarow["field"]);
			}
      
			//Nb equi-elements by class 
			if ($nbClass > 0)
				$limit = ceil($nbTotalData / $nbClass);
			else
				$limit = $nbTotalData;
		
			$count_cumule = 0;
			foreach ($distribution as $i=>$datarow)
			{
				$count_cumule += $datarow["count"];
				// if limit is reached
				if ( $count_cumule>=$limit ){
					$classes[] = $datarow["field"];
					$count_cumule = 0;
				}
			}
			
			$this->getLogger()->debug(__METHOD__, array("nbClass"=>$nbClass, "nbTotalData"=>$nbTotalData, "nbTotalData"=>$nbTotalData, "limit"=>$limit, "classes"=>$classes));
			
			if ( empty($classes) && !empty($distribution) ){
				$classes[] = $distribution[count($distribution)-1]["field"];
			}
			return $classes;
		}

    /**
     * Construct a graduate distribution of data by method HOMOGENEOUS (homogeneous distribution given by a Jenks algorith)
     * @param array{field=>, count=>} $distribution
     * @param int $nbClass
     * @return array(limitMax)
     */
    protected function symbologyGraduate_Homogeneous($distribution, $nbClass)
		{
			$valeurs  = array();
			foreach ($distribution as $i=>$datarow)
			{
				$val = $datarow["field"];
				if ( !is_numeric($val) ){
					$val = 0;
				}
				$valeurs = array_merge($valeurs, array_fill(0, $datarow["count"], $val));
			}
			
		  $oJenksGenerator = new JenksGenerator($valeurs, $nbClass);
		  $oJenksGenerator->mbObtenerDatos($valeurs, $nbClass);
		  $oJenksGenerator->generarIntervalos();
		  $this->getLogger()->debug(__METHOD__, array("nbClass"=>$nbClass, "oJenksGenerator->mdaValoresRuptura"=>$oJenksGenerator->mdaValoresRuptura, "valeurs"=>$valeurs));
			
		  return $oJenksGenerator->mdaValoresRuptura;
		}
    /**
     * Returns all symbols
     * @Get("/symbolset/concat/{mapfile}/{mapname}/{from_symbolset}/{with_symbolset}")
     * @QueryParam(name="directory", description="MAPFILE DIRECTORY", nullable=false)
     */
    public function concatSymbolSets(Request $request, ParamFetcher $paramFetcher, $mapfile, $mapname, $from_symbolset, $with_symbolset)
    {
      
        try {
            
            $directory = $paramFetcher->get("directory");
            $from_symbolset = urldecode($from_symbolset);
            $with_symbolset = urldecode($with_symbolset);
            
            $mapfile .= ".map";
            $saveObj = $this->getMapObj($directory, $mapfile);
            
            if ( !file_exists($from_symbolset) && file_exists($directory.$from_symbolset) ){
                $from_symbolset = $directory.$from_symbolset;
            }
            if ( !file_exists($with_symbolset) && file_exists($directory.$with_symbolset) ){
                $with_symbolset = $directory.$with_symbolset;
            }
                        
            $file_contents=file_get_contents($with_symbolset);
            $firstPos = strpos($file_contents, "SYMBOLSET");
            if( $firstPos ===false)
                return false;
            $string_toadd = "\n".substr($file_contents, $firstPos+9)."\n";
           
            $initial_file_contents = file_get_contents($from_symbolset);
            $lastPos = strrpos($initial_file_contents, "END");
            $initial_string  = substr($initial_file_contents, 0, $lastPos);
            
            $to_symbolset = "./etc/symbols-".$mapname.".sym";
            if ( !file_exists(dirname($to_symbolset)) && file_exists($directory.dirname($to_symbolset)) ){
                $to_symbolset = $directory.$to_symbolset;
            }
            $this->getLogger()->debug(__METHOD__, array("to_symbolset"=>$to_symbolset));
            file_put_contents($to_symbolset, $initial_string.$string_toadd );
            //first put in temporary mapfile in order to not destroy mapfile
            try{
                $templateFile = "template.map";
                $oMap =$this->getMapObj($directory, $templateFile);
                $this->setSymbolset($oMap, $to_symbolset);
            }catch (\Exception $exception){
                //back to old file
                @unlink($to_symbolset);
                file_put_contents($to_symbolset, $initial_file_contents);
                return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, 'Internal Server Error', $exception->getMessage());
            }
            
            $this->setSymbolset($saveObj, $to_symbolset);
            $this->saveMap($saveObj, $mapfile , "When concatenate symbolset files ");
            
            return $this->formatJsonSuccess(array("symbolset"=>str_replace($saveObj->mappath, "", $to_symbolset)));
        } catch (\Exception $exception){
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, 'Internal Server Error', $exception->getMessage());
        }
		}
    
    /**
     * Returns all symbols
     * @Get("/symbolset/reset/{mapfile}/{mapname}")
     * @QueryParam(name="directory", description="MAPFILE DIRECTORY", nullable=false)
     */
    public function resetSymbolSets(Request $request, ParamFetcher $paramFetcher, $mapfile, $mapname)
    {
        //reset mapserver error (to avoid warnings due to precedent errors)
        ms_ResetErrorList();
        try {
            $directory = $paramFetcher->get("directory");
            $mapfile .= ".map";
            $mapObj = $this->getMapObj($directory, $mapfile);
            $symbolset = $this->getParameter("default_symbolset");
            if ( !file_exists($symbolset) && file_exists($mapObj->mappath.$symbolset) ){
               $symbolset = $mapObj->mappath.$symbolset;
            }
            $this->setSymbolset($mapObj, $symbolset);
            
            $specifiq_symbolset = "./etc/symbols-".$mapname.".sym";
            if ( !file_exists(dirname($specifiq_symbolset)) && file_exists($mapObj->mappath.dirname($specifiq_symbolset)) ){
                $specifiq_symbolset = $mapObj->mappath.$specifiq_symbolset;
            }
            if ( file_exists($specifiq_symbolset) ){
                @unlink($specifiq_symbolset);
            }
            
            $this->saveMap($mapObj, $mapfile , "When reset symbolset files ");
            return $this->formatJsonSuccess(array("symbolset"=>str_replace($mapObj->mappath, "", $mapObj->symbolsetfilename)));
        } catch (\Exception $exception){
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, 'Internal Server Error', $exception->getMessage());
        }
		}
    
    /**
     * Returns all symbols
     * @Get("/library/symbols/{geometrytype}/{symbolset}")
     * @QueryParam(name="directory", description="MAPFILE DIRECTORY", nullable=false)
     */
    public function getSymbolLibrary(Request $request, ParamFetcher $paramFetcher, $geometrytype, $symbolset)
    {
        //reset mapserver error (to avoid warnings due to precedent errors)
        ms_ResetErrorList();
        
        try {
            $symbols = array();
            $directory = $paramFetcher->get("directory");
            $mapfile = "template.map";
            $mapObj = $this->getMapObj($directory, $mapfile);
            
            $symbolset = urldecode($symbolset) ?: $mapObj->symbolsetfilename;
            if ( !file_exists($symbolset) && file_exists($mapObj->mappath.$symbolset) ){
               $symbolset = $mapObj->mappath.$symbolset;
            }
            $this->setSymbolset($mapObj, $symbolset);
            
            $symbol_prefix = pathinfo($symbolset, PATHINFO_FILENAME);
            
            
            $imagepath = $this->getParameter("path_image_directory")."/SYMBOLS/{$geometrytype}/";
            @mkdir($imagepath, 0770, true);
            $imageurl = $this->getParameter("url_image_directory")."/SYMBOLS/{$geometrytype}/";
            $mapObj->web->set("imagepath", $imagepath);
            $mapObj->web->set("imageurl" , $imageurl);
            
            $layerObj = $this->getLayerObj($mapObj, 0, false);
            
            $classObj = $this->getClassObj($mapObj, $layerObj, 0, false);
            $styleObj = $this->getStyleObj($mapObj, $layerObj, $classObj, 0);
            for ($i=1; $i<$classObj->numstyles; $i++) $classObj->deleteStyle($i);
            
            $geometrytype = "MS_LAYER_".$geometrytype;
            if ( defined($geometrytype) )
            	$geometrytype = constant($geometrytype);
            $wkt = null;
            $feature_width = $feature_height = "1000";
            $feature_xmin = $feature_ymin = "0";
            $available_types = array();
            switch ( $geometrytype ){
            	case MS_LAYER_POINT :
                $available_types = array(MS_SYMBOL_VECTOR, MS_SYMBOL_ELLIPSE, MS_SYMBOL_PIXMAP, MS_SYMBOL_TRUETYPE);
                
            		$wkt = "POINT( {$feature_width} {$feature_height} )";
                $styleObj->set("size", 14);
                $styleObj->set("width", 1.5);
                $styleObj->set("outlinewidth", 2);
            	break;
            	case MS_LAYER_LINE :
            		$wkt = "LINESTRING(".
            		  "{$feature_xmin} {$feature_ymin}, ".
            		  "{$feature_width} {$feature_height}".
            		  ")";
                $styleObj->set("size", 4);
                $styleObj->set("width", 2);
            	break;
            	case MS_LAYER_POLYGON :
                $available_types = array(MS_SYMBOL_VECTOR, MS_SYMBOL_ELLIPSE, MS_SYMBOL_PIXMAP, MS_SYMBOL_TRUETYPE);
                
                $feature_xmax = $feature_xmin + $feature_width;
                $feature_ymax = $feature_ymin + $feature_height;
            		$wkt = "POLYGON((".
            		  "{$feature_xmin} {$feature_ymin}, ".
            		  "{$feature_xmin} {$feature_ymax}, ".
            		  "{$feature_xmax} {$feature_ymax}, ".
            		  "{$feature_xmax} {$feature_ymin},  ".
            		  "{$feature_xmin} {$feature_ymin}".
            		  "))";
                $styleObj->set("minsize", 4);
                $styleObj->set("size", 14);
                $styleObj->set("width", 1.5);
                $styleObj->set("outlinewidth", 2);
                $styleObj->set("initialgap", 4);
                $styleObj->set("gap", $styleObj->size+4);
            	break;
            }
            
            $layerObj->set("type", $geometrytype);
            $layerObj->addFeature(ms_shapeObjFromWkt($wkt));
            
            $transparent_color = '#000000';
            $styleObj->color->setHex($transparent_color);
            $styleObj->outlinecolor->setHex($transparent_color);
            $styleObj->updateFromString("STYLE ANTIALIAS FALSE END");
            
            for ( $iSymbol=0; $iSymbol<$mapObj->getNumSymbols(); $iSymbol++){
            	$styleObj->set("symbol", $iSymbol);
            	
            	$symbolObj = $mapObj->getSymbolObjectById($iSymbol);
            	if ( !$symbolObj ) continue;
            	$name = $symbolObj->name;
            	if ( empty($name) ) continue;
            	if ( !in_array($symbolObj->type, $available_types) ){
            		continue;
            	}
            	$symbolObj->set("filled", MS_TRUE);
            	
            	$symbolIcon = $this->generateClassIcon($classObj, $symbol_prefix."_".$iSymbol, $transparent_color);
            	if ( empty($symbolIcon) ) continue;
            	$symbols[] = array_merge(
            		array("name"=> $name, "index"=>$iSymbol), 
            		$symbolIcon
            	);
            }
            
            if ( $request->query->get("show", 0) ){
            	$content = array();
            	foreach($symbols as $symbol){
            		$content[] = "<div style='border:1px solid red;margin:5px;'><img style='background-color:red;' src=".$symbol["image"]."?_dc=".rand(0, time())."> ".$symbol["name"]."</div>";
            	}
            	return new Response(implode($content));
            }
            return $this->formatJsonSuccess(array("data"=>$symbols, "symbolset"=>str_replace($mapObj->mappath, "", $symbolset)));
            
                
     } catch (\Exception $exception){
        	throw $exception;
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, 'Internal Server Error', $exception->getMessage());
        }
    }
    /**
     * Extract from layerfile, the geometry type and returns its value
     * @Get("/getsheets/{layerfile}")
     * 
     * @param string $layerfile   complet path of filename
     * 
     * @ApiDoc(
     *   section="Layer",
     *   description="get sheets in file",
     *   requirements={
     *     {
     *       "name"="layerfile",
     *       "dataType"="text",
     *       "description"="complet path of filename"
     *     }
     *   },
     * )
     */
    public function getSheets(Request $request, $layerfile)
    {
        $path      = $request->query->get("directory", "");
        $layerfile = realpath($path)."/".urldecode($layerfile);
        
        $sheets = array();
        try {
            $extension = strtoupper(pathinfo($layerfile, PATHINFO_EXTENSION));
            if ( in_array($extension, explode(",", self::SPREADSHEET_EXTENSIONS)) ){
              $cmd = "ogrinfo -q \"".$layerfile."\"";
              exec($cmd, $sheets);
              foreach($sheets as $index=>$sheet_index){
                  if ( strpos("FAILURE:", $sheet_index)!==false ) throw new \Exception(implode(" ", array_slice($sheets, 0, min(count($sheets), 2))));
                  $sheet_index = preg_replace("! \([^)]+\)$!", "", $sheet_index);
                  $sheet_name = preg_replace("!^\d+: !", "", $sheet_index);
                  $sheets[$index] = array("sheet_index"=>$sheet_index, "sheet_name"=>$sheet_name);
              }
            }
        } catch( \Exception $exception){
            $this->getLogger()->debug(__METHOD__, array("err"=>$exception->getMessage()));
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, 'Internal Server Error', $exception->getMessage());
        }
        return $this->formatJsonSuccess(array("sheets"=>$sheets));
    }
    
    /**
     * Extract from layerfile, the geometry type and returns its value
     * @Get("/geometrytype/{layertype}/{layerfile}/{encoding}", requirements={"layertype"="VECTOR|WFS", "encoding"="data|headers"}, defaults={"encoding"="data"})
     * 
     * @param string $layerfile   complet path of filename
     * 
     * @ApiDoc(
     *   section="Layer",
     *   description="get geometry type",
     *   requirements={
     *     {
     *       "name"="layerfile",
     *       "dataType"="text",
     *       "description"="complet path of filename"
     *     }
     *   },
     * )
     */
    public function getGeometryType(Request $request, $layertype, $layerfile, $encoding="data")
    {
        $path      = $request->query->get("directory", "");
        $layerfile = urldecode($layerfile);
      
        //$this->getLogger()->debug(__METHOD__, array("layerFile"=>$layerfile));
        try {
        	  $layer_connection = null;
        	  $layer_name = null;
            $results = array();
            switch($layertype){
                case "VECTOR" :
                	  $layer_connection = realpath($path)."/".$layerfile;
                	  $layer_name = pathinfo($layerfile, PATHINFO_FILENAME);
                break;
                case "WFS" :
                case "POSTGIS" :
                    list($layer_connection, $layer_name) = explode("@", $layerfile);
                break;
            }
            if ($layer_connection && $layer_name)
                $results = $this->_getGeometryType($layertype, $layer_connection, $layer_name, ($encoding=="data"));
            return $this->formatJsonSuccess($results);
        } catch( \Exception $exception){
            //$this->getLogger()->debug(__METHOD__, array("err"=>$exception->getMessage()));
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, 'Internal Server Error', $exception->getMessage());
        }
    }
    
    /**
     * Create WxsMapfile
     * @Post("/createWxsMapfile/{mapfile}")
     * @QueryParam(name="directory", description="MAPFILE DIRECTORY", nullable=false)
     * @QueryParam(name="wms_mapfile", description="wms mapfile name", nullable=false)
     * @QueryParam(name="params", description="MAP PARAMS", nullable=true)
     */
    public function createWxsMapfile(Request $request, ParamFetcher $paramFetcher, $mapfile){
        
        //reset mapserver error (to avoid warnings due to precedent errors)
        ms_ResetErrorList();
        
        //load mapfile
        $directory = $paramFetcher->get("directory");
        $wms_mapfile = $paramFetcher->get("wms_mapfile").".map";
        $mapObj = $this->getMapObj($directory, $mapfile);
        $wxsInfo = unserialize($request->get("wxsInfo"));
        /*$wxsInfo = array(
            "wms_onlineresource"=> "wms_onlineresource",
            "wfs_onlineresource"=> "wfs_onlineresource",
            "geosourceUrl" => "http://metadata.carmencarto.fr/srv/fre",
            "layers"=> array(
                "layername" => array(
                    "wxsname" => "layerOgcname",
                    "wms_status" => 1,
                    "wfs_status" => 1,
                    "gml_include_items" => "item1, item2",
                    "metadata_uuid" => "zzzzzz"
                )
            )
            
        );*/
        $this->treatWxsMap($mapObj, $wxsInfo, $mapfile, $directory);
        $mapObj->save($directory.$wms_mapfile);
        $this->treatSpecificMapfile($directory, $wms_mapfile, $mapObj, ($wxsInfo["metadata_atom_uuid"]!=""));
        
        return new Response($directory."wms_".$mapfile.".map");
    }
    
    /**
     * remove accents from string
     * @param unknown $str
     * @param string $charset
     * @return mixed
     */
    protected function wd_remove_accents($str, $charset='utf-8')
    {
        $str = htmlentities($str, ENT_NOQUOTES, $charset);
    
        $str = preg_replace('#&([A-za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
        $str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str); // pour les ligatures e.g. '&oelig;'
        $str = preg_replace('#&[^;]+;#', '', $str); // supprime les autres caractères
    
        return $str;
    }
    
    /**
     * treat wxs map
     * @param unknown $mapObj
     */
    protected function treatWxsMap($oMap, $wxsInfo, $mapfile, $directory){
        
        $oMap->set("maxsize", 10000);
        //onlineresource
        $oMap->setMetaData("wms_onlineresource", $wxsInfo["wms_onlineresource"]);
        $oMap->setMetaData("wfs_onlineresource", $wxsInfo["wfs_onlineresource"]);
        
        //title, name
        $wxs_title = urldecode($oMap->name);
        $oMap->setMetaData("wms_title", $wxs_title);
        $oMap->setMetaData("wfs_title", $wxs_title);
        //$meta = strtr($wxs_title,'àáâãäåòóôõöøèéêëçìíîïùúûüÿñ', 'aaaaaaooooooeeeeciiiiuuuuyn');
        
        $meta = $this->wd_remove_accents($wxs_title);
        $meta = ereg_replace("[^a-zA-Z0-9]", "_", $meta);
        $oMap->set('name', $meta);
        
        //SRS
        $oMap->setMetaData("wms_srs", $oMap->getMetadata("geoide_wms_srs"));
        $oMap->setMetaData("wfs_srs", $oMap->getMetadata("geoide_wfs_srs"));
        
        //legend
        $oMap->legend->label->set("encoding", "utf-8");
        //$oMap->legend->label->set("type", MS_TRUETYPE);
        $oMap->legend->label->set("font", "Arial");
        $oMap->legend->label->set("size", 10);
        
        //INSPIRE parameters
        $oMap->setMetaData('ows_encoding','ISO-8859-1');

        //WMS Inspire
        if ($wxsInfo["metadata_wms_uuid"]!=""){
            $oMap->setMetaData("wms_enable_request", "*");
            $oMap->setMetaData("wms_inspire_capabilities", "url");
            $oMap->setMetaData("wms_inspire_metadataurl_type", "TC211");
            $oMap->setMetaData("wms_inspire_metadataurl_format", "text/html");
            $urlWmsMetadata = $wxsInfo["geosourceUrl"] . "/find?uuid=" . $wxsInfo["metadata_wms_uuid"];
            $oMap->setMetaData("wms_inspire_metadataurl_href", $urlWmsMetadata);
            $oMap->setMetaData("wms_service_onlineresource", $wxsInfo["wms_onlineresource"]);
            $oMap->setMetaData("wms_bbox_extended", "true");
        }
        
        //WFS Inspire
        if ($wxsInfo["metadata_wfs_uuid"]!=""){
            $oMap->setMetaData("wfs_inspire_capabilities", "url");
            $oMap->setMetaData("wfs_inspire_metadataurl_type", "TC211");
            $oMap->setMetaData("wfs_inspire_metadataurl_format", "text/html");
            $urlWfsMetadata = $wxsInfo["geosourceUrl"] . "/find?uuid=" . $wxsInfo["metadata_wfs_uuid"];
            $oMap->setMetaData("wfs_inspire_metadataurl_href", $urlWfsMetadata);
            $oMap->setMetaData("wfs_service_onlineresource", $wxsInfo["wfs_onlineresource"]);
            $oMap->setMetaData("wfs_bbox_extended", "true");
        }
        
        //ATOM Service
        if ($wxsInfo["metadata_atom_uuid"]!=""){
            //TODO à passer en paramètre
            $CARMEN_DOWNLOAD_FORMATS = array(
            		"SHAPE" => "application/x-shapefile",
            		"MIDMIF" => "application/binary",
            		"KML" => "application/vnd.google-earth.kml+xml",
            );
            
        
            $wfs_getfeature_formatlist ="";
            $countFormat=0;
            foreach ($CARMEN_DOWNLOAD_FORMATS as $key => $value) {
                $wfs_getfeature_formatlist .= $key . ($countFormat < count($CARMEN_DOWNLOAD_FORMATS) - 1 ? ',' : '');
                $countFormat++;
            }
            $oMap->setMetaData("wfs_getfeature_formatlist", $wfs_getfeature_formatlist);
        }
        
        
        
        //TODO CHECK INFOS
        //print_r($wxsInfo):
        $oMap->setmetadata('wms_accessconstraints', 'Aucun obstacle technique');
        $oMap->setmetadata('wfs_accessconstraints', 'Aucun obstacle technique');
        $oMap->setmetadata('wms_keywordlist_ISO_items', 'infoMapAccessService,WMS 1.1.1,WMS 1.3.0,SLD 1.1.0');
        $oMap->setmetadata('wfs_keywordlist', 'infoFeatureAccessService');
        $oMap->setmetadata('wms_keywordlist_vocabulary', 'ISO');
        if($oMap->getMetaData("wms_contactorganization")){
            $oMap->setmetadata('wms_addresstype', 'postal');
            $oMap->setmetadata('wfs_addresstype', 'postal');
        }
        
        
        
        $map_extent=$oMap->extent;
        $wms_extent=$map_extent->minx.' '.$map_extent->miny.' '.$map_extent->maxx.' '.$map_extent->maxy;
        
        for ($i = 0; $i < $oMap->numlayers; $i++){
            $oLayer = $oMap->getLayer($i);
            if ($oLayer && $wxsInfo["layers"][$oLayer->name])
            {
                $this->treatWxsLayer($oMap, $oLayer, $wxsInfo["layers"][$oLayer->name], $wms_extent, $wxsInfo["geosourceUrl"] , $wxsInfo["wms_onlineresource"]);
            }
        }
        
        if ($wxsInfo["metadata_atom_uuid"]!=""){
	        $this->CSAtom_GenerateAtomFiles ( $directory, $wxsInfo["mapfile"], $oMap, $wxsInfo);
        }
        else{
        	$this->CSAtom_DeleteAtomFiles ( $directory, $wxsInfo["mapfile"] );
        }
        
        
    }
    
    /**
     * specific treat not possible in mapscript 
     * @param unknown $map_path_wms
     * @param unknown $wms_file
     * @param unknown $m_mapWms
     */
    protected function treatSpecificMapfile($map_path_wms, $wms_file, $m_mapWms, $bAtom){
    	
    	{
    		$mapFileArray = file($map_path_wms.$wms_file);
    		$newLines = array();
    		$firstClass = 0;
    		foreach ($mapFileArray as $line_num => $line) {
    			/*if(strpos($line,'DRIVER "GD/PNG"') !== false) {
    				array_push($newLines, '    DRIVER "AGG/PNG"' . "\n");
    			}
    			else if(strpos($line,'DRIVER "GD/JPEG"') !== false) {
    				array_push($newLines, '    DRIVER "AGG/JPEG"' . "\n");
    			}*/
    			if(preg_match('"RESOLUTION .*"', trim($line)) == 1) {
    				array_push($newLines, $line);
    				array_push($newLines, '  DEFRESOLUTION '. $m_mapWms->resolution . "\n");
    			}/*
    			else if(preg_match('"SYMBOLSET .*"', trim($line)) == 1) {
    				//la ligne SYMBOLSET est supprimée
    	
    				//on récupère le répertoire où se trouve le fichier, dans le cas où on en est besoin pour les images utilisées comme symbol
    				$lastSlash = strrpos(trim($line), "/") + 1;
    				$repSym = substr(trim($line), 11, $lastSlash - 11); //11 = SYMBOLSET "
    			}*/
    			else if(trim($line) == "WEB" && $bAtom) {
    				//ajout des outputFormats necessaires au telechargement
    				array_push($newLines, '  OUTPUTFORMAT'."\n");
    				array_push($newLines, '    NAME "KML"'."\n");
    				array_push($newLines, '    DRIVER "OGR/KML"'."\n");
    				array_push($newLines, '    TRANSPARENT FALSE'."\n");
    				array_push($newLines, '    FORMATOPTION "STORAGE=filesystem"'."\n");
    				array_push($newLines, '    FORMATOPTION "FORM=simple"'."\n");
    				array_push($newLines, '    FORMATOPTION "FILENAME=result.kml"'."\n");
    				array_push($newLines, '  END'."\n"."\n");
    				array_push($newLines, '  OUTPUTFORMAT'."\n");
    				array_push($newLines, '    NAME "MIDMIF"'."\n");
    				array_push($newLines, '    DRIVER "OGR/MapInfo File"'."\n");
    				array_push($newLines, '    TRANSPARENT FALSE'."\n");
    				array_push($newLines, '    FORMATOPTION "STORAGE=filesystem"'."\n");
    				array_push($newLines, '    FORMATOPTION "FORM=multipart"'."\n");
    				array_push($newLines, '    FORMATOPTION "DSCO:FORMAT=MIF"'."\n");
    				array_push($newLines, '    FORMATOPTION "FILENAME=result.mif"'."\n");
    				array_push($newLines, '  END'."\n"."\n");
    				array_push($newLines, '  OUTPUTFORMAT'."\n");
    				array_push($newLines, '    NAME "SHAPE"'."\n");
    				array_push($newLines, '    DRIVER "OGR/ESRI Shapefile"'."\n");
    				array_push($newLines, '    TRANSPARENT FALSE'."\n");
    				array_push($newLines, '    FORMATOPTION "STORAGE=filesystem"'."\n");
    				array_push($newLines, '    FORMATOPTION "FORM=zip"'."\n");
    				array_push($newLines, '    FORMATOPTION "FILENAME=result.zip"'."\n");
    				array_push($newLines, '  END'."\n"."\n");
    				array_push($newLines, $line);
    			}
    			else if(trim($line) == "CLASS") {
    				if ($firstClass == 1){
    					array_push($newLines, '    CLASSGROUP "inspire_common:DEFAULT"'."\n");
    					$firstClass = 0;
    				}
    				array_push($newLines, $line);
    				array_push($newLines, '      GROUP "inspire_common:DEFAULT"'."\n");
    			}
    			else {
    				array_push($newLines, $line);
    			}
    		}
    		$new_string = implode("", $newLines);
    		$fp = fopen($map_path_wms.$wms_file, 'w+');
    		fwrite($fp, $new_string);
    		fclose($fp);
    	}
    	
    }
    /**
     * treat each wxsLayer
     * @param unknown $oLayer
     * @param unknown $layerInfo
     * @param unknown $wmsExtent
     * @param unknown $geosourceUrl
     * @param unknown $wmsOnlineRessource
     */
    protected function treatWxsLayer($oMap, $oLayer, $layerInfo, $wmsExtent, $geosourceUrl, $wmsOnlineRessource){
        
        $delete_layer=true;
        if ($oLayer->getMetadata('gml_include_items'))
            $oLayer->removeMetaData("gml_include_items");
        if ($oLayer->getMetadata('ows_include_items'))
            $oLayer->removeMetaData("ows_include_items");
        if($oLayer->getMetadata('wms_srs'))
            $oLayer->removeMetaData("wms_srs");
        if ($oLayer->getMetadata('wfs_srs'))
            $oLayer->removeMetaData("wfs_srs");
        
        $oLayer->set("name", $layerInfo["wxsname"]);
        
        if ($layerInfo["wms_status"])
        {
            $oLayer->setMetaData("wms_title",  $layerInfo["wxs_title"]);
            
            $tabMapSrs = explode(" ", $oMap->getMetadata("geoide_wms_srs"));
            $tabLayerSrs = explode(" ", $oLayer->getMetadata("geoide_wms_srs"));
            foreach($tabLayerSrs as $key => $value){
            	if(!in_array($value, $tabMapSrs)){
            		array_push($tabMapSrs, $value);
            	}
            }
            $wms_srs = implode(" ", $tabMapSrs);
            $oLayer->setMetaData("wms_srs", $wms_srs);
            $oLayer->setMetaData('wms_extent',$wmsExtent);
            $oLayer->setMetaData('wms_encoding','utf-8');
            $oLayer->set('dump',MS_TRUE);
            
            if ($layerInfo["metadata_uuid"]){
                $oLayer->setMetaData("wms_metadataurl_type", "TC211");
                $oLayer->setMetaData("wms_metadataurl_format", "text/html");
                $layer_metadata = $layerInfo["metadata_uuid"];
                $urlLayerMetadata = $geosourceUrl . "/find?uuid=" . $layer_metadata;
                $oLayer->setMetaData("wms_metadataurl_href", $urlLayerMetadata);
                $oLayer->setMetaData("wms_identifier_authority", "GeoSource");
                $oLayer->setMetaData("wms_identifier_value", $geosourceUrl."/".$layer_metadata);
                //$oLayer->setMetaData("wms_abstract", CSWms_getAbstract($layer_metadata, $geosourceUrl, $service_idx));
                $oLayer->setMetaData("wms_keywordlist_vocabulary", "ISO");
                //$oLayer->setMetaData("wms_keywordlist_ISO_items", CSWms_getKeywords($layer_metadata, $geosourceUrl, $service_idx));
            }
            $oLayer->setMetaData("wms_bbox_extended", "true");
            $oLayer->setMetaData("wms_style_inspire_common:DEFAULT_title", $layerInfo["wxsname"]);
            $oLayer->setMetaData("wms_style_inspire_common:DEFAULT_legendurl_width", "120");
            $oLayer->setMetaData("wms_style_inspire_common:DEFAULT_legendurl_height", "120");
            $oLayer->setMetaData("wms_style_inspire_common:DEFAULT_legendurl_format", "image/png");
            $legendUrl = $wmsOnlineRessource."&service=WMS&version=1.3.0&request=GetLegendGraphic&format=image/png&sld_version=1.1.0&layer=".$layerInfo["wxsname"];
            $oLayer->setMetaData("wms_style_inspire_common:DEFAULT_legendurl_href", $legendUrl);
            
            $oLayer->setMetaData("wms_enable_request", "*");
            //$oMap->setMetaData("wms_enable_request", "*");
            
            
            $delete_layer=false;
        }
        else{
            $oLayer->setMetaData("wms_enable_request", "!*");
            //$oMap->setMetaData("wms_enable_request", "!*");
        }
        
        if ($layerInfo["wfs_status"])
        {
            $oLayer->setMetaData("wfs_title",  $layerInfo["wxs_title"]);
            $tabSrs = explode(" ", $oMap->getMetadata("geoide_wfs_srs"));
            $tabLayerSrs = explode(" ", $oLayer->getMetadata("geoide_wfs_srs"));
            foreach($tabLayerSrs as $key => $value){
            	if(!in_array($value, $tabSrs)){
            		array_push($tabSrs, $value);
            	}
            }
            $wfs_srs = implode(" ", $tabSrs);
            $oLayer->setMetaData("wfs_srs", $wfs_srs);
            
            //applicate to map wfs_srs since mapserver does not take in account in layer level
            $tabMapSrs = explode(" ", $oMap->getMetadata("wfs_srs"));
            foreach($tabSrs as $key => $value){
                if(!in_array($value, $tabMapSrs)){
                    array_push($tabMapSrs, $value);
                }
            }
            $map_wfs_srs = implode(" ", $tabMapSrs);
            $oMap->setMetaData("wfs_srs", $map_wfs_srs);
            
            $oLayer->setMetaData('wfs_extent',$wmsExtent);
            $oLayer->setMetaData('wfs_encoding','utf-8');
            $oLayer->set('dump',MS_TRUE);
            $delete_layer=false;

            if ($layerInfo["metadata_uuid"]){
                $oLayer->setMetaData("wfs_metadataurl_type", "TC211");
                $oLayer->setMetaData("wfs_metadataurl_format", "text/html");
                $layer_metadata = $layerInfo["metadata_uuid"];
                $urlLayerMetadata = $geosourceUrl . "/find?uuid=" . $layer_metadata;
                //TODO doit when writing map
                //$oLayer->setMetaData("wfs_abstract", CSWms_getAbstract($layer_metadata, $geosourceUrl, $service_idx));
                $oLayer->setMetaData("wfs_metadataurl_href", $urlLayerMetadata);
                //$oLayer->setMetaData("wfs_keywordlist", CSWms_getKeywords($layer_metadata, $geosourceUrl, $service_idx));
            }
            $oLayer->setMetaData("wfs_bbox_extended", "true");
            $oLayer->setMetaData("wfs_enable_request", "*");
            $oLayer->setMetadata("wfs_namespace_uri", "http://www.opengis.net/wfs");
            //$oLayer->setMetadata("wfs_namespace_prefix", "wfs");
            //$oMap->setMetaData("wfs_enable_request", "*");
        }
        else
            $oLayer->setMetaData("wfs_enable_request", "!*");
        //$oMap->setMetaData("wfs_enable_request", "!*");

        
        
        
        if ($layerInfo["gml_include_items"]!="") {
            // ows_include_items needed when querying getFeatureInfo with format text/plain
            $oLayer->setMetaData('ows_include_items', str_replace(", ",",",  $layerInfo["gml_include_items"]));
            $oLayer->setMetaData('gml_include_items', str_replace(", ",",", $layerInfo["gml_include_items"]));
            $oLayer->set("template", "consultable");
        }
        
        if($delete_layer) {
            $oLayer->set('status', MS_DELETE);
        } else {
            if($oLayer->getMetadata('geoide_wms_srs'))
                $oLayer->removeMetaData("geoide_wms_srs");
            if ($oLayer->getMetadata('geoide_wfs_srs'))
                $oLayer->removeMetaData("geoide_wfs_srs");
        }
        
    }
    
    /**
     * 
     */
    protected function CSAtom_GenerateAtomFiles($cur_map_path, $mapname, $m_mapWms, $wxsInfo){
    	
    	$service_idx = $wxsInfo["account_id"];
    	$geosourceUrl = $wxsInfo["geosourceUrl"];
    	$urlServerData = $wxsInfo["urlServerData"];
    
    	//Creation du repertoire /Publication/ATOM
    	$dirATOM = $cur_map_path . "ATOM/";
    	$this->getLogger()->debug(__METHOD__,array("dir_atom"=>$dirATOM, "mapname"=> $mapname));
    	if (!file_exists($dirATOM)) {
    		mkdir($dirATOM, 0777, true);
    	}
    
    	//Suppression des ATOM précédemment générés pour cette carte
    	$this->CSAtom_DeleteAtomFiles($cur_map_path, $mapname);
    
    	//Creation du repertoire /Publication/ATOM/nom_carte
    	$dirATOMlayer = $dirATOM . $mapname . "/";
    	if (!file_exists($dirATOMlayer)) {
    		mkdir($dirATOMlayer, 0777, true);
    	}
    	 
    
    	$urlATOM = str_ireplace("WMS/","ATOM/", $m_mapWms->getMetaData("wms_onlineresource"));
    	$urlATOM = str_ireplace("?",".atom", $urlATOM);
    	$dateTime = date("c");
    
    	$xml = '<?xml version="1.0" encoding="utf-8"?> ' . "\n"
    			. '<feed xmlns="http://www.w3.org/2005/Atom" '
    					. 'xmlns:georss="http://www.georss.org/georss" '
    							. 'xmlns:inspire_dls="http://inspire.ec.europa.eu/schemas/inspire_dls/1.0" '
    									. 'xml:lang="' . $m_mapWms->getMetaData("wms_languages") . '">'  . "\n"
    											. ' <title>Service de téléchargement INSPIRE de la carte ' . $m_mapWms->getMetaData("wms_title") . '</title>' . "\n"
    													. ' <subtitle>Service de téléchargement INSPIRE de la carte ' . $m_mapWms->getMetaData("wms_title") . ' : ' . $m_mapWms->getMetaData("wms_abstract") . '</subtitle>' . "\n"
    															//URL Self
    	. ' <link href="' . $urlATOM . '" rel="self" type="application/atom+xml" hreflang="' . $m_mapWms->getMetaData("wms_languages") . '" title="' . $m_mapWms->getMetaData("wms_title") . '" />' . "\n"
    
    			//URL GeoSource
    	//. ' <link href="' . $geosourceUrl . '/metadata.show?uuid='. $m_mapWms->getMetaData("metadata_atom_uuid"). '" rel="self" type="application/atom+xml" hreflang="' . $m_mapWms->getMetaData("wms_languages") . '" title="' . utf8_encode($m_mapWms->getMetaData("wms_title")) . '" />' . "\n"
    
    	//ID (Self URL)
    	. ' <id>' . $urlATOM . '</id>' . "\n"
    			. ' <rights>Accès : ' . ($m_mapWms->getMetaData("wms_accessconstraints")) . '; Utilisation : ' . ($m_mapWms->getMetaData("wms_fees")) . '; ' . ($m_mapWms->getMetaData("wms_contactorganization")) . '</rights>' . "\n"
    					. ' <updated>' . $dateTime . '</updated>' . "\n"
    							. ' <author>' . "\n"
    									. ' <name>' . ($m_mapWms->getMetaData("wms_contactorganization")) . '</name>' . "\n"
    											. ' <email>' . $m_mapWms->getMetaData("wms_contactelectronicmailaddress") . '</email>' . "\n"
    													. ' </author>' . "\n";
    
    	for ($i = 0; $i < $m_mapWms->numlayers; $i++){
    		$oLayer = $m_mapWms->getLayer($i);
    		
    		
    		foreach($wxsInfo["layers"] as $key => $value){
    			//$this->getLogger()->debug(__METHOD__,array("layername" => $oLayer->name, "wxsName"=>$value["wxsname"]));
    			if ($oLayer->name == $value["wxsname"]){
    				//$this->getLogger()->debug(__METHOD__,array("layername" => $oLayer->name, "atom"=>$wxsInfo["layers"][$key]["atom"]));
    				//BF changment de condition cf plus bas
    				$bTreatForAtom = ($wxsInfo["layers"][$key]["atom"]==true); 
    				break;
    			}
    		}
    		
    		if($bTreatForAtom){
    			//TODO understand previous behaviour
    		//if(strcasecmp($oLayer->getMetaData('TELECHARGEABLE'), "ON") == 0){
    			$urlATOM_layer = str_ireplace(".atom", "/" . $oLayer->name . ".atom", $urlATOM);
    
    			$xml .= ' <entry>' . "\n"
    					. '   <title>' . ($oLayer->getMetaData("wms_title")) . '</title>' . "\n"
    							. '   <id>' . $urlATOM_layer . '</id>' . "\n"
    									. '   <inspire_dls:spatial_dataset_identifier_code>GeoSource</inspire_dls:spatial_dataset_identifier_code>' . "\n"
    											. '   <inspire_dls:spatial_dataset_identifier_namespace>' . $geosourceUrl . '</inspire_dls:spatial_dataset_identifier_namespace>' . "\n"
    													. '   <link href="' . $oLayer->getMetaData("wms_metadataurl_href") . '" '
    															. 'rel="related" '
    																	. 'type="text/html" '
    																			. 'hreflang="' . $m_mapWms->getMetaData("wms_languages") . '" />' . "\n"
    																					. '   <link href="' . $urlATOM_layer . '" '
    																							. 'rel="alternate" '
    																									. 'type="application/atom+xml" '
    																											. 'title="' . ($oLayer->getMetaData("wms_title")) . '" '
    																													. 'hreflang="' . $m_mapWms->getMetaData("wms_languages") . '" />' . "\n"
    																															. '   <updated>' . $dateTime . '</updated>' . "\n"
    																																	. ' </entry>' . "\n";
    			$this->CSAtom_GenerateAtomLayerFile($mapname, $service_idx, $geosourceUrl, $oLayer, $m_mapWms, $dirATOM, $urlATOM, $urlATOM_layer, $dateTime, $urlServerData);
    		}
    	}
    
    	$xml .= '</feed>';
    	return file_put_contents($dirATOM . $mapname . ".atom", str_ireplace("&", "&amp;", $xml));
    }
    
    protected function CSAtom_GenerateAtomLayerFile($mapname, $service_idx, $geosourceUrl, $oLayer, $m_mapWms, $dirATOM, $urlATOM, $urlATOM_layer, $dateTime, $urlServerData){
    	$xml = '<?xml version="1.0" encoding="utf-8"?> ' . "\n"
    			. '<feed xmlns="http://www.w3.org/2005/Atom" '
    					. 'xmlns:georss="http://www.georss.org/georss" '
    							. 'xmlns:inspire_dls="http://inspire.ec.europa.eu/schemas/inspire_dls/1.0" '
    									. 'xml:lang="' . $m_mapWms->getMetaData("wms_languages") . '">' . "\n"
    											. ' <title>Service de téléchargement INSPIRE de la donnée ' . ($oLayer->getMetaData("wms_title")) . '</title>' . "\n"
    													. ' <subtitle>Service de téléchargement INSPIRE de la donnée ' . ($oLayer->getMetaData("wms_title")) . ' : ' . ($oLayer->getMetaData("wms_abstract")) . '</subtitle>' . "\n"
    															//URL Self
    	. ' <link href="' . $urlATOM_layer . '" rel="self" type="application/atom+xml" hreflang="' . $m_mapWms->getMetaData("wms_languages") . '" title="' . ($m_mapWms->getMetaData("wms_title")) . '" />' . "\n"
    
    			//ID (Self URL)
    	. ' <id>' . $urlATOM_layer . '</id>' . "\n"
    			. ' <rights>Accès : ' . ($m_mapWms->getMetaData("wms_accessconstraints")) . '; Utilisation : ' . ($m_mapWms->getMetaData("wms_fees")) . '; ' . ($m_mapWms->getMetaData("wms_contactorganization")) . '</rights>' . "\n"
    					. ' <updated>' . $dateTime . '</updated>' . "\n"
    							. ' <author>' . "\n"
    									. ' <name>' . ($m_mapWms->getMetaData("wms_contactorganization")) . '</name>' . "\n"
    											. ' <email>' . $m_mapWms->getMetaData("wms_contactelectronicmailaddress") . '</email>' . "\n"
    													. ' </author>' . "\n";
    
    
    	// Boucle sur les formats et les projections
    	//TODO à passer en paramètre
      $CARMEN_DOWNLOAD_FORMATS = array(
            		"SHAPE" => "application/x-shapefile",
            		"MIDMIF" => "application/binary",
            		"KML" => "application/vnd.google-earth.kml+xml",
      );
    	
    	$CARMEN_URL_DOWNLOAD = $urlServerData."/ATOM/{USER}/{MAPFILE}?SERVICE=WFS&VERSION=1.1.0&REQUEST=GetFeature&TYPENAME={LAYER}&OUTPUTFORMAT={FORMAT}&SRSNAME={PROJECTION}";
    	
    	foreach($CARMEN_DOWNLOAD_FORMATS as $format => $mimeType) {
    		$url = $CARMEN_URL_DOWNLOAD;
    		$url = str_replace('{FORMAT}', $format, $url);
    		$url = str_replace('{MAPFILE}', $mapname . '.atom', $url);
    		$url = str_replace('{USER}', $service_idx, $url);
    		$url = str_replace('{LAYER}', $oLayer->name, $url);
    		$turl = $url;
    		$projs = explode(' ', trim($oLayer->getMetaData("wfs_srs")));
    
    		for ($k = 0; $k < count($projs); $k++) {
    			$projection = $projs[$k];
    			$projType = explode(':', $projection);
    			$url = str_replace('{PROJECTION}', $projection, $turl);
    
    			$xml .= ' <entry>' . "\n"
    					. '   <title>' . ($oLayer->getMetaData("wms_title")) . ' en projection ' . $projection . ' (' . $format . ')</title>' . "\n"
    							. '   <id>' . $urlATOM_layer . '</id>' . "\n"
    									. '   <link href="' . $oLayer->getMetaData("wms_metadataurl_href") . '" '
    											. 'rel="describedby" '
    													. 'type="text/html" '
    															. 'hreflang="' . $m_mapWms->getMetaData("wms_languages") . '" />' . "\n"
    																	. '   <link href="' . $urlATOM . '" '
    																			. 'rel="up" '
    																					. 'type="application/atom+xml" '
    																							. 'title="Service ATOM parent" '
    																									. 'hreflang="' . $m_mapWms->getMetaData("wms_languages") . '" />' . "\n"
    																											. '   <link href="' . $url . '" '
    																													. 'rel="alternate" '
    																															. 'type="' . $mimeType . '" '
    																																	. 'title="' . ($oLayer->getMetaData("wms_title")) . ' au format ' . $format . ' dans la projection ' . $projection . '(http://www.opengis.net/def/crs/OGC/1.3/' . $projection . ')' . '" '
    																																			. 'hreflang="' . $m_mapWms->getMetaData("wms_languages") . '" />' . "\n"
    																																					. '   <updated>' . $dateTime . '</updated>' . "\n"
    																																							. '   <category term="http://www.opengis.net/def/crs/' . $projType[0] . '/0/' . $projType[1] . '" label="' . $projection . '"/>' . "\n"
    																																									. ' </entry>' . "\n";
    		}
    	}
    
    	$xml .= '</feed>';
    
    	return file_put_contents($dirATOM . $mapname . '/' . $oLayer->name . '.atom' , str_ireplace("&", "&amp;", $xml));
    }
    
    
    //Supprime les fichiers ATOM liés à une carte
    protected function CSAtom_DeleteAtomFiles($cur_map_path, $mapname){
    	$dirATOM = $cur_map_path . "ATOM/";
    
    	$file = $dirATOM . $mapname. ".atom";
    	if (file_exists($file)){
    		//ATOM Top
    		unlink($file);
    
    		//ATOM Dataset feed
    		$this->rrmdir($dirATOM . $mapname . "/");
    	}
    }
    
    //Supprime un répertoire et son contenu
    protected function rrmdir($dir) {
    	if (is_dir($dir)) {
    		$objects = scandir($dir);
    		foreach ($objects as $object) {
    			if ($object != "." && $object != "..") {
    				if (filetype($dir."/".$object) == "dir") $this->rrmdir($dir."/".$object); else unlink($dir."/".$object);
    			}
    		}
    		reset($objects);
    		rmdir($dir);
    	}
    }
    
    /**
     * Extract from layerfile, the geometry type and returns its value
     * @Get("/getLayerExtent/{file}/{layer}/{projectionMap}")
     * @QueryParam(name="directory", description="MAPFILE DIRECTORY", nullable=false)
     * @param string $file   mapfile name without extension
     * @param string $layer  layer name
     *            
     * @example GET /api/getLayerExtent/environnement/layer/layer1
     *         
     * @ApiDoc(
     *   section="Layer",
     *   description="get Layer information",
     *   requirements={
     *     {
     *       "name"="file",
     *       "dataType"="text",
     *       "requirement"="\w/\d+",
     *       "description"="mapfile name without extension"
     *     },
     *     {
     *       "name"="layer",
     *       "dataType"="text",
     *       "requirement"="\w/\d+",
     *       "description"="layer name"
     *     },
     *   },
     * )
     *
     */
    public function getLayerExtent(Request $request, ParamFetcher $paramFetcher, $file, $layer, $projectionMap){
        //reset mapserver error (to avoid warnings due to precedent errors)
        ms_ResetErrorList();
        
        if ( is_numeric($projectionMap) ){
        	$projectionMap = "init=".$projectionMap;
        }
        //load mapfile
        $directory = $paramFetcher->get("directory");
        $mapObj = $this->getMapObj($directory, $file);
        $oLayer = @$mapObj->getLayerByName($layer);
        
        if($oLayer){
            $projectionLayer = $oLayer->getProjection();
            if ( empty($projectionLayer) ) $projectionLayer = $projectionMap;
            else $projectionLayer = substr($oLayer->getProjection(), 1);
            $projectionLayer = str_replace(array("+init=", "init="), "", $projectionLayer);
            
            $oRect = $oLayer->getExtent();
            if ( $oRect && strtolower($projectionMap) != strtolower($projectionLayer) ){
                $projectionLayer = ms_newProjectionObj($projectionLayer);
                $projectionMap = ms_newProjectionObj($projectionMap);
                $oRect->project($projectionLayer, $projectionMap);
            }
            if ( !$oRect ) {
                $extent = array("minx" => null,
                                "miny" => null,
                                "maxx" => null,
                                "maxy" => null);
                switch($oLayer->type){
                    case MS_LAYER_RASTER :
                        $result = array(); $return_val = 0;
                        exec("gdalinfo ".$oLayer->data, $result, $return_val);
                        if ( $return_val==0 ){
                            foreach($result as $line){
                                $coords = array();
                                if ( preg_match("!Lower Right\s*\(\s*([\d.e+-]+)\s*,\s*([\d.e+-]+)\s*\)!i", $line, $coords) ){
                                    $extent["maxx"] = $coords[1];
                                    $extent["miny"] = $coords[2];
                                }
                                if ( preg_match("!Upper left\s*\(\s*([\d.e+-]+)\s*,\s*([\d.e+-]+)\s*\)!i", $line, $coords) ){
                                    $extent["minx"] = $coords[1];
                                    $extent["maxy"] = $coords[2];
                                }
                            }
                            
                        } 
                    break;
                }
                
                $extent = array_diff($extent, array(null));
                if ( count($extent)==4 ){
                    return $this->formatJsonSuccess(array("extent"=>$extent));
                } else {
                    return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, 'Internal Server Error', "Impossible de déterminer l'emprise de la couche");
                }
            }
            if($oRect){
                $extent = array("minx" => $oRect->minx,
                                "miny" => $oRect->miny,
                                "maxx" => $oRect->maxx,
                                "maxy" => $oRect->maxy);
                return $this->formatJsonSuccess(array("extent"=>$extent));
            }else{                
                return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, 'Internal Server Error', "Impossible de déterminer l'emprise de la couche");
            }
        }
        return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, 'Internal Server Error', $exception->getMessage());
        

    }
    
    /**
     * Wrapper for the Carmen getInformation service
     * @Post("/getInformation/{mapfile}")
     * @RequestParam(name="directory", description="MAPFILE DIRECTORY", nullable=false)
     * @RequestParam(name="layers", description="Layers Query parameters", nullable=false)
     * @RequestParam(name="fields", description="Fields Query parameters", nullable=false)
     * @RequestParam(name="briefFields", description="BriefFields Query parameters", nullable=false)
     * @RequestParam(name="shape", description="Shape Query parameters", nullable=false)
     * @RequestParam(name="showGraphicalSelection", description="showGraphicalSelection Query parameters", nullable=false)
     * @RequestParam(name="shapeType", description="shapeType Query parameters", nullable=true, default="box")
     * @RequestParam(name="maxCountPerLayer", description="maxCountPerLayer Query parameters", nullable=true, default="0") 
     * 
     * @param string $file   mapfile name without extension
     *            
     * @example POST /api/getInformation/maCarte
     *         
     * @ApiDoc(
     *   section="Layer",
     *   description="get Layer information",
     *   requirements={
     *     {
     *       "name"="mapfile",
     *       "dataType"="text",
     *       "requirement"="\w/\d+",
     *       "description"="mapfile name without extension"
     *     }
     *   },
     * )
     *
     */
    public function getInformation(Request $request, ParamFetcher $paramFetcher, $mapfile){
        
      //reset mapserver error (to avoid warnings due to precedent errors)
      ms_ResetErrorList();
        
      // constantes
      define("I_QUERY_MAX_REP", 1000);
      define("CARMEN_SHAPE_QUERY_BOX", 0);
      define("CARMEN_SHAPE_QUERY_CIRCLE", 1);
      define('MAX_DOUBLE', '1.7976931348623157e308');
      define('MIN_DOUBLE', '4.9e-324');
      // Query modes
      define('QM_OGR_FILTER', 0);
      define('QM_MAPSERVER_QUERY', 1);
      // Encoding constants
      define("OWS_ENCODING", "UTF-8");
      define("MAPFILE_ENCODING", "ISO-8859-1");
      // miscellaneous error codes used in CARMEN
      define("CARMEN_ERROR", 256);
      define("CARMEN_ERROR_SESSION_EXPIRED", 257);
      define("CARMEN_MSG_SESSION_EXPIRED", "Votre session a expiré. La carte va être rechargée dans son environnement initial.");
      
      //utility functions
      $OWSEncode = function ($str) {
          return $str;
          //return iconv(MAPFILE_ENCODING, OWS_ENCODING, $str);
      };

      $MapfileEncode = function ($str) {
        return $str;
       //return iconv(OWS_ENCODING, MAPFILE_ENCODING, $str);
      };

      $NoEncode = function ($str) {
       return $str;
      };

      $escape_quote = function ($str) {
        return str_replace(array("\"","\'"), array("\\\"","\\\'"), $str);
      };

      function quote($str) 
      {
        return '"' . $str . '"';
      }

      $getName = function($field) {
        return quote($field["name"]);
      };

      function lastChar($str)
      {
        $endCar = null;
        if (strlen($str)>0)
        $endCar = substr("$str", strlen("$str") - 1, 1);
        return $endCar;
      }

      function removeLastChar($str)
      {
        return (strlen($str)>0) ?
        substr("$str", strlen("$str") - 1, 1) :
        $str;
      }

      function mergeRectObj($rect1, $rect2)
      {
        $rectMerged = ms_newRectObj();
        $rectMerged->minx = min($rect1->minx, $rect2->minx);
        $rectMerged->miny = min($rect1->miny, $rect2->miny);
        $rectMerged->maxx = max($rect1->maxx, $rect2->maxx);
        $rectMerged->maxy = max($rect1->maxy, $rect2->maxy);
        return $rectMerged;
      }

      function rectObjToStr($rect)
      {
        return '(' . $rect->minx . ',' .
        $rect->miny . ',' .
        $rect->maxx . ',' .
        $rect->maxy . ')';
      }

      function shapeStrToRectObj($shapeStr)
      {
        $coords = explode(',', substr($shapeStr,1,strlen($shapeStr)-2));
        $rect = ms_newRectObj();
        if (count($coords)==4)
          $rect->setExtent(
           min($coords[0], $coords[2]), 
           min($coords[1], $coords[3]),
           max($coords[0], $coords[2]), 
           max($coords[1], $coords[3]));
        return $rect;
      }

      function datasetNameFromFilename($filename) {
        $basen = basename($filename);
        // removing suffixe if present
        $posSuf = strpos($basen,'.');
        if (!($posSuf===FALSE))
          $basen = substr($basen,0, $posSuf);

        //  echo $basen;
        return $basen;
      }

      // add a '.shp' suffix to filename that have not
      function forceShapefileExtension($filename) {
        $posSuf = strpos($filename,'.');
        if (($posSuf===FALSE))
          $filename = $filename . '.shp';
        return $filename;
      }

      function fill_template($template, $featValues, $fields) {
      	
      	$OWSEncode = function ($str) {
      		return $str;//iconv(MAPFILE_ENCODING, OWS_ENCODING, $str);
      	};
      	
      	$MapfileEncode = function ($str) {
      		return $str;//iconv(OWS_ENCODING, MAPFILE_ENCODING, $str);
      	};
      	
      	for($i=0; $i<count($fields); $i++) {
          $template = str_replace(
            '<' . $fields[$i]["name"] . '>', 
            $OWSEncode($featValues[$MapfileEncode($fields[$i]["name"])]),
            $template);
        }
        for($i=0; $i<count($fields); $i++) {
          $template = str_replace(
            '[' . $fields[$i]["name"] . ']', 
            $OWSEncode($featValues[$MapfileEncode($fields[$i]["name"])]),
            $template);
        }
        return $template;
      }

      // CARMEN AJAX error handling function
      function handle_error($errno, $errmsg) {
        if (error_reporting()>0) {
          if (strstr($errmsg, 'CARMEN_ERROR')!=FALSE) {
            $info = explode('|', $errmsg);
            $errno = intval($info[0]);
            $errmsg = $info[1];      
          }
          @$result = array(
            'success' => false,
            'errmsg' => $errmsg,
            'failureType' => $errno  
          );

          @header("Content-type:text/html");
          @header("HTTP/1.0 400 Error");
          echo json_encode($result);
          die();
        }
      }

      /**
       * Force a string into alphanumeric
       *
       * @param string $text
       * @param string $from_enc
       * @return string
       */
      function to7bit($text,$from_enc) {
        $text = mb_convert_encoding($text,'HTML-ENTITIES',$from_enc);

        //delete accents
        $out_text = preg_replace( array('/ß/','/&(..)lig;/', '/&([aouAOU])uml;/','/&(.)[^;]*;/'),
         array('ss',"$1","$1".'e',"$1"),
         $text);
        //$out_text = str_replace(" ", "_", $out_text);

        // removing non alpha-numeric chars
        $out_text = eregi_replace("[^a-z0-9./]",'_',$out_text);
        //\)|\(|\ |\+|\;|\:|\*|\?|\"|\'
        $out_text = eregi_replace("[\)\(\+;:\*\?]",'_',$out_text);
        $out_text = str_replace(" ", "_", $out_text);
        // returning cleaned string
        return $out_text;
      }

      /**
       * Watching process function
       * kill resource process after a timeout and echo error message
       * @param $ressource : resource process
       * @param $time : time out length in seconds
       * @param $msgErr : error message to be echoed
       * @return unknown_type
       */

      function watchProcess($ressource, $time, $msgErr) {
        $time = isset($time) ? $time : 20;
        $start = date("U");
        if (is_resource($ressource)) {
         $info = proc_get_status($ressource);
         while ($info!=FALSE && $info["running"]==TRUE && $time>0) {
           $info = proc_get_status($ressource);
           $now = date("U");
           if (($now - $start) >= $time) {
            proc_terminate($ressource);
            echo $msgErr;
            exit(0);
           }  
         }
        }
      }

      // array of descriptors for proc_open
      $proc_io_descriptor = array(
        0 => array("pipe", "r"),  // stdin is a pipe that the child will read from
        1 => array("pipe", "w"),  // stdout is a pipe that the child will write to
        2 => array("pipe", "a") // stderr is a file to write to
      );

      // carmen date pretty printer function
      function dateformat($value, $format) {
       $res = $value;
       if (strlen($value)==8) {
        switch($format) {
         case "JJMMAAAA":
         case "MMJJAAAA":
           $res = substr($value, 0, 2) . "/" . substr($value, 2, 2) . "/" . substr($value, 4, 4);  
          break;
         case "AAAAMMJJ":
         case "AAAAJJMM":
           $res = substr($value, 0, 4) . "/" . substr($value, 4, 2) . "/" . substr($value, 6, 2);  
          break;
        }
       }
       return $res;
      }
      
      
      
      
      $layersJSON = $paramFetcher->get("layers");
      $fieldsJSON = $paramFetcher->get("fields");
      $briefFieldsJSON = $paramFetcher->get("briefFields");
      $shapeStr = $paramFetcher->get("shape");
      $showGraphicalSelection = $paramFetcher->get("showGraphicalSelection");
      $shapeType = $paramFetcher->get("shapeType");
      $maxCountPerLayer = $paramFetcher->get("maxCountPerLayer"); 
      $directory = $paramFetcher->get("directory");
      
      
      $mapfile = $directory . "/" .$mapfile . ".map";
      
      
      //set_error_handler("handle_error", E_ALL | E_STRICT);
      //if (!loadService())
      //  trigger_error(CARMEN_ERROR_SESSION_EXPIRED . "|" . CARMEN_MSG_SESSION_EXPIRED . "|CARMEN_ERROR", E_USER_ERROR);

      //recupération des paramètres
      /*
      $mapfile = Request("map", REQ_GET_POST, "");
      $layersJSON =  stripslashes(Request("layers", REQ_GET_POST, ""));
      $shapeStr = stripslashes(Request("shape", REQ_GET_POST, ""));
      $fieldsJSON = stripslashes(Request("fields", REQ_GET_POST, ""));
      $briefFieldsJSON = stripslashes(Request("briefFields", REQ_GET_POST, ""));
      $showGraphicalSelection = stripslashes(Request("showGraphicalSelection", REQ_GET_POST, ""));
      $shapeType = stripslashes(Request("shapeType", REQ_GET_POST, "box"));
      $maxCountPerLayer = stripslashes(Request("maxCountPerLayer", REQ_GET_POST, "0"));
      */
      
      
      $layerNames = json_decode($layersJSON, true);
      $fieldsList = json_decode($fieldsJSON, true);
      $briefFieldsList = json_decode($briefFieldsJSON, true);
      $maxCountPerLayer = intval($maxCountPerLayer);
/*
      return $this->formatJsonSuccess(
        array(
          "mapfile"=>$mapfile, 
          "layers"=> $layerNames, 
          "fields" => $fieldsList,  
          "brief" => $briefFieldsList, 
          "maxCountPerLayer" => $maxCountPerLayer
        ));
*/      
      $limitReached = false;

      if ($mapfile == ""
        || $layerNames == false
        || $shapeStr==""
        || $fieldsList==false
        || $briefFieldsList==false
        || $showGraphicalSelection=="") {
          return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, 'Missing or wrong parameters in service call');
      }

      $shapeQueryType = strcmp($shapeType,"box")==0 ? CARMEN_SHAPE_QUERY_BOX : CARMEN_SHAPE_QUERY_CIRCLE;


      // building queryfilename
    /*
      $queryfileName = datasetNameFromFilename($mapfile) . '_' . $_SESSION["service"]['idx'] . '_' . session_id() . '.qy';
      $queryfileFullPath = CARMEN_DATA_PATH_CACHE . $queryfileName;
      $selectedShapes = array();
*/
      $totalCount = 0;
      $selExtent = ms_newRectObj();
      $selExtent->setExtent(MAX_DOUBLE, MAX_DOUBLE, MIN_DOUBLE, MIN_DOUBLE);

      if ($shapeQueryType==CARMEN_SHAPE_QUERY_BOX) {
        $coords = explode(',', $MapfileEncode(substr($shapeStr, 1, strlen($shapeStr)-2)));
      }
      else {
        $coords = explode(',', $MapfileEncode(substr($shapeStr, 1, strlen($shapeStr)-2)));
        $radius = array_pop($coords);
      }

      $oMap = ms_newMapObj($mapfile);
      $strObjJson = "{";

      for ($l=0; $l<count($layerNames); $l++) {

        $layerName = $MapfileEncode($layerNames[$l]);
        $layer = @$oMap->getLayerByName($layerName);
        
        
        
        $res = false;
        if ($layer && !$limitReached) {
          if ($shapeQueryType==CARMEN_SHAPE_QUERY_BOX) {
            if(count($coords)>=2) {
              if (count($coords)==4) {
                // querying by a rectangle
                // echo "querying by a rectangle";
                $qRectQuery = ms_newRectObj();
                $qRectQuery->setextent($coords[0], $coords[1], $coords[2], $coords[3]);
                $res = (@$layer->queryByRect($qRectQuery) == MS_SUCCESS);
              }
              else if (count($coords)==2) {
                // querying by a point
                //echo "querying by a point";
                $qPtQuery = ms_newPointObj();
                $qPtQuery->setXY($coords[0], $coords[1]);
                
                //@todo See how to set the tolerance
                $res = (@$layer->queryByPoint($qPtQuery, MS_MULTIPLE,1) == MS_SUCCESS);
              }
            }
          } 
          else {
            $qPtQuery = ms_newPointObj();
            $qPtQuery->setXY($coords[0], $coords[1]);
            $res = (@$layer->queryByPoint($qPtQuery, MS_MULTIPLE, $radius) == MS_SUCCESS);
          }
        }
        
         //if (!$res){
         //    throw new MapserverException("tt");
         //}
        
        //return $this->formatJsonSuccess(array("res"=> ($res!=null ? "yes" : "no"), "coords"=> $coords));
        
        if ($res && !$limitReached) {
          $selExtentLayer = ms_newRectObj();
          $selExtentLayer->setExtent(MAX_DOUBLE, MAX_DOUBLE, MIN_DOUBLE, MIN_DOUBLE);
          
          $count = $maxCountPerLayer>0 ?  min($maxCountPerLayer, $layer->getNumResults()) : $layer->getNumResults();
          $totalCount = $totalCount + $count; 

          //il fait couche par couche, pas element par element
          if($totalCount > I_QUERY_MAX_REP){
            $limitReached = true;
          }
/*
          $queryfileNameLayer = datasetNameFromFilename($mapfile) . '_' . $l . '_' . $_SESSION["service"]['idx'] . '_' . session_id() . '.qy';
          $queryfileFullPathLayer = CARMEN_DATA_PATH_CACHE . $queryfileNameLayer;
*/
          // with mapserver client wfs layers, mapserv seems to automatically 
          // encode data from iso to utf8 in memory... so we we must not do an 
          // extra encoding utf8->iso transformation
          switch ($layer->connectiontype) {
            case MS_WFS :
              $dataEncode = $NoEncode;
              break;
            default :
              $dataEncode = $MapfileEncode;
              break;
          }

          $layer->open();


          $fields = $fieldsList[$l];
          $briefFields = $briefFieldsList[$l];

          // writing json step by step...
          $strObjJson.= '"' . $layerName . '": ';
          $escapeFields = array_map($escape_quote, $fields);
          $quoteFields = array_map($getName, $escapeFields);
          $escapeBriefFields = array_map($escape_quote, $briefFields);
          $quoteBriefFields = array_map($getName, $escapeBriefFields);
          $strObjJson.= '{ "fields" : [' . implode(",", $quoteFields) . ', "extent", "fid", "geometryWKT"] ,';
          $strObjJson.= ' "briefFields" : [' . implode(",", $quoteBriefFields) . ', "extent", "fid", "geometryWKT"] ,';
          $strObjJson.= ' "data" : [';
          $strBriefData = ' "briefData" : [';

          for ($i=0;$i<$count;$i++) {
            $resObj = $layer->getResult($i);
            $feat = $layer->getShape($resObj);//, $resObj->tileindex);
            $strObjJson.= '[';
            for($j=0; $j<count($fields); $j++) {
              switch($fields[$j]["type"]) {
                case  "URL":
                  $value = '{ "href" : "'.  fill_template($fields[$j]["url"], $feat->values, $fields) .
                    '", "text" : "' . $escape_quote($OWSEncode($feat->values[$dataEncode($fields[$j]["name"])])) . '"},';
                  break;
                case "AAAAMMJJ":
                case "AAAAJJMM":
                case "JJMMAAAA":
                case "MMJJAAAA": 
                  $value =  $escape_quote($OWSEncode($feat->values[$dataEncode($fields[$j]["name"])]));
                  $value = dateformat($value, $fields[$j]["type"]);  
                  $value = '"' . $value . '",'; 
                  break;
               default:
                $value = '"' . $escape_quote($OWSEncode($feat->values[$dataEncode($fields[$j]["name"])])) . '",';
              }

              $strObjJson.=  $value;
            }

            $strBriefData.= '[';
            for($j=0; $j<count($briefFields); $j++) {
              switch($briefFields[$j]["type"]) {
               case "URL":
                $value = '{ "href" : "'.  fill_template($briefFields[$j]["url"], $feat->values, $briefFields) .
                  '", "text" : "' . $escape_quote($OWSEncode($feat->values[$dataEncode($briefFields[$j]["name"])])) . '"},';
                break;
               case "AAAAMMJJ":
               case "AAAAJJMM":
               case "JJMMAAAA":
               case "MMJJAAAA": 
                  $value =  $escape_quote($OWSEncode($feat->values[$dataEncode($briefFields[$j]["name"])]));
                  $value = dateformat($value, $fields[$j]["type"]);  
                  $value = '"' . $value . '",'; 
                  break;
               default:
                $value = '"' . $escape_quote($OWSEncode($feat->values[$dataEncode($briefFields[$j]["name"])])) . '",';
              }
              $strBriefData.= $value ;

            }
            $wktL = $OWSEncode($feat->toWkt());
            $boundsL = $OWSEncode(rectObjToStr($feat->bounds));
            $shapeindexL = $OWSEncode($resObj->shapeindex);

            $selExtentLayer = mergeRectObj($selExtentLayer, $feat->bounds);      
            $strObjJson.= '"' . $boundsL . '"';
            $strObjJson.= ', "' . $shapeindexL . '"';

            //Ajout de la géométrie
            $strObjJson.= ', "' . $wktL . '"';

            $strObjJson.= '],';

            $strBriefData.= '"' . $boundsL . '"';
            $strBriefData.= ', "' . $shapeindexL . '"';

            //Ajout de la géométrie
            $strBriefData.= ', "' . $wktL . '"';
            //$strBriefData.= ', "' . "()" . '"';

            $strBriefData.= '],';

            $selectedShapes[] = array($layer->index, $resObj->tileindex, $resObj->shapeindex);

            $feat->free();
          }

          $selExtent = mergeRectObj($selExtent, $selExtentLayer);
          $strBriefData = ($count>0) ? substr($strBriefData, 0, strlen($strBriefData)-1) : $strBriefData;
          $strBriefData.= '],';
          $strObjJson = ($count>0) ? substr($strObjJson, 0, strlen($strObjJson)-1) : $strObjJson;
          $strObjJson.= '],';
          $strObjJson.= $strBriefData;
          $strObjJson.= '"extent" : "' . $OWSEncode(rectObjToStr($selExtentLayer)) . '",';
          $strObjJson.= '"queryfile" : "' . '"';
          $strObjJson.= '},';
/*
          if ($showGraphicalSelection)
            $oMap->saveQuery($queryfileFullPathLayer);
*/
          $layer->close();
        }
/*
        if ($showGraphicalSelection) {
          // saving full selection
          for ($i=0;$i<count($selectedShapes);$i++) {
            // making a new query for each first shape ($i==0) 
            $oMap->queryByIndex($selectedShapes[$i][0], $selectedShapes[$i][1], $selectedShapes[$i][2], $i!=0);
          } 
          $oMap->saveQuery($queryfileFullPath);
        }
 */ 
       }
      $strObjJson = lastChar($strObjJson)==',' ?
      substr($strObjJson, 0, strlen($strObjJson)-1) :
      $strObjJson;

      $strObjJson.= '}';
      if(!$limitReached){
        $res = array(
          "totalCount" => $totalCount, 
          "extent" => $OWSEncode(rectObjToStr($selExtent)),
          "results" => json_decode($strObjJson, true),
          "queryfile" => ""
          );
        /*
        // write json result
        echo '({"totalCount":"' .$totalCount . 
          '","extent":"' . $OWSEncode(rectObjToStr($selExtent)) . 
          '" , "results":' . $strObjJson . 
          ' , "queryfile":"' . $queryfileFullPath . '"})';*/
      }else{
        $res = array(
          "iQueryMaxRepReached" => $totalCount, 
          "I_QUERY_MAX_REP" => I_QUERY_MAX_REP
          );
        /*
        echo '({"iQueryMaxRepReached":"' .$totalCount .
        '","I_QUERY_MAX_REP":"' . I_QUERY_MAX_REP .
          '"})';*/
      }

      $oMap->free(); unset($oMap);
      return $this->formatJsonSuccess($res);
    }
    
    protected function getRandomColor(){
        $num = mt_rand ( 0, 0xffffff );
        return '#'. sprintf ( "%06x" , $num );
        return '#'.sprintf('$06s', dec2hex(mt_rand(0, 15)));
    }
}