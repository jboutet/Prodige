var _T_LAYERTREE = function (key) {
  var root = "LayerTree.";
  var tradution = _t(root + key);
  return (tradution==root + key ? key : tradution);
};

var TREE_INCREMENT = 20;
var TREE_INCREMENT_FORMULA = 'parseInt('+TREE_INCREMENT+')*(parseInt(values.record.getDepth())+1)';
var TREE_INCREMENT_FORMULA_IMAGE = 'parseInt('+TREE_INCREMENT+')*(parseInt(values.record.getDepth())+2)+5';
/**
 * Carmen.Model.MapGroup
 */
Ext.define('Carmen.Model.MapGroup', {
  extend: 'Ext.data.Model',
  idProperty : 'groupId',
  
  getProxy : function(mapId){
    return Ext.Factory.proxy({
      type: 'rest',
      model : (this instanceof Carmen.Model.MapGroup ? this.$className : 'Carmen.Model.MapGroup'),
      url: Routing.generate('carmen_ws_mapgroup', {
        mapId : ( Ext.isDefined(mapId) 
                  ? mapId 
                  : ( Ext.isDefined(Carmen.user.map) 
                      ? Carmen.user.map.get('mapId') 
                      : ( this instanceof Carmen.Model.MapGroup 
                          ? this.getMap().get('mapId') 
                          : '{mapId}')))
      })
    });
  },
  fields: [{
    // MapGroup Id (not saved)
      name : "groupId", 
      persist : false,
      type : "int",
      allowNull: false,
      defaultValue: 0
    },
    {name: "groupName",           type: "string"}
  ]
});

Ext.define("Carmen.LayerTree.GridView", {
  xtype : 'layertreegridview',
  extend : 'Ext.tree.View',
  
    rowTpl: [
        '{%',
            'var dataRowCls = values.recordIndex === -1 ? "" : " ' + Ext.baseCSSPrefix + 'grid-row";',
        '%}',
        '<tr class="{[values.rowClasses.join(" ")]} {[dataRowCls]}" {rowAttr:attributes} {ariaRowAttr}>',
            '<tpl for="columns">' +
                '{%',
                    'parent.view.renderCell(values, parent.record, parent.recordIndex, parent.rowIndex, xindex - 1, out, parent)',
                 '%}',
            '</tpl>',
        '</tr>',
        '<tpl if="icon>',
          '<tr class="{[values.rowClasses.join(" ")]} {[dataRowCls]}" {rowAttr:attributes} {ariaRowAttr}>',
            '<td colspan="{columns.length}">' ,
            '<div class="{iconCls}Container" style="width:100%;overflow-x:hidden;overflow-y: auto;max-height: 300px;">' ,
                  '<img onload="javascript:this.style.display = \'\';" onerror="javascript:this.style.display = \'none\';"',
                  ' src="{[this.noCacheIcon(values)]}" role="presentation" style="margin-left:{['+TREE_INCREMENT_FORMULA_IMAGE+']}px;"' ,
                  ' class="{childCls} {baseIconCls} {baseIconCls}-<tpl if="leaf">leaf<tpl else>parent</tpl> {iconCls}"',  
                  '/>',
                    '</div>',
            '</td>',
        '</tr>', 
        '</tpl>',
        {
            priority: 0,
            
                noCacheIcon : function(values){
                  var record = values.record;
                  var url = record.get('icon');
                  if ( url && url.indexOf('?')==-1 ) url+="?";
                  return (url ? url+'&time='+(new Date().getTime())+Math.floor(Math.random()*100)+'&rowId='+values.rowId : url);
                }
        }
    ],
  width: '100%',
    shrinkWrap : 3,
  initComponent : function(){
    var me = this;
    Ext.apply(me, {
      animate: false,  
      plugins: { 
        ptype: 'treeviewdragdrop',
        appendOnly: false,
        allowParentInserts : true,
        ddGroup: me.ddGroup,
        dropZone : {
          onNodeDrop : function(targetNode, dragZone, e, data){
            if ( this.currentPosition=="append" ){
              var hasChild = this.overRecord.childNodes.length>0;
              if ( hasChild ){
                targetNode = targetNode.nextSibling;
                this.overRecord = this.overRecord.childNodes[0];
                this.currentPosition = "before";
              }
            }
            return Ext.view.DropZone.prototype.onNodeDrop.call(this, targetNode, dragZone, e, data);
          }
        }
      }
    });
    
    me.callParent(arguments);
    
    me.on('refresh', me.autoExpandDataColumn, me);
  },
  
  autoExpandDataColumn : function(){
    var me = this.grid;
    var container = Ext.query( '#'+me.getId()+' .'+Ext.baseCSSPrefix + 'grid-item-container', false)[0];
    if ( !container ) return;
    var scrollWidth = 0;
    var decalage = 4;
    var actionColumns = Ext.query( '#'+me.getId()+' .'+Ext.baseCSSPrefix + 'grid-item-container .x-grid-cell-last', false);
    var dataColumns   = Ext.query( '#'+me.getId()+' .'+Ext.baseCSSPrefix + 'grid-item-container .x-grid-cell-first', false);
    Ext.each(actionColumns, function(actionColumn, index){
      var dataColumn = dataColumns[index];
      if ( !dataColumn ) return;
      var actionWidth = 0; 
      Ext.each(Ext.query('#'+actionColumn.getId()+' [role=button]', false), function(elt){ actionWidth += elt.getWidth()+1; });
      actionColumn.setWidth(actionWidth+decalage);
      dataColumn.setWidth(container.getWidth()-actionWidth-decalage-scrollWidth);
    })
  }
});
Ext.define("Carmen.LayerTree.Grid", {
	bufferedRenderer : false,
    shrinkWrap : 3,
  extend : 'Ext.tree.Panel',
  margins: '5 0 5 5',
  layout : 'fit',
  /*maxHeight: 300,*/
  width: '100%',
  useArrows: true,
  border: false,
  hideHeaders: true,
  //disableSelection: true,
  //containerScroll:true,
  //fitToFrame: true,
  rootVisible: true,//rootVisible: false,
  
  appUI : null,
  
    viewType: 'layertreegridview',
  initComponent : function(){
    var me = this;
    var appUI = this.appUI;
    var store = this.store;
    if ( !appUI ) return;
    me.on('afterlayout', function(){
      var container = Ext.query( '#'+me.getId()+' .'+Ext.baseCSSPrefix + 'grid-item-container', false)[0];
      if ( container ) 
        container.setWidth(me.up().getWidth()-18);
      me.getView().autoExpandDataColumn();
      //me.setMinHeight(me.up('#dataPanel').getHeight()); me.up().setHeight(me.up('#dataPanel').getHeight()-50); me.setHeight('100%')
    });
    me.viewConfig = {
      ddGroup : 'layerTreePanel',
      listeners : {
        drop : function( nodeHTMLElement, dataObject, dropDataModel, dropPosition, eOpts )
        {
          var dragDataModel = dataObject.records;
          if ( !dragDataModel.length ) return;
          dragDataModel = dragDataModel[0];
          var view = dataObject.view;
          var map = view.getStore().OL_MAP;
          var mapDrop = map.getItemById(dropDataModel.get('OL_ITEM').id);
          var mapDrag = map.getItemById(dragDataModel.get('OL_ITEM').id);
          
          switch(dropPosition){
            case "before" :
              map.insertItemBefore(mapDrag, mapDrop, true);
            break;
            case "after"  : 
              map.insertItemAfter(mapDrag, mapDrop, true);
            break;
            case "append" : 
              map.removeItemById(mapDrag.id, false);
              map.addItem(mapDrag, mapDrop);
            break;
          }
        },
        checkchange: function(node, checked, eOpts) 
        {
          // changing layer visibility   
          if (node.isLeaf()) {
            if (node.data.OL_ITEM) {
              // updating layer visibility on map
              var layer_id = node.data.OL_ITEM.id;
              var layer =  app.mapContent.getItemById(layer_id);
              layer.setVisibility(checked);
              node.set("visible", checked);
            }
          }
          me.getStore().fireEvent('nodechange');
          // populating to children
          node.eachChild(
            function(n) {
              n.set('checked', this.parentState);
              n.getOwnerTree().getView().fireEvent('checkchange', n, this.parentState);
              return true;
            },
            { parentState: checked }
          );
          // advertising parent node
          node.bubble(
            function(n) {
              if (n==this.startNode) {
                return true;
              }
              var checked = this.currentState;
              for (var i=0; i<n.childNodes.length; i++) {
                checked = checked || (n.childNodes[i].get('checked')===true);
              }
              n.set('checked', checked);
              return true;
            },
            { currentState: checked, startNode : node }
          );
          me.getView().autoExpandDataColumn();
        }
      }
    };
    me.columns = [
    {
        hideMode: 'visibility',
        xtype: 'treecolumn',
        dataIndex: 'text',
        cellWrap: true,
        cellTpl : [
              '<div class="row0" style="width:100%;" title="{value}">',
              '<div class="row0_first" style="width:{['+TREE_INCREMENT_FORMULA+']}px;" >',
              '<tpl for="lines">',
                  '<img src="{parent.blankUrl}" class="{parent.childCls} {parent.elbowCls}-img ',
                  '{parent.elbowCls}-<tpl if=".">line<tpl else>empty</tpl>" role="presentation"/>',
              '</tpl>',
              '<img src="{blankUrl}" class="{childCls} {elbowCls}-img {elbowCls}',
                  '<tpl if="isLast">-end</tpl>',
                  '<tpl if="expandable">-plus {expanderCls}</tpl>',
                  '" role="presentation"/>',
              '</div>',
              '<tpl if="checked !== null">',
                  '<icon id="node-icon-{record.data.nodeId}">{[this.drawNodeIcon(values)]}</icon>'+
              '</tpl>',
              '<div class="row0_second">',
              '<tpl if="href">',
                  '<a href="{href}" role="link" target="{hrefTarget}" class="{textCls} {childCls}">{value}</a>',
              '<tpl else>',
                  '<span class="{textCls} {childCls} layerTreeNode',
                    
                  '<tpl if="values.record.data.type==&quot;group&quot;">',
                    ' cmnGroupNode">',
                  '<tpl else>',
                    ' cmnLayerNode">',
                  '</tpl>',
                      
                  '<tpl if="values.record.data.removable">', 
                      '<a style="color: black;" href="{[this.edit(values)]}"><ins>{value}</ins></a>',
                    '<tpl else>',
                      '{value}',
                    '</tpl>',
                      
                  '</span>',
                  '<br/>',
               '</tpl>', 
              '</div>',
              '</div>',
              {
                nativeCheckboxCls : function(){
                	return Ext.tree.Column.prototype.checkboxCls;
                },
                
                drawNodeIcon : function(values){
                  return me.drawNodeIcon(values);
                },

                edit : function(values){
                  var record = values.record;
                  var func = "edit"+Ext.util.Format.capitalize(record.get('type'));
                  return "javascript:Carmen.application.ui.layerTreePanel."+func+"("+record.get('nodeId')+")";
                }
              }
              
        ]
    },
    {
      xtype: 'actioncolumn',
      width : 7-0,
      dataIndex: 'text',
      align : 'right',
      items: [
      {
        icon: '/bundles/carmenweb/images/metadata.png',
        text : 'site',
        tooltip : 'Visualiser la page métadonnée associée',
        handler: function(grid, rowIndex, colIndex) {
          var rec = store.getAt(rowIndex); 
          var treenode = store.getAt(rowIndex); 
          var layer = Carmen.user.map.getLayerById(treenode.data.layerId);
          if ( !layer ) return;
          window.open(layer.get('hrefLayerMetadataUuid'));
        },                                   
        getClass: function(v, metadata, r, rowIndex, colIndex, store) {     
          if ( !r.data.hasMetadata ){return "x-hidden-display";}
          if ( !Carmen.user.map ){return "x-hidden-display";}
          var layer = Carmen.user.map.getLayerById(r.data.layerId);
          if ( !layer ) {return "x-hidden-display";}
          if ( !layer.get('hrefLayerMetadataUuid') ) {return "x-hidden-display";}
          return "";
        },
        scope : this
      },
      {
        icon: '/bundles/carmenweb/images/file.png',
        text : 'site',
        tooltip : 'Visualiser le fichier métadonnée associé',
        handler: function(grid, rowIndex, colIndex) {
          var rec = store.getAt(rowIndex); 
          var treenode = store.getAt(rowIndex); 
          var layer = Carmen.user.map.getLayerById(treenode.data.layerId);
          if ( !layer ) return;
          window.open(layer.get('hrefLayerMetadataFile'));
        },                                   
        getClass: function(v, metadata, r, rowIndex, colIndex, store) {     
          if ( !r.data.hasMetadata ) return "x-hidden-display";
          if ( !Carmen.user.map )return "x-hidden-display";
          var layer = Carmen.user.map.getLayerById(r.data.layerId);
          if ( !layer ) return "x-hidden-display";
          if ( !layer.get('hrefLayerMetadataFile') ) return "x-hidden-display";
          return "";
        },
        scope : this
      },{
        icon: '/bundles/carmenweb/images/transparency-new.png',
        tooltip : _T_LAYERTREE('tools.transparency'),
        handler: function(grid, rowIndex, colIndex) {
          
          me.winTransparence = me.winTransparence || Ext.create('Ext.window.Window', {
            closeAction :'hide',
            width : 500,
            padding : 10,
            items : [{
              html : _T_LAYERTREE("transparency.title")
            }, {
              xtype : 'form',
              items : [{
                xtype : 'sliderfield',
                fieldLabel : _T_LAYERSTYLE('fields.layerTransparency'),
                name : 'layerTransparency',
                minValue : 0,
                maxValue : 100,
                value : -1,
                flex : 9,
                width : 200, 
                listeners : {
                  afterrender : function(){
                    this.textComponent = this.el.createChild({
                      tag : "label",
                      role : "textbox",
                      style : 'margin-left:5px',
                      cls : "x-form-item-label-default",
                      html : this.getValue()+'%'
                    });
                    this.inputEl.setWidth(this.el.getWidth());
                    this.bodyEl.setWidth(this.el.getWidth());
                    this.el.setWidth(this.el.getWidth()+70);
                  },
                  change : function() {
                    var transparency = this.getValue();
                    var opacity = 100-transparency;
                    this.textComponent = this.textComponent || win.down('[name=layerTransparency_text]');
                    if ( this.textComponent )
                      this.textComponent.setHtml(transparency+'%');
                    
                    var form = me.winTransparence.down('form');
                    var treenode = form.treenode;
                    me.store.OL_MAP.getItemById(treenode.get('OL_ITEM').id).setOpacity(opacity);
                  }
                }
              }]
            }],
            buttons : [{
              text : _T_LAYERTREE("buttons.valid"), 
              handler : function(btn){
                var form = me.winTransparence.down('form');
                var treenode = form.treenode;
                var layer = form.getRecord();
                if ( !form.isValid() ) return;
                if ( !form.isDirty() ) {
                  btn.up('window').close();
                  return;
                }
                form.updateRecord(layer),
                treenode.set("opacity", (100-layer.get('layerTransparency'))/100, {silent : true});
                me.store.OL_MAP.getItemById(treenode.get('OL_ITEM').id).setOpacity(treenode.get('opacity')*100);
                me.store.fireEvent("nodechange", me)
                
                btn.up('window').close();
              }
            }, {
              text : _T_LAYERTREE("buttons.cancel"),
              handler : function(btn){
                var form = me.winTransparence.down('form');
                var treenode = form.treenode;
                me.store.OL_MAP.getItemById(treenode.get('OL_ITEM').id).setOpacity(treenode.get('opacity')*100);
                btn.up('window').close();
              }
            }]
          });
          
          var treenode = store.getAt(rowIndex); 
          var layer = Carmen.user.map.getLayerById(treenode.data.layerId);
          if ( !layer ) return;
          me.winTransparence.setTitle('Transparence de la couche "'+layer.get('layerTitle')+'"');
          me.winTransparence.down('form').loadRecord(layer);
          me.winTransparence.down('form').treenode = treenode;
          me.winTransparence.show();
        },                                   
        getClass: function(v, metadata, r, rowIndex, colIndex, store) {          
          if(!r.data.leaf || !r.data.removable ){                                                                
            return "x-hidden-display";
          }else{
            return "";
          }
        },
        scope : this
      },
      {
        icon: '/bundles/carmenweb/images/plus.png',
        tooltip : _T_LAYERTREE('tools.add_group'),                                   
        getClass: function(v, metadata, r, rowIndex, colIndex, store) {          
          if(r.data.leaf) {                                                                      
            return "x-hidden-display";
          }else{
            return "";
          }
        },
        handler: this.addGroupNodeAction,
        scope : this
      },
      {
        icon: '/bundles/carmenweb/images/delete.png',
        tooltip : _T_LAYERTREE('tools.delete'),
        handler: function(grid, rowIndex, colIndex){this.deleteNode(grid, rowIndex, colIndex)},
        scope : this,                                   
        getClass: function(v, metadata, r, rowIndex, colIndex, store) {    
          if ( !r.data.removable ){
            return "x-hidden-display";
          }      
          return "";
        }
      },
      {
        icon: '/bundles/carmenweb/images/loupe.gif',
        tooltip :  _T_LAYERTREE('tools.styles'),
        handler: function(grid, rowIndex, colIndex) {
          var treenode = store.getAt(rowIndex); 
          var layer = me.store.OL_MAP.getItemById(treenode.get('OL_ITEM').id);
          if ( !layer ) return;
          layer.OL_layers[0];
          //layer.zoom();
        },                                 
        getClass: function(v, metadata, r, rowIndex, colIndex, store) {     return "x-hidden-display";     
          if(!r.data.leaf || !r.data.removable ){                                                                
            return "x-hidden-display";
          }else{
            return "";
          }
        },
        scope : this
      }]
    }];
    
    
    this.callParent(arguments);
    
    this.attachEventsChange();
    
    
    this.getView().on('afteritemcollapse', function(node){
      node.set('expanded', false);
      node.commit();
      this.store.fireEvent('nodechange');
    });
    this.getView().on('afteritemexpand', function(node){
      node.set('expanded', true);
      node.commit();
      this.store.fireEvent('nodechange');
    });
  },
    
  plugins: [{
    ptype: 'nodedisabled'
  }],
  
  drawNodeIcon : function(values){
  	if ( values.record.checked===null ) return "";
    var tpl = new Ext.XTemplate(
      '{[this.resetScaleRange(values.record)]}' +/* reset des acceptMin/MaxScale car raffraichissement de l'arbre et besoin d'un recalcul. Ce calcul est ensuite valable pour toute la lecture du XTemplate*/
                  '<div {ariaCellCheckboxAttr}',
                      ' class="{childCls} {[this.nativeCheckboxCls()]} ' +
                      '<tpl if="checked">{[this.nativeCheckboxCls()]}-checked </tpl> DescartesLayersTree"' +
                      ' title="Cliquez pour masquer le thème">'+
      '<div class="{[this.checkboxCls]} '+
      '{[this.checkboxCls]}' +
      '<tpl if="values.record.data.type==&quot;group&quot;">Group<tpl else>Layer</tpl>',
      '<tpl if="values.record.data.checked">' +
        '<tpl if="this.acceptMinScaleRange(values.record)===true && this.acceptMaxScaleRange(values.record)===true">Visible' +
        '<tpl elseif="this.acceptMinScaleRange(values.record)===true">' +
          'ZoomOut'+
        '<tpl elseif="this.acceptMaxScaleRange(values.record)===true">' +
          'ZoomIn'+
        '<tpl else>' +
          'Visible' +
        '</tpl>' +
      '<tpl else>' +
      'Hidden' +
      '</tpl>' +
      '<tpl if="values.record.data.type==&quot;layer&quot;">' +
        '<tpl if="this.getLayerModelProperty(values.record, &quot;layerLegendScale&quot;)===true ' +
             ' && (this.acceptMinScaleRange(values.record)!==true ' +
             ' || this.acceptMaxScaleRange(values.record)!==true)">' +
          ' {[this.checkboxCls]}LayerByScale'+
        '</tpl>' +
      '</tpl>' +
      '"' +
      '<tpl if="values.record.data.type==&quot;layer&quot;">' +
        '<tpl if="this.getLayerModelProperty(values.record, &quot;layerLegendScale&quot;)===true">' +
          ' title="Légende selon l\'échelle"'+
        '</tpl>' +
      '</tpl>' +
      
      '></div> '+
                  '</div>' 
    , {
      checkboxCls : 'DescartesLayersTree',
                nativeCheckboxCls : function(){
                  return Ext.tree.Column.prototype.checkboxCls;
                },
      getLayerModelProperty : function(node, property){
        if ( !Carmen.user.map ) return null;
        var layer = Carmen.user.map.getLayerById(node.data.layerId);
        if ( !layer ) return null;
        return layer.get(property);
      },
      getLayerMapProperty : function(node, property){
        if ( !Carmen.user.map ) return null;
        var layer =  app.mapContent.getItemById(node.data.OL_ITEM.id);
        if ( !layer ) return null;
        return layer.get(property);
      },
      resetScaleRange : function(node){
        delete node.acceptMaxScaleRange;
        delete node.acceptMinScaleRange;
      },
      acceptMaxScaleRange : function(node){
        if ( !Carmen.user.map ) return true;
        if ( Ext.isDefined(node.acceptMaxScaleRange) ) return node.acceptMaxScaleRange;
        var layer =  app.mapContent.getItemById(node.data.OL_ITEM.id);
        if ( !layer || !layer.acceptMaxScaleRange ) return true;
        return node.acceptMaxScaleRange = layer.acceptMaxScaleRange();
      },
      acceptMinScaleRange : function(node){
        if ( !Carmen.user.map ) return true;
        if ( Ext.isDefined(node.acceptMinScaleRange) ) return node.acceptMinScaleRange;
        var layer =  app.mapContent.getItemById(node.data.OL_ITEM.id);
        if ( !layer || !layer.acceptMinScaleRange ) return true;
        return node.acceptMinScaleRange = layer.acceptMinScaleRange();
      }
    });
    return tpl.apply(values)
  },
  

  
  refreshIcons : function(){
  	this.store.each(function(node){
  		var icon = Ext.get('node-icon-'+node.get('nodeId'));
  		if ( icon ){
  			icon.setHtml(this.drawNodeIcon({record : node}))
  		}
  	}, this)
  },
  
  /**
   * add a group node action
   */
  addGroupNodeAction : function(grid, rowIndex, colIndex)
  {
    var me = this;
    var store = this.store;
    var current_group = store.getAt(rowIndex);
    var collect_group = store.query('leaf', false);
    var grp_id = 'grp_'+(collect_group.getCount()+1);
    var id = grp_id, name = grp_id;
    var group = Ext.create('TreeGroup', {groupIdentifier : grp_id, groupName : grp_id});
    me.newWindowGroup(group, 'Ajouter', function(form){
      form.updateRecord(group);
      group = Descartes.Application.createGroup(false, null, group.get('groupName'), {
        type    : "group", 
        nodeId  : null,
        groupName       : group.get('groupName'),
        groupIdentifier : group.get('groupIdentifier'),
        depth   : (current_group.get('attributes').depth || 0)+1,
        open    : true,
        opacity : "1",
        forceOpacityControl : "1"
      });
          
      var descartesItem = group.descartesItem;
      var treeNode = group.treeNode;
      
      current_group.insertChild(0, treeNode);
      me.saveTree();
    });
    /*
    Ext.Msg.prompt('Ajouter un groupe ou sous-groupe', 'Nommez le groupe ou conserver le nom par défaut'
    , function(buttonId, valuePrompt){
      if ( buttonId!="ok" ) return;
      Ext.create('Carmen.Model.MapGroup', {groupName : valuePrompt}).save({
        success : function(record){
          var group = Descartes.Application.createGroup(false, null, valuePrompt, {
            type    : "group", 
            name    : valuePrompt,
            nodeId  : null,
            groupId : null,
            id      : id,
            depth   : (current_group.get('attributes').depth || 0)+1,
            open    : true,
            opacity : "1",
            forceOpacityControl : "1"
          });
          
          var descartesItem = group.descartesItem;
          var treeNode = group.treeNode;
          
          current_group.insertChild(0, treeNode);
          
          app.layerTreePrintStore.getRoot().insertChild(0, treeNode);
          me.saveTrees();
        }
      })
    }, this, false, name);*/
  },
  
  editGroup : function(nodeId){
  	var me = this;
    var recordIndex = this.store.findBy(function(rec){return rec.get("nodeId")==nodeId && rec.get("type")=="group";});
    if ( recordIndex<0 ) return;
    var record = this.store.getAt(recordIndex);
    if ( !record ) return;
    if ( !record.get('removable') ){
      return;
    }
    var group = Ext.create('TreeGroup', {groupIdentifier : record.get('groupIdentifier'), groupName : record.get('groupName')});
    me.newWindowGroup(group, 'Modifier', function(form){
      form.updateRecord(group);
      record.set('text', group.get('groupName'));
      record.set('groupName', group.get('groupName'));
      record.set('groupIdentifier', group.get('groupIdentifier'));
    });
    /*
    Ext.Msg.prompt(
      'Modifier un groupe ou sous-groupe', 
      'Nom du groupe', 
      function(buttonId, valuePrompt){
        if ( buttonId!="ok" ) return;
        Ext.create('Carmen.Model.MapGroup', {groupName : valuePrompt, groupId : record.get('nodeId')}).save({
          success : function(){
            record.set('text', valuePrompt);
          }
        })
      }, this, false, 
      record.get('text')
    );
    */
  },
  editLayer : function(nodeId){
    var recordIndex = this.store.findBy(function(rec){return rec.get("nodeId")==nodeId && rec.get("type")=="layer";});
    if ( recordIndex<0 ) return;
    Carmen.Window.Layer.editById(nodeId);
  },
  
  deleteNode : function(grid, rowIndex, colIndex, callbackSuccess, callbackFailure) {
    var me = this;
    var legend_store = this.store;
    var legend_root = legend_store.getRoot();
    var legend_node = legend_store.getAt(rowIndex);
    var OL_MAP = legend_store.OL_MAP;
    var map_node = OL_MAP.getItemById(legend_node.data.OL_ITEM.id);
    var map_root = OL_MAP.item;
    var first_item = (map_root.items.length ? map_root.items[0] : null);
    
    var print_store = Ext.StoreMgr.lookup('layerTreePrintStore');
    var print_root = print_store.getRoot();
    var print_node = print_store.findRecord('nodeId', legend_node.get('nodeId'));
    
    var moveLayersToTop = function(root, item, isOLLayer){
      var child;
      var field = (isOLLayer ? "items" : "childNodes");
      var test = (isOLLayer ? item[field] && item[field].length : item.get('type')=='group');
      if ( test ){
        for (var i=item[field].length-1; i>=0; i--){
          moveLayersToTop(root, item[field][i], isOLLayer)
        }
      } else {
        if ( isOLLayer){
          root.insertItemBefore(item, first_item);
        } else {
          root.insertChild(0, item)
        }
      }
    }
    
    var text = {
      datatype : _T_LAYERTREE("delete.datatypes."+legend_node.data.type),
      dataname : '"'+legend_node.data.text+'"'
    };
    Ext.Msg.confirm(
      new Ext.XTemplate(_T_LAYERTREE("delete.title")).apply(text)
    , new Ext.XTemplate(_T_LAYERTREE("delete.text")).apply(text)
    , function(buttonId){
      if ( buttonId!="yes" ) return;
      
      /* delete from OpenLayer hierarchy : all included layers are move to front */
      if ( map_node.items && map_node.items.length ){
        moveLayersToTop(OL_MAP, map_node, true);
      }
      OL_MAP.removeItemById(map_node.id);
      
      /* delete from legendStore hierarchy : all included layers are move to root */
      if ( legend_node.childNodes && legend_node.childNodes.length ){
        moveLayersToTop(legend_root, legend_node, false);
      }
      legend_node.remove();
      
      /* delete from legendPrintStore hierarchy : all included layers are move to root */
      if ( print_node && print_root ){
        if ( print_node.childNodes && print_node.childNodes.length ){
          moveLayersToTop(print_root, print_node, false);
        }
        print_node.remove();
      }
      
      /* Save of trees states */
      me.saveTree(function(response, layerTreeStore, layerTreePrintStore){
      	if ( !Carmen.user.map ) return;
      	var layerStore = Carmen.user.map.get('layers');
      	var removed = [];
      	layerStore.each(function(layer){
      		var find = layerTreeStore.find('layerId', layer.get('layerId'));
      		if ( find==-1 ){
      			removed.push(layer);
      		}
      	});
      	if ( removed.length ){
      		layerStore.remove(removed);
      	}
      	if (callbackSuccess) callbackSuccess(response, layerTreeStore, layerTreePrintStore);
      }, callbackFailure);
    });
  },
  
  newWindowGroup : function(record, event, onSuccess, onCancel){
  	var form = Ext.create('Ext.form.Panel', {
  	  padding : 10,
      items : [{
        xtype : 'textfield',
        name : 'groupIdentifier',
        fieldLabel : _T_LAYERTREE('edition.group.groupIdentifier')
      },{
        xtype : 'textfield',
        name : 'groupName',
        fieldLabel : _T_LAYERTREE('edition.group.groupName')
      }]
});
    form.loadRecord(record);
  	var win = new Ext.window.Window({

      title : new Ext.XTemplate(_T_LAYERTREE('edition.group.title')).apply({event:event}),
      items : [form],
      buttons : [{
        text : _T_LAYERTREE('edition.group.success'),
        handler : function(){
          if ( onSuccess ) onSuccess(form);
          var win = this.up('window');
          win.close();
        }
      }, {
      	text : _T_LAYERTREE('edition.group.cancel'),
      	handler : function(){
      		if ( onCancel ) onCancel(form);
      		var win = this.up('window');
          win.close();
      	}
      }]
    }).show()
  }
});
Ext.define('TreeGroup', {
    extend: 'Ext.data.Model',
    fields: ['groupIdentifier', 'groupName']
});
