 /**
   * 
   */
   
Ext.define('Carmen.ApplicationUI', {
  extend: 'Ext.Viewport',
  layout: 'border',
 
  
  /////////////////////////////////////////////
  // layerTreePanel
  /////////////////////////////////////////////
  // --> see initLayerTreePanel
  
  
  /////////////////////////////////////////////
  // Main Panel
  /////////////////////////////////////////////
  mainPanel : Ext.create('Ext.Panel', {
    region: 'center',
    id: 'mainPanel',
    //title: encode_utf8(jsOws.OWSContext.General.Title),
    //title: jsOws.OWSContext.General.Title,
    fitToFrame: true,
    dockedItems: [
    ],  
    layout: 'border'
  }),
  
  ////////////////////////////////////////////////
  // Center Panel
  ////////////////////////////////////////////////
  centerPanel : Ext.create('Ext.Panel', {
    layout: 'absolute',
    id: 'centerPanel',
    fitToFrame: true,
    region : 'center'
  }),

  ////////////////////////////////////////////////
  // Map Panel
  ////////////////////////////////////////////////
  mapPanel : Ext.create('Ext.Panel', {
    layout: 'fit',
    id: 'mapPanel',
    x: 0,
    y: 0,
    //height : '100%',
    //width : '100%',
    region : 'center'
//obedel: if there is a way to force 100% height in a extjs way, cos with layout absolute, the only way seems using  !important in external css.
/*
    style : {
      width : '100%  !important',
      height : '100%  !important'
    },
    bodyStyle : {
      width : '100% !important',
      height : '100% !important'
    }
*/
  }),
  
  ////////////////////////////////////////
  // Reference Map Panel
  ////////////////////////////////////////
  referencePanel : Ext.create('Ext.Panel', {
    id: 'referencePanel',
    border: true,
    //collapsible: true,
    margins: '5 5 5 5',
    width: 220,
    height: 110,
    border: true,
    style: {
      bottom: '40px',
      right: '20px'
    },
     layout: {
        type: 'absolute'
    },
    items: [{
      xtype: 'button',
      id: 'referencePanelMinButton',
      text: '<i class="fa falk-angle-down fa-1x"></i>',
      cls: 'x-toolbar-item',
      style: {
        bottom: '0px',
        right: '0px',
        'z-index': 10000
      },
      enableToggle : true,
      listeners: {
        'toggle' : {
            fn : function(btn, state) {
                if (state==true) { // pressed for reduce
                  //console.log('reducing');  
                  this.ownerCt.setSize(29,24);
                  this.setText('<i class="fa falk-angle-up fa-1x"></i>');
                }
                else {
                  //console.log('enlarging');  
                  this.ownerCt.setSize(220,110);
                  this.setText('<i class="fa falk-angle-down fa-1x"></i>');
                }
              }
        }
      },
      scope : this
    }],
    getDiv : function() {
      return Ext.fly(this.body.id.replace('body','innerCt'));
    }
  }),
  
  ////////////////////////////////////////
  // Banner
  ////////////////////////////////////////
  bannerPanel : Ext.create('Ext.Panel', {
    region: 'north',
    id: 'bannerPanel',
    height : 75,
    //fitToFrame: true,
    layout: {
        type: 'hbox'
    },
    items: [
      {
          xtype: 'panel',
          id: 'bannerPanel_cpt_title',
          html: '<h1>'+Carmen.config.title+'</h1><h2>'+Carmen.config.subtitle+'</h2>',
          style : {top : '0px'},
          y : 0,
          flex: 4
      },
     /* {
          hidden : true,
              id: 'bannerPanel_cpt_loading',
          xtype:'messagebox', 
          closable : false,
          buttons : false,
          //anchor : '50% 50%',
              html: "<span>Requête serveur en cours d'exécution...</span>",
          style: { 'z-index':1000000, 'text-align': 'center', 'border':'1px solid RED', 'font-size':'24px' }
          },
          {
              xtype: 'component',
              id: 'bannerPanel_cpt_loadingfail',
              html: "<span style='color:red'>Echec d'une requête serveur.</span>",
              hidden : true,
              style: { 'text-align': 'right' }
      },*/
      { xtype: 'container',
        flex: 1,
        layout : {
          type: 'vbox',
          align: 'end',
          pack: 'center',
          padding: '0 10 0 0'
        },
        defaults: {
          width: 140,
          margin: '5 0 0 0'
        },
        items : [{
          layout : {
              type: 'hbox',
                pack: 'end',
              align: 'begin',vertical:false
          }, 
          width:'100%',
        items : [
          {
              xtype: 'component',
              id: 'bannerPanel_cpt_login',
                margin: 5,
                html: '<span><i class="fa fa-user"></i> '+Carmen.user.username+'</span>'
          },
{
              xtype: 'button',
              id: 'bannerPanel_btn_logout',
              padding: 5,
              icon: null,
                text: '<i class="fa fa-power-off"></i>'
          }
          ]},
          {
              xtype: 'button',
              id: 'bannerPanel_btn_userInfo',
              padding: 5,
              icon: null,
              hidden : (VISIBILITY_APP_BTN_ACCOUNT ? false : true),
              text: '<i class="fa fa-cog"></i> '+_t("app.btn.account")
          }
          
        ]
      }
    ]
  }),
  
  ////////////////////////////////////////
  // TopPanel
  ////////////////////////////////////////
  topPanel : function(){
    var me = this;
    return Ext.create('Ext.toolbar.Toolbar', {
      region: 'north',
      id: 'topToolbar',
      height : 40,
      border: true,
      style: { 'background-color': 'darkgray' },
      items : [
        {
            xtype: 'button',
            id: 'new_map',
            reference: 'topPanel_new_map',
            icon: null,
            hidden: (!VISIBILITY_APP_BTN_NEW ? true : false),
            /*glyph: 72,*/
            text: '<i class="fa fa-file-text-o"></i> '+_t("app.btn.new"),
            handler : function(){
              Ext.create('Carmen.Window.Map').edit(null);
              //me.mapWindow.edit(null);
            }
        },
        {
            xtype: 'button',
            id: 'open_map',
            reference: 'topPanel_open_map',
            icon: null,
            hidden: (!VISIBILITY_APP_BTN_OPEN ? true : false),
            /*glyph: 72,*/
            text: '<i class="fa fa-folder-open"></i> '+_t("app.btn.open"),
            handler : function(){
              var openMapList = function(){
              var window_class = "Carmen.Window.Map.List";
              var window = Ext.getCmp(CMP_REFERENCE(window_class)) || Ext.create(window_class);
              window.show();
            }
              if ( Carmen.user.map ){
                Ext.Msg.confirm( 
                  _T_MAPLIST('window.open.title'), 
                  _T_MAPLIST('window.open.message'),
                  function(btn){
                    if (btn=="yes"){
                      openMapList();
                    }
                  }
                );
              } else {
                openMapList();
              }
            }
        },
        {
            xtype: 'button',
            id: 'publish_map',
            icon: null,
            hidden : true,
            /*glyph: 72,*/
            text: '<i class="fa fa-save"></i> '+_t("app.btn.save"),
            handler : function(){
              if(Carmen.user.map) {
                // hismail - Modification prodige4.0
                Ext.create('Carmen.Window.Map.Publish').show();
//                Ext.Ajax.request({
//                  method : 'POST',
//                  url: Routing.generate('carmen_ws_publish_map', {
//                    edit_id    : Carmen.user.map.get('mapId'), 
//                    publish_id : Carmen.user.map.getPublishedId()
//                  }),
//                  success : function(response) {
//                    Carmen.Model.Map.onSuccessSave(Ext.decode(response.responseText), true);
//                  },
//                  failure : function(response) {
//                    //
//                  }
//                })
                //
              }
            }
        },
        {
            xtype: 'button',
            id: 'edit_map',
            hidden : true,
            icon: null,
            text: '<i class="fa fa-cogs"></i> '+_t("app.btn.settings"),
            handler : function()
            {
              Ext.create('Carmen.Window.Map').edit(Carmen.user.map);
            }
        },
        {
          xtype: 'button',
          id: 'add_map',
          hidden : true,
          icon: null,
          text: _t("app.btn.add"),
          handler : function()
          {
            ADDCONTEXTE = false;
            Ext.create('Carmen.Window.Map.AddLayers').show();
          }
        },
        {
          xtype: 'button',
          id: 'add_context',
          icon: null,
          text: _t("app.btn.addContext"),
          hidden : true,
          handler : function()
          {
            ADDCONTEXTE = true;
            Ext.create('Carmen.Window.Map.AddLayers').show();
          }
        },
        {
            xtype: 'button',
            id: 'share_map',
            hidden : (!VISIBILITY_APP_BTN_SHARE ? true : false),
            icon: null,
            text: '<i class="fa fa-share-alt"></i> '+_t("app.btn.share"),
            handler : function()
            {
              var windowClass = "Carmen.Window.MapUIShare";
              var window = Ext.getCmp(CMP_REFERENCE(windowClass)) 
                || Ext.create(windowClass, {
                  id: CMP_REFERENCE(windowClass),
                  mapId: Carmen.user.map.get('mapId')
              });
              window.show();
            }
        },
         '->',
        {
            xtype: 'button',
            id: 'topPanel_btn_catalogue',
            icon: null,
            hidden: (!VISIBILITY_APP_BTN_CATALOG ? true : false),
            //disabled:true,
            text: _t("app.btn.catalog"),
            handler : function()
            {
              Carmen.Util.openWindowInTab("Catalogue", "/services/catalogue/"+Carmen.user.account.id, false, 500, 500);
            }
        },
        {
            xtype: 'button',
            id: 'topPanel_btn_postgis',
            icon: null,
            hidden: (!VISIBILITY_APP_BTN_POSTGIS ? true : false),
            text: _t("app.btn.postgis"),
            handler : function()
            {
              Carmen.Util.openWindowInTab("PostGIS", Carmen.parameters.phppgadmin_url, false, 500, 500);
            }
            
        },
        {
            xtype: 'button',
            id: 'topPanel_btn_geosource',
            icon: null,
            hidden: (!VISIBILITY_APP_BTN_GEOSOURCE ? true : false),
            text: _t("app.btn.geosource"),
            handler : function() {
              if( Carmen.user.account.get("accountGeonetworkurl") != "" ) {
                Carmen.Util.openWindowInTab("Geonetwork", Carmen.user.account.get("accountGeonetworkurl"), false, 500, 500);
              }
            },
            listeners: [{
                afterrender: function() {
                  this.setDisabled( Carmen.user.account.get("accountGeonetworkurl") != "" ? false : true );
                }
            }]
        },
        {
            xtype: 'button',
            id: 'topPanel_btn_help',
            icon: null,
            disabled:true,
            hidden:true,
            cls : 'not-active',
            text: '<i class="fa falk-rescue"></i> '+_t("app.btn.help")
        }
        
      ]
    })
  },
  
  /////////////////////////////////////////
  // Title Panel
  /////////////////////////////////////////
  titlePanel : Ext.create('Ext.Panel', {
    id: 'titlePanel',
    height: 20,
    width: 'auto',
    maxWidth: '40%',
    padding: '20 20 20 20',
    margin: '0 0 0 0',
    border: true,
    items : [{
      id: 'titleLabel',
      padding: '15 15 15 15',
      xtype: 'label',
      style : {
      'font-size' : '15px'
      },
      text: _t('app.no_map_selected')
    }]
  }),


  
  ////////////////////////////////////////
  // Left Toolbar
  ////////////////////////////////////////
  //leftToolbar : Ext.create('Ext.toolbar.Toolbar', {
  leftToolbar : function() {
    var me = this;

    return Ext.create('Ext.panel.Panel', {  
      id: 'leftPanel',
      //xtype: 'toolbar',
      dock: 'left',
      height: '100%',
      width: 55,
      padding: 0,
      //style: { 'background-color': 'darkgray' },
      bodyStyle: { 'background-color': 'inherit' },
      header: {
        title : '<span class="fa fa-plus"></span>',
        //bodyStyle: { 'background-color': 'darkgray' },
        titleAlign: 'center',
        style: {
          font: 'weight bold',  
          'background-color': 'inherit'
        }
      },
      items :[{
        xtype : 'toolbar',
        style: { 'background-color': 'inherit' },
        defaults: {
          margin: '0 5 0 5'
        },
        height: '100%',
//        layout : {
//          type : 'vbox',
//          pack : 'center',
//          align : 'stretchmax'
//        },
        vertical : true,
        height: '100%',
        width: 50,
        padding: 0,
        listeners : {
          afterlayout : function(toolbar){
 
            var all_height = toolbar.getHeight();
            var btnHeight = 0;
            Ext.each(toolbar.query('button'), function(el){
              all_height -= el.getHeight();
              btnHeight += parseInt(el.getHeight());
            });
            toolbar.minHeight = btnHeight;
                        
            Ext.each(toolbar.query('tbspacer'), function(el, index, allSpacers){
              if ( all_height > 0 ){
              	el.setVisible(true);
                el.setHeight(Math.max(0, (all_height+(index==0 ? 0 : 10))/allSpacers.length - (index==0 ? 10 : 0)));
              } else {
                el.setVisible(false);
              }  
            })
          }
        },
        items: [].concat(
          [{xtype: 'tbspacer'}],
          // in variable case
          Ext.Array.remove([
            VISIBILITY_APP_VECTOR_LAYER ? {
                xtype: 'button',
                id: 'leftToolbar_btn_addVector',
                html: '<i class="fa falk-vector fa-2x"></i>',
                tooltip: _t('app.tooltip.newLayerVector'),
                handler : function(){
                  Carmen.Window.Layer.editVector(null);
                }
            }
            : VISIBILITY_APP_RASTER_LAYER ? {
                xtype: 'button',
                id: 'leftToolbar_btn_addRaster',
                html: '<i class="fa falk-raster-2 fa-2x"></i>',
                tooltip: _t('app.tooltip.newLayerRaster'),
                handler : function(){
                  Carmen.Window.Layer.editRaster(null);
                }
            }
            : VISIBILITY_APP_PRODIGE_LAYER ? {
              xtype: 'button',
              id: 'leftToolbar_btn_addLayer',
              html: '<i class="fa falk-vector fa-2x"></i>',
              tooltip: _t('app.tooltip.newLayer'),
              handler : function(){
                Carmen.Window.Layer.editData(null);
              }
            } 
            : null
          ], null),
          
          // in variable case
          Ext.Array.remove([
            {xtype: 'tbspacer'},
            VISIBILITY_APP_PRODIGE_LAYER ? {
              xtype: 'button',
              id: 'leftToolbar_btn_addLocal',
              html: '<i class="fa fa-folder-open-o  fa-2x"></i>',
              tooltip: _t('app.tooltip.newLayerLocalData'),
              handler : function(){
                Carmen.Window.Layer.editLocalData(null);
              }
            } 
            : null
          ], null),
          // in all case
          [
            {xtype: 'tbspacer'},
            {
                xtype: 'button',
                id: 'leftToolbar_btn_addWMS',
                html: '<i class="fa falk-globe fa-2x"></i>',
                tooltip: _t('app.tooltip.newLayerWMS'),
                handler : function(){
                  Carmen.Window.Layer.editWMS(null);
                }
            },
            {xtype: 'tbspacer'},
            {
                xtype: 'button',
                id: 'leftToolbar_btn_addWFS',
                html: '<i class="fa falk-globe-vector-2 fa-2x"></i>',
                tooltip: _t('app.tooltip.newLayerWFS'),
                handler : function(){
                  Carmen.Window.Layer.editWFS(null);
                }
            },
            {xtype: 'tbspacer'},
            {
                xtype: 'button',
                id: 'leftToolbar_btn_addWMSC',
                html: '<i class="fa falk-globe-raster fa-2x"></i>',
                tooltip: _t('app.tooltip.newLayerWMSC'),
                handler : function(){
                  Carmen.Window.Layer.editWMSC(null);
                }
            },
            {xtype: 'tbspacer'}
          ], 
          
          // in conditionnal case
          VISIBILITY_APP_POSTGIS_LAYER ? [
          	{
              xtype: 'button',
              id: 'leftToolbar_btn_addPostGIS',
              hidden :  (!VISIBILITY_APP_POSTGIS_LAYER ? true : false),
              html: '<i class="fa falk-postgresql fa-2x"></i>',
              tooltip: _t('app.tooltip.newLayerPostgis'),
              handler : function(){
                Carmen.Window.Layer.editPostgis(null);
              }
            },
            {xtype: 'tbspacer'}
          ] : []
          
          // in all case
          ,[{
              xtype: 'button',
              id: 'leftToolbar_btn_addWMTS',
              html: '<i class="fa falk-globe fa-2x"></i>', //TODO hismail
              tooltip: _t('app.tooltip.newLayerWMTS'),
              handler : function(){
                Carmen.Window.Layer.editWMTS(null);
              }
          
            },
            {xtype: 'tbspacer'}
          ]
        )
      }]

    })
  },
  
  ///////////////////////////////////////
  // bottom Panel
  ///////////////////////////////////////
  bottomPanel : Ext.create('Ext.toolbar.Toolbar', {
    id: 'bottomPanel',
    xtype: 'toolbar',
    dock: 'bottom',
    layout: {
        type: 'hbox',
        align: 'stretch'
    },
    width: '100%',
    height: 24,
    padding: '2 0 0 0',
    border: true
  }), 
  
  ////////////////////////////////////////
  // toolbar Panel
  ////////////////////////////////////////
  toolbarPanel : Ext.create('Ext.Panel', {
    id: 'toolBarPanel',
    bodyStyle: "background: transparent;",//max-width:100%
    style: {
      top: '20px',
      right: '20px'/*,
    'max-width': '60%'*/
    },
    minHeight: 40,
    width: 538,
    height: 'auto',
    //border: true,
    layout: {
    	type : 'fit'
    },
      items: [
      {
        xtype: 'toolbar',
        //enableOverflow:true,
        //overflowHandler : 'scroller', 
        style: "background: transparent;",
        id: 'toolbarPanel_toolbar_navigation',
        minHeight: 40,
        //border: true,
        margin: 1,
        padding: 0,
        toggleTools : function(btn, pressed){
          btn.getEl().removeCls('cmncontrolpressed');
          if ( pressed ){
            btn.getEl().addCls('cmncontrolpressed');
          }
        },
        listeners : {
          afterlayout : function(){
            this.items.each(function(btn){
              btn.un('toggle', this.toggleTools);
              btn.on('toggle', this.toggleTools);
            }, this);
          }
        },
        items:[
        {
          xtype : 'button',
          tooltip: 'Information',      
          tooltipType: 'qtip',
          cls: 'x-btn-icon', 
          enableToggle: true,
          toggleGroup: 'mainMapControl',
          disabled: true,
          text : '<i class="fa fa-info fa-2x"></i>',
          width: ctrlWidth,
          height: ctrlHeight
        },
        {
          xtype : 'button',
          tooltip: 'Infobulles',      
          tooltipType: 'qtip',
          cls: 'x-btn-icon', 
          enableToggle: true,
          toggleGroup: 'mainMapControl',
          disabled: true,
          text : '<i class="fa fa-comment fa-2x"></i>',
          width: ctrlWidth,
          height: ctrlHeight
        },
        {
          xtype : 'button',
          tooltip: 'Requête attributaire',      
          tooltipType: 'qtip',
          cls: 'x-btn-icon', 
          enableToggle: true,
          toggleGroup: 'mainMapControl',
          disabled: true,
          text : '<i class="fa fa-binoculars fa-2x"></i>',
          width: ctrlWidth,
          height: ctrlHeight
        }
        ]
      }
      
   ]
  }),

  ///////////////////////////////////////////
  // Attribution Panel
  ///////////////////////////////////////////
  attributionPanel : Ext.create('Ext.Container', {
    id: 'attributionPanel',
    width: 400,
    border: true,
    style : {
      'bottom': '30px',
        'left': '20px',
      'overflow': 'visible',
      'height': 'auto !important'
      },
    bodyStyle : {
      'overflow': 'visible',
      'height': 'auto !important'
      },
    hidden : true,
    items : [{
      style : {
        'font-size': '15px',
        'overflow': 'visible',
      'height': 'auto !important'
      },
      xtype: 'container',
      html: '<div id="logo-map"><img src="/'+Carmen.config.logo.map+'" /></div>'
    },{
      bodyStyle : {
        'overflow': 'visible',
      'height': 'auto !important'
      },
      xtype: 'container',
      html: '<div id="attributionCopyright"><span>'+Carmen.config.copyright+'</span></div>'
    }]
  }),
  
  
  // should the page have a header banner
  displayHeader : false,
  
  // should the left navigation panel be shown
  displayNavPanel : false,

  ////////////////////////////////////////////
  // constructor function
  ////////////////////////////////////////////
  constructor: function(ctx, options) {
    

    
    defaultConfig = {
      layout: 'border',
      items: [],
      suspendLayout: true
    };


    defaultConfig.items.push(this.bannerPanel);
    defaultConfig.items.push(this.mainPanel);
    Ext.apply(this, options, defaultConfig);
    
    Carmen.ApplicationUI.superclass.constructor.apply(this, arguments);
    
    this.topPanel = this.topPanel();
    this.leftToolbar = this.leftToolbar();
    
    // map panel
    this.centerPanel.add(this.mapPanel);
    this.mainPanel.add(this.centerPanel);
    this.centerPanel.add(this.referencePanel);
    
    
    
    
    // top panel
    this.mainPanel.add(this.topPanel);

    // data panel
    
    // layer Tree Panel
    this.initDataPanel();
    this.dataPanel.addDocked(this.leftToolbar);
    this.mainPanel.add(this.dataPanel);
    
    // title panel
    this.centerPanel.add(this.titlePanel);
//    this.toolbarPanel.items.get(0).add(this.measureTool);
//    
//    // toolbar panel
//    this.controlMeasure = new Carmen.Control.Measure({
//      toolbarId : 'default', 
//      grpId : this.measureTool.id
//    });
//    this.controlMeasure.addButtons(this);
////    
//    this.measureTool.menu.add({
//      xtype : 'button',
//      tooltip: 'Mesure de distance',      
//      tooltipType: 'title',
//      cls: 'x-btn-icon',
//      enableToggle: true,
//      //id : 'measure_distance',
//      toggleGroup: 'mainMapControlMeasure',
//      text: '<i class="fa falk-rule fa-2x"></i>',
//      width: ctrlWidth,
//      height: ctrlHeight
//    });
//            
//    this.measureTool.menu.add({
//      xtype : 'button',
//      tooltip: 'Mesure de surface',      
//      tooltipType: 'title',
//      cls: 'x-btn-icon',
//      enableToggle: true,
//      //id : 'measure_surface',
//      toggleGroup: 'mainMapControlMeasure',
//      text: '<span class="fa-stack fa-lg"><i class="fa falk-square fa-stack-2x"></i><i class="fa falk-texture fa-stack-1x text-dark"></i></span>',
//      width: ctrlWidth,
//      height: ctrlHeight
//    });
//    this.measureTool.setActiveItem(this.measureTool.menu.items.get(0)); 
    this.centerPanel.add(this.toolbarPanel);
    this.centerPanel.add(this.attributionPanel);
    

    //////////////////////////////////////////////
    // banner panel action 
    //////////////////////////////////////////////
    // preferences
    
    // disconnect
    var logoutBtn = this.bannerPanel.queryById('bannerPanel_btn_logout');
    logoutBtn.on('click', function(){
      Ext.Msg.confirm('Déconnexion', 'Êtes-vous sûr de vouloir vous déconnecter ?', function(btn){
        if( btn == 'yes' ) {
        	if ( this.opener ){
        		this.close();
        		this.opener.focus();
        	} else {
            window.location = Routing.generate('carmen_ws_index', {'logout':1});
        	}
        }
      });
    }, this);
    
    
    
    
    var viewSize = Ext.getBody().getViewSize();
    
    ///////////////////////////////////////////////
    // bottom toolbar
    ///////////////////////////////////////////////
    this.mapPanel.addDocked(this.bottomPanel);
    // t scale container
    this.bottomPanel.add({
      xtype: 'tbitem',
      id: 'bottomToolbar_textScale'
    });
    // graphic scale container
    this.bottomPanel.add({
      xtype: 'tbitem',
      id: 'bottomToolbar_graphicScale'
    });
    // filling space
    this.bottomPanel.add({ xtype: 'tbfill' });
    // crs info container
    this.bottomPanel.add({
      xtype: 'tbtext',
      id: 'bottomToolbar_crsInfo',
      style : 'padding:1px'
    });
    // mouse position container
    this.bottomPanel.add({
      xtype: 'tbtext',
      id: Ext.id(),
      minWidth: 200,
      html: '<div id="bottomToolbar_mousePosition"></div>'
    });
    
    
    this.suspendLayout = false;
    
    
    this.centerPanel.on('resize', 
      function(elt, width, height, oldWidth, oldHeight, eOpts) {
        this.mapPanel.setHeight(this.centerPanel.getHeight());
        this.leftToolbar.down('toolbar').setHeight(Math.max(this.leftToolbar.down('toolbar').minHeight || 0, this.centerPanel.getHeight()-this.leftToolbar.header.getHeight()));
      },
      this
    );
    
    
    this.doLayout();
    
    var keyMap = new Ext.util.KeyMap({
      target: Ext.getBody(),
      binding: [{
        key: "s",
        ctrl:true,
        fn: function(keyCode, evt){
          evt.stopEvent();
          if ( !Carmen.user.map ) {
            return null;
          }
          Ext.create('Carmen.Window.Map.Publish').show();
        }
      }]
    });
    
    var demoBtn = this.bannerPanel.queryById('bannerPanel_btn_userInfo');
    demoBtn.on('click', 
      function(elt, evt, eOpts) {
      	var win = Ext.getCmp('window-account');
        if ( !win ){
          win = Ext.create('Carmen.Window.Account')
        }
        win.show();
      }, this);
    
    
  },
  
  
  ////////////////////////////////////////
  // setCurrentMap
  ////////////////////////////////////////
  onSetCurrentMap : [],
  setCurrentMap : function(map)
  {
//    if ( Carmen.user.map && Carmen.user.map instanceof Carmen.Model.Map ){
//      var layerStore = Carmen.user.map.get('layers');
//      if ( layerStore && layerStore instanceof Ext.data.Store ){
//        layerStore.un('update', this.updateTreesFromMapLayers, this);
//      }
//    }
    
    Carmen.user.map = map;
    var bShowButtons = (map!==null && map instanceof Carmen.Model.Map && map.get('mapId')>0);
    this.topPanel.down('#publish_map').setVisible(bShowButtons);
    this.topPanel.down('#edit_map').setVisible(bShowButtons);
    this.topPanel.down('#share_map').setVisible(bShowButtons);
    this.topPanel.down('#add_map').setVisible(bShowButtons);
    this.topPanel.down('#add_context').setVisible(bShowButtons);
    if ( bShowButtons ){
      
      this.setMapTitle(Ext.util.Format.ellipsis( Ext.util.Format.trim( Ext.util.Format.stripTags( map.get('mapTitle'))), 500, true) +
                  ' ['+Ext.util.Format.ellipsis( ( map.get('mapModel') ? "modele_" : "") + map.get('mapFile') + '.map' , 100, false)+']');
      var mdataMap = Carmen.application.context.mdataMap;
      this.setMapLogo((mdataMap.logo!="0" ? mdataMap.logoPath : null), (mdataMap.copyright!="0" ? decodeURIComponent(mdataMap.copyrightTxt) : null), map.get('mapUi'));
//      var layerStore = Carmen.user.map.get('layers');
//      if ( layerStore && layerStore instanceof Ext.data.Store ){
//        layerStore.on('update', this.updateTreesFromMapLayers, this);
//      }
    } else {
      this.layerTreePanel.setRootNode(Carmen.LayerTree.noNode);
      this.setMapTitle(_t('app.no_map_selected'));
      //this.setMapLogo();
    }
    
    for(var i=0; i<this.onSetCurrentMap.length; i++){
    	var func = this.onSetCurrentMap[i];
    	var scope = func.scope || this;
    	func = func.fn || func;
    	
    	func.apply(scope, arguments);
    }
    
  },
  
  updateTreesFromMapLayers : function(layerStore){
    var layerTreeStore = this.app.layerTreeStore;
    var layerTreeStorePrint = this.app.layerTreePrintStore;
    layerStore.each(function(layer){
      var layerTree = layerTreeStore.findRecord('layerId', layer.get('layerId'));
      var layerTreePrint = layerTreeStorePrint.findRecord('layerId', layer.get('layerId'));
      if ( layerTree ){
        layerTree.set('text', layer.get('layerTitle'));
        
      }
      if ( layerTreePrint ){
        layerTreePrint.set('text', layer.get('layerTitle'));
      }
      //layerTree.set("opacity", (100-layer.get('layerTransparency'))/100);
      if ( layerTree ){
      var OL_Layer = layerTreeStore.OL_MAP.getItemById(layerTree.get('OL_ITEM').id);
      if ( OL_Layer ) {
        OL_Layer.setOpacity(layer.get('layerOpacity'));
      }
      }
    })
  },

  ////////////////////////////////////////
  // initLayerTreePanel
  ////////////////////////////////////////
  initDataPanel : function(layerTreeStore) {
    var me = this;
    var layerTreeStore = this.app.layerTreeStore;
    var layerTreeStorePrint = this.app.layerTreePrintStore;
    
    
    var defaultTree = {
      backup : null,
      attachEventsChange : function(){
        var onTreeValid  = function(){this.onTreeValid(true)};
        var onTreeChange = function(){this.onTreeChange()};
        
        this.store.on('nodechange', onTreeChange, this);
        this.store.on('nodeinsert', onTreeChange, this);
        this.store.on('nodeappend', onTreeChange, this);
        this.store.on('nodemove'  , onTreeChange, this);
        this.store.on('noderemove', onTreeChange, this);
        
        this.store.on('rootchange', onTreeValid, this);
        this.createBackup(); 
      },
      createBackup : function(){
        if ( this.isSuspended("backup") ) return;
        this.backup = this.store.getRoot().copy(undefined, true);
      },
      restoreBackup : function(){
        this.suspendEvent("backup");
        this.store.removeAll();
        this.store.setRoot(this.backup.copy(undefined, true));
        this.restoreBackupMap(this.store.getRoot());
        this.resumeEvent("backup");
        this.onTreeValid(false);
      },
      onTreeChange : function(){
        Ext.each(this.query('button[name$=Trees]'), function(btn){btn.setDisabled(false);});
      },
      onTreeValid : function(bCreateBackup){
        Ext.each(this.query('button[name$=Trees]'), function(btn){btn.setDisabled(true);});
        if ( bCreateBackup ){
          this.createBackup();
        }
      },
      saveTree : function(){
        var jsonData = {tree_name:this.reference};
        jsonData[this.reference] = this.getSave();
        var args = [jsonData].concat(Array.prototype.slice.call(arguments, 0));
        var cb = args[1];
        var callbackSuccess = function(response){
          Carmen.Model.Map.onSuccessSave(Ext.decode(response.responseText), false);
          if ( cb ){
            cb.apply(this, arguments);
          }
        }
        args[1] = callbackSuccess;
        me.saveTrees.apply(me, args);
      },
  
      resetTree : function()
      {
        var me = this;
        if ( !Carmen.user.map ){
          Ext.each(me.query('button[name=saveTrees]'), function(btn){btn.setDisabled(true);});
          return;
        }
        me.restoreBackup();
        Ext.each(me.query('button[name=saveTrees]'), function(btn){btn.setDisabled(true);});
      },
      getSave : function(){
        var root = this.store.getRoot();
        var recurs = function(node){
          var tree = node.getData();  
          tree.children = [];
          for (var i=0; i<node.childNodes.length; i++){
            tree.children.push(recurs(node.childNodes[i]));
          }
          return tree;
        }
        return recurs(root);
      },
      buttons: [{ 
        disabled:true, 
        text: 'Enregistrer', 
        tooltip: 'Enregistrer', 
        name : 'saveTrees', 
        handler : function(btn){
          var activeItem = this.up('treepanel');
          if ( !activeItem ) return;
          activeItem.saveTree();
        },
        listeners : {
          afterrender : function(){
            var activeItem = this.up('treepanel');
            if ( !activeItem ) return;
            this.setTooltip(activeItem.getBtnTooltip('Enregistrer '));
          }
      }
      }, { 
        disabled:true, 
        text: 'Annuler', 
        tooltip: 'Annuler', 
        name : 'resetTrees', 
        handler : function(){
          var activeItem = this.up('treepanel');
          if ( !activeItem ) return;
          activeItem.resetTree();
        },
        listeners : {
          afterrender : function(){
            var activeItem = this.up('treepanel');
            if ( !activeItem ) return;
            this.setTooltip(activeItem.getBtnTooltip('Annuler les modifications réalisées sur '));
          }
        }
      }]
    };
    this.layerTreePanel = Ext.create('Carmen.LayerTree.Grid', Ext.applyIf({
      getBtnTooltip : function(prefix){return prefix+" l'arborescence";},
      reference : 'map_tree',
      store : layerTreeStore,
      title : "Arborescence",
      appUI: this,
      restoreBackupMap : function(node){
        var map = this.getStore().OL_MAP;
        var parentItem = map.getItemById(node.get('OL_ITEM').id);
        if ( parentItem ){
          for (var i=0; i<node.childNodes.length; i++){
            var child = node.childNodes[i];
            var childItem = map.getItemById(child.get('OL_ITEM').id);
            map.removeItemById(child.get('OL_ITEM').id);
            if ( child.get('type')=='layer' ){
              childItem.setOpacity(child.get('opacity')*100);
              var layer = Carmen.user.map.getLayerById(child.get('layerId'));
              if ( layer ) layer.set('layerTransparency', (100-child.get('opacity')*100));
            }
            map.addItem(childItem, parentItem);
            this.restoreBackupMap(child);
          }
        }
      }
    }, defaultTree));
    
    this.layerTreePrint = Ext.create('Carmen.LayerTreePrint.Grid', Ext.applyIf({
      getBtnTooltip : function(prefix){return prefix+" la légende d'impression";},
      reference : 'map_tree_print',
      restoreBackupMap : Ext.emptyFn,
      store : layerTreeStorePrint,
      hidden : (VISIBILITY_PRINT_lEGEND ? false : true),
      storeLegend : layerTreeStore,
      title : "Légende d'impression",
      appUI: this
    }, defaultTree));
    
    this.dataPanel = Ext.create('Ext.Panel', {
      id: 'dataPanel',
      border: true,
      //title: 'Légende',
      width: 335,
      maxWidth: 600,
      //closable: true,
      //closeAction : 'hide'
      //true,
      region: 'west',
      split:  {
        tracker : {tolerance : 0},
        listeners : {
          dragstart : function(){
            Ext.get('mapPanel').mask().setStyle('backgroundColor', 'transparent');
          },
          dragend : function(){
            Ext.get('mapPanel').unmask();
          }
        }
      },
      /*layout: {
          type: 'vbox',
          pack: 'start',
          align: 'stretch'
      },*/
      layout: 'border',
      items: [{
          xtype: 'panel',
          border: true,
      region: 'north',
          buttonAlign: 'left',
          style : {'background-color' : 'white'}
          
      }, {
      autoScroll : true,
        region : 'center',
        xtype:'tabpanel',
        border: false,
        activeItem : 0,
        items : [this.layerTreePanel, this.layerTreePrint]
      }]
    });
  
  },
  
  maskTrees : function(mask){
    if ( this.dataPanel.rendered ){
      this.dataPanel[(mask ? "mask" : "unmask")]();
    }
  },
  refreshTrees : function(currentExtent)
  {
    var me = this;
  	me.layerTreePanel.getView().refresh();
    me.layerTreePrint.getView().refresh();
    
    if ( currentExtent && Carmen.user.map ){
      Carmen.application.map.OL_map.zoomToExtent(currentExtent);
    } else {
      me.app.map.refresh();
    }
  },
  saveAllTrees : function(callbackSuccess, callbackFailure, moreParams)
  {
    var me = this;
    me.saveTrees({
      tree_name : 'all',
      map_tree       : me.layerTreePanel.getSave(),
      map_tree_print : me.layerTreePrint.getSave()
    }, callbackSuccess, callbackFailure, moreParams)
  },
  saveTreePrint : function(callbackSuccess, callbackFailure, moreParams)
  {
    var me = this;
    me.saveTrees({
      tree_name : 'map_tree_print',
      map_tree_print : me.layerTreePrint.getSave()
    }, callbackSuccess, callbackFailure, moreParams)
  },
  saveTrees : function(jsonData, callbackSuccess, callbackFailure, moreParams)
  {
  	moreParams = moreParams || {};
    var me = this;
    
    if ( !Carmen.user.map ){
      Ext.each(me.query('button[name$=Trees]'), function(btn){btn.setDisabled(true);});
      return;
    }
    me.maskTrees(true);
    Ext.Ajax.request({
      method : 'POST',
      url : Routing.generate('carmen_ws_savetrees_map', {id : Carmen.user.map.get('mapId')} ),
      jsonData : Ext.apply(jsonData, moreParams),
      success : function(response){
        me.maskTrees(false);
        if ( callbackSuccess ) callbackSuccess(response, me.layerTreePanel.getStore(), me.layerTreePrint.getStore());
        if ( Ext.isDefined(jsonData.map_tree) ){
          Ext.each(me.layerTreePanel.query('button[name$=Trees]'), function(btn){btn.setDisabled(true);});
        }
        if ( Ext.isDefined(jsonData.map_tree_print) ){
          Ext.each(me.layerTreePrint.query('button[name$=Trees]'), function(btn){btn.setDisabled(true);});
        }
      },
      failure : function(response){
        if (Carmen.notAuthenticatedException(response) ) return;
        me.maskTrees(false);
        if ( callbackFailure ) callbackFailure(response, me.layerTreePanel.getStore(), me.layerTreePrint.getStore());
      }
    })
  },


  getSelectedTreeNode : function()
  {
    return this.layerTreePanel.getSelection();
  },




  getMapDiv: function() {
    return this.mapPanel.body;
  },

  getTopToolBar: function() {
    //return this.mainPanel.getDockedComponent('toptoolbar');
    return this.toolPanel.queryById('toolAdvancedToolBar');
  },
  
  
  addToToolPanel: function(elt, toolbarId, pos) {
    
    var panel = toolbarId=='simple' ?
      this.toolPanel.queryById('toolSimpleToolBar') :
      this.toolPanel.queryById('toolAdvancedToolBar');
    
    // managing position
    if (pos>panel.items.items.length) {
        panel.add(elt);
    }
    else
      panel.insert(pos,elt);
  },

  addToCycleButtonGroup: function(toolbarId, title, grpId, elt, pos, active, toolbarPos) {
    var toolbar = 
      toolbarId=='simple'   ? this.toolPanel.queryById('toolSimpleToolBar')   :
      toolbarId=='advanced' ? this.toolPanel.queryById('toolAdvancedToolBar') : 
                              this.toolbarPanel;
    var btnCycle = toolbar.queryById('grpBtn-'+grpId);
    
    if (btnCycle==null) {
      btnCycle = Ext.create('Ext.button.Selector', {
        id:'grpBtn-'+grpId,
        "class":'grpBtn',
        menuAlign: 'tr-tl?',
        arrowVisible: true,
        arrowCls: 'selector',
        arrowAlign: 'left', 
        text : '',
        tooltip : title,     
        tooltipType: 'title',
        showText: true,
        width: 40,
        height: 40,
        enableToggle: true,
        toggleGroup: 'mainMapControl',
        //allowDepress: false,
        menu: {
          id: 'grpBtn-menu-'+grpId,
          layout: { 
            type : 'hbox',
            align: 'stretch'
          },
          plain: true,
          minWidth: 40,
          defaults: {
              enableToggle: false,
              layout: 'fit',
              width: 40,
              height: 40,
              margin: 1,
              padding: 0,
              text: '',
              renderTpl : '<span id="{id}-btnWrap" data-ref="btnWrap" role="presentation" unselectable="on" style="{btnWrapStyle}" ' +
                    'class="{btnWrapCls} {btnWrapCls}-{ui} {splitCls}{childElCls} buttonItem">' +
                      '<span id="{id}-btnEl" data-ref="btnEl" role="presentation" unselectable="on" style="{btnElStyle}" ' +
                      'class="{btnCls} {btnCls}-{ui} {textCls} {noTextCls} {hasIconCls} ' +
                      '{iconAlignCls} {textAlignCls} {btnElAutoHeightCls}{childElCls}">' +
                        '<tpl if="iconBeforeText">{[values.$comp.renderIcon(values)]}</tpl>' +
                          '<span id="{id}-btnInnerEl" data-ref="btnInnerEl" unselectable="on" ' +
                              'class="{innerCls} {innerCls}-{ui}{childElCls}">{text}</span>' +
                        '<tpl if="!iconBeforeText">{[values.$comp.renderIcon(values)]}</tpl>' +
                      '</span>' +
                    '</span>' +
                    '{[values.$comp.getAfterMarkup ? values.$comp.getAfterMarkup(values) : ""]}' +
                    // if "closable" (tab) add a close element icon
                    '<tpl if="closable">' +
                      '<span id="{id}-closeEl" data-ref="closeEl" class="{baseCls}-close-btn">' +
                          '<tpl if="closeText">' +
                              ' {closeText}' +
                          '</tpl>' +
                      '</span>' +
                    '</tpl>'
            },
          items : []
        }
      });
      if (toolbarPos) {
        if (toolbarPos>toolbar.items.items.length) 
          toolbar.add(btnCycle);
        else
          toolbar.insert(toolbarPos,btnCycle);
      }
      else
        toolbar.add(btnCycle);
        
    }
    elt.setWidth(40);
    if (pos>btnCycle.menu.items.items.length) {
        btnCycle.menu.add(elt);
    }
    else {
      btnCycle.menu.insert(pos,elt); 
    }
    if (active || btnCycle.getActiveItem()==null)
      btnCycle.setActiveItem(elt); 
    
      
  },
  addToBottomToolbar: function(elt, pos) {
    var panel = this.bottomPanel;
    // managing position
    if (pos>panel.items.items.length) {
        panel.add(elt);
    }
    else
      panel.insert(pos,elt);
  },
  
  getBottomToolBar : function() {
    return this.bottomPanel;
  },
  
  getBottomToolBarDiv : function() {
    return Ext.fly(this.bottomPanel.id + '-innerCt');
  },
  
  getNavigationPanel: function() {
    return this.navigationPanel;
  },
  
  getLocatorPanel: function() {
    return this.locatorPanel;
  },
  
  addToLocatorPanel: function(elt, pos, activeTab) {
    var panel = this.locatorPanel;
    // managing position
    if (pos>panel.items.items.length) {
        panel.add(elt);
    }
    else
      panel.insert(pos,elt);
    
    if (activeTab) 
      panel.setActiveTab(elt);
    /*
    // managing elt collapsing/expanding
    elt.on('collapse',
      function(p, eopts) {
        this.doLayout();
      },
      panel);
    elt.on('expand',
      function(p, eopts) {
        this.doLayout();
      },
      panel);
      */ 
  },
  
  
  resetToolbarPanel: function(elt, pos) {
    this.toolbarPanel.child().removeAll()
  },
  
  addMapToolToToolbarPanel: function(mapTool) {
    var getter = ['getButton', 'getComponent', 'getButtons'];
    var func;
    for(var i=0; i<getter.length; i++){
      if ( typeof mapTool[ getter[i] ]=="function" ){
        func = getter[i];
      }
    }
    if ( func ){
    	var component = mapTool[func]();
    	if ( component ){
    	  this.toolbarPanel.child().add( component )
    	}
    }
  },
  
  configureMapTools : function(){
  	var ui = this;
  	var tool;
  	if ( tool = this.app.mapTools['FavoriteAreas'] ){
  		OpenLayers.Util.extend(tool, {
  			configureStore : function(map)
  			{
  				if ( this.isConfigured ) return;
  				this.isConfigured = true;
  				ui.onSetCurrentMap.push({
  					scope : this,
  					fn : function(map){
              if ( map ){
                var store = Ext.create('Ext.data.JsonStore', {
                  model: 'Carmen.Model.FavoriteArea',
                  proxy : Carmen.Model.FavoriteArea.prototype.getProxy(map.get('mapId')),
                  autoLoad : true
                });  
                this.areaBox.displayField = 'areaName';
                this.areaBox.valueField = 'areaId';
                this.areaBox.bindStore(store);
              } else {
                this.areaBox.displayField = 'areaName';
                this.areaBox.valueField = 'value';
                this.areaBox.bindStore(this.areaStore);
              }
  					}
  				});
  			},
  			cbOnEndEdit : function(btn, value){
  				var me = this;
          var map = me.map;
          if (btn=='ok') {
            var gbmName = value.length>0 ? value : 
              Carmen.Control.FavoriteAreas.defaultName + 
              Carmen.Control.FavoriteAreas.getUniqueId().toString();
            var extent = map.getExtent();
            
            
            var record = Ext.create('Carmen.Model.FavoriteArea', {
              areaName : gbmName,
              areaXmin : extent.left,
              areaYmin : extent.bottom,
              areaXmax : extent.right,
              areaYmax : extent.top
            });
            record.save({
            	success : function(){
            		me.areaBox.getStore().add(record);
            	},
            	failure : function(response){
	                if (Carmen.notAuthenticatedException(response) ) return;
	                Ext.Msg.alert(_T_FAVORITE_AREAS("msg.cannot_create"))
              }
            })
          }
        },
        cbOnDeleteItem : function(record, index){
          var me = this;
          if ( !(record instanceof Carmen.Model.FavoriteArea) ){
          	me.onDeleteItem(record, index)
          	return;
          }
          record.erase({
            success : function(){
            	var selection = me.areaBox.getSelection();
              if ( selection && selection.getId()==record.getId() ){
                me.areaBox.setSelection(null);
              }
              me.onDeleteItem(record, index);
            },
            failure : function(response){
              if (Carmen.notAuthenticatedException(response) ) return;
              Ext.Msg.alert(_T_FAVORITE_AREAS("msg.cannot_delete"))
            }
          });
        },
        cbOnSelectItem : function(store, record, rowIndex){
        	var me = this;
          if ( !(record instanceof Carmen.Model.FavoriteArea) ){
            me.onSelectItem(store, record, rowIndex)
            return;
          }
          
          if (record.data.areaName == Carmen.Control.FavoriteAreas.newFavoriteAreaTxt) {
            ( this.cbShowEditor || this.showWindow ) ();
          }
          else if (record.data.areaName != Carmen.Control.FavoriteAreas.emptyFavoriteAreaTxt && record.data.areaName !='') {
            this.map.zoomToExtent(new OpenLayers.Bounds(
              record.data.areaXmin,
              record.data.areaYmin,
              record.data.areaXmax,
              record.data.areaYmax
            ));
          }
        }
  		})
  	}
  },
  
  addToToolbarPanel: function(elt, pos) {
    var panel = this.toolbarPanel.items.get(0);
    if (pos>panel.items.length) {
        panel.add(elt);
    }
    else
      panel.insert(pos,elt);
  },

  getToolbarPanel : function() {
    return this.toolbarPanel;
  },


  getDataPanel: function() {
    return this.dataPanel;
  },
  
  
  getMainPanel: function() {
    return this.mainPanel;
  },
  
  getMapPanel: function() {
    return this.mapPanel;
  },
  
  getCenterPanel: function() {
    return this.centerPanel;
  },
  
  getHeaderPanel: function() {
    return this.headerPanel;
  },
  
  
  setMapTitle : function(title) {
  	try{document.title = 'Administration cartographique'+(title ? ' - '+title : '');}catch(error){}
    var label = this.titlePanel.queryById('titleLabel');
    label.setText(title);
  },
  
  
  setMapLogo : function(logo, copyright, mapUi) {
    Ext.select('#logo-map  img').each(function(el){el.dom.src = (logo || ''); el.setVisibilityMode(  Ext.Element.DISPLAY ); el.setVisible(!!logo);}); 
    Ext.get('attributionCopyright').first('span').dom.innerHTML = (copyright ? copyright : '');
    Ext.get('attributionCopyright').setVisible(!Ext.isEmpty(copyright ? copyright : ''));
    Ext.getCmp('attributionPanel').setVisible(logo || copyright);
    if ( mapUi ){
      var traductions = {
        "font-weight" : {"bold" : ["Gras", "gras", "Bold", "bold"]},
        "font-style" : {"italic" : ["Italique", "italique", "Italic", "italic"]}
      };
      var style = {
        'display'          : 'table-cell',
        'opacity'          : (100-(mapUi.get('uiCopyrightTransparency')||0))/100,
        'background-color' : '#'+(mapUi.get('uiCopyrightBackgroundColor') || 'cacaca'),
        'color'            : '#'+(mapUi.get('uiCopyrightFontColor') || '000000'),
        'font-size'        : (mapUi.get('uiCopyrightFontSize') || Carmen.Defaults.FONTSIZE)+'px'
      };
      var fontStyle = (mapUi.get('uiCopyrightFont') || 'ARIAL.TTF');
      fontStyle = Carmen.Dictionaries.FONTS.findRecord('valueField', fontStyle);
      if ( fontStyle ){
        fontStyle = fontStyle.get('displayField').split('-');
        Ext.iterate(traductions, function(property, traduction){
          Ext.iterate(traduction, function(value, matches){
            Ext.each(matches, function(match){
              if ( fontStyle.indexOf(match)!=-1 ){
                style[property] = value;
                return false;
              }
            })
          })
        })
        style.font = fontStyle[0];
      }
        
      Ext.get('attributionCopyright').first('span').setStyle(style);
    }
  },

  doLayout: function() {
    Carmen.ApplicationUI.superclass.doLayout.apply(this, arguments);
    // hack needed to correctly display bottom toolbar
    this.getMainPanel().updateBox(this.getMainPanel().getBox());
    this.updateBox(this.getBox());
  },

  /**
   *  @brief : return the screen resolution in Pixel per Inch
   *  @return : a float value 
   */
  getScreenResolution : function() {
    var testDiv = document.createElement("div");
    testDiv.style.position = 'absolute';
    testDiv.style.width='1in';
    testDiv.style.padding = '0px';
    testDiv.id = "testDiv";
    this.doLayout();
    this.getMapDiv().appendChild(testDiv);
    return testDiv.offsetWidth;
 },
 
  /**
  * @brief :  posistionne les composants en mode d'affichage classique
  * c-à-d., minimize les panels data et locator
  */ 
  standardLayout : function() {
    this.locatorButton.toggle();
    this.dataButton.toggle();
  },
 
  /**
  * @brief :  posistionne les composants en mode d'affichage extracteur de données
  * c-à-d., minimize le panel data, redimensionne le bouton panel, cache le panel et le bouton locator
  */ 
  extractorLayout : function() {
    this.locatorButton.hide();
    this.dataButton.toggle();
    this.dataPanel.hide();
    this.dataButton.setText('<i class="fa fa-database fa-1x"></i>');
    this.dataButton.setScale('small');
    this.dataButton.setHeight(40);
    this.dataButton.setWidth(40);
    this.dataButton.setPosition(
      20,
      20
    );
    var elt= Ext.get('dataButton-btnInnerEl').child('i.fa');
    if (elt)
      elt.setStyle({'margin-left' : '0px' });
  },
  
  CLASS_NAME : "Carmen.ApplicationUI"

});

//Carmen.ApplicationUI.CLASS_NAME = "Carmen.ApplicationUI";
 

