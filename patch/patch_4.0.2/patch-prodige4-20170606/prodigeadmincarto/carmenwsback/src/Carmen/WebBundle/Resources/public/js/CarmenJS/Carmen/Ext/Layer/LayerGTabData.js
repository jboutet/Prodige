var _T_LAYER = function (key) {
  return _t("Layer.forms.GTabData." + key);
};

/**
 * Carmen.Model.Layer
 */
Ext.define('Carmen.Model.LayerData', {
  extend: 'Carmen.Model.Layer',
  idProperty: 'layerId',
  fields: []
});

/**
 * Generic tab for type of layer
 */
Ext.define('Carmen.FormSheet.Layer.GTabData', {
  extend: 'Carmen.Generic.Form',

  title: _T_LAYER('title'),
  frame: true,
  height: 'auto',
  layout: 'column',
  modelName: 'Carmen.Model.Layer',
  defaults:{
    columnWidth: 1
  },
  fieldDefaults: {
    labelWidth: 125
  },
  
  setWindowButtons : function(){
    var me = this;
    var bbar = me.up('window').down('toolbar[dock="bottom"]');
    bbar.removeAll();
    bbar.add([{
      text : _T_MAP_ADD('btn.save'),
      handler : function() {
        
        if(me.copyMapid && me.copyMapid != "") {
          //default representation
            Ext.Ajax.request({
              url: Routing.generate('carmen_ws_add_layers', {
                'mapToId':Carmen.user.map.id, /*id for current map file : Carmen.prodige.id*/
                'mapFromId': me.copyMapid, /*id of source mapfile */
                'mode':'add'
               }),
              method: 'POST',
              jsonData: {
                ar_layer_ids: me.copyLayers
              },
              success: function (response) {
                me.up('window').close();
                var jsonResponse = Ext.decode(response.responseText);
                Carmen.Model.Map.onSuccessSave(jsonResponse);
              },
              failure: function (response) {
                console.log('error - response is : ', response);
              }
            });
          }else{
             var submits = [];
             Ext.each(me.tabLayers, function(record){submits.push(record.getData({persist:true,serialize:true}));});
             
             Ext.Ajax.request({
                url : Routing.generate('carmen_ws_postlayers', {'mapId' :Carmen.user.map.get('mapId')}),
                async : false,
                method : 'POST',
                jsonData : {layers : submits}
              , success : function(response){
                  me.up('window').close();
                  var jsonResponse = Ext.decode(response.responseText);
                  Carmen.Model.Map.onSuccessSave(jsonResponse);
                }
              , failure : function(response){
                  if (Carmen.notAuthenticatedException(response) ) return;
                }
              });
          }
      }
    }, {
      text : _T_MAP_ADD('btn.cancel'),
      handler : function(){
        ADDCONTEXTE = false;
        me.up('window').close();
      }
    }]);
  },
  // methods
  initComponent: function() {
    var me = this;
    
    me.copyMapid = "";
    me.copyLayers = new Array();
    me.tabLayers = new Array();
    
    me.sDomStore = new Ext.data.TreeStore({
      storeId : 'SDOM',
      //autoLoad : true,
      //lazyFill: true,
      proxy: {
        type: 'jsonp',
        reader : {
          type : 'json'
        },
        url: Carmen.parameters.catalogue_url + "/geosource/getDomaines?mode=0"
      }
    });
    me.sDomStore.load();

    me.url = Carmen.parameters.catalogue_url + "/geosource/administrationCartoCouche?SRV_CARTO=" + Carmen.parameters.catalogue_url;

    me.layerStore = new Ext.data.Store({
      storeId: 'layer',
      fields:['text', 'type', 'table_name', 'icon', 'map_id', 'layers_id', 'layers', 'id'],
      proxy: {
        type: 'jsonp',
        reader : {
          type : 'json'
      },
      url: me.url
     }
    });
    me.on('afterrender', me.setWindowButtons, me);
    
    // items on this panel
    me.items = [{
        border : true,
        xtype:'treepanel',
        name:'fsTree',
        title: _T_LAYER('fields.titleLayersSettings'),
        columnWidth: 0.5,
        margin: '0 4 10 0',
        height: 200,
        padding: '0 0 0 0',
        scrollable: true,
        store: me.sDomStore,
        rootVisible: false,
        selModel: new Ext.selection.TreeModel({
          listeners: {
            selectionchange: function (selection, node) {
              me.copyMapid = "";
              me.copyLayers = new Array();
              me.tabLayers = new Array();
              node = node[0]; // un seul noeud sélectionnable
              var dom = -1;
              var sdom = -1;
              if(node.data.isDomain == true) {
                //dom
                dom = node.data.id.match(/dom_(\d+)/)[1];
              } else if(node.data.isSubDomain == true) {
                //sdom
                dom  = node.data.parentId.match(/dom_(\d+)/)[1];
                sdom = node.data.id.match(/sdom_(\d+)/)[1];
              } else {
                //rubrique
                dom = node.data.id;
              }
              me.layerStore.load({url: me.url + "&coucheType=0,1,-4&domain=" + dom + "&subdomain=" + sdom});
            }
          }
        })
      }, {
      	border : true,
        xtype:'gridpanel',
        viewConfig : {emptyText : 'Aucune couche de catalogue dans cette catégorie'},
        name:'fsForm',
        title: _T_LAYER('fields.titleLayerProperties'),
        columnWidth: 0.5,
        margin: '0 0 10 4',
        height: 200,
        padding: '0 0 0 0',
        scrollable: 'y',
        store: me.layerStore,
        columns: [
                  { dataIndex: 'icon', flex: 1, renderer: function(value) {return '<img src="' + value + '"/>'; } },
                  { text: 'Text', dataIndex: 'text', flex: 11 }
                 ],
          hideHeaders: true,
          mode: "SINGLE",
          selModel : Ext.create('Ext.selection.RowModel',{
          listeners : {
            select : {
              fn :function(selModel, record, index) {
                 //reinit on each selection
                 me.copyMapid = "";
                 me.copyLayers = new Array();
                 me.tabLayers = new Array();
                
                 Ext.ComponentQuery.query('[name=layerTitle]')[0].setValue(record.data.text);
                 if(record.data.map_id && record.data.map_id != "") {
                  //do nothing
                   me.copyMapid = record.data.map_id;
                   me.copyLayers = record.data.layers_id;
                   
                 }else{
                   me.copyMapid = "";
                   me.copyLayers = new Array();
                   for(var i=0; i<record.data.layers.length;i++){
                     var layer = record.data.layers[i];
                   //if(record.data.srid && record.data.srid != "") {
                       if((layer.type == "RASTER_UNIQUE") || (layer.type == "RASTER_DALLE")) {
                         var record_modele = Ext.create('Carmen.Model.LayerRaster', {
                             layerTitle : layer.title,
                             layerType : "RASTER",
                             layerMetadataUuid : record.data.id,
                             layerName: convertNameToIdentifier(layer.table_name,20),
                             msLayerName: convertNameToIdentifier(layer.table_name,20),
                             layerIdentifier: convertNameToIdentifier(layer.table_name),
                             msGeometryType: layer.type,
                             msLayerData: layer.table_name,
                             layerProjectionEpsg: layer.srid
                           });
                       }
                       else if(layer.type == "VECTOR") {
                         var record_modele = Ext.create('Carmen.Model.LayerVector', {
                             layerTitle: layer.title + ' ['+layer.table_name+']',
                             layerName: convertNameToIdentifier(layer.table_name,20),
                             msLayerName: convertNameToIdentifier(layer.table_name,20),
                             layerMetadataUuid : record.data.id,
                             layerIdentifier: layer.table_name,
                             layerProjectionEpsg: layer.srid,
                             msGeometryType: layer.geometry,
                             layerType : "POSTGIS",
                             msLayerConnectionType : "POSTGIS",
                             msLayerPgSchema :"public",
                             msLayerPgTable : layer.table_name,
                             msLayerPgGeometryField : "the_geom",
                             msLayerPgIdField : "gid",
                             msLayerPgProjection :  layer.srid
                             
                         });
                       }
                       //record_modele.phantom = true;
                       //me.loadRecord(record_modele);
                       //me.up('window').record = record_modele;
                       me.tabLayers.push(record_modele);
                   }
                   /*}
                   else { //TODO Benoist - A vérifier - Absence d'une projection 
                     Ext.Msg.alert('Attention', 'Cette couche n\'a pas une projection ....');
                   }*/
                 }
              }
            }
        }
      })
    },{
      xtype:'fieldset',
      layout: 'form',
      padding: 0,
      margin: 0,
      border: false,
      items: [{
        xtype: 'textfield',
        name: 'layerTitle',
        fieldLabel: _T_LAYER('fields.alias'),
        allowBlank: false,
        maxLength: 255,
        enforceMaxLength: true,
        flex: 12
      }]
    }];
    this.callParent(arguments);
  }
});
