/**
 * @_requires OpenLayers/Basetypes/Class.js
 * @_requires Carmen/Map.js
 */

Descartes.StringSplitters = {
	WMTS : {
		MATRIXIDS    : ' ',
		TILEORIGINS  : ' ',
		MATRIXLATLON : ',',
		RESOLUTIONS  : ' '
	},
  WMSC : {
    RESOLUTIONS  : ' '
  }
};

Descartes_Messages_Info_MetricScale = {TITLE_MESSAGE: "&Eacute;chelle actuelle : "};

Descartes.Application = new OpenLayers.Class({

  ui : null,
  
  map : null,
  
  context : null,
  
  mapTools : {},
  
  mapActions : {},
  
  mapInfos : {},

  initialize: function() {
    
    this.messages = Descartes.Utils.loadMessages(this.CLASS_NAME);
    
    // Utilisation du plugin EXTJS 5
    Descartes.ViewManager.usePlugin(Descartes.ViewManager.EXTJS5);
    // Personnalisation des vues
    Descartes.ViewManager.INFORMATION_IN_PLACE = 18;
    Descartes.ViewManager.TOOLTIP = 19;
    Descartes.ViewManager.REQUESTATTRIBUT = 20;
    //Descartes.ViewManager.LOCALIZED_MOUSE_POSITION = 113
    
    Descartes.ViewManager.activeViews[Descartes.ViewManager.SCALE_SELECTOR_IN_PLACE] = Descartes.UI.ScaleSelectorInPlaceExtJS5Extension;
    Descartes.ViewManager.activeViews[Descartes.ViewManager.INFORMATION_IN_PLACE] = Descartes.UI.InformationInPlaceExtJS5;
    Descartes.ViewManager.activeViews[Descartes.ViewManager.TOOLTIP] = Descartes.Info.ToolTipExtensionExtJS5;
    Descartes.ViewManager.activeViews[Descartes.ViewManager.REQUESTATTRIBUT] = Descartes.Info.AdvancedQueryAttributes;
      
    Descartes.FEATUREINFO_SERVER = Routing.generate('carmen_descartes_getfeatureinfo');
    
    
    // CARMEN specific parameters
    // set custom resolution
    OpenLayers.DOTS_PER_INCH = Carmen.DOTS_PER_INCH;
    // switch disgusting pink onImageLoadError in IE to transparent
    OpenLayers.Util.onImageLoadErrorColor = "transparent";
    
    // updating proj4js libPath to avoid rewriting url pbs when looking for unknown projections
    Proj4js.libPath = "/IHM/JavaScript/proj4js/lib/";
    
    // custom vtypes...
    Ext.apply(Ext.form.VTypes, {
       owsContext: function(v){
         return /^.*\.(ows)$/.test(v);
      },
      owsContextText: 'Seuls les fichiers de contexte ows sont autorisés'
    });
     
    // init LayerTreeStore
    this.layerTreeStore = Ext.create('Carmen.LayerTree.TreeStore', {
      hasChanges : true,
        id : 'layerTreeStore'
      });
    this.layerTreePrintStore = Ext.create('Carmen.LayerTree.TreeStore', {
      hasChanges : true,
        id : 'layerTreePrintStore'
      });
    
    // init UI 
    this.ui = new Carmen.ApplicationUI(null, {app : this});
    
    // init MapToolBar
    this._initMapToolBar();
    
  },
  
  loadContext: function(jsonContext) {
    if (jsonContext!=null) {
      this._parseContext(jsonContext);
      
      Descartes.application=this; 
        
      this._initMapContent();
   
      this._initMap();
      
      this._initLayerTree();
      
      this._initLayerTreePrint();
      
      this._initTools();
      
      this.map.OL_map.events.register('zoomend', this, function(){this.ui.layerTreePanel.refreshIcons();}); 
    }
    
  },
  
  reloadTreeContext: function(jsonContext) {
    if (jsonContext!=null) {
      this._parseContext(jsonContext);
      this._initLayerTree();
      this._initLayerTreePrint();
    }
  },
  
  
  /**
   * Methode: getMessage
   * Retourne un message stocké dans l'objet des messages associé à la classe.
   * 
   * Paramètres:
   * key - {String} Clé du message recherché.
   * 
   * Retour:
   * {String} Le texte du message recherché.
   */
  getMessage: function(key) {
    var aMessage = "";
    if (this.messages !== null && this.messages[key] !== null) {
      aMessage = this.messages[key];
    }

    return aMessage;
  },
  
  _initMapContent : function() {
  
    //Initialisation du mapContent
    if (this.mapContent!=null)
      delete this.mapContent;
    this.mapContent = new Descartes.MapContent({editable:true});
    
    var ctx = this.context;
    
    var jsonNode = ctx.mdataMap.LayerTree.LayerTreeNode;
    var jsonLayerList = ctx.layer;
    
    var att = jsonNode.attributes;
    var isRoot = (att.name == "root");
    
    if (isRoot) {
      var treeNode = Descartes.Application.createGroup(isRoot, att.nodeId);
      treeNode.OL_ITEM = {id : this.mapContent.item.id, index : this.mapContent.item.index, parentId : null, parentIndex : null};
      this.mapContent.item.TREE_NODE = treeNode;
      
      if ('LayerTreeNode' in jsonNode) {
        if (jsonNode.LayerTreeNode instanceof Array) {
          for (var i = 0; i < jsonNode.LayerTreeNode.length; i++) {
            this._addNode(jsonNode.LayerTreeNode[i], jsonLayerList);
          }
        } else {
          this._addNode(jsonNode.LayerTreeNode, jsonLayerList);
        }
      }
    } else {
      this._addNode(jsonNode, jsonLayerList);
    }
    
  },
  
  _addAnnotationLayer : function() {
    var ctx = this.context;
    if (ctx.isToolActiv('attribut_annotation') ||
            ctx.isToolActiv('styles_annotation') ||
            ctx.isToolActiv('point_annotation') ||
            ctx.isToolActiv('line_annotation') ||
            ctx.isToolActiv('polygon_annotation') ||
            ctx.isToolActiv('move_annotation') ||
            ctx.isToolActiv('modify_annotation') ||
            ctx.isToolActiv('text_annotation') ||
            ctx.isToolActiv('export_annotation') ||
            ctx.isToolActiv('buffer_from_selection_annotation') ||
            ctx.isToolActiv('buffer_all_annotation') || 
            ctx.isToolActiv('snapping')
    ) {
      var layerDefinition = {
        projection: this.context.getProjection()
      };
      
      var annotationConfig = this.context.getGeneralConfig('Annotation');
      
      var options = {
        visible: true,
        queryable: false,
        annotationConfig: annotationConfig
      };
      
      this.annotationLayer = new Descartes.Layer.Annotation("Annotations", layerDefinition, options);
      
      this.mapContent.addItem(this.annotationLayer);
    }
  },  

  _addNode : function(jsonNode, jsonLayerList, parentItem) {
    if ('LayerTreeNode' in jsonNode && !(jsonNode.LayerTreeNode instanceof Array) ){
      jsonNode = jsonNode.LayerTreeNode;
    }
    var att = jsonNode.attributes;
    var isGroup = (att.type == "group");
        
    if (isGroup) {
      this._addGroup(jsonNode, jsonLayerList, parentItem);
    } else {
      this._addLayer(jsonNode, jsonLayerList, parentItem);
    }
  },

  _addGroup : function(jsonNode, jsonLayerList, parentItem) {
    var att = jsonNode.attributes;
    var title = att.name;

    parentItem = parentItem || this.mapContent.item;
    var itemNode   = Descartes.Application.createGroup(false, att.nodeId, title, att);
    var item       = itemNode.descartesItem;
    var treeNode   = itemNode.treeNode;
    item.TREE_NODE = treeNode;
    treeNode.OL_ITEM = {id : item.id, index : item.index, parentId : parentItem.id, parentIndex : parentItem.index};
     
    if (parentItem) {
      this.mapContent.addItem(item, parentItem);
    } else {
      this.mapContent.addItem(item);
    }
    
      
    if ('LayerTreeNode' in jsonNode) {
      if (jsonNode.LayerTreeNode instanceof Array) {
        for (var i = 0; i < jsonNode.LayerTreeNode.length; i++) {
          this._addNode(jsonNode.LayerTreeNode[i], jsonLayerList, item);
        }
      } else {
        this._addNode(jsonNode.LayerTreeNode, jsonLayerList, item);
      }
    }
  },
  
  _getProviderUrl : function(providerUrl) {
    //l'url doit terminer par ? ou &
    
    var lastCar = providerUrl[providerUrl.length-1];
    if (lastCar == '?' || lastCar == '&') {
      //--> ok;
      return providerUrl;
    }
    
    if (providerUrl.indexOf("?") == -1) {
      //Il n'y a pas de ?
      providerUrl += "?";
    } else {
      providerUrl += "&";
    }

    return providerUrl;
  },

  _addLayer : function(jsonNode, jsonLayerList, parentItem) {
    var att = jsonNode.attributes;
    var item;
    var jsonLayerDesc = jsonLayerList[att.layerIdx];
      

    // ATTENTION : Il faut inverser le min et le max
    var minScale = Ext.num(jsonLayerDesc.MaxScaleDenominator, null, false);
    if (minScale != null && minScale <= 0.0) {
      minScale = null;
    }
    var maxScale = Ext.num(jsonLayerDesc.MinScaleDenominator, null, false);
    if (maxScale != null && maxScale <= 0.0) {
      maxScale = null;
    }
    
    var queryable = (!!jsonLayerDesc.Extension.layerSettings_briefFields && jsonLayerDesc.Extension.layerSettings_briefFields.length > 0
                && !!jsonLayerDesc.Extension.layerSettings_infoFields && jsonLayerDesc.Extension.layerSettings_infoFields.length > 0);
                
    var activeToToolTip = (!!jsonLayerDesc.Extension.layerSettings_ToolTipFields && jsonLayerDesc.Extension.layerSettings_ToolTipFields.length > 0);
       
    if ('DataURL' in jsonLayerDesc) {
      
      var providerUrl = this._getProviderUrl(jsonLayerDesc.DataURL.OnLineResource.attributes.url);
      
      var layer_Def = {
          title: att.name, 
          layerDefinition : {
            serverUrl: providerUrl,// + "&",
            layerName: att.mapName,
            featureServerUrl: providerUrl,// + "&",
            featureName: att.mapName,
            imageServerVersion : "1.3.0",
            serverVersion : "1.3.0"
          },
          options: {
            visible: (att.visible == "1"),
            alwaysVisible: false,
            queryable: queryable,
            activeToQuery: queryable,
            activeToToolTip: activeToToolTip,
            sheetable: false,
            legend: [],
            minScale: minScale,
            maxScale: maxScale,
            opacity: att.opacity * 100,
            opacityMax: 100,
            metadataURL: jsonLayerDesc.Extension.layerSettings_Metadata,
/*REMOVE*///            queryLimitedToMapMaxExtent: true,
            transparent: true,
            format: outputFormat,
            singleTile: true,
            configuration : {
              attributes : att,
              jsonLayerDesc : jsonLayerDesc
            }
          }
        };
        
        item = new Descartes.Layer.WMS(
          layer_Def.title, 
          layer_Def.layerDefinition, 
          layer_Def.options);


    } else if ('Server' in jsonLayerDesc) {
      // WMS layer ?
      var service = jsonLayerDesc.Server.attributes.service;

      var providerUrl = this._getProviderUrl(jsonLayerDesc.Server.OnLineResource.attributes.url);

      // Modif SWT [P2-4] 7638 : Lecture du WMS 1.3
      // Avant, on récupérait la version utilisée par MapServer
      // var version = Url.decode(jsonLayerDesc.Server.attributes.version);
      // Mais MapServer ne sait gérer que le 1.1.1, donc on lui
      // dit *toujours* 1.1.1...
      // On a donc introduit une nouvelle information : la version
      // à utiliser côté frontoffice (par OpenLayers).
      var prefix = (service.search(/WMT?S/i)>-1 ? "wms" : "wfs");
      
      var version = Url.decode(jsonLayerDesc.Server.attributes.version);
      var outputFormat = Url.decode(jsonLayerDesc.Extension[prefix+"_format"]);
      var projection = Ext.valueFrom(Url.decode(jsonLayerDesc.Extension[prefix+"_srs"]), this.context.getProjection(), false);
      var projUnits = Carmen.Util.getProjectionUnits(projection);
      
      var isWMS = (service.search(/WMS|WFS/i)>-1);
      if (isWMS) {
        
        var layerName = jsonLayerDesc.Extension[prefix+"_name"];
        layerName = decodeURIComponent(layerName.replace(/\+/g,  " "));
          
        var layer_Def = {
            title: att.name, 
            layerDefinition : {
              serverUrl: providerUrl,
              layerName: layerName,
              featureServerUrl: providerUrl,
              featureName: layerName,
              serverVersion : version!="undefined" ? version : "1.1.1"
            },
            options: {
              visible: (att.visible == "1"),
              alwaysVisible: false,
              queryable: queryable,
              activeToQuery: queryable,
              activeToToolTip: activeToToolTip,
              sheetable: false,
              legend: [],
              minScale: minScale,
              maxScale: maxScale,
              opacity: att.opacity * 100,
              opacityMax: 100,
              metadataURL: jsonLayerDesc.Extension.layerSettings_Metadata,
/*REMOVE*///              queryLimitedToMapMaxExtent: true,
              transparent: true,
              format: outputFormat,
              singleTile: true,
              configuration : {
                attributes : att,
                jsonLayerDesc : jsonLayerDesc
              }
            }
        };
          
        item = new Descartes.Layer.WMS(
            layer_Def.title, 
            layer_Def.layerDefinition, 
            layer_Def.options);
      }
      
      var isWMSC = Ext.valueFrom(jsonLayerDesc.Extension.layerSettings_wmscLayer,"OFF")=="ON";
      var isWMTS = Ext.valueFrom(jsonLayerDesc.Extension.layerSettings_wmtsLayer,"OFF")=="ON";
      if (isWMSC) {
        
        var layerName = jsonLayerDesc.Extension.wms_name;
        
        var str_boundingbox = Ext.valueFrom(jsonLayerDesc.Extension.wmsc_boundingbox, null, false);
        var maxExtent = this.context.getMaxExtent();
        if (str_boundingbox) {
          var sep = str_boundingbox.indexOf(',') != -1 ? ',' : ' ';
          var bb = Descartes.Utils.chompAndSplit(Url.decode(str_boundingbox), sep);
          var maxExtent = new OpenLayers.Bounds(bb[0], bb[1], bb[2], bb[3]);
        }
        // getting wmsc resolution if defined
        var str_res = Ext.valueFrom(jsonLayerDesc.Extension.wmsc_resolution, null, false);
        var resolutions = [];
        if (str_res!=null) {
          str_res = Url.decode(str_res);
          var sep = str_res.indexOf(',') != -1 ? ',' : ' ';
          var res = Descartes.Utils.chompAndSplit(str_res, sep);
          // kind of defensive prog to prevent wrong format...
          if (!isNaN(new Number(res[0]))) {
            resolutions = Carmen.Util.array_map(res, parseFloat);
          }
        }
          
        var layer_Def = {
            title: att.name, 
            layerDefinition : {
              serverUrl: providerUrl,
              layerName: layerName,
              featureServerUrl: providerUrl,
              featureName: layerName,
              params : {
                version : version
              }
            },
            options: {
              //maxExtent: maxExtent,
              resolutions: resolutions,
              visible: (att.visible == "1"),
              alwaysVisible: false,
              queryable: queryable,
              activeToQuery: queryable,
              activeToToolTip: activeToToolTip,
              sheetable: false,
              legend: [],
              minScale: minScale,
              maxScale: maxScale,
              opacity: att.opacity * 100,
              opacityMax: 100,
              metadataURL: jsonLayerDesc.Extension.layerSettings_Metadata,
/*REMOVE*///              queryLimitedToMapMaxExtent: true,
              transparent: true,
              format: outputFormat,
              configuration : {
                attributes : att,
                jsonLayerDesc : jsonLayerDesc
              }
            }
        };
          
        item = new Descartes.Layer.WMSC(
            layer_Def.title, 
            layer_Def.layerDefinition, 
            layer_Def.options);
      } else if (isWMTS) {
        
        var layerName = jsonLayerDesc.Extension.wms_name;
        
        var maxExtent_ = this.context.getMaxExtent();

        //var version =  jsonLayerDesc.Server.attributes.version;
        var outputFormat = jsonLayerDesc.Extension.wms_format;
        var style = jsonLayerDesc.Extension.wmts_style;
        var tileset = jsonLayerDesc.Extension.wmts_tileset;
        var wmts_layer_name = jsonLayerDesc.Extension.wmts_wms_layer;
        
        // resolution
        var serverResolution = decodeURIComponent(jsonLayerDesc.Extension.wmts_resolutions).split(Descartes.StringSplitters.WMTS.RESOLUTIONS);
        if (!isNaN(new Number(serverResolution[0]))) {
          serverResolution = Ext.Array.map(serverResolution, parseFloat);
        }
        for(var i=0;i<serverResolution.length;i++){
          serverResolution[i] = (serverResolution[i]/0.75)*0.7500000000000001;
        }
        
        // matrixIds
        var matrixIds = decodeURIComponent(jsonLayerDesc.Extension.wmts_matrixids).split(Descartes.StringSplitters.WMTS.MATRIXIDS);
        var tileOrigins = decodeURIComponent(jsonLayerDesc.Extension.wmts_tileorigin).split(Descartes.StringSplitters.WMTS.TILEORIGINS);  
        var tileOrigin;
        for (var i=0; i<matrixIds.length; i++) {
            var coords = tileOrigins[i].split(Descartes.StringSplitters.WMTS.MATRIXLATLON);
            var tlc = new OpenLayers.LonLat(parseFloat(coords[0]), parseFloat(coords[1]));
            
            if ( !tileOrigin ) tileOrigin = tlc;
            
            matrixIds[i] = {identifier : matrixIds[i], topLeftCorner : tlc };
            
        }

          
        var layer_Def = {
            title: att.name, 
            layerDefinition : {
              serverUrl: providerUrl,
              layerName: layerName,
              featureServerUrl: providerUrl,
              featureName: layerName,
              params : {
                version : version
              }
            },
            options: {
              maxExtent: maxExtent_,
              tileOrigin : tileOrigin,
              matrixIds : matrixIds,
            	matrixSet : jsonLayerDesc.Extension.wmts_tileset,
              resolutions: serverResolution,
              serverResolutions : serverResolution,
              format: outputFormat,
              style: style,
              
              visible: (att.visible == "1"),
              alwaysVisible: false,
              queryable: queryable,
              activeToQuery: queryable,
              activeToToolTip: activeToToolTip,
              sheetable: false,
              legend: [],
              minScale: minScale,
              maxScale: maxScale,
              opacity: att.opacity * 100,
              opacityMax: 100,
              metadataURL: jsonLayerDesc.Extension.layerSettings_Metadata,
/*REMOVE*///              queryLimitedToMapMaxExtent: true,
              transparent: true,
              configuration : {
                attributes : att,
                jsonLayerDesc : jsonLayerDesc
              }
            }
        };
          
        item = new Descartes.Layer.WMTS(
            layer_Def.title, 
            layer_Def.layerDefinition, 
            layer_Def.options);
      }
      
    } else if ('Document' in jsonLayerDesc) {
      
      var style = {};
      
      if (jsonLayerDesc.Extension.layerSettings_olStyle && jsonLayerDesc.Extension.layerSettings_olStyle.defaultStyle) {
        style = jsonLayerDesc.Extension.layerSettings_olStyle.defaultStyle;
      }
      
      var layerDefinition = {
        projection: this.context.getProjection(),
        style : style
      };
      
      var options = {
        visible: (att.visible == "1"),
        alwaysVisible: false,
        queryable: false,
        activeToQuery: false,
        activeToToolTip: false,
        sheetable: false,
        legend: [],
        minScale: minScale,
        maxScale: maxScale,
        opacity: att.opacity * 100,
        opacityMax: 100,
        annotationConfig : {
          data : jsonLayerDesc.Document
        }
      };
      
      item = new Descartes.Layer.Annotation(att.name, layerDefinition, options);
    }
        
    if (item) {
      
      var treeNode = this._convertNode(jsonNode, jsonLayerList);
      var p = (parentItem || this.mapContent.item);
      // hack for legend
      
      item.TREE_NODE = treeNode;
      
      if (parentItem) {
        this.mapContent.addItem(item, parentItem);
      } else {
        this.mapContent.addItem(item);
      }
      treeNode.OL_ITEM = {id : item.id, index : item.index, parentId : p.id, parentIndex : p.index};
      treeNode.icon = Descartes.Application.getLegendGraphicURL(item.OL_layers[0]);
    }
    return item;
  },

  /**
   * @brief delete the map object and 
   * clean all its dependencies
   * 
   */ 
  _cleanMap: function() {
    if (this.map!=null) {
      // destroying ol map
      this.map.OL_map.destroy();
      // destroying descartes map
      delete this.map.OL_map;
      delete this.map;
    }
  },
  
  /**
   * @brief init the map object from the context
   *
   */  
  _initMap: function() {
    // cleaning existing tools
    this._cleanTools();
    
    // cleaning existing map
    this._cleanMap();
  
    var ctx = this.context;
    
    // Modif SWT : #6210 [P2-17] Modification de la couleur de
    // fond de carte.
    // La valeur est écrite depuis le backoffice dans le champ
    // de metadonnée "WEB" nommé IMAGECOLOR (voir
    // CSImageBgColor.php côté backoffice).
    // Ce champ est mappé par tagmapping_carmen.csv vers
    // le champ de métadonnée "backgroundImageColor".
    // On la positionne cette valeur directement sur l'élément
    // HTML qui contient la carte.
    // Je ne sais pas si c'est le bon endroit pour faire ça, on
    // pourrait le faire plus tôt (à partir du moment où on a
    // le contexte et le div pour la carte...)
    
    if (ctx.mdataMap.backgroundImageColor) {
      this.ui.getMapDiv().setStyle("background-color", "#" + ctx.mdataMap.backgroundImageColor);
    }
    
    var panelContainer = this.ui.getMapPanel();
    var panelContainerSize = panelContainer.body.getSize(false);
    var size = new OpenLayers.Size(panelContainerSize.width,panelContainerSize.height);
    panelContainer.on('resize', function(){this.map.OL_map.updateSize()}, this)
    
    var initExtent = this._getInitialExtent(size);
     
    // allowing 2 levels of zoom out fro maxExtent in admin
    var maxExtentInAdmin = ctx.getMaxExtent();
    maxExtentInAdmin = maxExtentInAdmin.scale(8);
     
    var mapOptions = {
      size: size,
      initExtent: initExtent,
      maxExtent: maxExtentInAdmin,
      //restrictedExtent: ctx.getMaxExtent(), // no restriction on extent in admin
      projection: ctx.getProjection(),
      maxResolution : 'auto',
      fractionalZoom : true,
      numZoomLevels : 32,
      allOverlays : true,
      units : ctx.getProjectionUnits(),
      zoomOutsideMaxExtent : true, 
      controls: [],
      theme: Descartes.getThemeLocation()
    };

    // Modif SWT [P2-13] 6185 Améliorer le support des couches WMS-C
    // En fait le code précédent ne fonctionnait pas dans le cas du WMS-C !
    // En effet, ContinuousScalesMap spécifie en dur "fractionalZoom = true"
    // ce qui écraserait le "fractionalZoom = false" positionné si on a
    // un layer WMS-C.
    // Mais en fait c'est même pire que ça !!!
    // Les options spécifiées dans "mapOptions" sont positionnées dans la
    // map (l'instance de ContinuousScalesMap), mais ne sont pas utilisées
    // lors de l'appel à createOlMap. Donc même les résolutions spécifiées
    // sont ignorées !
    // Donc je change pour utiliser DiscreteScalesMap dans le cas WMS-C.

    // checking for Tile Cache (aka WMS-C layers)
    // if tile cache --> predefined zoom levels
    // so chganging default mapOptions
    if (ctx.hasLayerWMSC()) {
      // Mise en commentaire des propriétés qui ne seront pas utilisées.
//      mapOptions.fractionalZoom = false;
//      mapOptions.numZoomLevels = null;
//      mapOptions.maxResolution = null;
      // C'est une valeur par défaut complètement arbitraire et liée à la projection.
      mapOptions.resolutions = Carmen.Util.WMSC_RESOLUTIONS; 
      // On va essayer de prendre les résolutions du premier layer WMS-C qui a la
      // même projection que la carte et qui est correctement configuré.
      var compatibleWMSCLayer = null;
      for (var k in ctx.mdataLayer) {
        if (Ext.valueFrom(ctx.mdataLayer[k].wmscLayer, "OFF", false)=="ON") {
          var layerSRS = ctx.mdataLayer[k].wms_srs;
          if (!layerSRS) {
            continue;
          }
          layerSRS = unescape(layerSRS).toUpperCase();
          if (layerSRS !== ctx.mapProjection.toUpperCase()) {
            continue;
          }
          // On a le bon SRS, on vérifie maintenant les autres
          // paramètres.
          var layerResolutions = ctx.mdataLayer[k].wmsc_resolution;
          if (!layerResolutions) {
            continue;
          }
          var layerBBOX = ctx.mdataLayer[k].wmsc_boundingbox;
          if (!layerBBOX) {
            continue;
          }
          // On a trouvé notre layer de référence WMS-C.
          // S'il y a plusieurs layers WMS-C, on ne garantit pas
          // qu'ils fonctionneront tous (ils ont peut être des
          // configurations incompatibles) mais celui-ci
          // fonctionnera.
          compatibleWMSCLayer = ctx.mdataLayer[k];
          break;
        }
      }
      if (compatibleWMSCLayer) {
        var resolutionsStr = compatibleWMSCLayer.wmsc_resolution;
        resolutionsStr = Url.decode(resolutionsStr);
        var sep = resolutionsStr.indexOf(',') != -1 ? ',' : ' ';
        var res = Carmen.Util.chompAndSplit(resolutionsStr, sep);
        mapOptions.resolutions = Carmen.Util.array_map(res, parseFloat);
        // Se caler sur les niveaux de résolution du WMS-C ne suffit pas.
        // Dans WMS-C le tuilage est aligné à partir d'un (X, Y) d'origine.
        // Dans les versions récentes d'OpenLayers, il existe un paramètre "tileOrigin"
        // sur les layers "Grid" qui permet de le spécifier indépendamment pour chaque
        // layer.
        // Dans cette version, c'est impossible. C'est l'extent de la carte qui doit
        // être "calé" sur les tuiles.
        // De plus, le comportement de maxExtent/restrictedExtent ne permet pas de
        // paramétrer cela facilement.
        // On doit donc calculer "à la main" de nouvelles valeurs.
        var boundingBoxStr = compatibleWMSCLayer.wmsc_boundingbox;
        boundingBoxStr = Url.decode(boundingBoxStr)
        sep = boundingBoxStr.indexOf(',') != -1 ? ',' : ' ';
        var bb = Descartes.Utils.chompAndSplit(boundingBoxStr, sep);
        var tileOrigin = new OpenLayers.LonLat(bb[0], bb[1]);
        // Pour le calcul, on a besoin de la taille des tuiles.
        var tileSize = new OpenLayers.Size(256, 256);
        var tileSizeStr = compatibleWMSCLayer.wmsc_tilesize;
        if (tileSizeStr) {
          tileSizeStr = Url.decode(tileSizeStr);
          sep = tileSizeStr.indexOf(',') != -1 ? ',' : ' ';
          var ts = Descartes.Utils.chompAndSplit(tileSizeStr, sep);
          tileSize.w = ts[0];
          tileSize.h = ts[1];
        }
        var fixedOptions = this._computeBestWMSCExtent(mapOptions.maxExtent, tileOrigin, mapOptions.resolutions, tileSize, size);
        OpenLayers.Util.extend(mapOptions, fixedOptions);
        //mapOptions.fixedMaxExtent = true;
      }
      // On utilise DiscreteScales si on a un WMS-C.
      // On ne devrait le faire que si on a un "compatibleWMSCLayer",
      // mais je garde ce fonctionnement pour rester sur le comportement
      // précédent qui utilisait Carmen.Util.WMSC_RESOLUTIONS, même
      // si ça ne devrait pas être fait comme ça...
      this.map = new Descartes.Map.DiscreteScalesMap(this.ui.getMapDiv().id, this.mapContent, mapOptions);
    } else {
      // On utilise ContinuousScales seulement si on n'a pas de WMS-C.
      this.map = new Descartes.Map.ContinuousScalesMap(this.ui.getMapDiv().id, this.mapContent, mapOptions);
    }
    this.map.OL_map.tileManager.destroy();
    this.map.app = this;
    this.map.OL_map.app = this;
    
    
    //modif dds
    this._initStartLocation();
    
    this.map.show();
    
    var urlParams = Ext.urlDecode(window.location.href);
    
    var object = Ext.valueFrom(urlParams.object, null, false);
    if (object !=null ) {
      var center=this.map.OL_map.getCenter();
      scale=this.map.OL_map.getScale();
      var layerName=object.split(';')[0];
      var layer=this.map.OL_map.getLayersByName(layerName)[0];
      if(layer && layer.maxScale>scale){
        this.map.OL_map.zoomToScale(layer.maxScale,true);
        this.map.OL_map.setCenter(center);
        scale=layer.maxScale;
      }
      if(this.context.generalExt.MINSCALEDENOM){
        if(this.context.generalExt.MINSCALEDENOM>scale){
          this.map.OL_map.zoomToScale(this.context.generalExt.MINSCALEDENOM,true);
          this.map.OL_map.setCenter(center);
        }
      }
      //this.map.OL_map.setCenter(center);
    }
    
    // in backoffice, forcing no restrictin on Extent
    for (i=0;i<this.map.OL_map.layers.length;i++) {
      this.map.OL_map.layers[i].displayOutsideMaxExtent=true
    }
    this.map.OL_map.options.initExtent = initExtent;
    
    
    return this.map;
  },
  
  _computeBestWMSCExtent: function(extentToView, tileOrigin, resolutions, tileSize, screenSize) {
    // Le but de l'algorithme c'est de trouver la résolution la plus
    // fine possible parmis les résolutions données pour laquelle la
    // zone à visualiser (extentToView) est contenue dans l'écran.
    // Concrêtement, on regarde à la résolution la plus grossière le
    // nombre de tuiles nécessaires pour couvrir la zone à visualiser.
    // On passe à la résolution suivante tant qu'il n'y a pas trop de
    // tuiles pour rentrer dans screenSize.
    // Une fois cette résolution la plus fine trouvée, on renvoie la
    // liste des résolutions à laquelle on aura enlevé les résolutions
    // passées (on garde celle trouvée), ainsi que l'extent des tuiles
    // couvrantes à la résolution trouvée.

    var resIdx = 0;
    var gridExtent = null;
    while (resIdx < resolutions.length) {
      var resolution = resolutions[resIdx]; 

      // Taille d'une tuile en coordonnées monde
      // (pas forcément latlon, ca dépend du SRS)
      var tilelon = resolution * tileSize.w;
        var tilelat = resolution * tileSize.h;
        
        var tilexmin = Math.floor((extentToView.left - tileOrigin.lon) / tilelon);
        var tilexmax = Math.ceil((extentToView.right - tileOrigin.lon) / tilelon);
        var tileymin = Math.floor((extentToView.bottom - tileOrigin.lat) / tilelat);
        var tileymax = Math.ceil((extentToView.top - tileOrigin.lat) / tilelat);
        
        var numtilesx = tilexmax - tilexmin;
        var numtilesy = tileymax - tileymin;
        
        var sizex = numtilesx * tileSize.w;
        if (sizex > screenSize.w) {
          break;
        }
        var sizey = numtilesy * tileSize.h;
        if (sizey > screenSize.h) {
          break;
        }
        
        gridExtent = new OpenLayers.Bounds(
            tileOrigin.lon + tilexmin * tilelon,
            tileOrigin.lat + tileymin * tilelat,
            tileOrigin.lon + tilexmax * tilelon,
            tileOrigin.lat + tileymax * tilelat);
      
      resIdx++;
    }
    
    if (resIdx == resolutions.length || resIdx == 0) {
      return {};
    }
    
    return {
      resolutions: resolutions.slice(resIdx - 1),
      maxExtent: gridExtent,
      restrictedExtent: gridExtent,
      initExtent: gridExtent
      };
  },
  
  /***
   * Positionne la carte si certains paramètres sont passés dans l'URL (center et scale)
   * center : position du centre de la carte
   * scale : echelle de la carte
   * */
  _initStartLocation: function() {
    //modif dds
    var urlParams = Ext.urlDecode(window.location.href);
    var extent = Ext.valueFrom(urlParams.extent, null, false);
    var scale = Ext.valueFrom(urlParams.scale, null, false);
    var center = Ext.valueFrom(urlParams.center, null, false);
    var object = Ext.valueFrom(urlParams.object, null, false);
    
    if (scale !=null && center!=null) {
      var boundsCoordinate = center.split(",");
          var left = boundsCoordinate[0]; //xCenter;
          var bottom = boundsCoordinate[1]; //yCenter;
          
          //La fonction zoomToScale ne marche que si le centre de la carte a déjà été positionné
      this.map.OL_map.setCenter(new OpenLayers.LonLat(left,bottom));
      this.map.OL_map.zoomToScale(scale,true);
      //zoomToScale a pu deplacer le centre de la carte donc on relance un setCenter
      this.map.OL_map.setCenter(new OpenLayers.LonLat(left,bottom));
    }
    
    
    
  },
  _getInitialExtent: function(mapsize) {

    // looking for contextual parameters in URL
    var urlParams = Ext.urlDecode(window.location.href);
    var extent = Ext.valueFrom(urlParams.extent, null, false);
    
    if (extent!=null) {
      extent = extent.replace(new RegExp('[\\(\\)]', 'g'),'');
          
          var boundsCoordinate = extent.split(",");
          var left = boundsCoordinate[0]; //xMin;
          var bottom = boundsCoordinate[1]; //yMin;
          var right = boundsCoordinate[2]; //xMax;
          var top = boundsCoordinate[3]; //yMax;
          var initBounds = new OpenLayers.Bounds(left, bottom, right, top);  
           
      var ratio = Ext.valueFrom(urlParams.ratio, null, false); 
          if (ratio==null) {
             ratio = 0;
          } else {
             ratio = ratio / 2;
          }
          
          initBounds = Descartes.Utils.extendBounds(initBounds, mapsize, ratio);
      
      return initBounds;
    }
    
        var initialExtent = this.context.getExtent();
        
        if (initialExtent) {
          return initialExtent;
        }
        
        return null;
    },
  
  
  /**
   * @brief initialize the maptoolbar layout by adding
   * navigation tools
   * 
   */ 
  _initMapToolBar: function() {
    // building controls without associating them to a map
    this.mapTools['ZoomIn']  = new Carmen.Control.ZoomIn();
    this.mapTools['ZoomOut'] = new Carmen.Control.ZoomOut();
    this.mapTools['DragPan'] = new Carmen.Control.DragPan();
    this.mapTools['ZoomFitAll'] = new (Carmen.Control.wrapper(
      OpenLayers.Class(OpenLayers.Control,
        {
          trigger: function () { 
            var extent = this.map.options.initExtent!=null ? 
              this.map.options.initExtent :
              this.map.getMaxExtent();
            this.map.zoomToExtent(extent);
            }
        }),
         "ZoomFitAll",
        {
          xtype : 'button',
          tooltip: 'Zoom global',      
          tooltipType: 'qtip',
          cls: 'x-btn-icon',
          enableToggle: false,
          text: '<i class="fa fa-globe fa-2x"></i>',
          width: ctrlWidth,
          height: ctrlHeight
        }))(); 
    
    this.mapTools['ZoomHistory'] = new Carmen.Control.ZoomHistory({displayConfig : Carmen.Control.ZoomHistory.BOTH_BTN});
    
    var informationOptions = {
        configButton : {
          floatToolBar: {
            id:'selection',
            textToolBar:this.getMessage('INFORMATION_BUTTON'),
            iconClsPrefix:'DescartesToolInformation'
          }
        }
      };
      var informationPointOptions = {
        configButton : OpenLayers.Util.extend({isDefault:true}, informationOptions.configButton)
      };
    
    
    this.mapTools['Info'] = new 
      (OpenLayers.Class(Descartes.Tool.InformationRect, {
        
        // UI components
        btn: new Ext.Button({
            tooltip: 'Information',      
            tooltipType: 'qtip',
            cls: 'x-btn-icon', /* cmn-tool-info',*/
            enableToggle: true,
            toggleGroup: 'mainMapControl',
            disabled: true,
            text : '<i class="fa fa-info fa-2x"></i>',
            width: ctrlWidth,
            height: ctrlHeight
        }),

        getButton : function(){return this.btn},
        
        setMap: function(map){
          Descartes.Tool.InformationPoint.prototype.setMap.call(this, map);
          this.btn.addListener('toggle', Carmen.Util.buildExt2olHandlerToggle(this));
        },
        
        
        
        setMapfile : function(mapfile) { 
          this.mapfile = mapfile; 
        }
  
      }))(informationPointOptions);
    
    this.mapTools['Tooltip'] = new 
      (OpenLayers.Class(OpenLayers.Control, {
        
        // UI components
        btn: new Ext.Button({
            tooltip: 'Infobulles',      
            tooltipType: 'qtip',
            cls: 'x-btn-icon', /* cmn-tool-info',*/
            enableToggle: true,
            toggleGroup: 'mainMapControl',
            disabled: true,
            text : '<i class="fa fa-comment fa-2x"></i>',
            width: ctrlWidth,
            height: ctrlHeight
        }),

        getButton : function(){return this.btn},
        
        activate : function() {
          app.toolTipsEnabled = true;
          app.map.toolTip.activate();
        },
        
        deactivate : function() {
          app.toolTipsEnabled = false;
          app.map.toolTip.deactivate();
        },
        
        setMap: function(map){
          OpenLayers.Control.prototype.setMap.call(this, map);
          this.btn.addListener('toggle', Carmen.Util.buildExt2olHandlerToggle(this));
        }
        
      }))();
    
    this.mapTools['RequestAttribut'] = new Carmen.Control.AdvancedQueryAttributes();
    this.mapTools['Measure'] = new Carmen.Control.Measure();
    this.mapTools['FavoriteAreas'] = new Carmen.Control.FavoriteAreas();
    
    // add controls to toolbar
    this.ui.resetToolbarPanel();
    this.ui.addMapToolToToolbarPanel(this.mapTools['DragPan']);
    this.ui.addMapToolToToolbarPanel(this.mapTools['ZoomIn']);
    this.ui.addMapToolToToolbarPanel(this.mapTools['ZoomOut']);
    this.ui.addMapToolToToolbarPanel(this.mapTools['ZoomFitAll']);
    this.ui.addMapToolToToolbarPanel(this.mapTools['ZoomHistory']);
    this.ui.addMapToolToToolbarPanel(this.mapTools['Info']);
    this.ui.addMapToolToToolbarPanel(this.mapTools['Tooltip']);
    this.ui.addMapToolToToolbarPanel(this.mapTools['RequestAttribut']);
    this.ui.addMapToolToToolbarPanel(this.mapTools['Measure']);
    
    this.ui.addMapToolToToolbarPanel(this.mapTools['FavoriteAreas']);
    
    this.ui.configureMapTools();
    //this.ui.getToolbarPanel().disable();
    
    
    
    
  },  
  
  /**
   * @brief deattach controls from map
   */ 
  _cleanTools: function() {
    if (this.map!=null) {
      // removing control from ol map
      for (var c in this.mapTools) {
      	this.mapTools[c].deactivate && this.mapTools[c].deactivate();
        this.map.OL_map.removeControl(this.mapTools[c]);
      }
      while (this.map.OL_map.controls.length) {
        this.map.OL_map.controls[0].deactivate && this.map.OL_map.controls[0].deactivate();
        this.map.OL_map.removeControl(this.map.OL_map.controls[0]);
      }
      Carmen.Util.buildExt2olHandlerToggle(null)(null, true);
      
      // removing descartes actions
      for (var actionName in this.mapActions) {
        var a = this.mapActions[actionName];
        if ( !a ) continue;
        var r = a.renderer;
        if (r!=null && r.destroy!=null && typeof(r.destroy)=="function") {
          r.destroy();
        }
        a.renderer = null;
      }
      
      // cleaning descartes infos
      for (var infoName in this.mapInfos) {
        var i = this.mapInfos[infoName];
        if (i.div!=null)
          i.div.innerHTML="";
      }
      
    }
    //this.ui.getToolbarPanel().disable();  
  },
  
  /**
   * @brief attach controls to map
   */  
  _initTools: function() {    
    var ctx = this.context;
    var map = this.map;
    
    // attach tools to the map
    for (var c in this.mapTools) {
      this.map.OL_map.addControl(this.mapTools[c]);
    }
    // init favorites areas if necessary
    geoBookmarkConfig = this.context.getFavoriteAreas();
    if ('FavoriteAreas' in this.mapTools && geoBookmarkConfig!=null) {
      this.mapTools['FavoriteAreas'].initFromContext(geoBookmarkConfig);
    }

    // referenceMap
    var rm_config = ctx.mdataMap.ReferenceMap;
    var rm_imgUrl = rm_config.OnlineResource.attributes.href;
    var rm_size = new OpenLayers.Size(rm_config.attributes.width, rm_config.attributes.height);
    var rm_lc = rm_config.BoundingBox.LowerCorner.split(" ");
    var rm_uc = rm_config.BoundingBox.UpperCorner.split(" ");
    var rm_maxExtent = new OpenLayers.Bounds(rm_lc[0], rm_lc[1], rm_uc[0], rm_uc[1]);
    var rm_projection = ctx.getProjection();
    var rm_projectionUnits = ctx.getProjectionUnits();

    //remove previous minimap element
    Ext.each(this.ui.referencePanel.getDiv().query('.cmnControlOverviewMapElement', false), function(el){el.destroy();});
    this.miniMap = new Descartes.Tool.MiniMapImage(
      map.OL_map, 
      rm_imgUrl, 
      {
        size:rm_size, 
        maxExtent:rm_maxExtent, 
        projection:rm_projection, 
        units:rm_projectionUnits, 
        div: (this.ui.referencePanel.getDiv()).dom
      });

    // mouse position
    Ext.getCmp('bottomToolbar_crsInfo').update("SRS : " + Descartes.Utils.getProjectionName(this.map.OL_map.getProjection())+" ("+this.map.OL_map.getProjection()+")");
    this.mapInfos['mousePosition'] = map.addInfo(
      {
        type : Descartes.Map.MOUSE_POSITION_INFO, 
        div : 'bottomToolbar_mousePosition',
        options: {
          projection: false,
          numDigits : 2,
          formatOutput: function(lonLat) {
             var digits = parseInt(this.numDigits, 10);
             var newHtml = "";
             if (this.projection){
               newHtml += "SRS : " + Descartes.Utils.getProjectionName(this.map.getProjection()) +" ("+this.map.OL_map.getProjection()+")" + " ";
             }
             newHtml +=
                 this.prefix +
                 'X : '+Descartes.Utils.localizeNumber(lonLat.lon,digits) +
                 ' / Y : '+Descartes.Utils.localizeNumber(lonLat.lat,digits);
             return newHtml;
          }
        }
      }
    );
    
    // graphic scale
    this.mapInfos['scaleInfo'] = map.addInfo(
      {
        type : Descartes.Map.GRAPHIC_SCALE_INFO, 
        div : 'bottomToolbar_graphicScale' 
      }
    );
    this.mapInfos['scaleInfo'] = map.addInfo(
      {
        type : Descartes.Map.METRIC_SCALE_INFO, 
        div : 'bottomToolbar_textScale' 
      }
    );
    
    // scale selector
    this.mapActions['scaleSelector'] = map.addAction(
      {
        type : Descartes.Map.SCALE_SELECTOR_ACTION, 
        div : this.ui.getBottomToolBarDiv(), 
        options: 
          {
            label: true, 
            toolBar: this.ui.getBottomToolBar(),
            toolBarPos: 0
          }
      }
    );
    
    




    this.ui.doLayout();

    
    
     // inlays: scale, attributions...  
    if (Ext.valueFrom(ctx.mdataMap.inlay, "0") == "1") {
      // parameters for map inlays 
      var inlayParams = {
        displayTxt : Ext.valueFrom(ctx.mdataMap.scaleTxt, "0") == "1",
        displayImg : Ext.valueFrom(ctx.mdataMap.scaleImg, "0")== "1",
        displayLogo : Ext.valueFrom(ctx.mdataMap.logo, "0")== "1",
        fontSize : Ext.valueFrom(ctx.mdataMap.fontSize, null),
        color : Ext.valueFrom(ctx.mdataMap.fontColor, null),
        backgroundColor : Ext.valueFrom(ctx.mdataMap.backgroundColor, null),
        logoPath : Ext.valueFrom(Url.decode(ctx.mdataMap.logoPath), null)
      };
      
      // incrustation
      if (ctx.mdataMap.font) {
        OpenLayers.Util.extend(inlayParams, Carmen.Util.carmenFontDescToFontStyles(ctx.mdataMap.font));
      }

      // Setting map attribution to the base layer
      if (Ext.valueFrom(ctx.mdataMap.copyright, "0") == "1") {
        map.OL_map.baseLayer.attribution = Url.decode(ctx.mdataMap.copyrightTxt);
        map.addInfo({type : Descartes.Info.AttributionCarmen, div : null, options : inlayParams});
      }
            
            /*
            if (inlayParams.displayImg && inlayParams.displayTxt){
                var scale_options = 'both';
            } else {
              if (inlayParams.displayImg || inlayParams.displayTxt){
                var scale_options = 'alone';
              } else {
                var scale_options = 'none';
              }
            }
            scale_options = "none";
            if (true || inlayParams.displayImg) {
                map.addInfo({type : Descartes.Map.GRAPHIC_SCALE_INFO, div : 'bottomToolbar_graphicScale', options : {'scale_options': scale_options}});
            }
            
            // * Echelle numérique (qualifiée dans Descartes de "metric") 
            if (true || inlayParams.displayTxt) {
                map.addInfo({type : Descartes.Map.METRIC_SCALE_INFO,  
        div : 'bottomToolbar_textScale' , options : {'scale_options': scale_options}});
            }*/

    }
    
    var tooltipConf = this._initTooltipConfig();
    // obedel pb with control init
    this.map.addToolTip('ToolTip', tooltipConf);
    this.map.toolTip.deactivate();
    this.mapTools['Tooltip'].btn.setDisabled(false);
    this.mapTools['RequestAttribut'].btn.setDisabled(false);
    
    this.mapTools['Info'].setMapContent(this.mapContent);
    this.mapTools['Info'].btn.setDisabled(false);
      
      
    //}
    
    
    //map.addToolTip('ToolTip', tooltipConf);
//    map.addToolsInToolBar([{type : Descartes.Map.RECTANGLE_SELECTION, args : {resultUiParams:{type:Descartes.UI.TYPE_POPUP, withReturn:true}}}]);

        // Ajout du controle pour avoir la gestion de la navigation à la "molette"
        //Obligatoirement après addToolTip sinon on a une erreur
        // (cf méthode dans sites/carmenfront/descartes-extension/lib/Descartes/Core/MapOverloaded.js)
        this.map.addOpenLayersNavigationControl({type : Descartes.Map.OL_NAVIGATION});
    
    this.ui.doLayout();
  },  
    
  _initTooltipConfig : function(){
    //Gestion des tooltips
    // Ajout du gestionnaire d'info-bulle
    var tooltipConf = [];
    var layers = this.mapContent.getLayers();
    for(var i=0; i<layers.length; i++) {
      var layer = layers[i];
      layer.title = decodeURIComponent(layer.title);
      if (!("configuration" in layer)) {
        continue;
      }
      
      var jsonLayerDesc = layer.configuration.jsonLayerDesc;
      if (jsonLayerDesc.Extension.layerSettings_ToolTipFields && jsonLayerDesc.Extension.layerSettings_ToolTipFields.length > 0) {
        var toolTipFields = decodeURIComponent(jsonLayerDesc.Extension.layerSettings_ToolTipFields);
        var baseQueryURL = decodeURIComponent(jsonLayerDesc.Extension.layerSettings_queryURL);
        var list = Descartes.Utils.chompAndSplit(toolTipFields,';');
        var fields = [];
        var titles = [];
        var alias = {};
        var typeTitles = [];
        var typesFields = [];
        
        for (var j = 0; j < list.length; j++) {
          if(list[j]!=""){
                  var desc = Descartes.Utils.decodeDescartesFieldDesc(list[j], baseQueryURL);
                  
                  if (desc.title) {
                    // Si desc.title est vrai, on ajoute au layer.title et nom au tableau fields
              titles.push(decodeURIComponent(desc.name));
                    typeTitles.push(desc.type);
                  } else {
                    // sinon aux champs
                    fields.push(desc.name);
                    typesFields.push(desc.type);
                    alias[desc.name] = desc.alias;
                  }
              }
        }
              if (fields.length>0 || titles.length>0) {
                tooltipConf.push({
                  layer : layer,
                  fields : fields,
                  titles : titles,
                  alias : alias,
                  typeTitles : typeTitles,
                  typesFields : typesFields
                });
              }
      }
    }
    return tooltipConf;
  },  

  
  /**
   * fill the carmen ext layerTreeStore 
   * from the ows layerTree description
   */ 
  _initLayerTree : function() {
    // build tree desc  
    var rootNode = this._buildLayerTreeNode();
    this.layerTreeStore.setRoot(rootNode);
    this.layerTreeStore.OL_MAP = this.mapContent;
  },
  
  
  /**
   * fill the carmen ext layerTreePrintStore 
   * from the ows layerTree description
   */ 
  _initLayerTreePrint : function() {
    // build tree desc  
  	var me = this;
    var ctx = this.context;
    var jsonNode = ctx.mdataMap.LayerTreePrint.LayerTreeNode;
    var jsonLayerList = ctx.layer;
    
    var rootNode = this._convertNode(jsonNode, jsonLayerList, true);
    var copyAttributes = function(printNode){
      var treeNode = me.layerTreeStore.findRecord('nodeId', printNode.nodeId);
      if ( treeNode ){
        printNode.icon = treeNode.get('icon');
      }
      if ( !printNode.children ) return;
      Ext.each(printNode.children, copyAttributes);
    }
    copyAttributes(rootNode);
    
    this.layerTreePrintStore.setRoot(rootNode);
    
  },
  
  /**
   * build the layerTree data
   * from the ows layerTree description
   */   
  _buildLayerTreeNode : function() {
    var root = this.mapContent.item;
    
    /*RESET*/
    if ( this.layerTreeStore )
      this.layerTreeStore.removeAll();
    if ( root.TREE_NODE ){
    	root.TREE_NODE.children = [];
    }
    
    return this._constructTree(root);
  },
  
  _constructTree : function(parentItem){
    var node = parentItem.TREE_NODE;
    parentItem.TREE_NODE = null;
    if ( !node ) return null;
    
    
    if ( !parentItem.items || !(parentItem.items instanceof Array) ) {
      return node; 
    }
      
    if ( parentItem.items.length ) 
      node.children = node.children || [];
    
    var child;
    for (var i=0; i<parentItem.items.length; i++){
      child = this._constructTree(parentItem.items[i]);
      node.children.push(child);
      node.leaf = false;
    }
    return node;
  },

  /**
   * @brief convert an ows jsonNode description
   * to an extjs LayerTreeNode description
   */ 
  _convertNode: function(jsonNode ,jsonLayerList, is_recursive) {
    var extNode = {};
    if ( ('LayerTreeNode' in jsonNode) && !(jsonNode.LayerTreeNode instanceof Array) ){
      jsonNode = jsonNode.LayerTreeNode;
    }
    var att = jsonNode.attributes;
    var isRoot = (att.name == "root");
    // testing node type
    switch(att.type) {
      case "group":
        extNode = isRoot ?
          Ext.apply({}, Carmen.LayerTree.defaultRootNode) :
          Ext.apply({}, Carmen.LayerTree.defaultGroupNode);
          
        if (!isRoot) {
          var config = {
          	attributes : att,
            nodeId: att.nodeId,
            forceOpacityControl: (att.forceOpacityControl == "1"),
            expanded: (att.expanded),
            forceOpacityControl: (att.forceOpacityControl == "1"),
            text: decodeURIComponent(att.name),
            checked : att.visible=="1",
            children : []
          };
          extNode = Ext.apply(extNode, config);
        }

        if ( is_recursive ){
          // recursion
          var children = [];
          if ('LayerTreeNode' in jsonNode) {
            if (jsonNode.LayerTreeNode instanceof Array) {
              for (var i = 0; i < jsonNode.LayerTreeNode.length; i++) {
                children.push(this._convertNode(jsonNode.LayerTreeNode[i], jsonLayerList, is_recursive));
              }
            } else {
              children.push(this._convertNode(jsonNode.LayerTreeNode, jsonLayerList, is_recursive));
            }
          }
          extNode.children = children;
          extNode.visible = false;
          for(var i=0; i<children.length; i++){
          	if ( children[i].visible ){
          		extNode.visible = true;
          	}
          }
        }
      break;
      default: // layer type
        extNode = Ext.apply({}, Carmen.LayerTree.defaultLayerNode);
        var config = {
          attributes : att,
          forceOpacityControl: (att.forceOpacityControl == "1"),
          opacity: (att.opacity),
          expanded: (att.open == "1"),
          layerId: att.layerId,
          nodeId: att.nodeId,
          layerIdx: att.layerIdx,
          mapName: att.mapName,
          text: decodeURIComponent(att.name),
          checked : att.visible=="1"
        };
        extNode = Ext.apply(extNode, config);
    }
    return extNode;
  },
  
  /**
   * @brief load a new layer in a map from its conetxt description
   * @param jsonContext : owscontext in json
   * @param layerId : id of the new layer
   * 
   */ 
  updateLayerFromContext: function(jsonContext, layerId, treeSelection) {
    
    if (jsonContext==null)
      return;
    this._parseContext(jsonContext);
    
    // adding the layerNode in the layerTree
    var newLayerTreeNode = this.context.getLayerTreeNodeFromMdataValue("layerId",layerId);
    if ( !newLayerTreeNode ) return;
    var att = newLayerTreeNode.attributes;
    var existingNode = this.layerTreeStore.findNode("layerId", att.layerId);
    if (existingNode!=null) {
      var parentItem = this.mapContent.item;
      if ( treeSelection.length ){
        treeSelection = treeSelection[0];
        if ( treeSelection.get('type')!="group" ){
          treeSelection = treeSelection.parentNode;
        }
        parentItem = this.mapContent.getItemById(treeSelection.get('OL_ITEM').id, false);
      } else {
        treeSelection = this.layerTreeStore.getRoot();
      }
    
      // adding the layer in the map
      var oldLayerItem = app.mapContent.getItemById(existingNode.get('OL_ITEM').id);
      var oldLayerOL = app.map.OL_map.getLayer(oldLayerItem.OL_layers[0].id);
      var layerIndex = app.map.OL_map.getLayerIndex(oldLayerOL);
      app.map.OL_map.removeLayer(oldLayerOL, false);
      app.mapContent.removeItemByIndex(oldLayerItem.index);
                
      var OL_ITEM = this._addLayer(newLayerTreeNode, this._jsonLayerList, parentItem);
      var extLayerNode = OL_ITEM.TREE_NODE;
      delete OL_ITEM.TREE_NODE;
    
      app.map.OL_map.setLayerIndex(OL_ITEM.OL_layers[0], layerIndex);
      
      var newLayerId = extLayerNode.OL_ITEM.id;
      existingNode.set("attributes", extLayerNode.attributes, {commit:true});
      
      // move the layer at the top of map
      var newLayerItem = app.mapContent.getItemById(newLayerId);
      newLayerItem.setVisibility(oldLayerItem.isVisible());
      if ( newLayerItem.index!=oldLayerItem.index )app.mapContent.reaffectItem(newLayerItem.index, oldLayerItem.index, 'Top');
      existingNode.get('OL_ITEM').id = newLayerItem.id;
      existingNode.get('OL_ITEM').index = newLayerItem.index;
      existingNode.get('OL_ITEM').parentId = newLayerItem.parentId;
      existingNode.get('OL_ITEM').parentIndex = newLayerItem.parentIndex;
      
      this.map.toolTip.toolTipLayers = this._initTooltipConfig();
    }
    
    // adding the layerNode in the layerTreePrint
    var newLayerTreeNode = this.context.getLayerTreeNodePrintFromMdataValue("layerId",layerId);
    if ( !newLayerTreeNode ) return;
    var att = newLayerTreeNode.attributes;
    var existingNode = this.layerTreePrintStore.findNode("layerId", att.layerId);
    if (existingNode!=null) {
      existingNode.set(newLayerTreeNode.attributes, {commit:true});
    }
  
    return;
  },
  /**
   * @brief load a new layer in a map from its conetxt description
   * @param jsonContext : owscontext in json
   * @param layerId : id of the new layer
   * 
   */ 
  loadLayerFromContext: function(jsonContext, layerId, treeSelection) {
      if (jsonContext==null)
      return;
    this._parseContext(jsonContext);
    
    // adding the layerNode in the layerTree
    var newLayerTreeNode = this.context.getLayerTreeNodeFromMdataValue("layerId",layerId);
    if ( !newLayerTreeNode ) return;
    var att = newLayerTreeNode.attributes;
    var existingNode = this.layerTreeStore.findNode("layerId", att.layerId);
    if (existingNode!=null) {
      app.map.refresh();
    }
    else {
    	var parentItem = this.mapContent.item;
    	if ( treeSelection.length ){
    	  treeSelection = treeSelection[0];
    	  if ( treeSelection.get('type')!="group" ){
    	  	treeSelection = treeSelection.parentNode;
    	  }
    	  parentItem = this.mapContent.getItemById(treeSelection.get('OL_ITEM').id, false);
    	} else {
    		treeSelection = this.layerTreeStore.getRoot();
      }
      
      // adding the layer in the map
      var OL_ITEM = this._addLayer(newLayerTreeNode, this._jsonLayerList, parentItem);
      var extLayerNode = OL_ITEM.TREE_NODE;
      delete OL_ITEM.TREE_NODE;
      
      treeSelection.insertChild(0, extLayerNode);
      
      // move the layer at the top of map
      var newLayerItem = parentItem.items[parentItem.items.length-1];
      var firstLayerItem = parentItem.items[0];
      app.mapContent.reaffectItem(newLayerItem.index, firstLayerItem.index, 'Top');
    }
    
    // adding the layerNode in the layerTreePrint
    var newLayerTreeNode = this.context.getLayerTreeNodePrintFromMdataValue("layerId",layerId);
    if ( !newLayerTreeNode ) return;
    var att = newLayerTreeNode.attributes;
    var existingNode = this.layerTreePrintStore.findNode("layerId", att.layerId);
    if (existingNode==null) {
      treeSelection = this.layerTreePrintStore.getRoot();
      newLayerTreeNode = this._convertNode(newLayerTreeNode, this._jsonLayerList, false);
      newLayerTreeNode.icon = Descartes.Application.getLegendGraphicURL(OL_ITEM.OL_layers[0]);
      treeSelection.insertChild(0, newLayerTreeNode);
      
    }
  },
  
  /**
   * 
   * @param {type} descartesToolDesc
   * @returns {undefined}Utility function to initialize a Descartes besides
   */
  initTool : function(tool) {
		var toolInstance = null;
		var toolClassName = null;
		if (typeof tool.type === "string") {
			if (Descartes.Map.TOOLS_NAME.indexOf(tool.type) !== -1) {
				toolClassName = this.defaultTools[tool.type];
			}
		} else if (typeof tool.type === "function"){
			toolClassName = tool.type;
		}
		if (toolClassName !== null) {
			// ************************************************** //
			// **********BUG INSTANCIATION TOOL****************** //
			// ************************************************** //
			toolInstance = new toolClassName(OpenLayers.Class.isPrototype);
			var args;
			if (tool.args !== null){
				if ( !(tool.args instanceof Array)) {
					args = [tool.args];
				} else {
					args = tool.args;
				}
			} else {
				args = [];
			}
			toolClassName.prototype.initialize.apply(toolInstance, args);
		}
		if (toolInstance !== null) {
			//Si setMapContent définie alors on l'appelle
			if (toolInstance.setMapContent !== undefined) {
				toolInstance.setMapContent(this.mapContent);
			}
			
		}
		return toolInstance; 
  },
  
  /**
   * @brief parsing context
   * 
   */ 
  _parseContext: function(jsonContext) {
    this.context = new Carmen.JSONContext(jsonContext);
    Descartes.context=this.context;
    this._jsonNode = this.context.mdataMap.LayerTree.LayerTreeNode;
    this._jsonNodePrint = this.context.mdataMap.LayerTreePrint.LayerTreeNode;
    this._jsonLayerList = this.context.layer;
  },
  
  CLASS_NAME : "Descartes.Application"
  
});

Descartes.Application.CLASS_NAME = "Descartes.Application";

Descartes.Application.createGroup = function(isRoot, nodeId, title, attributes){
    attributes = attributes || {};
    var descartesItem;
    var config = Ext.apply({}, Carmen.LayerTree.defaultGroupNode);
          
    if (!isRoot) {
      config = Ext.apply(config, {
        nodeId : nodeId,
        forceOpacityControl: (attributes.forceOpacityControl == "1"),
        checked : (attributes.visible=="1"),
        expanded: !!(attributes.expanded),
        text: decodeURIComponent(title),
        groupName : decodeURIComponent(attributes.groupName || title),
        groupIdentifier : String(decodeURIComponent(attributes.groupIdentifier || attributes.groupName || title)).replace(/\W/g, '_').substring(0, 255),
        leaf : false,
        children : []
      });
    }
    var treeNode = isRoot ?
          Ext.apply({}, Carmen.LayerTree.defaultRootNode) :
          config;
    
    if ( !isRoot ){
      var options = {
        visible: (attributes.visible=="1"),
        checked : (attributes.visible=="1"),
        orphan: false,
        opened: !!(attributes.open),
        configuration : {
          attributes : attributes
        }
      };
      
      descartesItem = new Descartes.Group(title, options);
      
      treeNode.attributes = attributes;
    } else {
      return treeNode;
    }
    
    return {descartesItem : descartesItem, treeNode : treeNode};
};

Descartes.Application.getLegendGraphicURL = function(olLayer) {
  var getMapParams = Carmen.Util.clone(olLayer.params);
  //console.log(this.params);  	
  olLayer.params.REQUEST = 'GetLegendGraphic';
  olLayer.params.SLD_VERSION = '1.1.0';
  olLayer.params.EXCEPTIONS = 'application/vnd.ogc.se_xml';
  olLayer.params.LAYER = olLayer.params.LAYERS +'';
  delete(olLayer.params.LAYERS);
  //this.params.HEIGHT = Carmen.Layer.LEGEND_GRAPHIC_HEIGHT;
  //this.params.WIDTH = Carmen.Layer.LEGEND_GRAPHIC_WIDTH;
  var url = OpenLayers.Layer.HTTPRequest.prototype.getFullRequestString.apply(olLayer, []);
  olLayer.params = getMapParams;
  
  return url ;
};