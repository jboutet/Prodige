/**
 * Translate text classified by prefix (fixed) and key
 * @param string key
 * @return string
 */
var _T_MAPTOOLS = function(key){
  var root = "Map.forms.tools.";
  return (key!==null ? _t( root + key ) : root);
};
/**
 * Configure a field container composed by two columns
 * @param object config
 * @return object
 */
function BICOLUMNCONTAINER(config){
  return Ext.apply(config||{}, {
    layout: {
      type: 'table',
      columns: 2,
      tableAttrs : {
        width : '100%'
      },
      tdAttrs : {
        width : '50%',
        align : 'left'
      }
    },
    defaults : {
      labelWidth : 100,
      labelAlign: 'left',
      labelSeparator: ' '
    }
  })
}
/**
 * Configure a left column
 * @use use with BICOLUMNCONTAINER
 * @param object config
 * @return object
 */
function LEFTCOLUMN(config){
  return Ext.apply(config||{}, {
  })
}
/**
 * Configure a right column 
 * @use use with BICOLUMNCONTAINER
 * @param object config
 * @return object
 */
function RIGHTCOLUMN(config){
  return Ext.apply(config||{}, {
  })
}

Ext.define('Carmen.form.field.AttributeEditor', {
    extend: 'Ext.panel.Panel',
    xtype: 'cmnattributeeditor',
    
    // In this example, configuration is applied at initComponent time
    // because it depends on themeInfo object that is generated for the
    // FormGrid instance.
    initComponent: function() {
      var me = this;
      Ext.apply(this, {
        minHeight : 50,
        
        dockedItems: [{
          xtype: 'toolbar',
          dock: 'top',
          items: [{
            xtype:'textfield',
            name : 'attribute_name',
            emptyText : _T_MAPTOOLS('attributeseditor.emptyTextField')
          }, {
            xtype : 'button',
            icon: '/bundles/carmenweb/images/add.png',
            tooltip : _T_MAPTOOLS('attributeseditor.tooltip'),
            handler : function(){
              var field = me.down('[name="attribute_name"]');
              if (!field) return;
              var attribute_name = field.getValue();
              if ( attribute_name=="" ) return;
              if ( me.grid.store.find('attribute_name', attribute_name, 0, false, false, true)!=-1 ) return;
              me.grid.store.add({attribute_name : attribute_name});
              field.setValue('');
            }
          }]
        }],
        
        items : [me.grid = {
          hideHeaders : true,
          minHeight : 50,
          maxHeight : 100,
          autoScroll : true,
          xtype : 'grid',
          border : true,
          store : new Ext.data.JsonStore({
            fields : ['attribute_name'],
            listeners : {
              datachanged : function(){
                var field = me.up('form').down('[name="attribut_value_annotation"]');
                if ( !field ){alert('not found attribut_value_annotation field');return;}
                field.setValue(this.collect('attribute_name').join('|'));
              }
            }
          }),
          
          columns : [{
            dataIndex : 'attribute_name'
          }, {
          	width : 20,
            xtype : 'actioncolumn',
            items : [{
              xtype : 'button',
              icon: '/bundles/carmenweb/images/delete.png',
              handler : function(grid, rowIndex){
                var record = me.grid.store.getAt(rowIndex);
                if ( !record ) return;
                Ext.Msg.confirm(
                  _T_MAPTOOLS('attributeseditor.delete.title')
                , new Ext.XTemplate(_T_MAPTOOLS('attributeseditor.delete.message')).apply(record.data)
                , function(btn){
                    if (btn=='yes'){
                      me.grid.store.removeAt(rowIndex);
                    }
                  }
                );
              }
            }]
          }],
          
          viewConfig : {
            emptyText : _T_MAPTOOLS('attributeseditor.emptyTextGrid'),
            stripeRows : true,
            listeners : {
              drop : function(node, data, old_record){data.view.refresh(false);}
            },
            plugins: [{
              ptype: 'gridviewdragdrop',
              dragText: _T_MAPTOOLS('attributeseditor.dragText'),
              /**
               * override onViewRender to get access to the dragZone object
               * then override getDragData of dragZone object to disabled drag under input elements
               */
              dragZone: {
                getDragData : function(e) {
                  // do not enable drag under input elements
                  if ( Ext.get(e.getTarget()).is('input') ) return;
                  if ( Ext.get(e.getTarget()).is('.x-btn') ) return;
                  return Ext.view.DragZone.prototype.getDragData.apply(this, arguments);
                }
              }
            }]
          },
          columnLines : true
        }]
    });
    
    this.callParent();
  }

});

/**
 * 
 * @param Carmen.Model.Map map
 * @return {}
 */
function getTools()
{
  var tools = [
   [{
     title : "",
     items : [{xtype:'label', text:'', width : 100}, 'addLayer', 'symbology', 'geobookmark'],
     layout : 'hbox',
     flex : 3
   }],
   [{
     title : "",
     items : [{xtype:'displayfield', value:_T_MAPTOOLS('fieldsets.scale_tools'), width : 100}, 'scaleImg', 'scaleTxt', 'scale', '__empty__'],
     layout : 'hbox',
     flex : 3
   }],
   [{
     title : "navigation_tools",
     items : ['pan', 'zoom', 'fitall', '__empty__'],
     layout : 'hbox',
     flex : 4
   }],
   [{
     title : "print_tools",
     items : ['exportImg', 'exportPdf', '__empty__'],
     layout : 'hbox',
     flex : 4
   }],
   [{
     title : "",
     items : ['print',  'context', '__empty__'],
     layout : 'hbox',
     flex : 4
   }],
   [{
     title : "query_tools",
     items : ['info', 'queryAttributes', '__empty__'],
     layout : 'hbox',
     flex : 3
   }],
   [{
     title : "",
     items : ['buffer', 'measure', '__empty__'],
     layout : 'hbox',
     flex : 4
   }],
   [{
     title : "annotation_tools",
     items : ['annotation', '__empty__', '__empty__', '__empty__'],
     layout : 'hbox',
     flex : 4
   }]
   ];

  var checkbox_tools = ['addLayer', 'symbology', 'geobookmark', 'scaleImg', 'scaleTxt', 'scale', 'annotation'];
  
  var iconCls = {
      pan         : 'fa fa-fw icon-hand',
      zoom        : 'fa fa-fw fa-search-plus',
      fitall      : 'fa fa-fw fa-globe',
      measure     : 'fa fa-fw icon-straighten',
      context     : 'fa fa-fw fa-save',
      addLayer    : 'fa fa-fw fa-plus-square',
      symbology   : 'fa fa-fw fa-bars',
      geobookmark : 'fa fa-fw fa-star',
      exportImg   : 'fa fa-fw fa-file-image-o',
      exportPdf   : 'fa fa-fw fa-file-pdf-o',
      print       : 'fa fa-fw fa-print',
      scaleImg    : '',
      scaleTxt    : '',
      scale       : '',
      info        : 'fa fa-fw fa-info',
      buffer      : 'fa fa-fw icon-tampon',
      queryAttributes     : 'fa fa-fw fa-binoculars',
      annotation  : 'fa fa-fw fa-pencil'
  }
  
  var getFieldConfig = function(field) {
    var fieldConfig = {
      name : field, 
      defaultValue : "",
      serialize : function(value) {
          if(value == "")
            return null;
         return value;
      }
    };

    return fieldConfig;
  };
  var getCheckboxConfig = function(field) {
    var fieldConfig = {
      name : field, 
      type : 'boolean',
      defaultValue : false,
      convert : function(value) {
         if(value == "")
            return false;
         return value;
      },
      serialize : function(value) {
         if(value == "")
            return false;
         return value;
      }
    };

    return fieldConfig;
  };

  var tools_store = Ext.create('Ext.data.Store', {
    fields: ['text', 'value'],
    data : [
        {"text":"Barre d'outils simple", "value":"simple"},
        {"text":"Barre d'outils avancée", "value":"advanced"},
        {"text":"Outil absent", "value":""}
    ]
  });
  var specific_store_print = Ext.create('Ext.data.Store', {
    fields: ['text', 'value'],
    data : [
        {"text":"Modèles prédéfinis", "value":"predefinedModels"},
        {"text":"Modèles configurables", "value":"layoutModels"},
        {"text":"Outil absent", "value":""}
    ]
  });

  var getFormFieldFn = {
      component : function(fieldName, flex){
        if( fieldName == '__empty__' ) {
          return {xtype:'container', flex : flex, padding: '0 10 0 0'};
        }
        if(checkbox_tools.indexOf(fieldName)  != -1) {
          return {
            xtype:'checkbox', 
            id:'maptools_'+fieldName, 
            name:fieldName, 
            labelAlign: 'left', boxLabel:_T_MAPTOOLS('fields.'+fieldName), flex : flex, padding: '0 20 0 0', style:'color:#666', 
            beforeBoxLabelTextTpl: iconCls[fieldName] ? '<i class="'+iconCls[fieldName]+'"></i> ' : ''
          };
        }
        if(fieldName == "exportPdf"){
          return {
            xtype:'combobox', 
            id:'maptools_'+fieldName, 
            name:fieldName, 
            labelAlign: 'left', 
            fieldLabel:_T_MAPTOOLS('fields.'+fieldName), 
            store: specific_store_print, 
            displayField: 'text', valueField: 'value', flex : flex, padding: '0 20 0 0', 
            beforeLabelTextTpl: iconCls[fieldName] ? '<i class="'+iconCls[fieldName]+'"></i> ' : ''
          };
        }
        return {
          xtype:'combobox', id:'maptools_'+fieldName, name:fieldName, labelAlign: 'left', fieldLabel:_T_MAPTOOLS('fields.'+fieldName), store: tools_store, displayField: 'text', valueField: 'value', flex : flex, padding: '0 20 0 0'
          , beforeLabelTextTpl: iconCls[fieldName] ? '<i class="'+iconCls[fieldName]+'"></i> ' : ''
        };
      }
  };
  var getFormField = function(field, flex){
    var fn = getFormFieldFn[field] || getFormFieldFn.component;
    if ( Ext.isFunction(fn) ) {
      return fn(field, flex);
    }
    return null;
  };
  var nb_columns = 12;
  var modelFields = [];
  var formItems = [];
  Ext.each(tools, function(lineTools){
    var panelLine = {xtype:'panel', layout:'hbox', items : []};
    Ext.each(lineTools, function(group, index, lineTools_itself){
      var nbItemPerLine = Math.ceil((group.nb_columns || nb_columns)/group.flex);
      var nbLines = Math.floor(group.items.length/nbItemPerLine);
      var fieldset = {
        xtype:'panel'
      , frame : group.title!=''
      , layout:'anchor'
      , title : group.title!='' ? _T_MAPTOOLS('fieldsets.'+group.title) : ''
      , items:[]
      , flex:(group.nb_columns || nb_columns)/lineTools_itself.length
      , margin : '5 0 5 0'
      , padding : group.title!='' ? 10 : '0 10 10 10'
      };
      panelLine.items.push(fieldset);
      
      for (var i=0; i<group.items.length; i+=nbItemPerLine){
        var panelGroup = {xtype:'panel', anchor: '99%', layout:'hbox', flex:1, items:[]};
        fieldset.items.push(panelGroup);
        Ext.each(group.items.slice(i, i+nbItemPerLine), function(item){
        	if ( Ext.isObject(item) ){
        		panelGroup.items.push( item );
        		return;
        	}
          /*if ( item=="spacefiller" ) {
            panelGroup.items.push( {xtype:'panel', html:'', flex:nbItemPerLine} );
            return;
          }*/
          if ( checkbox_tools.indexOf(item)!=-1 ) modelFields.push     ( getCheckboxConfig(item) );
          else modelFields.push     ( getFieldConfig(item) );
          panelGroup.items.push( getFormField  (item, nbItemPerLine) )
        })
      }
    });
    formItems.push(panelLine);
  });
  return {modelFields : modelFields, formItems:formItems};
}

Carmen.__CMN_MAPTOOLS__ = getTools();

/**
 * @model Carmen.Model.MapTools
 */
Ext.define('Carmen.Model.MapTools', {
  extend: 'Ext.data.Model',
  
  statics : {
    /**
     * Convert an array of selected tools for map in a mapTool object
     * @param  array tools
     * @return Carmen.Model.MapTools
     */
    convertArray : function(tools)
    {
      var object = {};
      
      Ext.each(Carmen.Model.MapTools.prototype.fields, function(field){
        var fieldDef = field;
        if ( Ext.isObject(field) ){
          field = field.name;
        } else {
          fieldDef = {name : field, defaultValue:false};
        }
        object[field] = fieldDef.defaultValue;
      });
      Ext.each(tools, function(mapTool){
        var lexTool;
        //get lexTool
        if ( Ext.isObject(mapTool) ){
          if ( Ext.isDefined(mapTool.tool) ){
            lexTool = mapTool.tool;
          }
          else if (  Ext.isDefined(mapTool.toolIdentifier) ){ 
            lexTool = mapTool;
          }
          else {
            return;
          }
        }
        //read lexTool
        if (Ext.isObject(lexTool) && Ext.isDefined(lexTool.toolIdentifier) ) {
          object[lexTool.toolIdentifier] = mapTool.maptoolValue || true;
        }
      });


      return Ext.create('Carmen.Model.MapTools', object);
    }
  },
  
  idProperty : 'mapId',
  identifier: {
    type: 'sequential',
    prefix: ''
  },
  fields: Carmen.__CMN_MAPTOOLS__.modelFields
});


/**
 * @form Carmen.FormSheet.MapTools
 */
var CMP_NAME = 'Carmen.FormSheet.MapTools';
Ext.define('Carmen.FormSheet.MapTools', {
  extend: 'Carmen.Generic.Form', 
  xtype: CMP_REFERENCE(CMP_NAME),

  title: _T_MAPTOOLS('title'),
  
  height: 'auto',
  items: Carmen.__CMN_MAPTOOLS__.formItems,
  listeners : {
  	afterrender : function(){
  		
      var attribut_value_annotation = this.down('[name=attribut_value_annotation]');
      var cmnattributeeditor = this.down('cmnattributeeditor');
      if ( !(attribut_value_annotation && cmnattributeeditor) ) return;
      if ( !cmnattributeeditor.grid.store ) return; 
      attribut_value_annotation = attribut_value_annotation.getValue().split('|');
      var store = [];
      Ext.each(attribut_value_annotation, function(value){if ( String(value).trim()!='') store.push({attribute_name:value});});
      cmnattributeeditor.grid.store.loadRawData(store, false);
  	}
  }
});