function CMP_REFERENCE(name){return String(name).replace(/\W/g, "_");};

/**
 * Inspired from OpenLayers main loader class
 */ 
(function() {
    
    var singleFile = (typeof Carmen == "object" && Carmen.singleFile);
    
    /**
     * Namespace: Carmen
     */
    Ext.ns("Carmen");
    Ext.apply(Carmen, {
        
        /**
         * Property: _scriptName
         * {String} Relative path of this script.
         */
        _scriptName: "Carmen.js",

        /**
         * Function: _getScriptLocation
         * Return the path to this script.
         *
         * Returns:
         * {String} Path to this script
         */
        _getScriptLocation: function () {
            var scriptLocation = "";
            var scriptName = Carmen._scriptName;
         
            var scripts = document.getElementsByTagName('script');
            for (var i=0, len=scripts.length; i<len; i++) {
                var src = scripts[i].getAttribute('src');
                if (src) {
                    var index = src.lastIndexOf(scriptName); 
                    // set path length for src up to a query string
                    var pathLength = src.lastIndexOf('?');
                    if (pathLength < 0) {
                        pathLength = src.length;
                    }
                    var sep = src.lastIndexOf('/');
                    
                    // is it found, at the end of the URL?
                    if ((index > -1) && (index + scriptName.length == pathLength)) {
                        if ((sep==-1) || ((sep > -1) && (sep + scriptName.length + 1 == pathLength))) {
                          scriptLocation = src.slice(0, pathLength - scriptName.length);
                          break;
                        }
                    }
                }
            }
            return scriptLocation;
         }
    });
  
    /**
     * 
     * When we *are* part of a SFL build we do not dynamically include the 
     * Carmen library code as it will be appended at the end of this file.
     */
 
    if(CARMEN_DEBUG && !singleFile) {
      //alert("Mode Debug (this alert enables firefox2 to fully load scripts...)");
    var utilities = [
      "Carmen/Ext/Map/KeywordGrid.js"
    ];
    var models = [
      "Carmen/Ext/Account/Account.js",
      "Carmen/Ext/Map/MapUIBannerEditor.js",
      "Carmen/Ext/Map/MapUIPrintModelEditor.js",
      "Carmen/Ext/Map/MapUIShare.js",
      "Carmen/Ext/Map/KeymapLibrary.js",
      "Carmen/Ext/Map/MapUI.js",
      "Carmen/Ext/Map/MapTools.js",
      "Carmen/Ext/Map/MapDiffusion.js",
      "Carmen/Ext/Map/Map.js",
      "Carmen/Ext/Map/AddLayers.js",
      "Carmen/Ext/Layer/Layer.js",
      "Carmen/Ext/Layer/LayerGTabData.js",
      "Carmen/Ext/Layer/LayerGTabPostgis.js",
      "Carmen/Ext/Layer/LayerGTabRaster.js",
      "Carmen/Ext/Layer/LayerGTabVector.js",
      "Carmen/Ext/Layer/LayerGTabWFS.js",
      "Carmen/Ext/Layer/LayerGTabWMS.js",
      "Carmen/Ext/Layer/LayerGTabWMSC.js",
      "Carmen/Ext/Layer/LayerGTabWMTS.js",
      "Carmen/Ext/Layer/LayerTabField.js",
      "Carmen/Ext/Layer/LayerTabLabel.js",
      "Carmen/Ext/Layer/LayerTabMeta.js",
      "Carmen/Ext/Layer/LayerTabOGC.js",
      "Carmen/Ext/Layer/LayerTabStyle.js",
      "Carmen/Ext/Layer/LayerPanelColor.js",
      "Carmen/Ext/Layer/LayerPanelPropSym.js",
      "Carmen/Ext/Layer/LayerPanelSector.js",
      "Carmen/Ext/Layer/LayerPanelUniqSym.js",
      "Carmen/Ext/Layer/LayerPanelUniqValue.js",
      "Carmen/Ext/LayerTree/LayerTree.js",
      "Carmen/Ext/LayerTree/LayerTreeUI.js",
      "Carmen/Ext/LayerTree/LayerTreePrintUI.js"
    ];
    var windows = [
      "Carmen/Ext/Map/MapList.js",
      "Carmen/Ext/Map/LayerList.js",
      
      "Carmen/Ext/Account/AccountUI.js",
      "Carmen/Ext/Map/MapEditor.js",
      "Carmen/Ext/Layer/LayerUI.js"
    ];
    var jsfiles = [].concat([
       // "Carmen/Defaults.js",
        "Carmen/Ext/DragDropTag/DragDropTag.js",
        "Carmen/Dictionaries.js",
        "Carmen/Ext/IdentifierVType.js",
        "Carmen/Ext/Util.js",
        "Carmen/Util.js",
        "Carmen/Application.js",
        "consultation2.js",
        "WMSGetFeatureInfo.js",
        "WFSCapabilities.v1_0_0.js",
        //"Carmen/Ext/RowLayout.js",
        //"Carmen/Ext/BrowseButton.js",
        "Carmen/Ext/FileBrowser/Ext.ux.FileBrowserWindow.js",
        //"Carmen/Ext/FileBrowser/Ext.ux.FileUploadPanel.js",
        "Carmen/Ext/FileBrowser/Ext.ux.FileBrowserPanel.js",
        //"Carmen/Ext/FileUploadField.js",
        "Carmen/Ext/ExtCKEditor/ExtCKEditor.js",
        //TODO remove sliderTip ?
        "Carmen/Ext/SliderTip.js",
        //TODO remove compositeField ?
        "Carmen/Ext/CompositeField.js",
        //"Carmen/Ext/ColorChooser.js",
        "Carmen/Ext/colorpick/ext-ux.js",
        "Carmen/Ext/Selector.js",
		//"Carmen/Ext/IFrame.js",
        //"Carmen/Layer.js",
        //"Carmen/Map.js",
        //"Carmen/Handler/Circle.js",
        "Carmen/Control.js",
        /*"Carmen/Control/AdvancedAnnotation.js",
        "Carmen/Control/AreaExtractor.js",
        "Carmen/Control/AttributionCarmen.js",
        "Carmen/Control/AddOGC.js",
        "Carmen/Control/AddWMS.js",
        "Carmen/Control/AddWFS.js",
        "Carmen/Control/Buffer.js",
        "Carmen/Control/ContextManager.js",
        "Carmen/Control/Coordinate.js",*/
        "Carmen/Control/FavoriteAreas.js",/*
        "Carmen/Control/Info.js",
        "Carmen/Control/InfoCircle.js",
        "Carmen/Control/InfoPolygon.js",
        "Carmen/Control/InfoPolyline.js",
        "Carmen/Control/InfoURL.js",
        "Carmen/Control/Inlay.js",        */
        "Carmen/Control/LayerTreeManager.js",/*
        "Carmen/Control/Legend.js",
        "Carmen/Control/LocateByAddress.js",*/         
        "Carmen/Control/Measure.js",
        /*"Carmen/Control/PanArrows.js",
        "Carmen/Control/QueryAttributes.js",
        "Carmen/Control/QuerySpatial.js",*/
        "Carmen/Control/AdvancedQueryAttributes.js",
        /*"Carmen/Control/MapserverScale.js",        
        "Carmen/Control/ExportImg.js",
        "Carmen/Control/ExportPdf.js",
        "Carmen/Control/Print.js",
        //"Carmen/Control/ReferenceMap.js",
        "Carmen/Control/Refresh.js",
        "Carmen/Control/ScaleChooser.js",
        "Carmen/Control/ToolTip.js",*/
        "Carmen/Control/ZoomHistory.js"/*        
        "Carmen/Control/ZoomToPlace.js",
        "Carmen/Control/ZoomToPlaceFromDB.js",
        "Carmen/Layer/MapServerGroup.js",
        "Carmen/Layer/WMSGroup.js",
        "Carmen/Layer/WMTSGroup.js",
        "Carmen/Layer/Vector.js"*/
        
    ], [
        "Carmen/Ext/GenericWindow.js",
        "Carmen/Ext/GenericForm.js",
        "Carmen/Ext/ApplicationUI.js",
        "Carmen/Control/AddLocalData.js"
        
    ], utilities, models, windows);
    // proj4js defs
    //for (var i=0; i< tabEPSG.length; i++)
    //  jsfiles.push("Carmen/proj4Defs/EPSG"+tabEPSG[i][0]+".js");
    jsfiles.push("Carmen/proj4Defs/proj4js_def.js");
    var docWrite = true;
    if(docWrite) {
      var allScriptTags = new Array(jsfiles.length);
    }
    var host = Carmen._getScriptLocation();    
    for (var i=0, len=jsfiles.length; i<len; i++) {
      if (docWrite) {
        allScriptTags[i] = "<script src='" + host + jsfiles[i] + "'></script>"; 
      } else {
          var s = document.createElement("script");
          s.src = host + jsfiles[i];
          var h = document.getElementsByTagName("head").length ? 
                           document.getElementsByTagName("head")[0] : 
                           document.body;
          h.appendChild(s);
      }
    }
    if (docWrite) {
      document.write(allScriptTags.join(""));
    }  
  }
  
  /**
   * N'autorise pas la bufferisation des grids
   */
  Ext.grid.Panel.prototype.bufferedRenderer = false;
  /**
   * Uniformité de présentation
   */
  Ext.form.Field.prototype.labelSeparator = "";
  Ext.form.FieldContainer.prototype.labelSeparator = "";
  Ext.form.FieldSet.prototype.padding = 5;
  /**
   * N'autorise pas l'édition dans les combobox
   */
  Ext.form.field.ComboBox.prototype.editable = false;
  Ext.form.field.ComboBox.prototype.selectOnFocus = false;
  Ext.form.field.ComboBox.prototype.initComponent = (function(){
  	var fn = Ext.form.field.ComboBox.prototype.initComponent;
  	return function(){
    	var me = this;
    	if ( me.selectOnFocus && !me.editable ){
    		me.selectOnFocus = false;
    	}
      fn.apply(this);
  	}
  })();
  /**
   * N'autorise pas la gestion du focus (FocusManager) sur les boutons
   */
  Ext.button.Button.prototype.focusable = false;
  
  Ext.override(Ext.form.field.ComboBox, {
    afterRender: function() {
    	var me = this;
    	this.callParent(arguments);
    	if ( this.withCursor ) return;
    	this.inputEl.addListener('focus', function(){
    	  if ( !me.editable) {
    	    me.inputEl.blur();
    	    me.inputWrap.focus();
    	  }
    	});
    }
  })

  /**
   * Désactivations des actions de fenetre le temps de l'exécution des Ajax
   */
  var iCountAjax = 0;
  var loadingToast;
  Ext.Ajax.setTimeout(100000);
  /*Ext.window.Window.prototype.close = Ext.Function.createSequence(Ext.window.Window.prototype.close, function(){Ext.Ajax.abortAll();});
  Ext.Ajax.autoAbort = true;*/
  Ext.data.proxy.Ajax.prototype.timeout = 100000;
  Ext.Ajax.on('beforerequest', function(){
    iCountAjax++;
    var renderTo = null;//Ext.getCmp('mainPanel');
    if ( Ext.isDomReady ){
      if ( !loadingToast ) loadingToast =  Carmen.Notification.msg(null, "Traitement en cours...", false);
    }
    Ext.each(Ext.ComponentQuery.query('cmngenericwindow > toolbar[dock="bottom"]'), function(cmp){cmp.setDisabled(true)});
  });

  Ext.Ajax.on('requestcomplete', function(){
    iCountAjax--;
    if ( iCountAjax ) return;
    if ( loadingToast ){
      loadingToast.hide();
      loadingToast = null;
    }
    Ext.each(Ext.ComponentQuery.query('cmngenericwindow > toolbar[dock="bottom"]'), function(cmp){cmp.setDisabled(false)});
  });
  
  Carmen.notAuthenticatedException =  function(response){
    if ( response.status == 401 ){
      Ext.Msg.alert(_t('app.endofconnection.title'), _t('app.endofconnection.message'), function(){top.document.location.reload()}).setIcon(Ext.Msg.WARNING);
      return true;
    }
    return false;
  }
  Carmen.readFormFailure =  function(response){
    if ( Carmen.notAuthenticatedException(response) ) return;
    var title = this.getConfig().title ? this.getConfig().title : _t("form.msgFailureTitle");
    Ext.Msg.alert(title, _t("form.msgFailure"))
  }
  Ext.Ajax.on('requestexception', function(evt, response){
    if ( Carmen.notAuthenticatedException(response) ) return;
    
    iCountAjax--;
    
    if ( loadingToast ){
      loadingToast.hide();
      loadingToast = null;
    }
    
    if ( Ext.isDomReady ){
      if ( !Ext.get('bannerPanel_cpt_loadingfail') ) 
        Carmen.Notification.msg(null, "<b>Echec du traitement....</b>", 5, {id:'bannerPanel_cpt_loadingfail'});
    }
    Ext.each(Ext.ComponentQuery.query('cmngenericwindow > toolbar[dock="bottom"]'), function(cmp){cmp.setDisabled(false)});
  });
  
})();


/**
 * Constant: VERSION_NUMBER
 */
Carmen.VERSION_NUMBER="0.1beta";

