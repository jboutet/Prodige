<?php

namespace Carmen\WebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use JMS\SecurityExtraBundle\Annotation\Secure;
use JMS\Serializer\SerializationContext;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author alkante
 */
class DefaultController extends Controller
{
    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/", name="carmen_ws_index", options={"expose"=true})
     */
    public function indexAction(Request $request)
    {
        $session = $request->getSession();

        if($request->get('from',false) !==  'prodige') {
            $session->clear();
        }

        $uuid = $session->get('uuid', null);
        
        if( $uuid ) {
            $serializer = $this->get('jms_serializer');
            $context = new SerializationContext();
            $context->setSerializeNull(true);
            $user = $serializer->serialize($this->getUser(), 'json', $context);

            $config = $this->get('carmen.config')->getWebConfig();
            $defaults = $this->container->getParameter('jsdefaults', array());
        
            $connection = $this->getDoctrine()->getConnection();
            if ( $connection ){
                try {
                  $settings = $connection->fetchAll("select prodige_settings_constant, prodige_settings_value from parametrage.prodige_settings");
                  foreach($settings as $row){
                      $config[$row["prodige_settings_constant"]] = $row["prodige_settings_value"];
                  }
                } catch (\Exception $exception){}
            }
            return $this->render('CarmenWebBundle:Default:index.html.twig', array('user'=>$user, 
                                                                                  'parameters'=>$config, 
                                                                                  'jsdefaults'=>$defaults,
                                                                                  'isAdmin' => $request->get('isAdmin',0)));
        }
        else {
            // on est normalement ejecté avant par les vérif niveau CasProvider
            return $this->createAccessDeniedException("Paramètre manquant : uuid");
        }
    }
    /**
     * Returns the default DBAL PDO Connection.
     * 
     * @return \Doctrine\DBAL\Driver\PDOConnection
     */
    protected function getConnection()
    {
        return $this->getDoctrine()->getConnection();
    }
    
    
}
