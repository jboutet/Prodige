/** 
 * custom Vtype for vtype:'layerName'
 * recommended that the name not contain spaces, special characters, or begin with a number
 */ 
if ( typeof window._t == "undefined" ){
	function _t(key){
		switch (key){
      case 'vtype.fileidentifier' : 
      case 'vtype.identifier' : return "Valeur attendue incorrecte : il ne doit pas y avoir d'espace, de caractères spéciaux et ne doit pas commencer par un chiffre.";
      default : return key;
		}
	}
}
var fileIdentifierTest = /^(\w{1})([0-9a-z_\/ -]*)$/i;
Ext.apply(Ext.form.field.VTypes, {
    //  vtype validation function
    fileidentifier: function(val, field) {
        return fileIdentifierTest.test(val);
    },
    // vtype Text property: The error text to display when the validation function returns false
    fileidentifierText: _t('vtype.fileidentifier'),
    // vtype Mask property: The keystroke filter mask
    fileidentifierMask: /[0-9a-z_\/ -]/i
});

/** 
 * custom Vtype for vtype:'layerName'
 * recommended that the name not contain spaces, special characters, or begin with a number
 */ 
var identifierTest = /^([a-z]{1})([0-9a-z_\/]*)$/i;
Ext.apply(Ext.form.field.VTypes, {
    //  vtype validation function
    identifier: function(val, field) {
        return identifierTest.test(val);
    },
    // vtype Text property: The error text to display when the validation function returns false
    identifierText: _t('vtype.identifier'),
    // vtype Mask property: The keystroke filter mask
    identifierMask: /[0-9a-z_\/]/i
});

/**
 * Calculation identifier value from the name value as:
 * - removal of spaces
 * - removal of special characters
 * - should not start with a digit
 * - 20 characters max
 * @param string title  control name of source
 * @return string
 */
function convertNameToIdentifier(name, maxLength) {
  var string = String(name).trim();
  var decode = {
    "a" : "àäâÀÄÂ",
    "e" : "éèëêÉÈËÊ",
    "i" : "ïîÏÎ",
    "o" : "öôÖÔ",
    "u" : "ùüûÙÜÛ",
    "y" : "ŷÿŶŸ",
    "c" : "çÇ",
    "oe" : "œŒ",
    "1" : "¹",
    "2" : "²",
    "_" : "\\s-"
  };
  Ext.iterate(decode, function(replacement, search){
    string = string.replace(new RegExp('['+search+']', 'g'), replacement);
  })
  string = string.replace(/[^a-zA-Z0-9\-\_]/g, '');
  return (maxLength ? string.substr(0, maxLength-1) : string);
}

var qtipWarningOnIdentifierLength;
function warningOnIdentifierLength(textfield) {
  if ( textfield.rendered && !textfield.readOnly && String(textfield.getValue()).length>20 ){
     textfield.getActionEl().dom.setAttribute('data-errorqtip', _t('app.warningOnIdentifierLength.'+(textfield.name || 'default'))); 
     Ext.form.Labelable.initTip();
     Ext.form.Labelable.tip.setTarget(textfield.bodyEl);
     Ext.form.Labelable.tip.anchorToTarget = true;
     Ext.form.Labelable.tip.trackMouse = false;
     Ext.form.Labelable.tip.show();
     Ext.form.Labelable.tip.setWidth('auto');
     Ext.Function.createDelayed(function(){textfield.getActionEl().dom.setAttribute('data-errorqtip', '');Ext.form.Labelable.destroyTip();}, 2000); 
  }
}
function registerWarningOnIdentifierLength(textfield) {
  textfield.listeners = Ext.merge({change : warningOnIdentifierLength, blur : warningOnIdentifierLength}, textfield.listeners || {});
  return textfield;
}