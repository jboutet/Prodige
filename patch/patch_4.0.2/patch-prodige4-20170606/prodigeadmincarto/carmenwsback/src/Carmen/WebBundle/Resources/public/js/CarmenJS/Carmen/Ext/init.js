/**
 * Open the edit map (if exists) :
 * - Ajax Request to create a duplicate map specific to this edition.
 * - Apply to the Carmen Application the OWS Context resulting to this duplication
 * - Set the current Map edited by the user
 */
function openEditMap(uuid) {
	Ext.tip.QuickTipManager.init();
	initApp(false);
	Ext.get('loading').first().first().setHtml('Lecture de la carte');
	
  Ext.Ajax.request({
    url : Routing.generate('carmen_ws_edit_map_uuid', {
      uuid : uuid
    }),
    timeout : 10000000,
    success : function(response) {
      var jsonResponse = Ext.decode(response.responseText);
      Carmen.Model.Map.onSuccessSave(jsonResponse);
      window.clearLoading();
    },
    failure: function(response) {
      if ( Carmen.notAuthenticatedException(response) ) return;
      Ext.Msg.alert('Erreur', 'Carte inexistante ou invalide !');
      window.clearLoading();
    }
  });
}
