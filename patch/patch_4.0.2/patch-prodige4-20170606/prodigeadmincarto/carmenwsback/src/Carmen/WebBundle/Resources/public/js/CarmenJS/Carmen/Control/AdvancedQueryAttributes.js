/**
 * @_requires OpenLayers/Controljs
 */

/**
 * Class: Carmen.Control.AdvancedQueryAttributes
 * 
 * Inherits from: - <OpenLayers.Control>
*/
Carmen.Control.AdvancedQueryAttributes = new OpenLayers.Class(OpenLayers.Control, {

  operatorMap :  [[ '(\\\[field\\d+\\\])\\s*COMMENCE\\s+PAR\\s*(\\\[value\\d+\\\])' , '$1 ilike \'$2%\'' , 'COMMENCE PAR' ,'commence par ' , '$1 ~ "^$2"'],
                  [ '(\\\[field\\d+\\\])\\s*NE\\s+CONTIENT\\s+PAS\\s*(\\\[value\\d+\\\])' , 'not ($1 ilike \'%$2%\')' , 'NE CONTIENT PAS' ,'ne contient pas ' , 'NON\($1 ~ "$2"\)'],
                  [ '(\\\[field\\d+\\\])\\s*CONTIENT\\s*(\\\[value\\d+\\\])' , '$1 ilike \'%$2%\'' , 'CONTIENT' ,'contient ' , '$1 ~ "$2"'],
                  [ '(\\\[field\\d+\\\])\\s*\\=' , '$1 =' , '=' ,'est égal (=)' , '$1 ='],
                  [ '(\\\[field\\d+\\\])\\s*<>' , '$1 <>' , '<>' ,'est différent (<>)' , '$1 !='],
                  [ '(\\\[field\\d+\\\])\\s*>\\=' , '$1 >=' , '>=' ,'supérieur ou égal (>=)' , '$1 >='],
                  [ '(\\\[field\\d+\\\])\\s*>' , '$1 >' , '>' ,'strictement supérieur (>)' , '$1 >'],
                  [ '(\\\[field\\d+\\\])\\s*<\\=' , '$1 <=' , '<=' ,'inférieur ou égal (<=)' , '$1 <=' ],
                  [ '(\\\[field\\d+\\\])\\s*<' , '$1 <' , '<' ,'strictement inférieur (<)' , '$1 <']],
		              
  logicalOperatorMap  : [[ 'AND', 'ET', 'AND', '&&' ],
                         [ 'OR', 'OU', 'OR', '||' ],
                         [ 'NOT', 'NON', 'NOT', 'NOT' ],
                         [ '(', '(', '(', '(' ],
                         [ ')', ')', ')', ')' ]],   

  // UI components
  btn: Ext.create('Ext.Button',{
      tooltip: 'Requêtes attributaires',      
      tooltipType: 'title',
      cls: 'x-btn-icon', /* cmn-tool-requete',*/
      enableToggle: true,
      toggleGroup: 'mainMapControl',
      disabled: false,
      text : '<i class="fa fa-binoculars fa-2x"></i>',
      width: ctrlWidth,
      height: ctrlHeight
  }),



  win : Ext.create('Ext.Window', {
    layout:'anchor',
    //width: 682,
    //height: 300,
    constrain : true,
    bodyPadding: 10,
    //bodyStyle : 'max-height:500px',
    //plain: true,
    title: 'Requêtes attributaires',
    autoDestroy :true,
    resizable:true,
    closeAction: 'hide',
    collapsible : true
    //shadow : false
  }),
  
  winInfo : Ext.create('Ext.Window', {
	    layout:'anchor',
	    title: 'Information',
	    width:400,
	    height: 400,
      constrain : true,
	    plain: true,
	    title: '',
	    modal:false,
	    autoDestroy :true,
	    resizable:true,
	    closeAction: 'hide',
	    shadow : false,
      layout: 'accordion'
  }),
  
  comboLayer : null,
  comboField : null,
  comboOperator : null,
  comboLogical : null,
  textfieldValue : null,
  valueBtn : null,
  requestArea : null,
  requestPanel : null,
  requestTimeout : 360000,
  fullExtent : null,
   
  resizeComboToFitContent : function(combo){
    if (combo.el){
      if (!combo.elMetrics)
      {
        combo.elMetrics = Ext.util.TextMetrics.create(combo.el);
      }
      var m = combo.elMetrics, width = 0, el = combo.el, s = combo.getSize();
      combo.store.each(function (r) {
        var text = r.get(combo.displayField);
        width = Math.max(width, m.getWidth(text));
      }, combo);
      if (el) {
        width += el.getBorderWidth('lr');
        width += el.getPadding('lr');
      }
      if (combo.trigger) {
        width += combo.trigger.getWidth();
      }
      s.width = width;
      combo.setSize(s);
      /*combo.store.on({
          'datachange': this.resizeComboToFitContent,
          'add': this.resizeComboToFitContent,
          'remove': this.resizeComboToFitContent,
          'load': this.resizeComboToFitContent,
          'update': this.resizeComboToFitContent,
          buffer: 10,
          scope: combo
        });*/
    }
  },
  
  
  resizePanelToFitContent : function(panel){
    
  },

  gridInfoFormPanel : Ext.create('Ext.form.Panel', {
      id: 'CmnControlAdvancedQueryAttributes' +'_' + 'gridInfoFormPanel',
      hidden : true,
      fileUpload : true,
      waitTitle : "Traitement en cours..."
  }),

  
  /* Properties */ 

  // data of the layer combo box
  layerData : [], 
  // array of data of the field combo box
  fieldData : [], 
  
  currentFieldData : [],
  // array of layer field desc objets (one sub aray per layer)
  layerFieldsDesc : [], 
  // reference to the layerTreeManager of the app
  layerTreeManager : null,
  // reference to the layerTree of queryable layer
  layerTree : null,
  // mapfile of the app
  mapfile : null,
  // url of the queryAttributes service 
  serviceUrl : null,
  
  //variables de mémorisation
  currentField : "",
  opFieldValue : "",
  currentFieldValue : "",
   

  // methods
  initialize: function(serviceUrl, mapfile, options) {
    OpenLayers.Control.prototype.initialize.apply(this, [options]);    
    
    this.mapfile = mapfile;
    this.serviceurl = serviceUrl;

    this.initializeUIComponents();
    
    this.model = {};
    hasGFI = true;
    var rendererFn = Descartes.ViewManager.getView(Descartes.ViewManager.INFORMATION_IN_PLACE);
    this.renderer = new rendererFn(this.div, this.model);
    this.renderer.events.register('highlightSelectedFeatures', this, this.highlightSelectedFeatures);
    this.renderer.events.register('closed', this, function() {
      if (Ext.getCmp('window_ProdigeBufferInPlaceExtJS0') && (Descartes.Action.ProdigeBuffer.SELECTION_ACTIVATED == 1)){
          Ext.getCmp('window_ProdigeBufferInPlaceExtJS0').setVisible(false);
      }
      this.OL_map.removeLayer(this.vectorLayer);
      this.vectorLayer.destroyFeatures();
      //Desactivate buffer button
      Descartes.Action.ProdigeBuffer.SELECTION_ACTIVATED = 0;
    });
    this.vectorLayer = new OpenLayers.Layer.Vector("AdvancedQueryAttributes_vectorLayer");
  },




  initializeUIComponents : function() {
    // create the Grid
    /*this.gridInfoPanel = Ext.create('Ext.grid.Panel', {
      id : 'gridInfoPanel',
      store: new Ext.data.ArrayStore({
        fields: [],
        data: []
      }),
      columns: [],
      title:'Résultats sélectionnés',
      hidden: true,
      autoScroll: true,
      fitToFrame: true,
      border: false,
      iconCls : 'cmnInfoGridZoomIcon',
      tools:[{
        id:'xls',
        qtip: 'Export de la sélection au format Excel',
        scope : this
      },
      {
        id:'csv',
        qtip: 'Export de la sélection au format CSV'
      }]
    });*/      
   this.comboLayer = Ext.create('Ext.form.ComboBox',{   
        xtype: 'combo',
        hideLabels:false,
        name: 'layer',
        id: 'comboLayerAdvanced',
        fieldLabel: 'Couche',
        queryMode: 'local',
        editable: false,
        listConfig : {
          loadingText: 'Chargement',
          getInnerTpl: function() {return '<tpl for="."><div ext:qtip="{layerTitle}" class="x-combo-list-item">{layerTitle}</div></tpl>';},
          minWidth : 150
        },
        emptyText: 'Sélectionnez une couche',
        triggerAction: 'all',
        minChars: 1,
        queryDelay: 250,     
        store: new Ext.data.SimpleStore({
          fields: ['layerName', 'layerTitle'],
          data : []}
        ),
        validator: function(v) {
           return (v!='Sélectionnez une couche');
          },   
        displayField: 'layerTitle',
        valueField: 'layerName',
        width: 400          
      });
    
    this.comboField = Ext.create('Ext.form.ComboBox', { 
     	  xtype: 'combo',
        name: 'qitem',
        id: 'comboFieldAdvanced',
        fieldLabel: 'Champ',
        queryMode: 'local',
        editable: false,
        listConfig: {
          loadingText: 'Chargement',
          getInnerTpl: function() { return '<tpl for="."><div ext:qtip="{label}" class="x-combo-list-item">{label}</div></tpl>';}
        },
        emptyText: 'Sélectionnez',
        triggerAction: 'all',

        queryDelay: 250,     
        disabled : true,
        store: new Ext.data.SimpleStore({
          fields: ['value', 'labelRequest', 'label'],
          data : []}
        ),/*   
        validator: function(v) {
        	 return (v!='Sélectionnez un champ');
          },*/
        displayField: 'label',
        valueField: 'value',
        width: 400,
        style: {
          width: '400px'
        }
      });
      
    this.comboOperator = Ext.create('Ext.form.ComboBox', {
        xtype: 'combo',
        id: 'comboOperatorAdvanced',
        name: 'qstring',
        fieldLabel: 'Opérateur',
        editable: false,
        disabled : true,
        listConfig : {
          loadingText: 'Chargement',
          repeatTriggerClick : true
        },
        emptyText: 'Sélectionnez',
        triggerAction: 'all',
        queryDelay: 250,
        queryMode: 'local',
        store: new Ext.data.ArrayStore({
  	        fields: ['pattern','replacement', 'labelRequest', 'label'],
  	        data : this.operatorMap}
  	    ),
        displayField: 'label',
        valueField: 'labelRequest',
        width: 400
      });
      
    this.textfieldValue = Ext.create('Ext.form.TextField',{
      	xtype: 'textfield',
      	name: 'value',
        id: 'textfieldValueAdvanced',    		
      	fieldLabel: 'Valeur',
        disabled : true,
      	mode: 'local',
      	width: 270,
      	margin: '0 5 0 0'
      });
    
    this.comboPossibleValue = Ext.create('Ext.form.ComboBox',{   
        xtype: 'combo',
        hideLabels:true,
        name: 'comboPossibleValue',
        id: 'comboPossibleValue',
        fieldLabel: 'Valeur',
        queryMode: 'local',
        hidden: true,
        editable: false,
        listConfig : {
          loadingText: 'Chargement',
          getInnerTpl: function() {return '<tpl for="."><div ext:qtip="{valeurTitle}" class="x-combo-list-item">{valeurTitle}</div></tpl>';},
          minWidth : 150
        },
        emptyText: 'Sélectionnez une valeur',
        triggerAction: 'all',
        
        queryDelay: 240,     
        store: new Ext.data.ArrayStore({
          fields: ['valeurName', 'valeurTitle'],
          data : []}
        ),
                    
        displayField: 'valeurTitle',
        valueField: 'valeurName',
        width: 240          

      });
      
    this.valueBtn = Ext.create('Ext.Button', {
      tooltip: 'Ajouter la valeur dans la requête',
      tooltipType: 'title',
      //cls: 'x-btn-icon cmn-tool-addValue',
      //defaultRenderer: Carmen.Control.AddOGC.buildActionRender('fa fa-fw fa-file-o fa-1x', 'color: #000; font-size: 16px;'),
      xtype: 'button',
      cls: 'x-btn-icon',
      text : '<i class="fa fa-level-down"></i>',
      //scale: 'medium',
      listeners : {
        'click' : {
           fn : function(b,evt) {
             this.addValueToRequest(this.textfieldValue.getValue());
             //mémorisation 
             this.currentFieldValue = this.textfieldValue.getValue();
            },
           scope : this
        }
      }//,
      //width: 16,
      //height: 16
  	}); 
    
    
    this.listvalueBtn = Ext.create('Ext.Button', {
        tooltip: 'Lister les valeurs possibles',
        tooltipType: 'title',
        //cls: 'x-btn-icon cmn-tool-addValue',
        //defaultRenderer: Carmen.Control.AddOGC.buildActionRender('fa fa-fw fa-file-o fa-1x', 'color: #000; font-size: 16px;'),
        xtype: 'button',
        cls: 'x-btn-icon',
        text : '<i class="fa fa-search"></i>',
        //scale: 'medium',
        listeners : {
          'click' : {
             fn : function(b,evt) {
               this.doValueList(this.textfieldValue.getValue());

              },
             scope : this
          }
        }//,
    	});    
    

    
    this.comboLogical = Ext.create('Ext.form.ComboBox',{
        xtype: 'combo',
        id: 'logicalOperatorAdvanced',
        name: 'logicalOperator',
        fieldLabel: 'Opérateur logique',
        editable: false,
        disabled : true,
        listConfig: {
          loadingText: 'Chargement'
        },
        emptyText: 'Sélectionnez',
        triggerAction: 'all',
        minChars: 1,
        queryDelay: 250,     
        queryMode: 'local',
        store: new Ext.data.ArrayStore({
  	        fields: ['value', 'labelRequest' , 'label'],
  	        data : this.logicalOperatorMap          
  	        }
  	    ),
        displayField: 'label',
        valueField: 'labelRequest',
        width: 400
        
      });
    
    this.requestArea = Ext.create('Ext.form.TextArea',{
        xtype: 'textarea',
        id: 'whereClause',
        name: 'whereClause',
        fieldLabel: '<i>Les valeurs doivent être mises entre guillemets. Pensez à fermer les parenthèses ouvertes.</i>',
        disabled : true,
        labelAlign : 'top',
        editable: true,
        emptyText: 'Saisissez votre requête ou construisez la au moyen de l\'interface...',
        width: 400
      });

    this.valueLineLimit = Ext.create('Ext.form.field.Number',{
      	xtype: 'numberfield',
      	name: 'value',
        id: 'fieldLineLimit', 
      	fieldLabel: 'Limiter le nombre de lignes à :',
      	labelAlign : 'top',
        disabled : true,
      	mode: 'local',
      	width: 270,
      	margin: '0 5 0 0'
      });
    
    this.lineLimitCB = Ext.create('Ext.form.Checkbox', {
        tooltip: 'Activer le nombre limite de résultats',
        tooltipType: 'title',
        xtype: 'checkbox',
        id : 'CBLimit',
        disabled : true,
        listeners : {
        	change : {
             fn : function(b,evt) { 
            	 this.activateNbLimit();
              },
             scope : this
          }
        }
    	}); 
    
    this.valueSelectContainer = Ext.create('Ext.form.FieldContainer', {
      fieldLabel: 'Valeur',
      disabled :true,
      id : 'valueSelectContainer',
      combineErrors: true,
      msgTarget : 'side',
      layout: 'hbox',
      defaults: {
          flex: 1,
          hideLabel: true
      },
      items: [ this.textfieldValue, this.comboPossibleValue, this.valueBtn, this.listvalueBtn ]
    })

    this.requestPanel = Ext.create('Ext.form.Panel',{
      id: 'requestPanel',
      autoScroll: true,
      fitToFrame: true,
      defaults: {
        hideLabels:false
      },
      items: [{
        items : [this.comboLayer]
      } , {
        xtype: 'fieldset',
        id: 'fieldsetRequest',
        title: 'Paramètres de la requête',
        items: [{
          items : [this.comboField]
        },{
          items : [this.comboOperator]
        }, this.valueSelectContainer ,{
            items : [this.comboLogical]
        }]
      } ,{
        xtype: 'fieldset',
        id: 'fieldsetSqlArea',
        title: 'Expression',
        items : [this.requestArea,
        {
          xtype: 'fieldcontainer',
          fieldLabel: 'Limiter le nombre de lignes à',
          labelAlign : 'top',
          //disabled :false,
          id : 'limitContainer',
          combineErrors: true,
          msgTarget : 'side',
          layout: {type : 'hbox', align : 'stretchmax'},
          defaults: {
              flex: 1,
              hideLabel: true
          },
          items: [this.lineLimitCB, this.valueLineLimit]
        }]
      }],
      
      buttons: [{
              id: 'requestSubmit',
              text: 'Interroger'
          }, 
          {
              id: 'reinitializeSubmit',
              text: 'Réinitialiser'
          }]
    });
  },


    
  getButton: function() {
    return this.btn;
  }, 
  

  setMap: function(map) {
    OpenLayers.Control.prototype.setMap.apply(this, arguments);
    
    this.OL_map = map;
        
    this.btn.addListener('toggle', Carmen.Util.buildExt2olHandlerToggle(this));
    
    //this.map.app.ui.addToToolPanel(this.btn, this.toolbarTarget, this.toolbarPos);
    
    
    // adding listeners to link window closing and control deactivation   
    var control = this;
    /*this.win.on(
      'hide',
      function(w) { control.btn.toggle(false); },
      this.win);*/
    
    this.comboLogical.on({
      'select' : {
        fn : function (combo){
          this.addToRequest(combo);
          },
        scope : this
      }
    });
    
    this.comboOperator.on({
      'select' : {
        fn : function(combo){
          this.addToRequest(combo);
          //enable next fields
          this.textfieldValue.enable();
          this.valueSelectContainer.enable();
          //mémorisation du champ sélectionné
          this.opFieldValue = combo.value;
          //effacement
        },
        scope : this
      }
    });
    
    this.comboField.on({
      'change' : {
        fn : function(combo, value){
        	if ( value ){
            this.addToRequest(combo);
            //this.resizeComboToFitContent(comboField);
            //enable next fields
            this.comboOperator.enable();
            //mémorisation du champ sélectionné
            this.currentField = combo.value;
            //effacement
        	} else {
            this.comboOperator.disable();
        	}
          this.listvalueBtn.setText('<i class="fa fa-search"></i>');
        },
        scope : this
      }
    });

    
    this.comboPossibleValue.on({
      'select' : {
        fn : function(combo){
          //this.addToRequest(comboPossibleValue);
          this.updateToField(combo);
        },
        scope : this
      }
    });
    this.map.app.ui.doLayout();    
  },

  
  activate: function() {/*
  	if (this.getLayerTreeManager==null) {
      Ext.MessageBox.alert("Warning", "The control " + 
       this. CLASS_NAME + 
       " requires the LayerTreeManager control to work.");
      return null;
    }*/
    OpenLayers.Control.prototype.activate.apply(this, arguments);
    this.showWindow();    
  },
  
  deactivate: function() {
    // removing highlighting...
   // this.map.clearSelection();
    OpenLayers.Control.prototype.deactivate.apply(this, arguments);
    if (this.win.rendered)
      this.win.hide();
  },


  addToRequest : function(c, r, i) {
    var requestAreaStr = this.requestArea.getValue();
    requestAreaStr = requestAreaStr.length>0 ? 
       requestAreaStr + ' ' + c.getValue() : 
       requestAreaStr + c.getValue();
    this.requestArea.setValue(requestAreaStr); 
    
  },
   
  addValueToRequest : function(str) {
    var requestAreaStr = this.requestArea.getValue();
    requestAreaStr = requestAreaStr.length>0 ? 
        (requestAreaStr + ' ') : requestAreaStr;
    requestAreaStr = requestAreaStr + '"' + str + '"';      
    this.requestArea.setValue(requestAreaStr);
    
    this.comboField.reset();
    this.comboOperator.reset();
    this.comboLogical.reset();
    this.textfieldValue.reset();
    this.comboPossibleValue.reset();
    
    this.textfieldValue.show();
    this.comboPossibleValue.hide();
    this.textfieldValue.disable();
    this.comboPossibleValue.hide();
  },
  
  doValueList : function(str) {
	  var control = this;   
  	var queryParams;
  	var idField = -1;
  	
  	// requ ajax pour obtenir la liste des valeurs possibles et maj la liste des val
    if(this.currentField){
    	var query = 
        this.comboField.getValue()+' '+
        (this.comboOperator.getValue() || 'CONTIENT')+' '+
        '"'+str+'"';
        
      queryParams = control.buildParams(control.layerFieldsDesc, this.rewriteOgrRequest(query), this.rewriteOgrRequest(query), false)
    } else
      queryParams = control.buildParams(control.layerFieldsDesc, '1=1', '1=1', true)
       
  	var valueField = this.currentField;
  	// req ajax pour mettre a jour la liste des valeurs possibles
  	Ext.Ajax.request({
        url: Routing.generate('carmen_descartes_advancedqueryattributes'),
        scope : this,
        //timeout: this.requestTimeout,
        success: function(response) { 
        	response = Ext.decode(response.responseText); 
        	
        	if (response.totalCount > 0) {
          	  for(lname in response.results)  {       	    		  
          		  for(fieldsKey in response.results[lname].fields){
		  			      if(response.results[lname].fields[fieldsKey]==valueField){ 	    				  
          				  idField = fieldsKey;// id du chmp recherche dans la combo "champs"
          			  }	  
          		  }   	        
          		  if(idField!=-1){// rq. si idField!=-1 : il y a au moins un resultat        			  
                  var Tab=new Array();         			
                  var nbLimitResultats = 100;
                  
                  if(response.results[lname].data.length>=nbLimitResultats){
                    this.comboPossibleValue.emptyText = 'Liste limitée aux '+nbLimitResultats+' premières val.';
                  
                  }else{
                      this.comboPossibleValue.emptyText = 'Sélectionnez';
                  }
                  for(dataId in response.results[lname].data){
                      if(dataId<nbLimitResultats){// limite à 100 résultats
                        Tab.push([response.results[lname].data[dataId][idField], response.results[lname].data[dataId][idField]]);
                      }
                  }  				
                  this.comboPossibleValue.getStore().removeAll();
                  this.comboPossibleValue.getStore().loadData(Tab);  
                  this.comboPossibleValue.applyEmptyText();
                }else{
                  this.comboPossibleValue.getStore().removeAll();
                  this.comboPossibleValue.emptyText = 'Aucune valeur';
                  this.comboPossibleValue.applyEmptyText();
                }
          	  }	        	
          }else{
        	  this.comboPossibleValue.getStore().removeAll();
            this.comboPossibleValue.emptyText = 'Aucune valeur';
            this.comboPossibleValue.applyEmptyText();
          }	
        },
        failure: function (response) {
        	response = Ext.decode(response.responseText); 	
        	Carmen.Util.handleAjaxFailure(response, "Requêtes attributaires"); 
        },
        params: queryParams
    });
  	if(this.textfieldValue.isVisible()){
      this.comboPossibleValue.enable(); 
  		this.comboPossibleValue.show(); 
  		this.textfieldValue.hide();
      this.listvalueBtn.setText('<i class="fa fa-edit"></i>');
  	}else{
  		this.comboPossibleValue.setValue(null);
      this.comboPossibleValue.hide(); 
  		this.textfieldValue.show();
      this.listvalueBtn.setText('<i class="fa fa-search"></i>');
  	} 	
  },

  
  updateToField : function(c, r, i) {
	    var textfieldValueStr = this.textfieldValue.getValue();
	    
	    if(textfieldValueStr = textfieldValueStr.length>0){
	    	this.textfieldValue.setValue('');
	    } 
	    this.textfieldValue.setValue(c.getValue()); 
  },
  
  activateNbLimit : function() {	   
  	if(this.lineLimitCB.getValue()==true){
  		this.valueLineLimit.enable();
  	}else{
  		this.valueLineLimit.reset();//let enabled  
  		this.valueLineLimit.disable();
  	}	  
  },
  
  rewriteRequest : function(str) {
 // rewriting request according to SQL Syntax and field alias names...
    // protecting space in string
    str = str.replace(/\"([^\"]+)\"/g, function(x) { return x.replace(/\s/,'^^');});

    // rewriting field alias with field names
    for (var i=0; i<this.currentFieldData.length; i++) {
    var pattern = new RegExp('\s*' + this.escapeSpecialchars(this.currentFieldData[i][1]) + '\s*','g');
    str = str.replace(pattern, this.currentFieldData[i][0]);
    }
    // rewriting logical operator
    for (var i=0; i<this.logicalOperatorMap.length; i++) {
    var pattern = new RegExp('\s+' + this.escapeSpecialchars(this.logicalOperatorMap[i][1]) + '\s+','g');
    str = str.replace(pattern, ' ' + this.logicalOperatorMap[i][0] + ' ');
    }

    // rewriting operator rules...
    for(var i = 0; i < this.operatorMap.length; i++){
    var pattern = new RegExp(this.operatorMap[i][0], 'g');
    var patternStr = this.operatorMap[i][2];
    if(str.search(pattern) != -1){
    str = str.replace(pattern, this.operatorMap[i][1]);
    }
    }

    // rewriting field alias with field names
    for (var i=0; i<this.currentFieldData.length; i++) {
    var pattern = new RegExp('\s*' + this.escapeSpecialchars(this.currentFieldData[i][0]) + '\s*','g');

    // str = str.replace(pattern, ' CAST(' + this.currentFieldData[i][0] + ' AS integer) ');
    }
    //In case of string constraint, we remove (CAST ... ) sentence
    /*str = str.replace(/'\s*CAST\(/g, "");
    str = str.replace(/\s+AS\sinteger\)\s*'/g, "");*/



    str = str.replace(/\'\"/g, '\'');
    str = str.replace(/\"\'/g, '\'');
    str = str.replace(/%\"/g, '%');
    str = str.replace(/\"%/g, '%');
    str = str.replace(/\"/g, '\'');
    // removing space protection
    str = str.replace(/\^\^/g, ' ');

    return str;
  }, 
  

  
  isValidRequest : function(str, asString) {
    var me = this;
    var matches = {};
    var oStr = ''+str;
    var index = -1;
    
    var getIndex = function(match){
    	var tmp = oStr.indexOf(match, index+1);
    	if ( tmp==-1 ){
    		tmp = str.indexOf(match);
    	}
    	
    	while ( Ext.isDefined(matches[tmp]) ) tmp++;
    	return tmp;
    }
    
    //Rewriting request according to OGR syntax
    str = str.replace(/("[^\"]*?")/g, function(x, $1) { matches[index = getIndex($1)] = x;return '[value'+(index)+']';});
    
  // rewriting field alias with field names
    for (var i=0; i<me.currentFieldData.length; i++) {
    	index = -1;
      var pattern = new RegExp('\s*(' + me.escapeSpecialchars(me.currentFieldData[i][1]) + ')\s*','g');
      str = str.replace(pattern, function(x, $1) { matches[index = getIndex($1, index+1)] = ' [' + me.currentFieldData[i][0] + '] ';return '[field'+(index)+']';});
      index = -1;
      var pattern = new RegExp('\s*(' + me.escapeSpecialchars(me.currentFieldData[i][0]) + ')\s*','g');
      str = str.replace(pattern, function(x, $1) { matches[index = getIndex($1, index+1)] = ' [' + me.currentFieldData[i][0] + '] ';return '[field'+(index)+']';});
    }

  //Rewriting operator rules
    for(var i = 0; i < me.operatorMap.length; i++){
      var pattern = new RegExp(me.operatorMap[i][0], 'g');
      var patternStr = me.operatorMap[i][2];
      if(str.search(pattern) != -1){
        index = -1;
        if(patternStr == "COMMENCE PAR" || patternStr == "CONTIENT" || patternStr == "NE CONTIENT PAS"){
          var tmpPattern, tmpReplace;
          str.replace(pattern, function($0, $1, $2){
          	var field = matches[$1.replace(/\D/g, '')].replace(' [', '').replace('] ', '');
          	var value = matches[$2.replace(/\D/g, '')];
          	
          	tmpPattern = $0.replace($1, field).replace($2, value);
          	tmpReplace = me.operatorMap[i][4].replace(/"/g, '').replace('$1', field).replace('$2', value).replace('~', patternStr).replace(/NOT/g, 'NON'); 
          	return $0;
          });
          oStr = oStr.replace(tmpPattern, tmpReplace);
            
          str = str.replace(pattern, function(match, $1, $2) {
            matches[index = getIndex(patternStr, index+1)] = me.operatorMap[i][4];
            var replace = me.operatorMap[i][4];
            if ( !asString ) replace = replace.replace(/^(.+) ~ (.+)$/, '$1[operator'+(index)+']$2');
            return match.replace(pattern, replace);
          });
        }else{
          str = str.replace(pattern, function(match, $1) {
            matches[index = getIndex(patternStr, index+1)] = me.operatorMap[i][4];
            var replace = me.operatorMap[i][4];
            if ( !asString ) replace = replace.replace(/^(.+) [!=<>]+$/, '$1[operator'+(index)+']');
            return match.replace(pattern, replace);
          });
        }
      }
    }
    
    for(var i = 0; i < me.logicalOperatorMap.length; i++){
      var pattern = new RegExp('('+me.escapeSpecialchars(me.logicalOperatorMap[i][1])+')', 'g');
      index = -1;
      str = str.replace(pattern, function(match, $1) { matches[index = getIndex($1, index+1)] = me.logicalOperatorMap[i][3];return '[logical'+(index)+']';});
    }
    
    var openBracken = 0;
    var notBracken = []
    Ext.iterate(matches, function(key, value){
    	
      if ( value=="(" ) { if ( notBracken.indexOf('N')!=-1 && notBracken[notBracken.length-1]=='N' ) notBracken.push('O'); openBracken++;}
      if ( value==")" ) { if ( notBracken.indexOf('N')!=-1 ) notBracken.push('F'); openBracken--;}
      if ( value=="NOT" ) notBracken.push('N');
      if ( ["(", ")", "NOT"].indexOf(value)==-1 && notBracken.indexOf('N')!=-1 && notBracken[notBracken.length-1]=='O'  ) notBracken.push('D');
      var pattern = new RegExp("\\\[([flov])[a-z]+"+key+"\\\]");
      var altPattern = new RegExp('"(\\\^)?\\\[([flov])[a-z]+'+key+'\\\]"');
      if ( asString ){
        if ( altPattern.test(str) ){
          str = str.replace(altPattern, '"$1'+value.replace(/"/g, "")+'"');
        } else {
          str = str.replace(pattern, value);
        }
      } else {
        if ( altPattern.test(str) ){
          str = str.replace(altPattern, "$2");
        } else {
          str = str.replace(pattern, "$1");
        }
      }
    });

    console.log(str);
    str = Ext.String.trim(str);
    if ( asString ) return '('+str+')';
    str = str.replace(/ /g, '');
    str = str.toUpperCase();
    return openBracken==0 && ( Ext.isEmpty(notBracken) || /^((N+O+)+(D+F*)+)+F+$/.test(notBracken.join('')) ) && /^(L*FOVL*)+$/.test(str);
  }, 
  

  
  rewriteOgrRequest : function(str) {
  	return this.isValidRequest(str, true);
  },

 escapeSpecialchars : function(str) {
   var specialChars=['(',')','<','>','=','[',']'];
   for (var i=0;i<specialChars.length;i++) {
     var pattern = new RegExp("\\" + specialChars[i],'g');
     str = str.replace(pattern,'\\' + specialChars[i]);
   }
   return str;
 },

  // displaying results
  showWindow : function() {
    var control = this;
    //this.layerTree = this.getLayerTreeManager().getLayerTree().clone(this.transformNodeInfo);

    if (!this.win.rendered) {		
      this.win.add(this.requestPanel);
      
	    // adding listeners to the form
	    var submitBtn = Ext.getCmp('requestSubmit');
	    submitBtn.on(
	      'click',
	      function(btn, evt) {
	    	  //if (this.comboLayer.getValue()=='' || this.requestArea.getValue()==this.requestArea.originalValue){
	    	if (this.comboLayer.getValue()== 'none'){        
	           Ext.Msg.alert('Requêtes attributaires',
              'Vous n\'avez pas saisi de requête. Veuillez former une requête ou sélectionner une couche.');
            return;
	        }	    	
	    	if (this.requestArea.getValue()!='' || this.requestArea.getValue()!=this.requestArea.originalValue ){// dans le cas où il y a une req attributaire   		
		        var qstring = this.rewriteOgrRequest(this.requestArea.getValue());
		        var qOgrString = this.rewriteOgrRequest(this.requestArea.getValue());
		        
		        
		        var form = control.requestPanel.getForm();
		        if (!form.isValid() || !this.isValidRequest(this.requestArea.getValue()) )
		          Ext.Msg.alert('Requêtes attributaires',
		            'La requête saisie n\'est pas valide.');
		        else {
		          control.performQuery(control.buildParams(control.layerFieldsDesc, qstring, qOgrString, true));
		        }
	    	}else{// cas où il y a uniquement une couche de sélectionnée
	    		control.performQuery(control.buildParams(control.layerFieldsDesc, '1=1', '1=1', true));	
	    	}   
	      }, 
	      this);
	    
	    var submitReinitializeBtn = Ext.getCmp('reinitializeSubmit');
	    submitReinitializeBtn.on(
	      'click',
	      function(btn, evt) {
	    	  	// loading data in combos

	    	  	for(var i = 0; i < this.layerData.length; i++){
	    	  	  if(this.layerData[i][1].match(/<table.*<\/table>/)){
	    	        this.layerData[i][1] = this.layerData[i][1].replace(/<table.*<\/table>/, "");
	    	  	  }
	    	  	} 
	    	  	this.comboLayer.setValue('none')
      			var v = this.comboLayer.getValue();
      			var record = this.comboLayer.findRecord(this.comboLayer.valueField, v);
      			var i = this.comboLayer.store.indexOf(record);
  	        //clear values and disable fields
  	        this.comboField.clearValue();
  	        this.comboField.disable();
  	        this.comboLogical.clearValue();
  	        this.comboLogical.disable();
  	        this.comboOperator.clearValue();
  	        this.comboOperator.disable();
  	        this.comboPossibleValue.reset();
  	        this.comboPossibleValue.disable();
  	        this.valueSelectContainer.disable();
  	        this.requestArea.reset();//let enabled  
  	        this.lineLimitCB.setValue(false);
  	        this.lineLimitCB.disable();  
  	        this.valueLineLimit.reset();
  	        this.valueLineLimit.disable();
  	        if (i!=0) {
  	          this.comboField.setValue('');  
  	          this.comboField.getStore().removeAll();
  	          //console.log(control.layerField);
  	          this.comboField.getStore().loadData(control.fieldData[i-1]);
  	          this.currentFieldData = control.fieldData[i-1]; 
  	          this.comboField.enable();
  	          this.requestArea.enable();
  	        }
	        
	        
	      }, 
	      this);
	    
  
	    
      this.win.doLayout();
      
    } 

    if (!this.win.isVisible()) {
      this.initFields();
      var mainPanel = this.map.app.ui.getMainPanel();
      // automatic positionning don't work well after export tools are called... 
      /*
      var x = mainPanel.getEl().getX();
      var y = mainPanel.getEl().getY() + 27;
      //console.log("X,Y : " + x + " , " + y);
      this.win.setPagePosition(mainPanel.getEl().getX(), mainPanel.getEl().getY() + 27);
      */
      this.win.show();
      //this.gridInfoPanel.setWidth(this.win.getWidth()-15);
    }
  },


  getLayerTreeManager : function() {
    return app.map.getLayerTreeManager();
    if (this.layertreeManager == null) {
      var controls = this.map.getControlsByClass('Carmen.Control.LayerTreeManager');
      this.layerTreeManager = controls.length==0 ? null : controls[0];
    }
    return this.layerTreeManager;
  },

  getLayerTree : function() {
  	
    this.layerTree = this.getLayerTreeManager().getLayerTree().clone(this.transformNodeInfo);
    this.layerTree.filter(this.filterNodeLayerQueryable);
  	return this.layerTree;
  },

  getLayerRecursive : function(qLayerAtts){
	  var qLayerAtts1 = new Array();
	  for (var i=0; i<qLayerAtts.length; i++) {
	   if (qLayerAtts[i].attributes.type == 1){
			 qLayerAtts1 = qLayerAtts1.concat(this.getLayerRecursive(qLayerAtts[i].childNodes));
		 } else if (qLayerAtts[i].attributes.type == 0) {
			 qLayerAtts1.push(qLayerAtts[i]);
		 }
	  }
	  return qLayerAtts1;
  },
  // init the requestPanel fields, 
  // i.e the combos with data from the queryable layers
  initFields : function() {
  	var qLayerAtts = Ext.Array.pluck(app.layerTreeStore.queryRecords('type', 'layer'), 'data');
  	
    
  	this.layerData = [['none','Sélectionnez une couche']];
  	this.fieldData = [];
  	for (var i=0; i<qLayerAtts.length; i++) {
    
		  var layerAtt = qLayerAtts[i].attributes;
  		var layerObj = Carmen.user.map.getLayerById(layerAtt.layerId);;
      var layerFieldsData = [];
      this.layerFieldsDesc[layerAtt.mapName] = [];     
      //console.log(fieldsCarmenDesc);   		
  		layerObj && layerObj.get('fields').each(function(fieldDesc) {
  			//fieldDesc = Carmen.Util.decodeCarmenFieldDesc(fieldsCarmenDesc[j], layerAtt.baseQueryURL);
  			if (fieldDesc.get('fieldDatatype')=='STRING' && fieldDesc.get('fieldQueries') )             
  			  layerFieldsData.push([fieldDesc.get('fieldName'), '[' + fieldDesc.get('fieldAlias') +']', fieldDesc.get('fieldAlias')]);
  			if ( fieldDesc.get('fieldHeaders') || fieldDesc.get('fieldQueries') )
  			  this.layerFieldsDesc[layerAtt.mapName].push(fieldDesc);
  		}, this);
  		if ( layerFieldsData.length ){
        this.layerData.push([layerAtt.mapName, qLayerAtts[i].text]);
        this.fieldData.push(layerFieldsData);   
  		} else {
  			delete this.layerFieldsDesc[layerAtt.mapName];
  		} 
      
  	}
  	
  	// loading data in combos
 	
  	for(var i = 0; i < this.layerData.length; i++){
  	  if(this.layerData[i][1].match(/<table.*<\/table>/)){
        this.layerData[i][1] = this.layerData[i][1].replace(/<table.*<\/table>/, "");
  	  }
  	}
    this.comboLayer.getStore().loadData(this.layerData);
    
    // adding listener on layer combo
    var control = this;
    this.comboLayer.on(
      'change', 
      function() {
        console.log('change', this.comboLayer.getValue());
    		var v = this.comboLayer.getValue();
    		var record = this.comboLayer.findRecord(this.comboLayer.valueField, v);
    		var i = this.comboLayer.store.indexOf(record);
        //clear values and disable fields
        this.comboField.clearValue();
        this.comboField.disable();
        this.comboLogical.clearValue();
        this.comboLogical.disable();
        this.comboOperator.clearValue();
        this.comboOperator.disable();
        this.textfieldValue.reset();
        this.textfieldValue.disable();
        this.valueSelectContainer.disable();
        this.requestArea.reset();//let enabled  
        this.lineLimitCB.disable();  
        this.valueLineLimit.disable();
        if (i!=0) {
          this.comboField.setValue('');  
          this.comboField.getStore().removeAll();
          //console.log(control.layerField);
          this.comboField.getStore().loadData(control.fieldData[i-1]);
          this.currentFieldData = control.fieldData[i-1]; 
          this.comboField.enable();
          this.comboLogical.enable();
          
          this.requestArea.enable();
          this.lineLimitCB.enable();  
          
          //this.resizeComboToFitContent(this.comboField);//resize combo field 	
        }	
      },
      this
    );   
  },

  // build the parameters of the request
  buildParams : function(layerFieldsDesc, qstring, qOgrString, noParam) {
    var qitem = null;
    var form = Ext.getCmp('requestPanel').getForm();
    var values = form.getValues();
    
    //var comboLayer = Ext.getCmp('comboLayer');
    var mapName = this.comboLayer.getValue();


    // nb limite lie à la cb
    var valueLineLimit = this.valueLineLimit.getValue();
   
    if (noParam==true){// initialisation à -1 pour avoir des valeurs !="" (cf. dans index.php)
    	var qitem = -1;
    	var op = -1;
    	var pattern = -1;
   
    }else{
      var qitem =  this.currentField;
      var op = this.opFieldValue;
      var pattern = this.currentFieldValue;
    }
    
    var node = app.layerTreeStore.findRecord('mapName', mapName);
    var olNode;
    var mapfile = (node!=null && (olNode = app.layerTreeStore.OL_MAP.getItemById(node.get('OL_ITEM').id)) && olNode.mapfile!=null) ?
      olNode.mapfile :
      this.mapfile || Carmen.user.map.get('real_mapfile');
   
    var fieldsData = Ext.Array.pluck(layerFieldsDesc[mapName], 'data');
    Ext.each(fieldsData, function(field, index){
    	fieldsData[index] = Ext.apply({}, field);
    	delete fieldsData[index].layer;
    });
    var fields = []
    var params = {
      layer : mapName,
      qstring : qstring,
      qOgrString : qOgrString,
      pattern : pattern,
      op : op,
      qitem : qitem,
      fields : Ext.util.JSON.encode(fieldsData),
      valueLineLimit : valueLineLimit,
      map : mapfile 
     
    }

    //console.log(params);
    return params;    
  },


  highlightSelectedFeatures: function () {
    if ( !this.OL_map.getLayersByName(this.vectorLayer.name).length ){
      this.OL_map.addLayer(this.vectorLayer);
      this.OL_map.setLayerIndex(this.vectorLayer, 10000);
    }
    var bounds = this.model.bounds;
    if (bounds) {
        Descartes.Utils.zoomToSuitableExtent(this.OL_map, bounds);
    }
    var geometries = this.model.geometries;
    this.vectorLayer.destroyFeatures();
    if (geometries && geometries.length) {
      var wktFormat = new OpenLayers.Format.WKT();
      for(var i=0; i<geometries.length; i++) {
        var wkt = geometries[i];
        var features = wktFormat.read(wkt);
        if(features) {
                if(features.constructor != Array) {
                    features = [features];
                }
                this.vectorLayer.addFeatures(features);
            } else {
//                  console.log('Bad WKT');
            }
      }
//        console.log({"highlightSelectedFeatures non implémenté":geometries});
    }
  },

  // launch the request to the server
  performQuery : function (queryParams) {
    var control = this;
    
    var received = function (response) {
      response = Ext.decode(response.responseText);      
      if (response.totalCount == 0) {
        //console.log('no results...');
        control.clearGridPanel();
        Ext.Msg.alert("Requêtes attributaires", "Aucun résultat ne correspond à votre recherche.");
      }else{    	  
    	 
    	  var nbResults = (response.totalCount)*1;
    	  var nbLimit = (Carmen.parameters.PRO_REQUETEUR_NB_LIMIT_OBJ || 100)*1;   	  
    	  
    	  var readResults = function(){
    	  	var ajaxParams = {briefFields : [], fields : [], layerInfo : {}, layers : [], map : null, showGraphicalSelection : true, shape : '('+app.map.OL_map.getExtent().toString()+')'};
    	  	var jsonObject = response;
    	  	Ext.iterate(response.results, function(lname, result){
    	  		var briefFieldsName = Ext.Array.pluck(Ext.Array.pluck(control.layerFieldsDesc[lname], 'data'), 'fieldName').concat(['extent','fid','geometryWKT']);
    	  		var indexLayer = control.comboLayer.store.find(control.comboLayer.valueField, lname);
    	  		var fieldsName = Ext.Array.pluck(control.fieldData[indexLayer-1], 0).concat(['extent','fid','geometryWKT']);
    	  		
            var node = app.layerTreeStore.findRecord('mapName', lname);
            if ( !node ) return;
            node = node.data;
            var layerObj = Carmen.user.map.getLayerById(node.attributes.layerId);
            if ( !layerObj ) return;
            var briefFields = [], fields = [];
            layerObj.get('fields').each(function(field){
            	var fieldType = Carmen.Dictionaries.FIELDTYPES.findRecord('fieldTypeId', field.get('fieldType'));
            	var fieldDesc = {
            		name : field.get('fieldName'),
                alias : field.get('fieldAlias'),
            		type : fieldType ? fieldType.get('fieldTypeName') : 'Unknown',
                url : field.get('fieldUrl'),
            		title : null
            	};
              if ( briefFieldsName.indexOf(field.get('fieldName'))!=-1 ){
                briefFields.push(fieldDesc)
              }
              if ( fieldsName.indexOf(field.get('fieldName'))!=-1 ){
              	fields.push(fieldDesc)
              }
            });
            
            var fieldsIndex = [];
            Ext.each(fieldsName, function(field){
            	var index = briefFieldsName.indexOf(field);
            	if ( index!=-1 )
            	  fieldsIndex.push(index);
            });
            var data = [];
            Ext.each(result.data, function(row){
            	var dataRow = [];
            	Ext.each(row, function(value, index){
              	if ( fieldsIndex.indexOf(index)!=-1 ){
              		dataRow.push(value);
              	}
            	});
            	data.push(dataRow);
            });
            result.briefData = result.data;
            result.briefFields = briefFieldsName;
            result.data = data;
            result.fields = fieldsName;
            
            
            ajaxParams.briefFields.push(briefFields);
            ajaxParams.fields.push(fields);
            ajaxParams.layers.push(lname);
            ajaxParams.layerInfo[lname] = {title : layerObj.get('layerTitle')};
            
    	  	}, control);
    	  	
    	  	ajaxParams.briefFields = Ext.encode(ajaxParams.briefFields);
    	  	ajaxParams.fields = Ext.encode(ajaxParams.fields);
    	  	ajaxParams.layers = Ext.encode(ajaxParams.layers);
    	  	ajaxParams.layerInfo = Ext.encode(ajaxParams.layerInfo);
    	  	
    	  	control.renderer.draw(jsonObject, ajaxParams, app.map.OL_map, []);
    	  	control.win.collapse().setY(10);
    	  };
    	    
    	  if(nbResults > nbLimit){// msg d'alerte 
        	  Ext.Msg.confirm('Requêtes attributaires','Le nombre d\'informations à afficher est important, souhaitez-vous continuer?', function(btn, text){
        		  if (btn != 'yes') return; 
      		  	readResults();
        	  });
    	  }else{  
    	    readResults();
    	  }
      }   
    }
    Ext.Ajax.timeout = this.requestTimeout;
    Ext.Ajax.request({
      url: Routing.generate('carmen_descartes_advancedqueryattributes'),
      //timeout: this.requestTimeout,
      success: function(response) { 
      	 received(response); },
      failure: function (response) { 
         Carmen.Util.handleAjaxFailure(response, "Requêtes attributaires"); 
      },
      params: queryParams
    });
  }, 

  clearGridPanel : function() {
  	var gPanel = this.gridInfoPanel;
    if(gPanel!=undefined){
      gPanel.setTitle('Aucun résultat');
      //var xlsTool = this.gridInfoPanel.getTool('xls');
      var xlsTool = Ext.getCmp('queryPanel_xls');
      xlsTool.hide();
      //var csvTool = this.gridInfoPanel.getTool('csv');
      var csvTool = Ext.getCmp('queryPanel_csv');
      csvTool.hide();
      gPanel.reconfigure(Carmen.Util.emptyStore, Carmen.Util.emptyColumnModel);
      if (!gPanel.isVisible()){
        gPanel.show();
        gPanel.setWidth(this.win.getWidth()-15);
      }
    }
  },
  
  updateGridPanel : function(layerNode, queryfile) {
    var control = this;  
    
    var panel_id = this.displayClass +'_' + 'gridInfoPanel_' +  Ext.id();
    this.gridInfoPanel = Ext.create('Ext.grid.Panel', {
      id : panel_id,
      store: new Ext.data.ArrayStore({
        fields: [],
        data: []
      }),
      columns: [],
      title:'Résultats sélectionnés',
      hidden: true,
      autoScroll: true,
      fitToFrame: true,
      border: false,
      iconCls : 'cmnInfoGridZoomIcon',
      bounds: null,
      tools:[{
            id: 'queryPanel_zoom',
            qtip: 'Zoom sur les résultats',
            renderTpl: [
              '<i id="{id}-toolEl" data-ref="toolEl" class="fa fa-search fa-1x"' +
              '" style="color: #FFF; font-size: 16px;" role="presentation"/></i>'
            ],
            handler: function(){
              // show help here
				var bounds = this.toolOwner.bounds;
				control.map.zoomToSuitableExtent(bounds);
			}
      },{
        id:'queryPanel_xls',
        qtip: 'Export de la sélection au format Excel',
        scope : this,
		renderTpl: [
		  '<i id="{id}-toolEl" data-ref="toolEl" class="fa fa-file-excel-o fa-1x"' +
		  '" style="color: #FFF; font-size: 16px;" role="presentation"/></i>'
		]
      },
      {
        id:'queryPanel_csv',
        qtip: 'Export de la sélection au format CSV',
		renderTpl: [
		  '<i id="{id}-toolEl" data-ref="toolEl" class="fa fa-file-text-o fa-1x"' +
		  '" style="color: #FFF; font-size: 16px;" role="presentation"/></i>'
		]
      }]
    });     

    var gPanel = this.gridInfoPanel; 
    this.map.app.ui.delAllInfoPanel();
    this.map.app.ui.addToInfoPanel(gPanel);
    this.map.app.ui.infoPanel.setTitle('Résultats');
    // clearing selection
    this.map.clearSelection();
    // removing listeners...
    //gPanel.purgeListeners();    
    // updating panel title

    gPanel.setTitle(layerNode.attributes.name + ' ' + 
      layerNode.attributes.infoData.data.length + 
      ' résultats');
    var extentStr = layerNode.attributes.infoData.extent;
    var minScale = layerNode.attributes.minScale;
    var maxScale = layerNode.attributes.maxScale;
    gPanel.bounds = Carmen.Util.strExtentToOlBounds(extentStr, Carmen.Control.AdvancedQueryAttributes.PUNCTUAL_RADIUS);
    this.map.app.ui.infoPanel.bounds = gPanel.bounds;
    
    var lnode = layerNode;
    /*gPanel.header.on('click', 
        function() {
          control.map.zoomToSuitableExtent(bounds, minScale, maxScale);
          lnode.OlLayer.setDisplayMode(
             Carmen.Layer.MapServerGroup.DISPLAY_NQUERYMAP, 
             lnode.attributes.layerName, null, 
             lnode.attributes.infoData.qfile);
        });*/
    control.map.zoomToSuitableExtent(gPanel.bounds, minScale, maxScale);
    
    app.layerTreeStore.OL_MAP.getItemById(lnode.OL_ITEM.id).setDisplayMode(
       Carmen.Layer.MapServerGroup.DISPLAY_NQUERYMAP, 
       layerNode.attributes.mapName, null, 
       layerNode.attributes.infoData.qfile,
       layerNode.attributes.infoData.ogrMapfile);
     // configuring and showing the result table panel
     var gConfig = this.buildGridConfig(layerNode.attributes.infoFields, layerNode.attributes.infoData);

     // configuring exports...
	 //var xlsTool = gPanel.getTool('xls');
	 var xlsTool = Ext.getCmp('queryPanel_xls');
	 //xlsTool.removeAllListeners();
	 xlsTool.show();
	 xlsTool.on('click',
	  function() {
	    var url = Routing.generate('carmen_descartes_advancedqueryattributes_xls');
		var f = this.control.gridInfoFormPanel; 
		queryParams = { 
		  field : Ext.encode(this.node.attributes.infoFieldDesc),
		  data : Ext.encode(this.node.attributes.infoData.data),
		  selectionName : this.node.text,
		  output : "XLS"
		};
		Carmen.Util.doFormUpload(f, url, queryParams);
	  }, 
	  { control : control, node : layerNode } // function scope
	 );
	 
	 
	 
	 // configuring exports...
	 var xlsTool = Ext.getCmp('queryPanel_csv');
	 xlsTool.show();
	 xlsTool.on('click',
	  function() {
	    var url = Routing.generate('carmen_descartes_advancedqueryattributes_xls');
		var f = this.control.gridInfoFormPanel; 
		queryParams = { 
		  field : Ext.encode(this.node.attributes.infoFieldDesc),
		  data : Ext.encode(this.node.attributes.infoData.data),
		  selectionName : this.node.text,
		  output : "CSV"
		};
		Carmen.Util.doFormUpload(f, url, queryParams);
	  }, 
	  { control : control, node : layerNode } // function scope
	 );

     gPanel.reconfigure(gConfig.store, gConfig.columnModel);
     gPanel.on('rowclick', 
      function(grid, record, tr, rowIndex, e, eOpts ){
        var rec = grid.store.getAt(rowIndex);
        // zooming to the right extent
        var extentStr = rec.get('extent');
        var bounds = Carmen.Util.strExtentToOlBounds(extentStr, Carmen.Control.AdvancedQueryAttributes.PUNCTUAL_RADIUS);
        //control.map.zoomToSuitableExtent(bounds, minScale, maxScale);

        // highlighting the feature linked to the row
        var fid = rec.get('fid');
        app.layerTreeStore.OL_MAP.getItemById(lnode.OL_ITEM.id).setDisplayMode(
          Carmen.Layer.MapServerGroup.DISPLAY_QUERYMAP, 
          lnode.attributes.mapName, fid);

      });
      
      gPanel.on('cellclick', 
        function( grid, td, cellIndex, record, tr, rowIndex, e, eOpts){
			if (cellIndex==1) {
				var rec = grid.getStore().getAt(rowIndex); 
				var extentStr = rec.get('extent');
				var bounds = Carmen.Util.strExtentToOlBounds(extentStr, Carmen.Control.Info.PUNCTUAL_RADIUS);
				control.map.zoomToSuitableExtent(bounds, minScale, maxScale);
			 }
			 if (cellIndex==2) {
				var rec = grid.getStore().getAt(rowIndex); 
				var columns = grid.ownerCt.columnManager.columns;
				control.buildInfoWindow(rec, columns, rowIndex);					
				
			 }
      });
      
      
      gPanel.on('selectionchange', 
        function(grid, selected, eOpts){
			if (selected.length == 0){
					var minScale = lnode.attributes.minScale;
					var maxScale = lnode.attributes.maxScale;
					control.map.clearGraphicalSelection();
					app.layerTreeStore.OL_MAP.getItemById(lnode.OL_ITEM.id).setDisplayMode(
						Carmen.Layer.MapServerGroup.DISPLAY_NQUERYMAP, 
						lnode.attributes.mapName, null,
						queryfile);
			 }
      });
      // registering selection
      olLayer = app.layerTreeStore.OL_MAP.getItemById(lnode.OL_ITEM.id);
      lname = lnode.attributes.mapName;
      if (olLayer.handleSelection(lname)) { 
  	    this.map.addToSelectionLayers(olLayer);
  	    /*olLayer.setSelectionStore(lname, 
  	      new Ext.data.SimpleStore({
            fields: lnode.attributes.infoData.fields,
            data :  lnode.attributes.infoData.data
          }));*/
      } 
     if (!gPanel.isVisible()) {
       //console.log('isnotvisible');
       gPanel.show();
       gPanel.setWidth(this.win.getWidth()-15);
       //resize grid
       gPanel.setHeight(eval(this.win.getEl().getHeight(true)-this.requestPanel.getHeight()));
     }
     else{
       //console.log('isalreadyvisible');
       gPanel.setWidth(this.win.getWidth()-15);
       //resize grid
       gPanel.setHeight(eval(this.win.body.getStyle('max-height').replace(/px/,'')-this.requestPanel.getHeight()));
     }
  }, 
  
  extentRenderer : function(v) {
  	return '<i class="fa fa-fw fa-search fa-1x"' +
              '" style="color: #000; font-size: 16px;"></i>';
  },
  
  infoRenderer : function(v) {
  	return '<i class="fa fa-fw fa-info-circle fa-1x"' +
              '" style="color: #000; font-size: 16px;"></i>';
  },


  buildGridConfig: function (fieldsDesc, result) {
    var fields = new Array();
    var columns = new Array();
    
    columns.push(new Ext.grid.RowNumberer());
    columns.push({
      id : 'extent',
      header : '',
      width : 30,
      resizable: true,
      renderer: this.extentRenderer,
      dataIndex: 'extent'
    });
    columns.push({
	  //id : 'info',
	  header : '',
	  width : 40,
	  resizable: true,
	  renderer: this.infoRenderer,
	  dataIndex: 'info'
	});
    
    var arrFieldsDesc =  Carmen.Util.chompAndSplit(fieldsDesc,';');
    //console.log(arrFieldsDesc);
    for (var i=0; i<arrFieldsDesc.length; i++) {
    	//console.log(arrFieldsDesc[i]);
    	var desc = Carmen.Util.decodeCarmenFieldDesc(arrFieldsDesc[i]);
    	//console.log(desc);
    	var fDesc = {name : desc.name + i.toString(), type : Carmen.Util.carmenTypeToExtType(desc.type)};
    	if (desc.type=='date') 
    	 fDesc['dateFormat'] = Carmen.Util.carmenDateTypeToExtDateFormat(desc.type);
    	fields.push(fDesc);
    	
    	var colDesc = {
    		id: Ext.id(), //desc.name + i.toString(),
    		header: desc.alias,
    		width: 200,
    		sortable: true,
    		resizable: true,
    		renderer: Carmen.Util.carmenTypeToExtRenderer(desc.type),
    		dataIndex: desc.name + i.toString()
    	};
    	columns.push(colDesc);
    }
    fields.push({name : 'extent'});
    fields.push({name : 'fid'});
    //console.log(fields);
    var config = {
	  	store : new Ext.data.SimpleStore({
	        fields: fields,
	        data : result.data
	    }),
	    columnModel : columns 
	  };

	  return config;
  },


  // Filter and transform function used on tree
  filterNodeLayerQueryable :  function(node) {
  	var res = true;
    res = ('type' in node.attributes) && 
      ((node.attributes.type == 'group' && node.hasChildNodes()) ||
       (node.attributes.type == 'layer' && app.layerTreeStore.OL_MAP.getItemById(node.OL_ITEM.id).getVisibility(node.attributes.mapName)
       && node.attributes.briefFields && node.attributes.briefFields.length>0
       && node.attributes.infoFields && node.attributes.infoFields.length>0));
    return res;
  },

  filterNodeLayerWithResults : function(node) {
  	var res = true;
    res = ('type' in node.attributes) && 
      ((node.attributes.type == 'group' && node.hasChildNodes()) ||
       (node.attributes.type == 'layer' && node.attributes.infoData!=null) ||
       (node.attributes.type == Carmen.Control.LayerTreeManager.NODE_FEATURE));    
    return res;
  },

  transformNodeInfo : function(atts) {
  	var cloneAtts = Carmen.Util.clone(atts);
/*
  	if (cloneAtts.text!=Carmen.Control.LayerTreeManager.TREE_NODE_ROOT_LABEL)
  	   cloneAtts.uiProvider = Carmen.TreeNodeInfoUI;
  	else
  	   cloneAtts.uiProvider = Carmen.RootTreeNodeInfoUI;  	   
*/
  	cloneAtts.cls = 'none';
    cloneAtts.infoData = null;  	
  	cloneAtts.listeners = {};
  	cloneAtts.expandable = true;
  	if (cloneAtts.type == 'layer') {
  		cloneAtts.icon = '/IHM/images/NoTreeIcon.gif';
  		cloneAtts.iconCls = 'cmnInfoGridZoomIcon';
  	}
    cloneAtts.text = atts.layerAlias!=null ? atts.layerAlias : atts.text;
  	return cloneAtts;
  },
  
    buildInfoWindow : function(rec, columns, rowIndex){
	  
	    var data = [];
		for(var i=3;i<columns.length;i++){
		  var value = {};
		  value['key'] = columns[i].text;
		  value['value'] = columns[i].renderer.apply(null,[rec.get(columns[i].dataIndex)]);
		  data.push(value);
		}
		
		Ext.create('Ext.data.Store', {
			storeId:'infoStore',
			fields:['key', 'value'],
			data:{'items':
				data
			},
			proxy: {
				type: 'memory',
				reader: {
					type: 'json',
					rootProperty: 'items'
				}
			}
		});

		var gridInfoPanel = Ext.create('Ext.grid.Panel', {
			store: Ext.data.StoreManager.lookup('infoStore'),
			columns: [
				{ text: 'Libellé',  dataIndex: 'key', flex: 1 },
				{ text: 'Donnée', dataIndex: 'value', flex: 1 }
			],
			height: 200,
			width: 400
		});
		this.winInfo.setTitle("Information");
		this.winInfo.removeAll();
		this.winInfo.add(gridInfoPanel);
		this.winInfo.show();
  },
  
  CLASS_NAME: "Carmen.Control.AdvancedQueryAttributes"

});

Carmen.Control.AdvancedQueryAttributes.PUNCTUAL_RADIUS = 100.0;

