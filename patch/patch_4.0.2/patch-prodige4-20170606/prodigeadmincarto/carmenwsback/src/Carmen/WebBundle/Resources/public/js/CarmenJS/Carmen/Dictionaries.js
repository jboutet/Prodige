Ext.ns("Carmen.Dictionaries");
Ext.Ajax.timeout = 0;

var all_lexicals_proxy = new Ext.data.JsonStore({
  autoLoad : true,
  fields : [
    {name : 'entities', type : 'auto'}
  , {name : 'data', type : 'auto'}
  ],
  
  proxy: {
    type: 'ajax',
    url: Routing.generate('carmen_ws_get_all_lexicals')
  },
  listeners : {
    load : function(){
      var entities = this.collect('entities')[0];
      var data = this.collect('data')[0];
      Ext.each(entities, function(entity){
        var store = Ext.StoreMgr.lookup(entity);
        if ( !store ) return;
        store.loadRawData(data[entity]);
        store.fireEvent('load', store, store.getRange());
      });
    }
  }
});

Ext.define('Carmen.Model.LexCategoryKeyword', {
  extend: 'Ext.data.Model',
  idProperty : 'categoryId',
  fields : [
      {name : 'categoryId'}
    , {name : 'categoryName'}
    , {name : 'valueField', calculate : function(data){return data.categoryId;}}
    , {name : 'displayField', calculate : function(data){return data.categoryName;}}
  ]
});
Ext.define('Carmen.Model.Keyword', {
  extend: 'Ext.data.Model',
  idProperty : 'keywordId',
  fields : [
    'keywordId',
    {name : 'category', reference : 'Carmen.Model.LexCategoryKeyword'
    , convert : function(value){
        if ( value instanceof Carmen.Model.LexCategoryKeyword ){
          return value;
        }
        if ( Ext.isObject(value) ){
          return Ext.create('Carmen.Model.LexCategoryKeyword', value);
        }
        var index;
        var store = Carmen.Dictionaries.KEYWORDS_CATEGORIES;
        if ( !store ) return null;
        if ( (index = store.find('categoryId', value))!=-1 ){
          return store.getAt(index);
        }
      }
    , serialize : function(value){
        if ( value instanceof Carmen.Model.LexCategoryKeyword ){
          return value.get('categoryId');
        }
      }
    },
    'keywordName',
    {name : 'displayField', calculate : function(data){return data.keywordName;}},
    {name : 'valueField', calculate : function(data){return data.keywordId;}}
  ]
});
Ext.define('Carmen.Model.FavoriteArea', {
  extend: 'Ext.data.Model',
  idProperty : 'areaId',
  fields: [{
    name : 'areaId',
    persist : false
  }, 'areaName', 'areaXmin', 'areaYmin', 'areaXmax', 'areaYmax'],
  
  getProxy : function(mapId){
    return Ext.Factory.proxy({
      type: 'rest',
      model : (this instanceof Carmen.Model.FavoriteArea ? this.$className : 'Carmen.Model.FavoriteArea'),
      url: Routing.generate('carmen_ws_favoritearea', {
        mapId : ( Ext.isDefined(mapId) 
                  ? mapId 
                  : ( Ext.isDefined(Carmen.user.map) 
                      ? Carmen.user.map.get('mapId') 
                      : '{mapId}'))
      })
    });
  }
});


/**
 * Initialization of the application of dictionaries (remote or local) represented by JsonStore
 * @type Object : {KEY : store}
 */
Carmen.Dictionaries = {
  /**
   * @mode  remote
   * @table lex_units
   */
  UNITS : new Ext.data.JsonStore({
    storeId : 'lexUnits',
    autoLoad : false,
    fields : [
      {name : 'unitCode'}
    , {name : 'unitName'}
    , {name : 'valueField', mapping : 'unitCode'}
    , {name : 'displayField', mapping : 'unitName'}
    ]
  }),
  
  /**
   * @mode  remote
   * @table lex_iso_condition
   */

  
  CONDITION : new Ext.data.JsonStore({
    AUTRES_ID : "Autres",
    storeId : 'lexIsoCondition',
    autoLoad : false,
    fields : [
      {name : 'conditionId'}
    , {name : 'conditionName'}
    , {name : 'valueField', mapping : 'conditionName'}
    , {name : 'displayField', mapping : 'conditionName'}
    ]
  }),
  
  
  /**
   * @mode  remote
   * @table lex_projection
   */
  PROJECTIONS : new Ext.data.JsonStore({
    storeId : 'lexProjection',
    autoLoad : false,
    fields : [
      {name : 'projectionEpsg'}
    , {name : 'projectionName'}
    , {name : 'projectionProvider'}
    , {name : 'valueField', mapping : 'projectionEpsg'}
    , {name : 'displayField', calculate : function(data){return data.projectionName+' ('+data.projectionProvider+':'+data.projectionEpsg+')'}}
    ],
    
    clone : function(){
      return Ext.create(this.$className, Ext.apply({
        data: this.getData().getValues('data')
      }, this.initialConfig));
    }
  }),
  
  /**
   * @mode  local
   */
  OUTPUTFORMATS : new Ext.data.JsonStore({
    storeId : 'OUTPUT_FORMATS',
    fields : [
      {name : 'valueField'}
    , {name : 'displayField'}
    ],
    data : [{
      "valueField" : "PNG",
      "displayField" : "PNG"
    }, {
      "valueField" : "JPEG",
      "displayField" : "JPEG"
    }, {
      "valueField" : "GIF",
      "displayField" : "GIF"
    }]
  }),
  
  /**
   * @mode  remote
   * @table lex_category_keyword
   */
  KEYWORDS_CATEGORIES : new Ext.data.JsonStore({
    storeId : 'lexCategoryKeyword',
    model : 'Carmen.Model.LexCategoryKeyword',
    autoLoad : false
  }),
  
  /**
   * @mode  remote
   * @table lex_category_keyword
   */
  KEYWORDS : new Ext.data.JsonStore({
    storeId : 'KEYWORD',
    model : 'Carmen.Model.Keyword',
    autoLoad : false,
    proxy: {
      type: 'rest',
      url: Routing.generate('carmen_ws_get_keywords_combobox')
    }
  }),

  /**
   * @mode  remote
   * @table lex_color_id
   */
  COLORID : new Ext.data.JsonStore({
    storeId : 'lexColorId',
    autoLoad : false,
    fields : [
      {name : 'colorId'}
    , {name : 'colorName'}
    , {name : 'valueField', mapping : 'colorId'}
    , {name : 'displayField', mapping : 'colorName'}
    ]
  }),

  /**
   * @mode  remote
   */
  LOGOPATH_IMAGES : new Ext.data.JsonStore({
    storeId : 'LOGOPATH_IMAGES',
    autoLoad : true,
    fields : [
      {name : 'id'}
    , {name : 'value'}
    , {name : 'valueField', mapping : 'id'}
    , {name : 'displayField', mapping : 'value'}
    ],
    proxy: {
      type: 'rest',
      url: Routing.generate('carmen_ws_mapui_combobox_images')
    }
  }),

  /**
   * @mode  remote
   */
  FONTS : new Ext.data.JsonStore({
    storeId : 'FONTS',
    autoLoad : true,
    fields : [
      {name : 'id'}
    , {name : 'value'}
    , {name : 'valueField', mapping : 'id'}
    , {name : 'displayField', mapping : 'value'}
    , {name : 'withoutExtension', mapping : 'value', convert : function(value){return (Ext.isString(value) ? value.replace(/\..+$/g, "") : value);}}
    ],
    proxy: {
      type: 'rest',
      url: Routing.generate('carmen_ws_mapui_combobox_fonts')
    }
  }),

  /**
   * @mode  local
   */
  FONTSIZES : new Ext.data.JsonStore({
    storeId : 'FONTSIZES',
    fields:[
      {name : 'valueField'}
    , {name : 'displayField'}
    ],
    data : (function(){
      var data = [];
      for (var i=5; i<20; i++){
        data.push({valueField:i,  displayField:i});
      }
      return data;
    })()
  }),

  /**
   * @mode  remote
   */
  BANNERMODELS : new Ext.data.JsonStore({
    storeId : 'BANNERMODELS',
    autoLoad : false,
    fields : [
      {name : 'id'}
    , {name : 'value'}
    , {name : 'default'}
    , {name : 'valueField', mapping : 'id'}
    , {name : 'displayField', mapping : 'value'}
    ],
    proxy: {
      type: 'rest',
      url: Routing.generate('carmen_ws_mapui_combobox_bannermodels')
    }
  }),

  /**
   * @mode  remote
   */
  LAYOUTMODELS : new Ext.data.JsonStore({
    storeId : 'LAYOUTMODELS',
    autoLoad : false,
    fields : [
      {name : 'accountModelId'}
    , {name : 'accountModelName'}
    , {name : 'accountModelFile'}
    , {name : 'valueField', mapping : 'accountModelFile'}
    , {name : 'displayField', mapping : 'accountModelName'}
    ],
    proxy: {
      type: 'rest',
      url: Routing.generate('carmen_ws_mapui_combobox_layoutmodels')
    }
  }),
  
  /**
   * @mode  local
   */
  GEOMETRYTYPES : new Ext.data.JsonStore({
    storeId : 'GEOMETRY_TYPE',
    TYPES : {
      POINT : 'POINT',
      LINE : 'LINE',
      POLYGON : 'POLYGON'
    },
    fields : [
      {name : 'valueField'}
    , {name : 'displayField'}
    , {name : 'codeField', calculate : function(data){return data.valueField;} }
    ],
    data : [{
      "valueField" : "POINT",
      "displayField" : _t("Dictionnary.point")
    }, {
      "valueField" : "LINE",
      "displayField" : _t("Dictionnary.line")
    }, {
      "valueField" : "POLYGON",
      "displayField" : _t("Dictionnary.polygon")
    }]
  }),
  
  /**
   * @mode  local
   */
  RASTERTYPES : new Ext.data.JsonStore({
    TYPES : {
      RASTER    : 'RASTER',
      TILEINDEX : 'TILEINDEX'
    },
    EXTENSIONS : {
      RASTER    : ['gtiff','tiff','tif','ecw'],
      TILEINDEX : ['shp', 'tab'] 
    },
    
    storeId : 'RASTER_TYPE',
    fields : [
      {name : 'valueField'}
    , {name : 'displayField'}
    , {name : 'codeField', calculate : function(data){return data.valueField;} }
    ],
    data : [{
      "valueField" : 'RASTER',
      "displayField" : _t("Dictionnary.uniqRaster")
    }, {
      "valueField" : 'TILEINDEX',
      "displayField" : _t("Dictionnary.tileRaster")
    }]
  }),

  /**
   * @mode  remote
   * @table lex_layer_type
   */
  LAYERTYPES : new Ext.data.JsonStore({
    storeId : 'lexLayerType',
    autoLoad : false,
    fields : [
      {name : 'layerTypeCode'}
    , {name : 'layerTypeName'}
    , {name : 'codeField', mapping : 'layerTypeCode'}
    , {name : 'valueField', mapping : 'layerTypeCode'}
    , {name : 'displayField', mapping : 'layerTypeName'}
    ]
  }),

  /**
   * @mode  remote
   * @table lex_analyse_type
   */
  ANALYSETYPES : new Ext.data.JsonStore({
    storeId : 'lexAnalyseType',
    TYPES : {
      UNIQSYMBOL     : 'UNIQSYMBOL',
      UNIQVALUE      : 'UNIQVALUE',
      GRADUATECOLOR  : 'GRADUATECOLOR',
      GRADUATESYMBOL : 'GRADUATESYMBOL',
      PROPORTIONAL   : 'PROPORTIONAL',
      PIECHART       : 'PIECHART',
      BARCHART       : 'BARCHART',
      RASTER         : 'RASTER'
    },
    CONFIG : {
      UNIQSYMBOL     : {NBCLASSES : {MIN : 1, MAX : 1},                                 FIELDTYPE : null},
      UNIQVALUE      : {NBCLASSES : {MIN : 1, MAX : null},                              FIELDTYPE : null},
      GRADUATECOLOR  : {NBCLASSES : {MIN : 1, MAX : Carmen.Defaults.NB_MAX_MS_CLASSES}, FIELDTYPE : 'NUMBER', MINCLASSES : 5},
      GRADUATESYMBOL : {NBCLASSES : {MIN : 1, MAX : Carmen.Defaults.NB_MAX_MS_CLASSES}, FIELDTYPE : 'NUMBER', MINCLASSES : 5},
      PROPORTIONAL   : {NBCLASSES : {MIN : 1, MAX : 1},                                 FIELDTYPE : 'NUMBER'},
      PIECHART       : {NBCLASSES : {MIN : 2, MAX : Carmen.Defaults.NB_MAX_MS_CLASSES}, FIELDTYPE : 'NUMBER'},
      BARCHART       : {NBCLASSES : {MIN : 1, MAX : Carmen.Defaults.NB_MAX_MS_CLASSES}, FIELDTYPE : 'NUMBER'},
      RASTER         : {NBCLASSES : {MIN : 1, MAX : 1},                                 FIELDTYPE : null}
    },
    autoLoad : false,
    fields : [
      {name : 'analyseTypeId'}
    , {name : 'analyseTypeCode'}
    , {name : 'analyseTypeName'}
    , {name : 'codeField', mapping : 'analyseTypeCode'}
    , {name : 'valueField', mapping : 'analyseTypeId'}
    , {name : 'displayField', mapping : 'analyseTypeName'}
    ]
  }),

  /**
   * @mode  remote
   * @table lex_analyse_type
   */
  CLASSDISTRIBUTION : new Ext.data.JsonStore({
    storeId : 'DISTRIBUTION_TYPE',
    DEFAULT : 'SAMEAMPLITUDE',
    TYPES : {
      SAMEAMPLITUDE   : 'SAMEAMPLITUDE',
      SAMECOUNT       : 'SAMECOUNT',
      HOMOGENEOUS     : 'HOMOGENEOUS'
    },
    fields : [
      {name : 'codeField', calculate : function(data){return data.valueField;} }
    , {name : 'valueField'}
    , {name : 'displayField'}
    ],
    data : [{
      valueField   : 'SAMEAMPLITUDE',
      displayField : 'Même amplitude'
    },{
      valueField   : 'SAMECOUNT',
      displayField : 'Même nombre de classes'
    },{
      valueField   : 'HOMOGENEOUS',
      displayField : 'Classes homogènes'
    }]
  }),

  /**
   * @mode  remote
   * @file mapfiles/etc/symbols.sym
   */
  MS_SYMBOLES : new Ext.data.JsonStore({
    storeId : 'MS_SYMBOLES',
    TYPES : {
      SHOWLIBRARY : "SHOWLIBRARY",
      HATCHSYMBOL : "hatchsymbol"
    },
    
    autoLoad : false,
    fields : [
      {name : 'geometryType'}
    , {name : 'symbolName'}
    , {name : 'symbolPath'}
    ]
  }),
  

  /**
   * @mode  local
   */
  LABELPOSITION : {
    DEFAULT : 'AUTO',
    TYPES : {
      AUTO          : {
        code : 'AUTO',
        value : 'auto'
      },
      UPPER_LEFT       :  {
        code : 'UPPERLEFT',
        value : 'ul'
      },
      UPPER_CENTER     :  {
        code : 'UPPERCENTER',
        value : 'uc'
      },
      UPPER_RIGHT      :  {
        code : 'UPPERRIGHT',
        value : 'ur'
      },
      CENTER_LEFT    :  {
        code : 'CENTERLEFT',
        value : 'cl'
      },
      CENTER_CENTER  :  {
        code : 'CENTERCENTER',
        value : 'cc'
      },
      CENTER_RIGHT   :  {
        code : 'CENTERRIGHT',
        value : 'cr'
      },
      LOWER_LEFT    :  {
        code : 'LOWERLEFT',
        value : 'll'
      },
      LOWER_CENTER  :  {
        code : 'LOWERCENTER',
        value : 'lc'
      },
      LOWER_RIGHT   :  {
        code : 'LOWERRIGHT',
        value : 'lr'
      }
    }
  },
  
  /**
   * @mode  remote
   */
  FIELDTYPES : new Ext.data.JsonStore({
    storeId : 'lexFieldType',
    autoLoad : false,
    fields : [
      {name : 'fieldTypeId'}
    , {name : 'fieldTypeName'}
    , {name : 'valueField', mapping : 'fieldTypeId'}
    , {name : 'displayField', mapping : 'fieldTypeName'}
    ]
  }),
  
  /**
   * @mode  remote
   * @table lex_analyse_type
   */
  FIELDDATATYPES : new Ext.data.JsonStore({
    storeId : 'lexFieldDatatype',
    DEFAULT : 'STRING',
    TYPES : {
      STRING   : 'STRING',
      NUMBER   : 'NUMBER',
      BOOLEAN  : 'BOOLEAN',
      DATE     : 'DATE'
    },
    autoLoad : false,
    fields : [
      {name : 'fieldDatatypeId'}
    , {name : 'fieldDatatypeCode'}
    , {name : 'fieldDatatypeName'}
    , {name : 'codeField', mapping : 'fieldDatatypeCode'}
    , {name : 'valueField', mapping : 'fieldDatatypeId'}
    , {name : 'displayField', mapping : 'fieldDatatypeName'}
    ]
  }),
  
  /**
   * @mode  remote
   */
  GEOSOURCE_METADATA : new Ext.data.JsonStore({
    storeId : 'GEOSOURCE_METADATA',
    autoLoad : false,
    fields : [
      {name : 'value'}
    , {name : 'label'}
    , {name : 'valueField', mapping : 'value'}
    , {name : 'displayField', mapping : 'label'}
    ],
    proxy: {
      type: 'rest',
      url: Routing.generate('carmen_ws_get_csw_recordstitle')
    }
  }),
  
  /**
   * @mode  remote
   * @table lex_inspire_theme
   */
  INSPIRE_THEMES : new Ext.data.JsonStore({
    storeId : 'lexInspireTheme',
    autoLoad : false,
    fields : [
      {name : 'themeId'}
    , {name : 'themeName'}
    , {name : 'valueField', mapping : 'themeName'}
    , {name : 'displayField', mapping : 'themeName'}
    ]
  }),

  /**
   * @mode  remote
   * @table lex_inspire_theme
   */
  LANGUAGES : new Ext.data.JsonStore({
    storeId : 'lexIsoLanguage',
    autoLoad : false,
    fields : [
      {name : 'languageCode'}
    , {name : 'langiageName'}
    , {name : 'valueField', mapping : 'languageCode'}
    , {name : 'displayField', mapping : 'languageName'}
    ]
  })
  
};

var LEX_ANALYSE_TYPE = Carmen.Dictionaries.ANALYSETYPES;
var LEX_GEOMETRY_TYPE = Carmen.Dictionaries.GEOMETRYTYPES;
var MS_SYMBOLES = Carmen.Dictionaries.MS_SYMBOLES;
var FIELD_DATATYPES = Carmen.Dictionaries.FIELDDATATYPES;
var LABELPOSITION = Carmen.Dictionaries.LABELPOSITION;

Ext.define('Ext.ux.TagField.Projections', {

  extend: 'Ext.form.field.Tag',
  requires: [
    'Ext.plugin.DragDropTag'
  ],
  xtype: 'tagfieldprojections',
  store: Carmen.Dictionaries.PROJECTIONS,
  plugins:'dragdroptag',
  displayField: 'displayField',
  valueField: 'valueField',
  filterPickList: true,
  queryMode : 'local'
});
Ext.define('Ext.ux.Combobox.Projections', {
  extend: 'Ext.form.field.ComboBox',
  xtype: 'comboboxprojections',
  initComponent : function(){
    Ext.apply(this, {
      store : Carmen.Dictionaries.PROJECTIONS.clone() 
    });
    this.callParent(arguments);
  },
  displayField: 'displayField',
  valueField: 'valueField',
  queryMode : 'local',
  matchFieldWidth : true,
  autoLoadOnValue: true,
  autoSelect: true,
  syncSizeToList : function(){
    var width = 0;
    this.getStore().each(function(record){
      width = Math.max(width, Ext.util.TextMetrics.measure(this.inputEl, record.get(this.displayField)).width);
    }, this);
    var old = this.suspendLayout;
    this.suspendLayout = true;
    this.setWidth(width+Ext.getScrollbarSize().width+1);
    this.suspendLayout = old;
  },
  afterComponentLayout : function(){
    var res = this.callParent(arguments);

    var me = this;
    if ( !this.getStore().loadCount ){
      this.getStore().on('load', function(){me.syncSizeToList()});
      return;
    }
    me.syncSizeToList();
    return res;
  }
});