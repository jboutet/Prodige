var ADMINCARTO_URL = String((window.CARMEN_URL_ADMIN_CARTO || window.CARMEN_URL_ADMINCARTO || window.location.origin || ''));
var ADMINCARTO_URL_JS = ADMINCARTO_URL.replace('/app_dev.php', '');

var ALL_FILES_LOADED = false;

if ( typeof Array.prototype.forEach == "undefined" ) Array.prototype.forEach = function(callback, scope){return Ext.each(this, callback, scope);}
if ( typeof RegExp.quote == "undefined" ) RegExp.quote = function(str) {
    return (str+'').replace(/[.?*+^$[\]\\(){}|-]/g, "\\$&");
};

Ext.ns("Carmen");
Ext.ns('Ext.ux');
Ext.Loader.setConfig({
  disableCaching: false
});
//Ext.Loader.syncModeEnabled = true;
if ( typeof window._t=="undefined" ){
  Ext.Loader.loadScripts({
    url : String((window.CARMEN_URL_ADMIN_CARTO || window.CARMEN_URL_ADMINCARTO || '')).replace('/app_dev.php', '')+"/vendor/i18next/i18next.min.js?v1",
    onLoad : function(){
    	if ( typeof window._t!="undefined" ) return;
      window._t = window.i18n.t;
        // i18next initialisation
      i18n.init({
        crossDomain : true,
        lng: 'fr',
        fallbackLng: 'fr',
        preload     : ['fr'],
        resGetPath: ADMINCARTO_URL_JS+'/bundles/carmenweb/locales/__lng__/translation.json',
        debug : false,
        functions : {
          ajax : function (options) {
            var failure = options.error;
            var success = options.success;
            delete options.error;
            delete options.success;
            options.failure = function(response){failure(response.responseText, response.status, null)};
            options.success = function(response){success(Ext.decode(response.responseText), response.status, null);};
            options.cors = true;
            options.withCredentials = true;
            options.method = "GET";
            if ( options.timeout===0 ) options.timeout = 24*60*60*1000;
            try {
            if ( Routing.getRoute('frontcarto_query_getinformation_proxy') ){
              options.url = Routing.generate('frontcarto_query_getinformation_proxy', {proxy_url : options.url});
            }
            Ext.Ajax.request(options);
            } catch (error){
            	Ext.Ajax.request(options);
            }
          }
        }
      });
    }
  });
}

fos.Router.countMulti = 0;
fos.Router.setDataMultiHost = function(res){
	if( !res.routes ) {
		if ( fos.Router.countMulti==0 ){
			fos.Router.countMulti++;
      Ext.Loader.loadScripts({
        url : String((window.CARMEN_URL_ADMIN_CARTO || window.CARMEN_URL_ADMINCARTO || '')).replace('/app_dev.php', '')+"/app_dev.php/js/routing?callback=fos.Router.setDataMultiHost",
        onLoad : fos.Router.setDataMultiHost
      });
		}
    return;
	}
  Ext.iterate(res.routes, function(name, route){
    route.hosttokens.push(['text', res.host]);
    Routing.d.set(name, route);
  })
  if ( !Carmen.Dictionaries.PROJECTIONS.getProxy().url ){
    Carmen.Dictionaries.PROJECTIONS.setProxy({
      withCredentials : true,
      cors : true,
      type : 'ajax',
      url : Routing.generate('carmen_ws_get_combobox', {entity:'lexProjection'})
    });
    Carmen.Dictionaries.PROJECTIONS.load();
  }
};

try {
  Ext.Loader.loadScripts({
    url : String((window.CARMEN_URL_ADMIN_CARTO || window.CARMEN_URL_ADMINCARTO || '')).replace('/app_dev.php', '')+"/js/routing?callback=fos.Router.setDataMultiHost",
    onLoad : fos.Router.setDataMultiHost
  });
} catch(error){
  Ext.Loader.loadScripts({
    url : String((window.CARMEN_URL_ADMIN_CARTO || window.CARMEN_URL_ADMINCARTO || '')).replace('/app_dev.php', '')+"/app_dev.php/js/routing?callback=fos.Router.setDataMultiHost",
    onLoad : fos.Router.setDataMultiHost
  });
}

Ext.Loader.loadScripts({
  url : String((window.CARMEN_URL_ADMIN_CARTO || window.CARMEN_URL_ADMINCARTO || '')).replace('/app_dev.php', '')+'/bundles/carmenweb/js/CarmenJS/Carmen/Ext/Util.js',
  onLoad : function(){}
});
Ext.Loader.loadScripts({
  url : String((window.CARMEN_URL_ADMIN_CARTO || window.CARMEN_URL_ADMINCARTO || '')).replace('/app_dev.php', '')+'/bundles/carmenweb/js/CarmenJS/Carmen/Ext/IdentifierVType.js',
  onLoad : function(){}
});

Ext.Loader.setPath({
    'Ext.ux.FileBrowserPanel' : String((window.CARMEN_URL_ADMIN_CARTO || window.CARMEN_URL_ADMINCARTO || '')).replace('/app_dev.php', '')+'/bundles/carmenweb/js/CarmenJS/Carmen/Ext/FileBrowser/Ext.ux.FileBrowserPanel.js',
    'Ext.ux.FileBrowserPanel' : String((window.CARMEN_URL_ADMIN_CARTO || window.CARMEN_URL_ADMINCARTO || '')).replace('/app_dev.php', '')+'/bundles/carmenweb/js/CarmenJS/Carmen/Ext/FileBrowser/Ext.ux.FileBrowserPanel.js',
    'Ext.ux.FileBrowserWindow' : String((window.CARMEN_URL_ADMIN_CARTO || window.CARMEN_URL_ADMINCARTO || '')).replace('/app_dev.php', '')+'/bundles/carmenweb/js/CarmenJS/Carmen/Ext/FileBrowser/Ext.ux.FileBrowserWindow.js',
    'Ext.ux.upload' : String((window.CARMEN_URL_ADMIN_CARTO || window.CARMEN_URL_ADMINCARTO || '')).replace('/app_dev.php', '')+'/bundles/carmenweb/js/CarmenJS/Carmen/Ext/FileBrowser/lib/upload',
    'Ext.ux.upload.uploader' : String((window.CARMEN_URL_ADMIN_CARTO || window.CARMEN_URL_ADMINCARTO || '')).replace('/app_dev.php', '')+'/bundles/carmenweb/js/CarmenJS/Carmen/Ext/FileBrowser/lib/upload/uploader',
    'Ext.ux.upload.data' : String((window.CARMEN_URL_ADMIN_CARTO || window.CARMEN_URL_ADMINCARTO || '')).replace('/app_dev.php', '')+'/bundles/carmenweb/js/CarmenJS/Carmen/Ext/FileBrowser/lib/upload/data',
    'Ext.ux.upload.header' : String((window.CARMEN_URL_ADMIN_CARTO || window.CARMEN_URL_ADMINCARTO || '')).replace('/app_dev.php', '')+'/bundles/carmenweb/js/CarmenJS/Carmen/Ext/FileBrowser/lib/upload/header'
});

Ext.onReady(function(){
	var setAjaxProperty = function(property, value){
    Ext.Ajax[property] = value;
    Ext.Ajax.config[property] = value;
    Ext.data.Connection.prototype.config[property] = value;
    Ext.data.proxy.Ajax.prototype[property] = value;
	} 
	setAjaxProperty('withCredentials', true);
  setAjaxProperty('cors', true);
	setAjaxProperty('autoAbort', true);
});


Carmen = Carmen || {};
Carmen.notAuthenticatedException = Carmen.notAuthenticatedException || Ext.emptyFn;
Carmen.Util = Carmen.Util || {};
Carmen.Util.WINDOW_WIDTH = Carmen.Util.WINDOW_WIDTH || 1000;
Carmen.Dictionaries = Carmen.Dictionaries || {};

Carmen.Defaults = Carmen.Defaults || {};
Carmen.Defaults.PROJECTION = Carmen.Defaults.PROJECTION || '2154';
Carmen.Defaults.DATATYPE_TO_STOCKAGE = Carmen.Defaults.DATATYPE_TO_STOCKAGE || {
	1 : 'vector',
	'vector' : 1
};

var DEFAULT_TITLE = '<i><b>Sélectionnez une couche à paramétrer</b></i>';
var OPEN_SERVER_BROWSER = 'OPEN_SERVER_BROWSER';
var OPEN_CLIENT_BROWSER = 'OPEN_CLIENT_BROWSER';

if ( typeof TextEncode == "undefined" ){
  function encodeToUTF8(strLatin1) {
    var strUTF8 = "";
    for (var n = 0; n < strLatin1.length; n++) {
      var c = strLatin1.charCodeAt(n);
      if (c < 128) { strUTF8 += String.fromCharCode(c); }
      else if((c > 127) && (c < 2048)) {
        strUTF8 += String.fromCharCode((c >> 6) | 192);
        strUTF8 += String.fromCharCode((c & 63) | 128);
      }
      else {
        strUTF8 += String.fromCharCode((c >> 12) | 224);
        strUTF8 += String.fromCharCode(((c >> 6) & 63) | 128);
        strUTF8 += String.fromCharCode((c & 63) | 128);
      }
    }
    return strUTF8;
  }
  function TextEncode(strParam) {
    var t = new String("test");
    if (!t.charCodeAt) return strParam;
    strParam = encodeToUTF8(strParam);
    var strEncode = "";
    for(var i=0; i<strParam.length; i++) {
      strEncode += strParam.charCodeAt(i).toString(16);
    }
    return strEncode;
  }
}

Ext.Base.addMembers({
  callNative : function(args)
  {
    var method;
    // This code is intentionally inlined for the least amount of debugger stepping 
    return (method = this.callNative.caller) && (method.$previous ||
          ((method = method.$owner ? method : method.caller) &&
                method.$owner.self[method.$name])).apply(this, args || noArgs);
  }
})
function booleanToInt(value){return (value ? "1" : "0");}
function intToBoolean(value){return (value=="1");}

CarmenImportData = window.CarmenImportData || {};
CarmenImportData.verifyIntegrity = false;

var UNSAVED_MARK = " *";

var FILETYPES = {
  VECTOR : {
    selectExtension: '*.'+['shp', 'mif','tab','gxt', 'json', 'gml', 'gmt', 'kml'].join(',*.'),
    uploadExtension: '.'+['shp', 'prj', 'dbf', 'shx', 'qpj', 'mid', 'mif', 'tab', 'id', 'ind', 'map', 'dat','gxt', 'json', 'gml', 'gmt', 'kml'].join(',.'),
    readFile : true
  },
  GEOREFERENCED : {
    selectExtension: '*.csv,*.ods,*.xls,*.xlsx',
    uploadExtension: '.csv,.ods,.xls,.xlsx',
    readFile : true
  },
  TABULAIRE : {
    selectExtension: '*.csv,*.ods,*.xls,*.xlsx',
    uploadExtension: '.csv,.ods,.xls,.xlsx',
    readFile : true
  },
  MNT : {
  	selectExtension: '*.ASC,*.XYZ',
    uploadExtension: '.ASC,.XYZ',
    readFile : true
  },
  RASTER : {
    selectExtension: '*.gtiff,*.tiff,*.tif,*.ecw',
    uploadExtension: '.gtiff,.tiff,.tif,.ecw,.tab,.tfw',
    readFile : false
  },
  TILEINDEX : {
    selectExtension: '*.shp',
    uploadExtension: '.shp,.prj,.dbf,.shx,.qpj',
    readFile : false
  },
  ZIP : {
    selectExtension: '*.zip',
    uploadExtension: '.zip',
    readFile : true
  }
}

function positionMarkStoreTypes(){/*Marqueur de position pour le code global*/}
Ext.define('CarmenImportData.data.JsonMemoryStore', {
  extend : 'Ext.data.Store',
  proxy: {
    type: 'memory',
    reader: {
      type: 'json'
    }
  }
});
Ext.define('CarmenImportData.data.TreeStore', {
  extend : 'Ext.data.TreeStore',
  autoLoad : false,
  //root : Ext.create('Ext.data.TreeModel', {id:'root', expanded: true}),
  proxy: {
    timeout :  24*60*60*1000,
    type: 'ajax',
    withCredentials : true,
    cors : true,
    url : document.location.href+"/json",
    reader: {
      withCredentials : true,
      cors : true,
      type: 'json',
      typeProperty: 'mtype'
    }
  }
});

function positionMarkFieldContainer(){/*Marqueur de position pour le code global*/}
Ext.define('CarmenImportData.form.FieldContainer', {
  extend : 'Ext.form.FieldContainer',
  disabledCls : 'x-item-disabled',
  xtype : 'cid-fieldcontainer',
  setDisabled: function(disabled){
    this.callParent(arguments);
    this[(disabled ? "addCls" : "removeCls")]("x-item-disabled");
  }
});

function positionMarkDictionaries(){/*Marqueur de position pour le code global*/}

var urlProjection;
try {
	urlProjection = Routing.generate('carmen_ws_get_combobox', {entity:'lexProjection'})
} catch (error){urlProjection = null;}

Carmen.Dictionaries.PROJECTIONS = Carmen.Dictionaries.PROJECTIONS || new Ext.data.JsonStore({
	local : true,
	proxy : {
    withCredentials : true,
    cors : true,
    type : 'ajax',
    url : urlProjection
  },
  autoLoad : false,
  fields : [
    {name : 'projectionEpsg'}
  , {name : 'projectionName'}
  , {name : 'valueField', mapping : 'projectionEpsg'}
  , {name : 'displayField', calculate : function(data){return data.projectionName+' (EPSG:'+data.projectionEpsg+')'}}
  ]
});

var projection_source = Ext.create('CarmenImportData.data.JsonMemoryStore', {
  fields: ['valueField', 'displayField'],
  data : [{valueField : 'auto', displayField : 'Détection Automatique'}]//.concat(Carmen.Dictionaries.PROJECTIONS.getRange().getValues('data'))
})
function setRecordsProjection(store){
  projection_source.add(store.getData().getValues('data'));
}
if ( !Carmen.Dictionaries.PROJECTIONS.loadCount )
  Carmen.Dictionaries.PROJECTIONS.on('load', setRecordsProjection)
else 
  setRecordsProjection(Carmen.Dictionaries.PROJECTIONS);
  
if ( Carmen.Dictionaries.PROJECTIONS.local && Carmen.Dictionaries.PROJECTIONS.getProxy().url ){
	Carmen.Dictionaries.PROJECTIONS.load();
}
        
CarmenImportData.Datatypes = {
  enums : {
    vector : 'vector', 
    raster : 'raster', 
    tabulaire : 'tabulaire', 
    join : 'join',
    mnt : 'mnt',
    multi : 'multi'
  },
  //NB : value of enums is the key for labels
  labels : {
    vector : 'Vectoriel',
    raster : 'Raster' ,
    tabulaire : 'Table attributaire',
    join : 'Jointure - Vue',
    mnt : 'MNT',
    multi : 'Ensemble de série de données'
  },
    
  getLabel : function(intValue){
    return CarmenImportData.Datatypes.labels[intValue] || 'Inconnu';
  },
  
  getStore: function(valueField, displayField){
  	var key = ['carmenimportdata-datatypesstore', valueField, displayField].join('-');
  	var store = Ext.StoreMgr.lookup(key) || Ext.create('Ext.data.Store', {id : key, fields : [valueField, displayField], data : this.getDataset(valueField, displayField)});
    return store;
  },
  
  getDataset: function(valueField, displayField){
    var results = []; var item;
    Ext.iterate(CarmenImportData.Datatypes.labels, function(value, text){
      item = {};
      item[valueField] = value;
      item[displayField] = text;
      results.push(item);
    });
    return results;
  }
  
};


Carmen.Dictionaries.FIELDDATATYPES = Ext.create('CarmenImportData.data.JsonMemoryStore', {
  TYPES : {
    STRING : ['text'],
    NUMBER : ['int4', 'float'],
    DATE : ['date']
  },
  fields : ['displayField', 'codeField'], 
  data : [{
    displayField : 'Texte', codeField : 'text'
  },{
    displayField : 'Nombre entier', codeField : 'int4'
  },{
    displayField : 'Nombre décimal', codeField : 'float'
  },{
    displayField : 'Date', codeField : 'date'
  }]
});



function positionMarkModels(){/*Marqueur de position pour le code global*/}
Ext.define('CarmenImportData.model.ChampDonnee', {
  extend: 'Ext.data.Model',
  fields: [
  {name : 'id', persist:false},
  {name : 'field_name', type : 'string'},
  {name : 'field_datatype', type : 'string'
  , convert : function(value){return value;
      switch(value){
        case 'text' : return FIELD_DATATYPES.TYPES.STRING; 
        case 'integer' : case 'float' : return FIELD_DATATYPES.TYPES.NUMBER; 
        case 'boolean' : return FIELD_DATATYPES.TYPES.BOOLEAN; 
        case 'date' : return FIELD_DATATYPES.TYPES.DATE; 
        default : return (value ? value.toUpperCase() : FIELD_DATATYPES.TYPES.STRING);
      }
    }},
  {name : 'field_index', type : 'integer'},
  {name : 'field_visible', type : 'boolean', defaultValue : true},
  {name : 'xField', type : 'boolean', defaultValue : false},
  {name : 'yField', type : 'boolean', defaultValue : false}
  ]
});


function getCommonFields(fNom, fDescription, fStockage, fWms, fWfs, fStatus){
  var fields = ['nom', 'description', 'stockage', 'wms', 'wfs', 'status'];
  var args = Array.prototype.slice.call(arguments, 0);
  var res = [];
  fields.forEach(function(field, index){
    var translation = args[index];
    var translation_name = translation;
    if ( Ext.isObject(translation) ) {
      translation_name = translation.name;
      delete translation.name;
    } else {
      translation = {};
    }
      
    var column = Ext.apply({name : field, type : 'string'}, translation);
    if ( translation_name==null ){return;
      column.defaultValue = "";
    } else {
      column.depends = [translation_name];
      column.calculate = function(data){return data[translation_name];};
    }
    res.push(column);
  });
  return res;
}

Ext.define('CarmenImportData.model.Metadata', {
  extend: 'Ext.data.TreeModel',
  idProperty : 'id',
  entityName: 'Metadata',
  fields: [
    {name : 'uuid',                   type : 'string'}
  , {name : 'metadata_title',         type : 'string'}
  , {name : 'metadata_desc',          type : 'string'}
  , {name : 'metadata_type',          type : 'string'}
  , {name : 'metadata_datatype',          type : 'string'}
  , {name : 'metadata_status',          type : 'string', defaultValue : "Métadonnée enfant de l'ensemble de série de données"}
  ].concat(
  getCommonFields('metadata_title', 'metadata_desc', 'uuid', null, null, 'metadata_status')),
  
  getTypePath : function(){
    return (this.isRoot() ? [] : ['Métadonnée #'+parseInt(this.parentNode.indexOf(this)+1)]);
  }
});
Metadata = CarmenImportData.model.Metadata;

Ext.define('CarmenImportData.model.CoucheDonnees', {
  extend: 'Ext.data.TreeModel',
  idProperty : 'pk_couche_donnees',
  entityName: 'CoucheDonnees',
  expanded : true,
  leaf : true,
  loaded : true,
  fields: [
    {name : 'pk_couche_donnees',                     type : 'integer', convert : function(value){return Ext.Number.from(value, 0);}, serialize : function(value){return Ext.Number.from(value, 0);}}
  , {name : 'fmeta_fk_couche_donnees',               type : 'integer', calculate : function(data){return data.pk_couche_donnees}}
  , {name : 'fmeta_description',                     type : 'string' , defaultValue : ''}
  
  , {name : 'couchd_id',                             type : 'string', defaultValue : ''}
  , {name : 'couchd_nom',                            type : 'string', defaultValue : ''}
  , {name : 'couchd_description',                    type : 'string', defaultValue : ''}
  , {name : 'couchd_type_stockage',                  type : 'integer', defaultValue : 1
    , convert : function(value){
        if ( !Ext.isNumber(parseInt(value)) ) 
          return parseInt(Carmen.Defaults.DATATYPE_TO_STOCKAGE[value]);
        return parseInt(value);
      }
    , serialize : function(value, record){
        return parseInt(Carmen.Defaults.DATATYPE_TO_STOCKAGE[record.getLayerType()]);
      }}
  , {name : 'couchd_emplacement_stockage',           type : 'string', defaultValue : ''}
  , {name : 'original_couchd_emplacement_stockage',  type : 'string', mapping : 'couchd_emplacement_stockage'}
  , {name : 'couchd_fk_acces_server',                type : 'integer', defaultValue : 1}
  , {name : 'couchd_wms',                            type : 'boolean', defaultValue : false, convert : intToBoolean, serialize : booleanToInt}
  , {name : 'couchd_wfs',                            type : 'boolean', defaultValue : false, convert : intToBoolean, serialize : booleanToInt}
  , {name : 'couchd_download',                       type : 'boolean', defaultValue : false, convert : intToBoolean, serialize : booleanToInt}
  , {name : 'couchd_restriction',                    type : 'boolean', defaultValue : false, convert : intToBoolean, serialize : booleanToInt}
  , {name : 'couchd_restriction_exclusif',           type : 'boolean', defaultValue : false, convert : intToBoolean, serialize : booleanToInt}
  , {name : 'couchd_zonageid_fk',                    type : 'string', defaultValue : ''}
  , {name : 'couchd_restriction_buffer',             type : 'string', defaultValue : ''}
  , {name : 'couchd_extraction_attributaire',        type : 'boolean', defaultValue : false, convert : intToBoolean, serialize : booleanToInt}
  , {name : 'couchd_extraction_attributaire_champ',  type : 'string', defaultValue : ''}
  , {name : 'couchd_alert_msg_id',                   type : 'boolean', defaultValue : false, convert : intToBoolean, serialize : booleanToInt}
  , {name : 'layerfields'
    , defaultValue : Ext.create('CarmenImportData.data.JsonMemoryStore', {model : 'CarmenImportData.model.ChampDonnee'})
    , convert : function(value){
        value = value || [];
        if ( value instanceof Array ){
          value = Ext.create('CarmenImportData.data.JsonMemoryStore', {model : 'CarmenImportData.model.ChampDonnee', data : value});
        }
        value.each(function(record){record.widgets = null;});
        return value;
      }
    , serialize : function(value, couchd){
        value = value || [];
        var serialize = {drops : [], types : [], xField:null, yField: null};
        if ( !(value instanceof Array) ){
          value = value.getRange();
        }
        Ext.each(value, function(fieldRecord){
          if ( !fieldRecord.get('field_visible') ){
            serialize.drops.push(fieldRecord.get('field_name'))
          } else {
            serialize.types.push(fieldRecord.getData({persist:true, serialize : true}));
            if ( couchd.get('file_georeferenced') && fieldRecord.get('xField') ) serialize.xField = fieldRecord.get('field_name');
            if ( couchd.get('file_georeferenced') && fieldRecord.get('yField') ) serialize.yField = fieldRecord.get('field_name');
          }
        });
        return serialize;
      }
  }
  , {name : 'file_georeferenced',             type : 'boolean', defaultValue : false}
  , {name : 'file_name',                      type : 'string', defaultValue : '', persist : false}
  , {name : 'file_source',                    type : 'string', defaultValue : ''}
  , {name : 'autoLoad',                       type : 'boolean', defaultValue : false}
  , {name : 'file_sheet',                     type : 'string', defaultValue : ''}
  , {name : 'sheets'
    , defaultValue : Ext.create('CarmenImportData.data.JsonMemoryStore', {fields : ['sheet_index', 'sheet_name'], data : []})
    , convert : function(value){
        value = value || [];
        if ( value instanceof Array ){
          value = Ext.create('CarmenImportData.data.JsonMemoryStore', {fields : ['sheet_index', 'sheet_name'], data : value});
        }
        return value;
      }
    , persist : false
    }
  , {name : 'encoding_detected',                type : 'string', defaultValue : 'UTF-8', persist:false}
  , {name : 'encoding_source',                type : 'string', defaultValue : 'auto', serialize : function(value, record){
      if ( value=="auto" ) return record.get('encoding_detected');
      return value;
    }}
  , {name : 'projection_source',              type : 'string', defaultValue : 'auto'}
  , {name : 'projection_cible',               type : 'string', defaultValue : Carmen.Defaults.PROJECTION}
  , {name : 'unique_fields'                  
    , defaultValue : Ext.create('CarmenImportData.data.JsonMemoryStore', {model : 'CarmenImportData.model.ChampDonnee'})
    , convert : function(value, record){
        value = value || [];
        if ( value instanceof Array ){
        	if ( Ext.isEmpty(value) ){
        	  value = Ext.create('CarmenImportData.data.JsonMemoryStore', {model : 'CarmenImportData.model.ChampDonnee'});
        	} else {
            value = Ext.create('CarmenImportData.data.JsonMemoryStore', {model : 'CarmenImportData.model.ChampDonnee', data : value});
        	}
        }
        return value;
      }
    , serialize : function(value){
        if ( value instanceof Ext.data.Store ){return null;}
        return value;
      }
    }
  , {name : 'field_key',                     type : 'string'}
  , {name : 'import_status',                 type : 'string',  defaultValue : 'Configuration à faire', persist : false}
  , {name : 'import_action',                 type : 'string',  defaultValue : 'create'}
  , {name : 'leaf',                          type : 'boolean', defaultValue : true}
  , {name : 'synchronisation_mapfiles',        type : 'boolean', defaultValue : false}
  ].concat(getCommonFields('couchd_nom', 'couchd_description', 'couchd_emplacement_stockage', 'couchd_wms', 'couchd_wfs', 'import_status')),
  
  getLayerType : function(){
  	var cmp = Ext.getCmp("metadata_layertype");
    return (!this.parentNode || this.parentNode.isRoot()) ? (cmp ? cmp.getValue() : 'vector') : this.parentNode.get('metadata_datatype');
  },
  getUUID : function(){
    return (!this.parentNode || this.parentNode.isRoot()) ? Carmen.metadata.uuid : this.parentNode.get('uuid');
  },
  getMetadataId : function(){
    return (!this.parentNode || this.parentNode.isRoot()) ? Carmen.metadata.id : this.parentNode.get('id');
  },
  getTypePath : function(){
    return this.parentNode ? this.parentNode.getTypePath().concat(['Donnée #'+parseInt(this.parentNode.indexOf(this)+1)]) : "";
  }
});
CoucheDonnees = CarmenImportData.model.CoucheDonnees;


function positionMarkLibrary(){/*Marqueur de position pour le code global*/}
/**
 * Liste des fonctions appliquées sur les composants 
 * @type Object
 */
CarmenImportDataLibrary = {
  saveLayerConfiguration : function(reset){
    if (!Ext.isBoolean(reset)) reset = true;
    var formGrid = this;
    var form = (formGrid instanceof Ext.form.Panel ? formGrid : formGrid.getForm());
    if ( !form ) return false;
    if ( !form.getRecord() ) return false;
    if ( !form.isValid() ) {
      var errors = [];
      var glue = '';
      form.getForm().getFields().each(function(field){
        if ( !field.validate() /*&& (field.isDisabled && !field.isDisabled()) && (field.isReadOnly && !field.isReadOnly())*/ ){
          var previousNode;
          var label = field.boxLabel || field.fieldLabel
                   || (field.is('field')
                     ? field.up().boxLabel || field.up().fieldLabel
                       || ((previousNode = field.previousNode('[fieldLabel], [boxLabel]')) ? previousNode.boxLabel || previousNode.fieldLabel: field.name)
                     : '');
          if (!label)  return;
          errors.push( [glue, label, ' : ', field.getActiveErrors().join(',<br>')].join('') );
          glue = '<br/><br/>'; 
        }
      })
      if ( errors.length ){
        Ext.Msg.alert("Erreurs rencontrées", "Des erreurs existent sur le(s) champ(s) : <li>"+errors.join('</li><li>')+"</li>");
      }
      return false;
    }

    form.getRecord().set('synchronisation_mapfiles', false, {commit:true,silent:true});
    if ( CarmenImportData.verifyIntegrity ){
      var tmp_record = Ext.create(formGrid.getLayer().$className);
      form.updateRecord(tmp_record); 
      tmp_record.set('pk_couche_donnees', formGrid.getLayer().get('pk_couche_donnees'), {commit:true});
      tmp_record.parentNode = formGrid.getLayer().parentNode;
      if ( !CarmenImportData.library.verifyIntegrity.call(formGrid, tmp_record, form, reset) ){
        //aucune vérification à faire
        CarmenImportData.library.updateLayerPostSubmit.call(formGrid, form, reset);
      }
    } else {
      CarmenImportData.library.updateLayerPostSubmit.call(formGrid, form, reset);
    }
    return true;
  },
  
  updateLayerPostSubmit : function(form, reset){
    var formGrid = this;
    var record = form.getRecord();
    if ( !record ){
    	if ( reset )CarmenImportData.library.loadLayer.call(formGrid, null);
    	return;
    }
    
    form.updateRecord(record); 
    
    var status = record.get('import_status').split(/<br\/?>/);
    if ( record.get('import_status').indexOf("Une erreur s'est produite")!=-1 )
      status = [];
    var keep = "import";
    var modif = 'Configuration modifiée';
    var old_modif = status.indexOf(modif)!=-1;
    var tmp = [];
    for (var i=0; i<status.length; i++){
      if ( status[i].toLowerCase().indexOf(keep)==0 && status[i].toLowerCase().indexOf("importation")==-1 && status[i].toLowerCase().indexOf("(")==-1){
        tmp.push(status[i]);
      }
//      if ( status[i].toLowerCase().indexOf("configuration")==-1 && ' '+status[i].toLowerCase().indexOf(" erreur ")==-1 ){
//        tmp.push(status[i]);
//      }
    }
    if ( old_modif || form.isDirty() ) 
      tmp.push(modif);
      
    record.set('import_status', tmp.join('<br/>'));
    record.set('color', '');
    record.set('autoLoad', false);
    record.commit();
    
    if ( reset )CarmenImportData.library.loadLayer.call(formGrid, null);
  },
  
  verifyIntegrity : function(record, form, reset){
    var formGrid = this;
    if ( ["vector", "tabulaire"].indexOf(record.getLayerType().toLowerCase())==-1 ) return false;
    if ( Ext.isEmpty(record.get('file_source')) ) return false;
    if ( record.get('import_action')=="create" ) return false;
    if ( Ext.Number.from(record.get('pk_couche_donnees'), 0)==0 ) return false;
    
    var data = record.getData({persist : true, serialize : true});
    
    var etape = "Contrôle d'intégrité des structures";
    formGrid.mask(etape+"...");
    Ext.Ajax.request({
      withCredentials : true,
      cors : true,
      method : 'POST',
      type : 'json',
      url : Routing.generate('carmen_ws_verifyintegrity'+(formGrid.suffixRoute||''), {
        uuid : record.getUUID(),
        layertype : record.getLayerType().toUpperCase()
      }),
      jsonData : data,
      success : function(response){
        response = Ext.decode(response.responseText);
        var msg = Ext.Msg[response.success ? "alert" : "confirm"](
          etape, 
          response.message + (response.success ? "" : "<br><br>Les données seront supprimées et remplacées. <br>Souhaitez-vous poursuivre l'importation des données ?"), 
          function(btn){
            formGrid.unmask();
            if ( response.success ){
              CarmenImportData.library.updateLayerPostSubmit.call(formGrid, form, reset);
            } 
            else if ( ["yes", "ok"].indexOf(btn)!=-1 ){
              CarmenImportData.library.verifyLayerInMap.call(formGrid, record, form, reset);
            }else {
              if ( reset )CarmenImportData.library.loadLayer.call(formGrid, null);
            }
          }
        ).setIcon(response.success ? Ext.Msg.INFO : Ext.Msg.ERROR);
        if ( response.success ) Ext.Function.defer(function(){ msg.onClose(); }, 2000);
      },
      failure : function(response){
        formGrid.unmask();
        Ext.Msg.alert(etape, "Une erreur s'est produite durant le "+etape.toLowerCase()+" : "+response.responseText).setIcon(Ext.Msg.ERROR); 
      }
    })
    return true;
  },
  
  verifyLayerInMap : function(record, form, reset){
    var formGrid = this;
    var etape = "Recherche des cartes contenant cette couche";
    formGrid.mask(etape+"...");
    Ext.Ajax.request({
      withCredentials : true,
      cors : true,
      url : Carmen.Defaults.CATALOGUE_URL.replace(/\/$/g, "") + "/geosource/mapfilesforlayer/"+record.get('couchd_emplacement_stockage')+'/' + record.get('couchd_type_stockage'),
      success : function(response){
        formGrid.unmask();
        response = Ext.decode(response.responseText);
        var mapfiles = [];
        Ext.iterate(response.mapfiles, function(mapfile, layerName){mapfiles.push(mapfile);});
        if ( mapfiles.length ){
          Ext.Msg.confirm(etape, 
            "Les cartes suivantes contiennent la couche :<br/><li>" + mapfiles.join('.map</li><li>')+'.map</li>'+ 
            "<br>La couche sera supprimée des cartes citées. <br>Souhaitez-vous poursuivre l'importation des données ?",
            function(btn){
              if ( btn=="yes" ){
                form.getRecord().set('synchronisation_mapfiles', true, {commit:true,silent:true});
                //CarmenImportData.library.deleteLayerFromMaps.call(formGrid, record, form, reset);
                CarmenImportData.library.updateLayerPostSubmit.call(formGrid, form, reset);
              } else {
                if ( reset )CarmenImportData.library.loadLayer.call(formGrid, null);
              }
            }
          )
        } else {
          var msg = Ext.Msg.alert(etape, '<b>Aucune carte</b> ne contient cette couche.<br>La validation du paramétrage se poursuit.').setIcon(Ext.Msg.INFO);
          Ext.Function.defer(function(){ msg.onClose(); }, 2000);
          CarmenImportData.library.updateLayerPostSubmit.call(formGrid, form, reset);
        }
      },
      failure : function(response){
        formGrid.unmask();
        Ext.Msg.alert(etape, "Une erreur s'est produite durant la "+etape.toLowerCase()+" : "+response.responseText).setIcon(Ext.Msg.ERROR); 
      }
    })
  },
  
  cancelLayerConfiguration : function(){
    Ext.Ajax.abortAll();
    var formGrid = this;
    var form = formGrid.getForm();
    if ( !form ) return;
    CarmenImportData.library.loadLayer.call(formGrid, null);
  },
  
  
  readSelectedFile : function(wind, fileRecord, fileName, fullPathName, relativePath, absolutePath, fileSheet){
    var formGrid = this;
    if ( !Ext.isDefined(fileSheet) || !Ext.isString(fileSheet) ) fileSheet = null;
    
    var ctrlTo = formGrid.down('[name=file_name]');
    ctrlTo.setValue(fullPathName);
    var ctrlTo = formGrid.down('[name=file_source]');
    relativePath = decodeURIComponent(relativePath);
    ctrlTo.setValue(relativePath);
    wind && wind.close();
    
    var layertype = formGrid.getLayer().getLayerType().toUpperCase();
    if ( !FILETYPES[layertype].readFile ) return;
    if ( relativePath=="" || relativePath==null ) return;
    
    formGrid.down('[name=layerfields]  grid').mask('Détection des champs par analyse du fichier...');
    
    var data = formGrid.getLayer().getData({persist : true, serialize : true});
    
    if ( Ext.getCmp('showData') ) Ext.getCmp('showData').close();
    
    Ext.data.JsonP.request({
    	timeout :  24*60*60*1000,
      withCredentials : true,
      cors : true,
      method : 'GET',
      type : 'json',
      url : Routing.generate('carmen_ws_readfile'+(formGrid.suffixRoute||''), {
        pk_couche_donnees : data.pk_couche_donnees,
        couchd_type_stockage : data.couchd_type_stockage,
        layertype : layertype, 
        layerfile : encodeURIComponent(relativePath),
        layersheet : (fileSheet=="" ? null : fileSheet)
      }),
      success : function(response){
      	if ( !response ) return;
      	var fieldsgrid = formGrid.down('[name=layerfields]  grid');
      	if ( response.error ){
      		fieldsgrid.getView().emptyText = '<div class="x-grid-empty import-data-status red"><b>Problème de lecture du fichier</b><br/>'+response.error+'</div>';
      		formGrid.down('[name=layerfields]').setValue([]);
      		fieldsgrid.unmask();
      		return;
      	}

        response.encoding && formGrid.down('[name=encoding_detected]').setValue(response.encoding);
        formGrid.down('[name=file_source]').setValue(response.layerfile);
        formGrid.down('[name=sheets]').setValue(response.sheets, function(){
        	
          fieldsgrid.getView().emptyText = '<div class="x-grid-empty">Aucun champ détecté</div>';
          
          var xField = null, yField = null;
          if ( formGrid.down('[name=file_georeferenced]').getValue() ){
          	xField = fieldsgrid.getStore().findRecord('xField', true);
          	yField = fieldsgrid.getStore().findRecord('yField', true);
          }
          
          if ( response.sheets.length>0 ){
          	if ( fileSheet ){
              formGrid.down('[name=file_sheet]').suspendEvents(false);
              formGrid.down('[name=file_sheet]').setValue(fileSheet);
              formGrid.down('[name=file_sheet]').resumeEvents(false);
              
              formGrid.down('[name=layerfields]').setValue(response.layerfields, function(){
                if ( xField ){
                  var theRecord = fieldsgrid.getStore().findRecord('field_name', xField.get('field_name'));
                  if ( theRecord ) theRecord.set('xField', true, {commit:true});
                }
                if ( yField ){
                	var theRecord = fieldsgrid.getStore().findRecord('field_name', yField.get('field_name'));
                	if ( theRecord ) theRecord.set('yField', true, {commit:true});
                }
              });
              fieldsgrid.unmask();
          	} else {
              fieldsgrid.getView().emptyText = '<div class="x-grid-empty">Recherche des champs du fichier</div>';
              formGrid.down('[name=file_sheet]').setValue(response.sheets[0].sheet_name);
          	}
          } else {
            formGrid.down('[name=layerfields]').setValue(response.layerfields);
            fieldsgrid.unmask();
          }
          
          formGrid.readSelectedFile && formGrid.readSelectedFile(response);
          
          if ( response.import_action ){
            var import_actions = response.import_action.split(',');
            var current_action = formGrid.down('[name=import_action][value=true]');
            if ( current_action ){
              if ( !current_action.isVisible() || current_action.isDisabled() )
                current_action = null;
              else
                current_action = current_action.inputValue;
            } 
            else current_action = null;
            var available_actions = ['create', 'update', 'replace'];
            if (formGrid.getLayer() && !formGrid.getLayer().get('unique_fields').getCount()){ Ext.Array.remove(import_actions, 'update'); }
            
            Ext.each(available_actions, function(action){
              var cmp = Ext.getCmp('import_action_'+action);
              if ( !cmp ) return;
              cmp.setDisabled(false);
              if ( import_actions.indexOf(action)==-1 ){
                cmp.setDisabled(true);
              } else if ( import_actions.indexOf(action)==0 ){ 
                cmp.setValue(true);
              }
            })
            if ( current_action && import_actions.indexOf(current_action)!=-1 ){ 
              var cmp = Ext.getCmp('import_action_'+current_action);
              cmp && cmp.setValue(true);
            }
          }
        });
      },
      failure : function(){
        formGrid.down('[name=layerfields]  grid').unmask();
      }
    });
  }
} ;
CarmenImportData.library = Ext.apply(CarmenImportData.library||{}, CarmenImportDataLibrary);

function positionMarkConfigurator(){/*Marqueur de position pour le code global*/}
Ext.define('CarmenImportConfigurator', {
  requires : [
  'Ext.ux.FileBrowserPanel',
  'Ext.ux.FileBrowserWindow'
  ],
	extend : 'Ext.panel.Panel',
	xtype : 'carmenimportconfigurator',
	layertype : 'vector',
	formGrid : null,
  suffixRoute : '',
	fileBrowserAction : 'openServerBrowser',
	
    
  getFileBrowserOptions : function(layertype, forBrowser, forUploader){
  	var me = this;
    var store = null;
    var listeners = null;
    var moreCmps = [];
    switch ( layertype ){
      case "VECTOR" : 
        store = Ext.create('Ext.data.JsonStore', {
          proxy : {type:'memory', reader : {type:'json'}},
          fields : ['valueField', 'displayField', 'extensions'],
          data : [{
            valueField : false,
            displayField : 'Données vectorielles',
            extensions: FILETYPES[layertype]
          }, {
            valueField : true,
            displayField : 'Données tabulaires avec coordonnées',
            extensions: FILETYPES["GEOREFERENCED"]
          }]
        });
      break;
      case "RASTER" : 
        store = Ext.create('Ext.data.JsonStore', {
          proxy : {type:'memory', reader : {type:'json'}},
          fields : ['valueField', 'displayField', 'extensions'],
          data : [{
            valueField : false,
            displayField : 'Fichier raster',
            extensions: FILETYPES[layertype]
          }, {
            valueField : true,
            displayField : 'Tuilage raster',
            extensions: FILETYPES["TILEINDEX"]
          }]
        });
        listeners = {
          select : function(browser, combo, value){
            var record = combo.getSelection();
            if ( !record ) return;
            browser.fileBrowser.setExtensions(record.get('extensions'));
            var tileindex = combo.nextSibling('[name=tileindex]');
            tileindex && tileindex.setVisible(!record.get('valueField'));
            combo.up('window').un('close', combo.onWindowClose, combo);
            combo.up('window').on('close', combo.onWindowClose, combo);
            
          },
          afterrender : function(combo){
            this.fireEvent('select', combo, combo.getSelection());
          }
        };
        !forUploader && moreCmps.push({
        	labelWidth : 200,
        	width : '100%',
          layout: {
            type: 'hbox',
            pack: 'end',
            align: 'stretchmax'
          },
        	xtype : 'fieldcontainer',
        	name : 'tileindex',
          fieldLabel : "Index de tuilage pour le répertoire",
        	items : [{
        		xtype : 'textfield',
        		vtype : 'identifier',
        		width : 150
        	}, {
            xtype : 'label',
            text : '.shp',
            margin : '5 5 0 2'
        	}, {
            xtype : 'combo',
            name : 'raster_extension',
            queryMode : 'local',
            store : {fields : ['valueField', 'displayField'], data : [['_first_', "Tous les fichiers"]].concat(Ext.Array.map (FILETYPES["RASTER"].selectExtension.split(','), function(v){return [v.replace(/\*\./g, ''),v];}))},
            displayField : 'displayField',
            valueField : 'valueField',
            margin : '0 5 0 0',
            width : 140,
            value : '_first_'
          }, {
        		xtype : 'button',
        		text : "Générer et sélectionner le fichier d'index",
        		handler : function(btn){
        			var textfield = btn.previousSibling('textfield:not(combo)');
        			if ( !textfield ) return;
        			if ( !textfield.getValue() ) return;
        			Ext.Ajax.request({
        				method : 'GET',
        				url : Routing.generate('carmen_generaterastertileindex'+(me.suffixRoute||''), {
                  extension : btn.previousSibling('combo').getValue(),
                  shapefile : textfield.getValue(),
                  directory : btn.up('ux-filebrowserpanel').getCurrentPath().replace(/\/Publication\/?/, "") 
                }),
                success : function(response){
                  try {
                    response = Ext.decode(response.responseText);
                    var file_source = response.file_source;
                    var success = response.success;
                    var failures = response.failures;
                    var logs = response.logs;
                    var isSuccess = success.length>0;
                    var s_title = "Génération du fichier d'index de tuilage raster";
                    var s_name = "Le fichier de tuilage "+file_source;
                    
                    if ( !isSuccess ){
                      Ext.Msg.alert(
                        s_title, 
                        s_name+" n'a pas été généré." +
                        '<div style="height:200px;overflow:auto">'  + 
                        '<b>Messages d\'erreurs</b><br/>' + logs +
                        '</div>' 
                      ).setIcon(Ext.Msg.ERROR);
                    	
                    } 
                    else if ( failures.length>0 ){
                      Ext.Msg.confirm(
                        s_title, 
                        s_name+' a été généré avec des erreurs.'+
                        '<div style="height:200px;overflow:auto">' +
                        '<br><b>Erreurs sur les fichiers</b> : <li>'+failures.join('</li><li>')+'</li>'+
                        '<br><b>Succès sur les fichiers</b> : <li>'+success.join('</li><li>')+'</li>'+
                        '<br><br>'+
                        '<b>Messages d\'erreurs</b><br/>' + logs +
                        '</div>' +
                        '<br><br>' +
                        'Voulez-vous utilisez le fichier généré ?',
                        function(confirm){
                        	if ( confirm!="yes" ) return;
                          me.formGrid.down('[name=file_source]').setValue(file_source.replace("/Publication/", ""));
                          me.formGrid.down('[name=file_name]').setValue(file_source.replace("/Publication/", ""));
                        	var file_georeferenced = btn.up('window').down('combo[name=file_georeferenced]');
                        	file_georeferenced && file_georeferenced.setValue(true);
                        	btn.up('window').close();
                        }
                      ).setIcon(Ext.Msg.WARNING);
                    } else {
                      Ext.Msg.alert(
                        s_title, 
                        s_name+' a été généré avec succès pour les fichiers <li>'+success.join('</li><li>')+'</li>',
                        function(){
                          me.formGrid.down('[name=file_source]').setValue(file_source.replace("/Publication/", ""));
                          me.formGrid.down('[name=file_name]').setValue(file_source.replace("/Publication/", ""));
                          var file_georeferenced = btn.up('window').down('combo[name=file_georeferenced]');
                          file_georeferenced && file_georeferenced.setValue(true);
                          btn.up('window').close();
                        }
                      ).setIcon(Ext.Msg.INFO);
                    }
                  } catch(error){}
                },
                failure : function(response){
                  try {
                    response = Ext.decode(response.responseText);
                    Ext.Msg.alert("Génération du fichier d'index de tuilage raster", response.error).setIcon(Ext.Msg.ERROR);
                  } catch(error){}
                  
                }
        			})
        		}
        	}]
        });
      break;
      case "MNT" : 
        store = Ext.create('Ext.data.JsonStore', {
          proxy : {type:'memory', reader : {type:'json'}},
          fields : ['valueField', 'displayField', 'extensions'],
          data : [{
            valueField : false,
            displayField : "Fichiers : Sélection d'un ou plusieurs fichiers MNT",
            extensions: ['files', true]
          }, {
            valueField : true,
            displayField : "Répertoire : Sélection d'un répertoire contenant les fichiers MNT",
            extensions: ['folders', false]
          }]
        });
        listeners = {
        	select : function(browser, combo, value){
            var record = combo.getSelection();
            if ( !record ) return;
            browser.fileBrowser.setDataSet.apply(browser.fileBrowser, record.get('extensions')/*it is an array*/);
            combo.up('window').un('close', combo.onWindowClose, combo);
            combo.up('window').on('close', combo.onWindowClose, combo);
        	},
        	afterrender : function(combo){
        		this.fireEvent('select', combo, combo.getSelection());
        	}
        }
      break;
    }
    if ( !store ) return null;
    var labels = {
      vector : 'Données tabulaires<br/>avec coordonnées'+UNSAVED_MARK,
      raster : 'Tuilage raster'+UNSAVED_MARK,
      mnt : "Niveau de sélection"
    }
    var currentValue = me.down('checkbox[name=file_georeferenced]:not({isHidden()})');
    if ( currentValue ) currentValue = currentValue.getValue();
    else currentValue = false
    var cmp =  {
      defaults : {labelWidth : 200},
      labelWidth : 200,
      readOnly : !forBrowser,
//      including_layertype : ['raster', 'vector'],
//      exclude_layertype : '@see CarmenImportData.library.fields_actions.getCoucheDataSource.file_georeferenced.checkLayertype function ',//included_layertype
      labels : labels,
      fieldLabel: 'Type de données',
      xtype: 'combo',  
      queryMode : 'local',
      store : store,
      value : currentValue,
      displayField : 'displayField',
      valueField : 'valueField',
      name : 'file_georeferenced',
      listeners : listeners
    };
    if ( forBrowser ){
      cmp = {
        width : 450,
        labelWidth : 200,
        border : !forUploader,
        padding : '0 10 0 10',
        xtype : 'fieldset',
        layout : 'form',
        style : {'background-color':'white'},
        items : [cmp].concat(moreCmps)
      };
    }
    return cmp;
  },
  
	getFunctions : function(formGrid, layertype){
		var me = this;
    return {
      file_georeferenced : {
        checkLayerType : function(layertype){
          var field = this;
          this.setVisible(this.including_layertype.indexOf(layertype)!=-1); // visible only for raster and vector types
          this.setDisabled(this.including_layertype.indexOf(layertype)==-1); // visible only for raster and vector types
          if ( this.rendered ) this.setFieldLabel(this.labels[layertype.toLowerCase()]);
          
        },
        setValue : function(value){
          var field = this;
          if ( this.rendered ) Ext.form.field.Checkbox.prototype.setValue.call(this, !value); 
          Ext.form.field.Checkbox.prototype.setValue.call(this, value)
        },
        listeners : {
          change : function(checkbox, checked){
            var field = this;
//              var next = field.nextNode('field[reset], fieldcontainer[reset]');
//              while (next){
//                nExt.reset();
//                next = nExt.nextNode('field[reset], fieldcontainer[reset]');
//              }

            formGrid.query('[dataIndex=xField], [dataIndex=yField]').forEach(function(column){
              (column.rendered ? column.setVisible(checked) : (column.hidden = !checked));
//                var old = column.onWidgetAttach;
//                column.onWidgetAttach = Ext.Function.createSequence(column.onWidgetAttach, function(column, widget, record){
//                  widget.allowBlank = !checked;
//                  column.onWidgetAttach = old;
//                })
            });
            projection_source.clearFilter();
            projection_source.filterBy(function(record){
              var result = (checked ? record.get('valueField')!='auto' : true);
              return result;
            });
            var combo = formGrid.down('[name=projection_source]');
            if ( combo ){
            	if ( projection_source.find('valueField', 'auto')!=-1 ){
            		combo.setValue('auto');
            	} else {
            		combo.setValue(Carmen.Defaults.PROJECTION);
              }
            }
          }
        }
      },
      file_browser_container : {
        checkLayerType : function(layertype){
          var field = this;
          this.setVisible(true); // always visible
          var fieldLabel = this.fieldLabel;
          fieldLabel = fieldLabel.replace(new RegExp(RegExp.quote(UNSAVED_MARK), "g"), "");
          this.setFieldLabel( (layertype=="raster" ? fieldLabel : fieldLabel+UNSAVED_MARK) );
          var file_name = this.down('[name=file_name]');
          if ( file_name ) file_name.allowBlank = layertype!="raster";
        }
      },
      
      file_name : {
        setValue : function(value, fireEvent){
          var field = this;
          Ext.form.field.Text.prototype.setValue.call(this, value);
          if ( fireEvent ){
            var layertype = formGrid.getLayer().getLayerType().toUpperCase();
            var fullPathName = this.getValue();
            var relativePath = formGrid.down('[name=file_source]').getValue();
            CarmenImportData.library.readSelectedFile.call(formGrid, null, null, null, fullPathName, relativePath, null, null, layertype)
          }
        }
      },
      
      file_browser : {
        getFileBrowser : function(defaultPath, relativeRoot){
          var windowClass = "Ext.ux.FileBrowserWindow";
          if ( typeof eval(windowClass)=="undefined" ) {Ext.Msg.alert('Chargement de la page en cours...');return;}
          var layertype = formGrid.getLayer().getLayerType().toUpperCase();
          var file_georeferenced = formGrid.down('checkbox[name=file_georeferenced]:not({isHidden()})');
          if ( file_georeferenced && eval(file_georeferenced.getValue()) ){
            switch (layertype){
              case "VECTOR" : layertype = "GEOREFERENCED"; break;
              case "RASTER" : layertype = "TILEINDEX"; break;
            }
          }
          var window;
          try {
            var cmpId = windowClass.replace(/\./g, '-');
            window = Ext.getCmp(cmpId) 
              || Ext.create(windowClass, {
              	height : 400,
              	width : 800,
                id: cmpId,
                selectExtension: FILETYPES[layertype].selectExtension,
                uploadExtension: FILETYPES[layertype].uploadExtension,
                dataUrl: Routing.generate('carmen_ws_filebrowser', {routing:'noroute'}),
                defaultPath: defaultPath,
                relativeRoot: relativeRoot,
                listeners: {  
                  multiselectedfile: function(wind, records, relativeRoot, defaultPath){
                  	relativeRoot = String(relativeRoot).endPath();
                  	defaultPath = String(defaultPath).endPath();
                    formGrid.down('[name=file_sheet]').suspendEvents(false);
                    formGrid.down('[name=file_sheet]').setValue(null);
                    formGrid.down('[name=file_sheet]').resumeEvents();
                    var relativePath = [];
                    var fullPathName = [];
                    records.forEach(function(record){
                      fullPathName.push(record.get('name'));
                      filePath = record.get('full_path_name');
                      relativePath.push((relativeRoot+filePath).replace(defaultPath, ''));
                    });
                    if ( fullPathName.length>1 ){
                      fullPathName = '{'+fullPathName.join(', ')+'}';
                    } else {
                    	fullPathName = fullPathName.join(',');
                    }
                    relativePath = relativePath.join('|');
                    
                    var args = [wind, null, null, fullPathName, relativePath, null, null]
                    return CarmenImportData.library.readSelectedFile.apply(this, args);
                  },
                  selectedfile: function(){
                    var args = Array.prototype.slice.call(arguments, 0);
                    args.push(layertype);
                    formGrid.down('[name=file_sheet]').suspendEvents(false);
                    formGrid.down('[name=file_sheet]').setValue(null);
                    formGrid.down('[name=file_sheet]').resumeEvents();
                    return CarmenImportData.library.readSelectedFile.apply(this, args);
                  },
                  scope : formGrid
                }
              });
            return window;
          } catch(error){
          	window && window.destroy();
          	Ext.Msg.alert('Chargement de la page en cours...');
          	return null;
          }
        },
        fileBrowserOptionsEvents : function(fbOptions, browser){
          if (!fbOptions) return null;
          fbOptions.items[0].onWindowClose = function(){
            var record = this.getSelection();
            if ( !record ) return;
            var file_georeferenced = formGrid.down('checkbox[name=file_georeferenced]:not({isHidden()})');
            file_georeferenced && file_georeferenced.setValue(record.get('valueField'));
          };
          fbOptions.items[0].listeners = fbOptions.items[0].listeners || {};
          fbOptions.items[0].listeners.select = (fbOptions.items[0].listeners.select ? fbOptions.items[0].listeners.select.bind(fbOptions.items[0], browser) : function(combo, value){
            var record = combo.getSelection();
            if ( !record ) return;
            browser.fileBrowser.setExtensions(record.get('extensions'));
            combo.up('window').un('close', combo.onWindowClose, combo);
            combo.up('window').on('close', combo.onWindowClose, combo);
          });
          return fbOptions;
        },
        openServerBrowser : function(){
         // if ( !ALL_FILES_LOADED ) {Ext.Msg.alert('Chargement de la page en cours...');return;}
          var browser = this.getFileBrowser('/Root', '/Root/Publication');
          if ( !browser ) return;
          var addCmp = me.getFileBrowserOptions(formGrid.getLayer().getLayerType().toUpperCase(), true, false);
          if ( addCmp ){
            addCmp.region = 'north';
            addCmp.width = "100%"; 
            delete addCmp.padding;
            browser.height += 50;
            browser.fileBrowser.height += 50;
            this.fileBrowserOptionsEvents(addCmp, browser) 
            && (addCmp = browser.fileBrowser.insert(0, addCmp));
          }
          browser.show();
        },
        openClientBrowser : function(){
          //if ( !ALL_FILES_LOADED ) {Ext.Msg.alert('Chargement de la page en cours...');return;}
          var browser  = this.getFileBrowser('/Root/Publication/', '/Root/Publication/temp');
          if ( !browser ) return;
          var uploader = browser.fileBrowser.openUploader();
          var addCmp = me.getFileBrowserOptions(formGrid.getLayer().getLayerType().toUpperCase(), true, true);
          addCmp 
            && Ext.apply(addCmp, {width : 400}) 
            && this.fileBrowserOptionsEvents(addCmp, browser) 
            && uploader.getDockedItems('toolbar[dock="bottom"]')[0].insert(0, addCmp);
          
          if ( !uploader ) {browser.show();return}
        },
        handler: function(){
          //if ( !ALL_FILES_LOADED ) {Ext.Msg.alert('Chargement de la page en cours...');return;}
          var btn = this;
          btn[me.fileBrowserAction]();
        }
      },
      
      file_remove : {
        handler: function(){
          var btn = this;
          formGrid.down('[name=file_name]').setValue(null);
          formGrid.down('[name=file_source]').setValue(null);
          formGrid.down('[name=layerfields]').reset();
        }
      },
      
      // fieldcontainer named "sheets" converted to formField (for get/setValue usages)
      sheets : {
        isFormField : true,
        reset : function(){
          var field = this;
          this.down('combo').getStore().removeAll();
          this.down('combo').reset();
        },
        isDirty : function(){
          var field = this;
          return this.down('combo').isDirty();
        },
        getValue : function(){ 
          var field = this;
          return this.down('combo').getStore().getRange();
        },
        getModelData : function(){
          var field = this;
          return {sheets : this.getValue()};
        },
        getSubmitData : function(){
          var field = this;
          return {sheets : this.getValue()};
        },
        setValue : function(value, callback){ 
          var field = this;
          var combo = this.down('combo');
          var store = combo.getStore();
          if ( callback ) store.on('load', callback);
          store.removeAll();
          if ( value instanceof Array )
            store.loadRawData(value);
          else if ( value instanceof Ext.data.Store ){
            store.loadData(value.getRange());
          }
          if ( callback ) {
            store.fireEvent('load', store, store.getRange(), true);
            store.un('load', callback);
          }
          this.setDisabled(store.getCount()==0);
        },
        isValid : function(){var field = this;return this.down('combo').isValid() },
        validate : function(){var field = this;return this.isValid();},
        disabled : layertype!=CarmenImportData.Datatypes.enums.tabulaire,
        checkLayerType : function(layertype){
          var field = this;
          this.setDisabled(layertype!=CarmenImportData.Datatypes.enums.tabulaire);
        }
      },
      
      file_sheet : {
        listeners : {
          change : function(combo, fileSheet){
            var field = this;
            var fullPathName = formGrid.down('[name=file_name]').getValue();
            var relativePath = formGrid.down('[name=file_source]').getValue();
            CarmenImportData.library.readSelectedFile.call(formGrid, null, null, null, fullPathName, relativePath, null, fileSheet)
          }
        }
      },
      
      // fieldcontainer named "fields" converted to formField (for get/setValue usages)
      layerfields : {
        isFormField : true,
//          resetOriginalValue: function() {
//              this.originalValue = this.getValue();
//              this.validate();
//          },
        hasFieldVisible : function(forErrorMessage){
          var field = this;
          var grid = this.down('grid');
          var gridStore = grid.getStore();
          if (forErrorMessage && gridStore.getCount()==0) return true;
          return gridStore.find('field_visible', true)!=-1; 
        },
        hasFieldX : function(forErrorMessage){
          var field = this;
          var grid = this.down('grid');
          var gridStore = grid.getStore();
          if (forErrorMessage && gridStore.getCount()==0) return true;
          //Si la couche est géoréférencée : un champ X et un champ Y ont été choisis
          var file_georeferenced = formGrid.down('[name=file_georeferenced]:not([disabled=true])');
          return (file_georeferenced && eval(file_georeferenced.getValue()) ? gridStore.find('xField', true)!=-1 : true); 
        },
        hasFieldY : function(forErrorMessage){
          var field = this;
          var grid = this.down('grid');
          var gridStore = grid.getStore();
          if (forErrorMessage && gridStore.getCount()==0) return true;
          //Si la couche est géoréférencée : un champ X et un champ Y ont été choisis
          var file_georeferenced = formGrid.down('[name=file_georeferenced]:not([disabled=true])');
          return (file_georeferenced && eval(file_georeferenced.getValue()) ? gridStore.find('yField', true)!=-1 : true); 
        },
        
        reset : function(){
          var field = this;
          var store = this.down('grid').getStore();
          store.removeAll();
        },
        isValid : function(){
          var field = this;
          var grid = this.down('grid');
          var gridStore = grid.getStore();
          return this.allowBlank
          || this.disabled
          || !this.isVisible()
          || !formGrid.down('[name=file_source]').getValue()
          //Si un fichier source est sélectionné, au moins un champ est choisi
          || (this.hasFieldVisible() && this.hasFieldX() && this.hasFieldY());
        },
        validate : function(){
          var field = this;
          var valid = this.isValid();
          if ( !valid ){
            var errors = [];
            if ( !this.hasFieldVisible(true) ) errors.push("Veuillez sélectionner au moins un champ.");
            if ( !this.hasFieldX(true) ) errors.push("Veuillez sélectionner au moins un champ numérique pour la coordonnée X.");
            if ( !this.hasFieldY(true) ) errors.push("Veuillez sélectionner au moins un champ numérique pour la coordonnée Y.");
            
            this.setActiveErrors(errors);
          }
          else this.setActiveErrors([]);
          Ext.getCmp('layerfields_errors').setVisible(this.getActiveErrors().length>0) 
          return valid;
        },
        isDirty : function(){
          var field = this;
          return this.down('grid').getStore().getModifiedRecords().length>0;
        },
        setValue : function(value, callback){
          var field = this;
          var grid = this.down('grid');
          var store = grid.getStore();
          store.grid = grid;
          store.removeAll();
          if ( callback ) store.on('load', callback);
          grid.getSelectionModel().suspendEvents(false);
          grid.getSelectionModel().deselectAll();
          grid.getSelectionModel().resumeEvents();
          if ( value instanceof Ext.data.Store ){
            store.loadData(value.getRange());
          }
          else {
            store.loadRawData(value, false);
          }
          store.fireEvent('load', store, store.getRange())
          if ( callback ) {
            store.un('load', callback);
          }
        },
        getValue : function(){
          var field = this;
          return this.down('grid').getStore().getRange();
        },
        getModelData : function(){
          var field = this;
          return {layerfields : this.getValue()};
        },
        getSubmitData : function(){
          var field = this;
          return {layerfields : this.getValue()};
        },
        
        listeners : {
          afterlayout : function(){
            this.suspendLayout = true;
            var labelEl = this.up().labelEl || this.labelEl;
            var el = this.el.parent('.x-form-layout-wrap') || this.el;
            this.down('grid').setWidth(el.getWidth()-(labelEl ? labelEl.getWidth() : 0)-20);
            this.down('panel').setWidth(el.getWidth()-(labelEl ? labelEl.getWidth() : 0)-20);
            this.suspendLayout = false;
          }
        }
      },
      
      layerfields_grid_store : {
        listeners : {
          load : function(store){
            var grid = store.grid;
            if ( !grid ) return;
            var selected = store.query('field_visible', true).items;
            grid.getSelectionModel().noFireValidity = true;
            grid.getSelectionModel().deselectAll();
            grid.getSelectionModel().noFireValidity = true;
            grid.getSelectionModel().select(selected);
          }
        }
      },
      layerfields_grid : {
        listeners : {
          selectionchange : function(selModel , selected, options){
            var grid = this;
            var setVisible = function(item, visible){
              var is_selected = item.get('field_visible');
              item.set('field_visible', visible, {commit:true, silent:true});
              item.widgets = item.widgets || [];
              item.widgets.forEach(function(widget){
                var disabled = !visible;
                /*if (widget.xtype=="radio" && widget.getValue() && !visible) widget.setValue(false);*/
                if ( widget.is('radio') ){
                  disabled = disabled || (Carmen.Dictionaries.FIELDDATATYPES.TYPES.NUMBER.indexOf(item.get('field_datatype'))==-1);
                  if (is_selected!=disabled && widget.getValue()) widget.setValue(false);
                }
                widget.setDisabled(disabled);
                
              });
            };
            
            selModel.getStore().each(function(item){setVisible(item, false);}); 
            Ext.each(selected, function(item){setVisible(item, true);});
            if ( !selModel.noFireValidity ) formGrid.down('[name=layerfields]').validate();
            selModel.noFireValidity = false;
          }
        },
        columns : {
          defaults : {
            onWidgetAttach : function(column, widget, record){
            	if ( !record ) return;
              record.widgets = record.widgets || []; 
              if ( record.widgets[column.dataIndex] ) {
                Ext.Array.remove(record.widgets, record.widgets[column.dataIndex]); 
              }
              record.widgets.push(widget);
              record.widgets[column.dataIndex] = widget;
              if ( widget.is('radio') ){
                widget.setDisabled(Carmen.Dictionaries.FIELDDATATYPES.TYPES.NUMBER.indexOf(record.get('field_datatype'))==-1);
              }
            }
          }
        }
      },
      layerfields_grid_widgets : {
        field_datatype : {
          listeners : {
            change : function(widget){
              if ( !(widget.getWidgetRecord() && widget.getWidgetRecord().widgets) ) return;
              widget.getWidgetRecord().widgets.forEach(function(awidget){
                var disabled = Carmen.Dictionaries.FIELDDATATYPES.TYPES.NUMBER.indexOf(widget.getValue())==-1;
                if (awidget.is('radio')) {
                  awidget.setDisabled(disabled);
                  disabled && awidget.getValue() && awidget.setValue(false);
                }
              })
              formGrid.down('[name=layerfields]').validate();
              widget.getWidgetRecord().set('field_datatype', widget.getValue(),{commit:true,silent:true});
            }
          }
        },
        xField : {
          listeners : {
            change : function(widget){
              widget.getWidgetRecord().set('xField', widget.getValue(),{commit:true,silent:true});
              formGrid.down('[name=layerfields]').validate();
            }
          }
        },
        yField : {
          listeners : {
            change : function(widget){
              widget.getWidgetRecord().set('yField', widget.getValue(),{commit:true,silent:true});
              formGrid.down('[name=layerfields]').validate();
            }
          }
        }
      }
    };
  },
	
	initComponent : function(){
    var formGrid = this.formGrid || this;
    var layertype = formGrid.layertype || this.layertype;
    var functions = this.getFunctions(formGrid, layertype);

    ////////////////////////////////////
    var labelVector;
    Ext.apply(this, {
      items : (this.items||[]).concat([/*{
//          including_layertype : ['mnt'],
          checkLayerType : function(layertype){
            var available = this.getValue(); //this.including_layertype.indexOf(layertype)!=-1;
            formGrid.query('[dataIndex=xField], [dataIndex=yField]').forEach(function(column){
              (column.rendered ? column.setVisible(available) : (column.hidden = !available));
            });
            this.setDisabled(!available);
            this.setValue(available); // visible only for raster and vector types
          },
          xtype: 'hidden',  name : 'file_georeferenced', value:true, hideLabel : true
        },*/
        Ext.apply(functions.file_georeferenced, {
//          including_layertype : ['raster', 'vector', 'multi'],
//          exclude_layertype : '@see CarmenImportData.library.fields_actions.getCoucheDataSource.file_georeferenced.checkLayertype function ',//included_layertype
          labels : {
            vector : (labelVector = 'Données tabulaires<br/>avec coordonnées'+UNSAVED_MARK),
            raster : 'Tuilage raster'+UNSAVED_MARK
          },
          fieldLabel: labelVector,
          xtype: 'checkbox',  name : 'file_georeferenced'
        }),{
          name: 'file_source',
          xtype: 'hidden'
        },  
        Ext.apply(functions.file_browser_container, {
//          exclude_layertype : '@see CarmenImportData.library.fields_actions.getCoucheDataSource.file_browser_cont.checkLayertype function ',//included_layertype
          xtype: 'cid-fieldcontainer',
          id : 'fieldcontainer_file_name',
          fieldLabel: 'Fichier'+UNSAVED_MARK,
          layout: {
            type: 'table',
            tableAttrs : {width :'100%'}
          },
          items: [
          Ext.apply(functions.file_name, {
            name: 'file_name',
            readOnly : true,
            xtype: 'textfield',
            width: '100%'
          }), 
          Ext.apply(functions.file_browser, {
            xtype: 'button',
            text: 'Parcourir',
            width: 70,
            name : 'file_source_button',
            margin : '0 10 0 0'
          }), 
          Ext.apply(functions.file_remove, {
            xtype: 'button',
            text: 'Vider',
            width: 70
          })]
        // end filebrowser
        }),  
        Ext.apply(functions.sheets, {
          xtype : 'fieldcontainer',
          name : 'sheets',
          fieldLabel : 'Feuille'+UNSAVED_MARK,
//          exclude_layertype : ['raster', 'mnt'],
          width: '100%',
          layout: 'fit',
          isFormField : true,
          items : [Ext.apply(functions.file_sheet, {
            width: '100%',
            anchor: '100%',
            xtype : 'combo',
            name : 'file_sheet',
            emptyText : 'Sélectionnez la feuille du fichier',
            displayField : 'sheet_index',
            valueField : 'sheet_name',
            queryMode : 'local',
            store :  Ext.create('CarmenImportData.data.JsonMemoryStore', {fields : ['sheet_index', 'sheet_name'], data : []})
          })]
        // end Feuille
        }), {
          xtype : 'hidden',
          name : 'encoding_detected'
        }, {
//          exclude_layertype : 'raster',
          xtype : 'combo',
          fieldLabel : 'Encodage source'+UNSAVED_MARK,
          name : 'encoding_source',
          valueField : 'valueField',
          displayField : 'displayField',
          store : Ext.create('Ext.data.Store', {
            fields: ['valueField', 'displayField'],
            data : [{valueField : 'auto', displayField : 'Détection Automatique'},{valueField : 'Latin-1', displayField : 'Latin 1'},{valueField : 'UTF-8', displayField : 'UTF-8'}]
          }),
          queryMode : 'local'
          
        // end Encodage source
        }, {
//          exclude_layertype : 'raster',
          xtype : 'combo',
          fieldLabel : 'Projection source'+UNSAVED_MARK,
          name : 'projection_source',
          valueField : 'valueField',
          displayField : 'displayField',
          store : projection_source,
          queryMode : 'local',
//          setValue : function(value){
//          	var file_georeferenced = formGrid.down('[name=file_georeferenced]:not([disabled=true])');
//          	var notAuto = ["mnt", 'tabulaire'].indexOf(layertype.toLowerCase())!=-1
//          	            || ("vector"==layertype.toLowerCase() && file_georeferenced && file_georeferenced.getValue());
//          	console.log('setValue - notAuto', notAuto);
//            if ( notAuto && value=="auto"){
//              Ext.form.field.ComboBox.prototype.setValue.call(this, Carmen.Defaults.PROJECTION);
//            } else {
//            	Ext.form.field.ComboBox.prototype.setValue.call(this, value);
//            }
//          },
          checkLayerType : function(layertype){
              projection_source.clearFilter();
              projection_source.filterBy(function(record){
                var result = (["mnt", 'tabulaire'].indexOf(layertype.toLowerCase())!=-1 ? record.get('valueField')!='auto' : true);
                return result;
              });
              var combo = formGrid.down('[name=projection_source]');
              if ( combo ){
                if ( projection_source.find('valueField', 'auto')!=-1 ){
                  combo.setValue('auto');
                } else {
                  combo.setValue(Carmen.Defaults.PROJECTION);
                }
              }
          }
//          
        // end Projection source
        }, {
//          exclude_layertype : 'raster',
          xtype : 'combo',
          fieldLabel : 'Projection cible'+UNSAVED_MARK,
          name : 'projection_cible',
          disabled : true,
          valueField : 'valueField',
          displayField : 'displayField',
          queryMode : 'local',
          store : Carmen.Dictionaries.PROJECTIONS
          
        // end Projection cible
        },{
//          exclude_layertype : 'raster',
          fieldLabel : 'Champs',
          xtype : 'fieldcontainer',
          id : 'fieldcontainer_layerfields',
          layout : {
            type : 'table',
            tableAttrs : {width :'100%'}
          },
          scrollable:'x',
          items : [
          Ext.apply(functions.layerfields, {
//            exclude_layertype : 'raster',
            isFormField : true,
            msgTarget : 'layerfields_errors-body',
            //msgTarget : 'under',
            xtype : 'cid-fieldcontainer',
            name : 'layerfields',
            allowBlank : false,
            layout: {
              type: 'vbox',
              pack: 'stretchmax',
              align: 'end'
            },
            items : [
              Ext.merge(functions.layerfields_grid, {
                minWidth : 500,
                width : 500,
                xtype : 'grid',
                height : 200,
                border : false,
                reserveScrollbar : true,
                viewConfig : {emptyText : 'Aucun champ', deferEmptyText : false},
                selModel: {
                  selType : 'checkboxmodel',
                  checkOnly : true
                    , flex:0.05
                },
                store : Ext.create('CarmenImportData.data.JsonMemoryStore', Ext.apply(functions.layerfields_grid_store, {model : 'CarmenImportData.model.ChampDonnee'})),
                columns : {
                  defaults : {
                      align : 'center'
                  },
                  items : [
                    {text: "Champ", flex:0.30, dataIndex: 'field_name', align : 'left'},
                    {text: "Type de valeurs", flex:0.25, dataIndex: 'field_datatype', xtype : 'widgetcolumn'
                    , widget : Ext.apply(functions.layerfields_grid_widgets.field_datatype, {
                        xtype : 'combo', queryMode : 'local', displayField : 'displayField', valueField : 'codeField', 
                        store : Carmen.Dictionaries.FIELDDATATYPES
                      })
                    },
                    {xtype :'widgetcolumn', flex:0.20, text : 'Coordonnée X', dataIndex : 'xField', hidden : true
                    , widget : Ext.apply(functions.layerfields_grid_widgets.xField, {xtype : 'radio', name : 'xField'})
                    },
                    {xtype :'widgetcolumn', flex:0.20, text : 'Coordonnée Y', dataIndex : 'yField', hidden : true
                    , widget : Ext.apply(functions.layerfields_grid_widgets.yField, {xtype : 'radio', name : 'yField'})
                    }
                  ]
                }
              }), 
              {
                  xtype : 'panel', 
                  layout : 'fit',
                  //width : '100%',
                  id :'layerfields_errors', 
                  bodyCls:"x-form-error-msg x-form-invalid-under x-form-invalid-under-default",
                  hidden : true
              }]
              // end Champs
            })
          ]
        }]
      )
    });
      
    this.callParent(arguments);
	}
});



Ext.define('CarmenImportConfiguratorForm', {
	requires : [
  'Ext.ux.FileBrowserPanel',
	'Ext.ux.FileBrowserWindow'
	],
	xtype : 'carmenimportconfiguratorform',
  extend : 'Ext.form.Panel',
  suffixRoute : '_localdata',
	initComponent : function(){
    var record = Ext.create('CarmenImportData.model.CoucheDonnees', {});
  	Ext.apply(this, {
    //  id : 'formImportLocalData',
      width: '100%',
      fieldDefaults: {
        labelAlign: 'left',
        labelWidth: 150,
        width: '100%',
        msgTarget: 'side'
      },
      getLayer : function(){return record},
      items : (this.items||[]).concat([
        Ext.applyIf(this.configuratorConfig||{}, {
          suffixRoute : '_localdata',
          padding : '10 10 10 10',
          xtype : 'carmenimportconfigurator',
          fileBrowserAction : 'openClientBrowser',
          formGrid : this
        })
      ])
    });
    this.callParent(arguments);
    this.loadRecord(record);
	}
})
