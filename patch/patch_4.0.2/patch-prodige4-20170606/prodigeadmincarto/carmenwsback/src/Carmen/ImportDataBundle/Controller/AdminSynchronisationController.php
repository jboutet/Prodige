<?php
namespace Carmen\ImportDataBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\JsonResponse;

use Carmen\ApiBundle\Exception\ApiException;
use Prodige\ProdigeBundle\Controller\BaseController;
use JMS\Serializer\SerializationContext;
use Prodige\ProdigeBundle\Controller\User;
/**
 * @Route("/prodige/synchronisation")
 */
class AdminSynchronisationController extends BaseController
{
    const IMPORT_TYPE_SYNCHRO  = "synchronisation";
    const IMPORT_TYPE_AUTOMATE = "automate";
    const IMPORT_TYPE_DIFFERE  = "import_differe";
    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/admin/{uuid}/{mode}", name="prodige_synchronisation_admin", options={"expose"=true}, defaults={"mode"="html"}, requirements={"uuid", "mode"="html|js"})
     */
    public function adminAction($uuid, $mode="html") {
       // if ( !$this->debug ) \Carmen\ApiBundle\Controller\ProdigeController::checkSecurity($this->container, $uuid);
        
        $session = $this->getRequest()->getSession();

        if($this->getRequest()->query->get('from',false) !==  'prodige') {
            $session->clear();
        }
        
        
        $getContentType = function($url){
        	$explode_headers = function($v){$a = explode(': ',$v); if (count($a)==2) return array($a[0]=>$a[1]); return array();};
          $curl = curl_init($url);
        	curl_setopt_array($curl, array(
        			CURLOPT_RETURNTRANSFER => true,
        			CURLOPT_HEADER => true,
        			CURLOPT_NOBODY => true,
        			CURLOPT_CONNECTTIMEOUT => 30,
        			CURLOPT_TIMEOUT => 30,
        	));
        	$res = explode("\n", curl_exec($curl));
        	$res = array_map($explode_headers, $res);
        	$res = array_reduce($res, 'array_merge', array());
        	if ( !isset($res["Content-Type"]) ) return null;
        	$contentType = strtolower($res["Content-Type"]);
        	$contentType = explode(';', $contentType);
        	$contentType = $contentType[0];
        	return $contentType;
        };

        if($uuid !== null) {
        //    $dataDirectory = rtrim($this->getMapfileDirectory(), "/")."/";
            $CATALOGUE = $this->getCatalogueConnection("catalogue");
        
            
            $xml = $CATALOGUE->fetchColumn("select data from public.metadata where uuid=:uuid", array("uuid"=>$uuid));
            if ( empty($xml) ) return $this->render('CarmenWebBundle:Default:forbidden.html.twig');
            $xml = new \SimpleXMLElement($xml, 0, false, 'gmd', true);
            $metadata_title = $xml->xpath('//gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:citation/gmd:CI_Citation/gmd:title/gco:CharacterString');
            $metadata = array(
              "uuid" => $uuid,
            	"metadata_title" => (empty($metadata_title) ? $uuid : $metadata_title[0]->__toString())
            );
            
            $task = $CATALOGUE->fetchAssoc("select * from tache_import_donnees where uuid=:uuid", compact("uuid"));
            if (!$task){
            	$task = array();
            }
            
            if ( $mode=="html" ){
                    	
	            $serializer = $this->get('jms_serializer');
	            $context = new SerializationContext();
	            $context->setSerializeNull(true);
	            $user = $serializer->serialize($this->getUser(), 'json', $context);
	            return $this->render('CarmenImportDataBundle:Default:adminsynchronisation.html.twig', array(
	            		'user'=>$user, 
	            		"uuid"=>$uuid,
	            		'metadata' => $metadata,
	            		'task' => array_merge($task, array("type_frequency"=> ($task["import_frequency"] ? "repeted" : "fixed"))),
	            ));
            }
            
            
            
            
            $sources = array();
            $WFS_TYPE = "OGC:WFS";
            $ATOM_TYPE = "WWW:DOWNLOAD";
            $wfsLinks  = $xml->xpath('//gmd:MD_DigitalTransferOptions/gmd:onLine/gmd:CI_OnlineResource[starts-with(child::gmd:protocol/gco:CharacterString/text(), "'.$WFS_TYPE.'")]');
            $atomLinks = $xml->xpath('//gmd:MD_DigitalTransferOptions/gmd:onLine/gmd:CI_OnlineResource[child::gmd:protocol/gco:CharacterString/text()="'.$ATOM_TYPE.'-1.0-http--download"]');
            foreach($wfsLinks as $i=>$element){
            	$element instanceof \SimpleXMLElement;
            	$child = $element->children("gmd", true);
            	$link = array_map('trim', array(
            		"import_data_type" => "WFS",
            		"type" => $WFS_TYPE,
            		"content-type" => "GML",
            		"import_data_source" => $child->linkage->URL->__toString(),
            		"title" => ($child->name->count() ? $child->name->children("gco", true)->CharacterString->__toString() : ""),
            		"description" => ($child->description->count() ? $child->description->children("gco", true)->CharacterString->__toString() : ""),
            	));
            	$url_parts = array();
            	parse_str(parse_url($link["import_data_source"], PHP_URL_QUERY), $url_parts);
            	$url_parts = array_change_key_case($url_parts, UPPER_CASE);
            	$glue = (empty($url_parts) ? "?" : "&");
            	if ( !array_key_exists("REQUEST", $url_parts) ){
            	    $link["import_data_source"] .= $glue."request=GetCapabilities";
            	    $glue = "&";
            	}
            	if ( !array_key_exists("SERVICE", $url_parts) ){
            	    $link["import_data_source"] .= $glue."service=WFS";
            	}
            	$link["leaf"] = true;
            	$sources[] = $link;
            }
            foreach($atomLinks as $i=>$element){
            	$element instanceof \SimpleXMLElement;
            	$child = $element->children("gmd", true);
            	$nameGCO = $child->name->children("gco", true);
            	$nameGMX = $child->name->children("gmx", true);
            	if ( $nameGCO->count() ){
            		$title = $nameGCO->CharacterString->__toString();
            	}
            	else if ( $nameGMX->count() ){
            		$title = $nameGMX->__toString();
            	} else {
            		$title = "";
            	}
            	$atom = array_map('trim', array(
            		"import_data_type" => "ATOM",
            		"type" => $ATOM_TYPE,
            		"content-type" => "RSS",
            		"import_data_source" => $child->linkage->URL->__toString(),
            		"title" => $title,
            		"description" => ($child->description->count() ? $child->description->children("gco", true)->CharacterString->__toString() : ""),
            	));
            	$atom["leaf"] = true;
            	$url = trim($child->linkage->URL->__toString());
            	$url = html_entity_decode(urldecode($url));
            	$contentType = $getContentType($url);
            	$atom["CT"] = $contentType;
            	if ( isset($contentType) ){
            		if ( strpos($contentType, 'xml')!==false ){//donnée XML => flux ATOM ?
            			$xmlcontent = utf8_encode(file_get_contents($url, "r"));
            			$rss = new \SimpleXMLElement($xmlcontent);
            			$entries = $rss->entry;
            			if ( $entries->count()==0 ){
            				$atom["content-type"] = "XML";
            			}
            			foreach($entries as $entry){
            				$link = $entry->link[0];
            				$link instanceof \SimpleXMLElement;
            				$attributes = $link->attributes();
            			
            				if ( isset($attributes["href"]) ){
            					$data = $atom;
            					$data["import_data_source"] = $attributes["href"]->__toString();
            					if ( isset($attributes["title"]) ){
            						$data["title"] = $attributes["title"]->__toString();
            					}
            					
            					$contentType = $getContentType($data["import_data_source"]);
            					if ( isset($contentType) ){
            						if ( strpos($contentType, 'xml')===false ){
            							$data["content-type"] = "ZIP";
            						}
            					}
            					$data["CT"] = $contentType;
            					$data = array_map('trim', $data);
            					$data["leaf"] = true;
	            				$atom["expanded"] = true;
	            				$atom["leaf"] = false;
	            				unset($data["type"]);
	            				$atom["children"][] = $data;
	            				continue;
            				}
            			}
            		}
            		else if ( strpos($contentType, 'application/zip')!==false || strpos($contentType, 'application/octet-stream')!==false ){//donnée ZIP
            			$atom["content-type"] = "ZIP";
            		} else {
            			continue;
            		}
            	}
            	$sources[] = $atom;
            }
            
            $select_source = function(&$sources) use($task, &$select_source){
            	foreach($sources as $i=>$source){
            		$sources[$i]["is_selected"] = false;
            	  if ( !isset($source["children"]) ){
            		  $sources[$i]["children"] = array();
            	  }
            		if ( $task && $task["import_data_source"]==$source['import_data_source'] ){
            		  $sources[$i]["is_selected"] = true;
            		}
            		$select_source($sources[$i]["children"]);
            	}
            };
            $select_source($sources);
        
            if ( $mode=="js" ){
	            return new JsonResponse($sources);
            }
        }
        else {
            return $this->render('CarmenWebBundle:Default:forbidden.html.twig');
        }
    }
    
    
    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/{mode}/{uuid}", name="prodige_synchronisation_save", options={"expose"=true}, defaults={"mode"="save"}, requirements={"uuid", "mode"="save|execute"})
     */
    public function saveAction(Request $request, $uuid, $mode="save") 
    {    	
      $CATALOGUE = $this->getCatalogueConnection("catalogue");
      $user_id = User::GetUserId();
    	$data = array(
    		"uuid"=>$uuid,
        "pk_couche_donnees" => null,
        "user_id" => $user_id,
    		"import_type" => "synchronisation",
    		"import_frequency" => $request->get('import_frequency', null),
    		"import_table" => $request->get('import_table', null),
    		"import_data_type" => $request->get('import_data_type', 'VECTOR'),
    		"import_data_source" => $request->get('import_data_source', null),
    	);
    	
    	if ( $data["import_data_type"]=="WFS" ){
    		$data["import_extra_params"] = json_encode(array("typeName"=>$request->get("title")));
    	}
      
      $CATALOGUE->beginTransaction();
      $data["pk_tache_import_donnees"] = $CATALOGUE->fetchColumn("select pk_tache_import_donnees from tache_import_donnees where uuid=:uuid", compact("uuid"));
      if ( !$data["pk_tache_import_donnees"] ) {
      	unset($data["pk_tache_import_donnees"]);
      	$columns = array_keys($data);
      	$res = $CATALOGUE->executeQuery("insert into tache_import_donnees (".implode(", ", $columns).") values (:".implode(", :", $columns).")", $data);
      } else {
      	$columns = array_keys($data);
      	$columns = array_combine($columns, $columns);
      	$set = str_replace("=", "=:", http_build_query($columns, null, ", "));//col_name1=:col_name1, col_name2=:col_name2
      	$res = $CATALOGUE->executeQuery("update tache_import_donnees set ".$set." where pk_tache_import_donnees=:pk_tache_import_donnees", $data);
      }
    	
      $CATALOGUE->commit();
      if ( $mode=="execute" ){
      	return $this->forward("CarmenImportDataBundle:ImportTask:taskImport", array("uuid"=>$uuid));
      }
      return new JsonResponse(array(__METHOD__, "ok", $user_id, ));
    }
    
    #######################################################################

}
