<?php
namespace Carmen\ImportDataBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Carmen\ApiBundle\Services\Helpers;
use Carmen\OwsContextBundle\Controller\OwsContextController;
use Doctrine\ORM\Query\ResultSetMapping;
use Carmen\ApiBundle\Services\CarmenConfig;
use Doctrine\ORM\NativeQuery;
use Doctrine\ORM\Query;
use Carmen\ImportDataBundle\Controller\ImportTaskController;
use Prodige\ProdigeBundle\Services\ConfigReader;
use Carmen\ImportDataBundle\Controller\AbstractImportDataController;

class ImportGeocodageCommand extends ContainerAwareCommand
{
    protected $debug = false;
    
    protected $REQUESTED_URL;
    protected $GEOCODING_DATA;
    protected $logFile;
    
    
    protected $fieldsFileEJ = array(
        "identifiant_technique_ej",
        "statut_fiche",
        "num_si_biomed",
        "num_finess_ej",
        "num_siren",
        "raison_sociale",
        "autorisation_administrative",
        "date_autorisation",
        "date_creation",
        "date_fermeture",
        "date_installation",
        "date_fin_suspension",
        "type_structure",
        "structure_juridique",
        "statut_structure",
        "structure_appartenance",
        "telephone",
        "fax",
        "mail",
        "adresse_type_voie",
        "adresse_complement_voirie",
        "adresse_num_voie",
        "adresse_nom_voie",
        "adresse_lieudit_bp",
        "adresse_complement_raison_social",
        "adresse_distribution",
        "code_postal",
        "num_departement",
        "libelle_departement",
        "adresse_commune_routage",
        "code_insee_commune",
        "code_postal_commune",
        "libelle_commune",
        "pays",
        "etablissement_public_pole",
        "responsable_pole",
        "convention",
        "commentaire_convention",
        "geolocalisation_longitude",
        "geolocalisation_latitude",
        "geolocalisation_qualite",
    );
    
    protected $fieldsFileET = array(
        "id_tech_et",
        "id_tech_ej",
        "date_creation",
        "date_modification",
        "num_entite_territoriale",
        "raison_social",
        "num_entite_juridique",
        "num_finess_entite_territoriale_principale",
        "entite_territoriale_principale",
        "num_siret",
        "type_site",
        "categorie",
        "date_autorisation",
        "date_ouverture",
        "date_fermeture",
        "horaire_ouverture",
        "nombre_jour_ouverture",
        "nombre_heure_ouverture",
        "telephone",
        "fax",
        "mail",//20
        "adresse_complement_raison_social",
        "adresse_distribution",
        "adresse_num_voie",
        "adresse_type_voie",
        "adresse_complement_voirie",
        "adresse_nom_voie",
        "adresse_lieudit_bp",
        "num_departement",
        "libelle_departement",
        "adresse_commune_routage",//30
        "code_insee_commune",
        "code_postal_commune",
        "libelle_commune",
        "code_postal",
        "pays",
        "permanence_urgence",
        "geolocalisation_longitude",
        "geolocalisation_latitude",
        "geolocalisation_qualite",//39
    );
    
    /**
     * Configures the command definition
     */
    protected function configure()
    {
        //TODO create user
        
        $this
            ->setName('carmen:importgeocodage')
            ->addOption("debugcmd", null, InputOption::VALUE_NONE, "active le mode debug")
            ->setDescription('Import of GIS data in application')
        ;
    }

    /**
     * Executes the command
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        
        $container = $this->getContainer();
        $configReader = $container->get('prodige.configreader');
        $configReader instanceof ConfigReader;
        
        $output->setDecorated(true);
        $dialog = ($input->isInteractive() ? $this->getHelper('dialog') : null);
        
        $this->debug = $input->getOption("debugcmd");
        
        // global variables
        $this->user_id = 29;
        $this->REQUESTED_URL = "http://api-adresse.data.gouv.fr/search/csv/";
        
        $PRO_MAPFILE_PATH = $configReader->getParameter("PRO_MAPFILE_PATH");
        $this->GEOCODING_DATA = "/home/www/geocodage/geocodage_data/";
        if ( $this->debug ) {
            $temp = __DIR__."/";
            $basename = "geocodage";
            while(basename(realpath($temp))!=$basename && !is_dir($temp.$basename) && realpath($temp)!="/" ){
                $temp .= "../";
            }
            if ( basename(realpath($temp))==$basename && is_dir($temp) ){
                $this->GEOCODING_DATA = realpath($temp)."/geocodage_data/";
            }
            if ( basename(realpath($temp))!=$basename && is_dir($temp.$basename) ){
                $this->GEOCODING_DATA = realpath($temp)."/geocodage/geocodage_data/";
            }
        }
        
        $PATH_TO_LOG_FILE = $this->GEOCODING_DATA."../logs/";
        $LogFileName = "logFile.log";
        $this->logFile = $PATH_TO_LOG_FILE . $LogFileName;
        
        $INPUTFILENAME_EJ = "export_sig_ej_biomed.csv";
        $INPUTFILENAME_ET = "export_sig_et_biomed.csv";
        
        $inputFileEJ = $this->GEOCODING_DATA . $INPUTFILENAME_EJ;
        $inputFileET = $this->GEOCODING_DATA . $INPUTFILENAME_ET;
        
    
        if(!is_dir($this->GEOCODING_DATA)) {
            @mkdir($this->GEOCODING_DATA, 0770, true);
        }
        if(!is_dir($PATH_TO_LOG_FILE)) {
            @mkdir($PATH_TO_LOG_FILE, 0770, true);
        }
    
                

        $defaultFlux = array(
        		"user_id"           => $this->user_id,
        		"import_type"       => "geocodage",
        		"import_frequency"  => null,
        		"import_data_type"  => "VECTOR",
        );
        $defaultParams = array(
            "encoding_source"      => "UTF-8",
            "import_action"        => 'replace',
            "field_key"            => '',
            "projection_source"    => "4326",
            "projection_cible"     => "2154",
            "file_georeferenced"   => true,
            "file_sheet"           => '',
            "couchd_type_stockage" => 1
        );
        // Loop ...
        $makeField = function($fieldName){return array("field_name"=>$fieldName, "field_datatype"=>"STRING", "field_visible"=>true);};
        
        $exportFiles = array(
            $inputFileEJ => array(
                "id_columns" => 'identifiant_technique_ej',
                "filtercolumns" => array('adresse_num_voie', 'adresse_type_voie', 'adresse_nom_voie', 'code_postal_commune', 'libelle_commune'),
                "flux" => array_merge($defaultFlux, array(
    		        		"import_table" => "dgos_metropole_biomed_ej_test_p",
    		        		"import_data_source" => $PRO_MAPFILE_PATH."/temp/import_sig_ej_biomed.csv",
                )),
                "extraparams" => array_merge($defaultParams, array(
                    "couchd_nom"   => "Import du géocodage ".$INPUTFILENAME_EJ,
    		        		"import_table" => "dgos_metropole_biomed_ej_test_p",
                    "layerfields"  => array("drops"=>array(), "xField"=>"geolocalisation_longitude", "yField"=>"geolocalisation_latitude", "types"=>array_map($makeField, $this->fieldsFileEJ)),
                ))
            ), 
            $inputFileET => array(
                "id_columns" => 'id_tech_et',
                "filtercolumns" => array('adresse_lieudit_bp', 'adresse_complement_raison_social', 'adresse_nom_voie', 'code_postal_commune', 'libelle_commune'),
                "flux" => array_merge($defaultFlux, array(
    		        		"import_table" => "dgs_fr_biomed_et_p",
    		        		"import_data_source" => $PRO_MAPFILE_PATH."/temp/import_sig_et_biomed.csv",
                )),
                "extraparams" => array_merge($defaultParams, array(
                    "couchd_nom"   => "Import du géocodage ".$INPUTFILENAME_ET,
                    "layerfields"  => array("drops"=>array(), "xField"=>"geolocalisation_longitude", "yField"=>"geolocalisation_latitude", "types"=>array_map($makeField, $this->fieldsFileET)),
                ))
            ), 
        );
        
        
		        
        $tabInfoDELAYED = array(
        		'FILE		REPLACE_DATA	/home/prodige/cartes/Publication/temp/import_sig_ej_biomed.csv	dgos_metropole_biomed_ej_test_p	b.fontaine@alkante.com	4326			undefined	geolocalisation_longitude	geolocalisation_latitude			vecteur	{"identifiant_technique_ej":"varchar","statut_fiche":"varchar","num_si_biomed":"varchar","num_finess_ej":"varchar","num_siren":"varchar","raison_sociale":"varchar","autorisation_administrative":"varchar","date_autorisation":"varchar","date_creation":"varchar","date_fermeture":"varchar","date_installation":"varchar","date_fin_suspension":"varchar","type_structure":"varchar","structure_juridique":"varchar","statut_structure":"varchar","structure_appartenance":"varchar","telephone":"varchar","fax":"varchar","mail":"varchar","adresse_type_voie":"varchar","adresse_complement_voirie":"varchar","adresse_num_voie":"varchar","adresse_nom_voie":"varchar","adresse_lieudit_bp":"varchar","adresse_complement_raison_social":"varchar","adresse_distribution":"varchar","code_postal":"varchar","num_departement":"varchar","libelle_departement":"varchar","adresse_commune_routage":"varchar","code_insee_commune":"varchar","code_postal_commune":"varchar","libelle_commune":"varchar","pays":"varchar","etablissement_public_pole":"varchar","responsable_pole":"varchar","convention":"varchar","commentaire_convention":"varchar","geolocalisation_longitude":"varchar","geolocalisation_latitude":"varchar","geolocalisation_qualite":"varchar"}	4326',
        		'FILE		REPLACE_DATA	/home/prodige/cartes/Publication/temp/import_sig_et_biomed.csv	dgs_fr_biomed_et_p	b.fontaine@alkante.com	4326			undefined	geolocalisation_longitude	geolocalisation_latitude			vecteur	{"id_tech_et":"varchar","id_tech_ej":"varchar","date_creation":"varchar","date_modification":"varchar","num_entite_territoriale":"varchar","raison_social":"varchar","num_entite_juridique":"varchar","num_finess_entite_territoriale_principale":"varchar","entite_territoriale_principale":"varchar","num_siret":"varchar","type_site":"varchar","categorie":"varchar","date_autorisation":"varchar","date_ouverture":"varchar","date_fermeture":"varchar","horaire_ouverture":"varchar","nombre_jour_ouverture":"varchar","nombre_heure_ouverture":"varchar","telephone":"varchar","fax":"varchar","mail":"varchar","adresse_complement_raison_social":"varchar","adresse_distribution":"varchar","adresse_num_voie":"varchar","adresse_type_voie":"varchar","adresse_complement_voirie":"varchar","adresse_nom_voie":"varchar","adresse_lieudit_bp":"varchar","num_departement":"varchar","libelle_departement":"varchar","adresse_commune_routage":"varchar","code_insee_commune":"varchar","code_postal_commune":"varchar","libelle_commune":"varchar","code_postal":"varchar","pays":"varchar","permanence_urgence":"varchar","geolocalisation_longitude":"varchar","geolocalisation_latitude":"varchar","geolocalisation_qualite":"varchar"}	4326'
        );
        
        $strFileQueue = "/home/prodige/cartes/Publication/layerImportation.delayed";
        
        
        
        foreach($exportFiles as $exportFile=>$def) {
          $geocodingFile = $this->geocodingDataFromInputFile($exportFile, $def["filtercolumns"]);
          $outputFile    = $this->finalImportGeocodingData($exportFile, $geocodingFile, $def["id_columns"]);
          $this->addToQueue($exportFile, $def);
        }
                
    }


    /**
     * Write in log file
     * @param logfile
     * @param $msgToLog
     * */
    private function logToFile($msgToLog) {
        $time = @date('[d/M/Y:H:i:s]');
        $fd = fopen($this->logFile, "a+");
        if(!$fd) {
            fwrite($fd, $time . " : Echec dans l'ouverture du fichier de log : ".$this->logFile. "\n");
        }
        else {
            $msgToWrite = $time ." : ".$msgToLog;
            fwrite($fd, $msgToWrite . "\n");
            fclose($fd);
        }
    }

   /**
    * Detect delimiter in csv file 
    * @param $csvFile
    * @return $Delimiter
    */
    private function detectDelimiter($csvFile)
    {
        $delimiters = array(
            ';' => 0,
            ',' => 0,
            "\t" => 0,
            "|" => 0
        );
        $handle = fopen($csvFile, "r");
        $firstLine = fgets($handle);
        fclose($handle);
        foreach ($delimiters as $delimiter => &$count) {
            $count = count(str_getcsv($firstLine, $delimiter));
        }
        return array_search(max($delimiters), $delimiters);
    }

    /**
     * Filter columns in csv File
     * @param $inputFile
     * @param $outputFile
     * */
    private function filterCSVFile($inputFile, $filteredFile, $ar_filter_column) {

        $delimiter = $this->detectDelimiter($inputFile);
    
        $temp = array();
        foreach($ar_filter_column as $column_name){
            $temp[] = $this->returnIndexColumnName($inputFile, $delimiter, $column_name);
        }
        $ar_filter_column = $temp;

        $fileToRead = fopen($inputFile, 'r');
        $fileToWrite = fopen($filteredFile, 'w');

        if($fileToRead && $fileToWrite) {
            while(($row = fgets($fileToRead)) !== FALSE) {
                $data = str_getcsv($row, $delimiter);
                $outputData = array();
                for($i=0 ; $i < count($ar_filter_column) ; $i++) {
                    $outputData[] = $data[$ar_filter_column[$i]];
                }
                fputcsv($fileToWrite, $outputData);
            }
        }

        fclose($fileToRead);
        fclose($fileToWrite);

    }

    /**
     * Return index for column name
     * @param : $csvFile
     * @param : $delimiter
     * @param : $columnName
     * */
    private function returnIndexColumnName($csvFile, $delimiter, $columnName) {
        $index = -1;

        if(file_exists($csvFile)) {
            $handle = fopen($csvFile, "r");
            $firstLine = fgets($handle);
            fclose($handle);

            $header = str_getcsv($firstLine, $delimiter);

            for($i=0 ; $i < count($header) ; $i++) {
                if($header[$i] == $columnName) {
                    $index = $i;
                    break;
                }
            }
        }
        return $index;
    }
    /**
     * Shortcut to return the Doctrine Registry service.
     *
     * @return Registry
     *
     * @throws \LogicException If DoctrineBundle is not available
     */
    private function getDoctrine()
    {
        if (!$this->getContainer()->has('doctrine')) {
            throw new \LogicException('The DoctrineBundle is not registered in your application.');
        }

        return $this->getContainer()->get('doctrine');
    }
    
    
    /** add to delayerd import queue
     * @param : $strFileQueue : queue file
     * @param : file: file to copy
     * @param : newfile: file dest
     * @param : $strInfo : info to add to file
     * @param : 
     * */

    private function addToQueue($exportFile, $def)
    {
        $CATALOGUE = $this->getDoctrine()->getConnection("catalogue");
        $CATALOGUE->exec('set search_path to catalogue');
        $data = $def["flux"];
        
        $keys = array_merge(array("uuid", "pk_couche_donnees"), array_keys($data));
        $dkeys = array_combine(array_keys($data), array_keys($data));
        $inserts = $CATALOGUE->executeQuery(
        		" insert into tache_import_donnees (".implode(", ", $keys).")".
        		" select distinct meta.uuid, couche.pk_couche_donnees, :".implode(", :", array_merge($dkeys, array("user_id"=>"user_id::int", "import_frequency"=>"import_frequency::int")))." ".
        		" from couche_donnees couche ".
        		" inner join fiche_metadonnees fmeta on (fmeta.fmeta_fk_couche_donnees=couche.pk_couche_donnees) ".
        		" inner join public.metadata meta on (meta.id=fmeta.fmeta_id::int) ".
        		" where couche.couchd_emplacement_stockage=:import_table".
        		" returning *"
        		,
        		$data,
            array("user_id"=>\PDO::PARAM_INT)
        );
        
        foreach($inserts as $tache){
        	$layertype = strtoupper($tache["import_data_type"]);
        	$pk_couche_donnees = $tache["pk_couche_donnees"];
        	$pk_tache_import_donnees = $tache["pk_tache_import_donnees"];
        	$couchd_emplacement_stockage = $tache["import_table"];
        	$layerfile = $tache["import_data_source"];
        	//$caracteristiques = $this->getGISFileProperties($pk_couche_donnees, $layerfile, $couchd_type_stockage, null, $layertype, $couchd_emplacement_stockage, $directory);
        	$extraparams = $def["extraparams"];
        	$extraparams = array_merge(
        	   array(
        	     "couchd_nom" => "Couche non définie",
          	   "pk_couche_donnees"  => $pk_couche_donnees,
          	   "couchd_emplacement_stockage"  => $couchd_emplacement_stockage,
        	   ),
    	       $CATALOGUE->fetchAssoc("select * from couche_donnees where pk_couche_donnees=:pk_couche_donnees", array("pk_couche_donnees"=>$pk_couche_donnees)) ?: array(),
    	       $extraparams
        	);
        	
        	$CATALOGUE->executeQuery(
        	"update tache_import_donnees set import_extra_params=:import_extra_params where pk_tache_import_donnees=:pk_tache_import_donnees", array(
        			"pk_tache_import_donnees" => $pk_tache_import_donnees,
        			"import_extra_params" => json_encode($extraparams)
        	));
        }
    }

    /**
     * Geocode the input file, using BAN api and write the results in an output file
     * @param : $url
     * @param : $inputFile
     * @param : $ar_filter_column
     * @return geocoding file
     * */
    private function geocodingDataFromInputFile ($inputFile, $ar_filter_column) {

        if(file_exists($inputFile)) {
            $path_details = pathinfo($inputFile);
            $outputFileName = str_replace("export", "import", $path_details['filename']);
            $outputFile = $this->GEOCODING_DATA . "/" . $outputFileName . "." . $path_details['extension'];
            
            if ( $this->debug ) return $outputFile;

            //$this->logToFile("Appeler la fonction du filtrage 'filterCSVFile'. ");
            $this->filterCSVFile($inputFile, $outputFile, $ar_filter_column);

            if(file_exists($outputFile)) {
                $this->logToFile("Constitution du fichier à géocoder");
                //This needs to be the full path to the file you want to send.
                $file_name_with_full_path = realpath($outputFile);
                //cURL headers for file uploading
                $headers = array("Content-Type:multipart/form-data");
                // data to post
                $postfields = array(
                    "data" => new \CURLFile($file_name_with_full_path)//,
                    //"citycode" => "code_insee_commune"
                );

                $curl = curl_init();
 
                // cURL options
                $options = array(
                    CURLOPT_URL => $this->REQUESTED_URL,
                    CURLOPT_POST => true,
                    CURLOPT_HTTPHEADER => $headers,
                    CURLOPT_POSTFIELDS => $postfields,
                    CURLOPT_HEADER => false, //Not return header
                    CURLOPT_SSL_VERIFYPEER => false, //Don't veryify server certificate
                    CURLOPT_RETURNTRANSFER => true
                );

                curl_setopt_array($curl, $options);
                $this->logToFile("Ouverture de la connexion CURL.");
                $geocoding_data = curl_exec($curl);

                curl_close($curl);
                
                $output = fopen($outputFile, 'w+');
                if(!$output) {
                    $this->logToFile("Echec dans l'ouverture du fichier de sortie.");
                    return null;
                }
                else {
                    $this->logToFile("Ouverture d'un fichier de sortie, en mode écriture pour enregistrer les information de géocodage.");
                    fwrite($output, $geocoding_data);

                    $this->logToFile("Fermeture du fichier de sortie.");
                    fclose($output);
                    $this->logToFile("Récupération du géocodage réussie : ".$outputFile."[".filesize($outputFile)."]");
                    return $outputFile;
                }
            }
        }
        else {
            $this->logToFile("Le fichier à géocoder n'existe pas");
            return null;
        }
    }
    

    /**
     * Archive data from input file (file to geocoding)
     * Import a geocoding data for specifics columns to input file/Update the specifics columns
     * @param : $inputFile
     * @param : $geocodingFile
     * use instead of importGeocodingData
     * */
    private function finalImportGeocodingData($inputFile, $geocodingFile, $id_column_name) {

        if((file_exists($inputFile)) && (file_exists($geocodingFile))) {
            $date = date('Y_m_d_H_i_s');

            $archiveDirectory = $this->GEOCODING_DATA . "/" . $date;
            @mkdir($archiveDirectory, 0770, true);

            if(is_dir($archiveDirectory)) {

                $delimiterI = $this->detectDelimiter($inputFile);
                $delimiterG = $this->detectDelimiter($geocodingFile);

                // Obtenir les indices des colonnes à modifier dans le fichier d'entrée
                $index_id_column_name = $this->returnIndexColumnName($inputFile, $delimiterI, $id_column_name);

                $index_longtitude_source = $this->returnIndexColumnName($inputFile, $delimiterI, "geolocalisation_longitude");
                $index_latitude_source = $this->returnIndexColumnName($inputFile, $delimiterI, "geolocalisation_latitude");
                $index_qualite_source = $this->returnIndexColumnName($inputFile, $delimiterI, "geolocalisation_qualite");

                // Obtenir les indices des colonnes cibles dans le fichier géocodée
                $index_longtitude_destination = max($this->returnIndexColumnName($geocodingFile, $delimiterG, "longitude"), $this->returnIndexColumnName($geocodingFile, $delimiterG, "Lng"));
                $index_latitude_destination = max($this->returnIndexColumnName($geocodingFile, $delimiterG, "latitude"), $this->returnIndexColumnName($geocodingFile, $delimiterG, "Lat"));
                $index_qualite_destination = max($this->returnIndexColumnName($geocodingFile, $delimiterG, "result_score"), $this->returnIndexColumnName($geocodingFile, $delimiterG, "Score"));

                if(($index_id_column_name != -1) && ($index_longtitude_source != -1) && ($index_latitude_source != -1) && ($index_qualite_source != -1) && ($index_longtitude_destination != -1) && ($index_latitude_destination != -1) && ($index_qualite_destination != -1)) {
                    $fileToReadI = fopen($inputFile, 'r');
                    $fileToReadG = fopen($geocodingFile, 'r');

                    $path_details = pathinfo($inputFile);

                    // Ouverture du fichier d'archivage
                    $archiveFile = $archiveDirectory . "/" . $path_details['filename'] . "_" . $date . "." . $path_details['extension'] ;
                    $fileToArchive = fopen($archiveFile, 'w');

                    // Ouverture du fichier du copie
                    $copyFile = $path_details['dirname'] . "/". $path_details['filename'] . "_" .$date . "." . $path_details['extension'];
                    $fileToCopy = fopen($copyFile, 'w');

                    // Ouverture du fichier qui va etre ajouter à la file d'attente
                    $queueFile = $path_details['dirname'] . "/". $path_details['filename'] . "_queue_" . $date . "." . $path_details['extension'];
                    $fileToAddToQueue = fopen($queueFile, 'w');

                    // Archivage et copie ...
                    if($fileToReadI && $fileToReadG && $fileToArchive && $fileToCopy && $fileToAddToQueue) {

                        $this->logToFile("Archivage du fichier à géocoder ...");
                        $this->logToFile("Mise à jour du fichier à géocoder ...");

                        $isHeader = true;

                        while((($rowU = fgets($fileToReadI)) !== FALSE) && (($rowG = fgets($fileToReadG)) !== FALSE)) {

                            $dataU = str_getcsv($rowU, $delimiterI);
                            $dataG = str_getcsv($rowG, $delimiterG);

                            $archiveData = array();
                            $updateData = array();
                            $queueData = array();

                            if($isHeader) {
                                // En tete, à prendre les mémes noms du fichier source/d'entrée
                                for($i=0 ; $i < count($dataU) ; $i++) {
                                    $archiveData[$i] = $dataU[$i];
                                    $queueData[$i] = $dataU[$i];
                                    switch($i) {
                                        case $index_id_column_name : $updateData[0] = $dataU[$index_id_column_name]; break;
                                        case $index_longtitude_source : $updateData[1] = "Lng"; break;
                                        case $index_latitude_source : $updateData[2] = "Lat"; break;
                                        case $index_qualite_source : $updateData[3] = "Score"; break;
                                        default : break;
                                    }
                                    $isHeader = false;
                                }
                            }
                            else {
                                for($i=0 ; $i < count($dataU) ; $i++) {
                                    $archiveData[$i] = $dataU[$i];
                                    switch($i) {
                                        case $index_id_column_name :
                                            $updateData[0] = $dataU[$index_id_column_name];
                                            $queueData[$index_id_column_name] = $dataU[$index_id_column_name];
                                            break;
                                        case $index_longtitude_source :
                                            $updateData[1] = (($dataG[$index_longtitude_destination] != null) && ($dataG[$index_longtitude_destination] != "")) ? $dataG[$index_longtitude_destination] : $dataU[$index_longtitude_source];
                                            $queueData[$index_longtitude_source] = (($dataG[$index_longtitude_destination] != null) && ($dataG[$index_longtitude_destination] != "")) ? $dataG[$index_longtitude_destination] : $dataU[$index_longtitude_source];
                                            break;
                                        case $index_latitude_source :
                                            $updateData[2] = (($dataG[$index_latitude_destination] != null) && ($dataG[$index_latitude_destination] != "")) ? $dataG[$index_latitude_destination] : $dataU[$index_latitude_source];
                                            $queueData[$index_latitude_source] = (($dataG[$index_latitude_destination] != null) && ($dataG[$index_latitude_destination] != "")) ? $dataG[$index_latitude_destination] : $dataU[$index_latitude_source];
                                            break;
                                        case $index_qualite_source :
                                            $updateData[3] = (($dataG[$index_qualite_destination] != null) && ($dataG[$index_qualite_destination] != "")) ? $dataG[$index_qualite_destination]*100 : $dataU[$index_qualite_source];
                                            $queueData[$index_qualite_source] = (($dataG[$index_qualite_destination] != null) && ($dataG[$index_qualite_destination] != "")) ? $dataG[$index_qualite_destination]*100 : $dataU[$index_qualite_source];
                                            break;
                                        default :
                                            $queueData[$i] = $dataU[$i];
                                            break;
                                    }
                                }
                            }
                            fputcsv($fileToArchive, $archiveData, ';');
                            fputcsv($fileToCopy, $updateData, ';');
                            fputcsv($fileToAddToQueue, $queueData, ';');
                        }

                        fclose($fileToReadI);
                        fclose($fileToReadG);
                        fclose($fileToArchive);
                        fclose($fileToCopy);
                        fclose($fileToAddToQueue);
                        
                        $this->logToFile("finalFile ! ".$copyFile);
                        $this->logToFile("finalNameFile". $geocodingFile);
                        
                        unlink($inputFile);
                        unlink($geocodingFile);
                        //renommage en import...
                        copy($copyFile, $geocodingFile);
                        unlink($copyFile);
                        
                        //return $geocodingFile;
                        return $queueFile;

                    }
                    else {
                        $this->logToFile("Echec dans l'ouverture des fichiers à traiter");
                    }
                }
                else {
                    $this->logToFile("Un problème dans la détection des indices des données à modifier/mettre à jour. ");
                }
            }
            else {
                $this->logToFile("Echec dans la création du répertoire d'archivage. ");
            }
        }
        else {
            $this->logToFile("Le fichier d'entrée et/ou le fichier géocodé n'existent pas. ");
        }
    }
    
    
} //end of class

?>
