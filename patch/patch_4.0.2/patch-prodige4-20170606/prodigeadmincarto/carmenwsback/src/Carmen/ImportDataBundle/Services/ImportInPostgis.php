<?php
namespace Carmen\ImportDataBundle\Services;

//Date de creation: 31 mai 2006
//Description: Classe permettant la gestion des imports de donnees POSTGIS
use Prodige\ProdigeBundle\Common\DBManager\ViewFactory;
use Carmen\ImportDataBundle\Controller\AbstractImportDataController;

require_once(__DIR__."/RessourcesLocal.php");
require_once(__DIR__."/ClassXLS.php");
require_once(__DIR__."/ClassODS.php");
//require_once($AdminPath."include/ClassXYZ.php");

class ImportInPostgis {
	static public $CMD_BIN_OGR2OGR	= "ogr2ogr";
	static public $EXT_ESRI				= ".shp";
	static public $EXT_MAPINFO_TAB		= ".tab";
	static public $EXT_MAPINFO_MIF      = ".mif";
	static public $EXT_GML              = ".gml";
	static public $EXT_KML              = ".kml";
	static public $EXT_GMT              = ".gmt";
	static public $EXT_JSON             = ".json";
	static public $EXT_GXT              = ".gxt";
	static public $EXT_CSV              = ".csv";
	static public $EXT_ODS              = ".ods";
	static public $EXT_XLS              = ".xls";
	static public $EXT_XYZ				= ".XYZ";
	static public $EXT_ASC				= ".ASC";
	static public $EXT_ZIP        = ".zip";
  
	//option -k conserve la case des attributs

	//deprecated
	//static public $CREATE_DATA					= "{BIN_SHP2PG} -I -D -s {SRS} -W {SHP_ENCODING} -c \"{FILE}\" {TABLE} {PG_BDD} | {BIN_PGSQL} {PG_USR} {PG_MDP} -d {PG_BDD} -h {PG_SRV} -p {PG_PRT}";
	static public $OGR2OGR_COMMAND          = array(
		"{BIN_OGR2OGR}",
		"{BIN_PGCLIENTENCODING}",
		"{S_SRS}",
		"{T_SRS}",
		"-f PostgreSQL PG:\"host={PG_SRV} user={PG_USR} password={PG_MDP} dbname={PG_BDD} {PG_DATAENCODING}\"",
		"\"{FILE}\"",
		"{SHEET}",
		"-nln {TABLE}",
		"-lco LAUNDER=yes -lco DIM=2 -lco GEOMETRY_NAME=the_geom -lco PRECISION=YES -nlt 'GEOMETRY' -lco FID=gid -gt 100000"
	);
	static public $OGR2OGR_TOSHAPEFILE      = array(
		"{BIN_OGR2OGR}",
		"{S_SRS}",
		"{T_SRS}",
		"\"{FILE}.shp\"",
		"\"{FILE}\"",
	);
	static public $CREATE_DATA          = array(
		"-lco OVERWRITE=YES -overwrite",
	);
	static public $CREATE_DATA_GEOREF   = array(
		"-dialect sqlite -sql \"SELECT MakePoint(CAST( {XFIELD} as REAL ), CAST( {YFIELD} as REAL ), {GEOM_SRS}) Geometry, * FROM {SHEETORFILENAME} \"",
		"-lco OVERWRITE=YES -overwrite",
	);
	static public $CREATE_DATA_TABLE    = array(
		"-lco OVERWRITE=YES -overwrite",
	);
		
	//deprecated
	//static public $RECREATE_DATA				= "{BIN_SHP2PG} -D -s {SRS} -W {SHP_ENCODING} -d \"{FILE}\" {TABLE} {PG_BDD} | {BIN_PGSQL} {PG_USR} {PG_MDP} -d {PG_BDD} -h {PG_SRV} -p {PG_PRT}";
		
	//deprecated
	//static public $UPDATE_DATA         = "{BIN_SHP2PG} -D -s {SRS} -W {SHP_ENCODING} -a -g {PG_COL_GEOM} \"{FILE}\" {TABLE} {PG_BDD} | {BIN_PGSQL} {PG_USR} {PG_MDP} -d {PG_BDD} -h {PG_SRV} -p {PG_PRT}";
	static public $UPDATE_DATA          = array(
		"-skipfailures -update -append",
	);
	static public $UPDATE_DATA_TABLE    = array(
		"-skipfailures -update -append",
	);

	//deprecated
	//static public $CONVERT_DATA					= "{BIN_TAB2SHP} -f \"ESRI Shapefile\" \"{REP_CONVERT}{FILE_CONVERT}\" \"{FILE}\"";

	static public $CMD_PATTERN_TBL 				= "{TABLE}";
	static public $CMD_PATTERN_FIC 				= "{FILE}";
	static public $CMD_PATTERN_FILENAME 	= "{FILENAME}";
	static public $CMD_PATTERN_SHEET 			= "{SHEET}";
	static public $CMD_PATTERN_SHEETORFILENAME 			= "{SHEETORFILENAME}";
	static public $CMD_PATTERN_S_SRS      = "{S_SRS}";
	static public $CMD_PATTERN_T_SRS 			= "{T_SRS}";
	static public $CMD_PATTERN_GEOM_SRS 			= "{GEOM_SRS}";
	static public $CMD_PATTERN_PG_DATAENCODING     = " {PG_DATAENCODING}";
	static public $CMD_PATTERN_SHP_ENCODING     = "{SHP_ENCODING}";
	static public $CMD_PATTERN_FIC_CONVERT		= "{FILE_CONVERT}";
	static public $CMD_PATTERN_REP_CONVERT		= "{REP_CONVERT}";
	static public $CMD_PATTERN_PG_SRV 			= "{PG_SRV}";
	static public $CMD_PATTERN_PG_PRT 			= "{PG_PRT}";
	static public $CMD_PATTERN_PG_BDD 			= "{PG_BDD}";
	static public $CMD_PATTERN_PG_USR 			= "{PG_USR}";
	static public $CMD_PATTERN_XFIELD 			= "{XFIELD}";
	static public $CMD_PATTERN_YFIELD 			= "{YFIELD}";
	static public $CMD_PATTERN_PG_MDP 			= "{PG_MDP}";
	static public $CMD_PATTERN_PG_GEOM 			= "{PG_COL_GEOM}";
		
	static public $CMD_PATTERN_BIN_PGCLIENTENCODING     = "{BIN_PGCLIENTENCODING}";
	static public $CMD_PATTERN_BIN_SHP2SQL 		= "{BIN_SHP2PG}";
	static public $CMD_PATTERN_BIN_PGSQL 		= "{BIN_PGSQL}";
	static public $CMD_PATTERN_BIN_TAB2SHP 		= "{BIN_TAB2SHP}";
	static public $CMD_PATTERN_BIN_OGR2OGR     = "{BIN_OGR2OGR}";
		
	static protected $ERR_NOTABLE			= "Aucune table de stockage POSTGIS n'est définie pour la couche de donnée.";
	static protected $ERR_NOFICHIER			= "Aucun non de fichier n'est défini pour la couche de donnée.";
		
	static protected $SQL_GETSRS			= "select srid from geometry_columns where f_table_name = :TBL";
	static protected $SQL_ISUNIQUE			= "select 1 from (select count(*)>1 as ct from \"{TBL}\" group by \"{CHP}\") f where ct limit 1";
	static protected $SQL_ISTABLEEXISTE		= "select * from information_schema.tables where table_name ilike :TBL and table_schema ilike :TBLSCHEMA";
	static protected $SQL_ISCOLUMNEXISTE		= "select 1 from information_schema.columns where table_schema=:table_schema and table_name=:table_name and column_name=:column_name";
	static protected $SQL_ISCONSTRAINTEXISTE		= "select 1 from information_schema.table_constraints where table_schema=:table_schema and table_name=:table_name and constraint_name=:constraint_name";
	static protected $SQL_DELETETABLE		= "drop table \"{TBL}\" cascade";
	static protected $SQL_MAXGID			= "select max({gid}) from \"{TBL}\"";
	static protected $SQL_DESC				= "select column_name, data_type from information_schema.columns where table_name=:TBL";
	static protected $SQL_SELECT_BY_GID		= "select \"{CLE}\" from \"{TBL}\" where {gid}>:value";
	static protected $SQL_DELETE_BY_CLE		= "delete from \"{TBL}\" where {gid}<=:value and \"{CLE}\"=:value2";
	static protected $SQL_COPYTABLE			= "create table \"{TBL_ARC}\" as (select * from \"{TBL}\")";
	static protected $SQL_ADDCOLUMN     	= "alter table {TBL} add {value} {value2}";
	static protected $SQL_PATTERN_TBL		= "{TBL}";
	static protected $SQL_PATTERN_TBLASVALUE		= "TBL";
	static protected $SQL_PATTERN_TBLARC	= "{TBL_ARC}";
	static protected $SQL_PATTERN_GID		= "{gid}";
	static protected $SQL_PATTERN_VAL		= "value";
	static protected $SQL_PATTERN_VAL2	= "value2";
	static protected $SQL_PATTERN_VALASNAME		= "{value}";
	static protected $SQL_PATTERN_VAL2ASNAME		= "{value2}";
	static protected $SQL_PATTERN_CHP		= "{CHP}";
	static protected $SQL_PATTERN_CLE		= "{CLE}";

	private $m_couche 	= null;
	private $m_error 	= null;
	private $m_exts     = null;
		
	private $m_table 			= null;
	private $m_tableArc			= null;
	private $m_fichier 			= null;
	private $m_feuille 			= null;
	private $m_fichierShape 	= null;
	private $m_BDD_Srv			= null;
	private $m_BDD_Port			= null;
	private $m_BDD_Name			= null;
	private $m_BDD_User			= null;
	private $m_BDD_Mdp			= null;
	private $db_encoding			= "UTF-8";
	private $data_encoding			= "UTF-8";
	/**
	 * @var AbstractImportDataController
	 */
	private $controller			= null;
	/**
	 * @var \Doctrine\DBAL\Connection
	 */
	private $db_connection			= null;
	/**
	 * @var \Psr\Log\LoggerInterface
	 */
	private $logger = null;
	private $m_fichierISO_noExt = null;
	private $m_fichierShortISO_noExt = null;
	private $tempTabIso = null;
	private $tempMapIso = null;
	private $tempIndIso = null;
	private $tempIdIso  = null;
	private $tempDatIso = null;
	
	private $file_fields = array();
	
	/**
	 * @param \Doctrine\DBAL\Connection $db_connection
	 */
	public function __construct(AbstractImportDataController $controller, $schema="public", $CMD_BIN_OGR2OGR="ogr2ogr", $db_encoding="UTF-8"){
		$this->controller = $controller;
		$this->logger = $controller->getLogger();
		$this->db_connection = $controller->getProdigeConnection($schema);
		$this->db_encoding = str_replace("-", "", $db_encoding);
		self::$CMD_BIN_OGR2OGR = $CMD_BIN_OGR2OGR;
		
		
		$this->m_exts = array(
			ImportInPostgis::$EXT_ESRI,
			ImportInPostgis::$EXT_MAPINFO_TAB,
			ImportInPostgis::$EXT_MAPINFO_MIF,
			ImportInPostgis::$EXT_GML,
			ImportInPostgis::$EXT_KML,
			ImportInPostgis::$EXT_GMT,
			ImportInPostgis::$EXT_JSON,
			ImportInPostgis::$EXT_GXT,
			ImportInPostgis::$EXT_CSV,
			ImportInPostgis::$EXT_ODS,
			ImportInPostgis::$EXT_XLS,
			ImportInPostgis::$EXT_XYZ,
			ImportInPostgis::$EXT_ASC,
			ImportInPostgis::$EXT_ZIP,
		);
	}
		
	//fonction qui permet de se connecter sur la base POSGIS
	// retourne la connexion POSTGIS
	private function isConnected(){
		return $this->db_connection->isConnected();
	}
		
	/**
	 * @return string|null
	 */
	private function getBDD_Srv(){return $this->db_connection->getHost();}
	
	/**
	 * @return string|null
	 */
	private function getBDD_Port(){return $this->db_connection->getPort();}
	
	/**
	 * @return string|null
	 */
	private function getBDD_Name(){return $this->db_connection->getDatabase();}
	
	/**
	 * @return string|null
	 */
	private function getBDD_User(){return $this->db_connection->getUsername();}
	
	/**
	 * @return string|null
	 */
	private function getBDD_Mdp(){return $this->db_connection->getPassword();}

	/**
	 * @return string|null
	 */
	private function getDataEncoding(){
		return ($this->data_encoding=="auto" ? "" : " options='-c client_encoding=".$this->data_encoding."'");
	}
	/**
	 * @return string|null
	 */
	private function getPgEncoding(){
		return ($this->data_encoding=="auto" ? "" : "--config PGCLIENTENCODING ".$this->db_encoding." ");
	}
	
	/**
	 * Affect the data available for the exécution
	 * @param string $table
	 * @param string $fichier
	 * @param integer $metadata_id
	 * @param string|null $fichierShape
	 * @throws \Exception
	 */
	public function setDataExecution($table, $fichier, $metadata_id = -1, $feuille = null, $fichierShape = null, $data_encoding='UTF-8') {
		$this->data_encoding = str_replace("-", "", $data_encoding);
		if(is_null($table))
			$this->m_error = ImportInPostgis::$ERR_NOTABLE;
		else if(is_null($fichier))
			$this->m_error = ImportInPostgis::$ERR_NOFICHIER;
		else {
			$this->m_table = $table;
			$this->m_fichier = $fichier;
			$multipleFiles = explode(" ;; ", $this->m_fichier);
			if(count($multipleFiles) > 1){
				//L'utilisateur a saisit plusieurs fichiers.
				//On les concatene dans un meme fichier
				if(strtoupper(substr($multipleFiles[(count($multipleFiles)-1)], -4)) == ".ASC"){
					//Cas de fichiers .asc, on fait la conversion en fichier xyz
					for($i = 1; $i < count($multipleFiles); $i++){
						$fileConvertedXYZ = str_ireplace(".ASC", ".XYZ", $multipleFiles[$i]);
						$res = array(); $return = "";
						$cmd = "gdal_translate -of XYZ '".$multipleFiles[0].$multipleFiles[$i]."' '".$multipleFiles[0].$fileConvertedXYZ."'";
						exec($cmd." 2>&1", $res, $return);
						if($return){
							throw new \Exception("Problème lors de la conversion du fichier ".$multipleFiles[$i]." à l'aide de gdal_translate.");
						}
					}
				}
				$conteneurFile = $multipleFiles[0]."concat_".$multipleFiles[1];
				if(file_exists($conteneurFile)){
					$res = array();$return = "";
					$cmd = "rm '".$conteneurFile."';";
					$this->logger->debug(__METHOD__, compact("cmd"));
					exec($cmd." 2>&1", $res, $return);
					if($return){
						throw new \Exception("Problème lors de l'écrasement du fichier concaténé.");
					}
				}
				for($i = 1; $i < count($multipleFiles); $i++){
					$res = array();$return = "";
					if(substr($multipleFiles[(count($multipleFiles)-1)], -4) == ".ASC"){
						$multipleFiles[$i] = str_ireplace(".ASC", ".XYZ", $multipleFiles[$i]);
						$conteneurFile = str_ireplace(".ASC", ".XYZ", $conteneurFile);
					}
					$cmd = "cat '".$multipleFiles[0].$multipleFiles[$i]."' >> '".$conteneurFile."';";
					exec($cmd." 2>&1", $res, $return);
					if($return){
						throw new \Exception("Problème lors de la mise en relation des fichiers sélectionnés.");
					}
				}
				$this->m_fichier = $conteneurFile;
			}
			$this->m_feuille = $feuille;
			$this->m_fichierShape = $fichierShape;
			$this->m_tableArc = \RessourcesLocal::$PREFIXE_TBL_ARC.($this->m_table).\RessourcesLocal::$POSTFIXE_TBL_ARC;
				

			$this->metadata_id = $metadata_id;
		}
	}

	/**
	 * @param string|array $query The SQL query with or without comma (simple or multiple queries)
	 * @param array  $params
	 * @return \Doctrine\DBAL\Driver\Statement
	 * @throws \Doctrine\DBAL\DBALException
	 */
	public function executeQuery($query, $params = array())
	{
		$stmt = null;
		$queries = (is_array($query) ? $query : explode(";", $query));
		foreach($queries as $query){
			$query = trim($query);
			if ( empty($query) ) continue;
			$query = preg_replace("!;$!", "", $query);
			$stmt= $this->db_connection->executeQuery($query, $params);
		}
		return $stmt;
	}
		
	public function getm_fichierShape()
	{
		return $this->m_fichierShape;
	}
	public function setm_fichier($m_fichier)
	{
		return $this->m_fichier = $m_fichier;
	}

	public function set_feuille($feuille)
	{
		return $this->m_feuille = $feuille;
	}

	public function setFileFields($file_fields)
	{
		$this->file_fields = $file_fields;
		return $this;
	}
		
	//test l'existence de la table POSTGIS
	public function IsExistTableStockage(){
		return $this->IsTableExist($this->m_table);
	}
	//retourne le message d'erreur
	public function GetErrorMessage(){
		$result = "";
		if (!is_null($this->m_error))
		$result = $this->m_error;
		return $result;
	}
	//retourne le nom de la table POSTGIS
	public function GetImportTable(){
		$result = "";
		if (!is_null($this->m_table))
		$result = $this->m_table;
		return $result;
	}
	//retourne le nom complet du fichier d'importr
	public function GetImportFichier(){
		$result = "";
		if (!is_null($this->m_fichier))
		$result = $this->m_fichier;
		return $result;
	}
	//retourne le nom complet du fichier d'importr
	public function GetImportFeuille(){
		$result = "";
		if (!is_null($this->m_feuille))
		$result = $this->m_feuille;
		return $result;
	}
	//retourne le nom court du fichier d'importr
	public function GetImportShortFichier(){
		$result = "Aucun fichier";
		if (!is_null($this->m_fichier)) {
			$result = $this->m_fichier;
			$pos = strrpos($result, "/");
			if (!(is_bool($pos) && !$pos)) {
				$result = substr ($result, $pos+1);
			}
		}
		return $result;
	}
		
	//function qui detecte si le type de fichier est pris en compte
	public function IsTypeFichierDetected(){
		$result = false;
		$extensionFile = $this->GetTypeFichier();
		for ($idx=0;$idx<count($this->m_exts);$idx++){
			if (strcasecmp($extensionFile, $this->m_exts[$idx]) == 0) $result = true;
		}
		return $result;
	}
	//function qui detecte si le type de fichier est pris en compte
	public function GetTypeFichier(){
		$result = strrchr($this->GetImportShortFichier(), ".");
		return $result;
	}
	// function qui retourne le type de fichier (extension) qui va servir de base pour l'import (m_fichierShape)
	// peut être différent du fichier d'origine (ex. passage ods/xls->csv pour création de données géométriques)
	public function GetTypeFichierShape(){
		$finfo = pathinfo($this->m_fichierShape);
		$result = "";
		if ( array_key_exists('extension', $finfo) ) {
			$result =  "." . strtolower($finfo['extension']);
		}
		return $result;
	}
		
	/**
	 *
	 * @return unknown_type
	 */
	public function GetImportShortFichierWithoutExtension(){
		return substr($this->GetImportShortFichier(), 0, strpos($this->GetImportShortFichier(), "."));
	}

		
	/**
	 * carmen v1.6
	 * @brief Get the encoding of the layer file
	 * @param
	 * @return
	 */
	public function GetFileEncoding(){
		$strEncoding = "";
		$strFile_contents = "";
		$strFile_text = "";

		$extension = substr($this->GetImportFichier(), strpos($this->GetImportFichier(),".")+1, strlen($this->GetImportFichier()));

		switch($extension){
			case "shp":
				$strFile_no_extension = str_replace(".shp","",$this->GetImportFichier());
				$strFile_text = $strFile_no_extension.".dbf";
				break;
			case "SHP":
				$strFile_no_extension = str_replace(".SHP","",$this->GetImportFichier());
				$strFile_text = $strFile_no_extension.".DBF";
				break;
			case "tab":
				$strFile_no_extension = str_replace(".tab","",$this->GetImportFichier());
				$strFile_text = $strFile_no_extension.".tab";
				break;
			case "TAB":
				$strFile_no_extension = str_replace(".TAB","",$this->GetImportFichier());
				$strFile_text = $strFile_no_extension.".TAB";
				break;
			case "mif":
				$strFile_no_extension = str_replace(".mif","",$this->GetImportFichier());
				$strFile_text = $strFile_no_extension.".mif";
				break;
			case "MIF":
				$strFile_no_extension = str_replace(".MIF","",$this->GetImportFichier());
				$strFile_text = $strFile_no_extension.".MIF";
				break;
		}

		//get encoding
		if (file_exists($strFile_text)){
			$strFile_contents = file_get_contents($strFile_text);
			$strEncoding = mb_detect_encoding($strFile_contents, "UTF-8, ISO-8859-1");
		}
		return $strEncoding;
	}

	/**
	 * DONE PRODIGE40
	 * @param $s_srs
	 * @param $t_srs
	 * @param $xField
	 * @param $yField
	 * @param $zField
	 * @return boolean
	 * @throws \Doctrine\DBAL\DBALException
	 */
	public function GeoreferenceData($s_srs, $t_srs, $xField, $yField, $zField=0){
		$bError = false;
		$table_schema = "public";
		$tabSql  = array("set search_path to {$table_schema},public;");
		if ( $this->IsConstraintExist($this->m_table, "enforce_srid_the_geom", $table_schema) ){
			$tabSql[] = "alter table ".$this->m_table." drop constraint enforce_srid_the_geom";
		}

		$tabSql[] = "update ".$this->m_table." 
                 set the_geom = ST_SetSRID(ST_MakePoint(\"".strtolower($xField)."\"::double precision, \"".strtolower($yField)."\"::double precision), :s_srs) 
                   WHERE COALESCE(\"".strtolower($xField)."\"::text, ' ') <> ' '
                   AND COALESCE(\"".strtolower($yField)."\"::text, ' ') <> ' '
                   AND COALESCE(\"".strtolower($xField)."\"::text, '') <> '' 
                   AND COALESCE(\"".strtolower($yField)."\"::text, '') <> '';";
    $tabSql[] = "update ".$this->m_table." set the_geom  = st_transform(the_geom, :t_srs)";
		$this->executeQuery($tabSql, array("table_name"=>$this->m_table, "s_srs"=>intval($s_srs), "t_srs"=>intval($t_srs)));
		
		if ( !$this->IsConstraintExist($this->m_table, "enforce_srid_the_geom", $table_schema) ){
      $this->executeQuery("alter table ".$this->m_table." add constraint enforce_srid_the_geom check (st_srid(the_geom) = {$t_srs})"); 
		}
		
		return $bError;
	}

	/**
	 * drop fields from table
	 * @param strDropFields   string of fields separated by ","
	 * @return true if OK or strDropFields is empty, otherwise false
	 */
	public function DropFields($strDropFields=""){
		if ( $strDropFields == "" ) return true;
		if ( $this->isConnected() ) {
			$tabDropFields = explode( ",", $strDropFields);
			$strSql = "";
			foreach ( $tabDropFields as $key => $value ) {
				$strSql .= "alter table ".$this->m_table." drop column \"".strtolower($value)."\";";
			}
			$this->executeQuery($strSql, array());
			return true;
		} else {
			return false;
		}
	}

	/**
	 * set the type of the fields in table
	 * @param $tabTypeFields object of types indexed by fields name
	 * @param $tabFieldsError array of fields error
	 */
	public function SetTypeFields($tabTypeFields=array(), &$tabFieldsError=array()){
		$result = true;
		$tabFields = $this->GetChampsPostGIS();
		if ( $this->isConnected() ) {
			if ( count($tabTypeFields) > 0 ) {
				foreach ( $tabTypeFields as $field => $type ) {
					$bSetType = false;
					$fieldLower = strtolower($field);
					$strSQL = "alter table ".$this->m_table." alter column \"".$fieldLower."\" type ";
					switch ( $type ) {
						case "varchar" :
							if ( $tabFields[$fieldLower] != "varchar" ) {
								$strSQL .= "varchar";
								$bSetType = true;
							}
							break;
						case "int4" :
							if ( $tabFields[$fieldLower] != "int" ) {
								$strSQL .= "int using cast(cast(\"".$fieldLower."\" as float8) as int) ";
								$bSetType = true;
							}
							break;
						case "float8" :
							if ( $tabFields[$fieldLower] != "float8" ) {
								$strSQL .= "float8 using cast(\"".$fieldLower."\" as float8) ";
								$bSetType = true;
							}
							break;
						case "date" :
							if ( $tabFields[$fieldLower] != "date" ) {
								$strSQL .= "date using cast(\"".$fieldLower."\" as date) ";
								$bSetType = true;
							}
							break;
					}
					if ( $bSetType ) {
						try {
							$this->executeQuery($strSQL, array());
							$result = $result && true;
						} catch(\Exception $exception){
							$result = false;
							$tabFieldsError[utf8_encode($field)] = array("from" => $tabFields[$fieldLower], "to" => $type);
						}
					}
				}
			}
		} else {
			$result = false;
		}
		return $result;
	}

	/**
	 * @deprecated v3.3 - 22 avr. 13
	 * fonction permettant de convertir en double precision les champs de type varchar contenant des données numériques
	 * à n'utiliser que pour les données issues du csv
	 * @return unknown_type
	 */
	public function setNumericFields(){
		/*
		 $dbconn = pg_connect("host=".$this->getBDD_Srv()." port=".$this->getBDD_Port()." dbname=".$this->getBDD_Name()." user=".$this->getBDD_User()." password=".$this->getBDD_Mdp());
		 $tabFields = $this->GetChampsPostGIS();
		 foreach($tabFields as $name => $type){
			if($type =="varchar"){
			$strSql = "select float8(".$name.") from ".$this->m_table;
			$result = @$this->executeQuery($strSql, array());
			if ($result) {
			$strSQL = "alter table ".$this->m_table." add column __".$name."__  double precision;".
			" UPDATE ".$this->m_table." SET  __".$name."__   = float8(".$name.");".
			"alter table ".$this->m_table." drop column ".$name."; ".
			"ALTER table ".$this->m_table."  RENAME COLUMN __".$name."__ TO ".$name.";";
			$result = @$this->executeQuery($strSql, array());
			}
			}
			}
			*/
	}

	/**
	 * update metadata date and attributes catalog
	 * TODO PRODIGE4.0 : services vers catalogues
	 */
	protected function updateMetadata()
	{
	    return;
// 		if ( !$this->metadata_id || $this->metadata_id<0 ) return;
// 		$tabChampsParam = array();
// 		$tabChamps = $this->GetChampsPostGIS();
// 		if ( is_array($tabChamps) ) {
// 			$strChamps = "";
// 			foreach ( $tabChamps as $name => $type ) {
// 				$champ = utf8_encode($name)."|";
// 				if ( array_key_exists($type, \RessourcesLocal::$PG_OGR_TYPE) && !is_null(\RessourcesLocal::$PG_OGR_TYPE[$type]["type"]) ) {
// 					$champ .= \RessourcesLocal::$PG_OGR_TYPE[$type]["type"];
// 				} else {
// 					$champ .= "non défini";
// 				}
// 				$tabChampsParam[] = $champ;
// 			}
// 		}
// 		$this->controller->updateGeosourceMetadata($this->metadata_id, $tabChampsParam, true);
// 		return;
	}
	
	public function getFeatureCatalogFields($table=null)
	{
		$tabChampsParam = array();
		$tabChamps = $this->GetChampsPostGIS($table);
		if ( is_array($tabChamps) ) {
			$strChamps = "";
			foreach ( $tabChamps as $name => $type ) {
				if ( array_key_exists($type, \RessourcesLocal::$PG_OGR_TYPE) && !is_null(\RessourcesLocal::$PG_OGR_TYPE[$type]["type"]) ) {
					$type = \RessourcesLocal::$PG_OGR_TYPE[$type]["type"];
				} else {
					$type = "Non défini";
				}
				$tabChampsParam[$name] = $type;
			}
		}
		return $tabChampsParam;
	}

	/**
	 * @since v3.3 - 23 avr. 13
	 * Import de table au format :
	 * - CSV seulement, utiliser Xls2csv or Ods2csv avant si nécessaire
	 */
	public function CreatingDataFromTable($strDropFields= "", $typeCouche="vecteur", $tabTypeFields=array()){
		$bError = false;
		$result = "";

		
		$fileToImport = $this->GetImportFichier();
		if(!is_null($this->m_fichierShape) && $this->m_fichierShape != "")
		$fileToImport =$this->m_fichierShape;
		$command = implode(" ", array_merge(ImportInPostgis::$OGR2OGR_COMMAND, ImportInPostgis::$CREATE_DATA_TABLE));
		$command = str_replace(ImportInPostgis::$CMD_PATTERN_FIC, $fileToImport, $command);
		$command = str_replace(ImportInPostgis::$CMD_PATTERN_SHEET, 			    (empty($this->m_feuille) ? "" : '"'.$this->m_feuille.'"'),						         $command);
		$command = str_replace(ImportInPostgis::$CMD_PATTERN_TBL, $this->m_table, $command);
		$command = str_replace(ImportInPostgis::$CMD_PATTERN_PG_SRV, 		  $this->getBDD_Srv(), 		           $command);
		$command = str_replace(ImportInPostgis::$CMD_PATTERN_PG_PRT, 		  $this->getBDD_Port(), 		         $command);
		$command = str_replace(ImportInPostgis::$CMD_PATTERN_PG_BDD, 		  $this->getBDD_Name(), 		         $command);
		$command = str_replace(ImportInPostgis::$CMD_PATTERN_PG_USR, 		  $this->getBDD_User(), 		         $command);
		$command = str_replace(ImportInPostgis::$CMD_PATTERN_PG_MDP, 		  $this->getBDD_Mdp(), 		           $command);
		$command = str_replace(ImportInPostgis::$CMD_PATTERN_PG_DATAENCODING, 		  $this->getDataEncoding(), 		           $command);
		$command = str_replace(ImportInPostgis::$CMD_PATTERN_BIN_OGR2OGR,  self::$CMD_BIN_OGR2OGR,  $command);
		$command = str_replace(ImportInPostgis::$CMD_PATTERN_BIN_PGCLIENTENCODING,  $this->getPgEncoding(),  $command);

		$command = str_replace(ImportInPostgis::$CMD_PATTERN_T_SRS,        "",                              $command);
		$command = str_replace(ImportInPostgis::$CMD_PATTERN_S_SRS,        "",                              $command);
		
		set_time_limit(300);

		// execute the command for data import
		$res = array();
		$return_var = 0;
		//die($command);
    $this->logger->debug(__METHOD__, array("cmd"=>$command));
		$this->db_connection->isTransactionActive() && $this->db_connection->commit();
		
		
		$output = exec($command." 2>&1", $res);
		!$this->db_connection->isTransactionActive() && $this->db_connection->beginTransaction();
		
		$tmp = array();
		$cumul = array();
		$reject = true;
		foreach($res as $line){
			if ( stripos($line, ":")!==false ){
				if ( !empty($cumul) )
					$tmp[] = implode(" ", $cumul);
				if ( stripos($line, "ERROR:")!==false ){
					$reject = false;
					$cumul = array($line);
				} else {
					$reject = true;
					$cumul = array();
				}
			} else if ( !$reject ) {
				$cumul[] = $line;
			}
		}
		$res = $tmp;
		
		// check if data import succeed of failed
		if ( count($res) != 0 ) {
			$this->logger->debug(__METHOD__, array_merge(array("[ligne=".__LINE__."]", "[cmd={$command}]"), $res));
			$result = implode("<br/>", array_merge(array("Une erreur s'est produite."/*, "[cmd={$command}]"*/), $res));
			throw new \Exception($result);
		}
		else {
			// checking if table exists
			$bExists = false;
			$strSql = "SELECT tablename FROM pg_tables where tablename='".$this->m_table."'";
			$strSql .= " UNION SELECT viewname as tablename FROM pg_views where viewname='".$this->m_table."'";
			try {
			  $result = $this->executeQuery($strSql, array());
				// removing geometrycolumn
				$query = //"alter table \"".$this->m_table."\" rename ogc_fid to gid;" .
				 "alter table \"".$this->m_table."\" drop constraint \"" .$this->m_table ."_pkey\";" .
				 "alter table \"".$this->m_table."\" add constraint \"" .$this->m_table ."_pkey\" PRIMARY KEY(gid);" .
				($typeCouche=="vecteur" ?
				  "select DropGeometryColumn('" . $this->m_table ."','wkb_geometry');"
				  : "");
				try {
			    if ( $bExists || $result->rowCount() ){
				    $this->executeQuery($query, array());
			    }
				} catch (\Exception $exception) {
				  $result = "Une erreur s'est produite lors de la création de la table \"".$this->m_table."\".";
				  $bError = true;
				}
			} catch (\Exception $exception) {
				$result = "Une erreur s'est produite : La table \"".$this->m_table."\" n'existe pas.'";
				$bError = true;
			}
		}

		if ( !$bError ) {
			// dropping unused fields
			$bOk = $this->DropFields($strDropFields);
			if ( !$bOk ) {
				$bError = true;
				$result = "Une erreur s'est produite : Impossible de supprimer les champs ".$strDropFields.".";
			}
		}

		if ( !$bError ) {
			// setting fields type
			$tabFieldsError = array();
			$bOk = $this->SetTypeFields($tabTypeFields, $tabFieldsError);
			if ( !$bOk ) {
				$bError = true;
				$result = "Une erreur s'est produite, impossible de modifier le type des champs :";
				foreach ( $tabFieldsError as $field => $tabConvert ) {
					$result .= "<br/>- ".$field." (".$tabConvert["from"]." par ".$tabConvert["to"].")";
				}
			}
		}

		if ( !$bError ) {
			$result = "Importation en \"Création\" terminée avec succès";
		}
		if ( $bError )throw new \Exception($result);
		

		$this->updateMetadata();

		return $result;
	}

	/**
	 * Import en création
	 * le fichier importé: $this->m_fichier
	 * la table PostGIS: $this->m_table
	 */
	public function CreatingData($s_srs = -1, $t_srs = -1, $xField="", $yField="", $zField="", $strDropFields= "", $typeCouche="vecteur", $tabTypeFields=array()){
		$bError = false;
		
		
		$fileToImport = $this->GetImportFichier();
		
		//check if needs conversion and convert the text files
		//$PGCLIENTENCODING = $this->GetFileEncoding();

		/*if (!is_null($this->tempTabIso) && $this->tempTabIso!=""){//cas Fichier MapInfo
		 $fileToImport = $this->tempTabIso;
			}*/
		if($this->m_fichierShape!="")
		  $fileToImport =$this->m_fichierShape;
		$this->logger->debug(__METHOD__, array("file_to_import"=>$fileToImport, "time"=>microtime(true)));
		if($xField != "" && $yField!=""){
			$create_command = ImportInPostgis::$CREATE_DATA_GEOREF;
		} else {
			$create_command = ImportInPostgis::$CREATE_DATA;
		}
		$extension = $this->GetTypeFichier();
		
		//Convert Mapinfo files to Shapefile for encoding resolution
		if ( strtolower($extension)==".tab" ){
		    
    		$command = implode(" ", array_merge(ImportInPostgis::$OGR2OGR_TOSHAPEFILE, $create_command));
    		$command = str_replace(ImportInPostgis::$CMD_PATTERN_FIC, 			    $fileToImport,			               $command);
		
    		if ( $s_srs != -1 ) {
    			$command = str_replace(ImportInPostgis::$CMD_PATTERN_S_SRS,        "-s_srs EPSG:".$s_srs,           $command);
    		} else {
    			$command = str_replace(ImportInPostgis::$CMD_PATTERN_S_SRS,        "",                              $command);
    		}
    		$command = str_replace(ImportInPostgis::$CMD_PATTERN_T_SRS, "-t_srs EPSG:".$t_srs, $command);
		    $command = str_replace(ImportInPostgis::$CMD_PATTERN_BIN_OGR2OGR,  self::$CMD_BIN_OGR2OGR,  $command);
    		
    		// execute the command for data import
    		$res = array();
    		$return_var = 0;
    		$output = exec($command." 2>&1", $res, $return_var);
        $this->logger->debug(__METHOD__, array("cmd"=>$command, "output"=>$output, "res"=>$res));
        
    		$tmp = array();
    		$cumul = array();
    		$reject = true;
    		foreach($res as $line){
    			if ( stripos($line, ":")!==false ){
    				if ( !empty($cumul) )
    					$tmp[] = implode(" ", $cumul);
    				if ( stripos($line, "ERROR")==0 ){
    					$reject = false;
    					$cumul = array($line);
    				} else {
    					$reject = true;
    					$cumul = array();
    				}
    			} else if ( !$reject ) {
    				$cumul[] = $line;
    			}
    		}
    		$res = array_map("utf8_encode", $tmp);
    		
    		// check if data import succeed of failed
    		if ( count($res) == 0 ) {
    		  $fileToImport = $fileToImport.".shp";
    		}
		}
		$command = implode(" ", array_merge(ImportInPostgis::$OGR2OGR_COMMAND, $create_command));
		$command = str_replace(ImportInPostgis::$CMD_PATTERN_FIC, 			    $fileToImport,			               $command);
		$command = str_replace(ImportInPostgis::$CMD_PATTERN_SHEET, 			    (empty($this->m_feuille) ? "" : '"'.$this->m_feuille.'"'),						         $command);
		$command = str_replace(ImportInPostgis::$CMD_PATTERN_SHEETORFILENAME, (empty($this->m_feuille) ? pathinfo($fileToImport, PATHINFO_FILENAME) : '"'.$this->m_feuille.'"'),	       $command);
		$command = str_replace(ImportInPostgis::$CMD_PATTERN_TBL, 			    $this->m_table,						         $command);

		/*if ( $s_srs!=-1 && $t_srs==$s_srs){
			$command = str_replace(ImportInPostgis::$CMD_PATTERN_S_SRS,        "",                              $command);
			$command = str_replace(ImportInPostgis::$CMD_PATTERN_T_SRS,        "",                              $command);
		}*/
		
		
		if($xField !="" && $yField!=""){
			//import CSV, create table in source projection, reprojection is done later
		
    		if ( $s_srs != -1 ) {
    			$command = str_replace(ImportInPostgis::$CMD_PATTERN_S_SRS,        "-s_srs EPSG:".$s_srs,           $command);
    		} else {
    			$command = str_replace(ImportInPostgis::$CMD_PATTERN_S_SRS,        "-s_srs EPSG:".$t_srs,                              $command);
    		}
			$command = str_replace(ImportInPostgis::$CMD_PATTERN_T_SRS,        "-t_srs EPSG:".$t_srs,                          $command);
			$command = str_replace(ImportInPostgis::$CMD_PATTERN_GEOM_SRS,     $t_srs,                          $command);
			$command = str_replace(ImportInPostgis::$CMD_PATTERN_FILENAME,     pathinfo($fileToImport, PATHINFO_FILENAME),                          $command);
			$command = str_replace(ImportInPostgis::$CMD_PATTERN_XFIELD,       $xField,                          $command);
			$command = str_replace(ImportInPostgis::$CMD_PATTERN_YFIELD,       $yField,                          $command);
		}
		else {
			$command = str_replace(ImportInPostgis::$CMD_PATTERN_T_SRS, "-t_srs EPSG:".$t_srs, $command);
		}
		
		if ( $s_srs != -1 ) {
			$command = str_replace(ImportInPostgis::$CMD_PATTERN_S_SRS,        "-s_srs EPSG:".$s_srs,           $command);
		} else {
			$command = str_replace(ImportInPostgis::$CMD_PATTERN_S_SRS,        "",                              $command);
		}
		$command = str_replace(ImportInPostgis::$CMD_PATTERN_PG_SRV, 		  $this->getBDD_Srv(), 		           $command);
		$command = str_replace(ImportInPostgis::$CMD_PATTERN_PG_PRT, 		  $this->getBDD_Port(), 		         $command);
		$command = str_replace(ImportInPostgis::$CMD_PATTERN_PG_BDD, 		  $this->getBDD_Name(), 		         $command);
		$command = str_replace(ImportInPostgis::$CMD_PATTERN_PG_USR, 		  $this->getBDD_User(), 		         $command);
		$command = str_replace(ImportInPostgis::$CMD_PATTERN_PG_MDP, 		  $this->getBDD_Mdp(), 		           $command);
		$command = str_replace(ImportInPostgis::$CMD_PATTERN_PG_DATAENCODING, 		  /*$this->getDataEncoding()*/"", 		           $command);
		$command = str_replace(ImportInPostgis::$CMD_PATTERN_PG_GEOM, 		  \RessourcesLocal::$GEOM_POSTGIS, 	 $command);
		$command = str_replace(ImportInPostgis::$CMD_PATTERN_BIN_OGR2OGR,  self::$CMD_BIN_OGR2OGR,  $command);

		
		
		// data supposed to be in LATIN1
		// need to be more generic...
		// getFileEncoding does not work...
		// $pg_encoding_param = $this->GetFileEncoding()=="ISO-8859-1" ? "--config PGCLIENTENCODING LATIN1" : "";

		// GML and KML format do not need PGCLIENTENCODING (encoding is specified into the file)
		$extension = substr($this->GetImportFichier(), strpos($this->GetImportFichier(),"."), strlen($this->GetImportFichier())); // extension with "."
	  if ( $extension ==  ImportInPostgis::$EXT_GML || $extension ==  ImportInPostgis::$EXT_KML ) {
			$pg_encoding_param = "";
		} else {
			$pg_encoding_param = $this->getPgEncoding();
		}
		$pg_encoding_param = ($this->data_encoding=="Latin1" ? " -oo ENCODING=Latin1" : $pg_encoding_param);
    $command = str_replace(ImportInPostgis::$CMD_PATTERN_BIN_PGCLIENTENCODING,  $pg_encoding_param,  $command);
    

			
		// execute the command for data import
		$res = array();
		$return_var = 0;
		
		//echo $command;
		
		$bBeforeTableExists = $this->IsTableExist($this->m_table);
		$bBeforeTableExists && $this->executeQuery(array(
			"drop table if exists ".$this->m_table."_tmp cascade",
			"create table ".$this->m_table."_tmp (like ".$this->m_table." including defaults including constraints including indexes)",
			"insert into ".$this->m_table."_tmp select * from ".$this->m_table
		));
		
		
		
		// sauvegarde de la valeur initiale de la table
		$this->db_connection->isTransactionActive() && $this->db_connection->commit();
		$output = exec($command." 2>&1", $res, $return_var);
    $this->logger->debug(__METHOD__, array("cmd"=>$command, "output"=>$output, "res"=>$res));
		!$this->db_connection->isTransactionActive() && $this->db_connection->beginTransaction();
		
		$tmp = array();
		$cumul = array();
		$reject = true;
		foreach($res as $line){
			if ( stripos($line, ":")!==false ){
				if ( !empty($cumul) )
					$tmp[] = implode(" ", $cumul);
				if ( stripos($line, "ERROR")==0 ){
					$reject = false;
					$cumul = array($line);
				} else {
					$reject = true;
					$cumul = array();
				}
			} else if ( !$reject ) {
				$cumul[] = $line;
			}
		}
		$res = array_map("utf8_encode", $tmp);
		
		// check if data import succeed of failed
		if ( count($res) != 0 ) {
			// restauration de la table à sa valeur initiale
			$bBeforeTableExists && $this->executeQuery(array(
				"drop table if exists ".$this->m_table." cascade",
				"alter table ".$this->m_table."_tmp  rename to ".$this->m_table
			));
			$this->logger->debug(__METHOD__, array_merge(array("[ligne=".__LINE__."]", "[cmd={$command}]"), $res));
			$result = implode("<br/>", array_merge(array("Une erreur s'est produite.", "[cmd={$command}]"), $res));
			throw new \Exception($result);
		} else {
			// suppression de la table de sauvegarde
			$bBeforeTableExists && $this->executeQuery(array(
				"drop table if exists ".$this->m_table."_tmp cascade",
			));
		}

		//check if table exists, if yes no error, else error
		$bExists = false;

		$bTableExists = $this->IsTableExist($this->m_table);
		if (!$bTableExists){
			$result = "Une erreur s'est produite : La table \"".$this->m_table."\" n'existe pas. [cmd={$command}]";
			$bError = true;
			$bBeforeTableExists && $this->executeQuery(array(
				"drop table if exists ".$this->m_table." cascade",
				"alter table ".$this->m_table."_tmp  rename to ".$this->m_table
			));
			$result = implode("<br/>", array_merge((array)$result, (array)$res));
			throw new \Exception($result);
		}
		
		if (!$bError && $bTableExists) {
			$bExists = true;
			$bError = false;
			//treat georef Import
			if($xField != "" && $yField!=""){
// 				$bError = $this->GeoreferenceData($s_srs, $t_srs, $xField, $yField, $zField);
// 				if ( $bError ) {
// 					$result = "Une erreur s'est produite : Le Géoréférencement a échoué.";
// 				}
			}
		}
		
		if (!$bError) {
			$bOk = $this->DropFields($strDropFields);
			if ( !$bOk ) {
				$bError = true;
				$result = "Une erreur s'est produite : Impossible de supprimer les champs ".$strDropFields.".";
			}
		}
   
		if (!$bError) {
			$tabFieldsError = array();
			$bOk = $this->SetTypeFields($tabTypeFields, $tabFieldsError);
			if ( !$bOk ) {
				$bError = true;
				$result = array("Une erreur s'est produite, impossible de modifier le type des champs :");
				foreach ( $tabFieldsError as $field => $tabConvert ) {
					$result[] = "-".$field." (".$tabConvert["from"]." par ".$tabConvert["to"].")";
				}
				$result = implode("<br/>", $result);
			}
		}

		//ogr2ogr stops if a layer contains multiple geometries
		//solution : load all the layers with geometry type = GEOMETRY and then update
		//the table geometry columns
		//so POINT layers have geometry type = POINT in geometry_columns
		//so LINESTRING layers have geometry type = MULTILINESTRING in geometry_columns
		//so POLYGON layers will have geometry type = MULTIPOLYGON in geometry_columns
		//then a  multi(the_geom) is executed on all the tables with linestring and polygons
		//so they only have multilinestring or multipolygon
// 			if ( !$bError && $bExists ) {
// 				$query = 'select distinct public.geometrytype(the_geom) from "'.$this->m_table.'"';
// 				try {
// 					$stmt = $this->executeQuery($query, array());
			
// 					$geometry_type = $stmt->fetchColumn(0);

// 					$query = array();
// 					switch ($geometry_type){
// 						case 'POINT':
// 							$query[] = "update geometry_columns set \"type\"='POINT' where f_table_name=:".self::$SQL_PATTERN_TBLASVALUE;
// 							break;
// 						case 'LINESTRING':
// 							$query[] = "update geometry_columns set \"type\"='LINESTRING' where f_table_name=:".self::$SQL_PATTERN_TBLASVALUE;
// 							break;
// 						case 'MULTILINESTRING':
// 							$query[] = "update geometry_columns set \"type\"='MULTILINESTRING' where f_table_name=:".self::$SQL_PATTERN_TBLASVALUE;
// 							//effectue un multi(the_geom) sur la table
// 							$query[] = "update {TBL} set the_geom=multi(the_geom)";
// 							break;
// 						case 'POLYGON':
// 							$query[] = "update geometry_columns set \"type\"='POLYGON' where f_table_name=:".self::$SQL_PATTERN_TBLASVALUE;
// 							break;
// 						case 'MULTIPOLYGON':
// 							$query[] = "update geometry_columns set \"type\"='MULTIPOLYGON' where f_table_name=:".self::$SQL_PATTERN_TBLASVALUE;
// 							//effectue un multi(the_geom) sur la table
// 							$query[] = "update {TBL} set the_geom=multi(the_geom)";
// 							break;
// 					}
				
// 					!empty($query) && $this->executeQuery(str_replace(self::$SQL_PATTERN_TBL, '"'.$this->m_table.'"', $query), array(self::$SQL_PATTERN_TBLASVALUE=>$this->m_table));
// 				} catch (\Exception $exception){
// 					$bError = true;
// 					$result = "Une erreur s'est produite : Impossible de mettre à jour le type de géométrie des données. [{$exception->getMessage()}]";
// 				}
// 			}

		if ( !$bError ) {
			$result = "Importation en \"Création\" terminée avec succès";
			//changement de ogc_fid en gid
			//$query="alter table \"".$this->m_table."\" rename ogc_fid to gid";

			//$rs=$this->executeQuery($query, array());
			//pg_query($dbconn,"commit");
			//create gis index on the_geom
			try {
				$this->db_connection->isTransactionActive() && $this->db_connection->commit();
				$query = array();
				$query[] = "VACUUM ANALYZE \"{TBL}\" (the_geom)";
				$this->executeQuery(str_replace(self::$SQL_PATTERN_TBL, $this->m_table, $query), array());
				
			} catch (\Exception $exception){
				$this->db_connection->isTransactionActive() && $this->db_connection->rollBack();
			}
		} else {
			$this->db_connection->isTransactionActive() && $this->db_connection->rollBack();
		}
		if ( $bError ){
			try {
				!$this->db_connection->isTransactionActive() && $this->db_connection->beginTransaction();
				$this->executeQuery("DROP TABLE IF EXISTS \"".$this->m_table."\"  cascade", array());
				$this->db_connection->isTransactionActive() && $this->db_connection->commit();
			}catch(\Exception $exception){
				$this->db_connection->isTransactionActive() && $this->db_connection->rollBack();
			}
			throw new \Exception($result);
		}
    $this->logger->debug(__METHOD__, array("file imported"=>$fileToImport, "time"=>microtime(true)));
		$this->updateMetadata();

    $this->logger->debug(__METHOD__, array("metadata updated"=>$fileToImport, "time"=>microtime(true)));
		return $result;

	}
		
	/**
	 * rebuild views destroyed by a table/layer replacing
	 * TODO PRODIGE4.0 : migration
	 */
	protected function rebuildViews()
	{
		$factory = ViewFactory::getInstance($this->db_connection);
		$viewIds = $factory->getViewsFromTablename($this->m_table);
		foreach ( $viewIds as $viewId ) {
			$view = $factory->loadView($viewId);
			$c_view = $factory->loadCompositeView($viewId);
			$view->removeFromDb();
			$view->storeInDb();
			$c_view->removeFromDb();
			$c_view->storeInDb();
		}
	}
	
	
	/**
	 * remove views before replace table/layer
	 * TODO PRODIGE4.0 : migration
	 */
	protected function removeViews()
	{
		$factory = ViewFactory::getInstance($this->db_connection);
	  $viewIds = $factory->getViewsFromTablename($this->m_table);
	  foreach ( $viewIds as $viewId ) {
	    $view = $factory->loadView($viewId);
	    $c_view = $factory->loadCompositeView($viewId);
	    $view->removeFromDb();
	    $c_view->removeFromDb();
	  }
	}
	

	/**
	 * @since v3.3 - 23 avr. 13
	 * Import en annule et remplace de table au format :
	 * - CSV seulement, utiliser Xls2csv or Ods2csv avant si nécessaire
	 * TODO PRODIGE4.0 : migration
	 */
	public function ReplacingDataFromTable($strDropFields= "", $typeCouche="vecteur", $tabTypeFields=array()){
	  $this->removeViews();
	  $result=$this->CreatingDataFromTable($strDropFields, $typeCouche, $tabTypeFields);
		if (strrpos($result,"erreur")==""){
			$result= "Importation en \"Annule et Remplace\" terminée avec succès";
		}
		$this->rebuildViews();

		return $result;
	}

	/**
	 * Import en annule et remplace
	 * le fichier importe: $this->m_fichier
	 * la table PostGIS: $this->m_table
	 */
	public function ReplacingData($s_srs = -1, $t_srs = -1, $xField="", $yField="", $zField="", $strDropFields= "", $typeCouche="vecteur", $tabTypeFields=array()){

		// create table temporaire
		/*
		if ( $typeCouche == "table" ) {
		$table = $this->m_table;
		$table_temp = $this->m_table."_temp";
		$this->m_table = $table_temp;
		}
		*/		  
	  $this->removeViews();
		$strDropFields = htmlspecialchars($strDropFields, ENT_QUOTES);			
		$result=$this->CreatingData($s_srs, $t_srs, $xField, $yField, $zField, $strDropFields, $typeCouche, $tabTypeFields);
		if (strrpos($result,"erreur")==""){
			$result= "Importation en \"Annule et Remplace\" terminée avec succès";

			// delete old table and insert new values
			/*
			if ( $typeCouche == "table" ) {
			$this->m_table = $table;

			if ( $this->AddNewFields($table_temp, $table, array("the_geom")) ) {
			$dbconn = pg_connect("host=".$this->getBDD_Srv()." port=".$this->getBDD_Port()." dbname=".$this->getBDD_Name()." user=".$this->getBDD_User()." password=".$this->getBDD_Mdp());
			$query ="";
			$query.="alter table \"".$table_temp."\" drop column the_geom;";
			$query.="delete from ".$table.";";
			$this->executeQuery($query, array());
			pg_query($dbconn,"commit");
			$query="insert into ".$table." select * from ".$table_temp.";";
			$query.="drop table \"".$table_temp."\" cascade;";
			$this->executeQuery($query, array());
			pg_query($dbconn,"commit");
			} else {
			$result = "Une erreur s'est produite : Impossible de modifier la table \"".$this->m_table."\".";
			}
			}*/

		}
		$this->rebuildViews();

		return $result;
	}
		
	/**
	 * add new fields from table1 to table2
	 */
	/*
	 protected function AddNewFields($table1, $table2, $tabFieldsIgnore=array()){
	 $dbconn = $this->isConnected();
	 if ( $dbconn ) {

	 // get fields from table1
	 $tabFieldsTable1 = array();
	 $query="select * from ".$table1;
	 $sqlResu = @pg_exec($dbconn,$query);
	 $statusResuCmd = pg_result_status($sqlResu, PGSQL_STATUS_STRING);
	 if ($statusResuCmd != "SELECT") {
	 return false;
	 }
	 else {
	 $numfields = pg_num_fields($sqlResu);
	 for($fn=0; $fn<$numfields; $fn++) {
	 $strFieldName = pg_field_name($sqlResu,$fn);
	 $strFieldType = pg_field_type($sqlResu,$fn);
	 $tabFieldsTable1[$strFieldName] = $strFieldType;
	 }
	 }

	 // get fields from table2
	 $tabFieldsTable2 = array();
	 $query="select * from ".$table2;
	 $sqlResu = @pg_exec($dbconn,$query);
	 $statusResuCmd = pg_result_status($sqlResu, PGSQL_STATUS_STRING);
	 if ($statusResuCmd != "SELECT") {
	 return false;
	 }
	 else {
	 $numfields = pg_num_fields($sqlResu);
	 for($fn=0; $fn<$numfields; $fn++) {
	 $strFieldName = pg_field_name($sqlResu,$fn);
	 $strFieldType = pg_field_type($sqlResu,$fn);
	 $tabFieldsTable2[$strFieldName] = $strFieldType;
	 }
	 }

	 // create new fields
	 foreach ( $tabFieldsTable1 as $strFieldName => $strFieldType ) {
	 if ( !in_array(strtolower($strFieldName), $tabFieldsIgnore) && !array_key_exists($strFieldName, $tabFieldsTable2) ) {
	 $query="alter table ".$table2." add column \"".$strFieldName."\" ".$strFieldType;echo $query."\n\n";
	 $this->executeQuery($query, array());
	 pg_query($dbconn,"commit");
	 }
	 }

	 pg_close($dbconn);
	 return true;
	 } else {
	 return false;
	 }
	 }
	 */

	/**
	 * @since v3.3 - 23 avr. 13
	 * Import en mise à jour de table au format :
	 * - CSV seulement, utiliser Xls2csv or Ods2csv avant si nécessaire
	 */
	public function UpdatingDataFromTable($champPk, $strDropFields= "", $tabTypeFields=array()){
		$bError = false;
		$result = "";


		$strChampsAdd = $this->addNewFields($strDropFields, $tabTypeFields);

		$maxGid = $this->GetGidMax();
		$fileShape = $this->GetImportFichier();
		if (!is_null($this->m_fichierShape) && $this->m_fichierShape != "")
		$fileShape = $this->m_fichierShape;
		/*
		 // ogr update does not handles well -update -append option and -nln to rename table...
		 // so first renaming csv file to match table name
		 $newFileShape = sys_get_temp_dir() . "/" . $this->m_table . ".csv";
		 if (file_exists($newFileShape))
		 @unlink($newFileShape);
		 $res = copy($fileShape, $newFileShape);
		 	
		 if (!$res) {
		 $bError = true;
		 $result = "Une erreur s'est produite. Impossible de renommer le fichier " . basename($fileShape) . " pour l'importer.";
		 }
		 else {*/
		$command = implode(" ", array_merge(ImportInPostgis::$OGR2OGR_COMMAND, ImportInPostgis::$UPDATE_DATA_TABLE));
		$command = str_replace(ImportInPostgis::$CMD_PATTERN_FIC, 			   $fileShape,							          $command);
		$command = str_replace(ImportInPostgis::$CMD_PATTERN_SHEET, 			    (empty($this->m_feuille) ? "" : '"'.$this->m_feuille.'"'),						         $command);
		$command = str_replace(ImportInPostgis::$CMD_PATTERN_TBL, 			   $this->m_table,						        $command);
		$command = str_replace(ImportInPostgis::$CMD_PATTERN_PG_SRV, 		 $this->getBDD_Srv(), 		          $command);
		$command = str_replace(ImportInPostgis::$CMD_PATTERN_PG_PRT, 		 $this->getBDD_Port(), 		          $command);
		$command = str_replace(ImportInPostgis::$CMD_PATTERN_PG_BDD, 		 $this->getBDD_Name(), 		          $command);
		$command = str_replace(ImportInPostgis::$CMD_PATTERN_PG_USR, 		 $this->getBDD_User(), 		          $command);
		$command = str_replace(ImportInPostgis::$CMD_PATTERN_PG_MDP, 		 $this->getBDD_Mdp(), 		          $command);
		$command = str_replace(ImportInPostgis::$CMD_PATTERN_PG_DATAENCODING, 		  $this->getDataEncoding(), 		           $command);
		$command = str_replace(ImportInPostgis::$CMD_PATTERN_BIN_OGR2OGR,  self::$CMD_BIN_OGR2OGR,  $command);
		$command = str_replace(ImportInPostgis::$CMD_PATTERN_BIN_PGCLIENTENCODING,  $this->getPgEncoding(),  $command);
		
		// execute the command for data import
		set_time_limit(300);
		$res = array();
		$return_var = 0;
		//die($command);
    $this->logger->debug(__METHOD__, array("cmd"=>$command));
		$this->db_connection->isTransactionActive() && $this->db_connection->commit();
		$output = exec($command." 2>&1", $res);
		!$this->db_connection->isTransactionActive() && $this->db_connection->beginTransaction();
		$tmp = array();
		$cumul = array();
		$reject = true;
		foreach($res as $line){
			if ( stripos($line, ":")!==false ){
				if ( !empty($cumul) )
					$tmp[] = implode(" ", $cumul);
				if ( stripos($line, "ERROR:")!==false ){
					$reject = false;
					$cumul = array($line);
				} else {
					$reject = true;
					$cumul = array();
				}
			} else if ( !$reject ) {
				$cumul[] = $line;
			}
		}
		$res = $tmp;
		// check if data import succeed of failed
		if ( count($res) != 0 ) {
			$bError = true;
			$this->logger->debug(__METHOD__, array_merge(array("[ligne=".__LINE__."]", "[cmd={$command}]"), $res));
			$result = implode("<br/>", array_merge(array("Une erreur s'est produite."/*, "[cmd={$command}]"*/), $res));
			throw new \Exception($result);
		}
		else {
			if (!is_null($maxGid)){     // la table n'etait pas vide, il faut faire un UPDATE MANUEL
				//SELECTION des enregistrement avec un gid > gidMax
				//$SQL_SELECT_BY_GID		= "select {CLE} from \"{TBL}\" where {gid}>:value";
				$lstNewEnregistrements = array();
				$params = array();
				$m_sSql = ImportInPostgis::$SQL_SELECT_BY_GID;
				$m_sSql = str_replace(ImportInPostgis::$SQL_PATTERN_TBL, $this->m_table, 				$m_sSql);
				$m_sSql = str_replace(ImportInPostgis::$SQL_PATTERN_GID, \RessourcesLocal::$GID_POSTGIS, 	$m_sSql);
				$m_sSql = str_replace(ImportInPostgis::$SQL_PATTERN_CLE, $champPk, 						$m_sSql);
				$params[ImportInPostgis::$SQL_PATTERN_VAL] = $maxGid;
				
			
				$sqlResu = $this->executeQuery($m_sSql, $params);
				foreach($sqlResu as $idx=>$row){
					$lstNewEnregistrements[$idx] = current($row);
				}

				//Pour chaque enregistrement detecte,
				//supprime l'enregistrement avec la mm cle et un gid < gidMax
				//$SQL_DELETE_BY_CLE		= "delete from \"{TBL}\" where {gid}<=:value and {CLE}=:value2";
				for ($idx=0;$idx<count($lstNewEnregistrements);$idx++){
				  $params = array();
					$m_sSql = ImportInPostgis::$SQL_DELETE_BY_CLE;
					$m_sSql = str_replace(ImportInPostgis::$SQL_PATTERN_TBL, 	$this->m_table, 				$m_sSql);
					$m_sSql = str_replace(ImportInPostgis::$SQL_PATTERN_GID, 	\RessourcesLocal::$GID_POSTGIS, 	$m_sSql);
					$m_sSql = str_replace(ImportInPostgis::$SQL_PATTERN_CLE, 	$champPk, 						$m_sSql);
					$params[ImportInPostgis::$SQL_PATTERN_VAL] = $maxGid;
					$params[ImportInPostgis::$SQL_PATTERN_VAL2] = $lstNewEnregistrements[$idx];
				  $sqlResu = $this->executeQuery($m_sSql, $params);
				}
			}
		}

		if (!$bError) {
			// dropping unused fields
			$bOk = $this->DropFields($strDropFields);
			if ( !$bOk ) {
				$bError = true;
				$result = "Une erreur s'est produite : Impossible de supprimer les champs ".$strDropFields.".";
			}
		}

		if (!$bError) {
			// setting fields type
			$tabFieldsError = array();
			$bOk = $this->SetTypeFields($tabTypeFields, $tabFieldsError);
			if ( !$bOk ) {
				$bError = true;
				$result = "Une erreur s'est produite, impossible de modifier le type des champs :";
				foreach ( $tabFieldsError as $field => $tabConvert ) {
					$result .= "<br/>- ".$field." (".$tabConvert["from"]." par ".$tabConvert["to"].")";
				}
			}
		}

		if ( !$bError ) {
			$result = "Importation en \"Mise à jour\" terminée avec succès ".$strChampsAdd;
		}
		
		if ( $bError ) throw new \Exception($result);

		$this->updateMetadata();
		return $result;
	}

	/**
	 * Import en mise a jour
	 * le fichier importe: $this->m_fichier
	 * la table PostGIS: $this->m_table
	 */
	public function UpdatingData($champPk, $s_srs = -1, $t_srs = -1,  $xField="", $yField="", $zField="", $strDropFields= "", $typeCouche="vecteur", $tabTypeFields=array()){
		$bError = false;

		$maxGid = $this->GetGidMax();
		$fileShape = $this->GetImportFichier();
		if (!is_null($this->m_fichierShape)) {
			$fileShape = $this->m_fichierShape;
		}

		$strChampsAdd = $this->addNewFields($strDropFields, $tabTypeFields);

		$command = implode(" ", array_merge(ImportInPostgis::$OGR2OGR_COMMAND, ImportInPostgis::$UPDATE_DATA));
		$command = str_replace(ImportInPostgis::$CMD_PATTERN_FIC, 			   $fileShape,							          $command);
		$command = str_replace(ImportInPostgis::$CMD_PATTERN_SHEET, 			    (empty($this->m_feuille) ? "" : '"'.$this->m_feuille.'"'),						         $command);
		$command = str_replace(ImportInPostgis::$CMD_PATTERN_TBL, 			   $this->m_table,						        $command);
		if ( $s_srs != -1 ) {
			$command = str_replace(ImportInPostgis::$CMD_PATTERN_S_SRS,       "-s_srs EPSG:".$s_srs,           $command);
		} else {
			if ( $typeCouche == "table" ) { // add a source srs for table otherwise the ogr command does not work
				$command = str_replace(ImportInPostgis::$CMD_PATTERN_S_SRS,       "-s_srs EPSG:".$t_srs,           $command);
			} else {
				$command = str_replace(ImportInPostgis::$CMD_PATTERN_S_SRS,       "",                              $command);
			}
		}
		if($xField !="" && $yField!=""){
			//import CSV, create table in source projection, reprojection is done later
			$command = str_replace(ImportInPostgis::$CMD_PATTERN_T_SRS, 		   "-t_srs EPSG:".$s_srs,  				                $command);
		}
		else{
			$command = str_replace(ImportInPostgis::$CMD_PATTERN_T_SRS,        "-t_srs EPSG:".$t_srs,                          $command);
		}
		
		$command = str_replace(ImportInPostgis::$CMD_PATTERN_PG_SRV, 		 $this->getBDD_Srv(), 		          $command);
		$command = str_replace(ImportInPostgis::$CMD_PATTERN_PG_PRT, 		 $this->getBDD_Port(), 		          $command);
		$command = str_replace(ImportInPostgis::$CMD_PATTERN_PG_BDD, 		 $this->getBDD_Name(), 		          $command);
		$command = str_replace(ImportInPostgis::$CMD_PATTERN_PG_USR, 		 $this->getBDD_User(), 		          $command);
		$command = str_replace(ImportInPostgis::$CMD_PATTERN_PG_MDP, 		 $this->getBDD_Mdp(), 		          $command);
		$command = str_replace(ImportInPostgis::$CMD_PATTERN_PG_DATAENCODING, 		  $this->getDataEncoding(), 		           $command);
		$command = str_replace(ImportInPostgis::$CMD_PATTERN_PG_GEOM, 		 \RessourcesLocal::$GEOM_POSTGIS, 	  $command);
		$command = str_replace(ImportInPostgis::$CMD_PATTERN_BIN_OGR2OGR,  self::$CMD_BIN_OGR2OGR,  $command);
		//$command = str_replace(ImportInPostgis::$CMD_PATTERN_BIN_PGCLIENTENCODING,  \RessourcesLocal::$CMD_BIN_PGCLIENTENCODING,  $command);

		// data supposed to be in LATIN1
		// need to be more generic...
		// getFileEncoding does not work...
		// $pg_encoding_param = $this->GetFileEncoding()=="ISO-8859-1" ? "--config PGCLIENTENCODING LATIN1" : "";

		// GML and KML format do not need PGCLIENTENCODING (encoding is specified into the file)
		$extension = substr($this->GetImportFichier(), strpos($this->GetImportFichier(),"."), strlen($this->GetImportFichier())); // extension with "."
		if ( $extension ==  ImportInPostgis::$EXT_GML || $extension ==  ImportInPostgis::$EXT_KML ) {
			$pg_encoding_param = "";
		} else {
			$pg_encoding_param = $this->getPgEncoding();
		}
		if ( $extension ==  ImportInPostgis::$EXT_ESRI) {
			$pg_encoding_param .=" --config SHAPE_ENCODING \"".($this->GetFileEncoding()=="ISO-8859-1" ? "LATIN1" : "")."\"";
		}
		$command = str_replace(ImportInPostgis::$CMD_PATTERN_BIN_PGCLIENTENCODING,  $pg_encoding_param,  $command);
		
		set_time_limit(300);

		// execute the command for data import
		$res = array();
		$return_var = 0;
		
    $this->logger->debug(__METHOD__, array("cmd"=>$command));
		$this->db_connection->isTransactionActive() && $this->db_connection->commit();
		$output = exec($command." 2>&1", $res);
		!$this->db_connection->isTransactionActive() && $this->db_connection->beginTransaction();
		$tmp = array();
		$cumul = array();
		$reject = true;
		foreach($res as $line){
			if ( stripos($line, ":")!==false ){
				if ( !empty($cumul) )
					$tmp[] = implode(" ", $cumul);
				if ( stripos($line, "ERROR:")!==false ){
					$reject = false;
					$cumul = array($line);
				} else {
					$reject = true;
					$cumul = array();
				}
			} else if ( !$reject ) {
				$cumul[] = $line;
			}
		}
		$res = $tmp;

		// check if data import succeed of failed
		if ( count($res) != 0 ) {
			$this->logger->debug(__METHOD__, array_merge(array("[ligne=".__LINE__."]", "[cmd={$command}]"), $res));
			$result = implode("<br/>", array_merge(array("Une erreur s'est produite."/*, "[cmd={$command}]"*/), $res));
			throw new \Exception($result);
		}

		if (!is_null($maxGid)){     //la table n'etait pas vide, il faut faire un UPDATE MANUEL
			//SELECTION des enregistrement avec un gid > gidMax
			//$SQL_SELECT_BY_GID		= "select {CLE} from \"{TBL}\" where {gid}>:value";
			$lstNewEnregistrements = array();
			$params = array();
			$m_sSql = ImportInPostgis::$SQL_SELECT_BY_GID;
			$m_sSql = str_replace(ImportInPostgis::$SQL_PATTERN_TBL, $this->m_table, 				$m_sSql);
			$m_sSql = str_replace(ImportInPostgis::$SQL_PATTERN_GID, \RessourcesLocal::$GID_POSTGIS, 	$m_sSql);
			$m_sSql = str_replace(ImportInPostgis::$SQL_PATTERN_CLE, $champPk, 						$m_sSql);
			$params[ImportInPostgis::$SQL_PATTERN_VAL] = $maxGid;
		
			$sqlResu = $this->executeQuery($m_sSql, $params);
			foreach($sqlResu as $idx=>$row){
				$lstNewEnregistrements[$idx] = current($row);
			}
			
				
			//Pour chaque enregistrement detecte,
			//supprime l'enregistrement avec la mm cle et un gid < gidMax
			//$SQL_DELETE_BY_CLE		= "delete from \"{TBL}\" where {gid}<=:value and {CLE}=:value2";
			for ($idx=0;$idx<count($lstNewEnregistrements);$idx++){
				$params = array();
				$m_sSql = ImportInPostgis::$SQL_DELETE_BY_CLE;
				$m_sSql = str_replace(ImportInPostgis::$SQL_PATTERN_TBL, 	$this->m_table, 				$m_sSql);
				$m_sSql = str_replace(ImportInPostgis::$SQL_PATTERN_GID, 	\RessourcesLocal::$GID_POSTGIS, 	$m_sSql);
				$m_sSql = str_replace(ImportInPostgis::$SQL_PATTERN_CLE, 	$champPk, 						$m_sSql);
				$params[ImportInPostgis::$SQL_PATTERN_VAL] = $maxGid;
				$params[ImportInPostgis::$SQL_PATTERN_VAL2] = $lstNewEnregistrements[$idx];
				$sqlResu = $this->executeQuery($m_sSql, $params);
			}
		}

		if($xField != "" && $yField!=""){
			//cas des fichiers CSV
			$bError = $this->GeoreferenceData($s_srs, $t_srs, $xField, $yField, $zField);
			if ( $bError ) {
				$result = "Une erreur s'est produite : Le Géoréférencement a échoué.";
			}
		}

		if ( !$bError ) {
			$bOk = $this->DropFields($strDropFields);
			if ( !$bOk ) {
				$bError = true;
				$result = "Une erreur s'est produite : Impossible de supprimer les champs ".$strDropFields.".";
			}
		}

		if ( !$bError ) {
			$tabFieldsError = array();
			$bOk = $this->SetTypeFields($tabTypeFields, $tabFieldsError);
			if ( !$bOk ) {
				$bError = true;
				$result = "Une erreur s'est produite, impossible de modifier le type des champs :";
				foreach ( $tabFieldsError as $field => $tabConvert ) {
					$result .= "<br/>- ".$field." (".$tabConvert["from"]." par ".$tabConvert["to"].")";
				}
			}
		}

		if ( !$bError ) {
			$result = "Importation en \"Mise à jour\" terminée avec succès ".$strChampsAdd;
		}
		
		if ( $bError ) throw new \Exception($result);

		$this->updateMetadata();
		return $result;
	}
		
	/**
	 * add new fields to database table which do not exists
	 */
	protected function addNewFields($strDropFields="", $tabTypeFields=array()){

		$tabChampsPg  = $this->GetChampsPostGIS();
		$tabChampsFic = $this->GetChampsFile($strDropFields, true);
		$compareChampsName = function ($nameChampFic, $nameChampPg) {
			if ( strcasecmp(utf8_encode($nameChampFic), utf8_encode($nameChampPg)) == 0 ) {
				return 0;
			} else {
				return 1;
			}
		};
		$tabChampsAdd = array_diff_ukey($tabChampsFic, $tabChampsPg, $compareChampsName);
			
		$tabChampsNameAdd = array();
		foreach ( $tabChampsAdd as $name => $type ) {
			$tabChampsNameAdd[] = utf8_encode($name);
			$name = strtolower($name);
			if ( array_key_exists(utf8_encode($name), $tabTypeFields) ) {
				$type = $tabTypeFields[utf8_encode($name)];
			}
			$type = strtolower($type);
			if ( !$this->IsColumnExist($this->m_table, $name) && $this->IsTableExist($this->m_table) ){
				$m_sSql = ImportInPostgis::$SQL_ADDCOLUMN;
				$m_sSql = str_replace(ImportInPostgis::$SQL_PATTERN_TBL, $this->m_table,         $m_sSql);
				$m_sSql = str_replace(ImportInPostgis::$SQL_PATTERN_VALASNAME, $name,                  $m_sSql);
				$m_sSql = str_replace(ImportInPostgis::$SQL_PATTERN_VAL2ASNAME, $type, $m_sSql);
				$this->executeQuery($m_sSql);
			}
		}
		$strChampsAdd = "";
		if ( sizeof($tabChampsNameAdd) > 0 ) {
			$listChampsNameAdd = implode(", ", $tabChampsNameAdd);
			$strChampsAdd = "(Ajout ".(sizeof($tabChampsNameAdd)>1?"des":"du")." champ".(sizeof($tabChampsNameAdd)>1?"s":"")." ".$listChampsNameAdd.")";
		}

		return $strChampsAdd;
	}

	/**
	 * archivage de la table postGIS
	 * la table PostGIS: $this->m_table
	 * la table archivee PostGIS: $this->m_tableArchive
	 */
	public function ArchivingData(){
		$result = "";

		//on teste existence de la table d'archive
		$tblExiste = $this->IsTableExist($this->m_table);
		if ( !$tblExiste ) return $result;
		$tblArcExiste = $this->IsTableExist($this->m_tableArc);

		if ($tblArcExiste){
			//suppression de la table d'archivage
			$m_sSql = ImportInPostgis::$SQL_DELETETABLE;
			$m_sSql = str_replace(ImportInPostgis::$SQL_PATTERN_TBL, $this->m_tableArc, $m_sSql);
			$sqlResu = $this->executeQuery($m_sSql);
			$statusResuCmd = "DROP TABLE";
		}
		else{
			$statusResuCmd = "DROP TABLE";
		}
		//creation de la table d'archivage
		if ($statusResuCmd == "DROP TABLE") {
			$m_sSql = ImportInPostgis::$SQL_COPYTABLE;
			$m_sSql = str_replace(ImportInPostgis::$SQL_PATTERN_TBLARC, $this->m_tableArc, $m_sSql);
			$m_sSql = str_replace(ImportInPostgis::$SQL_PATTERN_TBL, $this->m_table, $m_sSql);
			$sqlResu = $this->executeQuery($m_sSql);
			
			$result = "Archivage terminé avec succès";
		}

		return $result;
	}
		
	/**
	 * fonction qui vérifie l'accès aux champs tableur et renvoie leur liste...
	 * @return chaîne contenant le mot clé "erreur" si une erreur est arrivée
	 *         chaîne JSON conteant la liste des champs associée à la donnée source
	 */
	public function IntegritingDataGetFields(){
		$result = "";
		$chpsFIC = $this->GetChampsFile("", true);
		$chpsPG  = array();
		
		if ( $this->IsTableExist($this->m_table) ) {
			$chpsPG = $this->GetChampsPostGIS();
		}
		if (is_null($chpsFIC)){
			$result = "[Erreur interne: impossible de lire les champs Fichier]";
		}
		else if (!is_array($chpsFIC)) {
			$result = $result.$chpsFIC;
		}
		else {
			foreach($chpsFIC as $key => $value){
				if ( array_key_exists(strtolower(utf8_encode($key)), $chpsPG) ) {  // les champs issus de PG sont forcément en minuscule
					$value = $chpsPG[strtolower($key)];
				}
				unset($chpsFIC[$key]);
				$chpsFIC[$key] = $value;
			}
			$result = array("msg"=>"Le contrôle d'intégrité (Etape 2) est valide", "data"=>$chpsFIC);
		}
		return ($result);
	}


	/**
	 * fonction qui vérifie l'intégrité d'un fichier tableur (xls, ods) en entrée
	 * @return unknown_type
	 */
	public function IntegritingDataGetLeaf(){
		if(strtolower($this->GetTypeFichier()) == strtolower(ImportInPostgis::$EXT_ODS)){
			$ods = new \ODS($this->GetImportFichier());
			$tabLeafs = $ods->getLeafs();
		}elseif(strtolower($this->GetTypeFichier()) == strtolower(ImportInPostgis::$EXT_XLS)){
			$xls = new \XLS($this->GetImportFichier());
			$tabLeafs = $xls->getLeafs();
		}
		if(is_array($tabLeafs) && !empty($tabLeafs)){
			$result = array("msg"=>"Le contrôle d'intégrité (étape 1) est valide", "data"=>array_values($tabLeafs));
		}else{
			$result = "[Erreur interne: impossible de détecter les feuilles du fichier]";
		}
		return ($result);
	}
		
	/**
	 * fonction qui vérifie l'intégrité des champs entre la table PostGIS et le Fichier
	 * @return chaîne contenant le mot clé "erreur" si une erreur est arrivée
	 *         chaîne contenant le message de confirmation si le contrôle d'intégrité s'est terminé correctement
	 */
	public function IntegritingData($xField="", $yField="", $zField="", $strDropFields="", $tabTypeFields=array()){
		$result = "ERREUR: ";

		$chpsPG 	= $this->GetChampsPostGIS();

		$chpsFIC 	= $this->GetChampsFile($strDropFields, true);

		if (is_null($chpsPG)){
			throw new \Exception("[Erreur interne: impossible de lire les champs POSTGIS]");
		}
		else if (is_null($chpsFIC)){
			throw new \Exception("[Erreur interne: impossible de lire les champs Fichier]");
		}
		else if (!is_array($chpsPG)){
			throw new \Exception($result.$chpsPG);
		}
		else if (!is_array($chpsFIC)){
			throw new \Exception($result.$chpsFIC);
		}
		else {
			// vérifie que les champs dans la table PostGIS existent dans le fichier
			$tabChampsAbsents = array();
			$tabChampsFic = array();
			foreach ( $chpsFIC as $name => $type ) {
				if ( array_key_exists(utf8_encode($name), $tabTypeFields) ) {
					$type = $this->toPGTtype($tabTypeFields[utf8_encode($name)]);
				}
				$tabChampsFic[strtolower(utf8_encode($name))] = utf8_encode($type);
			}
			foreach ( $chpsPG as $name => $type ) {
				$nameLower = strtolower(utf8_encode($name));
				if ( $nameLower != "gid" && $nameLower != "the_geom" ) {
					if ( !array_key_exists($nameLower, $tabChampsFic) ) {
						$tabChampsAbsents[] = $name;
					}
				}
			}
			if ( !empty($tabChampsAbsents) ) {
				$listChampsAbsents = implode(", ", $tabChampsAbsents);
				throw new \Exception("Erreur d'intégrité des champs : Le".(sizeof($tabChampsAbsents)>1?"s":"")." champ".(sizeof($tabChampsAbsents)>1?"s":"")." ".($listChampsAbsents)." ".(sizeof($tabChampsAbsents)>1?"sont":"est")." manquant".(sizeof($tabChampsAbsents)>1?"s":""));
			}
			// vérifie que les changements de type sont autorisés
			else {
				$integre = true;
				$descChamps = "";
				
				foreach ( $chpsPG as $name => $type ) {
					$nameLower = strtolower(utf8_encode($name));
					$typeLower = strtolower($type);
					
					if ( $nameLower != "gid" && $nameLower != "the_geom" && $nameLower != $xField && $nameLower != $yField) {
						if ( !$this->isTypeChangedAllowed($typeLower, strtolower($tabChampsFic[$nameLower])) ) {
							$descChamps .= "<br>Remplacement du type ".$type." par ".$tabChampsFic[$nameLower]." pour le champ ".utf8_encode($name)." non valide";
							$integre = false;
						}
					}
				}
				if (!$integre)
					throw new \Exception("Erreur d'intégrité des champs :".$descChamps);
				else
					$result = "Le contrôle d'intégrité est valide";
			}
		}
		return $result;
	}
		
	/**
	 * fonction qui vérifie si le remplacement du type d'un champ PostGIS par le type d'un champ Fichier est autorisé
	 * (se base sur des patterns)
	 * @param typePg   type du champ PostGIS
	 * @param typeFic  type du champ Fichier
	 * @return false si typePg n'est pas reconnu ou si le remplacement n'est pas autorisé, true sinon
	 */
	public function isTypeChangedAllowed($typePg, $typeFic){
		// tableau qui identifie les changements de type des champs autorisés, à compléter...
		// key   : pattern du type des champs PostGIS
		// value : array des patterns des types des champs Fichier
		$typeChangedAllowed = array("char"    => array("char", "string"),
                                "text"    => array("char", "string"),
                                "int"     => array("int", "integer", "number"),
                                "int4"     => array("int", "integer", "number"),
                                "float"   => array("float", "real", "number"/*XLS*/, "float"/*ODS*/),
                                "decimal" => array("decimal", "real", "number"/*XLS*/, "float"/*ODS*/),
                                "numeric" => array("numeric", "integer", "number", "real", "number"/*XLS*/, "float"/*ODS*/, "int4"), // le type numeric dans PostGIS peut-être remplacé par tous les types listés
                                "date"    => array("date"),
		);
		$bRes = true;

		$typePg  = strtolower($typePg);
		$typeFic = strtolower($typeFic);

		$isTypePgFound  = false;
		$isTypeFicFound = false;
		foreach ( $typeChangedAllowed as $typePgPattern => $tabTypesFicPattern ) {
			if ( strpos($typePg, $typePgPattern) !== false ) {
				$isTypePgFound = true;
				foreach ( $tabTypesFicPattern as $typeFicPattern ) {
					if ( strpos($typeFic, $typeFicPattern) !== false ) {
						$isTypeFicFound = true;
					}
				}
			}
		}

		if ( $isTypePgFound ) {
			if ( $isTypeFicFound ) {
				$bRes = true;
			} else {
				$bRes = false;
			}
		} else {
			$bRes = false;
		}

		return $bRes;
	}

	public function DeclaringTableDroits(){
		$result = "DeclaringTableDroits";
		return $result;
	}
		
	//fct de lecture du retour la commande "php_exec"
	private function interpreteCommandResultat($command, $ResCommand){
		$result = "";
		$result .= "Commande exécutée: ".$command;
		$result .= "<br>";
		$result .= "Retour de la commande: ";
		if (isset($ResCommand)){
			if (is_array($ResCommand)){
				if (count($ResCommand) >0){
					for($idx=0;$idx<count($ResCommand);$idx++){
						if (isset($ResCommand[$idx]))
						$result .= '<br>&nbsp&nbsp;&nbsp;&nbsp;-&nbsp;'.$ResCommand[$idx];
						else
						$result .= '&nbsp&nbsp;&nbsp;&nbsp;-&nbsp;???';
					}
				}
				else{
					$result .= "Aucun resultat dans la tableau de retour";
				}
			}
			else{
				$result .= "Valeur [".$ResCommand."]";
			}
		}
		else{
			$result .= "Aucun retour ???";
		}
		$result = str_replace("\"", "\\\"", $result);
		return $result;
	}
		
	public function DeletingFileTempo($file, $typeCouche="vector"){
		$res = array();
		
		if (!is_null($file) && $file != "" ){
			$compatibleExtension = array(
				'csv','ods','xls','xlsx',
		  	'asc','xyz',
		    'gtiff','tiff','tif','ecw'
			);
			$compatibleExtension = array_map(function($value){return array($value);}, $compatibleExtension);
			$compatibleExtension = array_combine($compatibleExtension, $compatibleExtension);
			$compatibleExtension = array_merge($compatibleExtension, array(
		  	'shp' => array('shp', 'prj', 'dbf', 'shx', 'qpj'), 
		  	'mif' => array('mif', 'mid'), 
		  	'tab' => array('tab', 'id', 'map', 'dat', 'ind')
			));
			
			$extension = strtolower(pathinfo($file, PATHINFO_EXTENSION));
			$radical = preg_replace("!\.".$extension."$!", "", $file);
			$compatibleExtension = $compatibleExtension[$extension];
			$compatibleExtension = array_merge(array_map("strtolower", $compatibleExtension), array_map("strtoupper", $compatibleExtension));
			$dirname = dirname($file);
			$filename = pathinfo($file, PATHINFO_FILENAME);
      $cmd = "find ".$dirname." -type f -maxdepth 1 -name '".$filename.".".implode("' -or -name '".$filename.".", $compatibleExtension)."' ";
      $files = array();
      exec($cmd, $files);
      
			if ( file_exists($radical.".filelist") ){
				$fopen = fopen($radical.".filelist", "r");
				while($line = fgets($fopen)) $files[] = $line;
				fclose($fopen);
			}
			
			set_time_limit(300);
			$cmd = 'rm -rf "'.implode('" "', $files).'"';
			$this->logger->debug(__METHOD__, array("cmd"=>$cmd));
			exec($cmd, $res);
		}
		//delete symbolic links and file_iso.tab (not a link)
		/*$this->m_fichierShortISO_noExt = str_replace(".tab", "", $this->GetImportShortFichier());
		$this->tempTabIso = \RessourcesLocal::$CONVERT_REP.$this->m_fichierShortISO_noExt."_iso.tab";
		if (!is_null($this->tempTabIso)){
		$fileTempo = $this->tempTabIso;
		set_time_limit(300);
		exec("rm ".(str_replace(ImportInPostgis::$EXT_MAPINFO_TAB, "*",  $fileTempo)),$res);
		}*/

		// delete columns "gid" and "the_geom" for table
		if ( $typeCouche == "table" ) {
			$query ="alter table \"".$this->m_table."\" drop column the_geom;";
			$this->executeQuery($query, array());
			//TODO PRODIGE4.0 : conserver le commit ? 
		}

		return true;
	}
		
	//function qui detecte l'existence de la colonne dans la table $table
	//dans PostGIS
	private function IsColumnExist($table_name, $column_name, $table_schema="public")
	{
		$result = false;
		if (!$this->isConnected()) return null;
		$m_sSql = ImportInPostgis::$SQL_ISCOLUMNEXISTE;
		$params = compact("table_schema", "table_name", "column_name");
		$sqlResu = $this->executeQuery($m_sSql, $params);
		$result = ($sqlResu->rowCount() == 1);
		return $result;
	
	
	}
		
	//function qui detecte l'existence de la contrainte pour la table $table
	//dans PostGIS
	private function IsConstraintExist($table_name, $constraint_name, $table_schema="public")
	{
		$result = false;
		if (!$this->isConnected()) return null;
		$m_sSql = ImportInPostgis::$SQL_ISCONSTRAINTEXISTE;
		$params = compact("table_schema", "table_name", "constraint_name");
		$sqlResu = $this->executeQuery($m_sSql, $params);
		$result = ($sqlResu->rowCount() == 1);
		return $result;
	
	}
		
	//function qui detecte l'existence de la table $table
	//dans PostGIS
	private function IsTableExist($table){
		$result = false;
		if (!$this->isConnected()) return null;
		$m_sSql = ImportInPostgis::$SQL_ISTABLEEXISTE;
		$table = explode(".", $table);
		if ( count($table)==2 ){
			$schema = $table[0];
		} else {
			$schema = "public";
		}
		$table = end($table);
		$params = array(ImportInPostgis::$SQL_PATTERN_TBLASVALUE => strtolower($table), "TBLSCHEMA" => strtolower($schema));
		$sqlResu = $this->executeQuery($m_sSql, $params);
		$result = ($sqlResu->rowCount() == 1);
		return $result;
	}
		
	/**
	 * fonction qui retourne les noms et types des champs d'une table PostGIS
	 * les noms des champs sont encodés en ISO-8859-1 (Latin1)
	 * @return array : result[name] = type
	 *         chaine contenant les raisons de l'erreur si une erreur est arrivée
	 *         chaine vide si impossible de lire les champs
	 */
	public function GetChampsPostGIS($table=null){
		$result = "";

		$params = array();
		$m_sSql = ImportInPostgis::$SQL_DESC;
	  $params[ImportInPostgis::$SQL_PATTERN_TBLASVALUE] = $table ?: $this->m_table;
		
		try {
		  $sqlResu = $this->executeQuery($m_sSql, $params);
		  $result = array();
		  foreach ($sqlResu as $row){
		  	$result[$row["column_name"]] = $this->toPGTtype($row["data_type"]);
		  }
		} catch (\Exception $exception) {
			$result = $exception->getCode()." [".str_replace("\"", "\\\"",$exception->getMessage())."]";
		}

		return $result;
	} 
		
	/**
	 * fonction qui retourne le gid max d'une table PostGIS
	 */
	private function GetGidMax(){
		$result = "";

	//TODO select gid from test_alkante order by gid desc limit 1 
		$m_sSql = ImportInPostgis::$SQL_MAXGID;
		$m_sSql = str_replace(ImportInPostgis::$SQL_PATTERN_TBL, $this->m_table, $m_sSql);
		$m_sSql = str_replace(ImportInPostgis::$SQL_PATTERN_GID, \RessourcesLocal::$GID_POSTGIS, $m_sSql);
		
		try {
			$sqlResu = $this->executeQuery($m_sSql);
			$result = $sqlResu->fetchColumn(0);
		} catch (\Exception $exception){
			$result = null;
		}

		return $result;
	}
		
	/**
	 * fonction qui retourne les noms et types des champs d'un fichier dans un tableau (ne fonctionne pas avec le format KML)
	 * les noms des champs sont encodés en ISO-8859-1 (Latin1)
	 * @param $strDropFields chaine séparé par une , des champs à supprimer suite à l'import
	 * @param $bTransPG booléen true pour effectuer le transtypage vers PostgreSQL, false sinon
	 * @return array : result[name] = type
	 *                 chaine contenant les raisons de l'erreur si une erreur est arrivée
	 *                 chaine vide si aucun champs
	 */
	private function GetChampsFile($strDropFields="", $bTransPG=false){

// 		$result = array();
// 		/* récupération des champs pour un fichier XLS */
// 		if ( strtolower($this->GetTypeFichier())==strtolower(ImportInPostgis::$EXT_XLS) ) {
// 			$xls = new \XLS($this->GetImportFichier());
// 			$tabField = $xls->getTabField($this->m_feuille);
// 			if ( is_array($tabField) ) {
// 				foreach ( $tabField as $tabNameType ) {
// 					$result[utf8_encode(htmlspecialchars($tabNameType['name'], ENT_QUOTES))] = ($tabNameType['type'] == "" || $tabNameType['type'] == "unknown" ? "string" : $tabNameType['type']);
// 				}
// 			} else {
// 				return $xls->getMsgError();
// 			}
// 		}
// 		/* récupération des champs pour un fichier ODS */
// 		else if ( strtolower($this->GetTypeFichier())==strtolower(ImportInPostgis::$EXT_ODS) ) {
// 			$ods = new \ODS($this->GetImportFichier());
// 			$tabField = $ods->getTabField($this->m_feuille);
// 			if ( is_array($tabField) ) {
// 				foreach ( $tabField as $tabNameType ) {
// 					$result[utf8_encode(addslashes($tabNameType['name']))] = ( $tabNameType['type'] == "" || $tabNameType['type'] == "unknown" ? "string" : $tabNameType['type'] );
// 				}
// 			} else {
// 				return $ods->getMsgError();
// 			}
// 		}
// 		/* récupération des champs par PHP_OGR */
// 		else {
// 			if ( strtolower($this->GetTypeFichier())==strtolower(ImportInPostgis::$EXT_XYZ) ) {
// 				$fileDuplicatedCSV = str_ireplace(".XYZ", ".csv", $this->GetImportFichier());
// 				//On ajoute l'entete x y z
// 				$res = array(); $return = "";
// 				$cmd = "sed '1s/^/X\\tY\\tZ\\n/' '".$this->GetImportFichier()."' > '".$fileDuplicatedCSV."'";
// 				exec($cmd." 2>&1", $res, $return);
// 				if($return){
// 					return "Problème lors de la mise en conformité du fichier (ajout de l'entête)";
// 				}
// 				//On remplace les tabulations et espaces par des virgules
// 				$res = array(); $return = "";
// 				$cmd = "sed -i 's/[ \\t]/,/g' '".$fileDuplicatedCSV."'";
// 				exec($cmd." 2>&1", $res, $return);
// 				if($return){
// 					return "Problème lors de la mise en conformité du fichier (duplication en fichier csv)";
// 				}
// 				$fileImport = $fileDuplicatedCSV;
// 			}else if( strtolower($this->GetTypeFichier())==strtolower(ImportInPostgis::$EXT_ASC) ){
// 				$fileConvertedCSV = str_ireplace(".ASC", ".csv", $this->GetImportFichier());
// 				//On utilise gdal_translate pour convertir le fichier .asc en un fichier .csv
// 				$res = array(); $return = "";
// 				$cmd = "gdal_translate -of XYZ '".$this->GetImportFichier()."' '".$fileConvertedCSV."'";
// 				exec($cmd." 2>&1", $res, $return);
// 				if($return){
// 					return "Problème lors de la conversion du fichier ".$this->GetImportFichier()." à l'aide de gdal_translate.";
// 				}
// 				//On ajoute l'entete x y z
// 				$res = array(); $return = "";
// 				$cmd = "sed -i '1s/^/X Y Z\\n/' '".$fileConvertedCSV."'";
// 				exec($cmd." 2>&1", $res, $return);
// 				if($return){
// 					return "Problème lors de la mise en conformité du fichier (ajout de l'entête)";
// 				}
// 				//On remplace le separateur espace du fichier .csv par un separateur virgule
// 				$res = array(); $return = "";
// 				$cmd = "sed -i 's/ /,/g' '".$fileConvertedCSV."'";
// 				exec($cmd." 2>&1", $res, $return);
// 				if($return){
// 					return "Problème lors de la mise en conformité du fichier (modification du séparateur).";
// 				}
// 				$fileImport = $fileConvertedCSV;
// 			}else{
// 				// récupère l'emplacement du fichier
// 				$fileImport = $this->GetImportFichier();
// 				if ( !is_null($this->m_fichierShape) ) $fileImport = $this->m_fichierShape;
// 			}
// 			// charge tous les drivers
// 			OGRRegisterAll();

// 			// crée un data source avec le fichier
// // 				$fileImport = str_replace(" ", "%20", $fileImport);
// 			$oDS = OGROpen($fileImport, false);
// 			if ( !$oDS ) return "Problème d'ouverture du fichier ".$fileImport."";

// 			// récupère un layer (le dernier)
// 			$layer = OGR_DS_GetLayer($oDS, OGR_DS_GetLayerCount($oDS)-1);
// 			if ( !$layer ) return "Problème de lecture d'une couche du fichier";

// 			// récupère un feature
// 			$oFeature = OGR_L_GetNextFeature($layer);
// 			if ( !$oFeature ) return "Problème de lecture d'un objet du fichier";

			// récupère les noms des fields et leurs types
			$result = array();
// 			$nbFields = OGR_F_GetFieldCount($oFeature);
			foreach ($this->file_fields as $field){
// 			for ($i = 0; $i<$nbFields; $i++ ) {
// 				$oFieldDef = OGR_F_GetFieldDefnRef($oFeature, $i);
// 				$strFieldName = utf8_encode(OGR_Fld_GetNameRef($oFieldDef));
// 				$oFieldType = OGR_Fld_GetType($oFieldDef);
// 				$strFieldType = OGR_GetFieldTypeName($oFieldType);
				$result[$field["field_name"]] = $field["field_datatype"];
			}
// 		}

		if($strDropFields!=""){
      $tabFieldAssoc = array();
			$tabField = explode(",", $strDropFields);
			foreach($tabField as $id => $value){
			  $tabFieldAssoc[utf8_encode($value)] = "String";
			}
			$result = array_diff_key($result, $tabFieldAssoc);
		}

		if($bTransPG){
			$result = array_map(array($this, "toPGTtype"), $result);
		}
		return $result;
	}

	protected function toPGTtype($type)
	{
		if ( preg_match("/string|char|text/i", $type) > 0 )         {$type = "varchar";}
		if ( preg_match("/int/i", $type) > 0 )                      {$type = "int4";}
		if ( preg_match("/real|number|float|double/i", $type) > 0 ) {$type = "float8";}
		if ( preg_match("/date/i", $type) > 0 )                     {$type = "date";}
		return $type;
	}
		
	//function qui retourne les noms de champs d'un fichier
	//le fichier importe: $this->m_fichier
	public function IsChampUnique($champs){
		$result = false;

		$m_sSql = ImportInPostgis::$SQL_ISUNIQUE;
		$m_sSql = str_replace(ImportInPostgis::$SQL_PATTERN_TBL, $this->m_table, $m_sSql);
		$m_sSql = str_replace(ImportInPostgis::$SQL_PATTERN_CHP, $champs, $m_sSql);

		try {
			$sqlResu = $this->executeQuery($m_sSql);
			$result = ($sqlResu->rowCount()==0);
		} catch (\Exception $exception){
			$result = null;
		}
		return $result;
	}
	
	//fonction qui permet de savoir si la connexion sur la base POSGIS est OK
	// retourne la connexion POSTGIS
	public function getActualSRS(){
		$result = "";


		$m_sSql = ImportInPostgis::$SQL_GETSRS;
		$m_sSql = str_replace(ImportInPostgis::$SQL_PATTERN_TBL, $this->m_table, $m_sSql);
	
		try {
			$sqlResu = $this->executeQuery($m_sSql);
			$result = $sqlResu->fetchColumn(0);
		} catch (\Exception $exception){
			$result = null;
		}
		

		return $result;
	}
}
