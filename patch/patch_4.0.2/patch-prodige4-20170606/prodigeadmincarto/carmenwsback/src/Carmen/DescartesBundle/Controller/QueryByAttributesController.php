<?php

namespace Carmen\DescartesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

include_once __DIR__.'/Utils.php';

// use FOS\RestBundle\Controller\Annotations\Delete;
// use FOS\RestBundle\Controller\Annotations\Get;
// use FOS\RestBundle\Controller\Annotations\Route;
// use FOS\RestBundle\Controller\Annotations\Head;
// use FOS\RestBundle\Controller\Annotations\Link;
// use FOS\RestBundle\Controller\Annotations\Patch;
// use FOS\RestBundle\Controller\Annotations\Post;
// use FOS\RestBundle\Controller\Annotations\Put;
// use FOS\RestBundle\Controller\Annotations\Unlink;
// use FOS\RestBundle\Request\ParamFetcher;
// use FOS\RestBundle\Controller\Annotations\QueryParam;   // GET params
// use FOS\RestBundle\Controller\Annotations\RequestParam; // POST params

/**
 * Carmen Descartes Controller.
 * 
 * @author alkante <support@alkante.com>
 * @Route(options={"expose"=true})
 */
class QueryByAttributesController extends \Carmen\ApiBundle\Controller\BaseController
{

    /**
     * TODO convertToSymfony2
     *
     * @Route("/advancedqueryattributes", name="carmen_descartes_advancedqueryattributes")
     * @Method({"GET", "POST"})
     * 
     * @param Request $request  The request object.
     * 
     * @return JsonResponse Json object.
     *
     * @see /home/devperso/bfontaine/prodige3.4/prodigefrontcarto/services/AdvancedQueryAttributes/index.php
     *
     * #@used IHM/JavaScript/CarmenJS/Carmen/Application.js={426}
     * #@used IHM/JavaScript/CarmenJS/Carmen/Control/AdvancedQueryAttributes.js={651; 1147}
     */
    public function queryByAttributes(Request $request)
    {
// /**
//  * QueryAttributes Service
//  */

// require_once("./../../lib/lib_session.php");
// require_once("./../../app_conf.php");
// require_once("./../../lib/lib_request.php");
// require_once("./../../lib/lib_service.php");
// require_once("./../util.php");


// set_time_limit(600);
// set_error_handler("handle_error", E_ALL | E_STRICT);
// if (!loadService())
//   trigger_error(CARMEN_ERROR_SESSION_EXPIRED . "|" . CARMEN_MSG_SESSION_EXPIRED . "|CARMEN_ERROR", E_USER_ERROR);


// Retrieving parameters
$mapfile = $this->getMapfileDirectory()."/".$request->get("map", "").".map";
$layer =  $request->get("layer", "");
$qitem =  $request->get("qitem", "");
$qstring = $request->get("qstring", "");
$qOgrString = $request->get("qOgrString", "");
$op         = $request->get("op", "");
$pattern = $request->get("pattern", "");
$fieldsJSON = stripslashes($request->get("fields", ""));
$fieldsList = json_decode($fieldsJSON, true);
// nb limite lie à la cb
$valueLineLimit = $request->get("valueLineLimit", "") ?: 100;
// listvalpossible : param ajax : pour obtenir une liste de valeur dans le combo
//$listvalpossible = $request->get("listvalpossible", "");
//echo $mapfile."_______".$layer."******".$qitem."---".$qstring; die();

$queryMode = "";

if ($mapfile==""
		|| $layer==""
		|| $qitem==""
		|| $qstring==""
		|| $op=="" 
	//	|| $pattern==""
		|| $fieldsList==false
) {
  trigger_error(CARMEN_ERROR . "|Missing or wrong parameters in queryAttributes call.|CARMEN_ERROR", E_USER_ERROR);
}

// building queryfilename
$queryfileName = datasetNameFromFilename($mapfile) . '_' . $_SESSION["service"]['idx'] . '_' . session_id() . '.qy';
$queryfileOgrName = datasetNameFromFilename($mapfile) . '_' . $_SESSION["service"]['idx'] . '_' . session_id() . '.map';
$queryfileFullPath = CARMEN_DATA_PATH_CACHE . $queryfileName;
$queryfileOgrFullPath = CARMEN_DATA_PATH_CACHE . $queryfileOgrName;



//debug
$debugQstring = "";
$comment = "";

$totalCount = 0;

$oMap = ms_newMapObj($mapfile);
$strObjJson = "{";

$selExtent = ms_newRectObj();
$selExtent->setExtent(doubleval(MAX_DOUBLE), doubleval(MAX_DOUBLE), doubleval(MIN_DOUBLE), doubleval(MIN_DOUBLE));


$layer = $oMap->getLayerByName($layer);

if(@$oMap->getLayerByName("layerOgr"))
  $layerOgr = $oMap->getLayerByName("layerOgr");
else
  $layerOgr = ms_newLayerObj($oMap, $layer);
$layerOgr->set("name", "layerOgr");

$layerOgr->updateFromString(
              "LAYER
              TYPE POINT    
              METADATA
              END # METADATA
              VALIDATION
                'labelrequires' '[01]'
              END
              END # LAYER");

$res = false;

$dataInEncode = 'MapfileEncode';
$dataOutEncode = 'OWSEncode';
//setting up the query
if ($layer) {
  // depending on the connection type the qstring does not play the same role
  // for postgis -> where clause
  // elsewhere -> regex to test qitem
  // so complex queries have to be passed as data filter for non postgis layer...
  
	// with mapserver client wfs layers, mapserv seems to automatically 
  // encode data from iso to utf8 in memory... so we we must not do an 
  // extra encoding utf8->iso transformation
  if ($layer->connectiontype == MS_WFS ||	$layer->getMetaData("LAYER_ENCODING")=="UTF-8") {
		$dataInEncode = 'NoEncode';
		$dataOutEncode = 'NoEncode';
  }  
  switch($layer->connectiontype) {
    case MS_SHAPEFILE :
    case MS_TILED_SHAPEFILE :
      // redefining the connection as an ogr connection
      // to allow complex query expressions via DATA sql restrictions
      if($queryMode == QM_OGR_FILTER){
        //Dans le cas d'une requete OGR on rajoute un novueau layer comportant la selection
        $layerData = (preg_match('/^'.preg_quote(CARMEN_URL_PATH_DATA, '/').'/', $layer->data) ? $layer->data : CARMEN_URL_PATH_DATA."cartes/Publication/wfs/temp/".substr($layer->data, 2));
      	$datasetName = datasetNameFromFilename($dataOutEncode($layerData));
      	$resSetConnection = $layer->set("data", $dataInEncode(forceShapefileExtension($dataOutEncode($layerData))));
      	$resSetConnection = $layerOgr->set("connection", $dataInEncode(forceShapefileExtension($dataOutEncode($layerData))));
      	$layerOgr->setConnectiontype(MS_OGR);
      	
      	
      	// building qstring
      	$qstring = str_replace("PATTERN", $pattern, $qstring);
      	$qstring = str_replace("FIELD", $qitem, $qstring);
		
      	$sqlStr = 'SELECT * FROM ' . $datasetName;
      	$layerOgr->set("data", $dataInEncode($sqlStr));
      	$layerOgr->setFilter($dataInEncode($qOgrString));

      	$queryMode = QM_OGR_FILTER;
      	$oClass = $layerOgr->getClass(0);
      	$oClass->deleteStyle(0);
      	$oStyle = ms_newStyleObj($oClass);
      	$oStyle->set("size", 5);
      	$oStyle->color->setRGB(255, 255, 0);
      	$oStyle->set("symbolname", "Cercle");
      	$oStyle->set("width", "1");
      	$oStyle->outlinecolor->setRGB(0, 0, 0);
        $res = ($layerOgr->open()==MS_SUCCESS);
      	$debugQstring = $sqlStr . ' ' . $layerOgr->connection;
      }else{
      	//Sinon rien ne change
        $datasetName = datasetNameFromFilename($dataOutEncode($layer->data));
      
        $resSetConnection = $layer->set("connection", $dataInEncode(forceShapefileExtension($dataOutEncode(CARMEN_URL_PATH_DATA."cartes/Publication/wfs/temp/".substr($layer->data, 2)))));
        $layer->setConnectiontype(MS_OGR);
      
        // building qstring
        $qstring = str_replace("PATTERN", $pattern, $qstring);
        $qstring = str_replace("FIELD", $qitem, $qstring);
      
        $sqlStr = 'SELECT * FROM ' . $datasetName . ' WHERE ' . stripslashes($qstring);

        $layer->set("data", $dataInEncode($sqlStr));
        $queryMode = QM_OGR_FILTER;

        $res = ($layer->open()==MS_SUCCESS);
        $debugQstring = $sqlStr . ' ' . $layer->connection;
      }
      //$comment = "\"SHP " .  $sqlStr . "\"";
      break;
    case MS_OGR :
      // defining or completing the DATA sql restrictions
      $datasetName = datasetNameFromFilename($dataOutEncode($layer->connection));
      //$qstring = 'Nombre_Chaussées like \'%' . utf8_decode('é') . '%\'';  
      
      // building qstring
      $qstring = str_replace("PATTERN", $pattern, $qstring);
      $qstring = str_replace("FIELD", $qitem, $qstring);
      
      $sqlStr = 'SELECT * FROM ' . $datasetName . ' WHERE ' . stripslashes($qstring);
      $comment = $sqlStr;
      $layer->set("data", $dataInEncode($sqlStr));
      $queryMode = QM_OGR_FILTER;
      $res = (@$layer->open()==MS_SUCCESS);
      $comment = "\"OGR " .  $res . "\"";
      // todo : look if there is already a restriction
      // but not simple to handle cos LEFT JOIN and so on...
      // if (isset($layer->data) && strlen(($layer->data))>0)
       $debugQstring = $sqlStr;
      break;
    case MS_POSTGIS :
      $queryMode = QM_MAPSERVER_QUERY;
      // cas de recherhce unbiqt sur la couche : on interroge toute la couche
  	  if($qitem==-1){
  		  $qitem="";
  	  }
//           if($qitem!="")
//       	$qstring = str_replace($qitem."]", $qitem.']::text', $qstring);
        
    //  $qstring = "(" . stripslashes($qstring) . ")"; 

      if ( $qitem ){
 //         $qstring = "([finess] ~ '^')";
          ms_ResetErrorList();
          //$layer->setFilter("(".str_replace("ilike", "~*", $qstring).")");
          $oMap->setConfigOption("MS_ERRORFILE", str_replace(".map", ".log", $mapfile));
          $oMap->set('debug', 5);
          $layer->set('debug', 5);
        $res = (@$layer->queryByAttributes($qitem, $qstring ,MS_MULTIPLE)==MS_SUCCESS);
        $res = $res && ($layer->open()==MS_SUCCESS);
      } 
      else if ( $qstring!="1=1" ){
        $res = (@$layer->queryByAttributes($fieldsList[0]["fieldName"], $qstring ,MS_MULTIPLE)==MS_SUCCESS);
        $res = $res && ($layer->open()==MS_SUCCESS);
      }
      else {
        $layerOgr = $layer;
      	$res = (@$layer->open()==MS_SUCCESS);
      	
        $queryMode = QM_OGR_FILTER;
      }     
      
      $comment = "\"POSTGIS " .  $res . "\"";
      $debugQstring = $qstring;
      break;
    default:
      // general case : handling query with mapscript queryByAttributes
      $queryMode = QM_MAPSERVER_QUERY;      
      $qstring = str_replace($qitem.']', $qitem.']::text', $qstring);
      $qstring = "\"" . stripslashes($qstring) . "\"";
      $res = (@$layer->queryByAttributes($dataInEncode($qitem), $dataInEncode($qstring) ,MS_MULTIPLE)==MS_SUCCESS);
      if ($res)
        $res = (@$layer->open()==MS_SUCCESS);
      $debugQstring = $qstring;
      $comment = "\"OTHER " .  $res . "\"";
      break;
  }
}

$selExtentLayer = ms_newRectObj();
$selExtentLayer->setExtent(doubleval(MAX_DOUBLE), doubleval(MAX_DOUBLE), doubleval(MIN_DOUBLE), doubleval(MIN_DOUBLE));
  	
$strObjJson = array();
if ($res) {
    $tmp = array();
    foreach($fieldsList as $index=>$field){
        foreach ($field as $key=>$value )
            $tmp[$index][strtolower(str_replace('field', '', $key))] = $value;
    }
    $fieldsList = $tmp;
    

  $fields = $fieldsList;
  $layerFields = $layer->getItems();
  
  $escapeFields = array_map("escape_quote", $fields);
  $quoteFields = str_replace('"', '', array_map("getName", $escapeFields));
  $strObjJson = array(
    "fields" => array_merge($quoteFields, array("extent", "fid", "geometryWKT")),
    "data" => array()
  );
  $keyField = $quoteFields[0]; 
  for($j=0; $j<count($quoteFields); $j++) {
      if ( strtolower($quoteFields[$j])==strtolower($qitem) ) {
      	$keyField = $quoteFields[$j];
      	break;
      }
  }
      
  switch($queryMode) { 
    case QM_OGR_FILTER:
      $debugString = 'mode_OGR' . $debugQstring;
      
      $status = $layerOgr->whichShapes($layerOgr->getExtent());
      $count=0;
      $selectedShapes = array();
      $selectedTiles = array();
      while ($shape = $layerOgr->nextShape())
      {
        $values = array();
        $dataKey = $shape->values[$keyField];
        
        for($j=0; $j<count($fields); $j++) {
          // TODO : strange behaviour with ogr data source
          // seems to need to force field name conversion to utf-8 to handle special chars éè...
          if ($fields[$j]["datatype"]=="URL") {
            $values[] = array("href" => fill_template($fields[$j]["url"], $shape->values, $layerFields),
            		           "text" => escape_quote($dataOutEncode($shape->values[$dataInEncode($fields[$j]["name"])])));
          }
          else if ($fields[$j]["datatype"]=="IMG") {
			      $values[] = fill_template($fields[$j]["url"], $shape->values, $layerFields);
          }
          else {
            $values[] = escape_quote($dataOutEncode($shape->values[$dataInEncode($fields[$j]["name"])]));
          }       
        }

        $selExtentLayer = mergeRectObj($selExtentLayer, $shape->bounds);

        $values[] = $dataOutEncode(rectObjToStr($shape->bounds));
        $values[] = $dataOutEncode($shape->index);
        $values[] = $dataOutEncode($shape->toWkt());
        

        array_push($selectedShapes, $shape->index);
        array_push($selectedTiles, $shape->tileindex);
        $shape->free();
        
        if ( !array_key_exists($dataKey, $strObjJson["data"]) )
        	$count++;
        $strObjJson["data"][$dataKey] =  $values;   
        if ( $count>$valueLineLimit ) break;
      }
      $oMap->save($queryfileOgrFullPath);
      
      $layer->close();
      if ($layer!==$layerOgr) $layerOgr->close();
      
      break;
    case QM_MAPSERVER_QUERY:
      $count = 0;
      
      for ($i=0;$i<$layer->getNumResults();$i++) {
        $resObj = $layer->getResult($i);
        $feat = $layer->getShape($resObj);
        $dataKey = $feat->values[$keyField];
        
        $values = array();
        for($j=0; $j<count($fields); $j++) {
          if ($fields[$j]["datatype"]=="URL"){
            $values[] = array("href" => fill_template($fields[$j]["url"], $feat->values, $layerFields),
            		           "text" => escape_quote($dataOutEncode($feat->values[$dataInEncode($fields[$j]["name"])])));
          }
          else if ($fields[$j]["datatype"]=="IMG"){
						$values[] = fill_template($fields[$j]["url"], $feat->values, $layerFields);
          }
		  		else{
            $values[] = escape_quote($dataOutEncode($feat->values[$dataInEncode($fields[$j]["name"])])) ;
		  		}
        }
          
        $selExtentLayer = mergeRectObj($selExtentLayer, $feat->bounds);

        $values[] = $dataOutEncode(rectObjToStr($feat->bounds));
        $values[] = $dataOutEncode($resObj->shapeindex);
        $values[] = $dataOutEncode($feat->toWkt());
        
        $feat->free();
        
        if ( !array_key_exists($dataKey, $strObjJson["data"]) )
        	$count++;
        $strObjJson["data"][$dataKey] = $values;
        if ( $count>$valueLineLimit ) break;

      }
      $debugString = 'modePostGIS';

      $oMap->saveQuery($queryfileFullPath);
      break;
  }

  $layer->close();
  if ( $layer!==$layerOgr ) $layerOgr->close();

  $totalCount = $totalCount + $count;

  $strObjJson["extent"] = $dataOutEncode(rectObjToStr($selExtentLayer));
  //, "fid", "geometryWKT"
  
  ksort($strObjJson["data"]);
  $strObjJson["data"] = array_values($strObjJson["data"]);
  $strObjJson = array($dataOutEncode($layer->name) => $strObjJson);

  $selExtent = $selExtentLayer;
}
$response =  array(
		  "totalCount" => $totalCount
		, "extent"=>rectObjToStr($selExtent)
		, "results" => $strObjJson
		, "queryfile" => $queryfileFullPath
		, "mapfileOgr" => ($queryMode == QM_OGR_FILTER ? $queryfileOgrFullPath : "")
);
// write json result
return new Response(json_encode($response));
//. ', "debugString" : "' . $debugQstring . '"

    }
    

}