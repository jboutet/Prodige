<?php

namespace Carmen\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\VirtualProperty;
use JMS\Serializer\Annotation\SerializedName;

/**
 * Field
 *
 * @ORM\Table(name="carmen.field", indexes={@ORM\Index(name="idx_fields", columns={"layer_id"}), @ORM\Index(name="idx_fields_0", columns={"field_type_id"})})
 * @ORM\Entity
 */
class Field
{
    /**
     * @var integer
     *
     * @ORM\Column(name="field_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="carmen.field_field_id_seq", allocationSize=1, initialValue=1)
     */
    private $fieldId;

    /**
     * @var integer
     *
     * @ORM\Column(name="field_rank", type="integer", nullable=true)
     */
    private $fieldRank;

    /**
     * @var string
     *
     * @ORM\Column(name="field_name", type="string", length=255, nullable=true)
     */
    private $fieldName;

    /**
     * @var string
     *
     * @ORM\Column(name="field_alias", type="string", length=255, nullable=true)
     */
    private $fieldAlias;

    /**
     * @var boolean
     *
     * @ORM\Column(name="field_headers", type="boolean", nullable=true)
     */
    private $fieldHeaders = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="field_queries", type="boolean", nullable=true)
     */
    private $fieldQueries = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="field_tooltips", type="boolean", nullable=true)
     */
    private $fieldTooltips = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="field_ogc", type="boolean", nullable=true)
     */
    private $fieldOgc = false;

    /**
     * @var string
     *
     * @ORM\Column(name="field_url", type="string", nullable=true)
     */
    private $fieldUrl;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="field_duplicate", type="boolean", nullable=true)
     */
    private $fieldDuplicate = false;

    /**
     * @var \Layer
     *
     * @ORM\ManyToOne(targetEntity="Layer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="layer_id", referencedColumnName="layer_id")
     * })
     */
    private $layer;

    /**
     * @var \LexFieldType
     *
     * @ORM\ManyToOne(targetEntity="LexFieldType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="field_type_id", referencedColumnName="field_type_id")
     * })
     */
    private $fieldType;

    /**
     * @var \LexFieldDatatype
     *
     * @ORM\ManyToOne(targetEntity="LexFieldDatatype")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="field_datatype_id", referencedColumnName="field_datatype_id")
     * })
     */
    private $fieldDatatype;


    /**
     * 
     * Set field Id
     *
     * @param integer $id 
     * @return \Carmen\ApiBundle\Entity\Field
     */
    public function setId($id)
    {
        $this->fieldId = $id;

        return $this;
    }
    

    /**
     * Get fieldId
     *
     * @return integer 
     */
    public function getFieldId()
    {
        return $this->fieldId;
    }

    /**
     * Set fieldRank
     *
     * @param integer $fieldRank
     * @return Field
     */
    public function setFieldRank($fieldRank)
    {
        $this->fieldRank = $fieldRank;

        return $this;
    }

    /**
     * Get fieldRank
     *
     * @return integer 
     */
    public function getFieldRank()
    {
        return $this->fieldRank;
    }

    /**
     * Set fieldName
     *
     * @param string $fieldName
     * @return Field
     */
    public function setFieldName($fieldName)
    {
        $this->fieldName = $fieldName;

        return $this;
    }

    /**
     * Get fieldName
     *
     * @return string 
     */
    public function getFieldName()
    {
        return $this->fieldName;
    }

    /**
     * Set fieldAlias
     *
     * @param string $fieldAlias
     * @return Field
     */
    public function setFieldAlias($fieldAlias)
    {
        $this->fieldAlias = $fieldAlias;

        return $this;
    }

    /**
     * Get fieldAlias
     *
     * @return string 
     */
    public function getFieldAlias()
    {
        return $this->fieldAlias;
    }

    /**
     * Set fieldHeaders
     *
     * @param boolean $fieldHeaders
     * @return Field
     */
    public function setFieldHeaders($fieldHeaders)
    {
        $this->fieldHeaders = $fieldHeaders;

        return $this;
    }

    /**
     * Get fieldHeaders
     *
     * @return boolean 
     */
    public function getFieldHeaders()
    {
        return $this->fieldHeaders;
    }

    /**
     * Set fieldQueries
     *
     * @param boolean $fieldQueries
     * @return Field
     */
    public function setFieldQueries($fieldQueries)
    {
        $this->fieldQueries = $fieldQueries;

        return $this;
    }

    /**
     * Get fieldQueries
     *
     * @return boolean 
     */
    public function getFieldQueries()
    {
        return $this->fieldQueries;
    }

    /**
     * Set fieldTooltips
     *
     * @param boolean $fieldTooltips
     * @return Field
     */
    public function setFieldTooltips($fieldTooltips)
    {
        $this->fieldTooltips = $fieldTooltips;

        return $this;
    }

    /**
     * Get fieldTooltips
     *
     * @return boolean 
     */
    public function getFieldTooltips()
    {
        return $this->fieldTooltips;
    }

    /**
     * Set fieldOgc
     *
     * @param boolean $fieldOgc
     * @return Field
     */
    public function setFieldOgc($fieldOgc)
    {
        $this->fieldOgc = $fieldOgc;

        return $this;
    }

    /**
     * Get fieldOgc
     *
     * @return boolean 
     */
    public function getFieldOgc()
    {
        return $this->fieldOgc;
    }

    /**
     * Set fieldDuplicate
     *
     * @param boolean $fieldDuplicate
     * @return Field
     */
    public function setFieldDuplicate($fieldDuplicate)
    {
        $this->fieldDuplicate = $fieldDuplicate;

        return $this;
    }

    /**
     * Get fieldDuplicate
     *
     * @return boolean
     */
    public function getFieldDuplicate()
    {
        return $this->fieldDuplicate;
    }

    /**
     * Set fieldUrl
     *
     * @param string $fieldUrl
     * @return Field
     */
    public function setFieldUrl($fieldUrl)
    {
        $this->fieldUrl = $fieldUrl;

        return $this;
    }

    /**
     * Get fieldUrl
     *
     * @return string 
     */
    public function getFieldUrl()
    {
        return $this->fieldUrl;
    }

    /**
     * Set layer
     *
     * @param \Carmen\ApiBundle\Entity\Layer $layer
     * @return Field
     */
    public function setLayer(\Carmen\ApiBundle\Entity\Layer $layer = null)
    {
        $this->layer = $layer;

        return $this;
    }

    /**
     * Get layer
     *
     * @return \Carmen\ApiBundle\Entity\Layer 
     */
    public function getLayer()
    {
        return $this->layer;
    }

    /**
     * Set fieldType
     *
     * @param \Carmen\ApiBundle\Entity\LexFieldType $fieldType
     * @return Field
     */
    public function setFieldType(\Carmen\ApiBundle\Entity\LexFieldType $fieldType = null)
    {
        $this->fieldType = $fieldType;

        return $this;
    }

    /**
     * Get fieldType
     *
     * @return \Carmen\ApiBundle\Entity\LexFieldType 
     */
    public function getFieldType()
    {
        return $this->fieldType;
    }

    /**
     * Set fieldDatatype
     *
     * @param \Carmen\ApiBundle\Entity\LexFieldDatatype $fieldDatatype
     * @return Field
     */
    public function setFieldDatatype(\Carmen\ApiBundle\Entity\LexFieldDatatype $fieldDatatype = null)
    {
        $this->fieldDatatype = $fieldDatatype;

        return $this;
    }

    /**
     * Get fieldDatatype
     *
     * @return \Carmen\ApiBundle\Entity\LexFieldDatatype 
     */
    public function getFieldDatatype()
    {
        return $this->fieldDatatype;
    }
    
    /**
     * @VirtualProperty
     * @SerializedName("fieldDataName")
     *
     * @return string
     */
    public function getFieldDataName()
    {
        return utf8_encode(iconv("UTF-8", $this->getLayer()->getLayerDataEncoding()?:"UTF-8", $this->getFieldName()));
    }
    
    /**
     * @VirtualProperty
     * @SerializedName("fieldTypeJs")
     *
     * @return string
     */
    public function getFieldTypeJs()
    {
        $fieldType = $this->fieldType;
        if ( !$fieldType ) return 'TXT';
        $fieldType = $this->fieldType->getFieldTypeName();
        if ( stripos($fieldType, 'Image')!==false ) return 'IMG';
        if ( stripos($fieldType, 'URL')!==false   ) return 'URL';
        if ( stripos($fieldType, 'Date')!==false  ) return preg_replace('!Date \(([AMJ]+)\)!', '$1', $fieldType);
        return 'TXT';
    }
}
