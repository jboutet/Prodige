<?php

namespace Carmen\ApiBundle\Controller;

use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use JMS\Serializer\SerializationContext;
use Symfony\Component\HttpFoundation\JsonResponse;
use Prodige\ProdigeBundle\Services\GeonetworkInterface;

/**
 * Carmen Prodige API Controller.
 * 
 * @author alkante <support@alkante.com>
 */
class ProdigeController extends BaseController {

    const DYNAMIC_MAP = 0;
    const STATIC_MAP  = 1;
    /**
     * @Route("/edit_map/{uuid}", name="carmen_edit_map", requirements={"uuid"=".*"}, options={"expose"=true})
     * @Method({"GET"})
     *
     * @param Request $request  The request object.
     * @param string  $mapId    The mapId.
     */
    public function editMapByMapFileAction(Request $request, $uuid=null) 
    {
        // vérification de sécurité
        $isAdmin = self::checkSecurity($this->container, $uuid);
    
        $session = $request->getSession();
        $session->set("uuid", $uuid);
         
        return $this->forward('CarmenWebBundle:Default:index', array(), array('from'=>'prodige', "isAdmin" => $isAdmin ? 1 : 0));
    }
    
    /**
     * link map metadata to layer metadatas
     *
     * @param string  $mapUuid  The map uuid
     * @param string  $layers   The layers of this map
     */
    public function saveMapAction($map_uuid, $layers)
    {
         
        //self::checkSecurity($this->container, $map_uuid);
         
         //first detect if map exists in catalogue database and is of type map
         $CATALOGUE = $this->getCatalogueConnection('catalogue,public');
         
         $metadata = $CATALOGUE->fetchAssoc(" SELECT data, id ".
                                         " FROM metadata " .
                                         " inner join fiche_metadonnees on fiche_metadonnees.fmeta_id::bigint = metadata.id".
                                         " inner join carte_projet on fiche_metadonnees.pk_fiche_metadonnees = carte_projet.cartp_fk_fiche_metadonnees".
                                        " WHERE uuid=:uuid", array("uuid" => $map_uuid));
         if($metadata){
             $XMLDATA =  $metadata["data"];
             $metadataId = $metadata["id"];
         
             $version  = "1.0";
             $encoding = "UTF-8";
             $metadata_doc = new \DOMDocument($version, $encoding);
             $entete = "<?xml version=\"".$version."\" encoding=\"".$encoding."\"?>\n";
             $metadata_data = $entete.$XMLDATA;
             $metadata_doc->loadXML(($metadata_data));
         
             //suppression du noeud s'il existe
             $xpath = new \DOMXpath($metadata_doc);
             $nodeToDelete = $xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/srv:coupledResource");
             foreach($nodeToDelete as $k => $item){
                 $item->parentNode->removeChild($item);
             }
         
             //suppression du noeud s'il existe
             $xpath2 = new \DOMXpath($metadata_doc);
             $nodeToDelete2 = $xpath2->query("/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/srv:operatesOn");
             foreach($nodeToDelete2 as $k2 => $item2){
                 $item2->parentNode->removeChild($item2);
             }
             foreach($layers as $layer){
                 $uuid = $layer->getLayerMetadataUuid();
                 $layerName = $layer->getLayerName();
                 
                 if($uuid!="" && $layerName !=""){
                     $IdentInfo = $metadata_doc->getElementsByTagName("SV_ServiceIdentification")->item(0);
                     $strXML ='<srv:coupledResource xmlns:srv="http://www.isotc211.org/2005/srv" xmlns:gco="http://www.isotc211.org/2005/gco" xmlns:gmd="http://www.isotc211.org/2005/gmd">
                                  <srv:SV_CoupledResource>
                                   <srv:operationName>
                                      <gco:CharacterString>GetCapabilities</gco:CharacterString>
                                   </srv:operationName>
                                   <srv:identifier>
                                      <gco:CharacterString>'.$uuid.'</gco:CharacterString>
                                   </srv:identifier>
                                   <gco:ScopedName>'.$layerName.'</gco:ScopedName>
                                </srv:SV_CoupledResource>
                              </srv:coupledResource>';
                     $addXmlCarte = new \DOMDocument;
                     $addXmlCarte->loadXML($strXML);
                     $nodeToImport = $addXmlCarte->getElementsByTagName("coupledResource")->item(0);
                     $newNodeDesc = $metadata_doc->importNode($nodeToImport, true);
                     $IdentInfo->appendChild($newNodeDesc);
         
                     $strXML2 = '<srv:operatesOn xmlns:xlink="http://www.w3.org/1999/xlink"  xmlns:srv="http://www.isotc211.org/2005/srv" 
                               uuidref="'.$uuid.'" xlink:href="'.PRO_GEONETWORK_URLBASE.'/srv/metadata/'.$uuid.'"/>';
                     $addXmlCarte2 = new \DOMDocument;
         
                     $addXmlCarte2->loadXML($strXML2);
                     $nodeToImport2 = $addXmlCarte2->getElementsByTagName("operatesOn")->item(0);
                     $newNodeDesc2 = $metadata_doc->importNode($nodeToImport2, true);
                     $IdentInfo->appendChild($newNodeDesc2);
                 }
             }
             $new_metadata_data = $metadata_doc->saveXML();
             $new_metadata_data = str_replace($entete, "", $new_metadata_data);
             if($new_metadata_data != "") {
                 $urlUpdateData = "md.edit.save";
                 $formData = array(
                     "id"=> $metadataId,
                     "data" => $new_metadata_data
                 );
                 //send to geosource
                 $geonetwork = new GeonetworkInterface(PRO_GEONETWORK_URLBASE, 'srv/fre/');
                 $geonetwork->post($urlUpdateData, $formData);
             }
             
             
         }
        
    }
    /**
     * @Route("/geosource/init_map/{uuid}", name="carmen_eval_init_map", requirements={"uuid"=".*"}, options={"expose"=true})
     * @Method({"GET"})
     *
     * @param Request $request  The request object.
     * @param string  $mapId    The mapId.
     */
    public function evalInitMapAction(Request $request, $uuid=null) 
    {
        // vérification de sécurité
        $isAdmin = self::checkSecurity($this->container, $uuid);
    
        $uuid = $request->get("uuid");
        $session = $request->getSession();
        $session->set("uuid", $uuid);
         
        
        //first detect if map exists in catalogue database
        $CATALOGUE = $this->getCatalogueConnection('catalogue,public');
        
        $carte = $CATALOGUE->fetchAssoc(
                "select cartp_format, cartp_nom, stkcard_path ".
                " from carte_projet ".
                " inner join fiche_metadonnees on (carte_projet.cartp_fk_fiche_metadonnees = fiche_metadonnees.pk_fiche_metadonnees) ".
                " inner join stockage_carte on (stockage_carte.pk_stockage_carte = carte_projet.cartp_fk_stockage_carte)".
                " inner join metadata on (fiche_metadonnees.fmeta_id::bigint = metadata.id) ".
                " where metadata.uuid=:uuid", array ("uuid" => $uuid));
        
        
        if($carte){
            $format =  $carte["cartp_format"];
            if(intval($format)==self::DYNAMIC_MAP){//dynamic map
                return $this->redirectToRoute("carmen_edit_map", array("uuid"=>$uuid));
            }
        }

        $serializer = $this->get('jms_serializer');
        $context = new SerializationContext();
        $context->setSerializeNull(true);
        $user = $serializer->serialize($this->getUser(), 'json', $context);
        
        $config = $this->get('carmen.config')->getWebConfig();
        $defaults = $this->container->getParameter('jsdefaults', array());
        
        return $this->render('CarmenWebBundle:Default:initmap.html.twig', array(
            'user'=>$user,
            'parameters'=>$config,
            'jsdefaults'=>$defaults,
            'isAdmin' => $isAdmin ? 1 : 0,
            'carte' => $carte ?: null
        ));
    }
    
    /**
     * @Route("/geosource/init_map/{uuid}", name="carmen_initialize_map", requirements={"uuid"=".*"}, options={"expose"=true})
     * @Method({"POST"})
     *
     * @param Request $request  The request object.
     * @param string  $mapId    The mapId.
     */
    public function initializeMapAction(Request $request, $uuid) 
    {
        $CATALOGUE = $this->getCatalogueConnection('catalogue,public');
        $cartp_format = intval($request->get('cartp_format', '0'));
        
        $pk_stockage_carte = $CATALOGUE->fetchColumn(
                "select cartp_fk_stockage_carte ".
                " from carte_projet ".
                " inner join fiche_metadonnees on (carte_projet.cartp_fk_fiche_metadonnees = fiche_metadonnees.pk_fiche_metadonnees) ".
                " inner join stockage_carte on (stockage_carte.pk_stockage_carte = carte_projet.cartp_fk_stockage_carte)".
                " inner join metadata on (fiche_metadonnees.fmeta_id::bigint = metadata.id) ".
                " where metadata.uuid=:uuid", array ("uuid" => $uuid));
        
        
        switch ($cartp_format){
            case self::DYNAMIC_MAP :
                $map_file = $request->get('map_file', null);
                if ( $map_file ) $map_file .=  ".map";
            break;
            case self::STATIC_MAP :
                $map_file = $request->get('data_file', null);
                
            break;
            default :
            break;
        }
        
        
        $namespaces = "ARRAY[ARRAY['gmd', 'http://www.isotc211.org/2005/gmd'], ARRAY['gco','http://www.isotc211.org/2005/gco']]"; 
        $metadata_desc = ", array_to_string(xpath('/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:abstract/gco:CharacterString/text()'::text, data::xml, {$namespaces}), ' ') as metadata_desc";
        $metadata_title = ", array_to_string(xpath('/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:citation/gmd:CI_Citation/gmd:title/gco:CharacterString/text()'::text, data::xml, {$namespaces}), ' ') as metadata_title";
        if ( !$CATALOGUE->fetchColumn("select xml_is_well_formed(data::text) from public.metadata where uuid=:uuid", compact("uuid")) ){
        	$metadata_title = ", :undefined_metadata_title as metadata_title";
        	$metadata_desc = ", '' as metadata_desc";
        }
   
   
        $sql = " select m.id".
               $metadata_title.
               $metadata_desc.
               " from public.metadata m ".
               " where m.uuid= :uuid";
        list($fmeta_id, $metadata_title, $metadata_desc) = $CATALOGUE->fetchArray($sql, array("uuid"=>$uuid, "undefined_metadata_title"=>$uuid));
        
        $CATALOGUE->beginTransaction();
        if ( !$pk_stockage_carte ){
            $pk_carte_projet = $CATALOGUE->fetchColumn("select nextval('catalogue.seq_carte_projet')");
            $pk_fiche_metadonnees = $CATALOGUE->fetchColumn("select nextval('catalogue.seq_fiche_metadonnees')");
            $pk_stockage_carte = $CATALOGUE->fetchColumn("select nextval('catalogue.seq_stockage_carte')");
            
            $tabSql = array(
                "fiche_metadonnees" => array(
                    "pk_fiche_metadonnees" => $pk_fiche_metadonnees,
                    "fmeta_id" => $fmeta_id,
                ),
                "stockage_carte" => array(
                    "pk_stockage_carte" => $pk_stockage_carte,
                    "stkcard_id" => substr("1_".$pk_stockage_carte."_".$map_file, 0, 30),
                    "stkcard_path" => $map_file,
                    "stk_server" => 1,
                ),
                "carte_projet" => array(
                    "pk_carte_projet"   => $pk_carte_projet,
                    "cartp_id"          => "1_".$pk_carte_projet,
                    "cartp_nom"         => $metadata_title ?: $uuid,
                    "cartp_description" => $metadata_desc,
                    "cartp_format"      => $cartp_format,
                    "cartp_fk_stockage_carte" => $pk_stockage_carte,
                    "cartp_fk_fiche_metadonnees" => $pk_fiche_metadonnees,
                )
            );
            
            foreach($tabSql as $table=>$params){
                $keys = array_keys($params);
                $sql = "insert into ".$table." (".implode(", ", $keys).") values (:".implode(", :", $keys).")";
                $CATALOGUE->executeQuery($sql, $params);
            }
            
            $sql = "insert into SSDOM_VISUALISE_CARTE (ssdcart_fk_carte_projet, ssdcart_fk_sous_domaine) ".
                   " select distinct :pk_carte_projet::int, ssdcouch_fk_sous_domaine from ssdom_dispose_metadata WHERE uuid = :uuid";
            $CATALOGUE->executeQuery($sql, array("pk_carte_projet"=>$pk_carte_projet, "uuid"=>$uuid));
            
            //used to add links into metadata
            $response = $this->forward('ProdigeProdigeBundle:UpdateDomSdom:updateDomSdom', array(
                'fmeta_id'  => $fmeta_id,
                'modeData' => 'carte'
            ));
            
            if ( 0 === strpos($response->headers->get('Content-Type' ), 'application/json') ) {
                $response = json_decode($response->getContent(), true);
                if($response["success"]!=true){
                    throw $this->createHttpException('Echec de l\'initialisation de la carte');
                }
            
            }      
            
        } else {
            $params = array("pk_stockage_carte" => $pk_stockage_carte, "stkcard_path" => $map_file,);
            $sql = "update stockage_carte set stkcard_path=:stkcard_path where pk_stockage_carte=:pk_stockage_carte";
            $CATALOGUE->executeQuery($sql, $params);
        }
        $CATALOGUE->commit();
        if ( $cartp_format==self::DYNAMIC_MAP ){
            $adresse_catalogue = rtrim($this->getParameter('PRODIGE_URL_CATALOGUE'), "/");;
            $url = $adresse_catalogue."/geosource/mapInit/".$uuid;
            $response = $this->redirect($url);
        } else {
            $response = $this->forward('CarmenApiBundle:Prodige:evalInitMap', compact("request", "uuid"));
        }
        return $response;
    }
    
    /**
     * @Route(
     *    "/geosource/staticmap/browser/{uuid}/{routing}/{action}", 
     *    name="prodige_staticmap_browser", 
     *    defaults={"action"=""}, 
     *    requirements={"uuid"=".*", "action", "routing"="noroute|absolute|relative"}, 
     *    options={"expose"=true}
     * )
     * @Method({"GET","POST","PUT","PATCH","DELETE"})
     *
     * @param Request $request  The request object.
     * @param string  $mapId    The mapId.
     */
    public function staticMapBrowserAction(Request $request, $uuid, $routing, $action="") 
    {
        $CATALOGUE = $this->getCatalogueConnection('catalogue,public');
        $action = $action ?: $request->get('action', "");
        $response = $this->forward("CarmenApiBundle:FileBrowser:fileBrowserRest", compact("routing", "action","request"));
        if ( $action=="get-files" ) {
            $files = json_decode($response->getContent(), true);
            
            $search_files = array();
            foreach ($files as $index=>$file){
                $search_files[$index] = $file["full_path_name"];
                $files["_".$index] = $file;
                unset($files[$index]);
            }
            $existing = $CATALOGUE->fetchAll(
                "select stkcard_path ".
                " from carte_projet ".
                " inner join fiche_metadonnees on (carte_projet.cartp_fk_fiche_metadonnees = fiche_metadonnees.pk_fiche_metadonnees) ".
                " inner join stockage_carte on (stockage_carte.pk_stockage_carte = carte_projet.cartp_fk_stockage_carte)".
                " inner join metadata on (fiche_metadonnees.fmeta_id::bigint = metadata.id) ".
                " where metadata.uuid <> :uuid and stkcard_path in (:search_files)", 
                array ("uuid" => $uuid, "search_files"=>$search_files), 
                array("uuid" => \PDO::PARAM_STR, "search_files"=>\Doctrine\DBAL\Connection::PARAM_STR_ARRAY)
            );
            
            foreach ($existing as $file){
                $index = array_search($file["stkcard_path"], $search_files);
                unset($files["_".$index]);
            }
            $files = array_values($files);
        
            return new JsonResponse($files);
        }
        return $response;
    }

    /**
     * Get Doctrine connection to database CATALOGUE
     * @param ContainerInterface $container
     * @param string $schema
     * @return \Doctrine\DBAL\Connection
     */
    protected static function _getCatalogueConnection(ContainerInterface $container, $schema="public")
    {
        $connection = $container->get('doctrine')->getConnection(\Prodige\ProdigeBundle\Controller\BaseController::CONNECTION_CATALOGUE);
        $connection->exec('set search_path to '.$schema);
        return $connection;
    }
    /**
     * @param string $schema
     * @return \Doctrine\DBAL\Connection
     */
    protected function getCatalogueConnection($schema="public")
    {
        return self::_getCatalogueConnection($this->container, $schema);
    }
    
    /**
     * Vérifie les droits sur une fiche de métadonnée $uuid
     * @param ContainerInterface $container
     * @param string $uuid
     * @return boolean isAdminProdige
     * @throws AuthenticationException
     */
    public static function checkSecurity(ContainerInterface $container, $uuid=null)
    {
        if( $uuid ) {
            // récupérer l'ID de l'objet correspondant à l'uuid
            $conn = self::_getCatalogueConnection($container);
            $metadataId = $conn->fetchAll('select id from public.metadata where uuid = :uuid limit 1', array('uuid'=>$uuid));
            if(count($metadataId)>0) {
                $metadataId = $metadataId[0]['id'];
            } else {
								//TODO take into account carteperso and WMS maps, they don't have metadata uuid but arbitrary uuid

                $serv = $container->get('prodige.verifyrights'); $serv instanceof \Prodige\ProdigeBundle\Controller\VerifyRightsController;
                $req = new \Symfony\Component\HttpFoundation\Request(array(
                    "GLOBAL_RIGHTS" => "CMS"
                ));
                $response = $serv->verifyRightsAction($req);
                if( ! $response instanceof \Symfony\Component\HttpFoundation\Response ) {
                    throw new AuthenticationException('Erreur lors de la vérification des droits : format de réponse erroné');
                }
                $rights = json_decode($response->getContent(), true);
                
                if(json_last_error()===JSON_ERROR_NONE) {
                    return (isset($rights['ADMINISTRATION']) ? $rights['ADMINISTRATION'] : false) ;
                }
            }

            // verify CMS rights on metadata object (map, dataset...)
            $serv = $container->get('prodige.verifyrights'); $serv instanceof \Prodige\ProdigeBundle\Controller\VerifyRightsController;
            $req = new \Symfony\Component\HttpFoundation\Request(array(
                "ID"          => $metadataId,
                "TRAITEMENTS" => "CMS",
                "OBJET_TYPE"  => "metadata", 
                "OBJET_STYPE" => "",
            ));
            $response = $serv->verifyRightsAction($req);
            

            if( ! $response instanceof \Symfony\Component\HttpFoundation\Response ) {
                throw new AuthenticationException('Erreur lors de la vérification des droits : format de réponse erroné');
            }
            $rights = json_decode($response->getContent(), true);
            
            if(json_last_error()===JSON_ERROR_NONE) {
                if( ! @$rights['CMS'] ) {
                            \Prodige\ProdigeBundle\Common\SecurityExceptions::throwAccessDeniedExceptionCarte();
                }else{
                    return ( isset($rights["isAdmProdige"]) ? $rights["isAdmProdige"] : false) ;
                }
            } else {
                throw new AuthenticationException('Erreur lors de la vérification des droits : ' .  json_last_error_msg());
            }

        } else {
            throw new AuthenticationException('Paramètre manquant : uuid');
        }
    }
}
