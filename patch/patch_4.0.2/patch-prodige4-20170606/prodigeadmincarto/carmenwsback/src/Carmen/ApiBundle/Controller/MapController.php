<?php

namespace Carmen\ApiBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use Doctrine\DBAL\Query\QueryBuilder as SqlQueryBuilder;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

use Carmen\ApiBundle\Exception\ApiException;
use Carmen\ApiBundle\Entity\Map;
use Carmen\ApiBundle\Entity\Keyword;
use Carmen\ApiBundle\Entity\Layer;
use Carmen\ApiBundle\Entity\MapUi;
use Carmen\ApiBundle\Entity\MapLocator;
use Carmen\ApiBundle\Entity\UiModel;
use Carmen\ApiBundle\Entity\Field;
use Carmen\ApiBundle\Entity\LexTool;
use Carmen\ApiBundle\Entity\MapTool;
use Carmen\ApiBundle\Entity\MapAnnotationAttribute;
use Carmen\ApiBundle\Entity\Users;
use Carmen\ApiBundle\Entity\MapGroup;

use Carmen\OwsContextBundle\Controller\OwsContextController;
use Carmen\ApiBundle\Services\Helpers;
use Doctrine\ORM\Query\Expr;
use Carmen\ApiBundle\Entity\MapAudit;
use Carmen\ApiBundle\Entity\MapAudit2;
use Carmen\ApiBundle\Entity\LexMapAuditStatus;
use Carmen\ApiBundle\Entity\Account;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\DBAL\Types\Type;

// use FOS\RestBundle\Controller\Annotations\Delete;
// use FOS\RestBundle\Controller\Annotations\Get;
// use FOS\RestBundle\Controller\Annotations\Route;
// use FOS\RestBundle\Controller\Annotations\Head;
// use FOS\RestBundle\Controller\Annotations\Link;
// use FOS\RestBundle\Controller\Annotations\Patch;
// use FOS\RestBundle\Controller\Annotations\Post;
// use FOS\RestBundle\Controller\Annotations\Put;
// use FOS\RestBundle\Controller\Annotations\Unlink;
// use FOS\RestBundle\Request\ParamFetcher;
// use FOS\RestBundle\Controller\Annotations\QueryParam;   // GET params
// use FOS\RestBundle\Controller\Annotations\RequestParam; // POST params

// use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * Carmen Default API Controller.
 * 
 * @author alkante <support@alkante.com>
 * 
 * @Route("/api/map")
 */
class MapController extends BaseController
{
    const MAP_ENTITY = 'CarmenApiBundle:Map';
    const MAP_AUDIT = 'CarmenApiBundle:MapAudit';
    const KEYWORD_ENTITY = 'CarmenApiBundle:Keyword';
    
    public function saveMapAudit(EntityManager $em, Map $map, $action)
    {
        $mapAudit = new MapAudit();
        $mapAudit->setMapFile(($map->getMapModel() ? "modele_".preg_replace("!^modele_!", "", $map->getMapFile()) : $map->getMapFile()));
        $mapAudit->setMapAuditDate(new \DateTime());
        $mapAudit->setMapAuditStatus($action);
        $mapAudit->setAccount($map->getAccount());
        $mapAudit->setUserEmail($this->getUser()->getUserEmail());
        $em->persist($mapAudit);
    }
    
    /**
     * Duplicate or copy a map. 
     * <li><b>{$from_map_id<>null, $to_map_id=null}</b>        :<br/>$from_map_id<=>"published map" and we want copy the published map in an edited map</li>
     * <li><b>{$from_map_id=$to_map_id, $to_map_id<>null}</b>  :<br/>$from_map_id<=>"edited map". The map is created by application and will be published for the first time</li>
     * <li><b>{$from_map_id<>$to_map_id, $to_map_id<>null}</b> :<br/>$from_map_id<=>"edited map" + $to_map_id<=>"published map". The edited map will be recopy to its published map</li>
     * @param EntityManager $em
     * @param int $from_map_id
     * @param int $to_map_id
     * @throws ApiException
     * @return \Carmen\ApiBundle\Entity\Map
     * @todo : récupérer par layer->getFields()
     */
    protected function duplicateMapBySQL(EntityManager $em, $from_map_id, $to_map_id=null, $bForEdition=false)
    {
            $definitions = array(
              "map_id"       => array("from"=>"map", "to" => array("layer", "map_group", "map_locator", "map_tree", "map_tree_print", "map_ui", "favorite_area", "map_annotation_attribute", "map_keyword", "map_tool", )),
              "layer_id"     => array("from" => array("layer", "layer_layer_id_seq"), "to"=> array("field", "node_id"=>"map_tree", " node_id "=>"map_tree_print", "locator_criteria_layer_id"=>"map_locator")),
              "group_id"     => array("from" => array("map_group", "layer_layer_id_seq"), "to"=> array("node_id"=>"map_tree", " node_id "=>"map_tree_print", "node_parent"=>"map_tree", " node_parent "=>"map_tree_print")),
              "locator_id"   => array("from" => "map_locator", "to"=> array("locator_criteria_related"=>"map_locator"), "orderBy"=>"locator_criteria_rank"),
              "ui_id"        => array("from" => "map_ui", "to"=> array("ui_model")),
              "area_id"      => array("from" => "favorite_area"),
              "attribute_id" => array("from" => "map_annotation_attribute"),
              "field_id"     => array("from" => "field"),
              "ui_model_id"  => array("from" => "ui_model"),
            );
        
            $schema = "_tmp".$this->getUserId()."_".time();
            $tabSql = array("drop schema if exists {$schema} cascade", "create schema {$schema}");
            $created_tables = array();
            $created_columns = array();
            $conditions = array();
            foreach( $definitions as $pkey=>$usage ){
                $from = $usage["from"];
                if ( is_array($from) ){
                    list($from, $sequence) = $from;
                }
                else {
                    $sequence = "{$from}_{$pkey}_seq";
                }
                $orderBy = "";
                if ( isset($usage["orderBy"]) ){
                    $orderBy = " order by ".$usage["orderBy"];
                }
                if ( !array_key_exists($from, $created_tables) ){
                //only for the "map" table (the others are created with the $TO loop)
                    $created_tables[$from] = $orderBy;
                    $created_columns[$from] = $pkey;
                $tabSql[] = "create table {$schema}.{$from} (like carmen.{$from}, {$pkey}_new integer default ".($to_map_id ? $to_map_id : "nextval('carmen.{$sequence}'::regclass)")." )";
                    $tabSql[] = "insert into {$schema}.{$from} select * from carmen.{$from} where {$pkey} = :map_id".$orderBy;
                if ( $bForEdition ){
                    $tabSql[] = "update {$schema}.{$from} set published_id = :map_id";
                    $tabSql[] = "update {$schema}.{$from} set user_id = :user_id";
                } else {
                	$tabSql[] = "update {$schema}.{$from} set published_id = NULL";
                	$tabSql[] = "update {$schema}.{$from} set user_id = NULL";
                	$tabSql[] = "update {$schema}.{$from} set map_datepublication = date_trunc('second', now()::timestamp without time zone)";
                }
            } else {
                    $created_tables[$from] = $orderBy;
                    $created_columns[$from] = $pkey;
                    $tabSql[] = "alter table {$schema}.{$from} add column {$pkey}_new integer default nextval('carmen.{$sequence}'::regclass)";
                }
                if(isset($usage["to"])){
	                foreach ($usage["to"]?:array() as $joinKey=>$to){
	                    if ( is_numeric($joinKey) ) $joinKey = $pkey;
	                    $joinKey = trim($joinKey);
	                    if ( !array_key_exists($to, $created_tables) ){
	                        $created_tables[$to] = "";
	                        $tabSql[] = "create table {$schema}.{$to} (like carmen.{$to})";
	                        $tabSql[] = "insert into {$schema}.{$to} select * from carmen.{$to} where {$joinKey} in (select {$pkey} from {$schema}.{$from})";
	                    }
	                    $tabSql[] = "update {$schema}.{$to} newjoin set {$joinKey}=newmaster.{$pkey}_new from {$schema}.{$from} newmaster where newmaster.{$pkey}=newjoin.{$joinKey}";
	                }
                }
                $tabSql[] = "update {$schema}.{$from} newjoin set {$pkey}={$pkey}_new";
            }
            //die(implode(";\n", $tabSql));
            foreach ($created_columns as $table=>$column) {
                $tabSql[] = "alter table {$schema}.{$table} drop column {$column}_new";
            }
            foreach ($created_tables as $table=>$orderBy){
                $tabSql[] = "insert into carmen.{$table} select * from {$schema}.{$table}".$orderBy;
            } 
		        
            foreach($tabSql as $query){
              $em->createNativeQuery($query, new ResultSetMapping())
                 ->execute(array("map_id"=>$from_map_id, "user_id"=>$this->getUserId()));
		        }          
        
        $new_map_id = $from_map_id;
        $rsm  = new ResultSetMapping();
        $new_map = $em->createNativeQuery("select map_id from {$schema}.map", $rsm->addScalarResult("map_id", "map_id"))->getResult();;
        foreach ($new_map as $row){
        	$new_map_id = $row["map_id"];
        }
        
        $em->createNativeQuery("drop schema if exists {$schema} cascade", new ResultSetMapping())->execute();
        
        return $new_map_id;
    }
    
    /**
     * Duplicate or copy a map. 
     * <li><b>{$from_map_id<>null, $to_map_id=null}</b>        :<br/>$from_map_id<=>"published map" and we want copy the published map in an edited map</li>
     * <li><b>{$from_map_id=$to_map_id, $to_map_id<>null}</b>  :<br/>$from_map_id<=>"edited map". The map is created by application and will be published for the first time</li>
     * <li><b>{$from_map_id<>$to_map_id, $to_map_id<>null}</b> :<br/>$from_map_id<=>"edited map" + $to_map_id<=>"published map". The edited map will be recopy to its published map</li>
     * @param EntityManager $em
     * @param int $from_map_id
     * @param int $to_map_id
     * @throws ApiException
     * @return \Carmen\ApiBundle\Entity\Map
     * @todo : récupérer par layer->getFields()
     */
    protected function copyMapTo(EntityManager $em, $from_map_id, $to_map_id=null)
    {
        $user = $this->getUser();
        $user = $this->getRepository('CarmenApiBundle:Users')->find($user->getUserId());
        
        $user instanceof Users;
        $edit_directory = $this->getMapfileDirectory();
             
        //1. Récupérer la carte à éditer
        $criteria = array("mapId"=>$from_map_id, "publishedMap"=>$to_map_id);
        $old_map = $this->getRepository(MapController::MAP_ENTITY)->findOneBy($criteria);
        if( !$old_map ) {
            $exception = new EntityNotFoundException();
            throw new ApiException($this->_t("Not Found")." ".__METHOD__." ".__LINE__, $criteria, Response::HTTP_NOT_FOUND, $exception);
        }
        $old_map instanceof Map;
        
        $mapAudit = null;
        $mapAuditStatus = null;
        $bCopyData = true;
        /**
         * Create the copy for edition
         */
        if ( $to_map_id===null ){
            $mode = "CREATE_EDITION";
            //2. Suppress old edited map by the connected user TODO
            $criteria = array("user"=>$user->getUserId(), "publishedMap"=>$from_map_id);
            $existing_edits = $this->getRepository(MapController::MAP_ENTITY)->findBy($criteria);
            foreach($existing_edits as $existing_edit) {
                $existing_edit instanceof Map;
                $this->deleteMapAction($existing_edit->getMapId(), false);
            }
            
            //2. Suppress old edited map by the connected user TODO
            $oldiers = array();
            exec('find "'.$edit_directory.'" -maxdepth 1 -type f -mtime +3 -name "temp_admin_'.$user->getUserId().'_*.map"', $oldiers); 
            foreach($oldiers as $oldier){
                $oldierId = str_replace('temp_admin_'.$user->getUserId().'_', '', pathinfo($oldier, PATHINFO_FILENAME));
                if ( is_numeric($oldierId) ){
                    try {
                        if ( $this->getRepository(MapController::MAP_ENTITY)->find($oldierId) ){
                            $this->deleteMapAction($oldierId, false);
                        } else {
                            @unlink($oldier);
                            @unlink(str_replace(".map", ".json", $oldier));
                        }
                    } catch(\Exception $exception){}
                }
            }
            
            $new_map_id = $this->duplicateMapBySQL($em, $from_map_id, null, true);
            
            $new_map = $this->getRepository(MapController::MAP_ENTITY)->find($new_map_id);
            if( !$new_map ) {
                $exception = new EntityNotFoundException();
                throw new ApiException($this->_t("Not Found")." ".__METHOD__." ".__LINE__, $criteria, Response::HTTP_NOT_FOUND, $exception);
            }
            $new_map instanceof Map;
            $new_map->getLayers();
            $new_map->getLocators();
            $new_map->getTools();
            $new_map->getGroups();
            $new_map->getKeywords();
        }
        /**
         * The edited map is a new map => create the published instance
         */
        else if ( $to_map_id==$from_map_id ){
            //2. create publication copy
            
            $new_map_id = $this->duplicateMapBySQL($em, $from_map_id, null);
            
            $new_map = $this->getRepository(MapController::MAP_ENTITY)->find($new_map_id);
            if( !$new_map ) {
                $exception = new EntityNotFoundException();
                throw new ApiException($this->_t("Not Found")." ".__METHOD__." ".__LINE__, $criteria, Response::HTTP_NOT_FOUND, $exception);
            }
            $new_map instanceof Map;
            
            $old_mapfile = null;
            $old_is_model = $old_map->getMapModel();
            
            $old_map->setPublishedMap($new_map);
            $old_map->setUser($user);
            
            $bCopyData = false;
            $mode = "CREATE_PUBLICATION";
            $mapAuditStatus = LexMapAuditStatus::STATUS_CREATE;
            $mapAudit = $old_map;
        }
        /**
         * The edited map is published in its published instance
         */
        else {
            //query database to get some old properties
            
            $old_map->setPublishedMap($old_map);
            $em->persist($old_map); 
            $em->flush();
            $this->deleteMapAction($to_map_id, false);
               
            $new_map_id = $this->duplicateMapBySQL($em, $from_map_id, $to_map_id);
            $new_map = $this->getRepository(MapController::MAP_ENTITY)->find($new_map_id);
            if( !$new_map ) {
                $exception = new EntityNotFoundException();
                throw new ApiException($this->_t("Not Found")." ".__METHOD__." ".__LINE__, $criteria, Response::HTTP_NOT_FOUND, $exception);
            }
            $new_map instanceof Map;
            
            $old_map->setPublishedMap($new_map);
            
            $old_mapfile = $old_map->getMapfile();
            $old_is_model = $new_map->getMapModel();
            
            $mode = "PUBLISH";
            $mapAuditStatus = LexMapAuditStatus::STATUS_UPDATE;
            $mapAudit = $old_map;
        }
        
        $new_map instanceof Map;
        
        
        //Create a new map
        $em->persist($old_map); 
        $em->flush();                   
        $newMapId = $new_map->getMapId();
    
        if ( $mode == "CREATE_EDITION" ){
            $publishMap = $old_map; $editMap = $new_map;
            $mapfile = $this->ensureEditingMapfile($editMap, $publishMap);
            $publishMap = $new_map; $editMap = $old_map;
        } else {
            $publishMap = $old_map; $editMap = $new_map;
            
            $editedfile = "temp_admin_".$user->getUserId()."_".$publishMap->getMapId();
            $publishMap->setFullMapfilePath($edit_directory.$editedfile.".map");
            //Publication of the mapfile and deletion of edition map data
            $this->restorePublishedMapfile($new_map->getMapModel(), $editedfile, $old_map->getMapFile(), $old_mapfile);
        }
        
        return $publishMap;
    }
    
    /**
     * generate legend icons for viewer using helper from Mapserver API
     * @param string $map_file
     */
    protected function generateLegendIcons($map_file){
      //update legend icons (specific PRODIGE)
       $paramsGET = array(
            "directory" => $this->getMapfileDirectory()
        );
       $result = $this->callMapserverApi('/helpers/generate_legend/'.$map_file, "POST" , $paramsGET);
    }
    /**
     * Returns keywords formatted for use with a combobox.
     *
     * @Route("/combobox/keywords", name="carmen_ws_get_keywords_combobox", options={"expose"=true})
     * @Method({"GET"})
     *
     * @return JsonResponse Json array.
     */
    public function getKeywordsForCombo(Request $request)
    {
        $qb = $this->createQueryBuilder('CarmenApiBundle:Keyword', 'k')
                   ->leftJoin('k.category', 'category')->addSelect('partial category.{categoryId}')
        ;

        return new JsonResponse($qb->getQuery()->getResult(Query::HYDRATE_ARRAY));
    }

    /**
     * Restfull api to serve Entity.
     *
     * @Route("/rest/{id}", name="carmen_ws_map", defaults={"id"=null}, options={"expose"=true})
     * @Method({"GET","POST","PUT","DELETE"})
     * 
     * @param Request $request
     * @param unknown $entity
     * @param unknown $id
     */
    public function mapRestAction(Request $request,  $id = null)
    {
        switch ($request->getMethod()) {
            case 'GET':
                return $this->getMapAction($request, $id);
                break;
            case 'POST':
            case 'PUT':
                return $this->postMapAction($request, $id);
                break;
            case 'DELETE':
                return $this->deleteMapAction($id);
                break;
        }

        return $this->formatJsonError(Response::HTTP_METHOD_NOT_ALLOWED, $this->_t("Method not allowed"));
    }

    /**
     * Returns a list of Map or one Map given Id.
     * 
     * Extra filters can be added using query parameters such as 'filter[someColumn]=someValue'.
     * @see BaseController::getQueryBuilderList()
     * 
     * @param Request $request The request object.
     * @param string  $id      If set, returns a list with the requested Map only (if exists).
     * 
     * @Route("/get/{id}", name="carmen_ws_get_maps", requirements={"id"="\d+"}, defaults={"id"=null}, options={"expose"=true})
     * @Method({"GET"})
     * 
     * @return JsonResponse Json array.
     */
    public function getMapAction(Request $request, $id = null)
    {
        $account = $this->getUser()->getAccount();
        $filters = $request->query->get('filter', array());
        if ( !is_array($filters) && !empty($filters) ){
            $filters = json_decode($filters, true) ?: array();
        }
        $property = array(); $value = array();
        foreach ($filters as $filter){
            foreach($filter as $var_name=>$var_value){
                ${"$var_name"}[] = $var_value;
            }
        }
        $filters = array_combine($property, $value);
        $filters["account"] = $this->getUser()->getAccount()->getAccountId();
        
        $sorters = $request->query->get('sort', array());
        if ( !is_array($sorters) && !empty($sorters) ){
            $sorters = json_decode($sorters, true) ?: array();
        }
        $property = array(); $direction = array();
        foreach ($sorters as $filter){
            foreach($filter as $var_name=>$var_value){
                ${"$var_name"}[] = $var_value;
            }
        }
        $sorters = array_combine($property, $direction);

        $dqlQuery = $this->getQueryBuilderList(self::MAP_ENTITY, 'map', $id, $filters, $sorters/*, true*/);
        $dqlQuery->addSelect("tools");
        $dqlQuery->addSelect("mapProjectionEpsg");
        $dqlQuery->leftJoin("map.tools", "tools");
        $dqlQuery->leftJoin("map.mapProjectionEpsg", "mapProjectionEpsg");
        if ( $id ){
	        $dqlQuery->addSelect("uiModels");
	        $dqlQuery->leftJoin("map.mapUi", "mapUi");
	        $dqlQuery->leftJoin("mapUi.uiModels", "uiModels");
        }
        
        $defaults = array("layer_count"=>0, "has_layer_wms"=>false, "has_layer_wfs"=>false, "has_layer_atom"=>false);
        $data = $dqlQuery->getQuery()->getResult(Query::HYDRATE_ARRAY);
        $maps = array();
        
        $env = $this->getEnv();
        $conf = $this->container->getParameter('carmen_config');
        $separator  = isset($conf['carmen']['dns_url_prefix_sep']) ? $conf['carmen']['dns_url_prefix_sep'] : '.';
        
        foreach($data as $map){
            $maps[$map["mapId"]] = array_merge($map, $defaults, array("mapOnline"=>Map::getMapOnlineForMapfile($account, $map["mapFile"], $env, $separator)));
        }
        
        if ( !empty($maps) ){
            // TODO should be using DQL here...
            $sql = new \Doctrine\DBAL\Query\QueryBuilder($this->getConnection());
            $sql->select("map.map_id")
                ->addSelect("coalesce(count(layer_Id), 0) as layer_count")
                ->addSelect("coalesce(max(cast(layer_Wms as text) )::boolean, false) as has_layer_wms")
                ->addSelect("coalesce(max(cast(layer_Wfs as text) )::boolean, false) as has_layer_wfs")
                ->addSelect("coalesce(max(cast(layer_Atom as text))::boolean, false) as has_layer_atom")
                ->from("carmen.map", "map")
                ->leftJoin("map", "carmen.layer", "layer", "layer.map_id=map.map_id")
                ->where("map.map_Id in (".implode(", ", array_keys($maps)).")")
                ->groupBy("map.map_id");
            
            $data = $sql->execute()->fetchAll(\PDO::FETCH_ASSOC);
        
            foreach($data as $layer){
                $maps[$layer["map_id"]] = array_merge($maps[$layer["map_id"]], $defaults, $layer);
            }
            
        }
        return new JsonResponse(array_values($maps));
    }

    /**
     * Make a copy for edition of the published map.
     * @Route("/test/tree/{id}", name="carmen_ws_testtree_map", requirements={"id"="\d+"}, options={"expose"=true})
     * @Method({"GET"})
     * 
     * @param Request $request  The request object.
     * @param string  $id       If set, updates an existing user, else creates it.
     * 
     * @return JsonResponse Json object.
     */
    public function treeMapAction(Request $request, $id)
    {
        ini_set("xdebug.var_display_max_depth", 5);
        $criteria = array("mapId"=>$id);
	      $map = $this->getRepository(MapController::MAP_ENTITY)->findOneBy($criteria);
        $res = $this->getRepository("CarmenApiBundle:MapTree")->getTreeByMap($map);
        return new JsonResponse($res);
    }
    
    /**
     * Find the map matching this metadata uuid and forward to the editMapAction
     * @Route("/uuid/edit/{uuid}", name="carmen_ws_edit_map_uuid", requirements={"uuid"=".*"}, options={"expose"=true})
     * @Method({"GET"})
     * 
     * @param Request $request  The request object.
     * @param string  $uuid     metadata uuid
     * 
     * @return JsonResponse Json object.
     */
    public function editMapUuidAction(Request $request, $uuid)
    {
        $criteria = array("mapWmsmetadataUuid"=>$uuid, "publishedMap" => null);
	      $map = $this->getRepository(MapController::MAP_ENTITY)->findOneBy($criteria);
        if( !$map ) {
            throw $this->createNotFoundException("Carte non trouvée : uuid=$uuid");
        }
        return $this->editMapAction($request, $map->getMapId());
    }

    /**
     * Make a copy for edition of the published map.
     * @Route("/edit/{id}/{returnMap}", name="carmen_ws_edit_map", requirements={"id"="\d+", "returnMap"="all|resume"}, defaults={"returnMap"="all"}, options={"expose"=true})
     * @Method({"GET"})
     * 
     * @param Request $request  The request object.
     * @param string  $id       If set, updates an existing user, else creates it.
     * 
     * @return JsonResponse Json object.
     */
    public function editMapAction(Request $request, $id, $returnMap="all")
    {
        $me = $this;
        $oldMapId = $id;
        return $this->transactional(
            function(EntityManager $em) use ($me, $request, $oldMapId) {
                if( null != $oldMapId ) {
                    /**Create the edited copy for a map*/
                    $edit_map = $this->copyMapTo($em, $oldMapId, null);
                    return $edit_map;
                }
            },
            function(Map $map)use($returnMap){
                if ($returnMap=="resume"){
                    return new JsonResponse(array(
                        "success" => true,
                        "map" => array(
                            "mapId" => $map->getMapId(),
                            "publishedId" => $map->getPublishedMap()->getMapId(), 
                            "mapFile" => $map->getMapFile(),
                            "mapModel" => $map->getMapModel(),
                            "real_mapfile" => pathinfo($map->getFullMapfilePath(), PATHINFO_FILENAME),
                        )
                    ));
                }
                return $this->returnMap($map);
            }
        );
    }

    /**
     * return layers from Map
     * @param integer $mapId
     * @return NULL
     */
    protected function getMapLayers($mapId) {
        $criteria = array("account"=>$this->getUser()->getAccount()->getAccountId(), "mapId"=>$mapId);
        $publish_map = $this->getRepository(MapController::MAP_ENTITY)->findOneBy($criteria);

        if($publish_map) {
            return $publish_map->getLayers()->getValues();
        }
        return NULL;
    }

    /**
     * Get all layers for map file
     * @Route("/getMapLayers/{mapId}", name="carmen_ws_get_map_layers", options={"expose"=true})
     * @Method({"GET"})
     *
     * @param Request $request  The request object.
     * @param string  $mapFile      The map file.
     *
     * @return JsonResponse Json object.
     */
    public function getMapLayersAction(Request $request, $mapId)
    {
        $layersinfo = array();

        $callback = $request->get("callback", "");

        $layerset = $this->getMapLayers($mapId);
        if($layerset != NULL) {
            $i = 0;
            foreach($layerset as $new_layer) {
                $layersinfo[$i]['layerTitle'] = $new_layer->getLayerTitle();
                $layersinfo[$i]['layerId'] = $new_layer->getLayerId();
                $layersinfo[$i]['layerMsname'] = $new_layer->getLayerMsname();
                $layersinfo[$i]['active'] = false;
                $layersinfo[$i]['icon'] = "/images/leaf.gif";
                $i++;
            }
        }
        return new Response($callback."(".json_encode($layersinfo).")");
    }


    protected function arrayToString($arraytoconvert) {
        $str = "";
        foreach($arraytoconvert as $array_layer_ids) {
            foreach($array_layer_ids as $layer_id) {
                $str .= (($str != "") ? ", " : "").$layer_id;
            }
        }
        return $str;
    }
    
    /**
     * Copy layers & fields to layer & field tables in DB
     * @Route("/postLayers/{mapId}", name="carmen_ws_postlayers", options={"expose"=true})
     * @Method({"POST"})
     *
     * @param Request $request  The request object.
     * @param int mapId current map
     * @return JsonResponse Json object.
     */
    public function postLayers(Request $request, $mapId) {
       
        $layers = $request->request->get("layers");
        
        $layerController = new LayerController();
        $layerController->setContainer( $this->container );
        
        foreach ($layers as $layer){
            $requestLayer = new Request();
            foreach ($layer as $key => $value){
                $requestLayer->request->set($key, $value);
            }
            $response = $layerController->postLayerAction($requestLayer, $mapId);
            //var_dump($requestLayer->request->all(), $response->getContent());
        }
        return $this->reloadMap($mapId);
    }
    /**
     * Copy layers & fields to layer & field tables in DB
     * @Route("/copyLayersAndFieldsTables/{mapToId}/{mapFromId}/{mode}", name="carmen_ws_add_layers", options={"expose"=true})
     * @Method({"GET", "POST"})
     *
     * @param Request $request  The request object.
     * @param int mapToId current map
     * @param int mapFromId source map
     * @param string  $mode add/replace
     * @return JsonResponse Json object.
     */
    public function copyLayersAndFieldsTablesAction(Request $request, $mapToId,  $mapFromId, $mode) {
        
        $map_file_copy_layers_name="";
        $tabLayerMsNewNames = array();
        $me = $this;
        return $this->transactional(
            function(EntityManager $em) use ($me, $request, $mapToId, $mapFromId, &$map_file_copy_layers_name, &$tabLayerMsNewNames, $mode) {
                //BDD
                //take mapname from database
                
                $oMap = $this->getRepository(MapController::MAP_ENTITY)->find($mapToId);
                $ar_layer_ids = $request->get("ar_layer_ids", array());
                if ( empty($ar_layer_ids) ) return null;
                
                $tabMapNames  = $em->getConnection()->fetchAll("select map_id, map_file from carmen.map where map_id = {$mapFromId}");
                $map_file_copy_layers_name = $tabMapNames[0]["map_file"];
                if ( !$map_file_copy_layers_name ){
                    throw new \Exception("Aucune représentation par défaut pour cette métadonnée");//return null;
                }
                
                $str_layer_ids = implode(", ", $ar_layer_ids);
                $tabLayerNames  = $em->getConnection()->fetchAll("select layer_id, layer_msname from carmen.layer where layer_id in ({$str_layer_ids})");
                $tmp = array();
                foreach ($tabLayerNames as $row){
                    $tmp[array_search($row["layer_id"], $ar_layer_ids)]["layer_msname"] = $row["layer_msname"];
                }
                $tabLayerNames = $tmp;
                ksort($tabLayerNames);
                
                //New LayerMsNames must be recalculated
                $query = $em->createQuery("select l.layerMsname from ".LayerController::LAYER_ENTITY." l where l.map=:map_id")
                            ->setParameter("map_id", $mapToId);
                $msnames = $query->getResult();
                $count_layer = 0;
                $max_id = 0;
                foreach($msnames as $msname) {
                    if ( preg_match("!^layer(\d+)$!", $msname["layerMsname"]) ){
                        $max_id = max($max_id, preg_replace("!^layer(\d+)$!e", "intval('$1')", $msname["layerMsname"]));
                    }
                    $count_layer++;
                }
                $max_id = max($count_layer, $max_id)+1;
                
                foreach($tabLayerNames as $key =>$infos){
                    $tabLayerMsNewNames[$infos["layer_msname"]] = "layer".$max_id;
                    $max_id++;
                }
                
                
                $schema = "_tmp".$this->getUserId()."_".time();
                $tabSql = array("drop schema if exists {$schema} cascade", "create schema {$schema}");
                if($mode == "Replace") {
                    $ar_deleted_layer_ids = $em->getConnection()->fetchAll("select layer_id from carmen.layer where map_id = {$mapToId}");
                    if(! empty($ar_deleted_layer_ids)){
                        $tabSql[] = "delete from carmen.field where layer_id in ({$this->arrayToString($ar_deleted_layer_ids)})";
                        $tabSql[] = "delete from carmen.layer where map_id = {$mapToId}";
                    }
                }
                $pkey = "layer_id";
                $from = "layer";
                

                $sequence = "{$from}_{$pkey}_seq";
                $tabSql[] = "create table {$schema}.{$from} (like carmen.{$from}, {$pkey}_new integer default nextval('carmen.{$sequence}'::regclass), layer_rank int )";
                $tabSql[] = "insert into {$schema}.{$from} select * from carmen.{$from} where layer_id in ({$str_layer_ids})";
                foreach($ar_layer_ids as $rank=>$layer_id){
                    $tabSql[] = "update {$schema}.{$from} set layer_rank = ".$em->getConnection()->convertToDatabaseValue($rank, Type::INTEGER)." where layer_id = ".$em->getConnection()->convertToDatabaseValue($layer_id, Type::INTEGER)."";
                }

                $to = "field";
                $tabSql[] = "create table {$schema}.{$to} (like carmen.{$to},  field_id_new integer default nextval('carmen.field_field_id_seq'::regclass) )";
                $tabSql[] = "insert into {$schema}.{$to} select * from carmen.{$to} where layer_id in ({$str_layer_ids})";
                $tabSql[] = "update {$schema}.{$to} set layer_id = {$schema}.{$from}.{$pkey}_new from {$schema}.{$from} where field.layer_id = layer.layer_id";

                $tabSql[] = "update {$schema}.{$from} set layer_id = {$pkey}_new";
                $tabSql[] = "update {$schema}.{$to} set field_id = field_id_new";
                
                $tabSql[] = "update {$schema}.{$from} set map_id = {$mapToId}";
                $tabSql[] = "update {$schema}.{$from} set layer_msname ='old_'||layer_msname";
                foreach($tabLayerMsNewNames as $old_name=>$new_name){
                    $tabSql[] = "update {$schema}.{$from} set layer_msname ='{$new_name}' where layer_msname='old_{$old_name}'";
                }
                

                foreach($tabSql as $query) {
                    //var_dump($query);
                    $em->createNativeQuery($query, new ResultSetMapping())
                    ->execute();
                }

                $ar_new_layer_ids =  $em->getConnection()->fetchAll("select {$schema}.{$from}.{$pkey}_new from {$schema}.{$from}");

                $tabSql = array();
                $tabSql[] = "alter table {$schema}.{$from} drop column {$pkey}_new";
                $tabSql[] = "alter table {$schema}.{$to} drop column field_id_new";
                
                foreach(array("map_tree", "map_tree_print") as $tree_table){
                    $tabSql[] = "update carmen.{$tree_table} set node_pos=node_pos+".count($ar_layer_ids)." where node_parent is null and map_id={$mapToId}";
                    $tabSql[] = "insert into carmen.{$tree_table} (node_id, node_is_layer, node_parent, map_id, node_pos)
                     select layer_id, true, null, {$from}.map_id, layer_rank
                     from {$schema}.{$from}
                     ";
                }
                $tabSql[] = "alter table {$schema}.{$from} drop column layer_rank";
                
                $tabSql[] = "insert into carmen.layer select * from {$schema}.{$from}";
                $tabSql[] = "insert into carmen.field select * from {$schema}.{$to}";
                $tabSql[] = "drop schema if exists {$schema} cascade";

                foreach($tabSql as $query) {
                    //var_dump($query);
                    $em->createNativeQuery($query, new ResultSetMapping())
                    ->execute();
                }

                // Maintien des tree(s)
//                 foreach(array("MapTree"=>"map_tree", "MapTreePrint"=>"map_tree_print") as $tree_type=>$tree_table) {
//                  $em->createNativeQuery("update carmen.{$tree_table} set node_pos=node_pos + :nbLayersToAdd where map_id=:map_id and node_parent is null", new ResultSetMapping())
//                     ->setParameter("map_id", $mapToId)
//                     ->setParameter("nbLayersToAdd", count($ar_new_layer_ids))
//                     ->execute();


//                  foreach($ar_new_layer_ids as $index=>$ar_layer_id) {
//                      foreach($ar_layer_id as $layer_id) {
//                          $class_tree = "Carmen\\ApiBundle\\Entity\\".$tree_type;
//                          $nodetree = new $class_tree();
//                          $nodetree->setId($layer_id);
//                          $nodetree->setMap($oMap);
//                          $nodetree->setNodeIsLayer(true);
//                          $nodetree->setNodeOpened(false);
//                          $nodetree->setNodePos($index);
//                          $em->persist($nodetree);
//                      }
//                  }
//                 }
                return $oMap;
            },
            function(Map $display_map=null)use($request, $mapToId, &$map_file_copy_layers_name, &$tabLayerMsNewNames, $mode) {
                if ( is_null($display_map) ) return $this->reloadMap($mapToId);
                // map server
                $params = array(
                    "directory" => $this->getMapfileDirectory(),
                    "map_name_to_add" => $map_file_copy_layers_name,
                    "mapname" => $display_map->getMapFile(),
                    "layers_ms_new_names_to_add"=> $tabLayerMsNewNames
                );
                $editMap = $display_map;
                $publishMap = $this->getRepository(MapController::MAP_ENTITY)->find($display_map->getPublishedMap()->getMapId());
                $mapfile = $this->ensureEditingMapfile($editMap, $publishMap);
                $msMap = $this->callMapserverApi("/api/copylayers/".$mapfile."/".$mode, 'POST', array(), $params);

                return $this->reloadMap($mapToId);
            },
            function(\Exception $exception)use($request) {
                return $this->formatJsonError($exception->getCode()?:500, $exception->getMessage(), "", array("success"=>false, "msg"=>$this->_t("An error occured while adding layers"), 'stack'=>$exception->getTrace()));
            }
        );
        //die();
    }

    /**
     * 
     * @param unknown $mapId
     */
    protected function reloadMap($mapId){

        $databaseMap = $this->getRepository(MapController::MAP_ENTITY)->find($mapId);
        return $this->returnMap($databaseMap);

    }

    /**
     * Make a copy for edition of the published map.
     * @Route("/publish/{edit_id}/{publish_id}", name="carmen_ws_publish_map", requirements={"edit_id"="\d+", "publish_id"="\d+"}, options={"expose"=true})
     * @Method({"POST"})
     * 
     * @param Request $request  The request object.
     * @param string  $id       If set, updates an existing user, else creates it.
     * 
     * @return JsonResponse Json object.
     */
    public function publishMapAction(Request $request, $edit_id, $publish_id)
    {
        $me = $this;
        return $this->transactional(
            function(EntityManager $em) use ($me, $request, $edit_id, $publish_id) {
                $criteria = array("mapId"=>$publish_id);
                $publish_map = $this->getRepository(MapController::MAP_ENTITY)->findOneBy($criteria);
                if( !$publish_map ) {
                    $exception = new EntityNotFoundException();
                    throw new ApiException($this->_t("Not Found")." ".__METHOD__." ".__LINE__, $criteria, Response::HTTP_NOT_FOUND, $exception);
                }
                $publish_map instanceof Map;
                
                $old_mapFile = $publish_map->getMapFile();
                $old_mapModel =  $publish_map->getMapModel();
                
                $publish_map->setMapModel( $request->request->get('mapModel', ($publish_map->getMapModel() ? "true" : "false")) == "true" );
                $publish_map->setMapFile( $request->request->get('mapFile', $publish_map->getMapFile()));
                
                $mapFile = $publish_map->getMapFile();
                $mapModel =  $publish_map->getMapModel();
                
                if ( $old_mapFile!=$mapFile || $old_mapModel!=$mapModel ){
                	// publishing a new map
                	$publish_map->setPublishedMap($publish_map);
                	$em->persist($publish_map);
                	$em->flush($publish_map);
                }
                
                $edit_id = $publish_map->getPublishedMap()->getMapId();
                
                
                /**Publish an edited map*/
                $publish_map = $this->copyMapTo($em, $publish_id, $edit_id);
                $new_publish_id = $publish_map->getMapId();
  
                $this->getLogger()->debug(__METHOD__, compact(array("edit_id","old_publish_id", "publish_id", "new_publish_id", "mapFile", "mapModel")));
                
                $this->saveMapAudit($em, $publish_map, ($publish_id==$edit_id ? LexMapAuditStatus::STATUS_CREATE : LexMapAuditStatus::STATUS_UPDATE));
                
                
                /** generate tumbnails */
                $save_tumbnails = $request->request->get('save_thumbnails', false);
                
                if ($save_tumbnails) {
                  $fullPathToMap = $this->getCarmenConfig()->getMapserverMapfileDir();
                  $fullPathToMap = $fullPathToMap.$publish_map->getMapFile().".map";
                  $updater = $this->get('prodige.thumbnailsmetadataupdater');
                  $updater->updateMetadataThumbnails($fullPathToMap, $publish_map->getMapFile(), $publish_map->getMapWmsmetadataUuid());
                }
                
                /**specific prodige link map and layers**/
                $prodigeController = new ProdigeController();
                $prodigeController->setContainer( $this->container );
        
                $prodigeController->saveMapAction($publish_map->getMapWmsmetadataUuid(), $publish_map->getLayers());
                
                
                return $publish_map;
            },
            function(Map $publish_map)use($request){
                $this->getManager()->clear();
                $publish_map =  $this->getRepository(MapController::MAP_ENTITY)->find($publish_map->getMapId());
                return $this->transactional(
                    function(EntityManager $em)use ($publish_map){
                        
                        /**save geosource map **/
                        //NOT USED IN PRODIGE
	                    	/*$mapfile = $publish_map->getMapFile();
	                    	$params = array(
	                    			"directory" => $this->getMapfileDirectory()
	                    	);
	                    	$msMap = $this->callMapserverApi("/api/map/{$mapfile}", 'GET', $params);
	                    	try{
	                    	$this->saveGeosourceMapMetadata($publish_map, $msMap);
	                    	}catch(\Exception $exception){}
	                    	*/
                        /**create WMS mapfile**/
                        /*NOT USED IN PRODIGE
                        $wxsInfo = array(
                            "mapfile"               => $publish_map->getMapFile(),
                            "wms_onlineresource"=> $publish_map->getMapOnlinResource("WMS"),
                            "wfs_onlineresource"=> $publish_map->getMapOnlinResource("WFS"),
                        		"metadata_wms_uuid" => $publish_map->getMapWmsmetadataUuid(),
                        		"metadata_wfs_uuid" => $publish_map->getMapWfsmetadataUuid(),
                        		"metadata_atom_uuid"=> $publish_map->getMapAtommetadataUuid(),
                        		"urlServerData"     => $publish_map->getAccount()->getAccountDns()->getDnsScheme()."://". $publish_map->getAccount()->getAccountDns()->getDnsPrefixData()
                        													 .".".$publish_map->getAccount()->getAccountDns()->getDnsUrl(),
                        		"account_id"			  => $publish_map->getAccount()->getAccountId(),
                            "geosourceUrl" => $publish_map->getAccount()->getAccountGeonetworkurl(),
                            "layers" => array()
                        );
                        $layerset = $publish_map->getLayers()->getValues();

                        foreach($layerset as $new_layer){
                            $new_layer instanceof Layer;
                            $layerName = $new_layer->getLayerMsname();
                            $wxsInfo["layers"][$layerName] = array();
                            $wxsInfo["layers"][$layerName]["wxsname"] = $new_layer->getLayerName();
                            $wxsInfo["layers"][$layerName]["wxs_title"] = $new_layer->getLayerTitle();
                            $wxsInfo["layers"][$layerName]["wms_status"] = $new_layer->getLayerWms();
                            $wxsInfo["layers"][$layerName]["wfs_status"] = $new_layer->getLayerWfs();
                            $wxsInfo["layers"][$layerName]["downloadable"] = $new_layer->getLayerDownloadable();
                            $wxsInfo["layers"][$layerName]["atom"] = $new_layer->getLayerAtom();
                            
                                             
                            $fields = $new_layer->getFields()->getValues();
                            $strIncludeItems=""; 
                            foreach($fields as $new_field){
                                $new_field instanceof Field;
                                if($new_field->getFieldOgc())
                                    $strIncludeItems.=$new_field->getFieldName().", ";
                            }
                            if($strIncludeItems!="")
                                $strIncludeItems = substr($strIncludeItems, 0, -2);
                            $wxsInfo["layers"][$layerName]["gml_include_items"] = $strIncludeItems;
                            //TODO à vérifier
                            $wxsInfo["layers"][$layerName]["metadata_uuid"] = $new_layer->getLayerMetadataFile();
                      
                        }
                        
                         * $pathInfo= "/helpers/createWxsMapfile/".$publish_map->getMapFile();
       
                        $paramsGET = array(
                            "directory" => $this->getMapfileDirectory(),
                            "wms_mapfile" => "wms_".$publish_map->getMapFile()
                        );
                        $result = $this->callMapserverApi(
                            $pathInfo,
                            "POST",
                            $paramsGET,
                            array("wxsInfo" => serialize($wxsInfo))
                        ); */
                    
                    
                        //Provision of context files in the published data
                        $this->getOWSContext($publish_map->getMapId(), $publish_map->getMapFile(), true, "OWS");
                        //SPECIFIC PRODIGE 
                        //$this->generateLegendIcons($publish_map->getMapFile());
                        return $publish_map;
                    }, 
                    function(Map $publish_map)use($request){
                    	$data = array(
                    		"publishedId" => $publish_map->getPublishedMap()->getMapId(),
                    		"mapId" => $publish_map->getMapId(),
                    		"mapModel" => $publish_map->getMapModel(),
                    		"mapFile" => $publish_map->getMapFile(),
                    	);
                    	return $this->formatJsonSuccess(array("map"=>$data, "success" => true));
                    },
                    function(\Exception $exception)use($request){
				                $this->getManager()->clear();
                        return $this->formatJsonError($exception->getCode()?:500, $exception->getMessage(), "", array("success"=>false, "msg"=>$this->_t("An error occured during publication of the map")));
                    }
                );
            },
            function(\Exception $exception)use($request){
				        $this->getManager()->clear();
                return $this->formatJsonError($exception->getCode()?:500, $exception->getMessage(), "", array("success"=>false, "msg"=>$this->_t("An error occured during publication of the map"), 
                        "data"=>($exception instanceof ApiException ? $exception->getData() : $exception->getTraceAsString())
                ));
            }
        );
    }

    /**
     * @todo change get/post route definition
     * @Route("/post/{id}", name="carmen_ws_post_maps", requirements={"id"="\d+"}, defaults={"id"=null}, options={"expose"=true})
     * @Method({"POST", "PUT", "PATCH"})
     * 
     * @param Request $request  The request object.
     * @param string  $id       If set, updates an existing user, else creates it.
     * 
     * @return JsonResponse Json object.
     */
    public function postMapAction(Request $request, $id = null)
    {		set_time_limit(-1);
        return $this->transactional(
            function(EntityManager $em) use ($request, $id) {
                $bCreateMode = ($id===null);
                $user = $this->getUser();
                $user instanceof Users;
                $account = $user->getAccount();
                $account instanceof Account;
                $edit_directory = $this->getMapfileDirectory();
                $save = $request->request->all();
                
                /**
                 * [STEP 1] Keywords associated :
                 * - If a keyword has no ID then we create it according the selected keyword's category
                 * - Constructs the keywords dataset for the saved map by reading all keywords ID
                 */
                $keywords = $request->request->get('keywords', array());
                $keywords_id = array();
                foreach ($keywords as $index=>$keyword){
                    if ( !$keyword["keywordId"] ){
                        //mise en place du contexte d'enregistrement d'un keyword
                        $request->request->replace($keyword);
                        $keywords[$index] = $keyword = $this->doGenericPost($request, MapController::KEYWORD_ENTITY);
                        //restauration aux données principales soumises et sauvegardées
                        $request->request->replace($save);
                    }
                    $keywords_id[] = $keyword["keywordId"];
                }
                $request->request->set('keywords', $keywords_id);
                
                /**
                 * [STEP 2] Transform tools associated :
                 * The LexTools associated to the map are submitted in a Carmen.Model.MapTools (ExtJS) record where :
                 * - the LexTool identifier is a name of field in the record 
                 * - the property of association to the map is the value of field in the record 
                 * [{tool_identifier:is_associated_to_map}]
                 * Exceptions : some LexTool are associated to map with a scalar value (cf. table map_tool) 
                 * and some others save multiple values (cf. map_annotation_attribute) 
                 */
                $lexTools = $this->getRepository('CarmenApiBundle:LexTool')->findAll();
                $tmp = array();
                foreach ($lexTools as $lexTool){
                    $lexTool instanceof LexTool; 
                    $tmp[$lexTool->getToolIdentifier()] = $lexTool;
                    if ( $lexTool->getToolXmlNodeValue() ){
                    	$tmp[$lexTool->getToolXmlNodeValue()] = $lexTool;
                    }
                }
                $lexTools = $tmp;
                
                $tools = $request->request->get('tools') ?: array();
                if ( $bCreateMode ){
                    $tools = array('pan'=>true, 'zoom'=>true, 'fitall'=>true);
                }
                $mapTools = array();
                
            	  foreach($tools as $tool_identifier=>$tool_value){
                	  //BFE & HIL : specific PRODIGE(4.0), tools simple / advanced or null
                    /*if(is_bool($tool_value) ){
                        if($tool_value) {
                    	$mapTool = new MapTool();
                    	$mapTool->setTool($lexTools[$tool_identifier]);
                    	$mapTools[$tool_identifier] = $mapTool;
                        }
                    }*/
                    if($tool_value !== null) {
                        /*map_tool.maptool_value*/
                        if(isset($lexTools[$tool_identifier])) {
                            $mapTool = new MapTool();
                            $mapTool->setTool($lexTools[$tool_identifier]);
                            if(!is_bool($tool_value) ){
                                $mapTool->setMaptoolValue($tool_value);
                            } else {
                                if ( !$tool_value ) continue;
                            }
                            $mapTools[$tool_identifier] = $mapTool;
                        }
                    }
                }
                $request->request->remove('tools');
                
                // save a copy MapUi and remove it from the request so that its not handled as a Map property
                // the MapUi object is handled by its own after the Map is persisted (see below)
                $mapui = $request->request->get('mapUi', null);
                $request->request->remove('mapUi');
                
                $request->request->remove('layers');
                $request->request->remove('groups');
                $mapLocators = $request->request->get('locators', array());
                $request->request->remove('locators');
                
                /**
                 * [STEP 3] Save (create or update) the POST data in the map by internal submits in entity 
                 */
                $oMap = $this->doGenericPost($request, MapController::MAP_ENTITY, $id, null, true);
                $oMap instanceof Map;
                $em->merge($oMap);
                
                
                /**
                 * [STEP 4] For created map : configure the draft map
                 */
                if ( $bCreateMode ) {
                    //1. Suppress old edited map by the connected user TODO
                    $criteria = array("user"=>$user->getUserId());
                    $existing_edits = $this->getRepository(MapController::MAP_ENTITY)->findBy($criteria);
                    foreach($existing_edits as $existing_edit) {
                        $existing_edit instanceof Map;
                        $this->deleteMapAction($existing_edit->getMapId(), false);
                    }
                    
                    //2. configure admin map entry
                    $oMap->setPublishedMap($oMap);
                    $oMap->setUser($this->getUser());
                    $oMap->setMapDatepublication(new \DateTime());
                }

                /**
                 * [STEP 4] Save the tools to map - if there are submitted
                 */
                if ( !empty($tools) ){
                	$this->removeAll($em, $oMap->getTools());
                	foreach ($mapTools as $mapTool){
                		$oMap->addTools($mapTool);
                	}
                }
                
                $em->persist($oMap);
                $em->flush();
                
                
                /**
                 * [STEP 6] Save the MapLocators if submitted
                 */
                // delete old mapLocators attached to the Map
                $em->createQuery('DELETE FROM CarmenApiBundle:MapLocator l WHERE l.map = :mapId')
                   ->setParameter('mapId', $oMap->getMapId())
                   ->execute();
                // create and add new locators
                $created = array();
                $this->removeAll($em, $oMap->getLocators());
                foreach ($mapLocators as $locator) {
                    unset($locator['locatorId']); // if set, remove it
                    $locator['map'] = $oMap->getMapId();
                    if( isset($locator['locatorCriteriaRelated']) 
                        && is_int($locator['locatorCriteriaRelated']) 
                        && isset($created[$locator['locatorCriteriaRelated']]) 
                    ) {
                        $locator['locatorCriteriaRelated'] = $created[$locator['locatorCriteriaRelated']]->getLocatorId();
                    } else {
                        unset($locator['locatorCriteriaRelated']);
                        unset($locator['locatorCriteriaRelatedField']);
                    }
                    $locatorRequest = new Request(array()/*get*/, $locator/*post*/);
                    $entity = $this->_doGenericPost($locatorRequest, 'CarmenApiBundle:MapLocator');
                    $created[] = $entity;
                }
                foreach ($created as $locator){
                    $oMap->addLocators($locator);
                } 
                
                /**
                 * Removes keywords from the editable Keyword category which are no longer used by a map
                 */
                $this->deleteUnusedKeywords();
                
                /**
                 * [STEP 5] Save the MapUi if submitted
                 */
                $mapUiId = $oMap->getMapUi() ? $oMap->getMapUi()->getUiId() : null;
                $mapui['map'] = $oMap->getMapId(); // set the reference to the map
                if( $mapUiId ) {
                    // remove old uiModels, they will be re-created with the submitted data
                    foreach ( $oMap->getMapUi()->getUiModels() as $uiModel ) {
                        $em->remove($uiModel);
                    }
                    $em->flush();
                }
                
                $mapLocators = isset($mapui['mapLocators']) && $mapui['mapLocators'] ? $mapui['mapLocators'] : array(); // keet a copy of mapLocators
                unset($mapui['mapLocators']);                    // remove it from mapUi so that it's not handled by the form
                $uiModels = isset($mapui['uiModels']) && $mapui['uiModels'] ? $mapui['uiModels']  : array();       // keet a copy of uiModels
                unset($mapui['uiModels']);                       // remove it from mapUi so that it's not handled by the form
                
                $mapUiRequest = new Request(array()/*get*/, $mapui/*post*/);
                $entity = $this->_doGenericPost($mapUiRequest, MapUiController::MAPUI_ENTITY, $mapUiId);
                if ( $entity ) {
                    $entity->getUiModels()->clear();
                    foreach ($uiModels as $key => $accountModelFile) {
                        $uiModel = new UiModel();
                        $uiModel->setModelRank($key)
                                ->setAccountModel($em->getRepository("CarmenApiBundle:AccountModel")->findOneBy(array("account"=>$account->getAccountId(), "accountModelFile"=>$accountModelFile)));
                        $entity->adduiModels($uiModel);
                    }
                    $em->persist($entity);
                    $em->flush();
                }
                
                
                try {
                    $request->request->replace($save);
                    $mapserver_params = $this->getMapserverParams($request);
                    
                    $editMap = $oMap;
                    $publishMap = $this->getRepository(MapController::MAP_ENTITY)->find($oMap->getPublishedMap()->getMapId());
                    $mapfile = $this->ensureEditingMapfile($editMap, $publishMap);
                    
                    $result = $this->callMapserverApi("/api/map/{$mapfile}", 'POST', array(), $mapserver_params, true, array(
                        CURLOPT_CONNECTTIMEOUT => 0,
                        CURLOPT_TIMEOUT        => 0
                    ));

                    if( is_array($result) && isset($result['image']) && isset($result['extent']) ) {
                        $oMap->setMapRefmapImage( $result['image'] );
                        $oMap->setMapRefmapExtentXmin( $result['extent']['minx'] );
                        $oMap->setMapRefmapExtentYmin( $result['extent']['miny'] );
                        $oMap->setMapRefmapExtentXmax( $result['extent']['maxx'] );
                        $oMap->setMapRefmapExtentYmax( $result['extent']['maxy'] );
                        $em->persist($oMap);
                        $em->flush();
                    }
	                  
                }catch (\Exception $ex){
                    throw $ex;
                }
                
                return $oMap;
            },
            function(Map $map){return $this->returnMap($map, true/*saveGeosourceMetadata*/);}
        );
    }
    
    /**
     * Removes keywords from the editable Keyword category which are no longer used by a map
     * @return boolean
     */
    protected function deleteUnusedKeywords()
    {
        $sql = new SqlQueryBuilder($this->getConnection());
        $sql->delete("carmen.keyword", "keyword")
            ->where("category_id=".Keyword::EDITABLE_CATEGORY)
            ->andWhere("keyword_id not in (select keyword_id from carmen.map_keyword)");
        return $sql->execute();
    }
    
    /**
     * Construct the JsonResponse for a map
     * <li>generate and get OWSContext</li>
     * @param Map $map map entity
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    protected function returnMap(Map $map, $bSaveGeosourceMetadata=false, $onlyContext=false, $onlyObject=false)
    {
    
                    
        $user = $this->getUser();       $user    instanceof Users;
        $account = $user->getAccount(); $account instanceof Account;
        $metadataFile = str_replace("__file__", "%s", $this->generateUrl("carmen_data_account_metadata_file", array("account_path"=>$account->getAccountPath(), "file"=>"__file__")));
        if ( $metadataUrl = $account->getAccountGeonetworkurl() ) 
        	$metadataUrl = $metadataUrl."/fre/find?uuid=%s";
        
        $real_mapfile = pathinfo($map->getFullMapfilePath(), PATHINFO_FILENAME);
        if ( empty($real_mapfile) ){
          $real_mapfile = $this->ensureEditingMapfile($map, $map->getPublishedMap());
        }
        if ( $onlyContext ){
	        $result = array(
	            "jsonContext" => $this->getOWSContext($map->getMapId(), $map->getMapFile())
	        );
        	return $this->formatJsonSuccess($result);
        }
        
        
        $dbMap = json_decode($this->jsonSerializeEntity($map), true)?:array();
        
        if ( !$onlyObject ){
        try {
            $params = array(
                "directory" => $this->getMapfileDirectory()
            );
            $msMap = $this->callMapserverApi("/api/map/{$real_mapfile}", 'GET', $params);
                
            /**  NOT USED IN PRODIGE   
            if( $bSaveGeosourceMetadata ) {
            try{
                $this->saveGeosourceMapMetadata($map, $msMap);
        	      }catch(\Exception $exception){}
            }**/
                
                
            $imagePath = $this->getCarmenConfig()->getMapserverMapfileDir().'/'.$msMap["reference"]['image'];
            if( file_exists($imagePath) ) {
                // convert image to base64 so it can be displayed client side
                $msMap["reference"]['base64image'] = Helpers::base64image($imagePath);
            }
        } catch(\Exception $e) {
            $this->getLogger()->error($e->getMessage());
            throw $e;
        }
        
        
        $dbLayers = $dbMap["layers"] ?: array();
        $msLayers = $msMap["layers"] ?: array();
        unset($dbMap["layers"]);
        unset($msMap["layers"]);
        $layers = array();
        foreach($dbLayers as $dbLayer){
            
            $dbLayer["hrefLayerMetadataFile"] = "";
            $dbLayer["hrefLayerMetadataUuid"] = "";
            if ( $dbLayer["layerMetadataFile"] && strpos($dbLayer["layerMetadataFile"], "http")===false){
                $dbLayer["hrefLayerMetadataFile"] = sprintf($metadataFile, $dbLayer["layerMetadataFile"]);
            }else{
                $dbLayer["hrefLayerMetadataFile"] = $dbLayer["layerMetadataFile"];
            }
            if ( $dbLayer["layerMetadataUuid"] && $metadataUrl )
              $dbLayer["hrefLayerMetadataUuid"] = sprintf($metadataUrl, $dbLayer["layerMetadataUuid"]);
            $msLayer = null;
            foreach($msLayers as $msLayer){
                if ( $msLayer["name"]==$dbLayer["layerMsname"] ) {
                    $msLayer = LayerController::convertMapserverParams($msLayer);
                    break;
                }
            }
            if ( !isset($msLayer) ) $msLayer = array();
            $layers[] = array_merge($dbLayer, $msLayer);
        }
        
        $sql = new \Doctrine\DBAL\Query\QueryBuilder($this->getConnection());
        $sql->addSelect("count(layer_Id) as layer_count")
            ->addSelect("max(cast(layer_Wms as text)) as has_layer_wms")
            ->addSelect("max(cast(layer_Wfs as text)) as has_layer_wfs")
            ->addSelect("max(cast(layer_Atom as text)) as has_layer_atom")
            ->from("carmen.layer", "layer")
            ->where("map_Id = :mapId")->setParameter('mapId', $map->getMapId());
        $hasLayers = $sql->execute()->fetchAll(\PDO::FETCH_ASSOC);
        }
        $result = array(
            "success" => true,
            "map" => array_merge(
                $dbMap
            ,   array( "real_mapfile"=>$real_mapfile )
            , ($onlyObject 
            	? array() 
            	: array_merge($msMap
            ,   array("layers"=>$layers)
            ,   count($hasLayers)>0 ? $hasLayers[0] : array()
                )
              )
            ),
            "jsonContext" => ($onlyObject ? null : $this->getOWSContext($map->getMapId(),$real_mapfile))
        );
                    
        return $this->formatJsonSuccess($result);
    }


    /**
     * @todo change get/post route definition
     * @Route("/post/multi", name="carmen_ws_post_multi_maps", options={"expose"=true})
     * @Method({"POST", "PUT", "PATCH"})
     * 
     * @param Request $request  The request object.
     * @param string  $id       If set, updates an existing user, else creates it.
     * 
     * @return JsonResponse Json object.
     */
    public function postMultiMapsAction(Request $request)
    {
        try {
        	$directory = $this->getMapfileDirectory();
          $save = $request->request->all();
          $maps = $request->request->get('maps') ?: array();
          foreach($maps as $data){
          	$changeMapfile = array_intersect(array_keys($data), array("mapProjectionEpsg", "mapMinscale", "mapMaxscale"));
          	$changeMapfile = !empty($changeMapfile);
          	
          	$this->transactional(
          		function()use($request, $data, $directory, $changeMapfile){
          			$old_mapfile = null;
		            $request->request->replace($data);
	              $old_map = $this->getRepository(MapController::MAP_ENTITY)->find($data["mapId"]);
	              if ( $old_map ) {
	              	$old_map instanceof Map;
	              	$old_mapfile = $old_map->getMapfile();
	              }
	              
		            $oMap = $this->doGenericPost($request, MapController::MAP_ENTITY, $data["mapId"], null, true);
		            $oMap instanceof Map;
		            
		            if ( $old_mapfile && $old_mapfile!=$oMap->getMapFile() ){
		            	$path = $directory."/".($oMap->getMapModel() ? "modele_" : "")."%s.map";
		            	$old_mapfile = sprintf($path, $old_mapfile);
		            	$new_mapfile = sprintf($path, $oMap->getMapFile());
		            	if ( file_exists($old_mapfile) && !file_exists($new_mapfile) )
		            	  rename($old_mapfile, $new_mapfile);
		            }
		            $this->getOWSContext($oMap->getMapId(), $oMap->getMapFile());
		            
		            if ( $changeMapfile ){
		              $mapfile = ($oMap->getMapModel() ? "modele_".preg_replace("!^modele_!", "", $oMap->getMapFile()) : $oMap->getMapFile());
                  $result = $this->callMapserverApi("/api/map/floating/{$mapfile}", 'POST', array(), $this->getMapserverParams($request));
		            } 
	          	},
	          	null,
	          	function(\Exception $exception){throw $exception;}
          	);
          }
          $request->request->replace($save);
          
        }catch (\Exception $exception) {
            $data = array();
            if ( $exception instanceof ApiException ){
                $data = $exception->getData();
            }
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, $this->_t("Internal Server Error"), $this->_t("Transactional Exception").' : '.$exception->getMessage(), $data);
        }
        return $this->formatJsonSuccess();
    }


    /**
     * Deletes an Entity.
     *
     * @Route("/delete/{id}", name="carmen_ws_delete_map", options={"expose"=true})
     * @Method({"DELETE","GET"})
     *
     * @param string  $id If set, updates an existing user, else creates it.
     *
     * @return Response Empty response if success, JsonResponse error if failed.
     */
    public function deleteMapAction($id, $saveAudit=true)
    {
        $me = $this;
        $mapId = $id;
        return $this->transactional(
            function(EntityManager $em) use ($me, $mapId, $saveAudit) {
                if( null != $mapId ) {
                    $edit_directory = $me->getMapfileDirectory();
                    $read_directory = $me->getMapfileDirectory();
                    $files = array();
                    
                    $read_map = $this->getRepository('CarmenApiBundle:Map')->find($mapId);
                    if ( !$read_map ) {
                        throw new EntityNotFoundException();
                    }
                    $read_map instanceof Map;
                    
                    if ( $read_map->getPublishedMap() ){
                        $temp_mapfile = $this->ensureEditingMapfile($read_map, $read_map->getPublishedMap());
                        $files = array_merge($files, glob($edit_directory."/".$temp_mapfile.".*"));
                    } else {
                        $files = array_merge($files, glob($read_directory."/".$read_map->getMapFile().".*"));
                    }
            
                    //1. récupération de toutes les cartes en cours d'édition sur la carte sélectionnée
                    
                    $maps = array($mapId);
                    $qb = $me->getQueryBuilderList("CarmenApiBundle:Map", 'map', null, array("publishedMap"=> $mapId));
                    $editing = $qb->getQuery()->getResult();
                    foreach($editing as $edit_map){
                        $edit_map instanceof Map;
                        $maps[] = $edit_map->getMapId();
                        $files[] = $edit_directory."/".$edit_map->getMapFile();
                    }
                    $maps = array_reverse($maps);
                    $maps = implode(", ", $maps);
                    
                    //2. Conditions de suppression
                    // TODO should be using DQL here...
                    $conditions = array(
                        "layer_id in (select layer_id from carmen.layer where map_id in ({$maps}))" => array(
                             "field"
                        ),
                        "ui_id in (select ui_id from carmen.map_ui where map_id in ({$maps}))" => array(
                             "ui_model"
                        ),
                        "locator_id in (select locator_criteria_related from carmen.map_locator where map_id in ({$maps}))" => array(
                             "map_locator"
                        ),
                        "map_id in ({$maps})" => array(
                             "favorite_area", "map_group", "map_annotation_attribute", "map_tree", "map_tree_print"
                        ,    "map_keyword", "map_tool", "map_locator"
                        ,    "map_ui", "layer"
                        ,    "map"
                        )
                    );
                    
                    foreach($conditions as $where=>$tables){
                        foreach ($tables as $table){
                            // TODO should be using DQL here...
                            $sql = new \Doctrine\DBAL\Query\QueryBuilder($me->getConnection());
                            $sql->delete("carmen.".$table)
                                ->where($where);
                            $sql->execute();
                        }
                    }
                    
                    foreach ($files as $file){
                        if ( file_exists($file) && is_file($file) ){
                            $success = unlink($file);
                            if (!$success) {
                                throw new ApiException(sprintf($me->_t("Cannot delete file %s"), $file));
                            }
                        }
                    }
                
        	          if ( $saveAudit ) $this->saveMapAudit($em, $read_map, LexMapAuditStatus::STATUS_DELETE);
                }
            }
        );
    }

    /**
     * Make a copy for edition of the published map.
     * @Route("/savetrees/{id}", name="carmen_ws_savetrees_map", requirements={"id"="\d+"}, options={"expose"=true})
     * @Method({"POST"})
     * 
     * @param Request $request  The request object.
     * @param string  $id       If set, updates an existing user, else creates it.
     * 
     * @return JsonResponse Json object.
     */
    public function saveMapTreesAction(Request $request, $id)
    {
        $me = $this;
        $mapId = $id;
        return $this->transactional(
            function(EntityManager $em) use ($request, $me, $mapId) {
            	  $request instanceof Request;
                $oMap = $this->getRepository('CarmenApiBundle:Map')->find($mapId);
                $newIds = array();
                $readTree = function($class, $saveEntities, $node, $nodeParent=null, $nodePos=0)use(&$readTree, $em, $oMap, $request, &$newIds){
                  if ( !isset($node["root"]) || !$node["root"] ){
                  	if ( $saveEntities ) {
                  		switch( $node["type"] ){
                  			case "group" :
                  				$mapGroup = null;
                  				try {
                  				  $mapGroup = $this->getRepository('CarmenApiBundle:MapGroup')
                  				              ->findOneBy(array("groupId"=>$node["nodeId"], "map"=>$oMap));
                  				} catch(\Exception $exception){
                  				}
                  				if ( !$mapGroup ) {
	                  				$mapGroup = new MapGroup();
	                  				$mapGroup->setMap($oMap);
                  				}
                  				$mapGroup->setGroupName($node["groupName"]);
                  				$mapGroup->setGroupIdentifier($node["groupIdentifier"]);
                  				$em->persist($mapGroup);
                          $em->flush();
                          $node["nodeId"] = $mapGroup->getGroupId();
                          $newIds[$node["id"]] = $node["nodeId"];
                  			break;
                  			case "layer" :
		                      $em->createQueryBuilder()
		                      ->update("CarmenApiBundle:Layer", 'layer')
		                      ->set("layer.layerVisible", ":layerVisible")
		                      ->set("layer.layerOpacity", ":layerOpacity")
		                      ->where("layer.map = :map")
		                      ->andWhere("layer.layerId = :layerId")
		                      ->setParameter(":map", $oMap)
		                      ->setParameter(":layerId", $node["nodeId"])
		                      ->setParameter(":layerVisible", ($node["checked"] ? true : false))
		                      ->setParameter(":layerOpacity", (isset($node["opacity"]) ? $node["opacity"] : 1)*100)
		                      ->getQuery()->execute();
                  			break;
                  		}
                  	} else {
                  		switch( $node["type"] ){
                  			case "group" :
                  				if ( !isset($node["nodeId"]) && isset($newIds[$node["id"]]) ){
                  					$node["nodeId"] = $newIds[$node["id"]];
                  				}
                  			break;
                  		}
                  	}
                    $entity = new $class();
                    $entity->setMap($oMap);
                    $entity->setId($node["nodeId"]);
                    $entity->setNodeIsLayer(boolval($node["type"]=="layer"));
                    $entity->setNodeOpened(($node["leaf"] ? false : boolval($node["expanded"])));
                    $entity->setNodeParent($nodeParent);
                    $entity->setNodePos($nodePos);
                    $em->persist($entity);
                    $em->flush();
                    $nodeParent = $entity->getNodeId();
                  } else {
                    $nodeParent = null;
                  }
                  if ( isset($node["children"]) && is_array($node["children"]) ){
                    foreach($node["children"] as $position => $child){
                      $readTree($class, $saveEntities, $child, $nodeParent, $position);
                    }
                  }
                };
                
                $tree_name = $request->request->get("tree_name", "none");
                
                if ( $tree_name=="map_tree" || $tree_name=="all" ){
                /** 1. Save trees */
                /* In map_tree, the tree referes to existing layers or group in map */
	                $map_tree = $request->request->get("map_tree");
	                $this->getRepository('CarmenApiBundle:MapTree')->createQueryBuilder("tree")
	                ->delete("CarmenApiBundle:MapTree", "tree")->where("tree.map=".$mapId)
	                ->getQuery()->execute();
                $readTree("Carmen\ApiBundle\Entity\MapTree", true, $map_tree);
                
                /** 2. Delete of layers or groups that are not more existing in map_tree and thus not more existing in map */
                $sql = $em->createQueryBuilder()
                ->select("layer.layerId")
                ->from('CarmenApiBundle:Layer', "layer")
                ->leftJoin("CarmenApiBundle:MapTree", "tree", Expr\Join::WITH, "(tree.nodeIsLayer=true and tree.map=layer.map and tree.nodeId=layer.layerId)")
                ->where("layer.map= :map and tree.nodeId is null")
                ->setParameter("map", $oMap)
                ->getQuery();
                $deletedLayers = $sql->getResult(Query::HYDRATE_SCALAR);
                
                foreach($deletedLayers as $layerId){
                	LayerController::_deleteLayer($this, $mapId, $layerId);
                }
                
                $deletedGroups = $em->createQueryBuilder()
                ->select("grp")
                ->from('CarmenApiBundle:MapGroup', "grp")
                ->leftJoin("CarmenApiBundle:MapTree", "tree", Expr\Join::WITH, "(tree.nodeIsLayer=false and tree.map=grp.map and tree.nodeId=grp.groupId)")
                ->where("grp.map= :map and tree.nodeId is null")
                ->setParameter("map", $oMap)
                ->getQuery()->getResult(Query::HYDRATE_OBJECT);
                
                foreach($deletedGroups as $group){
                	  $em->remove($group);
                	  $em->flush();
                    foreach(array('MapTree', 'MapTreePrint') as $treeType){
	                    $trees = $me->getRepository('CarmenApiBundle:'.$treeType)->findBy(array("map"=>$oMap, "nodeId"=>$group->getGroupId(), "nodeIsLayer"=>false));
	                    foreach($trees as $tree){
	                    	$em->remove($tree);
                        $em->flush();
	                    }
                    }
                }
                
                }
                
                if ( $tree_name=="map_tree_print" || $tree_name=="all" ){
                	/** 1. Save trees */
	                /* In map_tree_print, the tree referes to existing layers or group in map */
	                $map_tree_print = $request->request->get("map_tree_print");
	                $this->getRepository('CarmenApiBundle:MapTreePrint')->createQueryBuilder("tree")
	                ->delete("CarmenApiBundle:MapTreePrint", "tree")->where("tree.map=".$mapId)
	                ->getQuery()->execute();
	                $readTree("Carmen\ApiBundle\Entity\MapTreePrint", false, $map_tree_print);
                }
                return $oMap;
            },
            function($map) use($request){
                
                $publishMap = $request->request->get('publishMap', null);
                if ( $publishMap && isset($publishMap["edit_id"]) && isset($publishMap["publish_id"]) ){
                	if ( isset($publishMap["POST"]) ) $request->request->replace($publishMap["POST"]);
                	return $this->publishMapAction($request, $publishMap["edit_id"], $publishMap["publish_id"]);
                }
                
                return $this->returnMap($map, false, true); 
            }
        );
    }
    
    /**
     * Extract from submitted values the Mapserver parameters for a node Map
     * @param Request $request
     * @return array
     */
    protected function getMapserverParams(Request $request)
    {
        $me = $this;
        $columns = array(
            "directory"     => function(array $data){ return $this->getMapfileDirectory();},
            "name"          => "mapTitle",
            "fontset"       => null,
            "symbolset"     => "symbolset",
            "imagecolor"    => "mapBgcolor",
            "imagetype"     => "mapOutputformat",
            "transparent"     => function(array $data){ return ($data["mapTransparency"] ? "true" : "false");},
            "extent"        => function(array $data){ return (isset($data["mapExtentXmin"]) ? implode(" ", array($data["mapExtentXmin"], $data["mapExtentYmin"], $data["mapExtentXmax"], $data["mapExtentYmax"])) : null); },
            "units"         => "mapUnits",
            "projection"    => function(array $data){ return (isset($data["mapProjectionEpsg"]) && $this->getRepository('CarmenApiBundle:LexProjection')->find($data["mapProjectionEpsg"]) ? 
                                                      strtoupper($this->getRepository('CarmenApiBundle:LexProjection')->find($data["mapProjectionEpsg"])->getProjectionProvider()).":".$data["mapProjectionEpsg"] 
                                                    : null); },
            "maxscaledenom" => "mapMaxscale",
            "minscaledenom" => "mapMinscale",
            "metadata"      => function(array $data){ 
            	if (isset($data["msMetadataMap"])){
            		//specific case for password both in mapserver and database
            		if( isset ($data["mapPassword"])){
            			$data["msMetadataMap"]["PS_g3_SECURITE_PWD"] = md5($data["mapPassword"]);
            		}
            		return json_encode($data["msMetadataMap"]); 
            	}
            	else return null; 
            },
            "template"      => null,
        );
        $data = $request->request->all();
        $results = array();
        foreach ($columns as $mapserver_key=>$post_key) {
            if ( $post_key===null ) continue;
            
            if ( is_callable($post_key) ){
                $results[$mapserver_key] = $post_key($data);
            } 
            else if ( isset($data[$post_key]) ){
                $results[$mapserver_key] = $data[$post_key];
            } else {
                
            }
        }
        $results = array_diff($results, array(null));
        
        // only add reference if it really exists
        if( isset($data["reference"]) ) {
            $results["reference"] = json_encode($data["reference"]);
        }
        
        return $results;
    }
    
    /**
     * Make a copy for edition of the published map.
     * @Route("/datashare/{map_id}", name="carmen_ws_map_datashare", requirements={"map_id"="\d+"}, options={"expose"=true})
     * @Method({"GET"})
     * 
     * @param Request $request  The request object.
     * @param string  $map_id       If set, updates an existing user, else creates it.
     * 
     * @return JsonResponse Json object.
     */
    public function getMapDataShareAction(Request $request, $map_id)
    {
        $env = $this->getEnv();
        $conf = $this->container->getParameter('carmen_config');
        $sep  = isset($conf['carmen']['dns_url_prefix_sep']) ? $conf['carmen']['dns_url_prefix_sep'] : '.';
        
        $result = array(
            "publicMap"    => "",
            "WMSUrl"       => "", 
            "WMSMetadata"  => "",
            "WFSUrl"       => "", 
            "WFSMetadata"  => "",
            "ATOMUrl"      => "", 
            "ATOMMetadata" => ""
         );

        $user = $this->getUser();
        if( !is_null($user) && $user instanceof Users ) {
            $geonetworkUrl = $user->getAccount()->getAccountGeonetworkurl();

            $criteria = array("mapId" => $map_id);
            $map = $this->getRepository(MapController::MAP_ENTITY)->findOneBy($criteria);
            
            //layers have WxS Publication ?
            $countLayer = array();
            $countLayer["WMS"] = 0;
            $countLayer["WFS"] = 0;
            $countLayer["ATOM"] = 0;
            if( !is_null($map) && $map instanceof Map ) {
                $result["publicMap"] = $map->getMapOnline($env, $sep);
                foreach ($map->getLayers() as $layer) {
                	$layer instanceof Layer;
                	//if( $layer->getLayerMetadataUuid() ) {
                	// WMS
                	if( $layer->getLayerWms() ) {
                		$countLayer["WMS"]++;
                	}
                	// WFS
                	if( $layer->getLayerWfs() ) {
                		$countLayer["WFS"]++;
                	}
                	// ATOM
                	if( $layer->getLayerAtom() /* TODO and/or $layer->getLayerDownloadable() ? */ ) {
                		$countLayer["ATOM"]++;
                	}
                	if($countLayer["WMS"]>0 && $countLayer["WFS"]>0 && $countLayer["ATOM"]>0)
                		break;
                	//}
                }
                $types = array("WMS", "WFS", "ATOM");
                foreach($types as $type) {
                    if($countLayer[$type]>0){
                        $result[$type."Url"] = $map->getMapOnlinResource($type, $env, $sep);
                        $uid = call_user_func(array($map, "getMap".strtolower(ucfirst($type))."MetadataUuid"));
                        if( $geonetworkUrl != "" && $uid != "" ) {
                            $result[$type."Metadata"] = $geonetworkUrl."/fre/find?uuid=".$uid;
                        }
                    }
                }
            }
        }
        return new JsonResponse($result);
    }
    
    /**
     * Saves Geosource map metadata
     * 
     * @param Map   $map   the Map
     * @param array $msMap Mapfile Map data
     */
    protected function saveGeosourceMapMetadata(Map $map, array $msMap)
    {
        $this->createOrUpdateGeosourceMapMetadata($map, $msMap);
    }
    
}
