/**
 * @date 25/03/2016
 * @author alkante
 * @version 3.05
 */
DELETE FROM carmen.lex_analyse_type;
INSERT INTO carmen.lex_analyse_type( analyse_type_name, analyse_type_code ) VALUES ( 'Symbole unique', 'UNIQSYMBOL' ); 
INSERT INTO carmen.lex_analyse_type( analyse_type_name, analyse_type_code ) VALUES ( 'Analyse par valeur unique', 'UNIQVALUE' ); 
INSERT INTO carmen.lex_analyse_type( analyse_type_name, analyse_type_code ) VALUES ( 'Analyse par couleurs graduées', 'GRADUATECOLOR' ); 
INSERT INTO carmen.lex_analyse_type( analyse_type_name, analyse_type_code ) VALUES ( 'Analyse par symboles gradués', 'GRADUATESYMBOL' ); 
INSERT INTO carmen.lex_analyse_type( analyse_type_name, analyse_type_code ) VALUES ( 'Analyse par symboles proportionnels', 'PROPORTIONAL' ); 
INSERT INTO carmen.lex_analyse_type( analyse_type_name, analyse_type_code ) VALUES ( 'Analyse par secteurs', 'PIECHART' ); 
INSERT INTO carmen.lex_analyse_type( analyse_type_name, analyse_type_code ) VALUES ( 'Analyse par histogrammes', 'BARCHART' ); 
INSERT INTO carmen.lex_analyse_type( analyse_type_name, analyse_type_code ) VALUES ( 'Raster', 'RASTER' ); 


DELETE FROM carmen.lex_category_keyword;
INSERT INTO carmen.lex_category_keyword( category_name ) VALUES ( 'Mots-clés libres' ); 


DELETE FROM carmen.lex_color_id;
INSERT INTO carmen.lex_color_id( color_name, color_key ) VALUES ( 'vert', 'green' ); 
INSERT INTO carmen.lex_color_id( color_name, color_key ) VALUES ( 'noir', 'black' ); 
INSERT INTO carmen.lex_color_id( color_name, color_key ) VALUES ( 'gris', 'grey' ); 
INSERT INTO carmen.lex_color_id( color_name, color_key ) VALUES ( 'violet', 'purple' ); 
INSERT INTO carmen.lex_color_id( color_name, color_key ) VALUES ( 'Olive', 'olive' ); 
INSERT INTO carmen.lex_color_id( color_name, color_key ) VALUES ( 'bleu', 'blue' ); 


DELETE FROM carmen.lex_field_datatype;
INSERT INTO carmen.lex_field_datatype( field_datatype_name, field_datatype_code ) VALUES ( 'Texte', 'STRING' ); 
INSERT INTO carmen.lex_field_datatype( field_datatype_name, field_datatype_code ) VALUES ( 'Numerique', 'NUMBER' ); 
INSERT INTO carmen.lex_field_datatype( field_datatype_name, field_datatype_code ) VALUES ( 'Date', 'DATE' ); 
INSERT INTO carmen.lex_field_datatype( field_datatype_name, field_datatype_code ) VALUES ( 'Booléen', 'BOOLEAN' ); 


DELETE FROM carmen.lex_field_type;
INSERT INTO carmen.lex_field_type( field_type_name ) VALUES ( 'Normal' ); 
INSERT INTO carmen.lex_field_type( field_type_name ) VALUES ( 'URL' ); 
INSERT INTO carmen.lex_field_type( field_type_name ) VALUES ( 'Image' ); 
INSERT INTO carmen.lex_field_type( field_type_name ) VALUES ( 'Date (AAAAMMJJ)' ); 
INSERT INTO carmen.lex_field_type( field_type_name ) VALUES ( 'Date (AAAAJJMM)' ); 
INSERT INTO carmen.lex_field_type( field_type_name ) VALUES ( 'Date (MMJJAAAA)' ); 
INSERT INTO carmen.lex_field_type( field_type_name ) VALUES ( 'Date (JJMMAAAA)' ); 


DELETE FROM carmen.lex_inspire_theme;
INSERT INTO carmen.lex_inspire_theme( theme_name ) VALUES ( 'Adresses' ); 
INSERT INTO carmen.lex_inspire_theme( theme_name ) VALUES ( 'Altitude' ); 
INSERT INTO carmen.lex_inspire_theme( theme_name ) VALUES ( 'Bâtiments' ); 
INSERT INTO carmen.lex_inspire_theme( theme_name ) VALUES ( 'Caractéristiques géographiques météorologiques' ); 
INSERT INTO carmen.lex_inspire_theme( theme_name ) VALUES ( 'Caractéristiques géographiques océanographiques' ); 
INSERT INTO carmen.lex_inspire_theme( theme_name ) VALUES ( 'Conditions atmosphériques' ); 
INSERT INTO carmen.lex_inspire_theme( theme_name ) VALUES ( 'Dénominations géographiques' ); 
INSERT INTO carmen.lex_inspire_theme( theme_name ) VALUES ( 'Géologie' ); 
INSERT INTO carmen.lex_inspire_theme( theme_name ) VALUES ( 'Habitats et biotopes' ); 
INSERT INTO carmen.lex_inspire_theme( theme_name ) VALUES ( 'Hydrographie' ); 
INSERT INTO carmen.lex_inspire_theme( theme_name ) VALUES ( 'Installations agricoles et aquacoles' ); 
INSERT INTO carmen.lex_inspire_theme( theme_name ) VALUES ( 'Installations de suivi environnemental' ); 
INSERT INTO carmen.lex_inspire_theme( theme_name ) VALUES ( 'Lieux de production et sites industriels' ); 
INSERT INTO carmen.lex_inspire_theme( theme_name ) VALUES ( 'Occupation des terres' ); 
INSERT INTO carmen.lex_inspire_theme( theme_name ) VALUES ( 'Ortho-imagerie' ); 
INSERT INTO carmen.lex_inspire_theme( theme_name ) VALUES ( 'Parcelles cadastrales' ); 
INSERT INTO carmen.lex_inspire_theme( theme_name ) VALUES ( 'Ressources minérales' ); 
INSERT INTO carmen.lex_inspire_theme( theme_name ) VALUES ( 'Référentiels de coordonnées' ); 
INSERT INTO carmen.lex_inspire_theme( theme_name ) VALUES ( 'Régions biogéographiques' ); 
INSERT INTO carmen.lex_inspire_theme( theme_name ) VALUES ( 'Régions maritimes' ); 
INSERT INTO carmen.lex_inspire_theme( theme_name ) VALUES ( 'Répartition de la population  démographie' ); 
INSERT INTO carmen.lex_inspire_theme( theme_name ) VALUES ( 'Répartition des espèces' ); 
INSERT INTO carmen.lex_inspire_theme( theme_name ) VALUES ( 'Réseaux de transport' ); 
INSERT INTO carmen.lex_inspire_theme( theme_name ) VALUES ( 'Santé et sécurité des personnes' ); 
INSERT INTO carmen.lex_inspire_theme( theme_name ) VALUES ( 'Services d''utilité publique et services publics' ); 
INSERT INTO carmen.lex_inspire_theme( theme_name ) VALUES ( 'Sites protégés' ); 
INSERT INTO carmen.lex_inspire_theme( theme_name ) VALUES ( 'Sols' ); 
INSERT INTO carmen.lex_inspire_theme( theme_name ) VALUES ( 'Sources d''énergie' ); 
INSERT INTO carmen.lex_inspire_theme( theme_name ) VALUES ( 'Systèmes de maillage géographique' ); 
INSERT INTO carmen.lex_inspire_theme( theme_name ) VALUES ( 'Unités administratives' ); 
INSERT INTO carmen.lex_inspire_theme( theme_name ) VALUES ( 'Unités statistiques' ); 
INSERT INTO carmen.lex_inspire_theme( theme_name ) VALUES ( 'Usage des sols' ); 
INSERT INTO carmen.lex_inspire_theme( theme_name ) VALUES ( 'Zones de gestion, de restriction ou de réglementation et unités de déclaration' ); 
INSERT INTO carmen.lex_inspire_theme( theme_name ) VALUES ( 'Zones à risque naturel' ); 


DELETE FROM carmen.lex_iso_condition;
INSERT INTO carmen.lex_iso_condition( condition_name ) VALUES ( 'Aucune condition ne s''applique' ); 
INSERT INTO carmen.lex_iso_condition( condition_name ) VALUES ( 'Conditions inconnues' ); 
INSERT INTO carmen.lex_iso_condition( condition_name ) VALUES ( 'Autres' ); 


DELETE FROM carmen.lex_iso_constraint;
INSERT INTO carmen.lex_iso_constraint( constraint_name ) VALUES ( 'Autres restrictions' ); 
INSERT INTO carmen.lex_iso_constraint( constraint_name ) VALUES ( 'Brevet' ); 
INSERT INTO carmen.lex_iso_constraint( constraint_name ) VALUES ( 'Brevet en instance' ); 
INSERT INTO carmen.lex_iso_constraint( constraint_name ) VALUES ( 'Droit de propriété intellectuelle / Droit patrimonial' ); 
INSERT INTO carmen.lex_iso_constraint( constraint_name ) VALUES ( 'Droit d''auteur /  Droit moral (copyright)' ); 
INSERT INTO carmen.lex_iso_constraint( constraint_name ) VALUES ( 'Licence' ); 
INSERT INTO carmen.lex_iso_constraint( constraint_name ) VALUES ( 'Marque de commerce' ); 
INSERT INTO carmen.lex_iso_constraint( constraint_name ) VALUES ( 'Restreint' ); 


DELETE FROM carmen.lex_iso_language;
INSERT INTO carmen.lex_iso_language( language_code, language_name ) VALUES ( 'ger', 'Allemand' ); 
INSERT INTO carmen.lex_iso_language( language_code, language_name ) VALUES ( 'eng', 'Anglais' ); 
INSERT INTO carmen.lex_iso_language( language_code, language_name ) VALUES ( 'bul', 'Bulgare' ); 
INSERT INTO carmen.lex_iso_language( language_code, language_name ) VALUES ( 'dan', 'Danois' ); 
INSERT INTO carmen.lex_iso_language( language_code, language_name ) VALUES ( 'spa', 'Espagnol' ); 
INSERT INTO carmen.lex_iso_language( language_code, language_name ) VALUES ( 'est', 'Estonien' ); 
INSERT INTO carmen.lex_iso_language( language_code, language_name ) VALUES ( 'fin', 'Finlandais' ); 
INSERT INTO carmen.lex_iso_language( language_code, language_name ) VALUES ( 'fre', 'Français' ); 
INSERT INTO carmen.lex_iso_language( language_code, language_name ) VALUES ( 'gre', 'Grec' ); 
INSERT INTO carmen.lex_iso_language( language_code, language_name ) VALUES ( 'hun', 'Hongrois' ); 
INSERT INTO carmen.lex_iso_language( language_code, language_name ) VALUES ( 'irl', 'Irlandais' ); 
INSERT INTO carmen.lex_iso_language( language_code, language_name ) VALUES ( 'ice', 'Islandais' ); 
INSERT INTO carmen.lex_iso_language( language_code, language_name ) VALUES ( 'ita', 'Italien' ); 
INSERT INTO carmen.lex_iso_language( language_code, language_name ) VALUES ( 'lav', 'Letton' ); 
INSERT INTO carmen.lex_iso_language( language_code, language_name ) VALUES ( 'lit', 'Lituanien' ); 
INSERT INTO carmen.lex_iso_language( language_code, language_name ) VALUES ( 'mlt', 'Maltais' ); 
INSERT INTO carmen.lex_iso_language( language_code, language_name ) VALUES ( 'dut', 'Néerlandais' ); 
INSERT INTO carmen.lex_iso_language( language_code, language_name ) VALUES ( 'nor', 'Norvégien' ); 
INSERT INTO carmen.lex_iso_language( language_code, language_name ) VALUES ( 'pol', 'Polonais' ); 
INSERT INTO carmen.lex_iso_language( language_code, language_name ) VALUES ( 'por', 'Portugais' ); 
INSERT INTO carmen.lex_iso_language( language_code, language_name ) VALUES ( 'roh', 'Romanche' ); 
INSERT INTO carmen.lex_iso_language( language_code, language_name ) VALUES ( 'rum', 'Roumain' ); 
INSERT INTO carmen.lex_iso_language( language_code, language_name ) VALUES ( 'slo', 'Slovaque' ); 
INSERT INTO carmen.lex_iso_language( language_code, language_name ) VALUES ( 'slv', 'Slovène' ); 
INSERT INTO carmen.lex_iso_language( language_code, language_name ) VALUES ( 'swe', 'Suédois' ); 
INSERT INTO carmen.lex_iso_language( language_code, language_name ) VALUES ( 'cze', 'Tchèque' ); 


DELETE FROM carmen.lex_iso_theme;
INSERT INTO carmen.lex_iso_theme( theme_name, theme_code ) VALUES ( 'Agriculture', 'farming' ); 
INSERT INTO carmen.lex_iso_theme( theme_name, theme_code ) VALUES ( 'Altimétrie', 'elevation' ); 
INSERT INTO carmen.lex_iso_theme( theme_name, theme_code ) VALUES ( 'Biologie, faune et flore', 'biota' ); 
INSERT INTO carmen.lex_iso_theme( theme_name, theme_code ) VALUES ( 'Cadastre, aménagement', 'planningCadastre' ); 
INSERT INTO carmen.lex_iso_theme( theme_name, theme_code ) VALUES ( 'Carte de référence de la couverture terrestre', 'imageryBaseMapsEarthCover' ); 
INSERT INTO carmen.lex_iso_theme( theme_name, theme_code ) VALUES ( 'Climatologie, météorologie', 'climatologyMeteorologyAtmosphere' ); 
INSERT INTO carmen.lex_iso_theme( theme_name, theme_code ) VALUES ( 'Constructions et ouvrages', 'structure' ); 
INSERT INTO carmen.lex_iso_theme( theme_name, theme_code ) VALUES ( 'Eaux intérieures, Hydrographie', 'inlandWaters' ); 
INSERT INTO carmen.lex_iso_theme( theme_name, theme_code ) VALUES ( 'Economie', 'economy' ); 
INSERT INTO carmen.lex_iso_theme( theme_name, theme_code ) VALUES ( 'Environnement', 'environment' ); 
INSERT INTO carmen.lex_iso_theme( theme_name, theme_code ) VALUES ( 'Infrastructures de transport', 'transportation' ); 
INSERT INTO carmen.lex_iso_theme( theme_name, theme_code ) VALUES ( 'Infrastructures militaires', 'intelligenceMilitary' ); 
INSERT INTO carmen.lex_iso_theme( theme_name, theme_code ) VALUES ( 'Limites politiques et administratives', 'boundaries' ); 
INSERT INTO carmen.lex_iso_theme( theme_name, theme_code ) VALUES ( 'Localisation', 'location' ); 
INSERT INTO carmen.lex_iso_theme( theme_name, theme_code ) VALUES ( 'Océans', 'oceans' ); 
INSERT INTO carmen.lex_iso_theme( theme_name, theme_code ) VALUES ( 'Santé', 'health' ); 
INSERT INTO carmen.lex_iso_theme( theme_name, theme_code ) VALUES ( 'Sciences de la terre, géosciences', 'geoscientificInformation' ); 
INSERT INTO carmen.lex_iso_theme( theme_name, theme_code ) VALUES ( 'Télécommunication, approvisionnement et énergie', 'utilitiesCommunication' ); 


DELETE FROM carmen.lex_layer_type;
INSERT INTO carmen.lex_layer_type( layer_type_code, layer_type_name ) VALUES ( 'VECTOR', 'Vector' ); 
INSERT INTO carmen.lex_layer_type( layer_type_code, layer_type_name ) VALUES ( 'RASTER', 'Raster' ); 
INSERT INTO carmen.lex_layer_type( layer_type_code, layer_type_name ) VALUES ( 'WMS', 'WMS' ); 
INSERT INTO carmen.lex_layer_type( layer_type_code, layer_type_name ) VALUES ( 'WFS', 'WFS' ); 
INSERT INTO carmen.lex_layer_type( layer_type_code, layer_type_name ) VALUES ( 'WMSC', 'WMS-C' ); 
INSERT INTO carmen.lex_layer_type( layer_type_code, layer_type_name ) VALUES ( 'POSTGIS', 'Postgis' );
INSERT INTO carmen.lex_layer_type( layer_type_code, layer_type_name ) VALUES ( 'WMTS', 'WMTS' ); 


DELETE FROM carmen.lex_map_audit_status;
INSERT INTO carmen.lex_map_audit_status( map_audit_status_name ) VALUES ( 'CREATE' ); 
INSERT INTO carmen.lex_map_audit_status( map_audit_status_name ) VALUES ( 'UPDATE' ); 
INSERT INTO carmen.lex_map_audit_status( map_audit_status_name ) VALUES ( 'DELETE' ); 


DELETE FROM carmen.lex_projection;
INSERT INTO carmen.lex_projection( projection_epsg, projection_name, projection_provider ) VALUES ( 2154, 'RGF93 / Lambert-93', 'EPSG' ); 
INSERT INTO carmen.lex_projection( projection_epsg, projection_name, projection_provider ) VALUES ( 4258, 'ETRS89 (INSPIRE)', 'EPSG' ); 
INSERT INTO carmen.lex_projection( projection_epsg, projection_name, projection_provider ) VALUES ( 84, 'CRS84 (INSPIRE)' , 'CRS' ); 
INSERT INTO carmen.lex_projection( projection_epsg, projection_name, projection_provider ) VALUES ( 4171, 'RGF93 non projete' , 'EPSG' ); 
INSERT INTO carmen.lex_projection( projection_epsg, projection_name, projection_provider ) VALUES ( 3942, 'RGF93 / CC42' , 'EPSG' ); 
INSERT INTO carmen.lex_projection( projection_epsg, projection_name, projection_provider ) VALUES ( 3943, 'RGF93 / CC43' , 'EPSG' ); 
INSERT INTO carmen.lex_projection( projection_epsg, projection_name, projection_provider ) VALUES ( 3944, 'RGF93 / CC44' , 'EPSG' ); 
INSERT INTO carmen.lex_projection( projection_epsg, projection_name, projection_provider ) VALUES ( 3945, 'RGF93 / CC45' , 'EPSG' ); 
INSERT INTO carmen.lex_projection( projection_epsg, projection_name, projection_provider ) VALUES ( 3946, 'RGF93 / CC46' , 'EPSG' ); 
INSERT INTO carmen.lex_projection( projection_epsg, projection_name, projection_provider ) VALUES ( 3947, 'RGF93 / CC47' , 'EPSG' ); 
INSERT INTO carmen.lex_projection( projection_epsg, projection_name, projection_provider ) VALUES ( 3948, 'RGF93 / CC48' , 'EPSG' ); 
INSERT INTO carmen.lex_projection( projection_epsg, projection_name, projection_provider ) VALUES ( 3949, 'RGF93 / CC49' , 'EPSG' ); 
INSERT INTO carmen.lex_projection( projection_epsg, projection_name, projection_provider ) VALUES ( 3950, 'RGF93 / CC50' , 'EPSG' ); 
INSERT INTO carmen.lex_projection( projection_epsg, projection_name, projection_provider ) VALUES ( 32620, 'WGS84 / UTM 20 Nord' , 'EPSG' ); 
INSERT INTO carmen.lex_projection( projection_epsg, projection_name, projection_provider ) VALUES ( 32621, 'WGS84 / UTM zone 21 Nord' , 'EPSG' ); 
INSERT INTO carmen.lex_projection( projection_epsg, projection_name, projection_provider ) VALUES ( 2972, 'RGFG95 / UTM 22 Nord' , 'EPSG' ); 
INSERT INTO carmen.lex_projection( projection_epsg, projection_name, projection_provider ) VALUES ( 2975, 'RGR92 / UTM 40 sud' , 'EPSG' ); 
INSERT INTO carmen.lex_projection( projection_epsg, projection_name, projection_provider ) VALUES ( 4326, 'WGS84' , 'EPSG' ); 
INSERT INTO carmen.lex_projection( projection_epsg, projection_name, projection_provider ) VALUES ( 27571, 'NTF(Paris) / Lambert zone 1' , 'EPSG' ); 
INSERT INTO carmen.lex_projection( projection_epsg, projection_name, projection_provider ) VALUES ( 27572, 'NTF(Paris) / Lambert zone 2' , 'EPSG' ); 
INSERT INTO carmen.lex_projection( projection_epsg, projection_name, projection_provider ) VALUES ( 27573, 'NTF(Paris) / Lambert zone 3' , 'EPSG' ); 
INSERT INTO carmen.lex_projection( projection_epsg, projection_name, projection_provider ) VALUES ( 27574, 'NTF(Paris) / Lambert zone 4' , 'EPSG' ); 
INSERT INTO carmen.lex_projection( projection_epsg, projection_name, projection_provider ) VALUES ( 27561, 'NTF(Paris) / Lambert Nord France' , 'EPSG' ); 
INSERT INTO carmen.lex_projection( projection_epsg, projection_name, projection_provider ) VALUES ( 27562, 'NTF(Paris) / Lambert centre France' , 'EPSG' ); 
INSERT INTO carmen.lex_projection( projection_epsg, projection_name, projection_provider ) VALUES ( 27563, 'NTF(Paris) / Lambert Sud France' , 'EPSG' ); 
INSERT INTO carmen.lex_projection( projection_epsg, projection_name, projection_provider ) VALUES ( 27564, 'NTF(Paris) / Lambert Cors' , 'EPSG' ); 
INSERT INTO carmen.lex_projection( projection_epsg, projection_name, projection_provider ) VALUES ( 2971, 'CSG 67 / UTM 22 Nord' , 'EPSG' ); 
INSERT INTO carmen.lex_projection( projection_epsg, projection_name, projection_provider ) VALUES ( 3312, 'CSG 67 / UTM 21 Nord' , 'EPSG' ); 
INSERT INTO carmen.lex_projection( projection_epsg, projection_name, projection_provider ) VALUES ( 2973, 'Fort Desaix / UTM 20' , 'EPSG' ); 
INSERT INTO carmen.lex_projection( projection_epsg, projection_name, projection_provider ) VALUES ( 4625, 'Fort Desaix' , 'EPSG' ); 
INSERT INTO carmen.lex_projection( projection_epsg, projection_name, projection_provider ) VALUES ( 4559, 'RRAF 91 / UTM 20' , 'EPSG' ); 
INSERT INTO carmen.lex_projection( projection_epsg, projection_name, projection_provider ) VALUES ( 3727, 'Piton des neiges / Gauss Laborde Réunion' , 'EPSG' ); 
INSERT INTO carmen.lex_projection( projection_epsg, projection_name, projection_provider ) VALUES ( 2987, 'IGN1950 / UTM 21 Nord' , 'EPSG' ); 
INSERT INTO carmen.lex_projection( projection_epsg, projection_name, projection_provider ) VALUES ( 32738, 'WGS84 / UTM 38 Sud' , 'EPSG' ); 
INSERT INTO carmen.lex_projection( projection_epsg, projection_name, projection_provider ) VALUES ( 32301, 'WGS72 / UTM 1 Sud' , 'EPSG' ); 
INSERT INTO carmen.lex_projection( projection_epsg, projection_name, projection_provider ) VALUES ( 3296, 'RGPF / UTM 5 Sud' , 'EPSG' ); 
INSERT INTO carmen.lex_projection( projection_epsg, projection_name, projection_provider ) VALUES ( 3297, 'RGPF / UTM 6 Sud' , 'EPSG' ); 
INSERT INTO carmen.lex_projection( projection_epsg, projection_name, projection_provider ) VALUES ( 2980, 'Combani 1950 / UTM 38 sud' , 'EPSG' ); 
INSERT INTO carmen.lex_projection( projection_epsg, projection_name, projection_provider ) VALUES ( 3163, 'RGNC91-93 / Lambert New Caledoni' , 'EPSG' ); 
INSERT INTO carmen.lex_projection( projection_epsg, projection_name, projection_provider ) VALUES ( 3169, 'RGNC91-93 / UTM 57' , 'EPSG' ); 
INSERT INTO carmen.lex_projection( projection_epsg, projection_name, projection_provider ) VALUES ( 3170, 'RGNC91-93 / UTM 58' , 'EPSG' ); 
INSERT INTO carmen.lex_projection( projection_epsg, projection_name, projection_provider ) VALUES ( 3171, 'RGNC91-93 / UTM 59' , 'EPSG' ); 
INSERT INTO carmen.lex_projection( projection_epsg, projection_name, projection_provider ) VALUES ( 32630, 'WGS84 / UTM 30 Nord' , 'EPSG' ); 
INSERT INTO carmen.lex_projection( projection_epsg, projection_name, projection_provider ) VALUES ( 32631, 'WGS84 / UTM 31 Nord' , 'EPSG' ); 
INSERT INTO carmen.lex_projection( projection_epsg, projection_name, projection_provider ) VALUES ( 32632, 'WGS84 / UTM 32 Nord' , 'EPSG' ); 
INSERT INTO carmen.lex_projection( projection_epsg, projection_name, projection_provider ) VALUES ( 27581, 'NTF(Paris) / Lambert France 1' , 'EPSG' ); 
INSERT INTO carmen.lex_projection( projection_epsg, projection_name, projection_provider ) VALUES ( 27582, 'NTF(Paris) / Lambert France 2' , 'EPSG' ); 
INSERT INTO carmen.lex_projection( projection_epsg, projection_name, projection_provider ) VALUES ( 27583, 'NTF(Paris) / Lambert France 3' , 'EPSG' ); 
INSERT INTO carmen.lex_projection( projection_epsg, projection_name, projection_provider ) VALUES ( 27584, 'NTF(Paris) / Lambert France 4' , 'EPSG' ); 
INSERT INTO carmen.lex_projection( projection_epsg, projection_name, projection_provider ) VALUES ( 4471, 'RGM04 / UTM zone 38S' , 'EPSG' ); 
INSERT INTO carmen.lex_projection( projection_epsg, projection_name, projection_provider ) VALUES ( 3857, 'WGS 84 / Pseudo-Mercator' , 'EPSG' ); 
INSERT INTO carmen.lex_projection( projection_epsg, projection_name, projection_provider ) VALUES ( 4467, 'RGSPM06 / UTM 21' , 'EPSG' ); 


DELETE FROM carmen.lex_tool;
INSERT INTO carmen.lex_tool( tool_identifier, tool_default_enabled, tool_xml_nodeprefix, tool_xml_nodevalue ) VALUES ( 'pan', 'true', 'tool', 'attribut_value_pan' ); 
INSERT INTO carmen.lex_tool( tool_identifier, tool_default_enabled, tool_xml_nodeprefix, tool_xml_nodevalue ) VALUES ( 'zoom', 'true', 'tool', 'attribut_value_zoom' ); 
INSERT INTO carmen.lex_tool( tool_identifier, tool_default_enabled, tool_xml_nodeprefix, tool_xml_nodevalue ) VALUES ( 'fitall', 'true', 'tool', 'attribut_value_fitall' ); 
INSERT INTO carmen.lex_tool( tool_identifier, tool_default_enabled, tool_xml_nodeprefix, tool_xml_nodevalue ) VALUES ( 'measure', 'false', 'tool', 'attribut_value_measure' ); 
INSERT INTO carmen.lex_tool( tool_identifier, tool_default_enabled, tool_xml_nodeprefix, tool_xml_nodevalue ) VALUES ( 'symbology', 'false', 'tool', null ); 
INSERT INTO carmen.lex_tool( tool_identifier, tool_default_enabled, tool_xml_nodeprefix, tool_xml_nodevalue ) VALUES ( 'context', 'false', 'tool', 'attribut_value_context' ); 
INSERT INTO carmen.lex_tool( tool_identifier, tool_default_enabled, tool_xml_nodeprefix, tool_xml_nodevalue ) VALUES ( 'geobookmark', 'false', 'tool', null ); 
INSERT INTO carmen.lex_tool( tool_identifier, tool_default_enabled, tool_xml_nodeprefix, tool_xml_nodevalue ) VALUES ( 'addLayer', 'false', 'tool', null ); 
--INSERT INTO carmen.lex_tool( tool_identifier, tool_default_enabled, tool_xml_nodeprefix, tool_xml_nodevalue ) VALUES ( 'download', 'false', 'tool', null ); 
INSERT INTO carmen.lex_tool( tool_identifier, tool_default_enabled, tool_xml_nodeprefix, tool_xml_nodevalue ) VALUES ( 'exportImg', 'false', 'tool', 'attribut_value_exportimg' ); 
INSERT INTO carmen.lex_tool( tool_identifier, tool_default_enabled, tool_xml_nodeprefix, tool_xml_nodevalue ) VALUES ( 'exportPdf', 'false', 'tool', 'attribut_value_exportpdf' ); 
INSERT INTO carmen.lex_tool( tool_identifier, tool_default_enabled, tool_xml_nodeprefix, tool_xml_nodevalue ) VALUES ( 'print', 'false', 'tool', 'attribut_value_print' ); 
--INSERT INTO carmen.lex_tool( tool_identifier, tool_default_enabled, tool_xml_nodeprefix, tool_xml_nodevalue ) VALUES ( 'toolTips', 'false', 'tool',  ); 
INSERT INTO carmen.lex_tool( tool_identifier, tool_default_enabled, tool_xml_nodeprefix, tool_xml_nodevalue ) VALUES ( 'info', 'false', 'tool', 'attribut_value_info' ); 
INSERT INTO carmen.lex_tool( tool_identifier, tool_default_enabled, tool_xml_nodeprefix, tool_xml_nodevalue ) VALUES ( 'buffer', 'false', 'tool', 'attribut_value_buffer' ); 
INSERT INTO carmen.lex_tool( tool_identifier, tool_default_enabled, tool_xml_nodeprefix, tool_xml_nodevalue ) VALUES ( 'queryAttributes', 'false', 'tool', 'attribut_value_queryAttributes' ); 
INSERT INTO carmen.lex_tool( tool_identifier, tool_default_enabled, tool_xml_nodeprefix, tool_xml_nodevalue ) VALUES ( 'scaleImg', 'false', 'mapSettings', null ); 
INSERT INTO carmen.lex_tool( tool_identifier, tool_default_enabled, tool_xml_nodeprefix, tool_xml_nodevalue ) VALUES ( 'scaleTxt', 'false', 'mapSettings', null ); 
INSERT INTO carmen.lex_tool( tool_identifier, tool_default_enabled, tool_xml_nodeprefix, tool_xml_nodevalue ) VALUES ( 'scale', 'false', 'tool', null ); 
--INSERT INTO carmen.lex_tool( tool_identifier, tool_default_enabled, tool_xml_nodeprefix, tool_xml_nodevalue ) VALUES ( 'point_annotation', 'false', 'tool', null ); 
--INSERT INTO carmen.lex_tool( tool_identifier, tool_default_enabled, tool_xml_nodeprefix, tool_xml_nodevalue ) VALUES ( 'line_annotation', 'false', 'tool', null ); 
--INSERT INTO carmen.lex_tool( tool_identifier, tool_default_enabled, tool_xml_nodeprefix, tool_xml_nodevalue ) VALUES ( 'polygon_annotation', 'false', 'tool', null ); 
-- INSERT INTO carmen.lex_tool( tool_identifier, tool_default_enabled, tool_xml_nodeprefix, tool_xml_nodevalue ) VALUES ( 'text_annotation', 'false', 'tool', null ); 
-- INSERT INTO carmen.lex_tool( tool_identifier, tool_default_enabled, tool_xml_nodeprefix, tool_xml_nodevalue ) VALUES ( 'snapping', 'false', 'tool', null ); 
-- INSERT INTO carmen.lex_tool( tool_identifier, tool_default_enabled, tool_xml_nodeprefix, tool_xml_nodevalue ) VALUES ( 'move_annotation', 'false', 'tool', null ); 
-- INSERT INTO carmen.lex_tool( tool_identifier, tool_default_enabled, tool_xml_nodeprefix, tool_xml_nodevalue ) VALUES ( 'modify_annotation', 'false', 'tool', null ); 
-- INSERT INTO carmen.lex_tool( tool_identifier, tool_default_enabled, tool_xml_nodeprefix, tool_xml_nodevalue ) VALUES ( 'styles_annotation', 'false', 'tool', null ); 
-- INSERT INTO carmen.lex_tool( tool_identifier, tool_default_enabled, tool_xml_nodeprefix, tool_xml_nodevalue ) VALUES ( 'export_annotation', 'false', 'tool', null ); 
-- INSERT INTO carmen.lex_tool( tool_identifier, tool_default_enabled, tool_xml_nodeprefix, tool_xml_nodevalue ) VALUES ( 'buffer_from_selection_annotation', 'false', 'tool', null ); 
INSERT INTO carmen.lex_tool( tool_identifier, tool_default_enabled, tool_xml_nodeprefix, tool_xml_nodevalue ) VALUES ( 'annotation', 'false', 'tool', null ); 
-- INSERT INTO carmen.lex_tool( tool_identifier, tool_default_enabled, tool_xml_nodeprefix, tool_xml_nodevalue ) VALUES ( 'buffer_all_annotation', 'false', 'tool', 'buffer_all_value_annotation' ); 
-- INSERT INTO carmen.lex_tool( tool_identifier, tool_default_enabled, tool_xml_nodeprefix, tool_xml_nodevalue ) VALUES ( 'attribut_annotation', 'false', 'tool', 'attribut_value_annotation' ); 


DELETE FROM carmen.lex_units;
INSERT INTO carmen.lex_units( unit_code, unit_name ) VALUES ( 'METERS', 'Mètres' ); 
INSERT INTO carmen.lex_units( unit_code, unit_name ) VALUES ( 'KILOMETERS', 'Kilomètres' ); 
INSERT INTO carmen.lex_units( unit_code, unit_name ) VALUES ( 'FEET', 'Pieds' ); 
INSERT INTO carmen.lex_units( unit_code, unit_name ) VALUES ( 'INCHES', 'Pouces' ); 
INSERT INTO carmen.lex_units( unit_code, unit_name ) VALUES ( 'MILES', 'Miles' ); 
INSERT INTO carmen.lex_units( unit_code, unit_name ) VALUES ( 'DD', 'DD' ); 


DELETE FROM carmen.lex_wxs_type;
INSERT INTO carmen.lex_wxs_type( wxs_type_name ) VALUES ( 'WMS' ); 
INSERT INTO carmen.lex_wxs_type( wxs_type_name ) VALUES ( 'WFS' );
INSERT INTO carmen.lex_wxs_type( wxs_type_name ) VALUES ( 'WMTS' );
INSERT INTO carmen.lex_wxs_type( wxs_type_name ) VALUES ( 'WMSC' );


