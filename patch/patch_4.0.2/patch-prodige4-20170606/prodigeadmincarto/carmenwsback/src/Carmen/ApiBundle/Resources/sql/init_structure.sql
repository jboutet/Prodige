/**
 * @date 25/03/2016
 * @author alkante
 * @version 3.05
 */
DROP SCHEMA IF EXISTS carmen CASCADE;
CREATE SCHEMA carmen;

CREATE TABLE carmen.lex_analyse_type (
  analyse_type_id      serial NOT NULL,
  analyse_type_code    varchar(20),
  analyse_type_name    varchar(255),
  CONSTRAINT pk_analyse_type PRIMARY KEY (analyse_type_id)
);

COMMENT ON TABLE carmen.lex_analyse_type IS 'Analyse Types';

COMMENT ON COLUMN carmen.lex_analyse_type.analyse_type_id IS 'primary key';

COMMENT ON COLUMN carmen.lex_analyse_type.analyse_type_code IS 'Analyse type code';

COMMENT ON COLUMN carmen.lex_analyse_type.analyse_type_name IS 'Analyse type name';

CREATE TABLE carmen.lex_category_keyword (
  category_id          serial NOT NULL,
  category_name        varchar(255),
  CONSTRAINT pk_category PRIMARY KEY (category_id)
);

COMMENT ON TABLE carmen.lex_category_keyword IS 'List of keyword categories. Predefined category : (0, `Mots clés libres`)';

COMMENT ON COLUMN carmen.lex_category_keyword.category_id IS 'Category ID';

COMMENT ON COLUMN carmen.lex_category_keyword.category_name IS 'Category name';

CREATE TABLE carmen.lex_color_id (
  color_id             serial NOT NULL,
  color_key            varchar(100),
  color_name           varchar(100),
  CONSTRAINT pk_lex_color_id PRIMARY KEY (color_id)
);

COMMENT ON TABLE carmen.lex_color_id IS 'Glossary of colors';

COMMENT ON COLUMN carmen.lex_color_id.color_id IS 'primary key';

COMMENT ON COLUMN carmen.lex_color_id.color_key IS 'Color key';

COMMENT ON COLUMN carmen.lex_color_id.color_name IS 'Color name';

CREATE TABLE carmen.lex_dns (
  dns_id               serial NOT NULL,
  dns_prefix_backoffice varchar(150),
  dns_prefix_data      varchar(150),
  dns_prefix_frontoffice varchar(150),
  dns_prefix_metadata  varchar(150),
  dns_scheme           varchar(10) DEFAULT 'http'::character varying,
  dns_url              varchar(255),
  CONSTRAINT pk_dns PRIMARY KEY (dns_id)
);

COMMENT ON TABLE carmen.lex_dns IS 'DNS types';

COMMENT ON COLUMN carmen.lex_dns.dns_id IS 'primary key';

COMMENT ON COLUMN carmen.lex_dns.dns_url IS 'DNS URL';

CREATE TABLE carmen.lex_field_datatype (
  field_datatype_id    serial NOT NULL,
  field_datatype_code  varchar(20),
  field_datatype_name  varchar(255),
  CONSTRAINT pk_field_datatype PRIMARY KEY (field_datatype_id)
);

COMMENT ON TABLE carmen.lex_field_datatype IS 'Field data Types';

COMMENT ON COLUMN carmen.lex_field_datatype.field_datatype_id IS 'primary key';

COMMENT ON COLUMN carmen.lex_field_datatype.field_datatype_code IS 'Field data type code';

COMMENT ON COLUMN carmen.lex_field_datatype.field_datatype_name IS 'Field data type name';

CREATE TABLE carmen.lex_field_type (
  field_type_id        serial NOT NULL,
  field_type_name      varchar(255),
  CONSTRAINT pk_field_type PRIMARY KEY (field_type_id)
);

COMMENT ON TABLE carmen.lex_field_type IS 'Field types';

COMMENT ON COLUMN carmen.lex_field_type.field_type_id IS 'primary key';

COMMENT ON COLUMN carmen.lex_field_type.field_type_name IS 'Field type name';

CREATE TABLE carmen.lex_inspire_theme (
  theme_id             serial NOT NULL,
  theme_name           varchar(255),
  CONSTRAINT pk_lex_inspire_theme PRIMARY KEY (theme_id)
);

COMMENT ON TABLE carmen.lex_inspire_theme IS 'List of Inspire themes';

COMMENT ON COLUMN carmen.lex_inspire_theme.theme_id IS 'primary key';

COMMENT ON COLUMN carmen.lex_inspire_theme.theme_name IS 'Name';

CREATE TABLE carmen.lex_iso_condition (
  condition_id         serial NOT NULL,
  condition_name       varchar(255),
  CONSTRAINT pk_lex_iso_constraints_0 PRIMARY KEY (condition_id)
);

COMMENT ON TABLE carmen.lex_iso_condition IS 'List of available conditions';

COMMENT ON COLUMN carmen.lex_iso_condition.condition_id IS 'primary key';

COMMENT ON COLUMN carmen.lex_iso_condition.condition_name IS 'Name';

CREATE TABLE carmen.lex_iso_constraint (
  constraint_id        serial NOT NULL,
  constraint_name      varchar(255),
  CONSTRAINT pk_lex_iso_constraints PRIMARY KEY (constraint_id)
);

COMMENT ON TABLE carmen.lex_iso_constraint IS 'Glossary of available constraints';

COMMENT ON COLUMN carmen.lex_iso_constraint.constraint_id IS 'primary key';

COMMENT ON COLUMN carmen.lex_iso_constraint.constraint_name IS 'Name';

CREATE TABLE carmen.lex_iso_language (
  language_code        varchar(3) NOT NULL,
  language_name        varchar(255),
  CONSTRAINT pk_lex_iso_languages PRIMARY KEY (language_code)
);

COMMENT ON TABLE carmen.lex_iso_language IS 'Glossary of available languages for OGC services';

COMMENT ON COLUMN carmen.lex_iso_language.language_code IS 'primary key';

COMMENT ON COLUMN carmen.lex_iso_language.language_name IS 'language name';

CREATE TABLE carmen.lex_iso_theme (
  theme_id             serial NOT NULL,
  theme_code           varchar(255),
  theme_name           varchar(255),
  CONSTRAINT pk_lex_iso_theme PRIMARY KEY (theme_id)
);

COMMENT ON TABLE carmen.lex_iso_theme IS 'List of ISO themes';

COMMENT ON COLUMN carmen.lex_iso_theme.theme_id IS 'primary key';

COMMENT ON COLUMN carmen.lex_iso_theme.theme_code IS 'code (name anglais) associé';

COMMENT ON COLUMN carmen.lex_iso_theme.theme_name IS 'Name';

CREATE TABLE carmen.lex_layer_type (
  layer_type_code      varchar(20) NOT NULL,
  layer_type_name      varchar(20),
  CONSTRAINT pk_lex_layer_type PRIMARY KEY (layer_type_code)
);

COMMENT ON TABLE carmen.lex_layer_type IS 'layers type';

COMMENT ON COLUMN carmen.lex_layer_type.layer_type_code IS 'primary key';

COMMENT ON COLUMN carmen.lex_layer_type.layer_type_name IS 'layer type name';

CREATE TABLE carmen.lex_map_audit_status (
  map_audit_status_id  serial NOT NULL,
  map_audit_status_name text NOT NULL,
  CONSTRAINT pk_lex_map_audit_status PRIMARY KEY (map_audit_status_id)
);

COMMENT ON TABLE carmen.lex_map_audit_status IS 'Glossary of map transactions (create, update...)';

COMMENT ON COLUMN carmen.lex_map_audit_status.map_audit_status_id IS 'primary key';

CREATE TABLE carmen.lex_projection (
  projection_epsg      numeric NOT NULL,
  projection_name      varchar(255),
  projection_provider  varchar(255) default 'EPSG',
  CONSTRAINT pk_lex_projections PRIMARY KEY (projection_epsg)
);

COMMENT ON TABLE carmen.lex_projection IS 'Glossary of authorized projections';

COMMENT ON COLUMN carmen.lex_projection.projection_epsg IS 'EPSG code of projection';

COMMENT ON COLUMN carmen.lex_projection.projection_name IS 'Projection name';

CREATE TABLE carmen.lex_tool (
  tool_id              serial NOT NULL,
  tool_default_enabled bool DEFAULT false NOT NULL,
  tool_identifier      varchar(255),
  tool_xml_nodeprefix  varchar(50) DEFAULT 'tool'::character varying NOT NULL,
  tool_xml_nodevalue   varchar(50),
  CONSTRAINT pk_tool PRIMARY KEY (tool_id)
);

COMMENT ON TABLE carmen.lex_tool IS 'Glossary of tools';

COMMENT ON COLUMN carmen.lex_tool.tool_id IS 'primary key';

COMMENT ON COLUMN carmen.lex_tool.tool_default_enabled IS 'Set true if this tool is activate by default for a new map';

COMMENT ON COLUMN carmen.lex_tool.tool_identifier IS 'Tool ID in context';

COMMENT ON COLUMN carmen.lex_tool.tool_xml_nodeprefix IS 'Give the OWS Context / Json node prefix for this tool. Default is `tool` but may be `mapSettings`';

COMMENT ON COLUMN carmen.lex_tool.tool_xml_nodevalue IS 'Give the OWS Context / Json node name for specify the tool value.';

CREATE TABLE carmen.lex_units (
  unit_code            varchar(20) NOT NULL,
  unit_name            varchar(100),
  CONSTRAINT pk_lex_units PRIMARY KEY (unit_code)
);

COMMENT ON TABLE carmen.lex_units IS 'Glossary of measurement units';

COMMENT ON COLUMN carmen.lex_units.unit_code IS 'Mapserver code of UNIT';

COMMENT ON COLUMN carmen.lex_units.unit_name IS 'Label for the unit';

CREATE TABLE carmen.lex_wxs_type (
  wxs_type_id          serial NOT NULL,
  wxs_type_name        varchar(100),
  CONSTRAINT pk_wxs_type PRIMARY KEY (wxs_type_id)
);

COMMENT ON TABLE carmen.lex_wxs_type IS 'Services type';

COMMENT ON COLUMN carmen.lex_wxs_type.wxs_type_id IS 'primary key';

COMMENT ON COLUMN carmen.lex_wxs_type.wxs_type_name IS 'Service name';

CREATE TABLE carmen.account (
  account_id           serial NOT NULL,
  account_dbpostgres   varchar(64),
  account_dns_id       int4,
  account_geonetworkurl varchar(500),
  account_hostpostgres varchar(255),
  account_name         varchar(255),
  account_passwordpostgres varchar(255),
  account_path         varchar(255),
  account_portpostgres int4 DEFAULT 5432 NOT NULL,
  account_userpostgres varchar(255),
  CONSTRAINT pk_accounts PRIMARY KEY (account_id)
);

CREATE INDEX idx_account ON carmen.account (account_dns_id);

COMMENT ON TABLE carmen.account IS 'List of all CARMEN accounts';

COMMENT ON COLUMN carmen.account.account_id IS 'primary key';

COMMENT ON COLUMN carmen.account.account_dbpostgres IS 'Database name PostgresQL';

COMMENT ON COLUMN carmen.account.account_dns_id IS 'account DNS name';

COMMENT ON COLUMN carmen.account.account_geonetworkurl IS 'URL of Geonetwork/Geosource application for query the account metadata';

COMMENT ON COLUMN carmen.account.account_hostpostgres IS 'Host PostgresSQL';

COMMENT ON COLUMN carmen.account.account_name IS 'account name';

COMMENT ON COLUMN carmen.account.account_passwordpostgres IS 'Password Read-only Account PostgreSQL';

COMMENT ON COLUMN carmen.account.account_path IS 'account path directory';

COMMENT ON COLUMN carmen.account.account_portpostgres IS 'Port PostgresSQL';

COMMENT ON COLUMN carmen.account.account_userpostgres IS 'Identifier Read-only Account PostgreSQL';

CREATE TABLE carmen.keyword (
  keyword_id           serial NOT NULL,
  category_id          int4,
  keyword_name         varchar(255),
  CONSTRAINT pk_keywords PRIMARY KEY (keyword_id)
);

CREATE INDEX idx_keywords ON carmen.keyword (category_id);

COMMENT ON TABLE carmen.keyword IS 'List of keywords';

COMMENT ON COLUMN carmen.keyword.keyword_id IS 'primary key';

COMMENT ON COLUMN carmen.keyword.category_id IS 'category ID';

COMMENT ON COLUMN carmen.keyword.keyword_name IS 'Keyword';

CREATE TABLE carmen.map_audit (
  map_audit_id         serial NOT NULL,
  account_id           int4 NOT NULL,
  map_audit_date       timestamp NOT NULL,
  map_audit_status     varchar(50) NOT NULL,
  map_file             varchar(255),
  user_email           text NOT NULL,
  CONSTRAINT pk_map_audit PRIMARY KEY (map_audit_id)
);

CREATE INDEX idx_map_audit ON carmen.map_audit (account_id);

CREATE INDEX idx_map_audit_0 ON carmen.map_audit (map_file);

CREATE INDEX idx_map_audit_1 ON carmen.map_audit (map_audit_status);

CREATE INDEX idx_map_audit_2 ON carmen.map_audit (user_email);

COMMENT ON TABLE carmen.map_audit IS 'Statistics on map transactions';

COMMENT ON COLUMN carmen.map_audit.map_audit_id IS 'primary key';

COMMENT ON COLUMN carmen.map_audit.account_id IS 'Account ID';

COMMENT ON COLUMN carmen.map_audit.map_audit_date IS 'Map transaction date';

COMMENT ON COLUMN carmen.map_audit.map_audit_status IS 'Map transaction';

COMMENT ON COLUMN carmen.map_audit.map_file IS 'Map file';

COMMENT ON COLUMN carmen.map_audit.user_email IS 'User ID for transaction';





CREATE TABLE carmen.users (
  user_id              serial NOT NULL,
  account_id           int4,
  user_email           text,
  CONSTRAINT pk_user PRIMARY KEY (user_id),
  CONSTRAINT uk_user_email UNIQUE (user_email)
);

CREATE INDEX idx_user ON carmen.users (account_id);

COMMENT ON TABLE carmen.users IS 'List of users by account';

COMMENT ON COLUMN carmen.users.user_id IS 'primary key';

COMMENT ON COLUMN carmen.users.account_id IS 'Account ID';

COMMENT ON COLUMN carmen.users.user_email IS 'User email (sent by authentication service)';


CREATE TABLE carmen.wxs (
  wxs_id               serial NOT NULL,
  account_id           int4,
  wxs_name             varchar(255),
  wxs_rank             int4,
  wxs_type_id          int4,
  wxs_url              varchar(1024),
  CONSTRAINT pk_wxs PRIMARY KEY (wxs_id)
);

CREATE INDEX idx_wxs ON carmen.wxs (account_id);

CREATE INDEX idx_wxs_0 ON carmen.wxs (wxs_type_id);

COMMENT ON TABLE carmen.wxs IS 'List of WXS services for the account';

COMMENT ON COLUMN carmen.wxs.wxs_id IS 'primary key';

COMMENT ON COLUMN carmen.wxs.account_id IS 'Account ID';

COMMENT ON COLUMN carmen.wxs.wxs_name IS 'Service name';

COMMENT ON COLUMN carmen.wxs.wxs_rank IS 'Service rank';

COMMENT ON COLUMN carmen.wxs.wxs_type_id IS 'Service type';

COMMENT ON COLUMN carmen.wxs.wxs_url IS 'Service url';

CREATE TABLE carmen.contact (
  contact_id           serial NOT NULL,
  contact_address      varchar(1024),
  contact_city         varchar(255),
  contact_country      varchar(255),
  contact_email        varchar(255),
  contact_fax          varchar(255),
  contact_name         varchar(255),
  contact_organization varchar(255),
  contact_phone        varchar(255),
  contact_position     varchar(255),
  contact_unit         varchar(255),
  contact_zipcode      varchar(10),
  user_id              int4,
  CONSTRAINT pk_contact PRIMARY KEY (contact_id)
);

CREATE INDEX idx_contact ON carmen.contact (user_id);

COMMENT ON TABLE carmen.contact IS 'Contact information associated with the account';

COMMENT ON COLUMN carmen.contact.contact_id IS 'primary key';

COMMENT ON COLUMN carmen.contact.user_id IS 'user identifier';

CREATE TABLE carmen.preferences (
  preference_id        serial NOT NULL,
  preference_background_color varchar(7),
  preference_background_transparency bool,
  preference_maxscale  int4,
  preference_minscale  int4,
  preference_outputformat varchar(10),
  preference_srs       int4,
  preference_units     varchar(20),
  preferences_extent_xmax float4,
  preferences_extent_xmin float4,
  preferences_extent_ymax float4,
  preferences_extent_ymin float4,
  user_id              int4,
  CONSTRAINT pk_preferences PRIMARY KEY (preference_id),
  CONSTRAINT idx_preferences UNIQUE (user_id)
);

CREATE INDEX idx_preferences_0 ON carmen.preferences (preference_units);

COMMENT ON TABLE carmen.preferences IS 'Storage of map preferences by user';

COMMENT ON COLUMN carmen.preferences.preference_id IS 'primary key';

COMMENT ON COLUMN carmen.preferences.preference_background_color IS 'Background color (hexa)';

COMMENT ON COLUMN carmen.preferences.preference_background_transparency IS 'Background transparency';

COMMENT ON COLUMN carmen.preferences.preference_maxscale IS 'maximal scale';

COMMENT ON COLUMN carmen.preferences.preference_minscale IS 'minimal scale';

COMMENT ON COLUMN carmen.preferences.preference_outputformat IS 'output format';

COMMENT ON COLUMN carmen.preferences.preference_srs IS 'projection EPSG code';

COMMENT ON COLUMN carmen.preferences.preference_units IS 'Measure units';

COMMENT ON COLUMN carmen.preferences.preferences_extent_xmax IS 'Map extent - XMAX';

COMMENT ON COLUMN carmen.preferences.preferences_extent_xmin IS 'Map extent - XMIN';

COMMENT ON COLUMN carmen.preferences.preferences_extent_ymax IS 'Map extent - YMAX';

COMMENT ON COLUMN carmen.preferences.preferences_extent_ymin IS 'Map extent - YMIN';

COMMENT ON COLUMN carmen.preferences.user_id IS 'account identifier';

CREATE TABLE carmen.user_audit (
  user_id              int4 NOT NULL,
  user_creation_date   date NOT NULL,
  user_last_connection_date date,
  CONSTRAINT pk_user_audit PRIMARY KEY (user_id)
);

COMMENT ON TABLE carmen.user_audit IS 'Statistics on users (create date, connection date, ...)';

COMMENT ON COLUMN carmen.user_audit.user_creation_date IS 'User DB creation date';

COMMENT ON COLUMN carmen.user_audit.user_last_connection_date IS 'User last connection date';

CREATE TABLE carmen.favorite_area (
  area_id              serial NOT NULL,
  area_name            varchar(255),
  area_xmax            numeric,
  area_xmin            numeric,
  area_ymax            numeric,
  area_ymin            numeric,
  map_id               int4,
  CONSTRAINT pk_favorite_areas PRIMARY KEY (area_id)
);

CREATE INDEX idx_favorite_areas ON carmen.favorite_area (map_id);

COMMENT ON TABLE carmen.favorite_area IS 'List of favorites area by map';

COMMENT ON COLUMN carmen.favorite_area.area_id IS 'Area ID';

COMMENT ON COLUMN carmen.favorite_area.area_name IS 'name de la area';

COMMENT ON COLUMN carmen.favorite_area.area_xmax IS 'XMAX coordinate for the area - Projection WGS84';

COMMENT ON COLUMN carmen.favorite_area.area_xmin IS 'XMIN coordinate for the area - Projection WGS84';

COMMENT ON COLUMN carmen.favorite_area.area_ymax IS 'YMAX coordinate for the area - Projection WGS84';

COMMENT ON COLUMN carmen.favorite_area.area_ymin IS 'YMIN coordinate for the area - Projection WGS84';

COMMENT ON COLUMN carmen.favorite_area.map_id IS 'Map ID';

CREATE TABLE carmen.field (
  field_id             serial NOT NULL,
  field_alias          varchar(255),
  field_datatype_id    int4,
  field_duplicate      bool,
  field_headers        bool,
  field_name           varchar(255),
  field_ogc            bool,
  field_queries        bool,
  field_rank           int4,
  field_tooltips       bool,
  field_type_id        int4,
  field_url            text,
  layer_id             int4,
  CONSTRAINT pk_fields PRIMARY KEY (field_id)
);

CREATE INDEX idx_fields ON carmen.field (layer_id);

CREATE INDEX idx_fields_0 ON carmen.field (field_type_id);

COMMENT ON TABLE carmen.field IS 'List of fields for a layer';

COMMENT ON COLUMN carmen.field.field_id IS 'primary key';

COMMENT ON COLUMN carmen.field.field_alias IS 'Field alias';

COMMENT ON COLUMN carmen.field.field_duplicate IS 'Field is a duplicate of an original field ?';

COMMENT ON COLUMN carmen.field.field_headers IS 'Field enabled in headers ?';

COMMENT ON COLUMN carmen.field.field_name IS 'Field name';

COMMENT ON COLUMN carmen.field.field_ogc IS 'Field enabled in OGC ?';

COMMENT ON COLUMN carmen.field.field_queries IS 'Field enabled in queries ?';

COMMENT ON COLUMN carmen.field.field_rank IS 'Field rank in layer';

COMMENT ON COLUMN carmen.field.field_tooltips IS 'Field enabled in tooltips ?';

COMMENT ON COLUMN carmen.field.field_type_id IS 'Field type ID';

COMMENT ON COLUMN carmen.field.field_url IS 'Field URL prefix';

COMMENT ON COLUMN carmen.field.layer_id IS 'Layer identifier';

CREATE TABLE carmen.layer (
  layer_id             serial NOT NULL,
  map_id               int4,
  layer_title          varchar(255),
  layer_name           varchar(255),
  layer_type_code      varchar(20) NOT NULL,
  layer_analyse_type_id int4,
  layer_identifier     varchar(255),
  layer_msname         varchar(255),
  layer_minscale       int8,
  layer_maxscale       int8,
  layer_projection_epsg numeric,
  layer_opacity        int4,
  layer_outputformat   varchar(100),
  layer_data_encoding  varchar(100),
  layer_haslabel       bool,
  layer_legend         bool,
  layer_legend_scale   bool,
  layer_field_url      text,
  layer_metadata_file  varchar(255),
  layer_metadata_uuid  varchar(255),
  layer_server_url     text,
  layer_server_version varchar(100),
  layer_tiled_layer  text,
  layer_tiled_server_url  text,
  layer_tiled_resolutions text,
  layer_wmsc_boundingbox text,
  layer_wmts_matrixids  text,
  layer_wmts_tileorigin text,
  layer_wmts_tileset    text,
  layer_wmts_style      text,
  layer_wxsname        varchar(255),
  layer_wms_query      bool,
  layer_atom           bool,
  layer_visible        bool,
  layer_wfs            bool,
  layer_wms            bool,
  layer_downloadable   bool,
    
  CONSTRAINT pk_layer_0 PRIMARY KEY (layer_id)
);

CREATE INDEX idx_layer ON carmen.layer (map_id);

CREATE INDEX idx_layer_0 ON carmen.layer (layer_analyse_type_id);

CREATE INDEX idx_layer_type_id ON carmen.layer (layer_type_code);

COMMENT ON TABLE carmen.layer IS 'List of map layers';

COMMENT ON COLUMN carmen.layer.layer_id IS 'primary key';

COMMENT ON COLUMN carmen.layer.layer_analyse_type_id IS 'Layer analyse type';

COMMENT ON COLUMN carmen.layer.layer_atom IS 'Layer can be Atom ?';

COMMENT ON COLUMN carmen.layer.layer_downloadable IS 'Layer downloadable ?';

COMMENT ON COLUMN carmen.layer.layer_field_url IS 'URL prefix for all fields in layer';

COMMENT ON COLUMN carmen.layer.layer_haslabel IS 'True if the layer shows its labels';

COMMENT ON COLUMN carmen.layer.layer_legend IS 'Visibility of the layer in legend';

COMMENT ON COLUMN carmen.layer.layer_legend_scale IS 'Scale for visibility of the layer in legend';

COMMENT ON COLUMN carmen.layer.layer_maxscale IS 'Layer max scale visibility';

COMMENT ON COLUMN carmen.layer.layer_metadata_file IS 'The filename of metadata store in the Carmen Metadata directory';

COMMENT ON COLUMN carmen.layer.layer_metadata_uuid IS 'UUID of the Geonetwork  metadata combinated with the account Geonetwork URL';

COMMENT ON COLUMN carmen.layer.layer_minscale IS 'Layer min scale visibility';

COMMENT ON COLUMN carmen.layer.layer_name IS 'Layer name (for WMS calls)';

COMMENT ON COLUMN carmen.layer.layer_opacity IS 'Layer opacity (0-100)';

COMMENT ON COLUMN carmen.layer.layer_outputformat IS 'Output format for this layer (ex : png, ...)';

COMMENT ON COLUMN carmen.layer.layer_projection_epsg IS 'Layer EPSG projection ID';

COMMENT ON COLUMN carmen.layer.layer_server_url IS 'URL of the server for W(x)S layers';

COMMENT ON COLUMN carmen.layer.layer_server_version IS 'Version of the server for W(x)S layers';

COMMENT ON COLUMN carmen.layer.layer_title IS 'Layer title';

COMMENT ON COLUMN carmen.layer.layer_type_code IS 'layer type code';

COMMENT ON COLUMN carmen.layer.layer_visible IS 'Layer visibility';

COMMENT ON COLUMN carmen.layer.layer_wfs IS 'WFS exportable ?';

COMMENT ON COLUMN carmen.layer.layer_wms IS 'WMS exportable ?';

COMMENT ON COLUMN carmen.layer.layer_wxsname IS 'The name of the layer in server WxS';

COMMENT ON COLUMN carmen.layer.layer_identifier IS 'The identifier of the layer for context URL';

COMMENT ON COLUMN carmen.layer.map_id IS 'Map ID';

COMMENT ON COLUMN carmen.layer.layer_data_encoding IS 'source data (vector data) encoding';

CREATE TABLE carmen.map (
  map_id               serial NOT NULL,
  account_id           int4 NOT NULL,
  map_atommetadata_uuid varchar(255),
  map_buffer_global    float4,
  map_catalogable      bool,
  map_datepublication  timestamp,
  map_extent_xmax      float8,
  map_extent_xmin      float8,
  map_extent_ymax      float8,
  map_extent_ymin      float8 NOT NULL,
  map_file             varchar(255),
  map_maxscale         float4,
  map_minscale         float4,
  map_model            bool,
  map_password         text,
  map_projection_epsg  numeric,
  map_bgcolor          varchar(20) default 'ffffff',
  map_refmap_extent_xmax float8,
  map_refmap_extent_xmin float8,
  map_refmap_extent_ymax float8,
  map_refmap_extent_ymin float8,
  map_refmap_image     varchar(255),
  map_summary          text,
  map_title            varchar(255),
  map_wfsmetadata_uuid varchar(255),
  map_wmsmetadata_uuid varchar(255),
  published_id         int4,
  user_id              int4,
  CONSTRAINT pk_map PRIMARY KEY (map_id)
);

CREATE INDEX idx_map ON carmen.map (account_id);

CREATE INDEX idx_map_0 ON carmen.map (map_projection_epsg);

CREATE INDEX idx_map_1 ON carmen.map (user_id);

COMMENT ON TABLE carmen.map IS 'List of Maps';

COMMENT ON COLUMN carmen.map.map_id IS 'primary key';

COMMENT ON COLUMN carmen.map.account_id IS 'account ID';

COMMENT ON COLUMN carmen.map.map_buffer_global IS 'value, in meters, of the global buffer';

COMMENT ON COLUMN carmen.map.map_catalogable IS 'Can be added to catalogue ?';

COMMENT ON COLUMN carmen.map.map_datepublication IS 'Date of publication for published maps or Creation date for draft maps';

COMMENT ON COLUMN carmen.map.map_extent_xmax IS 'xmax map extent, expressed in map projectionnn@init database preferences/preferences_extent_xmaxnn@used mapfile  MAP/EXTENT{sting3}n@used context  OWSContext/General/BoundingBox/UpperCorner{string1}n@used context  OWSContext/General/MaxBoundingBox/UpperCorner{string1}';

COMMENT ON COLUMN carmen.map.map_extent_xmin IS 'xmin map extent, expressed in map projectionnn@init database preferences/preferences_extent_xminnn@used mapfile  MAP/EXTENT{sting1}n@used context  OWSContext/General/BoundingBox/LowerCorner{string1}n@used context  OWSContext/General/MaxBoundingBox/LowerCorner{string1}';

COMMENT ON COLUMN carmen.map.map_extent_ymax IS 'ymax map extent, expressed in map projectionnn@init database preferences/preferences_extent_ymaxnn@used mapfile  MAP/EXTENT{sting4}n@used context  OWSContext/General/BoundingBox/UpperCorner{string2}n@used context  OWSContext/General/MaxBoundingBox/UpperCorner{string2}';

COMMENT ON COLUMN carmen.map.map_extent_ymin IS 'ymin map extent, expressed in map projectionnn@init database preferences/preferences_extent_yminnn@used mapfile  MAP/EXTENT{sting2}n@used context  OWSContext/General/BoundingBox/LowerCorner{string2}n@used context  OWSContext/General/MaxBoundingBox/LowerCorner{string2}';

COMMENT ON COLUMN carmen.map.map_file IS 'path to MapFile';

COMMENT ON COLUMN carmen.map.map_maxscale IS 'Max scale applied to the mapnn@init database preferences/preference_maxscalenn@used mapfile  MAP/WEB/MAXSCALEDENOM';

COMMENT ON COLUMN carmen.map.map_minscale IS 'Min scale applied to the mapnn@init database preferences/preference_minscalenn@used mapfile  MAP/WEB/MINSCALEDENOM';

COMMENT ON COLUMN carmen.map.map_model IS 'This map is a model ?';

COMMENT ON COLUMN carmen.map.map_password IS 'Map password';

COMMENT ON COLUMN carmen.map.map_projection_epsg IS 'Map EPSG projection ID';

COMMENT ON COLUMN carmen.map.map_summary IS 'Map summary';

COMMENT ON COLUMN carmen.map.map_title IS 'Map titlenn@used mapfile  MAP/NAMEn@used context  OWSContext[id]n@used context  OWSContext/General/Title';

COMMENT ON COLUMN carmen.map.published_id IS 'ID of the published map for this draft map (not null = it is a draft map, null = it is a published map)';

COMMENT ON COLUMN carmen.map.user_id IS 'Current user who edits this map';

CREATE TABLE carmen.map_annotation_attribute (
  attribute_id         serial NOT NULL,
  attribute_name       varchar(255),
  attribute_rank       int4,
  map_id               int4,
  CONSTRAINT pk_map_annotations_attributs PRIMARY KEY (attribute_id)
);

CREATE INDEX idx_map_annotations_attributs ON carmen.map_annotation_attribute (map_id);

COMMENT ON TABLE carmen.map_annotation_attribute IS 'List of annotation attributes for the map';

COMMENT ON COLUMN carmen.map_annotation_attribute.attribute_id IS 'primary key';

COMMENT ON COLUMN carmen.map_annotation_attribute.attribute_name IS 'Attribute name';

COMMENT ON COLUMN carmen.map_annotation_attribute.attribute_rank IS 'Attibute rank';

COMMENT ON COLUMN carmen.map_annotation_attribute.map_id IS 'Map ID';

CREATE TABLE carmen.map_group (
  group_id             serial NOT NULL,
  group_name           text NOT NULL,
  group_identifier     varchar(255),
  map_id               int4 NOT NULL,
  CONSTRAINT pk_group PRIMARY KEY (group_id)
);

CREATE INDEX idx_group ON carmen.map_group (map_id);

COMMENT ON TABLE carmen.map_group IS 'List of layer groups';

COMMENT ON COLUMN carmen.map_group.group_id IS 'group ID (auto incremented number shared with layer ID)';

COMMENT ON COLUMN carmen.map_group.group_name IS 'Group name';

COMMENT ON COLUMN carmen.map_group.group_identifier IS 'Group identifier for context URL';

COMMENT ON COLUMN carmen.map_group.map_id IS 'Map ID';

CREATE TABLE carmen.map_keyword (
  keyword_id           int4,
  map_id               int4
);

CREATE INDEX idx_map_keyword ON carmen.map_keyword (map_id, keyword_id);

CREATE INDEX idx_map_keywords ON carmen.map_keyword (map_id);

CREATE INDEX idx_map_keywords_0 ON carmen.map_keyword (keyword_id);

COMMENT ON TABLE carmen.map_keyword IS 'Keywords for map';

COMMENT ON COLUMN carmen.map_keyword.keyword_id IS 'Keyword ID';

COMMENT ON COLUMN carmen.map_keyword.map_id IS 'Map ID';

CREATE TABLE carmen.map_locator (
  locator_id           serial NOT NULL,
  locator_criteria_field_id varchar(255),
  locator_criteria_field_name varchar(255),
  locator_criteria_layer_id int4,
  locator_criteria_name varchar(255),
  locator_criteria_rank int4,
  locator_criteria_related int4,
  locator_criteria_related_field varchar(255),
  locator_criteria_visibility bool,
  map_id               int4,
  CONSTRAINT pk_map_locator PRIMARY KEY (locator_id)
);

CREATE INDEX idx_map_locator ON carmen.map_locator (locator_criteria_related);

CREATE INDEX idx_map_locator_0 ON carmen.map_locator (map_id);

COMMENT ON TABLE carmen.map_locator IS 'Search locator : criteria settings';

COMMENT ON COLUMN carmen.map_locator.locator_id IS 'primary key';

COMMENT ON COLUMN carmen.map_locator.locator_criteria_field_id IS 'Field ID for criteria';

COMMENT ON COLUMN carmen.map_locator.locator_criteria_field_name IS 'Field name for criteria';

COMMENT ON COLUMN carmen.map_locator.locator_criteria_layer_id IS 'Layer ID for criteria';

COMMENT ON COLUMN carmen.map_locator.locator_criteria_name IS 'Criteria name';

COMMENT ON COLUMN carmen.map_locator.locator_criteria_rank IS 'Criteria rank';

COMMENT ON COLUMN carmen.map_locator.locator_criteria_related IS 'Related criteria ID';

COMMENT ON COLUMN carmen.map_locator.locator_criteria_related_field IS 'Related criteria field';

COMMENT ON COLUMN carmen.map_locator.locator_criteria_visibility IS 'Criteria is visible ?';

COMMENT ON COLUMN carmen.map_locator.map_id IS 'Map ID';

CREATE TABLE carmen.map_tool (
  map_id               int4 NOT NULL,
  tool_id              int4 NOT NULL,
  maptool_value        text,
  CONSTRAINT pk_map_tool PRIMARY KEY (map_id, tool_id)
);

CREATE INDEX idx_map_tools ON carmen.map_tool (map_id);

CREATE INDEX idx_map_tools_0 ON carmen.map_tool (tool_id);

COMMENT ON TABLE carmen.map_tool IS 'List of available tools by map';

COMMENT ON COLUMN carmen.map_tool.map_id IS 'Map ID';

COMMENT ON COLUMN carmen.map_tool.tool_id IS 'Tool ID';

COMMENT ON COLUMN carmen.map_tool.maptool_value IS 'The value of tool if it is enabled';

CREATE TABLE carmen.map_tree (
  node_id              int4 NOT NULL,
  map_id               int4 NOT NULL,
  node_is_layer        bool NOT NULL,
  node_opened          bool DEFAULT true NOT NULL,
  node_parent          int4,
  node_pos             int4 DEFAULT 0 NOT NULL,
  CONSTRAINT pk_map_tree PRIMARY KEY (node_id)
);

ALTER TABLE carmen.map_tree ADD CONSTRAINT ck_map_tree_nodirectcycle CHECK (node_id <> node_parent);

CREATE INDEX idx_map_tree ON carmen.map_tree (map_id);

COMMENT ON TABLE carmen.map_tree IS 'Tree for order and classify layers and groups in map';

COMMENT ON COLUMN carmen.map_tree.node_id IS 'Node ID (group_id ou layer_id)';

COMMENT ON COLUMN carmen.map_tree.map_id IS 'Map ID';

COMMENT ON COLUMN carmen.map_tree.node_is_layer IS 'Node is a layer ? (false=is a group)';

COMMENT ON COLUMN carmen.map_tree.node_opened IS 'Node is opened ? (used in Groups nodes only)';

COMMENT ON COLUMN carmen.map_tree.node_parent IS 'Parent ID for current node. (root = null)';

COMMENT ON COLUMN carmen.map_tree.node_pos IS 'Node position (order) in current level (start with 0 for each level)';

CREATE TABLE carmen.map_tree_print (
  node_id              int4 NOT NULL,
  map_id               int4 NOT NULL,
  node_is_layer        bool NOT NULL,
  node_opened          bool DEFAULT true NOT NULL,
  node_parent          int4,
  node_pos             int4 DEFAULT 0 NOT NULL,
  CONSTRAINT pk_map_tree_print PRIMARY KEY (node_id)
);

ALTER TABLE carmen.map_tree_print ADD CONSTRAINT ck_map_tree_printnodirectcycle CHECK (node_id <> node_parent);

CREATE INDEX idx_map_tree_print ON carmen.map_tree_print (map_id);

COMMENT ON TABLE carmen.map_tree_print IS 'Print mode : Tree for order and classify layers and groups in map';

COMMENT ON COLUMN carmen.map_tree_print.node_id IS 'Print mode : Node ID (group_id ou layer_id)';

COMMENT ON COLUMN carmen.map_tree_print.map_id IS 'Print mode : Map ID';

COMMENT ON COLUMN carmen.map_tree_print.node_is_layer IS 'Print mode : Node is a layer ? (false=is a group)';

COMMENT ON COLUMN carmen.map_tree_print.node_opened IS 'Print mode : Node is opened ? (used in Groups nodes only)';

COMMENT ON COLUMN carmen.map_tree_print.node_parent IS 'Print mode : Parent ID for current node. (root = null)';

COMMENT ON COLUMN carmen.map_tree_print.node_pos IS 'Print mode : Node position (order) in current level (start with 0 for each level)';

CREATE TABLE carmen.map_ui (
  ui_id                serial NOT NULL,
  map_id               int4,
  ui_banner            bool,
  ui_banner_file       text,
  ui_color_id          int4,
  ui_copyright         bool,
  ui_copyright_background_color varchar(7),
  ui_copyright_font    varchar(255),
  ui_copyright_font_color varchar(7),
  ui_copyright_font_size int4,
  ui_copyright_text    text,
  ui_copyright_transparency int4 DEFAULT 0,
  ui_focus             bool,
  ui_keymap            bool,
  ui_legend            bool,
  ui_legend_print      bool DEFAULT true,
  ui_locate            bool,
  ui_logo              bool,
  ui_logopath          text,
  CONSTRAINT pk_map_ui PRIMARY KEY (ui_id)
);

CREATE INDEX idx_map_ui ON carmen.map_ui (map_id);

CREATE INDEX idx_map_ui_0 ON carmen.map_ui (ui_color_id);

COMMENT ON TABLE carmen.map_ui IS 'UI settings, by map';

COMMENT ON COLUMN carmen.map_ui.ui_id IS 'primary key';

COMMENT ON COLUMN carmen.map_ui.map_id IS 'Map ID';

COMMENT ON COLUMN carmen.map_ui.ui_banner IS 'Banner enabled ?';

COMMENT ON COLUMN carmen.map_ui.ui_banner_file IS 'Banner file';

COMMENT ON COLUMN carmen.map_ui.ui_color_id IS 'UI color ID';

COMMENT ON COLUMN carmen.map_ui.ui_copyright IS 'Copyright enabled ?';

COMMENT ON COLUMN carmen.map_ui.ui_copyright_background_color IS 'Copyright background color';

COMMENT ON COLUMN carmen.map_ui.ui_copyright_font IS 'Copyright font';

COMMENT ON COLUMN carmen.map_ui.ui_copyright_font_color IS 'Copyright font color';

COMMENT ON COLUMN carmen.map_ui.ui_copyright_font_size IS 'Copyright font size';

COMMENT ON COLUMN carmen.map_ui.ui_copyright_text IS 'Copyright text';

COMMENT ON COLUMN carmen.map_ui.ui_copyright_transparency IS 'Copyright transparency (0..100)';

COMMENT ON COLUMN carmen.map_ui.ui_focus IS 'Focus action enabled ?';

COMMENT ON COLUMN carmen.map_ui.ui_keymap IS 'Keymap enabled ?';

COMMENT ON COLUMN carmen.map_ui.ui_legend IS 'Legend enabled ?';

COMMENT ON COLUMN carmen.map_ui.ui_legend_print IS 'Legend enabled during map print ?';

COMMENT ON COLUMN carmen.map_ui.ui_locate IS 'Locate action enabled ?';

COMMENT ON COLUMN carmen.map_ui.ui_logo IS 'Logo enabled ?';

COMMENT ON COLUMN carmen.map_ui.ui_logopath IS 'Logo path';

CREATE TABLE carmen.ui_model (
  ui_model_id          serial NOT NULL,
  account_model_id    int4,
  model_rank           int4,
  ui_id                int4,
  CONSTRAINT pk_ui_model PRIMARY KEY (ui_model_id)
);

CREATE INDEX idx_ui_model ON carmen.ui_model (ui_id);

CREATE INDEX idx_ui_model_0 ON carmen.ui_model (account_model_id);

COMMENT ON TABLE carmen.ui_model IS 'List of models used in map';

COMMENT ON COLUMN carmen.ui_model.ui_model_id IS 'primary key';

COMMENT ON COLUMN carmen.ui_model.account_model_id IS 'modele id';

COMMENT ON COLUMN carmen.ui_model.model_rank IS 'Model rank';

COMMENT ON COLUMN carmen.ui_model.ui_id IS 'Map UI ID';

CREATE TABLE carmen.account_model (
  account_model_id     serial NOT NULL,
  account_id           int4 NOT NULL,
  account_model_name   text,
  account_model_file   text, 
  CONSTRAINT pk_account_model PRIMARY KEY (account_model_id)
);


COMMENT ON TABLE carmen.account_model IS 'List of account models';

COMMENT ON COLUMN carmen.account_model.account_model_id IS 'primary key';

COMMENT ON COLUMN carmen.account_model.account_model_name IS 'Model name';

COMMENT ON COLUMN carmen.account_model.account_model_file IS 'Model file name';

COMMENT ON COLUMN carmen.account_model.account_id IS 'account identifier';

CREATE INDEX idx_account_model ON carmen.account_model (account_id);

ALTER TABLE carmen.account ADD CONSTRAINT fk_account FOREIGN KEY (account_dns_id) REFERENCES carmen.lex_dns(dns_id);

ALTER TABLE carmen.contact ADD CONSTRAINT fk_contact_user FOREIGN KEY (user_id) REFERENCES carmen.users(user_id);

ALTER TABLE carmen.favorite_area ADD CONSTRAINT fk_favorite_areas FOREIGN KEY (map_id) REFERENCES carmen.map(map_id);

ALTER TABLE carmen.field ADD CONSTRAINT fk_fields FOREIGN KEY (layer_id) REFERENCES carmen.layer(layer_id);

ALTER TABLE carmen.field ADD CONSTRAINT fk_field_lex_field_datatype FOREIGN KEY (field_datatype_id) REFERENCES carmen.lex_field_datatype(field_datatype_id);

ALTER TABLE carmen.field ADD CONSTRAINT fk_fields_0 FOREIGN KEY (field_type_id) REFERENCES carmen.lex_field_type(field_type_id);

ALTER TABLE carmen.keyword ADD CONSTRAINT fk_keywords FOREIGN KEY (category_id) REFERENCES carmen.lex_category_keyword(category_id);

ALTER TABLE carmen.layer ADD CONSTRAINT fk_layer_0 FOREIGN KEY (layer_analyse_type_id) REFERENCES carmen.lex_analyse_type(analyse_type_id);

ALTER TABLE carmen.layer ADD CONSTRAINT fk_layer_lex_layer_type FOREIGN KEY (layer_type_code) REFERENCES carmen.lex_layer_type(layer_type_code);

ALTER TABLE carmen.layer ADD CONSTRAINT fk_layer FOREIGN KEY (map_id) REFERENCES carmen.map(map_id);

ALTER TABLE carmen.map ADD CONSTRAINT fk_map FOREIGN KEY (account_id) REFERENCES carmen.account(account_id);

ALTER TABLE carmen.map ADD CONSTRAINT fk_map_lex_projection FOREIGN KEY (map_projection_epsg) REFERENCES carmen.lex_projection(projection_epsg);

ALTER TABLE carmen.map ADD CONSTRAINT fk_map_publishedid FOREIGN KEY (published_id) REFERENCES carmen.map(map_id);

ALTER TABLE carmen.map ADD CONSTRAINT fk_map_users FOREIGN KEY (user_id) REFERENCES carmen.users(user_id);

ALTER TABLE carmen.map_annotation_attribute ADD CONSTRAINT fk_map_annotations_attributs FOREIGN KEY (map_id) REFERENCES carmen.map(map_id);

ALTER TABLE carmen.map_audit ADD CONSTRAINT fk_map_audit_account_id FOREIGN KEY (account_id) REFERENCES carmen.account(account_id);

ALTER TABLE carmen.map_group ADD CONSTRAINT fk_group_map FOREIGN KEY (map_id) REFERENCES carmen.map(map_id);

ALTER TABLE carmen.map_keyword ADD CONSTRAINT fk_map_keywords_0 FOREIGN KEY (keyword_id) REFERENCES carmen.keyword(keyword_id);

ALTER TABLE carmen.map_keyword ADD CONSTRAINT fk_map_keywords FOREIGN KEY (map_id) REFERENCES carmen.map(map_id);

ALTER TABLE carmen.map_locator ADD CONSTRAINT fk_map_locator_0 FOREIGN KEY (map_id) REFERENCES carmen.map(map_id);

ALTER TABLE carmen.map_locator ADD CONSTRAINT fk_map_locator FOREIGN KEY (locator_criteria_related) REFERENCES carmen.map_locator(locator_id) ON DELETE SET NULL;

ALTER TABLE carmen.map_tool ADD CONSTRAINT fk_map_tools_0 FOREIGN KEY (tool_id) REFERENCES carmen.lex_tool(tool_id);

ALTER TABLE carmen.map_tool ADD CONSTRAINT fk_map_tools FOREIGN KEY (map_id) REFERENCES carmen.map(map_id);

ALTER TABLE carmen.map_tree ADD CONSTRAINT fk_map_tree_map FOREIGN KEY (map_id) REFERENCES carmen.map(map_id);

ALTER TABLE carmen.map_tree_print ADD CONSTRAINT fk_map_tree_print_map FOREIGN KEY (map_id) REFERENCES carmen.map(map_id);

ALTER TABLE carmen.map_ui ADD CONSTRAINT fk_map_ui_0 FOREIGN KEY (ui_color_id) REFERENCES carmen.lex_color_id(color_id);

ALTER TABLE carmen.map_ui ADD CONSTRAINT fk_map_ui FOREIGN KEY (map_id) REFERENCES carmen.map(map_id);

ALTER TABLE carmen.preferences ADD CONSTRAINT fk_preferences_lex_units FOREIGN KEY (preference_units) REFERENCES carmen.lex_units(unit_code);

ALTER TABLE carmen.preferences ADD CONSTRAINT fk_preferences_user FOREIGN KEY (user_id) REFERENCES carmen.users(user_id);

ALTER TABLE carmen.ui_model ADD CONSTRAINT fk_ui_model FOREIGN KEY (ui_id) REFERENCES carmen.map_ui(ui_id);

ALTER TABLE carmen.ui_model ADD CONSTRAINT fk_ui_model_0 FOREIGN KEY (account_model_id) REFERENCES carmen.account_model(account_model_id);

ALTER TABLE carmen.user_audit ADD CONSTRAINT fk_user_audit FOREIGN KEY (user_id) REFERENCES carmen.users(user_id);

ALTER TABLE carmen.users ADD CONSTRAINT fk_user FOREIGN KEY (account_id) REFERENCES carmen.account(account_id);

ALTER TABLE carmen.wxs ADD CONSTRAINT fk_wxs FOREIGN KEY (account_id) REFERENCES carmen.account(account_id);

ALTER TABLE carmen.wxs ADD CONSTRAINT fk_wxs_0 FOREIGN KEY (wxs_type_id) REFERENCES carmen.lex_wxs_type(wxs_type_id);

ALTER TABLE carmen.account_model ADD CONSTRAINT fk_account_model FOREIGN KEY (account_id) REFERENCES carmen.account(account_id);

