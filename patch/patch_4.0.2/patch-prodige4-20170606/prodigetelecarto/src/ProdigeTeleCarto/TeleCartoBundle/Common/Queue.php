<?php
namespace ProdigeTeleCarto\TeleCartoBundle\Common;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;


use ZipArchive;
use Prodige\ProdigeBundle\Services\GeonetworkInterface;
use Prodige\ProdigeBundle\Controller\BaseController;

//include_once("lib/lib_service.php");
//include_once("dbpgsql.class.php");
//require_once("lib/ods/ods.php");
//require_once("lib/excel/Workbook.php");

/************************************************************
 * @abstract classe de gestion de la file de téléchargement *
 * @author Alkante                                          *
 ************************************************************/

class Queue {
    
    private $metadataInfo = array();

    private $conn;
    protected static $controller = null;

    static public $VECTOR_FILE = 0;
    static public $VECTOR_POSTGIS = 1;
    static public $TABLE_POSTGIS = 2;
    static public $RASTER_FILE = 3;
    static public $MAJIC_FILE = 4;
    //URL can be WFS getFeature
    static public $URL = 5;
    static public $RASTER_HR_FILE = 6;

    static private $GDAL_TRANSLATE_CMD = "%GDAL_TRANSLATE_PATH% --config GDAL_CACHEMAX 300 -projwin %GDAL_PROJ_WIN% -of %FORMAT_OPTION% %FILE_IN% %FILE_OUT%";
    static private $GDAL_WARP_CMD = "%GDAL_WARP_PATH% -of VRT -s_srs %PROJ_IN% -t_srs %PROJ_OUT% %FILE_IN% %FILE_OUT%";

    protected $strFolderPath;

    /* instance de gestion des connexions PostGreSQL / PostGIS */
    protected $dbPgSql;

    /**********************************************************
     * @abstract constructeur de la classe Queue              *
     * @param strFolderPath   chemin vers le répertoire QUEUE *
     **********************************************************/
    function __construct($strFolderPath) {
        $this->strFolderPath = $strFolderPath;
        $this->conn = $this->getConnection('prodige');
        $this->catalogueConn = $this->getConnection('catalogue');
    }

    public static function initController(Controller $controller) {
        self::$controller = $controller;
    }

    /*if(!self::$container) throw new \Exception('container is not initialized, call LibMap:initContainer');*/

    /************************************************************************************************************************
     * @abstract ajoute une demande de téléchargement dans la file                                                          *
     * @param service_idx     identifiant du service                                                                        *
     * @param strMail         adresse mail de l'utilisateur                                                                 *
     * @param formatOutput    format de sortie (extension sans le ".")                                                      *
     * @param strProjection   projection de sortie (EPSG:****)                                                              *
     * @param strData         données en entrée (séparée par "%")                                                           *
     * @param strMetadataId   identifiants des métadonnées en entrée (séparée par "%")                                      *
     * @param intTypeInput    type de données en entrée (0 = Fichier, 1 = PostGIS)                                          *
     * @param pgConnection    chaîne qui contient les paramètres de connexion à la base (format ogr avec doubles quotes)    *
     * @param metadataFile    emplacement du fichier de métadonnée                                                          *
     * @param bTerritoire     booléen indiquant si le téléchargement se fait sur un (des) territoire(s) (1 : oui, 0 : non)  *
     * @param territoire_type identifiant de la division administrative                                                     *
     * @param territoire_data chaîne des identifiants des territoires (séparés par "%")                                     *
     * @param buffer          tolérance d'extraction                                                                        *
     * @param extract_area    geometrie de découpage (format WKT, projection EPSG:$PRO_IMPORT_EPSG)                         *
     * @param restricted_area_field ensembles des champs de restriction territoriale des données                            *
     * @param restricted_area_buffer buffer d'extraction de la donnée restreinte
     * @param territoire_area code des territoires des données dans prodige_perimetre                                       *
     * @return crée un fichier et l'ajoute dans le répertoire QUEUE, retourne son nom si ok, null sinon                     *
     ************************************************************************************************************************/
    public function add_to_queue($service_idx, $strMail, $formatOutput, $strProjection, $strData, $strMetadataId, $intTypeInput, $pgConnection, $metadataFile, $bTerritoire, $territoire_type, $territoire_data, $buffer, $extract_area, $restricted_area_field, $restricted_area_buffer, $territoire_area, $extractionattributaire_couche,  $id_ens_serie) {
        $fname = $this->strFolderPath."/".time()."_".mt_rand(1000,9999).".queue";
        if($fd = fopen($fname, "w")){
            fwrite($fd, $service_idx."|".$strMail."|".$formatOutput."|".$strProjection."|".$strData."|".$strMetadataId."|".$intTypeInput."|".$pgConnection."|".$metadataFile."|".$bTerritoire."|".$territoire_type."|".$territoire_data."|".$buffer."|".$extract_area."|".$restricted_area_field."|".$restricted_area_buffer."|".$territoire_area."|".$extractionattributaire_couche."|".date("Ymd G:i:s")."|".$id_ens_serie);
            fclose($fd);
            return $fname;
        } else {
            if ($fdlog = fopen($this->strFolderPath."/queue.log", "+w")) {
                fwrite($fdlog, $service_idx."|".$strMail."|".$formatOutput."|".$strProjection."|".$strData."|".$strMetadataId."|".$intTypeInput."|".$pgConnection."|".$metadataFile."|".$bTerritoire."|".$territoire_type."|".$territoire_data."|".$buffer."|".$extract_area."|".$restricted_area_field."|".$restricted_area_buffer."|".$territoire_area."|".$extractionattributaire_couche."|".date("Ymd G:i:s")."|".$id_ens_serie."\n");
                fclose($fdlog);
            }
            return null;
        }
    }

    /***************************************************************
     * @abstract supprime une demande de téléchargement            *
     * @param filename    nom du fichier dans le répertoire QUEUE  *
     * @return le fichier associé à la demande est supprimé        *
     ***************************************************************/
    public function remove_from_queue($filename) {
        unlink($filename);
    }
    
    /********************************************************************************************
     * @abstract recherche la liste les téléchargment en attente                                *
    * @return la liste des téléchargments                                                      *
    ********************************************************************************************/
    public function list_queue(){
        $tabListQueue = array();
        $contents = "";
        $handle   = "";
        $filename = "";
    
        $tabFolder = scandir($this->strFolderPath);
        unset($tabFolder[0]);
        unset($tabFolder[1]);
        while(!empty($tabFolder)){
            $strFileName = array_shift($tabFolder);
            if ( substr($strFileName, -6 )!=".queue"){   // fichiers non traités
                continue;
            }
            $filename = $this->strFolderPath."/".$strFileName;
            $handle = fopen($filename, "r");
            $contents = fread($handle, filesize($filename));
            $contents = $contents.$strFileName;
            array_push($tabListQueue,$contents);
            fclose($handle);
        }
        return $tabListQueue;
    }
    /**********************************************************************************************************
     * @abstract vérifie la date des fichiers QUEUE                                                           *
    * @param delai en nombre de jour pour déclencher une alerte de hors délai pour un fichier dans la file   *
    * @return la liste des fichiers hors delai                                                               *
    **********************************************************************************************************/
    public function check_date_file($delai){
        $array_file = $this->list_queue();
        $tab_date=array();
        foreach($array_file as $value){
            $tab_value = explode("|",$value);
            if(count($tab_value)>18){
                $date = DateTime::createFromFormat ( "Ymd H:i:s" ,$tab_value[18]  );
                $now = new DateTime();
                $diff=$date->diff($now);
                 
                if($diff->days>$delai){
                    array_push($tab_date,$date->format("d/m/Y H:i:s"));
                    array_push($tab_date,str_replace("%",", ",$tab_value[4]));
                    array_push($tab_date,$tab_value[2]);
                }
            }
        }
        return $tab_date;
    }
    /********************************************************************************************
     * @abstract suprime tous les fichiers de la liste de téléchargement en attente             *
    ********************************************************************************************/
    public function remove_all_from_queue(){
        $filename = "";
        $tabFolder = scandir($this->strFolderPath);
        unset($tabFolder[0]);
        unset($tabFolder[1]);
        while(!empty($tabFolder)){
            $strFileName = array_shift($tabFolder);
            if ( substr($strFileName, -6 )!=".queue"){   // fichiers non traités
                continue;
            }
            $filename = $this->strFolderPath."/".$strFileName;
            $this->remove_from_queue($filename);
        }
    }

    /********************************************************************************************
     * @abstract lance le traitement d'une demande de téléchargement                            *
     * @param filename   nom du fichier lié à la demande                                        *
     * @param bMail      booléen indiquant l'envoi de mail après le traitement (défaut = false) *
     * @return url du zip des données                                                           *
     ********************************************************************************************/
    public function process($filename, $bMail=false) {
        $contents = "";
        $handle   = "";
        $tabEPSG  = array();

        //Prodige 4.0 : Ne sera pas utilisable dans le contexte PRODIGE
        /*if(defined("ADD_METADATA_FILE") && ADD_METADATA_FILE){
            $epsg=new CSEPSGProcess();
            $epsg->loadFileEPSG(PATH_PROJ_FILE);
            $tabEPSG = $epsg->getSortArray();
        }*/

        $handle = fopen($filename, "r");
        $contents = fread($handle, filesize($filename));
        fclose($handle);
        $urlzip = $this->treat_contents($contents, $bMail, $tabEPSG);
        $this->remove_from_queue($filename);

        return $urlzip;
    }

    /****************************************************************************************
     * @abstract lance le traitement des demandes de téléchargement présents dans la file   *
     * @param bMail   booléen indiquant l'envoi de mail après le traitement (défaut = true) *
     ****************************************************************************************/
    public function process_queue($bMail=true) {
        $contents = "";
        $handle   = "";
        $filename = "";
        $tabEPSG  = array();

        //Prodige 4.0 : Ne sera pas utilisable dans le contexte PRODIGE
        /*if(defined("ADD_METADATA_FILE") && ADD_METADATA_FILE){
            $epsg=new CSEPSGProcess();
            $epsg->loadFileEPSG(PATH_PROJ_FILE);
            $tabEPSG = $epsg->getSortArray();
        }*/

        $tabFolder = scandir($this->strFolderPath);
        unset($tabFolder[0]);
        unset($tabFolder[1]);
        while(!empty($tabFolder)) {
            $strFileName = array_shift($tabFolder);
            if(substr($strFileName, -6 ) != ".queue")   // fichiers non traités
                continue;
            $filename = $this->strFolderPath."/".$strFileName;
            $handle = fopen($filename, "r");
            $contents = fread($handle, filesize($filename));
            fclose($handle);
            $this->treat_contents($contents, $bMail, $tabEPSG);
            $this->remove_from_queue($filename);
        }
    }

    /**
     * Get Doctrine connection
     * @param string $name
     * @param string $schema
     * @return \Doctrine\DBAL\Connection
     */
    protected function getConnection($name, $schema="public") {
        if(!self::$controller) 
            throw new \Exception('controller is not initialized, call Queue:initController');

        $conn = self::$controller->getDoctrine()->getConnection($name);
        $conn->exec('set search_path to '.$schema);

        return $conn;
    }

    protected function getTableColumns($tableDataName) {
        $strSql_getTableColumns= "SELECT a.attnum as NUMB, a.attname as FIELD, t.typname as TYPE, a.attlen as LEN, a.atttypmod, a.attnotnull, a.atthasdef as DEF".
                                 " FROM pg_class c".
                                 "  inner join pg_attribute a on a.attrelid = c.oid" .
                                 "  inner join pg_type t on a.atttypid = t.oid" .
                                 " WHERE c.relname = '".mb_strtolower($tableDataName)."'".
                                 " and a.attnum > 0".
                                 " ORDER BY attnum";
        $tabColumnsInfo = $this->conn->fetchAll($strSql_getTableColumns);
        return $tabColumnsInfo;
    }

    /*********************************************************************************
     * @abstract traite une demande de téléchargement                                *
     * @param contents  contenu de la demande (paramètres nécessaires au traitement) *
     * @param bMail     booléen indiquant l'envoi de mail après le traitement        *
     * @param tabEPSG   tableau de projections (srid => projection)                  *
     * @return url du zip des données                                                *
     *********************************************************************************/
    protected function treat_contents($contents, $bMail, $tabEPSG) {
        //global $PRO_METADATA_LICENCE_PATH;
        // récupère les paramètres de la demande
        $tabParam        = explode("|",$contents);
        $service_idx     = $tabParam[0];
        $email           = $tabParam[1];
        $format          = $tabParam[2];
        $projection      = $tabParam[3];
        $strData         = $tabParam[4];
        $strMetadata     = $tabParam[5];
        $typeInput       = $tabParam[6];
        $pgConn          = $tabParam[7];
        $meta_file       = $tabParam[8];
        $bTerritoire     = $tabParam[9];
        $territoire_type = $tabParam[10];
        $territoire_data = $tabParam[11];
        $buffer          = $tabParam[12];
        $extract_area    = $tabParam[13];
        $restricted_area_field = $tabParam[14];
        $restricted_area_buffer = $tabParam[15];
        $territoire_area = $tabParam[16];
        $extractionattributaire_couche = $tabParam[17];
        $id_ens_serie = $tabParam[19];

        // instancie la classe de connexion à la base de données du service
        //$service = loadService($service_idx);
        //$pgParam = loadPgParam(URL_PATH_DATA.$service["path"]."IHM/Postgis/");
        $pgParam = array("machine" => $this->conn->getHost(),
                         "user"    => $this->conn->getUsername(),
                         "bdd"     => $this->conn->getDatabase(),
                         "mdp"     => $this->conn->getPassword(),
                         "port"    => $this->conn->getPort());
        //$this->dbPgSql = new DbPgSql($pgParam['user'], $pgParam['machine'], $pgParam['mdp'], $pgParam['bdd'], $pgParam['port']);

        // crée le répertoire principal de la demande de téléchargement
        $strMainTeleDir = "Telechargement_".time()."_".mt_rand(1000,9999);
        @mkdir(DIR_DOWNLOAD."/".$strMainTeleDir, 0777, true);

        // ajoute le fichier licence
        if(defined("ADD_LICENCE_FILE") && ADD_LICENCE_FILE) {
            $licenceFileUrl = PRO_METADATA_LICENCE_PATH;
            /*$this->dbPgSql->setSchema("parametrage");
             $strSql = "SELECT prodige_settings_value FROM prodige_settings WHERE prodige_settings_constant='".LICENCE_CONSTANT_NAME."'";
             $res = $this->dbPgSql->execute($strSql);
             if ( !empty($res) ) {
             $licenceFileUrl = $res[0]["prodige_settings_value"];*/

            $licenceFileContents = @file_get_contents($licenceFileUrl);
            $licenceFileName = basename($licenceFileUrl);
            if($licenceFileContents != "" && $licenceFile = fopen(DIR_DOWNLOAD."/".$strMainTeleDir."/".$licenceFileName, "w")) {
                fwrite($licenceFile, $licenceFileContents);
                fclose($licenceFile);
            }
            //}
        }

        // récupère les données en entrée et les traite
        $tabData                 = explode("%", $strData);
        $tabMetadata             = explode("%", $strMetadata);
        $tabRestrictedArea       = explode("%", $restricted_area_field);
        $tabRestrictedAreaBuffer = explode("%", $restricted_area_buffer);
        $tabTerritoireArea       = explode("%", $territoire_area);
        //ecnryptage des
        $bEncryptZip = false;
        
        for($i=0; $i<sizeof($tabMetadata); $i++) {
            $metadataId = $tabMetadata[$i];
            $tabMetadataInfo = $this->getMetadataInfo($metadataId);
            
            $data = explode(",", $tabData[$i]);
            switch ($typeInput) {
                case Queue::$VECTOR_FILE :
                case Queue::$VECTOR_POSTGIS :
                    
                    $strDirectory = $tabMetadataInfo["metadata_uuid"]."_".time()."_".mt_rand(1000,9999);
                    
                    $toDirectory = DIR_DOWNLOAD."/".$strMainTeleDir."/".$strDirectory;
                    foreach($data as $a_data){
                        $this->treat_data_vector(
                            $toDirectory, 
                            $typeInput, 
                            $a_data, 
                            $tabMetadata[$i], 
                            $format, 
                            $projection, 
                            $pgConn, 
                            $meta_file, 
                            $tabEPSG, 
                            $bTerritoire, 
                            $territoire_type, 
                            $territoire_data, 
                            $buffer, 
                            $extract_area, 
                            $tabRestrictedArea[$i], 
                            $tabRestrictedAreaBuffer[$i], 
                            $tabTerritoireArea[$i], 
                            $extractionattributaire_couche
                        );
                    }
                    //supprime les données temporaires
                    $this->CleanData($data);
                    break;
                case Queue::$TABLE_POSTGIS :
                    foreach($data as $a_data){
                        $this->treat_data_table($strMainTeleDir, $a_data, $tabMetadata[$i], $format, $pgConn, $meta_file);
                    }
                    break;
                    
                case Queue::$RASTER_HR_FILE :
                    $paramsData = explode(":", $tabData[$i]);
                    if ( $fdlogFile = fopen($this->strFolderPath."/queue.log", "a+") ) {
                        fwrite($fdlogFile, "LOG :  ".print_r($paramsData, true)." LE ".date("Ymd G:i:s")."\n");
                        fclose($fdlogFile);
                    }
                    $dataAreaLimit = null;
                    if (count($paramsData)>1) {
                        $dataAreaLimit = explode(",", $paramsData[1]);
                        if ( $fdlogFile = fopen($this->strFolderPath."/queue.log", "a+") ) {
                            fwrite($fdlogFile, "LOG :  ".print_r($dataAreaLimit, true)." LE ".date("Ymd G:i:s")."\n");
                            fclose($fdlogFile);
                        }
                    }
                    // check for raster color type : palette -> 0, bands (rgb,...) -> 1
                    $dataColorInterp=1;
                    if (count($paramsData)>2) {
                        $dataColorInterp = $paramsData[2];
                        if ( $fdlogFile = fopen($this->strFolderPath."/queue.log", "a+") ) {
                            fwrite($fdlogFile, "LOG : ColorInterp -> ". $dataColorInterp." LE ".date("Ymd G:i:s")."\n");
                            fclose($fdlogFile);
                        }
                    }
                    $this->treat_data_raster_hr($strMainTeleDir, $typeInput, $paramsData[0], $tabMetadata[$i], $format, $projection, $pgConn, $meta_file, $tabEPSG,
                        $bTerritoire, $territoire_type, $territoire_data, $buffer, $extract_area, $dataAreaLimit, $dataColorInterp, $tabRestrictedArea[$i],
                        $tabTerritoireArea[$i]);
                    break;
                case Queue::$RASTER_FILE :
                    $paramsData = explode(":", $tabData[$i]);
                    if($fdlogFile = fopen($this->strFolderPath."/queue.log", "a+")) {
                        fwrite($fdlogFile, sprintf("[%s] LOG :  ".print_r($paramsData, true)."\n", date("Y-m-d G:i:s")));
                        fclose($fdlogFile);
                    }
                    $dataAreaLimit = null;
                    if(count($paramsData) > 1) {
                        $dataAreaLimit = explode(",", $paramsData[1]);
                        if($fdlogFile = fopen($this->strFolderPath."/queue.log", "a+")) {
                            fwrite($fdlogFile, sprintf("[%s] LOG :  ".print_r($dataAreaLimit, true)."\n", date("Y-m-d G:i:s")));
                            fclose($fdlogFile);
                        }
                    }
                    // check for raster color type : palette -> 0, bands (rgb,...) -> 1
                    $dataColorInterp = 1;
                    if(count($paramsData) > 2) {
                        $dataColorInterp = $paramsData[2];
                        if($fdlogFile = fopen($this->strFolderPath."/queue.log", "a+")) {
                            fwrite($fdlogFile, sprintf("[%s] LOG : ColorInterp -> ". $dataColorInterp."\n", date("Y-m-d G:i:s")));
                            fclose($fdlogFile);
                        }
                    }

                    $this->treat_data_raster($strMainTeleDir, $typeInput, $paramsData[0], $tabMetadata[$i], $format, $projection, $pgConn, $meta_file, $tabEPSG, $bTerritoire, $territoire_type, $territoire_data, $buffer, $extract_area, $dataAreaLimit, $dataColorInterp, $tabRestrictedArea[$i], $tabTerritoireArea[$i]);
                    break;
                case Queue::$MAJIC_FILE :
                    $this->treat_data_majic($strMainTeleDir, $tabData[$i], $tabMetadata[$i], $meta_file, $territoire_data, $service["path"]);
                    $bEncryptZip = true;
                    break;
                case Queue::$URL:
                    $this->treat_data_URL($strMainTeleDir, $tabData[$i], $tabMetadata[$i], $format, $meta_file);
                    break;
            }
        }

        //dans le cas d'un ensemble de serie de données
        // ajoute les fichiers concernant l'ensmeble de série de métadonnées
        if($id_ens_serie != "") {
            $this->add_metadata_files($id_ens_serie, DIR_DOWNLOAD."/".$strMainTeleDir, "fiche_ensemble_de_serie_".$id_ens_serie);
        }
        $strPassword = "";
        $strArchiveFile = "";
        // crée le zip des données
        if($bEncryptZip) {
            $strPassword = $this->zipEncryptDirToFile(DIR_DOWNLOAD, $strMainTeleDir, DIR_DOWNLOAD."/".$strMainTeleDir.".zip");
            $strArchiveFile = $strMainTeleDir.".zip";
        } else {
            //traitement en fonction du volume
            if($this->dirsize(DIR_DOWNLOAD."/".$strMainTeleDir) > 2097152) { //2Go
                $strArchiveFile = $this->tarGzDirToFile(DIR_DOWNLOAD, $strMainTeleDir, $strMainTeleDir.".tar.gz");
            } else {
                $strArchiveFile = $this->zipDirToFile(DIR_DOWNLOAD, $strMainTeleDir, DIR_DOWNLOAD."/".$strMainTeleDir.".zip");
            }
        }

        // supprime le répertoire des données
        exec("rm -r ".DIR_DOWNLOAD."/".$strMainTeleDir);

        // envoi le mail
        if($bMail) {
            $this->send_mail($email, URL_SERVER_DOWNLOAD."/".$strArchiveFile, $strPassword);
        }
        return URL_SERVER_DOWNLOAD."/".$strArchiveFile;
    }

    protected function getSubExtraction($fullTable, $currentTable, $idExtrac, $pkExtrac, $nbPasse, &$arrayLiaison=array()) {
        while(!empty($currentTable) && $currentTable["pk"] != $pkExtrac && $nbPasse < 20) {
            $idLinked = array();
            foreach($fullTable as $tabData) {
                if($tabData["id_join"] == $currentTable["id"]) {
                    $idLinked[] = array("pk" => $tabData["pk"], "id" => $tabData["id"]);
                }
            }
            if(count($idLinked) == 0) {
                $arrayLiaison[] = array(
                                        "champ_liaison" => $currentTable["champ_join"],
                                        "id_liaison"    => $currentTable["id_join"],
                                        "champ_id"      => $currentTable["champ_id"],
                                        "table"         => $currentTable["table"],
                                        "ispk"          => false,
                                        "id"            => $currentTable["id"],
                                        "pk"            => $currentTable["pk"]
                                    );
                return;
            } else {
                $nbchild = 0;
                foreach($idLinked as $tabSub) {
                    $arrayLiaison[] = array(
                                            "champ_liaison" => $currentTable["champ_join"],
                                            "id_liaison"    => $currentTable["id_join"],
                                            "champ_id"      => $currentTable["champ_id"],
                                            "table"         => $currentTable["table"],
                                            "ispk"          => false,
                                            "id"            => $currentTable["id"],
                                            "pk"            => $currentTable["pk"]
                                        );
                    $nbchild++;
                    $cTable = $fullTable[$tabSub["id"]];
                    $subchild = $this->getSubExtraction($fullTable, $cTable, $idExtrac, $pkExtrac, ++$nbPasse, $arrayLiaison);
                }
            }
            $currentTable = null;
        }
        if($currentTable["pk"] == $pkExtrac) {
            $arrayLiaison[] = array(
                                    "champ_liaison" => $currentTable["champ_join"],
                                    "id_liaison"    => $currentTable["id_join"],
                                    "champ_id"      => $currentTable["champ_id"],
                                    "table"         => $currentTable["table"],
                                    "ispk"          => true,
                                    "id"            => $currentTable["id"],
                                    "pk"            => $currentTable["pk"]
                                );
        }
    }

    protected function getExtractionattributaireWhereClause($coucheExtractionTable, $coucheColonneExtraction, $territoire_type, $territoire_data, $tableTerritoire, $champTerritoire, $tabTerritoireId) {

        $strSql = "SELECT pk_critere_moteur AS pk, critere_moteur_order AS id, critere_moteur_table AS table, critere_moteur_champ_id AS champ_id, critere_moteur_champ_nom AS champ_nom, critere_moteur_id_join AS id_join, critere_moteur_champ_join AS champ_join, critere_moteur_extractionattributaire AS extractionattributaire
                   FROM prodige_download_param ORDER BY critere_moteur_order";
        //$this->dbPgSql->setSchema("parametrage");
        $this->conn->exec('set search_path to parametrage');
        //$tableExtraction = $this->dbPgSql->execute($strSql);
        $tableExtraction = $this->conn->fetchAll($strSql);
        $extractions = array();
        $idExtraction = "";
        $pkExtraction = "";
        $strWhere     = "";

        foreach($tableExtraction as $lineExtraction) {
            if($lineExtraction["pk"] == $territoire_type)
                $territoire_id = $lineExtraction["id"];
            if($lineExtraction["extractionattributaire"] == 1) {
                $idExtraction = $lineExtraction["id"];
                $pkExtraction = $lineExtraction["pk"];
            }
            $extractions[$lineExtraction["id"]] = array(
                                                        "pk"                     => $lineExtraction["pk"],
                                                        "id"                     => $lineExtraction["id"],
                                                        "table"                  => $lineExtraction["table"],
                                                        "champ_id"               => $lineExtraction["champ_id"],
                                                        "champ_nom"              => $lineExtraction["champ_nom"],
                                                        "id_join"                => $lineExtraction["id_join"],
                                                        "champ_join"             => $lineExtraction["champ_join"],
                                                        "extractionattributaire" => $lineExtraction["extractionattributaire"]
                                                    );
        }

        $currentExtractionTable = $extractions[$territoire_id];
        $passe = 1;
        $arrayLiaison = array();
        $subExtraction = $this->getSubExtraction($extractions, $currentExtractionTable, $idExtraction, $pkExtraction, 0, $arrayLiaison);
        $extractionAttributaireFound   = false;
        $idExtractionAttributaireFound = -1;
        $strWhereExtraction = "";
        foreach($arrayLiaison as $key => $tabLiaison) {
            $arrayLiaison[$key]["indice"] = $key;
            if(strcmp($tabLiaison["ispk"], 1) == 0) {
                $extractionAttributaireFound = true;
                $idExtractionAttributaireFound = $key;
            }
        }
        if($extractionAttributaireFound && $coucheExtractionTable != "" && $coucheColonneExtraction != "") {
            if(count($arrayLiaison) == 1) {
                $strWhereExtraction = " ".$coucheExtractionTable.".".$coucheColonneExtraction."::text = '".$territoire_data."'::text";
            } else {
                $tabBase = $arrayLiaison[$idExtractionAttributaireFound];
                $extractionSubchilds = array($tabBase);
                while($tabBase["pk"] != $territoire_type) {
                    foreach($arrayLiaison as $key => $tabLiaison) {
                        if($tabLiaison["id"] = $tabBase["id_liaison"])
                            $tabBase = $arrayLiaison[$tabLiaison["indice"]];
                    }
                    $extractionSubchilds[] = $tabBase;
                }
                $strWhereExtraction = "<CODE>";
                //On a enfin l'arborescence dans $extractionSubchilds
                for($i = 0; $i <= count($extractionSubchilds)-2; $i++) {
                    $extractionSubchilds[$i]["champ_liaison"] = ($extractionSubchilds[$i]["champ_liaison"] == "" ? "''" : $extractionSubchilds[$i]["champ_liaison"]);
                    if($strWhereExtraction == "<CODE>")
                        $strWhereExtraction = " (SELECT ".$extractionSubchilds[$i]["table"].".".$extractionSubchilds[$i]["champ_id"]."::text FROM ".$extractionSubchilds[$i]["table"]." WHERE ".$extractionSubchilds[$i]["table"].".".$extractionSubchilds[$i]["champ_liaison"]."::text IN <CODE>) ";
                    else
                        $strWhereExtraction = str_replace("<CODE>", " (SELECT ".$extractionSubchilds[$i]["table"].".".$extractionSubchilds[$i]["champ_id"]."::text FROM ".$extractionSubchilds[$i]["table"]." WHERE ".$extractionSubchilds[$i]["table"].".".$extractionSubchilds[$i]["champ_liaison"]."::text IN <CODE>) ", $strWhereExtraction);
                }
                $tabBase = $extractionSubchilds[0];
                // 	      echo "\n\n=====================\n\n $strWhereExtraction \n\n================\n\n";
                $strWhereExtraction = " ".$coucheExtractionTable.".".$coucheColonneExtraction."::text in (".$strWhereExtraction.") ";
                // 	      echo "\n\n=====================\n\n $strWhereExtraction \n\n================\n\n";
                $strWhereExtraction = str_replace("<CODE>", "('".$territoire_data."'::text)", $strWhereExtraction);
                // 	      echo "\n\n=====================\n\n $strWhereExtraction \n\n================\n\n";
            }
        } else {
            $strWhereExtraction = "";
        }
        return $strWhereExtraction;
    }
    /**************************************************************************************************************************
     * @abstract lance le traitement d'une donnée                                                                             *
     * @param $toDirectory   nom du répertoire principal des données                                                       *
     * @param typeInput         type de données en entrée (0 = Fichier, 1 = PostGIS)                                          *
     * @param data              nom de la donnée en entrée (nom du fichier ou nom de la table)                                *
     * @param metadata_id       identifiant de la métadonnée associée aux données                                             *
     * @param format            format de sortie (extension sans le ".")                                                      *
     * @param projection        projection de sortie (EPSG:****)                                                              *
     * @param pgConn            chaîne qui contient les paramètres de connexion à la base (format ogr avec doubles quotes)    *
     * @param meta_file         emplacement du fichier de métadonnées                                                         *
     * @param tabEPSG           tableau de projections (srid => projection)                                                   *
     * @param bTerritoire       booléen indiquant si le téléchargement se fait sur un (des) territoire(s) (1 : oui, 0 : non)  *
     * @param territoire_type   identifiant de la division administrative                                                     *
     * @param territoire_data   chaîne des identifiants des territoires (séparés par "%")                                     *
     * @param buffer            tolérance d'extraction                                                                        *
     * @param extract_area      geometrie de découpage (format WKT, projection EPSG:$PRO_IMPORT_EPSG                          *
     * @param restricted_area_field champs de restriction territorial des données (dans la table prodige_perimetre )          *
     * @param restricted_area_buffer tolerance d'extraction en mètres
     * @param territoire_area   territoires de restriction territorial des données                                            *

     **************************************************************************************************************************/
    protected function treat_data_vector($toDirectory, $typeInput, $data, $metadata_id, $format, $projection, $pgConn, $meta_file, $tabEPSG, $bTerritoire, $territoire_type, $territoire_data, $buffer, $extract_area, $restricted_area_field, $restricted_area_buffer, $territoire_area, $coucheExtractionAttribut) {

        @mkdir($toDirectory, 0777, true);
        //global $PRO_IMPORT_EPSG;
        $tabError = array();
    
        if($typeInput == Queue::$VECTOR_POSTGIS) {
            $fileName =$data;
            $originalExtension = "Postgis";
        } else {
            $fileName = substr($data, strrpos($data, "/")+1,strrpos($data, ".")-strrpos($data, "/")-1);
            $originalExtension = strtolower(substr($data, strrpos($data, '.') + 1));
        }
        
        $default_extension = "the_current_format";
        $tabFormats = array(
            "shp"  => array("format"=>"ESRI Shapefile", "nln"=>false, "extension"=>$default_extension),
            "tab"  => array("format"=>"MapInfo File"  , "nln"=>false, "extension"=>$default_extension),
            "mif"  => array("format"=>"MapInfo File"  , "nln"=>false, "extension"=>$default_extension),
            "kml"  => array("format"=>"KML"           , "nln"=>false, "extension"=>$default_extension),
            "gml"  => array("format"=>"GML"           , "nln"=>false, "extension"=>$default_extension),
            "csv"  => array("format"=>"CSV"           , "nln"=>false, "extension"=>$default_extension),
            "bna"  => array("format"=>"BNA"           , "nln"=>false, "extension"=>$default_extension),
            "gxt"  => array("format"=>"Geoconcept"    , "nln"=>false, "extension"=>$default_extension),
            "gmt"  => array("format"=>"GMT"           , "nln"=>false, "extension"=>$default_extension),
            "dgn"  => array("format"=>"DGN"           , "nln"=>false, "extension"=>$default_extension),
            "dxf"  => array("format"=>"DXF"           , "nln"=>false, "extension"=>$default_extension),
            "json" => array("format"=>"GeoJSON"       , "nln"=>false, "extension"=>$default_extension),
            "xyz"  => array("format"=>"CSV"           , "nln"=>true , "extension"=>$default_extension, "lco"=>array("SEPARATOR=TAB")),
            "asc"  => array("format"=>"CSV"           , "nln"=>true , "extension"=>$default_extension, "lco"=>array("SEPARATOR=COMMA")),
        );
        
        if ( !isset($tabFormats[strtolower($format)]) ){
            return;//throw new \Exception("Format {$format} non reconnu");
        }
        $formatProperties = $tabFormats[strtolower($format)];
        
        if ( $formatProperties["extension"]==$default_extension ) $formatProperties["extension"] = strtolower($format);
        $strFileName = $fileName.".".$formatProperties["extension"];
        
        $strCommande = implode(" ", array(
            "mkdir -p ".escapeshellarg($toDirectory).";",
            
            OGR_PATH,
            '-lco ENCODING="UTF-8"',
            '-gt 10000',
            '-f "'.$formatProperties["format"].'"',
            ($formatProperties["nln"] ? '-nln "'.$strFileName.'"' : ''),
            '"'.$toDirectory.'/'.$strFileName.'"',
            ''
        ));
        

        //check prj ressources
        if($projection != "") {
            if(file_exists(realpath(dirname(__FILE__))."/ressources/".str_replace(":", "", $projection)."_".$format.".srs")) {
                $strCommande .= " -t_srs ".$projection." -a_srs ".realpath(dirname(__FILE__))."/ressources/".str_replace(":", "", $projection)."_".$format.".srs ";
            } else if(file_exists(realpath(dirname(__FILE__))."/ressources/".str_replace(":", "", $projection).".srs")) {
                $strCommande .= " -t_srs ".$projection." -a_srs ".realpath(dirname(__FILE__))."/ressources/".str_replace(":", "", $projection).".srs ";
            } else {
                $strCommande .= " -t_srs ".$projection." -a_srs ".$projection." ";
            }
        }

        switch($typeInput) {

            case Queue::$VECTOR_FILE :
                //Files
                $strCommande .=  $data;
                break;

            case Queue::$VECTOR_POSTGIS :

                $strSql = "select 1 from information_schema.tables where table_name=?";
                $exists = $this->conn->fetchColumn($strSql, array($data));
                if ( !$exists ){
                
                    // écrit dans le fichier de log administrateur
                    if($fdlogFile = fopen($this->strFolderPath."/queue.log", "a+")) {
                        fwrite($fdlogFile, sprintf("[%s] ERREUR : La table {$data} n'existe pas\n", date("Y-m-d G:i:s")));
                        fclose($fdlogFile);
                    }
                    // génère un fichier d'erreur dans le répertoire des données (visible pour l'utilisateur)
                    if($fdlogFile = fopen($toDirectory."/erreur.log", "w+")) {
                        fwrite($fdlogFile, sprintf("[%s] ERREUR : La table {$data} n'existe pas\n", date("Y-m-d G:i:s")));
                        fclose($fdlogFile);
                    }
                    return;
                }
                //Postgis
                $strCommande .=  $pgConn;

                $input_srid = "";
                $input_type = "";

                // récupère les propriétés de la table et les affecte à la sortie
                $strSql = "select * from public.geometry_columns where f_table_name=?";
                //$this->dbPgSql->setSchema("public");
                //$tabTable = $this->dbPgSql->execute($strSql);
                $tabTable = $this->conn->fetchAll($strSql, array($data));

                if(!empty($tabTable)) {
                    $input_srid = $tabTable[0]["srid"];
                    $input_type = $tabTable[0]["type"];
                }
                //since views are not well registered in geometry_columns
                if($input_srid==0){
                    $geomInfo = $this->conn->fetchAll(
                        "select public.st_srid(the_geom) as srid, public.geometrytype(the_geom) as type
                                                    from public.".$data." limit 1"
                    );
                    if(!empty($geomInfo)) {
                        $input_srid = $geomInfo[0]["srid"];
                        $input_type = $geomInfo[0]["type"];
                    }
                }
                
                // ajoute la requête de clippage
                $bClip = false;
                //restriction territoriale des droits
                
                if($restricted_area_field != "" && $territoire_area != "") {
                    switch($input_type) {
                        case "POLYGON":
                        case "MULTIPOLYGON":
                            $input_type = "MULTIPOLYGON";
                            break;
                        case "LINESTRING":
                        case "MULTILINESTRING":
                            $input_type = "MULTILINESTRING";
                            break;
                        case "POINT":
                            $input_type = "POINT";
                            break;
                        case "MULTIPOINT":
                            $input_type = "MULTIPOINT";
                            break;
                    }
                    //si le buffer restreint est défini, il est forcé
                    if($restricted_area_buffer != "")
                        $buffer = $restricted_area_buffer;
 
                    $strSql = $this->createSqlClip($data, $buffer, "st_intersects", "prodige_perimetre", $restricted_area_field, "", array($territoire_area), $restricted_area_buffer, false, $input_type);
                    if($strSql) {
                        $strCommande .=  " -sql \"".$strSql."\"";
                        $bClip = true;
                    }
                }
                else if($bTerritoire == "1") {
                    switch($input_type) {
                        case "POLYGON":
                        case "MULTIPOLYGON":
                            $input_type = "MULTIPOLYGON";
                            break;
                        case "LINESTRING":
                        case "MULTILINESTRING":
                            $input_type = "MULTILINESTRING";
                            break;
                        case "POINT":
                            $input_type = "POINT";
                            break;
                        case "MULTIPOINT":
                            $input_type = "MULTIPOINT";
                            break;
                    }
                    //extraction basée sur une donnée d'une table
                    if($territoire_type != "") {
                        $strSql = "SELECT critere_moteur_table, critere_moteur_champ_id FROM prodige_download_param WHERE pk_critere_moteur=?";
                        //$this->dbPgSql->setSchema("parametrage");
                        $this->conn->exec('set search_path to parametrage');
                        //$tabTerritoire = $this->dbPgSql->execute($strSql);
                        $tabTerritoire = $this->conn->fetchAll($strSql, array($territoire_type));

                        if(!empty($tabTerritoire)) {
                            $tableTerritoireName   = /*utf8_encode*/($tabTerritoire[0]['critere_moteur_table']);
                            $champTerritoireIdName = /*utf8_encode*/($tabTerritoire[0]['critere_moteur_champ_id']);
                            $tabTerritoireId       = explode("%", $territoire_data);

                            //On récupère la clause where si extraction attributaire
                            $extractionWhere = "";
                            $extractionWhere = $this->getExtractionattributaireWhereClause($data, $coucheExtractionAttribut, $territoire_type, $territoire_data, $tableTerritoireName, $champTerritoireIdName, $tabTerritoireId);
                            //$strSql = $this->createSqlClip($data, $buffer, "st_intersects", $tableTerritoireName, $champTerritoireIdName, $tabTerritoireId, 0 ,true, $input_type);
                            $strSql = $this->createSqlClip($data, $buffer, "st_intersects", $tableTerritoireName, $champTerritoireIdName, $extractionWhere, $tabTerritoireId, 0 ,true, $input_type);
                        }
                    }
                    //extraction basée sur un objet WKT
                    else if($extract_area != "") {
                        $strSql = $this->createSqlClipFromWkt($data, $buffer, "st_intersects", $extract_area, $input_type);
                    }
                    if($strSql) {
                        $strCommande .=  " -sql \"".$strSql."\"";
                        $bClip = true;
                    }
                }

                // pas de clippage, ajoute les champs de la table pour conserver leur ordonnancement
                if(!$bClip) {
                    //$tabColumnsInfo = $this->dbPgSql->getTableColumns($data);
                    $tabColumnsInfo = $this->getTableColumns($data);
                    $strTableFields = "";
                    $glue = "";
//                     for($i=0; $i<sizeof($tabColumnsInfo); $i++) {
//                         if($tabColumnsInfo[$i]["field"] != "gid") {
//                             $strTableFields .= $glue."public.".$data.".\\\"".($tabColumnsInfo[$i]["field"])."\\\"";
//                             $glue = ",";
//                         }
//                     }
                    if(strtolower($format) == "asc" || strtolower($format) == "xyz")
                        $strOrder = " order by y,x asc ";
                    else
                        $strOrder = "";
                    $strCommande .= " -sql \"select distinct ".( $strTableFields ? $strTableFields : "*" )." from public.".$data.$strOrder." \"";
                }

                if($input_srid != "-1") {
                    $strCommande .= " -s_srs EPSG:".$input_srid." -nlt \"PROMOTE_TO_MULTI\"";
                }
                break;
        }

        // ajoute un fichier d'information
        if(defined("ADD_METADATA_FILE") && ADD_METADATA_FILE) {
            $strText = "La couche ";
            $strText .= "provient ";
            if($typeInput == Queue::$VECTOR_POSTGIS) {
                $strText.= "de la table ";
            } else {
                $strText.= "du fichier géographique ";
            }
            $strText.= $fileName;
            $strText.= " au format ".$originalExtension.".\r\n";
            $strText.= "Pour le téléchargement vous avez choisi le format ".strtoupper($format);

            $tabProjection = explode(":", $projection);
            if(array_key_exists($tabProjection[1], $tabEPSG)) {
                $mapProjection = $tabEPSG[$tabProjection[1]]." (".$projection.")";
            }
            $strText .= " et le système de référence spatiale".$mapProjection.".\r\n";
            $strText .= "Nous attirons votre attention sur le fait que le fichier de métadonnées fourni dans le zip de téléchargement correspond au fichier géographique d'origine de la carte.".
                        "\r\nIl est de votre ressort de mettre à jour ou compléter les métadonnées avec les informations décrites dans le présent document.";

            if($fdmetadataFile = fopen($toDirectory."/info_donnees_telechargement.txt", "w") ) {
                fwrite($fdmetadataFile, utf8_decode($strText));
                fclose($fdmetadataFile);
            }
        }
        // ajoute le fichier de métadonnée (passé en paramètre selon l'ancienne méthode)
        if(file_exists($meta_file) && is_file($meta_file)) {
            copy($meta_file, $toDirectory."/".$meta_file);
        }
        // ajoute les fichiers de la métadonnée
        $this->add_metadata_files($metadata_id, $toDirectory, $fileName);

        //On modifie le fichier dans les cas de donnees 3D
        if(strtolower($format) == "xyz") {
            //On remplace par la bonne extension .xyz au lieu de .csv
            $strCommande .= "; mv '".$toDirectory."/".$strFileName."/".$strFileName.".csv' '".$toDirectory."/".$strFileName."/".$strFileName."';";
            //On supprime la ligne d'entete
            $strCommande .= " sed -i -e \"1d\" '".$toDirectory."/".$strFileName."/".$strFileName."';";
        } 
        elseif(strtolower($format) == "asc") {
            //On remplace par la bonne extension .xyz au lieu de .csv
            $strCommande .= "; mv '".$toDirectory."/".$strFileName."/".$strFileName.".csv' '".$toDirectory."/".$strFileName."/".$strFileName.".xyz';";
            //On modifie le séparateur
            $strCommande .= " sed -i 's/,/\\t/g' ".$toDirectory."/".$strFileName."/".$strFileName.".xyz;";
            //On transforme le fichier depuis son format XYZ vers ASCII
            $strCommande .= " gdal_translate -q -of AAIGrid '".$toDirectory."/".$strFileName."/".$strFileName.".xyz' '".$toDirectory."/".(str_replace(".xyz", "_asc", $strFileName))."/".(str_replace(".xyz", ".asc", $strFileName))."';";
            //Suppression du fichier XYZ
            $strCommande .= " rm -rf '".$toDirectory."/".$strFileName."/".$strFileName.".xyz';";
        }
        if($fdlogFile = fopen($this->strFolderPath."/queue.log", "a+")) {
            fwrite($fdlogFile, sprintf("[%s] Commande: ".$strCommande."\n", date("Y-m-d G:i:s")));
            fclose($fdlogFile);
        }
        
        // exécute la commande de conversion
        $tabError = array(); $returnVal = 0;
        self::$controller->get('logger')->debug(__METHOD__, array("cmd"=>$strCommande));
        exec($strCommande. ' 2>&1', $tabError, $returnVal);

        if($returnVal != 0) {
            // écrit dans le fichier de log administrateur
            if($fdlogFile = fopen($this->strFolderPath."/queue.log", "a+")) {
                fwrite($fdlogFile, sprintf("[%s] Erreur lors de la génération:  ".$strCommande."\n", date("Y-m-d G:i:s")));
                fwrite($fdlogFile, implode(PHP_EOL, $tabError));
                fclose($fdlogFile);
            }
            // génère un fichier d'erreur dans le répertoire des données (visible pour l'utilisateur)
            if($fdlogFile = fopen($toDirectory."/erreur.log", "w+")) {
                fwrite($fdlogFile, sprintf("[%s] Erreur lors de la génération: \n", date("Y-m-d G:i:s")));
                fwrite($fdlogFile, implode(PHP_EOL, $tabError));
                fclose($fdlogFile);
            }
        } else {/*DEBUG*/
//             // génère un fichier d'erreur dans le répertoire des données (visible pour l'utilisateur)
//             if($fdlogFile = fopen($toDirectory."/execution.log", "w+")) {
//                 fwrite($fdlogFile, sprintf("[%s] SUCCESS:  ".$strCommande."\n", date("Y-m-d G:i:s")));
//                 fclose($fdlogFile);
//             }
        }
    }

    /**************************************************************************************************************************
     * @abstract lance le traitement d'une donnée de type table                                                               *
     * @param $strMainTeleDir   nom du répertoire principal des données                                                       *
     * @param data              nom de la donnée en entrée (nom du fichier ou nom de la table)                                *
     * @param metadata_id       identifiant de la métadonnée associée aux données                                             *
     * @param format            format de sortie (extension sans le ".")                                                      *
     * @param pgConn            chaîne qui contient les paramètres de connexion à la base (format ogr avec doubles quotes)    *
     * @param meta_file         emplacement du fichier de métadonnées                                                         *
     **************************************************************************************************************************/
    protected function treat_data_table($strMainTeleDir,  $data, $metadata_id, $format, $pgConn, $meta_file) {
        
        $strCommande = OGR_PATH.' -lco ENCODING="UTF-8" ';
        $tabError = array();
        
        $fileName =$data;
        $originalExtension = "Postgis";
        
        $strDirectory = $fileName."_".time()."_".mt_rand(1000,9999);
        switch(strtolower($format)) {
          case "csv":
              $strFileName = $fileName.".csv ";
              $strCommande .=  "-f \"CSV\" ".DIR_DOWNLOAD."/".$strMainTeleDir."/".$strDirectory."/".$strFileName;
              break;
          case "ods":
              $strFileName = $fileName.".ods ";
              $strCommande .=  "-f \"ODS\" ".DIR_DOWNLOAD."/".$strMainTeleDir."/".$strDirectory."/".$strFileName;
              break;
          case "xlsx":
              $strFileName = $fileName.".xlsx ";
              $strCommande .=  "-f \"XLSX\" ".DIR_DOWNLOAD."/".$strMainTeleDir."/".$strDirectory."/".$strFileName;
              break;
        }
        
        $strCommande .=  $pgConn;
        
          
        //$tabColumnsInfo = $this->dbPgSql->getTableColumns($data);
        $tabColumnsInfo = $this->getTableColumns($data);
        $strTableFields = "";
        $glue = "";
        for($i=0; $i<sizeof($tabColumnsInfo); $i++) {
            if($tabColumnsInfo[$i]["field"] != "gid") {
                $strTableFields .= $glue."public.".$data.".\\\"".($tabColumnsInfo[$i]["field"])."\\\"";
                $glue = ",";
            }
        }
        $strOrder = "";
        $strCommande .= " -sql \"select distinct ".( $strTableFields ? $strTableFields : "*" )." from public.".$data.$strOrder." \"";
        
        @mkdir(DIR_DOWNLOAD."/".$strMainTeleDir."/".$strDirectory, 0777, true);
        
        // ajoute un fichier d'information
        if(defined("ADD_METADATA_FILE") && ADD_METADATA_FILE) {
            $strText = "La couche ";
            $strText .= "provient ";
            if($typeInput == Queue::$VECTOR_POSTGIS) {
                $strText.= "de la table ";
            } else {
                $strText.= "du fichier géographique ";
            }
            $strText.= $fileName;
            $strText.= " au format ".$originalExtension.".\r\n";
            $strText.= "Pour le téléchargement vous avez choisi le format ".strtoupper($format);
        
            $tabProjection = explode(":", $projection);
            if(array_key_exists($tabProjection[1], $tabEPSG)) {
                $mapProjection = $tabEPSG[$tabProjection[1]]." (".$projection.")";
            }
            $strText .= " et le système de référence spatiale".$mapProjection.".\r\n";
            $strText .= "Nous attirons votre attention sur le fait que le fichier de métadonnées fourni dans le zip de téléchargement correspond au fichier géographique d'origine de la carte.".
                "\r\nIl est de votre ressort de mettre à jour ou compléter les métadonnées avec les informations décrites dans le présent document.";
        
            if($fdmetadataFile = fopen(DIR_DOWNLOAD."/".$strMainTeleDir."/".$strDirectory."/info_donnees_telechargement.txt", "w") ) {
                fwrite($fdmetadataFile, utf8_decode($strText));
                fclose($fdmetadataFile);
            }
        }
        // ajoute le fichier de métadonnée (passé en paramètre selon l'ancienne méthode)
        if(file_exists($meta_file) && is_file($meta_file)) {
            copy($meta_file, DIR_DOWNLOAD."/".$strMainTeleDir."/".$strDirectory."/".$meta_file);
        }
        // ajoute les fichiers de la métadonnée
        $this->add_metadata_files($metadata_id, DIR_DOWNLOAD."/".$strMainTeleDir."/".$strDirectory, $fileName);
        
        if($fdlogFile = fopen($this->strFolderPath."/queue.log", "a+")) {
            fwrite($fdlogFile, sprintf("[%s] Commande: ".$strCommande."\n", date("Y-m-d G:i:s")));
            fclose($fdlogFile);
        }
        // exécute la commande de conversion
        passthru($strCommande, $tabError);
        
        if($tabError != 0) {
            // écrit dans le fichier de log administrateur
            if($fdlogFile = fopen($this->strFolderPath."/queue.log", "a+")) {
                fwrite($fdlogFile, sprintf("[%s] Erreur lors de la génération:  ".$strCommande."\n", date("Y-m-d G:i:s")));
                fclose($fdlogFile);
            }
            // génère un fichier d'erreur dans le répertoire des données (visible pour l'utilisateur)
            if($fdlogFile = fopen(DIR_DOWNLOAD."/".$strMainTeleDir."/".$strDirectory."/erreur.log", "w+")) {
                fwrite($fdlogFile, "Erreur lors de la génération LE ".date("Ymd G:i:s")."\n");
                fclose($fdlogFile);
            }
        }


    }

    /**
     * lance la génération d'un fichier ods à partir d'une table de données
     * @param $odsFile : chemin complet vers le fichier ods de sortie
     * @param $data : nom de la table à exporter
     * @return boolean : true si l'export s'est bien passé, false sinon
     */
    protected function buildOdsFile($file, $data){
        ini_set('memory_limit', '1024M');

        $pgNumericTypePattern = '/(.*int.*)|(.*float.*)|(.*real.*)|(.*numeric.*)|(.*decimal.*)|(.*serial.*)|(.*double precision.*)/';
        $object = newOds();
        $tabColumnsInfo = $this->dbPgSql->getTableColumns($data);
        $typeCol = array();
        $tabCol = array();
        if ( !empty($tabColumnsInfo) ) {
            // first line
            for($i=0; $i<sizeof($tabColumnsInfo); $i++){
                if($tabColumnsInfo[$i]["field"] != "gid"){
                    array_push($tabCol,$tabColumnsInfo[$i]["field"]);
                    if (preg_match($pgNumericTypePattern, $tabColumnsInfo[$i]["type"]>0))
                        array_push($typeCol, 'float');
                        else
                            array_push($typeCol, 'string');
                }
            }
            for ($i=0; $i<count($tabCol); $i++) {
                $object->addCell(0,0,$i,$tabCol[$i],'string');
            }

            // data
            $strListFields = implode(',', $tabCol);
            $strSql = "SELECT ".$strListFields." FROM ".$data.";";
            $this->dbPgSql->setSchema("public");
            $res = $this->dbPgSql->getResult($strSql, true);
            $iCount = pg_num_rows($res);
            if($iCount > 0) {
                for($i=0; $i<$iCount; $i++) {
                    $list = pg_fetch_array($res);
                    foreach($list as $key => $value){
                        if (is_numeric($key)){
                            $object->addCell(0,1+$i,$key,$value,$typeCol[$key]);
                        }
                    }
                    
                    unset($list);
                }
            }
            saveOds($object,$file);
            return true;
        }
        return false;
    }

    /**
     * lance la génération d'un fichier xls à partir d'une table de données
     * @param $odsFile : chemin complet vers le fichier ods de sortie
     * @param $data : nom de la table à exporter
     * @return boolean : true si l'export s'est bien passé, false sinon
     */
    protected function buildXlsFile($file, $data){
        ini_set('memory_limit', '1024M');
        set_time_limit(0);

        $pgNumericTypePattern = '/(.*int.*)|(.*float.*)|(.*real.*)|(.*numeric.*)|(.*decimal.*)|(.*serial.*)|(.*double precision.*)/';

        $workbook = new Workbook($file);
        $worksheet =& $workbook->add_worksheet('Sheet1');

        $tabColumnsInfo = $this->dbPgSql->getTableColumns($data);
        $typeCol = array();
        $tabCol = array();
        if ( !empty($tabColumnsInfo) ) {
            // first line
            for($i=0; $i<sizeof($tabColumnsInfo); $i++){
                if($tabColumnsInfo[$i]["field"] != "gid"){
                    array_push($tabCol,$tabColumnsInfo[$i]["field"]);
                    if (preg_match($pgNumericTypePattern, $tabColumnsInfo[$i]["type"]>0))
                        array_push($typeCol, 'float');
                        else
                            array_push($typeCol, 'string');
                }
            }
            $strListFields ="";
    			  // type of column not used for excel output...
    			  for ($i=0; $i<count($tabCol); $i++) {
        				$worksheet->write(0, $i, $tabCol[$i]);
        				$strListFields.="\"".$tabCol[$i]."\", ";
    			  }
    			  $strListFields = substr($strListFields,0, -2);
    			// data
    			//$strListFields = implode(',', $tabCol);

            // data
            $strListFields = implode(',', $tabCol);
            $strSql = "SELECT ".$strListFields." FROM ".$data.";";
            $this->dbPgSql->setSchema("public");
            $res = $this->dbPgSql->getResult($strSql, true);
            $iCount = pg_num_rows($res);
            if($iCount > 0) {
                for($i=0; $i<$iCount; $i++) {
                    $list = pg_fetch_array($res);
                    foreach($list as $key => $value){
                        if (is_numeric($key))
                            $worksheet->write(1+$i,$key,$value);
                    }
                    unset($list);
                }
            }
            $workbook->close();
            return true;
        }
        return false;
    }

    /**
     * lance la génération d'un fichier csv à partir d'une table de données
     * @param $csvFile
     * @param $data
     * @return boolean
     */
    protected function buildCsvFile($csvFile, $data){
        $fp = fopen($csvFile, 'w');
        $tabColumnsInfo = $this->dbPgSql->getTableColumns($data);
        if ( !empty($tabColumnsInfo) ) {
            $strListFields ="";
            for($i=0; $i<sizeof($tabColumnsInfo); $i++){
                if($tabColumnsInfo[$i]["field"] != "gid"){
                    $strListFields.= '"'.$tabColumnsInfo[$i]["field"].'", ';
                    $tableFields[]=$tabColumnsInfo[$i]["field"];
                }
            }
            $strListFields = substr($strListFields, 0, -2);
            fputcsv($fp, $tableFields);

            $strSql = "SELECT ".$strListFields." FROM ".$data.";";

            $this->dbPgSql->setSchema("public");

            $res = $this->dbPgSql->getResult($strSql, true);
            $iCount = pg_num_rows($res);
            if( $iCount > 0 ) {
                for($i=0; $i<$iCount; $i++) {
                    $list = pg_fetch_array($res);
                    $tabData = array();
                    foreach($list as $key => $value){
                        if(is_numeric($key))
                            $tabData[]=$value;
                    }
                    fputcsv($fp, $tabData);
                }
            }
            fclose($fp);
            return true;
        }
        return false;
    }

    /***************************************************************************************************************************
     * @abstract convertit une chaine du type EPSG:xxxx en un identifiant entier du type xxxx
     * @param str 	chaîne décrivant la projection
     **************************************************************************************************************************/
    private function projectionStr2Id($str) {
        $res = -1;
        $pos = strpos($str,":");
        if($pos === false)
            $res = intval($res);
        else
            $res = intval(substr($str,$pos+1));
        return $res;
    }

    /******************************************************************************************************************************
     * @abstract lance le traitement d'une donnée                                                                                 *
     * @param $strMainTeleDir   nom du répertoire principal des données                                                           *
     * @param typeInput         type de données en entrée (0 = Fichier, 1 = PostGIS)                                              *
     * @param data              nom de la donnée en entrée (nom du fichier ou nom de la table)                                    *
     * @param metadata_id       identifiant de la métadonnée associée aux données                                                 *
     * @param format            format de sortie (extension sans le ".")                                                          *
     * @param projection        projection de sortie (EPSG:****), si chaîne vide alors -t_srs et -a_srs ne seront pas renseignés  *
     * @param pgConn            chaîne qui contient les paramètres de connexion à la base (format ogr avec doubles quotes)        *
     * @param meta_file         emplacement du fichier de métadonnées                                                             *
     * @param tabEPSG           tableau de projections (srid => projection)                                                       *
     * @param bTerritoire       booléen indiquant si le téléchargement se fait sur un (des) territoire(s) (1 : oui, 0 : non)      *
     * @param territoire_type   identifiant de la division administrative                                                         *
     * @param territoire_data   chaîne des identifiants des territoires (séparés par "%")                                         *
     * @param extract_area      geometrie de découpage (format WKT, projection EPSG:$PRO_IMPORT_EPSG                              *
     * @param buffer            tolérance d'extraction                                                                            *
     * @param restricted_area_field champs de restriction territorial des données (dans la table prodige_perimetre )              *
     * @param territoire_area   territoires de restriction territorial des données                                                *
     ******************************************************************************************************************************/
    protected function treat_data_raster($strMainTeleDir, $typeInput, $data, $metadata_id, $format, $projection, $pgConn, $meta_file, $tabEPSG, $bTerritoire, $territoire_type, $territoire_data, $buffer, $extract_area, $dataAreaLimit, $dataColorInterp) {
        //global $PRO_IMPORT_EPSG;
        //global $PRO_LICENCE_ECW;

        $epsgCode = $this->projectionStr2Id($projection);

        $strSql = "SELECT st_xmin(T.the_geom) as xmin, st_ymin(T.the_geom) as ymin, st_xmax(T.the_geom) as xmax, st_ymax(T.the_geom) as ymax " .
                  //"FROM (SELECT st_expand(box2d(st_geometryfromtext('" . $extract_area ."'," . PRO_IMPORT_EPSG . ")),	" . $buffer . ") as the_geom) T;";
                  "FROM (SELECT st_expand(box2d(st_geometryfromtext(?, ?)), ?) as the_geom) T;";
        //$this->dbPgSql->setSchema("public");
        $this->conn->exec('set search_path to public');
        //$tabExtent = $this->dbPgSql->execute($strSql);
        $tabExtent = $this->conn->fetchAll($strSql, array($extract_area, PRO_IMPORT_EPSG, $buffer));

        if(!empty($tabExtent)) {
            // 2 steps :
            // 1 - generating vrt file with gdalwarp for reprojection
            // 2 - extraction and conversion from vrt with gdal_translate

            $fileName = basename($data, ".vrt") . "_extract";
            $strDirectory = $fileName."_".time()."_".mt_rand(1000,9999);
            $path = DIR_DOWNLOAD."/".$strMainTeleDir."/".$strDirectory."/";
            @mkdir($path, 0777, true);
            $fileName1 = basename($data, ".vrt") . "_reproj" .  ".vrt";

            // 1
            $strCmd1 = Queue::$GDAL_WARP_CMD;
            $strCmd1 = str_replace("%GDAL_WARP_PATH%", GDAL_WARP_PATH, $strCmd1);
            //static private $GDAL_WARP_CMD = "%GDAL_WARP_PATH% -of VRT -srs %PROJ_IN% -t_srs %PROJ_OUT% %FILE_IN% %FILE_OUT%";

            // raster file are supposed to be originally in epsg:$PRO_IMPORT_EPSG
            $strCmd1 = str_replace("%PROJ_IN%",  "epsg:".PRO_IMPORT_EPSG, $strCmd1);
            $strCmd1 = str_replace("%PROJ_OUT%",  "epsg:".$epsgCode, $strCmd1);
            $strCmd1 = str_replace("%FILE_IN%", $data, $strCmd1);
            $strCmd1 = str_replace("%FILE_OUT%", $path.$fileName1, $strCmd1);

            // 2
            //$fileName = basename($data, ".vrt") . "_extract";
            //$strDirectory = $fileName."_".time()."_".mt_rand(1000,9999);

            $strCmd = Queue::$GDAL_TRANSLATE_CMD;
            $strCmd = str_replace("%GDAL_TRANSLATE_PATH%", GDAL_TRANSLATE_PATH, $strCmd);


            $extractAreaExtent = array_merge(array(), $tabExtent[0]);
            //re adjusting limit of extraction area to fit data area limits if necessary
            if($dataAreaLimit != null) {
                $extractAreaExtent["xmin"] = max($extractAreaExtent["xmin"], $dataAreaLimit[0]);
                $extractAreaExtent["ymin"] = max($extractAreaExtent["ymin"], $dataAreaLimit[3]);
                $extractAreaExtent["xmax"] = min($extractAreaExtent["xmax"], $dataAreaLimit[2]);
                $extractAreaExtent["ymax"] = min($extractAreaExtent["ymax"], $dataAreaLimit[1]);

                $strExtentComputed = $extractAreaExtent["xmin"].",".
                                     $extractAreaExtent["ymax"].",".
                                     $extractAreaExtent["xmax"].",".
                                     $extractAreaExtent["ymin"];
                    $strExtentData = implode(",", $dataAreaLimit);
                    $strExtentRequest = $tabExtent[0]["xmin"].",".
                                        $tabExtent[0]["ymax"].",".
                                        $tabExtent[0]["xmax"].",".
                                        $tabExtent[0]["ymin"];

                        // add a notification to user if the extent request fall outside the data src extent
                        if ($strExtentComputed != $strExtentRequest) {
                            // ajout d'une tolérance d'un centimètre pour éviter les problèmes liés aux arrondis
                            $extractAreaExtent["xmin"] = $extractAreaExtent["xmin"] + 1;
                            $extractAreaExtent["xmax"] = $extractAreaExtent["xmax"] - 1;
                            $extractAreaExtent["ymin"] = $extractAreaExtent["ymin"] + 1;
                            $extractAreaExtent["ymax"] = $extractAreaExtent["ymax"] - 1;

                            // écrit dans le fichier de log administrateur
                            if($fdlogFile = fopen($this->strFolderPath."/queue.log", "a+")) {
                                fwrite($fdlogFile, sprintf("[%s] Les limites de la zone demandée (".$strExtentRequest.") débordent des limites du raster (".$strExtentData.").".
                                                   "Les limites demandées ont été ajustées à (".$strExtentComputed.")\n", date("Y-m-d G:i:s")));
                                fclose($fdlogFile);
                            }
                            // génère un fichier d'erreur dans le répertoire des données (visible pour l'utilisateur)
                            if(!file_exists($path))
                                @mkdir($path, 0777, true);
                            if($fdlogFile = fopen($path."/erreur.log", "w+")) {
                                fwrite($fdlogFile, "Les limites de la zone demandée (" . $strExtentRequest . ") débordent des limites du raster (" . $strExtentData . ")." .
                                                   "Les limites demandées ont été ajustées à (". $strExtentComputed .") Le ".date("Ymd G:i:s")."\n");
                                fclose($fdlogFile);
                            }
                        }
            }
            // reprojecting extraction coordinates to fit destinarion data projection
            $strSql_reproj = "SELECT st_xmin(T.the_geom) as xmin, st_ymin(T.the_geom) as ymin, st_xmax(T.the_geom) as xmax, st_ymax(T.the_geom) as ymax " .
                             "FROM (SELECT box2d(st_transform(st_geometryfromtext(" .
                             /*"POLYGON ((" .  $extractAreaExtent["xmin"] . " " .  $extractAreaExtent["ymin"] . "," .
                             $extractAreaExtent["xmin"] . " " .  $extractAreaExtent["ymax"] . "," .
                             $extractAreaExtent["xmax"] . " " .  $extractAreaExtent["ymax"] . "," .
                             $extractAreaExtent["xmax"] . " " .  $extractAreaExtent["ymin"] . "," .
                             $extractAreaExtent["xmin"] . " " .  $extractAreaExtent["ymin"] . "))" .
                             "'," . $PRO_IMPORT_EPSG . ")," . $epsgCode . ")) as the_geom) T;";*/
                             "'POLYGON (( '|| :lower_left  ||', ".
                                       "  '|| :upper_left  ||', ".
                                       "  '|| :upper_right ||', ".
                                       "  '|| :lower_right ||', ".
                                       "  '|| :lower_left  ||'))', :import_srid), :export_srid)) as the_geom) T" ;

                //$this->dbPgSql->setSchema("public");
                $this->conn->exec('set search_path to public');
                //$tabExtentReproj = $this->dbPgSql->execute($strSql_reproj);
                
                $tabExtentReproj = $this->conn->fetchAll($strSql_reproj, array(
                        "lower_left"  => $extractAreaExtent["xmin"]." ".$extractAreaExtent["ymin"],
                        "upper_left"  => $extractAreaExtent["xmin"]." ".$extractAreaExtent["ymax"],
                        "upper_right" => $extractAreaExtent["xmax"]." ".$extractAreaExtent["ymax"],
                        "lower_right" => $extractAreaExtent["xmax"]." ".$extractAreaExtent["ymin"],
                        "import_srid" => PRO_IMPORT_EPSG,
                        "export_srid" => $epsgCode));

                if(empty($tabExtentReproj)) {
                    // écrit dans le fichier de log administrateur
                    if($fdlogFile = fopen($this->strFolderPath."/queue.log", "a+")) {
                        fwrite($fdlogFile, sprintf("[%s] Erreur dans la reprojection des limites d'extraction\n", date("Y-m-d G:i:s")));
                        fclose($fdlogFile);
                    }
                    // génère un fichier d'erreur dans le répertoire des données (visible pour l'utilisateur)
                    if(!file_exists($path))
                        @mkdir($path, 0777, true);
                    if($fdlogFile = fopen($path."/erreur.log", "w+")) {
                        fwrite($fdlogFile, "Erreur dans la reprojection des limites d'extraction Le ".date("Ymd G:i:s")."\n");
                        fclose($fdlogFile);
                    }
                }
                else {
                    // Carefull uly =ymax and not ymin
                    $strCmd = str_replace("%GDAL_PROJ_WIN%", ($tabExtentReproj[0]["xmin"] . " " . $tabExtentReproj[0]["ymax"] . " " . $tabExtentReproj[0]["xmax"] . " " . $tabExtentReproj[0]["ymin"])  ,$strCmd);
                    $strCmd = str_replace("%FILE_IN%", $path . $fileName1  ,$strCmd);

                    $strFileName = "";
                    $strWorldFileName = "";
                    switch(strtolower($format)) {
                        case "gtiff":
                            $strFileName =  $path. $fileName.".tiff ";
                            $strWorldFileName = $path. $fileName.".tfw";
                            $formatOption = "GTIFF -co TFW=YES -co BIGTIFF=YES";
                            break;
                        case "ecw":
                            $strFileName =  $path. $fileName.".ecw ";
                            //TODO hismail - PRO_LICENCE_ECW introuvable 
                            $PRO_LICENCE_ECW = "off"; // Pour tester
                            $formatOption = "ECW".($PRO_LICENCE_ECW == "on" ? " -co LARGE_OK=YES" : "");
                            // when paletted color, force expansion to true color (3 bands) for ECW
                            if($dataColorInterp == 0)
                                $formatOption .= " -expand rgb";
                            break;
                        case "jpeg":
                        default:
                            $strFileName = $path.$fileName.".jpg ";
                            $strWorldFileName = $path.$fileName.".wld";
                            $formatOption = "JPEG -co WORLDFILE=YES";
                            if($dataColorInterp == 0)
                                $formatOption .= " -expand rgb";
                            break;
                    }

                    $strCmd = str_replace("%FILE_OUT%", $strFileName  ,$strCmd);
                    $strCmd = str_replace("%FORMAT_OPTION%", $formatOption  ,$strCmd);

                    // executing commands 1 the 2
                    // creating data extraction directory
                    if(!file_exists($path))
                        @mkdir($path, 0777, true);

                        // 1 : creating vrt file for reprojection...
                        $str = exec($strCmd1, $tabRes, $resVal);
                        if($resVal != 0 ) {
                            // écrit dans le fichier de log administrateur
                            if($fdlogFile = fopen($this->strFolderPath."/queue.log", "a+")) {
                                fwrite($fdlogFile, sprintf("[%s] Erreur lors de la génération du fichier de reprojection raster :  ".$strCmd1."\n", date("Y-m-d G:s")));
                                fclose($fdlogFile);
                            }
                            // génère un fichier d'erreur dans le répertoire des données (visible pour l'utilisateur)
                            if($fdlogFile = fopen(DIR_DOWNLOAD."/".$strMainTeleDir."/".$strDirectory."/erreur.log", "w+")) {
                                fwrite($fdlogFile, "Erreur lors de la génération du fichier de reprojection raster le ".date("Ymd G:i:s")."\n");
                                fclose($fdlogFile);
                            }
                        }
                        else {
                            // 2 : creating raster extract file
                            exec($strCmd, $tabRes, $resVal);
                            //echo $strCmd . "\n" . $resVal ;
                            if($resVal != 0) {
                                // écrit dans le fichier de log administrateur
                                if($fdlogFile = fopen($this->strFolderPath."/queue.log", "a+")) {
                                    fwrite($fdlogFile, sprintf("[%s] Erreur lors de la génération du fichier d'extraction raster :  ".$strCmd."\n", date("Y-m-d G:i")));
                                    fclose($fdlogFile);
                                }
                                // génère un fichier d'erreur dans le répertoire des données (visible pour l'utilisateur)
                                if($fdlogFile = fopen(DIR_DOWNLOAD."/".$strMainTeleDir."/".$strDirectory."/erreur.log", "w+")) {
                                    fwrite($fdlogFile, "Erreur lors de la génération du fichier d'extraction raster le ".date("Ymd G:i:s")."\n");
                                    fclose($fdlogFile);
                                }
                            }
                            else {
                                // removing temporary vrt file
                                unlink($path . $fileName1);
                                // in case of JPG format, rename world file extension from .wld to .jpgw
                                if(strtolower($format) == "jpeg" && file_exists($strWorldFileName)) {
                                    $strJpgwFilename = str_replace(".wld",".jpgw", $strWorldFileName);
                                    rename($strWorldFileName, $strJpgwFilename);
                                }
                            }
                        }
                }
        }
        //////////////////////////////////
        // Partie metadonnées
        // ajoute un fichier d'information
        if(defined("ADD_METADATA_FILE") && ADD_METADATA_FILE) {
            $strText  = "La couche raster " . $fileName . " que vous venez de télecharger est une extraction d'un fichier raster qui a subi une éventuelle reprojection et conversion de format\n";
            $strText .= "Pour le téléchargement vous avez choisi le format ".strtoupper($format);

            $tabProjection = explode(":", $projection);
            if(array_key_exists($tabProjection[1], $tabEPSG)) {
                $mapProjection = $tabEPSG[$tabProjection[1]]." (".$projection.")";
            }
            $strText .= " et le système de référence spatiale".$mapProjection.".\r\n";
            $strText .= "Nous attirons votre attention sur le fait que le fichier de métadonnées fourni dans le zip de téléchargement correspond au fichier géographique d'origine de la carte.".
                        "\r\nIl est de votre ressort de mettre à jour ou compléter les métadonnées avec les informations décrites dans le présent document.";

            if($fdmetadataFile = fopen($path . "/info_donnees_telechargement.txt", "w")) {
                fwrite($fdmetadataFile, utf8_decode($strText));
                fclose($fdmetadataFile);
            }
        }
        // ajoute le fichier de métadonnée (passé en paramètre selon l'ancienne méthode)
        if(file_exists($meta_file) && is_file($meta_file)) {
            copy($meta_file, $path ."/".$meta_file);
        }
        // ajoute les fichiers de la métadonnée
        $this->add_metadata_files($metadata_id, $path, $fileName);
    }

    /******************************************************************************************************************************
     * @abstract lance le traitement d'une donnée                                                                                 *
    * @param $strMainTeleDir   nom du répertoire principal des données                                                           *
    * @param typeInput         type de données en entrée (0 = Fichier, 1 = PostGIS)                                              *
    * @param data              nom de la donnée en entrée (nom du fichier ou nom de la table)                                    *
    * @param metadata_id       identifiant de la métadonnée associée aux données                                                 *
    * @param format            format de sortie (extension sans le ".")                                                          *
    * @param projection        projection de sortie (EPSG:****), si chaîne vide alors -t_srs et -a_srs ne seront pas renseignés  *
    * @param pgConn            chaîne qui contient les paramètres de connexion à la base (format ogr avec doubles quotes)        *
    * @param meta_file         emplacement du fichier de métadonnées                                                             *
    * @param tabEPSG           tableau de projections (srid => projection)                                                       *
    * @param bTerritoire       booléen indiquant si le téléchargement se fait sur un (des) territoire(s) (1 : oui, 0 : non)      *
    * @param territoire_type   identifiant de la division administrative                                                         *
    * @param territoire_data   chaîne des identifiants des territoires (séparés par "%")                                         *
    * @param extract_area      geometrie de découpage (format WKT, projection EPSG:$PRO_IMPORT_EPSG                              *
    * @param buffer            tolérance d'extraction                                                                            *
    * @param restricted_area_field champs de restriction territorial des données (dans la table prodige_perimetre )              *
    * @param territoire_area   territoires de restriction territorial des données                                                *
    ******************************************************************************************************************************/
    protected function treat_data_raster_hr($strMainTeleDir, $typeInput, $data, $metadata_id, $format, $projection, $pgConn, $meta_file, $tabEPSG, $bTerritoire, $territoire_type, $territoire_data, $buffer, $extract_area, $dataAreaLimit, $dataColorInterp) {
        global $PRO_IMPORT_EPSG;
        global $PRO_LICENCE_ECW;
        global $PRO_SITE_URL;
    
        // Recherche du fichier pour extraire la ou les tuiles
        $files = $this->catalogueConn->fetchAll("select couchd_emplacement_stockage from catalogue.couche_donnees inner join catalogue.fiche_metadonnees on (pk_couche_donnees=fmeta_fk_couche_donnees) where fmeta_id=:fmeta_id", array("fmeta_id"=>$metadata_id));
        foreach ($files as $file) {
            $file = $file["couchd_emplacement_stockage"];
            	
            switch ($format){
              case "ECW":{
                  $pathInfo=pathinfo($file);
                  $fileName=$pathInfo["filename"];

                  $path_shp=URL_PATH_DATA."/cartes/Publication/".$pathInfo["dirname"]."/".$fileName.".";

                  $fileNamedest = basename($data, ".vrt") . "_extract";
                  $strDirectory = $fileNamedest."_".time()."_".mt_rand(1000,9999);
                  	
                  $dest = DIR_DOWNLOAD."/".$strMainTeleDir."/";
                  $path= DIR_DOWNLOAD."/".$strMainTeleDir."/".$strDirectory."/";

                  $cmd="cp %path%* %dest%";
                  $cmd=str_replace("%path%",$path_shp,$cmd);
                  $cmd=str_replace("%dest%",$dest,$cmd);

                  exec($cmd);
                  break;
              }
              case 'TILED':
                  $path_shp="";
    
                  $strSql = "SELECT st_xmin(T.the_geom) as xmin, st_ymin(T.the_geom) as ymin, st_xmax(T.the_geom) as xmax, st_ymax(T.the_geom) as ymax " .
                      "FROM (SELECT expand(box2d(st_geometryfromtext('" . $extract_area ."'," . $PRO_IMPORT_EPSG . ")),	" . $buffer . ") as the_geom) T;";
    
                  $this->dbPgSql->setSchema("public");
                  $tabExtent = $this->dbPgSql->execute($strSql);
    
                  //vérification et asjutement de la zone de sélection
                  if ( !empty($tabExtent) ) {
                      $fileName = basename($data, ".vrt") . "_extract";
                      $strDirectory = $fileName."_".time()."_".mt_rand(1000,9999);
                      $path = DIR_DOWNLOAD."/".$strMainTeleDir."/".$strDirectory."/";
                      	
                      $extractAreaExtent = array_merge(array(), $tabExtent[0]);
                      //re adjusting limit of extraction area to fit data area limits if necessary
                      if ($dataAreaLimit!=null) {
                          $extractAreaExtent["xmin"] = max($extractAreaExtent["xmin"], $dataAreaLimit[0]);
                          $extractAreaExtent["ymin"] = max($extractAreaExtent["ymin"], $dataAreaLimit[3]);
                          $extractAreaExtent["xmax"] = min($extractAreaExtent["xmax"], $dataAreaLimit[2]);
                          $extractAreaExtent["ymax"] = min($extractAreaExtent["ymax"], $dataAreaLimit[1]);
    
                          $strExtentComputed = $extractAreaExtent["xmin"] . "," .
                              $extractAreaExtent["ymax"] . "," .
                              $extractAreaExtent["xmax"] . "," .
                              $extractAreaExtent["ymin"];
                          $strExtentData = implode(",", $dataAreaLimit);
                          $strExtentRequest = $tabExtent[0]["xmin"] . "," .
                              $tabExtent[0]["ymax"] . "," .
                              $tabExtent[0]["xmax"] . "," .
                              $tabExtent[0]["ymin"];
    
                          // add a notification to user if the extent request fall outside the data src extent
                          if ($strExtentComputed != $strExtentRequest) {
                              // ajout d'une tolérance d'un centimètre pour éviter les problèmes liés aux arrondis
                              $extractAreaExtent["xmin"] = $extractAreaExtent["xmin"]+1;
                              $extractAreaExtent["xmax"] = $extractAreaExtent["xmax"]-1;
                              $extractAreaExtent["ymin"] = $extractAreaExtent["ymin"]+1;
                              $extractAreaExtent["ymax"] = $extractAreaExtent["ymax"]-1;
                              	
                              // écrit dans le fichier de log administrateur
                              if ( $fdlogFile = fopen($this->strFolderPath."/queue.log", "a+") ) {
                                  fwrite($fdlogFile, "Les limites de la zone demandée (" . $strExtentRequest . ") débordent des limites du raster (" . $strExtentData . ")." .
                                      "Les limites demandées ont été ajustées à (". $strExtentComputed .") Le ".date("Ymd G:i:s")."\n");
                                  fclose($fdlogFile);
                              }
                              // génère un fichier d'erreur dans le répertoire des données (visible pour l'utilisateur)
                              if (!file_exists($path))
                                  @mkdir($path, 0777, true);
                              if ( $fdlogFile = fopen($path."/erreur.log", "w+") ) {
                                  fwrite($fdlogFile, "Les limites de la zone demandée (" . $strExtentRequest . ") débordent des limites du raster (" . $strExtentData . ")." .
                                      "Les limites demandées ont été ajustées à (". $strExtentComputed .") Le ".date("Ymd G:i:s")."\n");
                                  fclose($fdlogFile);
                              }
                          }
                      }
                  }
                  $path_shp = URL_PATH_DATA."/cartes/Publication/".$file;
                  $cmd="ogrinfo -al -spat %spat% ".$path_shp;
                  $cmd=str_replace("%spat%", ($extractAreaExtent["xmin"] . " " . $extractAreaExtent["ymax"] . " " . $extractAreaExtent["xmax"] . " " . $extractAreaExtent["ymin"]), $cmd);
                  $tabOutput=array();
                  if ( $fdlogFile = fopen($this->strFolderPath."/queue.log", "a+") ) {
                      fwrite($fdlogFile, $cmd);
                  }
                  exec($cmd,$tabOutput);

                  foreach ($tabOutput as $key => $val){
                      if(substr_count($val,"location")==1){
                          $tabsplit=split("=",$val);
                          if(count($tabsplit)==2){
                              $url_file=$tabsplit[1];
                              $url_file=str_replace(" ","",$url_file);
                              $dest = DIR_DOWNLOAD."/".$strMainTeleDir."/";//."/".$fileName;
                              $src = pathinfo($url_file)["dirname"]."/".pathinfo($url_file)["filename"];

                              // copy all files, not only image file
                              $cmd="cp %path%* %dest%";
                              $cmd=str_replace("%path%",$src,$cmd);
                              $cmd=str_replace("%dest%",$dest,$cmd);
                              exec($cmd);
                              	
                              //$result = copy($url_file, $dest);
                              /*if(!$result){
                               // génère un fichier d'erreur dans le répertoire des données (visible pour l'utilisateur)
                              if (!file_exists($path))
                                  mkdir($path, 0777);
                              if ( $fdlogFile = fopen($path."/erreur.log", "w+") ) {
                              fwrite($fdlogFile, "problème pour copier le fihcier ".$val."\n");
                              fclose($fdlogFile);
                              }
                              }*/
                          }
                      }
                  }
                  $cmd="cd ".DIR_DOWNLOAD."/".$strMainTeleDir."/ && gdaltindex emprise.shp *.ecw";
                  if ( $fdlogFile = fopen($this->strFolderPath."/queue.log", "a+") ) {
                      fwrite($fdlogFile, $cmd);
                  }
                  exec($cmd);
    
                  break;
            }
        }
        	
        //////////////////////////////////
        // Partie metadonnées
        // ajoute un fichier d'information
        // ajoute les fichiers de la métadonnée
        @mkdir(DIR_DOWNLOAD."/".$strMainTeleDir."/".$strDirectory, 0777, true);
    
        if ( defined("ADD_METADATA_FILE") && ADD_METADATA_FILE ) {
            $strText = "La couche raster " . $fileName . " que vous venez de télecharger est une extraction d'un fichier raster qui a subi une éventuelle reprojection et conversion de format\n";
            $strText.= "Pour le téléchargement vous avez choisi le format ".strtoupper($format);
    
            $tabProjection = explode(":", $projection);
            if ( array_key_exists($tabProjection[1], $tabEPSG) ) {
                $mapProjection = $tabEPSG[$tabProjection[1]]." (".$projection.")";
            }
            $strText.=" et le système de référence spatiale".$mapProjection.".\r\n";
            $strText.="Nous attirons votre attention sur le fait que le fichier de métadonnées fourni dans le zip de téléchargement correspond au fichier géographique d'origine de la carte.".
                "\r\nIl est de votre ressort de mettre à jour ou compléter les métadonnées avec les informations décrites dans le présent document.";
            	
            if ( $fdmetadataFile = fopen($path . "/info_donnees_telechargement.txt", "w") ) {
                fwrite($fdmetadataFile, utf8_decode($strText));
                fclose($fdmetadataFile);
            }
        }
        // ajoute le fichier de métadonnée (passé en paramètre selon l'ancienne méthode)
        if ( file_exists($meta_file) && is_file($meta_file) ) {
            copy($meta_file, $path ."/".$meta_file);
        }
        // ajoute les fichiers de la métadonnée
        $this->add_metadata_files($metadata_id, $path, $fileName);//*/
    }
    /**************************************************************************************************************************
     * @abstract lance le traitement d'une donnée                                                                             *
     * @param $strMainTeleDir   nom du répertoire principal des données                                                       *
     * @param data              nom de la donnée en entrée (nom du fichier ou nom de la table)                                *
     * @param metadata_id       identifiant de la métadonnée associée aux données                                             *
     * @param format            format de sortie (extension sans le ".")                                                      *
     **************************************************************************************************************************/
    protected function treat_data_URL($strMainTeleDir, $data, $metadata_id, $format, $meta_file) {

        $tabError = array();
        $parts = parse_url($data);

        parse_str($parts['query'], $query);
        if(isset($query["typeName"]))
            $fileName = $query["typeName"];
        else
            $fileName = "download";

        $strDirectory = $fileName."_".time()."_".mt_rand(1000,9999);
        $path = DIR_DOWNLOAD."/".$strMainTeleDir."/".$strDirectory."/";
        @mkdir(DIR_DOWNLOAD."/".$strMainTeleDir."/".$strDirectory, 0777, true);

        $contentFile = @file_get_contents($data);
        if($contentFile) {

            $filePath = $path.$fileName.".".$format;
            if($file = fopen($filePath, "w+")) {
                fwrite($file, $contentFile);
                fclose($file);
            }
            else {
                $tabError[] = "Impossible d'ecrire le fichier à partir de l'URL.\n";
            }
        } else {
            $tabError[] = "Impossible de récuéperer le contenu de l'URL.\n";
        }

        if(!empty($tabError)) {
            // écrit dans le fichier de log administrateur
            if($fdlogFile = fopen($this->strFolderPath."/queue.log", "a+")) {
                fwrite($fdlogFile, sprintf("[%s] Erreur lors de la génération:  ".implode(" ",$tabError)."\n", date("Y-m-d G:i:s")));
                fclose($fdlogFile);
            }
            // génère un fichier d'erreur dans le répertoire des données (visible pour l'utilisateur)
            if($fdlogFile = fopen(DIR_DOWNLOAD."/".$strMainTeleDir."/".$strDirectory."/erreur.log", "w+")) {
                fwrite($fdlogFile, "Erreur lors de la génération LE ".date("Ymd G:i:s")."\n");
                fclose($fdlogFile);
            }
        } else {
            //////////////////////////////////
            // Partie metadonnées
            // ajoute le fichier de métadonnée (passé en paramètre selon l'ancienne méthode)
            if(file_exists($meta_file) && is_file($meta_file)) {
                copy($meta_file, $path ."/".$meta_file);
            }
            // ajoute les fichiers de la métadonnée
            $this->add_metadata_files($metadata_id, $path, $fileName);
        }
    }

    /**************************************************************************************************************************
     * @abstract lance le traitement d'une donnée                                                                             *
     * @param $strMainTeleDir   nom du répertoire principal des données                                                       *
     * @param typeInput         type de données en entrée (0 = Fichier, 1 = PostGIS)                                          *
     * @param data              nom de la donnée en entrée (nom du fichier ou nom de la table)                                *
     * @param metadata_id       identifiant de la métadonnée associée aux données                                             *
     * @param format            format de sortie (extension sans le ".")                                                      *
     * @param projection        projection de sortie (EPSG:****)                                                              *
     * @param pgConn            chaîne qui contient les paramètres de connexion à la base (format ogr avec doubles quotes)    *
     * @param meta_file         emplacement du fichier de métadonnées                                                         *
     * @param tabEPSG           tableau de projections (srid => projection)                                                   *
     * @param bTerritoire       booléen indiquant si le téléchargement se fait sur un (des) territoire(s) (1 : oui, 0 : non)  *
     * @param territoire_type   identifiant de la division administrative                                                     *
     * @param territoire_data   chaîne des identifiants des territoires (séparés par "%")                                     *
     * @param extract_area      geometrie de découpage (format WKT, projection EPSG:$PRO_IMPORT_EPSG)                         *
     * @param buffer            tolérance d'extraction                                                                        *
     **************************************************************************************************************************/
    protected function treat_data_majic($strMainTeleDir, $data, $metadata_id, $meta_file,  $territoire_data, $dataPath) {
        $tabError = array();
        $path = DIR_DOWNLOAD."/".$strMainTeleDir."/";
        $rootPath  = URL_PATH_DATA.$dataPath."Publication/majic/".$metadata_id;
        if(is_dir($rootPath)) {
            $tabInsee = explode("¤", $territoire_data);
            //parcours des répertoires insee
            foreach($tabInsee as $key => $insee) {
                if(is_dir($rootPath."/".$insee)) {
                    $directory = opendir($rootPath."/".$insee);
                    while($Entry = readdir($directory)) {
                        if($Entry!="." && $Entry != "..") {
                            if(file_exists($path.$Entry)) {
                                //fusion des fichiers
                                $strError= $this->addFileMajic($path, $rootPath."/".$insee."/", $Entry);
                                if($strError != "") {
                                    $tabError[] = $strError;
                                }
                            } else {
                                //copie du premier fichier
                                copy($rootPath."/".$insee."/".$Entry, $path.$Entry);
                            }
                        }
                    }
                } else {
                    //$tabError[] = "le répertoire ".$insee." n'existe pas.\n";
                }
            }
            // conversion "passage  la ligne" unix -> windows
            /*if ( $fdlogFile = fopen($this->strFolderPath."/queue.log", "a+") ) {
            fwrite($fdlogFile, "MAJIC : traitement du repertoire  " . $path . "  LE ".date("Ymd G:i:s")."\n");
            fclose($fdlogFile);
            }
            $directory = opendir($path);
            while($Entry = readdir($directory)) {
            if($Entry!="." && $Entry!=".." && $Entry!="export_full_metadata.zip" && $Entry!="majic.pdf" && $Entry!="specif_file_not_defined.txt"){
            if (file_exists($path.$Entry)){
            $cmd = "sed -i -e 's/$/\r/g' " . $path.$Entry;
            $resCmd = exec($cmd);
            if ( $fdlogFile = fopen($this->strFolderPath."/queue.log", "a+") ) {
            fwrite($fdlogFile, "MAJIC : fichier  " . $Entry . " traite \n");
            fclose($fdlogFile);
            }
            }
            }
            }*/
        }
        else {
            $tabError[] = "le répertoire ".$rootPath." n'existe pas.\n";
        }
        if(!empty($tabError)) {
            // écrit dans le fichier de log administrateur
            if($fdlogFile = fopen($this->strFolderPath."/queue.log", "a+")) {
                fwrite($fdlogFile, sprintf("[%s] Erreur lors de la génération:  ".implode(" ",$tabError)."\n", date("Y-m-d G:i:s")));
                fclose($fdlogFile);
            }
            // génère un fichier d'erreur dans le répertoire des données (visible pour l'utilisateur)
            if($fdlogFile = fopen(DIR_DOWNLOAD."/".$strMainTeleDir."/".$strDirectory."/erreur.log", "w+")) {
                fwrite($fdlogFile, "Erreur lors de la génération LE ".date("Ymd G:i:s")."\n");
                fclose($fdlogFile);
            }
        } else {
            //////////////////////////////////
            // Partie metadonnées
            // ajoute le fichier de métadonnée (passé en paramètre selon l'ancienne méthode)
            if(file_exists($meta_file) && is_file($meta_file)) {
                copy($meta_file, $path ."/".$meta_file);
            }
            // ajoute les fichiers de la métadonnée
            $this->add_metadata_files($metadata_id, $path, "majic");
        }
    }

    /**
     *
     * @param $path chemin de copie du fichier
     * @param $rootPath chemin d'origine du fichier
     * @param $Entry nom du fichier origine
     * @return liste d'errreurs
     */
    protected function addFileMajic($path, $rootPath, $Entry) {
        $strError = "";
        if($inputFile = fopen($rootPath.$Entry, "r+")) {
            $strInput = fread($inputFile,  filesize($rootPath.$Entry));
            fclose($inputFile);
            if($outputFile = fopen($path.$Entry, "a+")) {
                fwrite($outputFile, $strInput);
                fclose($outputFile);
            } else {
                $strError .= "impossible d'ouvrir le fichier ".$path.$Entry.".\n";
            }
        } else {
            $strError .= "impossible d'ouvrir le fichier ".$rootPath.$Entry.".\n";
        }
        return $strError;
    }

    /***************************************************************************************************************************************************************************************************
     * @abstract crée et retourne une requête de clippage à partir d'un territoire décrit par un objet d'une couche (travaille sur le schema "public")                                                 *
     * @param tablename               nom de la table des données                                                                                                                                      *
     * @param buffer                  valeur du buffer d'extraction                                                                                                                                    *
     * @param strFonctionName         nom de la fonction utilisée pour filtrer les objets                                                                                                              *
     *                                st_intersects (défaut) : conserve les données attributaires de tous les objets qui intersectent le territoire (la géométrie des objets correspond à l'intersection) *
     *                                st_within              : conserve uniquement les objets qui sont entièrement à l'intérieur du territoire                                                            *
     * @param tableTerritoireName     nom de la table qui contient les territoires                                                                                                                     *
     * @param champTerritoireIdName   nom du champ identifiant de la table des territoires                                                                                                             *
     * @param tabTerritoireId         tableau des identifiants du(des) territoire(s) dans la table des territoires                                                                                     *
     * @return chaîne qui contient la requête sql au format ogr, vide si paramètres manquants ou erronés                                                                                               *
     ***************************************************************************************************************************************************************************************************/
    protected function createSqlClip($tableDataName="", $buffer=0, $strFonctionName="st_intersects", $tableTerritoireName="", $champTerritoireIdName="",
        $extractionWhere="", $tabTerritoireId=array(), $restricted_area_buffer =0, $bIntersectGeom=true, $input_type) {
            $strSql = "";
            if($buffer == "") //pour les extractions liées aux restrictions territoriales, buffer = ""
                $buffer = 1;
                // sort si un ou plusieurs paramètres sont manquants ou erronés

                if($tableDataName=="" || ($strFonctionName != "st_intersects" && $strFonctionName != "st_within") || $tableTerritoireName=="" || $champTerritoireIdName=="" || empty($tabTerritoireId)) {
                    return $strSql;
                }

                // crée une chaîne qui contient les identifiants des territoires séparés par une virgule
                $strTerritoireId = "'".implode("', '", $tabTerritoireId)."'";

                // récupère les champs de la table des données
                //$tabColumnsInfo = $this->dbPgSql->getTableColumns($tableDataName);
                $tabColumnsInfo = $this->getTableColumns($tableDataName);

                if(empty($tabColumnsInfo)) {
                    return $strSql;
                }
                $strTableFields="";
                for($i=0; $i<sizeof($tabColumnsInfo); $i++) {
                    if($tabColumnsInfo[$i]["field"] != "the_geom" && $tabColumnsInfo[$i]["field"] != "gid")
                        //$strTableFields.= "public.".$tableDataName."."./*utf8_encode*/($tabColumnsInfo[$i]["field"]).", ";
                        $strTableFields.= "public.".$tableDataName.".\\\""./*utf8_encode*/($tabColumnsInfo[$i]["field"])."\\\", ";
                }


                // crée la requête de sélection
                $strSql.= " select * from( ";
 
                if($bIntersectGeom) {
                    if($extractionWhere != "") {
                        //On enleve la derniere virgule
                        $strSql .= " SELECT ".$strTableFields."public.".$tableDataName.".the_geom";
                    } else {
                        $strSql.= " SELECT ".$strTableFields."st_multi(st_intersection(".
                        //cas POLYGON, nettoyage des géométries par buffer 0 avant découpage
                                  ($input_type == "MULTIPOLYGON" 
                                  ? "st_buffer(public.".$tableDataName.".the_geom,0)"
                                  : "public.".$tableDataName.".the_geom").
                                  ", (SELECT st_buffer(st_union(the_geom), ".$buffer.") FROM public.".$tableTerritoireName." WHERE ".$champTerritoireIdName." IN (".$strTerritoireId.")))) as intersection";
                    }
                } else {
                    $strSql.= " SELECT ".$strTableFields."public.".$tableDataName.".the_geom";
                }
                $strSql.= " FROM public.".$tableDataName;
                if($extractionWhere == "") {
                    $strSql.= " WHERE  public.".$tableDataName.".the_geom && (st_expand((SELECT st_union(the_geom) FROM public.".$tableTerritoireName." WHERE ".$champTerritoireIdName." IN (".$strTerritoireId.")), ".$buffer."))" .
                              " AND st_isvalid(public.".$tableDataName.".the_geom )= true ".
                              " AND ".$strFonctionName."(public.".$tableDataName.".the_geom, (SELECT st_buffer(st_union(the_geom), ".$buffer.") FROM public.".$tableTerritoireName." WHERE ".$champTerritoireIdName." IN (".$strTerritoireId.")))";
                } else {
                    $strSql.= " WHERE ".$extractionWhere;
                }
                $strSql.= " ) clip  ";
                if($bIntersectGeom && $extractionWhere == "") {
                    $strSql.= "where st_geometrytype(clip.intersection)<> 'GEOMETRYCOLLECTION'";
                }
                return $strSql;
    }

    /***************************************************************************************************************************************************************************************************
     * @abstract crée et retourne une requête de clippage à partir d'une geometrie wkt (travaille sur le schema "public")                                                                              *
     * @param tablename               nom de la table des données                                                                                                                                      *
     * @param buffer                  valeur du buffer d'extraction                                                                                                                                    *
     * @param strFonctionName         nom de la fonction utilisée pour filtrer les objets                                                                                                              *
     *                                st_intersects (défaut) : conserve les données attributaires de tous les objets qui intersectent le territoire (la géométrie des objets correspond à l'intersection) *
     *                                st_within              : conserve uniquement les objets qui sont entièrement à l'intérieur du territoire                                                            *
     * @param tableTerritoireName     nom de la table qui contient les territoires                                                                                                                     *
     * @param champTerritoireIdName   nom du champ identifiant de la table des territoires                                                                                                             *
     * @param tabTerritoireId         tableau des identifiants du(des) territoire(s) dans la table des territoires                                                                                     *
     * @return chaîne qui contient la requête sql au format ogr, vide si paramètres manquants ou erronés                                                                                               *
     ***************************************************************************************************************************************************************************************************/
    protected function createSqlClipFromWkt($tableDataName="", $buffer=0, $strFonctionName="st_intersects", $wktGeom="", $input_type) {
        //global $PRO_IMPORT_EPSG;
        $strSql = "";

        // sort si un ou plusieurs paramètres sont manquants ou erronés
        if(!$tableDataName || ($strFonctionName != "st_intersects" && $strFonctionName != "st_within") || $wktGeom == "") {
            return $strSql;
        }

        // récupère les champs de la table des données
        //$this->dbPgSql->setSchema("public");
        $this->conn->exec('set search_path to public');
        //$tabColumnsInfo = $this->dbPgSql->getTableColumns($tableDataName);
        $tabColumnsInfo = $this->getTableColumns($tableDataName);

        if(empty($tabColumnsInfo)) {
            return $strSql;
        }
        $strTableFields="";
        for($i=0; $i<sizeof($tabColumnsInfo); $i++) {
            if($tabColumnsInfo[$i]["field"] != "the_geom" && $tabColumnsInfo[$i]["field"] != "gid")
                //$strTableFields.= "public.".$tableDataName."."./*utf8_encode*/($tabColumnsInfo[$i]["field"]).", ";
                $strTableFields.= "public.".$tableDataName.".\\\""./*utf8_encode*/($tabColumnsInfo[$i]["field"])."\\\", ";
        }

        // crée la requête de sélection
        $epsg = PRO_IMPORT_EPSG;
        $geomMask = "st_buffer(st_geomfromtext('" . $wktGeom . "'," . $epsg . ")," . $buffer . ")";

        $strSql.= " select * from( ";
        $strSql.= " SELECT ".$strTableFields."st_multi(st_intersection(".
                  //cas POLYGON, nettoyage des géométries par buffer 0 avant découpage
                 ($input_type == "MULTIPOLYGON" 
                 ? "st_buffer(public.".$tableDataName.".the_geom,0)"
                 : "public.".$tableDataName.".the_geom").
                 ", " . $geomMask . ")) as intersection";
            $strSql.= " FROM public.".$tableDataName;
            $strSql.= " WHERE public.".$tableDataName.".the_geom && st_expand(st_geomfromtext('" . $wktGeom . "'," . $epsg . ")," . $buffer . ")" .
                      " AND st_isvalid(st_buffer(public.".$tableDataName.".the_geom,0) )= true ".
                      " AND ".$strFonctionName."(public.".$tableDataName.".the_geom, ". $geomMask . ")";
            $strSql.= " ) clip  where st_geometrytype(clip.intersection)<> 'GEOMETRYCOLLECTION'";

            return $strSql;
    }
    
    /**
     * return info on metadata
     * @param unknown $metadataId
     * @return string
     */
    public function getMetadataInfo($metadataId){
        if ( isset($this->metadataInfo[$metadataId]) ) return $this->metadataInfo[$metadataId];
        
        if(!ctype_digit($metadataId)){
            return false;
        }
        $strSql = /*"set search_path to public;" .*/
        " SELECT m.id, m.uuid, m.data FROM metadata as m" .
        " WHERE m.id=?";
        
        //$dao = new DAO();
        $conn = $this->catalogueConn;
        $result = array();
        $tabMetadataInfo = $conn->fetchAll($strSql, array($metadataId));
        if(!empty($tabMetadataInfo)) {
            $result['success']        = true;
            $result['metadata_id']    = $tabMetadataInfo[0]["id"];
            $result['metadata_uuid']  = ($tabMetadataInfo[0]["uuid"]);

            //On ne remplace pas les caracteres html contenus dans le xml (probleme de decodage du xml sinon notamment avec les chevrons < et >)
            $result['metadata_data']	= ($tabMetadataInfo[0]["data"]);

            /*$strUrl = PRO_GEONETWORK_URLBASE."srv/fre/xml.relation?type=fcat&fast=false&id=".$rs->Read(0);
            
            $xmlDoc = new DOMDocument('1.0', 'UTF-8');

            $strXml = $gntw->get($strUrl);
            if ( $xmlDoc->loadXML($strXml) === TRUE ) {
                $relations = $xmlDoc->getElementsByTagName('relation');
                if($relations->length>0){
                    for ( $i=0; $i<$relations->length; $i++ ) {
                        if ( $relations->item($i)->hasAttribute('type') && $relations->item($i)->getAttribute('type') =="fcat" ) {
                            $fcatMetadata = $relations->item($i)->getElementsByTagName("id")->item(0)->nodeValue;
                            $strSqlCat = "set search_path to public;" .
                            " SELECT c.id, c.uuid, c.data FROM metadata as c" .
                            //" LEFT JOIN relations as r ON m.id=r.id" .
                            //" LEFT JOIN metadata as c ON r.relatedid=c.id" .
                            " WHERE c.id=".$fcatMetadata;
                            $rsCat = $dao->BuildResultSet($strSqlCat);
                            if ( $rsCat->GetNbRows() > 0 ) {
                                $rsCat->First();
                                if ( !$rsCat->EOF() ) {
                                    $result['catalogue_id']   = ($rs->Read(0));
                                    $result['catalogue_uuid'] = ($rs->Read(1));
                                    $result['catalogue_data'] = ($rs->Read(2));
                                }
                            }
                        }
                    }
                }
            }*/
        }
        
        $this->metadataInfo[$metadataId] = $result;
        return $result;
    }

    /*********************************************************************************************
     * @abstract ajoute les fichiers liés à une métadonnée (PDF, ZIP, Spécification)             *
     *           récupère les informations de la métadonnée par un appel au service du catalogue *
     * @param metadata_id   identifiant de la métadonnée                                         *
     * @param dir           répertoire des données
     * @param filename      nom du fichier ou de la table Postgis                                        *
     * @return les fichiers relatifs à la métadonnée sont ajoutés                                *
     *********************************************************************************************/
    protected function add_metadata_files($metadata_id=null, $dir, $fileName) {
        //global $PRO_GEONETWORK_DIRECTORY;
        //global $PRO_SITE_URL;
        if($metadata_id) {

            // récupère les informations de la métadonnée
            
            $tabMetadataInfo = $this->getMetadataInfo($metadata_id);

            if(!empty($tabMetadataInfo) && $tabMetadataInfo['success']) {
                // ajoute le fichier pdf de la métadonnée
                /*
                $url = "http://".$PRO_SITE_URL."/". $PRO_GEONETWORK_DIRECTORY."/srv/fr/pdf?id=".$metadata_id;
                $pdfContents = @file_get_contents($url);
                if ( $pdfFile = @fopen($dir."/".$fileName.".pdf", "w") ) {
                fwrite($pdfFile, $pdfContents);
                fclose($pdfFile);
                }
                */
                // ajoute l'export PDF complet de la métadonnée
                //$url = "http://".$PRO_SITE_URL."/PRRA/Services/getMetadataPDFComplet.php?uuid=".($tabMetadataInfo['metadata_uuid']);
                //$zipPdfContents = @file_get_contents($url);
                $context = self::$controller->createUnsecureSslContext();
                $zipPdfContents = file_get_contents(CARMEN_URL_SERVER_DOWNLOAD."/metadata/pdf?uuid=".($tabMetadataInfo['metadata_uuid']), false, $context);
                if($zipPdfFile = @fopen($dir."/PDF_Complet_".$fileName.".zip", "w")) {
                    fwrite($zipPdfFile, $zipPdfContents);
                    fclose($zipPdfFile);
                }

                // ajoute le zip
                
                
                $geonetwork = self::$controller->getGeonetworkInterface();
                
                $exportZipContent = $geonetwork->get("mef.export?uuid=".($tabMetadataInfo['metadata_uuid'])."&format=full&version=2");
                if($exportZipFile = fopen($dir."/export_full_metadata.zip", "w")) {
                    fwrite($exportZipFile, $exportZipContent);
                    fclose($exportZipFile);
                }
                // ajoute le fichier de spécification
                $_metadata_XMLData = $metadata_XMLData = ($tabMetadataInfo['metadata_data']);
                $metadata_XMLData = preg_replace('/\s\s+/', ' ', $metadata_XMLData);
                $metadata_XMLData = preg_replace('/>\s+</', '><', $metadata_XMLData);
                $metadata_XMLData = str_replace(array("\r","\n","\t"), "", $metadata_XMLData);
                //$metadata_XMLData = str_replace("&", "&amp;", $metadata_XMLData);
                $metadata_XMLData = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>".$metadata_XMLData;

                $bSpecifFound = false;
                $dom = new \DomDocument();
                if($dom->loadXML($metadata_XMLData)) {
                    $identificationInfo = $dom->getElementsByTagName("identificationInfo")->item(0);
                    if($identificationInfo) {
                        $otherCitationDetails = $identificationInfo->getElementsByTagName("otherCitationDetails")->item(0);
                        if($otherCitationDetails) {
                            $CharacterString = $otherCitationDetails->getElementsByTagName("CharacterString")->item(0);
                            if($CharacterString) {
                                $linkSpecifFile = $CharacterString->nodeValue;
                                if($linkSpecifFile) {
                                    if(substr($linkSpecifFile, 0, 4) != "http") {
                                        $linkSpecifFile = "http://".PRO_SITE_URL."/".$linkSpecifFile;
                                    }
                                    $contentSpecifFile = @file_get_contents($linkSpecifFile);
                                    if($contentSpecifFile) {
                                        if(($fname = $this->get_url_param_fname($linkSpecifFile)) != "") {
                                            $specifFileName = $fname;
                                        } elseif(($fname = substr($linkSpecifFile,strrpos($linkSpecifFile, "/")+1)) != "" ) {
                                            $specifFileName = $fname;
                                        } else {
                                            $specifFileName = "specification.txt";
                                        }
                                        if($specifFile = fopen($dir."/".$specifFileName, "w")) {
                                            fwrite($specifFile, $contentSpecifFile);
                                            fclose($specifFile);
                                            $bSpecifFound = true;
                                        }
                                    } else {
                                        /*if ( $specifFile = fopen($dir."/specif_file_not_found.txt", "w") ) {
                                         fwrite($specifFile, utf8_decode("Le fichier de spécification n'a pas été détecté à l'adresse : ".$linkSpecifFile."\n\n"));
                                         fclose($specifFile);
                                         $bSpecifFound = true;
                                         }*/
                                    }
                                } else {
                                    /*if ( $specifFile = fopen($dir."/specif_file_not_defined.txt", "w") ) {
                                     fwrite($specifFile, utf8_decode("Aucun lien vers le fichier de spécification n'est définie dans la métadonnée.\n\n"));
                                     fclose($specifFile);
                                     $bSpecifFound = true;
                                     }*/
                                }
                            }
                        }
                    }

                    /*if ( !$bSpecifFound ) {
                     if ( $specifFile = fopen($dir."/specif_link_not_found.txt", "w") ) {
                     fwrite($specifFile, utf8_decode("Aucun lien vers le fichier de spécification n'a été trouvé dans la métadonnée.\n\n").$_metadata_XMLData);
                     fclose($specifFile);
                     }
                     }*/
                } else {
                    if($erreur_xml = fopen($dir."/erreur_metadata_parsing.txt", "w")) {
                        fwrite($erreur_xml, utf8_decode("Erreur de parsing du XML de la métadonnée.\n\n")/*.$metadata_XMLData*/);
                        fclose($erreur_xml);
                    }
                }

                // impossible de récupérer les informations de la métadonnée
            } else {
                /*if($erreur_file = fopen($dir."/erreur_metadata.txt", "w")) {
                    fwrite($erreur_file, utf8_decode("Impossible de récupérér les informations relatives à la métadonnée."));
                    fclose($erreur_file);
                }*/
            }
        }
    }

    /**********************************************************************
     * retourne la valeur du paramètre fname d'une url                    *
     * @param url       url                                               *
     * @return string   valeur du paramètre fname s'il existe, vide sinon *
     **********************************************************************/
    protected function get_url_param_fname($url) {
        $res = "";
        $tabUrlParam = explode("?", $url);
        if(array_key_exists(1, $tabUrlParam)) {
            $strParam = $tabUrlParam[1];
            $tabParam = explode("&", $strParam);
            foreach($tabParam as $param) {
                $tabParamNameValue = explode("=", $param);
                if($tabParamNameValue[0] == "fname") {
                    $res = $tabParamNameValue[1];
                }
            }
        }
        return $res;
    }

    /**
     * Suppression des vues temporaires
     * @param $tabData tableau des données
     * @return unknown_type
     */
    protected function CleanData($tabData) {

        foreach($tabData as $key => $tableName) {
            $strSql = "SELECT pk_view FROM prodige_view_info WHERE temporary is not null and view_name = ?";
            //$this->dbPgSql->setSchema("parametrage");
            $this->conn->exec('set search_path to parametrage');
            //$tabRes = $this->dbPgSql->execute($strSql);
            $tabRes = $this->conn->fetchAll($strSql, array($tableName));
            if(!empty($tabRes)) {
                $pk_view = $tabRes[0]["pk_view"];
                $ar_strSQL = array();
                $ar_strSQL[] = "DELETE FROM prodige_computed_field_info WHERE pk_view=:pk_view";
                $ar_strSQL[] = "DELETE FROM prodige_join_info WHERE pk_view=:pk_view";
                $ar_strSQL[] = "DELETE FROM prodige_view_info WHERE pk_view=:pk_view";
                $ar_strSQL[] = "DELETE FROM public.geometry_columns WHERE f_table_name=:pk_view and f_table_schema='public'; ";
                $ar_strSQL[] = "DROP VIEW IF EXISTS \"public".$tableName."\";";
                //$this->dbPgSql->setSchema("parametrage");
                $this->conn->exec('set search_path to parametrage');
                //$this->dbPgSql->execute($strSQL2);
                foreach($ar_strSQL as $strSql) {
                    $this->conn->executeQuery($strSql, array("pk_view"=>$pk_view));
                }
            }
        }
    }

    /*******************************************************************************************************************************
     * crée une archive "zip" des dossiers/sous-dossiers et fichiers contenus dans un répertoire                                   *
     * @param rootSourceDirPath     chemin de la racine jusqu'au dossier à archiver (ne contient pas le nom du dossier à archiver) *
     * @param sourceDirName         nom du répertoire à archiver                                                                   *
     * @param rootZipPath           chemin de la racine jusqu'au fichier zip (contient le nom du fichier zip)                      *
     * @param zip                   objet zipArchive si déjà existant (optionnel, utile pour appel récursif)                       *
     * @param zipRelativePath       chemin relatif à l'interieur de l'archive (optionnel, utile pour appel récursif)               *
     * @param tabFilesIgnore        tableaux des fichiers à ignorer                                                                *
     * @return true si l'archive a bien été créée, false sinon                                                                     *
     *******************************************************************************************************************************/
    protected function zipDirToFile($rootSourceDirPath, $sourceDirName, $rootZipPath, $zip=null, $zipRelativePath="", $tabFilesIgnore=array()){

        if($zip == null) {
            $zip = new ZipArchive();
        }
        if($zipRelativePath == "") {
            if($zip->open($rootZipPath, ZIPARCHIVE::CREATE) !== TRUE ) {
                return false;
            }
        }

        if($zip->addEmptyDir($zipRelativePath.$sourceDirName) == FALSE) {
            return false;
        }

        $dir = opendir($rootSourceDirPath."/".$sourceDirName);
        while($entry = @readdir($dir)) {
            if(is_dir($rootSourceDirPath."/".$sourceDirName."/".$entry) && $entry != "." && $entry != ".." ) {
                $res = $this->zipDirToFile($rootSourceDirPath."/".$sourceDirName, $entry, $rootZipPath, $zip, $zipRelativePath.$sourceDirName."/");
                if(!$res) {
                    closedir($dir);
                return false;
                }
            } else if($entry != "." && $entry != ".." && !in_array($entry, $tabFilesIgnore)) {
                $zip->addFile($rootSourceDirPath."/".$sourceDirName."/".$entry, $zipRelativePath.$sourceDirName."/".$entry);
            }
        }
        closedir($dir);

        if($zipRelativePath == "") {
            $zip->close();
        }

        return $sourceDirName.".zip";
    }

    /**
     *
     * @param $rootSourceDirPath
     * @param $sourceDirName
     * @param $rootTgzPath
     * @return unknown_type
     */
    protected function tarGzDirToFile($rootSourceDirPath, $sourceDirName, $rootTgzPath) {

        $cmd = "cd ".$rootSourceDirPath." &&  tar czf ".$rootTgzPath." ".$sourceDirName;
        exec($cmd, $tabError);
        if($tabError != 0) {
            // écrit dans le fichier de log administrateur
            if($fdlogFile = fopen($this->strFolderPath."/queue.log", "a+")) {
                fwrite($fdlogFile, sprintf("[%s] Erreur lors de la génération:  ".$cmd."\n", date("Y-m-d G:i:s")));
                fclose($fdlogFile);
            }
        }
        return $sourceDirName.".tar.gz";
    }

    /**
     * fonction de generation d'un mot de passe
     * @param $len
     * @return unknown_type
     */
    protected function gen_md5_password($len = 12)
    {
        // function calculates 32-digit hexadecimal md5 hash
        // of some random data
        return substr(md5(rand().rand()), 0, $len);
    }

    /*******************************************************************************************************************************
     * crée une archive "zip" des dossiers/sous-dossiers et fichiers contenus dans un répertoire                                   *
     * @param rootSourceDirPath     chemin de la racine jusqu'au dossier à archiver (ne contient pas le nom du dossier à archiver) *
     * @param sourceDirName         nom du répertoire à archiver                                                                   *
     * @param rootZipPath           chemin de la racine jusqu'au fichier zip (contient le nom du fichier zip)                      *
     * @param zip                   objet zipArchive si déjà existant (optionnel, utile pour appel récursif)                       *
     * @param zipRelativePath       chemin relatif à l'interieur de l'archive (optionnel, utile pour appel récursif)               *
     * @param tabFilesIgnore        tableaux des fichiers à ignorer                                                                *
     * @return true si l'archive a bien été créée, false sinon                                                                     *
     *******************************************************************************************************************************/
    protected function zipEncryptDirToFile($rootSourceDirPath, $sourceDirName, $rootZipPath, $zip=null, $zipRelativePath="", $tabFilesIgnore=array()){

        $strPassword = $this->gen_md5_password();
        $strCmd = "zip -r -P ".$strPassword." ".$rootZipPath." ".$rootSourceDirPath."/".$sourceDirName;

        $str = exec($strCmd, $tabRes, $resVal);
        if($resVal != 0) {
            // écrit dans le fichier de log administrateur
            if($fdlogFile = fopen($this->strFolderPath."/queue.log", "a+")) {
                fwrite($fdlogFile, sprintf("[%s] Erreur lors de la génération du cryptage du fichier zip\n", date("Y-m-d G:i")));
                fclose($fdlogFile);
                return false;
            }
        }
        return $strPassword;
    }

    /****************************************************************
     * @abstract envoi un mail pour signaler la fin d'un traitement *
     * @param email     adresse mail du destinataire                *
     * @param urlZip    url du fichier zip à télécharger            *
     * @param strPassword  mot de passe à communiquer dans un mail séparé*
     * @return le mail est envoyé                                   *
     ****************************************************************/
    protected function send_mail($email, $urlZip, $strPassword="") {
        $url1 = ($strPassword != "" && defined("SECURE_URL_DOWNLOAD") ? str_replace(URL_SERVER_DOWNLOAD, SECURE_URL_DOWNLOAD, $urlZip) : $urlZip);
        $msg  = "Bonjour,<br><br>";
        $msg .= "Le téléchargement demandé est accessible à l'URL suivante : <br>".
                "<a href=\"".$url1."\">".$url1."</a><br><br>";
        $msg .= "Cette URL sera accessible durant 10 jours.";
        $msg .= "<br><br>Ce message est généré automatiquement. Merci de ne pas répondre.";

        $headers  = "From: \"".utf8_decode(TELE_CARTO_NAME."\"<".(defined("PRO_MAIL_ADRESS") ? PRO_MAIL_ADRESS : ADMINISTRATOR_MAIL)).">\n";
        $headers .= "Reply-To: ".(defined("PRO_MAIL_ADRESS") ? PRO_MAIL_ADRESS : ADMINISTRATOR_MAIL)."\n";
        $headers .= "Content-Type: text/html; charset=\"iso-8859-1\""."\n";
        $headers .= "Content-Transfer-Encoding: 8bit";
        $message  = "<html><head><title>".utf8_decode(TELE_CARTO_NAME)."</title></head><body>".utf8_decode($msg)."</body></html>";


        mail($email, TELE_CARTO_NAME, $message, $headers);
        if($strPassword != "") {
            $msgMdp     = "Bonjour,<br><br>";
            $msgMdp    .= "Les données que vous avez demandé sont protégées par le mot de passe suivant :<br>".$strPassword."<br>";
            $msgMdp    .= "<br><br>Ce message est généré automatiquement. Merci de ne pas répondre.";
            $messageMdp = "<html><head><title>".utf8_decode(TELE_CARTO_NAME)."</title></head><body>".utf8_decode($msgMdp)."</body></html>";
            mail($email, TELE_CARTO_NAME, $messageMdp, $headers);
        }
    }

    /**
     * Calculate the size of a directory by iterating its contents
     *
     */
    protected function dirsize($path)
    {
        $size = 0;
        $cmd = "du -s ".$path;
        exec($cmd, $tabError);
        if($tabError != 0) {
            $tabInfo = explode("\t", $tabError[0]);
            $size = $tabInfo[0];
        }
        return $size;
    }

}