<?php

namespace Prodige\ProdigeBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Prodige\ProdigeBundle\Services\ConfigReader;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

/**
 * 
 * @author alkante
 * 
 * @Route("/prodige")
 */
class VerifyRightsController extends BaseController {

    /**
     * URL de validation des requêtes CAS
     * 
     * @Route("/cas_validate", name="casValidate")
     * @return Response
     */
    public function casValidate(Request $request) {
        $user = User::GetUser();
        return new Response($this->isProd() ? 'OK' : $user->GetLogin());
    }

    /**
     * Permet de forcer une authentification inter-appli (geosource-prodige).
     * Cette route DOIT être sécurisée derrière le firewall CAS.
     * Si le paramètre GET 'casuser' est passé, force un logout si ce user ne correspond pas à celui de la session CAS courante.
     * 
     * @Route("/connect", name="auth_connect")
     * @return Response
     */
    public function connect(Request $request) {
        $casuser = $request->query->get('casuser', null);
        
        $user = User::GetUser();
        $this->getLogger()->info(__METHOD__, array($user->GetLogin(), $casuser));
        $session_id = $request->query->get('sessionid', null);
        
        if (isset($_SESSION["CONNECT_SESSION_ID"]) && null !== $casuser && $_SESSION["CONNECT_SESSION_ID"][0] !== $casuser) {
            //$user->getLoggerConnexions()->info(sprintf('[1] %s, Déconnexion demandée', $_SESSION["CONNECT_SESSION_ID"][0], $_SESSION["CONNECT_SESSION_ID"][1]));
            if ( $session_id ){
                $_SESSION["CONNECT_SESSION_ID"] = array($casuser, $session_id);
            } else {
                unset($_SESSION["CONNECT_SESSION_ID"]);
            }
        }
        if ( !isset($_SESSION["CONNECT_SESSION_ID"]) ){
            $_SESSION["CONNECT_SESSION_ID"] = array($casuser, uniqid(md5($casuser)));
            $user->getLoggerConnexions()->info(sprintf('[1] %s, Connexion réussie', $_SESSION["CONNECT_SESSION_ID"][0], $_SESSION["CONNECT_SESSION_ID"][1]));
        }
        
        $session_id = $request->query->get('sessionid', $_SESSION["CONNECT_SESSION_ID"][1]);
        $_SESSION["CONNECT_SESSION_ID"][1] = $session_id;
        $user->LogSessionUser();
        
    
        $server_host = (isset($_SERVER) ? $_SERVER["HTTP_HOST"] : "PHP CLI");
        
        // si demandé, vérifier si le casuser passé en paramètre est le même que celui connecté 
        if (null !== $casuser && PRO_USER_INTERNET_USR_ID === $casuser && $user->GetLogin() !== $casuser) {
            \phpCAS::logout();
            //throw $this->createAccessDeniedException(sprintf('User %s does not match', $this->isProd() ? '' : $user->GetLogin()));
        }
        
        /*
        $noBroadcast = $request->query->get('no-broadcast', false);
        // broadcaster la connexion aux applis qui ne forcent pas l'authentification par défaut
        $services = array(
            $this->getParameter('PRODIGE_URL_FRONTCARTO') . '/cas_validate',
            $this->getParameter('PRODIGE_URL_ADMINCARTO') . '/cas_validate',
        );
        foreach ($services as $url) {
            $this->curl($url, 'GET', array('no-broadcast'=>1));
        }
        */
        $callback = $request->query->get('callback', null);
        
        return new Response(($callback ?: "top.onSiteConnect")."(".json_encode(array(
            "success"=>true, 
            "connected"=>\phpCAS::isAuthenticated(), 
            "login"=>$this->isProd() ? 'OK' : $user->GetLogin(), 
            'sessionid'=>$session_id, 
            'index'=>$request->query->get('index', 0)+1
        )).($callback ? "" : ", '".$user->GetLogin()."'").")");
    }
    
    /**
     * @Route("/verify_rights_multiple", name="prodige_verify_rights_url_multiple", options={"expose"=true})
     * 
     * @see /mnt/devperso/bfontaine/prodige3.4/prodigecatalogue/PRRA/Services/getUserRights.php
     * 
     * TODO : 
     * - constantes et variables globales du début de fonction
     * - fin de la fonction avec complément du tabRes par le user
     * - tests et vérification de validité des traitements copiés/collés depuis prodige
     * - remplacement des callservices
     */
    public function verifyRightsMultipleAction(Request $request) {
    	$metadatas = $request->get("metadatas", array());
      $callback = $request->get("callback", "");
    	$results = array();
    	foreach($metadatas as $metadata){
    		if ( is_string($metadata) ) $metadata = json_decode($metadata, true) ?: $metadata;
    		if ( isset($results[$metadata["ID"]]) ) continue;
    		$request->query->set("ID", $metadata["ID"]);
    		$request->query->set("OBJET_TYPE", $metadata["OBJET_TYPE"]);
    		$request->query->set("OBJET_STYPE", $metadata["OBJET_STYPE"]);
    		$results[$metadata["ID"]] = $this->verifyRightsAction($request, true);;
    	}
        if ($callback != "")
            return new Response($callback . "(" . json_encode($results) . ")");
        else
            return new JsonResponse($results);
    }
    
   
    
    /**
     * 
     * @Route("/verify_rights", name="prodige_verify_rights_url", options={"expose"=true})
     * 
     * @see /mnt/devperso/bfontaine/prodige3.4/prodigecatalogue/PRRA/Services/getUserRights.php
     * 
     * TODO : 
     * - constantes et variables globales du début de fonction
     * - fin de la fonction avec complément du tabRes par le user
     * - tests et vérification de validité des traitements copiés/collés depuis prodige
     * - remplacement des callservices
     * 
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response|\Symfony\Component\HttpFoundation\JsonResponse
     */
    public function verifyRightsAction(Request $request, $resultAsJson=false) {
        $config_reader = $this->get('prodige.config_reader');
        $config_reader instanceof ConfigReader;

        defined("PRO_TRT_TYPE_EDITION") || define("PRO_TRT_TYPE_EDITION", "1");
        defined("PRO_OBJET_TYPE_COUCHE") || define("PRO_OBJET_TYPE_COUCHE", "1");

        $objectId = $request->get("ID", "");
        $traitements = $request->get("TRAITEMENTS", "");
        $objectType = $request->get("OBJET_TYPE", "");
        $objectSType = $request->get("OBJET_STYPE", "");
        $map = $request->get("map", "");
        $callback = $request->get("callback", "");

        $tabRes = array();

        $dao = $this->getDAO("catalogue", "public,catalogue");
        $conn = $dao->getConnection();
        $user = User::GetUser();
        //$user->initUser("manger");

        $rsCouche = null;
        $rsCarte = null;
        $rsService = null;

        $IsADM_PRODIGE = false;

        if (!is_null($user)) {
            if ($user->IsProdigeAdmin()) {
                $IsADM_PRODIGE = true;
            }
        }
        $tabTraitement = explode("|", $traitements);
        $tabTraitement = array_unique($tabTraitement);
        $tabTraitement = array_diff($tabTraitement, array(""));

        if($objectId==""){
            //afect objectId when service working with uuid
            $uuid = $request->get("uuid", "");
            if($uuid!=""){
                $rsMetadata = $dao->BuildResultSet("select id from metadata where uuid=:uuid", array("uuid"=>$uuid));
                if ($rsMetadata->GetNbRows() > 0) {
                    $rsMetadata->First();
                    $objectId = $rsMetadata->Read(0);
                }
            }
        }
        //TODO MLA à vérifier et changer par l'appel précédent
        //Si l'ID soumis correspond à l'UUID d'une métadonnée
        $rsMetadata = $dao->BuildResultSet("select id from metadata where uuid=:uuid", array("uuid"=>$objectId));
        if ($rsMetadata->GetNbRows() > 0) {
            $rsMetadata->First();
            $objectId = $rsMetadata->Read(0);
        }
        
        if($objectId!=""){
            // vérification d'affectation dom/sdom sur la métadonnée (ssdom_dispose_metadata)
            $tabFieldsMetadata = array("DOM_NOM", "SSDOM_NOM");
            $rsMetadata = $dao->BuildResultSet(
                "SELECT " . implode(", ", $tabFieldsMetadata) .
                " FROM  ssdom_dispose_metadata" .
                " inner join sous_domaine on ssdom_dispose_metadata.ssdcouch_fk_sous_domaine = sous_domaine.pk_sous_domaine ".
                " inner join domaine on sous_domaine. 	ssdom_fk_domaine = domaine.pk_domaine ".
                " WHERE uuid = (select uuid from metadata where id= :objectId)", array("objectId" => $objectId)
            );
            // indiquer dans la réponse si la métadonnée est bien affectée à un dom/sdom
            $tabRes['sdom_dispose_metadata'] = $rsMetadata->GetNbRows() > 0;
        }
        
        // construction des requêtes nécessaires aux traitements
        if (!empty($tabTraitement)) {
            $query_params = array("objectId" => $objectId);

            if ($objectType == "service") {
                if ($objectSType == "invoke") {
                    $tabFieldsCarte = array("DOM_NOM", "SSDOM_NOM",
                        "PK_CARTE_PROJET", "PK_STOCKAGE_CARTE", "FMETA_ID", "STATUT",
                        "CARTP_NOM", "CARTP_FORMAT",
                        "STKCARD_PATH", "ACCS_ADRESSE_ADMIN", "PATH_ADMINISTRATION",
                        "ACCS_LOGIN_ADMIN", "ACCS_PWD_ADMIN", "PK_FICHE_METADONNEES",
                        "CARTP_WMS"
                    );
                    $rsCarte = $dao->BuildResultSet("SELECT " . implode(", ", $tabFieldsCarte) . " FROM CARTES_SDOM WHERE FMETA_ID = :objectId", $query_params);
                } else {
                    $tabFieldsService = array("FMETA_ID", "STATUT", "PK_FICHE_METADONNEES");
                    $rsService = $dao->BuildResultSet(
                        "SELECT " . implode(", ", $tabFieldsService) .
                        " FROM FICHE_METADONNEES" .
                        " WHERE FMETA_ID = :objectId", $query_params
                    );
                }
            } elseif($objectType =="metadata") {
                // $rsMetadata déjà déclaré de manière globale
            } else {
                $tabFieldsCouche = array("DOM_NOM", "SSDOM_NOM",
                    "PK_COUCHE_DONNEES", "FMETA_ID", "STATUT",
                    "COUCHE_NOM", "COUCHE_TABLE", "COUCHE_TYPE", "COUCHE_SRV_ADMIN", "COUCHE_SERVICE_ADMIN",
                    "COUCHD_DOWNLOAD", "COUCHD_WMS", "COUCHD_WFS", "COUCHD_VISUALISABLE",
                    "ACCS_LOGIN_ADMIN", "ACCS_PWD_ADMIN", "PK_FICHE_METADONNEES"
                );
                $rsCouche = $dao->BuildResultSet(
                    "SELECT " . implode(", ", $tabFieldsCouche) .
                    " FROM COUCHE_SDOM" .
                    " LEFT JOIN V_ACCES_COUCHE ON V_ACCES_COUCHE.COUCHE_PK=COUCHE_SDOM.PK_COUCHE_DONNEES " .
                    " WHERE FMETA_ID = :objectId", $query_params
                );
            }
        }
        //TODO MIGRATION 3.4 => 4.0
        
            if (array_search("TELECHARGEMENT", $tabTraitement) !== FALSE) {
                $bAllow = false;
                
                if ($objectType == "dataset"  || $objectType == "nonGeographicDataset") {
                    //vérification du caractère téléchargement libre de la donnée
                    $query = 'SELECT couchd_download FROM couche_donnees WHERE pk_couche_donnees in (select fmeta_fk_couche_donnees from fiche_metadonnees where  FMETA_ID = :objectId)';
                    $rs = $dao->BuildResultSet($query, $query_params);
                    $rs->First();
                    $bFreeDownload = $rs->Read(0);
                    if ($rsCouche->GetNbRows() > 0) {
                        for ($rsCouche->First(); !$rsCouche->EOF(); $rsCouche->Next()) {
                            $domaine = $rsCouche->read(array_search("DOM_NOM", $tabFieldsCouche));
                            $sous_domaine = $rsCouche->read(array_search("SSDOM_NOM", $tabFieldsCouche));
                            $objetCouche = $rsCouche->read(array_search("PK_COUCHE_DONNEES", $tabFieldsCouche));
                            $acces_adress_admin = $rsCouche->read(array_search("COUCHE_SRV_ADMIN", $tabFieldsCouche));
                            $couche_table = $rsCouche->read(array_search("COUCHE_TABLE", $tabFieldsCouche));
                            $couche_type = $rsCouche->read(array_search("COUCHE_TYPE", $tabFieldsCouche));
                            $couche_statut = $rsCouche->read(array_search("STATUT", $tabFieldsCouche));

                            //vérification de l'autorisation sur le domaine et sur l'objet
                            $bAllow = $couche_statut==4 && $user->HasTraitement('TELECHARGEMENT', html_entity_decode($domaine, ENT_QUOTES, 'UTF-8'), html_entity_decode($sous_domaine, ENT_QUOTES, 'UTF-8'), PRO_TRT_TYPE_TELECHARGEMENT, $objetCouche, PRO_OBJET_TYPE_COUCHE);
                            if ($bAllow)
                                break;
                        }
                        //vérification des restrictions de compétences
                        $bAllow = $bAllow && $user->HasTraitementCompetence('TELECHARGEMENT', $objetCouche);
                        //vérification des restrictions territoriales
                        $trTerr = $user->GetTraitementTerritoire('TELECHARGEMENT', $objetCouche);
                        if ($trTerr === false)
                            $bAllow = false;
                    }
                    $bAllow = ($bFreeDownload || $bAllow) && $couche_table && $this->existData($couche_table, $acces_adress_admin, "couche", $couche_type);
                }
                if($objectType == "series"){
                    $bAllow = true;
                }
                $tabRes["TELECHARGEMENT"] = $bAllow;
            }

            if (array_search("NAVIGATION", $tabTraitement) !== FALSE) {
                $bAllow = false;
                //visualisation d'une carte
                if ($objectType == "service") {
                    // métadonnée de carte
                    if ($objectSType == "invoke") {
                        for ($rsCarte->First(); !$rsCarte->EOF(); $rsCarte->Next()) {
                            $domaine = $rsCarte->read(array_search("DOM_NOM", $tabFieldsCarte));
                            $sous_domaine = $rsCarte->read(array_search("SSDOM_NOM", $tabFieldsCarte));
                            $objetCarte = $rsCarte->read(array_search("PK_CARTE_PROJET", $tabFieldsCarte));
                            $acces_adress_admin = $rsCarte->read(array_search("ACCS_ADRESSE_ADMIN", $tabFieldsCarte));
                            $carte_data = $rsCarte->read(array_search("STKCARD_PATH", $tabFieldsCarte));
                            $carte_type = $rsCarte->read(array_search("CARTP_FORMAT", $tabFieldsCarte));
                            $carte_statut = $rsCarte->read(array_search("STATUT", $tabFieldsCarte));

                            //vérification de l'autorisation sur le domaine et sur l'objet
                            $bAllow = $user->HasTraitement('NAVIGATION', html_entity_decode($domaine, ENT_QUOTES, 'UTF-8'), html_entity_decode($sous_domaine, ENT_QUOTES, 'UTF-8'), PRO_TRT_TYPE_CONSULTATION, $objetCarte, PRO_OBJET_TYPE_CARTE);
                            if ($bAllow)
                                break;
                        }
                        $bAllow = $bAllow && $user->HasTraitementCompetence('NAVIGATION', $objetCarte);
                        $bAllow = $bAllow && $this->existData($carte_data, $acces_adress_admin, "carte", $carte_type);
                        $bAllow = $bAllow && ($carte_statut == 4);
                        
                        
                    }
                    // métadonnée de service
                    else {
                        $bAllow = true;
                    }

                    $tabRes["NAVIGATION"] = $bAllow;
                } elseif ($objectType == "dataset") {
                    //visualisation d'une donnée
                    //vérification du caractère de visualisation libre de la donnée
                    $query = 'SELECT couchd_wfs, couchd_wms FROM couche_donnees WHERE pk_couche_donnees in (select fmeta_fk_couche_donnees from fiche_metadonnees where  FMETA_ID = :objectId)';
                    $rs = $dao->BuildResultSet($query, $query_params);
                    $rs->First();
                    $bFreeVisu = $rs->Read(0) || $rs->Read(1);
                    //vérification des droits sur un des domaines de la donnée
                    if ($rsCouche->GetNbRows() > 0) {
                        for ($rsCouche->First(); !$rsCouche->EOF(); $rsCouche->Next()) {
                            $domaine = $rsCouche->read(array_search("DOM_NOM", $tabFieldsCouche));
                            $sous_domaine = $rsCouche->read(array_search("SSDOM_NOM", $tabFieldsCouche));
                            $objetCouche = $rsCouche->read(array_search("PK_COUCHE_DONNEES", $tabFieldsCouche));
                            $acces_adress_admin = $rsCouche->read(array_search("COUCHE_SRV_ADMIN", $tabFieldsCouche));
                            $couche_table = $rsCouche->read(array_search("COUCHE_TABLE", $tabFieldsCouche));
                            $couche_type = $rsCouche->read(array_search("COUCHE_TYPE", $tabFieldsCouche));
                            $couche_statut = $rsCouche->read(array_search("STATUT", $tabFieldsCouche));
                            //vérification de l'autorisation sur le domaine et sur l'objet
                            $bAllow = $user->HasTraitement('NAVIGATION', html_entity_decode($domaine, ENT_QUOTES, 'UTF-8'), html_entity_decode($sous_domaine, ENT_QUOTES, 'UTF-8'), PRO_TRT_TYPE_CONSULTATION, $objetCouche, PRO_OBJET_TYPE_COUCHE);
                            if ($bAllow)
                                break;
                        }
                        //vérification des restrictions de compétences (non prévu)      
                        //$bAllow = $bAllow && $user->HasTraitementCompetence('NAVIGATION', $objetCouche);
                        //vérification des restrictions territoriales
                        $trTerr = $user->GetTraitementTerritoire('NAVIGATION', $objetCouche);
                        //echo json_encode($trTerr);
                        if ($trTerr === false)
                            $bAllow = false;
                        
                        //restricition territoriale en place
                        if(is_array($trTerr))
                            $tabRes["RESTRICTION_TERRITORIALE"] = true;
                        
                        $bAllow = ($bFreeVisu || $bAllow) && $this->existData($couche_table, $acces_adress_admin, "couche", $couche_type);
                        $bAllow = $bAllow && ($couche_statut == 4);
                    }
                    $tabRes["NAVIGATION"] = $bAllow;
                }
            }

            if (array_search("CMS", $tabTraitement) !== FALSE) {
                $tabRes["PRO_EDITION"] = false;
                
                $bAllow = false;
                /*if ($objectType == "service") {
                    // métadonnée de carte
                    if ($objectSType == "invoke") {
                        for ($rsCarte->First(); !$rsCarte->EOF(); $rsCarte->Next()) {
                            $domaine = $rsCarte->read(array_search("DOM_NOM", $tabFieldsCarte));
                            $sous_domaine = $rsCarte->read(array_search("SSDOM_NOM", $tabFieldsCarte));
                            $objetMetadonne = $rsCarte->read(array_search("PK_FICHE_METADONNEES", $tabFieldsCarte));
                            //vérification de l'autorisation sur le domaine et sur l'objet
                            $bAllow = $user->HasTraitement('CMS', html_entity_decode($domaine, ENT_QUOTES, 'UTF-8'), html_entity_decode($sous_domaine, ENT_QUOTES, 'UTF-8'), PRO_TRT_TYPE_MODIFICATION, $objetMetadonne, PRO_OBJET_TYPE_METADONNEE);
                 
                            if ($bAllow)
                                break;
                        }
                    }
                    // métadonnée de service
                    else {
                        if ($IsADM_PRODIGE) {
                            $bAllow = true;
                        }
                    }
                } elseif ($objectType == "dataset" || $objectType == "series" || $objectType == "nonGeographicDataset") {
                    for ($rsCouche->First(); !$rsCouche->EOF(); $rsCouche->Next()) {
                        $domaine = $rsCouche->read(array_search("DOM_NOM", $tabFieldsCouche));
                        $sous_domaine = $rsCouche->read(array_search("SSDOM_NOM", $tabFieldsCouche));
                        $couche_type = $rsCouche->read(array_search("COUCHE_TYPE", $tabFieldsCouche));
                        $acces_adress_admin = $rsCouche->read(array_search("COUCHE_SRV_ADMIN", $tabFieldsCouche));
                        $couche_table = $rsCouche->read(array_search("COUCHE_TABLE", $tabFieldsCouche));
                        $objetMetadonne = $rsCouche->read(array_search("PK_FICHE_METADONNEES", $tabFieldsCouche));
                        //vérification de l'autorisation sur le domaine et sur l'objet
                        $bAllow = $user->HasTraitement('CMS', html_entity_decode($domaine, ENT_QUOTES, 'UTF-8'), html_entity_decode($sous_domaine, ENT_QUOTES, 'UTF-8'), PRO_TRT_TYPE_MODIFICATION, $objetMetadonne, PRO_OBJET_TYPE_METADONNEE);
                        if ($bAllow)
                            break;
                    }
                    
                    //droit de créer une structure de donnée = module edition actif + droit edition + donnée n'existe pas
                    $tabRes["PRO_EDITION"] = (PRO_EDITION == "on" && $user->HasTraitement('EDITION EN LIGNE') && !(isset($couche_table) && $this->existData($couche_table, $acces_adress_admin, "couche", $couche_type)));
                    
                        
                }elseif($objectType == "metadata") {
                    
                    for ($rsMetadata->First(); !$rsMetadata->EOF(); $rsMetadata->Next()) {
                      $domaine = $rsMetadata->read(array_search("DOM_NOM", $tabFieldsMetadata));
                      $sous_domaine = $rsMetadata->read(array_search("SSDOM_NOM", $tabFieldsMetadata));
                      
                      //only to check CMS rights on dom/sdom
                      $bAllow = $user->HasTraitement('CMS', html_entity_decode($domaine, ENT_QUOTES, 'UTF-8'), html_entity_decode($sous_domaine, ENT_QUOTES, 'UTF-8'));
                      if ($bAllow)
                            break;
                    }
                }
                */
                //droit de créer une structure de donnée = module edition actif + droit edition + donnée n'existe pas
                if ($objectType == "dataset") {
                    $tabRes["PRO_EDITION"] = (PRO_EDITION == "on" && $user->HasTraitement('EDITION EN LIGNE') && !(isset($couche_table) && $this->existData($couche_table, $acces_adress_admin, "couche", $couche_type)));
                }
                
                for ($rsMetadata->First(); !$rsMetadata->EOF(); $rsMetadata->Next()) {
                      $domaine = $rsMetadata->read(array_search("DOM_NOM", $tabFieldsMetadata));
                      $sous_domaine = $rsMetadata->read(array_search("SSDOM_NOM", $tabFieldsMetadata));
                      //only to check CMS rights on dom/sdom
                      $bAllow = $user->HasTraitement('CMS', html_entity_decode($domaine, ENT_QUOTES, 'UTF-8'), html_entity_decode($sous_domaine, ENT_QUOTES, 'UTF-8'));
                      if ($bAllow)
                            break;
                }

                $tabRes["CMS"] = $bAllow;
            }
            
            if ( $objectId!="" ){
                $rsSynchronised = $dao->BuildResultSet(
                    "SELECT pk_tache_import_donnees	" .
                    " FROM 	tache_import_donnees" .
                    " WHERE uuid = (select uuid from metadata where id= :objectId)", array("objectId" => $objectId)
                );
                $tabRes["SYNCHRONIZE"] = !$rsSynchronised->EOF();
            } else {
                $tabRes["SYNCHRONIZE"] = false;
            }
            //TODO BFE à supprimer
            if (array_search("CMS_MODE_PROPOSITION", $tabTraitement) !== FALSE) {
                $bAllow = false;
                if ($objectType == "service") {
                    // métadonnée de carte
                    if ($objectSType == "invoke") {
                        for ($rsCarte->First(); !$rsCarte->EOF(); $rsCarte->Next()) {
                            $domaine = $rsCarte->read(array_search("DOM_NOM", $tabFieldsCarte));
                            $sous_domaine = $rsCarte->read(array_search("SSDOM_NOM", $tabFieldsCarte));
                            $objetMetadonne = $rsCarte->read(array_search("PK_FICHE_METADONNEES", $tabFieldsCarte));
                            //vérification de l'autorisation sur le domaine et sur l'objet
                            $bAllow = $user->HasTraitement('CMS', html_entity_decode($domaine, ENT_QUOTES, 'UTF-8'), html_entity_decode($sous_domaine, ENT_QUOTES, 'UTF-8'), (PRO_MODE_PROPOSITION_ACTIF == "on" ? PRO_TRT_TYPE_PROPOSITION : PRO_TRT_TYPE_VALIDATION), $objetMetadonne, PRO_OBJET_TYPE_METADONNEE);
                            if ($bAllow)
                                break;
                        }
                    }
                    // métadonnée de service
                    else {
                        if ($IsADM_PRODIGE) {
                            $bAllow = true;
                        }
                    }
                } elseif ($objectType == "dataset" || $objectType == "series" || $objectType == "nonGeographicDataset") {
                    for ($rsCouche->First(); !$rsCouche->EOF(); $rsCouche->Next()) {
                        $domaine = $rsCouche->read(array_search("DOM_NOM", $tabFieldsCouche));
                        $sous_domaine = $rsCouche->read(array_search("SSDOM_NOM", $tabFieldsCouche));
                        $objetMetadonne = $rsCouche->read(array_search("PK_FICHE_METADONNEES", $tabFieldsCouche));
                        //vérification de l'autorisation sur le domaine et sur l'objet
                        $bAllow = $user->HasTraitement('CMS', html_entity_decode($domaine, ENT_QUOTES, 'UTF-8'), html_entity_decode($sous_domaine, ENT_QUOTES, 'UTF-8'), ($PRO_MODE_PROPOSITION_ACTIF == "on" ? PRO_TRT_TYPE_PROPOSITION : PRO_TRT_TYPE_VALIDATION), $objetMetadonne, PRO_OBJET_TYPE_METADONNEE);
                        if ($bAllow)
                            break;
                    }
                }
                $tabRes["CMS_MODE_PROPOSITION"] = $bAllow;
            }
            //TODO BFE à supprimer
            if (array_search("PROPOSITION", $tabTraitement) !== FALSE) {
                $bAllow = false;
                if ($objectType == "service") {
                    // métadonnée de carte
                    if ($objectSType == "invoke") {
                        for ($rsCarte->First(); !$rsCarte->EOF(); $rsCarte->Next()) {
                            $domaine = $rsCarte->read(array_search("DOM_NOM", $tabFieldsCarte));
                            $sous_domaine = $rsCarte->read(array_search("SSDOM_NOM", $tabFieldsCarte));
                            $objetMetadonne = $rsCarte->read(array_search("PK_FICHE_METADONNEES", $tabFieldsCarte));
                            //vérification de l'autorisation sur le domaine et sur l'objet
                            $bAllow = $user->HasTraitement('PROPOSITION', html_entity_decode($domaine, ENT_QUOTES, 'UTF-8'), html_entity_decode($sous_domaine, ENT_QUOTES, 'UTF-8'), PRO_TRT_TYPE_VALIDATION, $objetMetadonne, PRO_OBJET_TYPE_METADONNEE);
                            if ($bAllow)
                                break;
                        }
                    }
                    // métadonnée de service
                    else {
                        if ($IsADM_PRODIGE) {
                            $bAllow = true;
                        }
                    }
                } elseif ($objectType == "dataset" || $objectType == "series" || $objectType == "nonGeographicDataset") {
                    for ($rsCouche->First(); !$rsCouche->EOF(); $rsCouche->Next()) {
                        $domaine = $rsCouche->read(array_search("DOM_NOM", $tabFieldsCouche));
                        $sous_domaine = $rsCouche->read(array_search("SSDOM_NOM", $tabFieldsCouche));
                        $objetMetadonne = $rsCouche->read(array_search("PK_FICHE_METADONNEES", $tabFieldsCouche));
                        //vérification de l'autorisation sur le domaine et sur l'objet
                        $bAllow = $user->HasTraitement('PROPOSITION', html_entity_decode($domaine, ENT_QUOTES, 'UTF-8'), html_entity_decode($sous_domaine, ENT_QUOTES, 'UTF-8'), PRO_TRT_TYPE_VALIDATION, $objetMetadonne, PRO_OBJET_TYPE_METADONNEE);
                        if ($bAllow)
                            break;
                    }
                }
                $tabRes["PROPOSITION"] = $bAllow;
            }
            //TODO BFE à supprimer
            if (array_search("PUBLICATION", $tabTraitement) !== FALSE) {
                $bAllow = false;
                if ($objectType == "service") {
                    // métadonnée de carte
                    if ($objectSType == "invoke") {
                        for ($rsCarte->First(); !$rsCarte->EOF(); $rsCarte->Next()) {
                            $domaine = $rsCarte->read(array_search("DOM_NOM", $tabFieldsCarte));
                            $sous_domaine = $rsCarte->read(array_search("SSDOM_NOM", $tabFieldsCarte));
                            $objetMetadonne = $rsCarte->read(array_search("PK_FICHE_METADONNEES", $tabFieldsCarte));
                            //vérification de l'autorisation sur le domaine et sur l'objet
                            $bAllow = $user->HasTraitement('PUBLICATION', html_entity_decode($domaine, ENT_QUOTES, 'UTF-8'), html_entity_decode($sous_domaine, ENT_QUOTES, 'UTF-8'), PRO_TRT_TYPE_PUBLICATION, $objetMetadonne, PRO_OBJET_TYPE_METADONNEE);
                            if ($bAllow)
                                break;
                        }
                    }
                    // métadonnée de service
                    else {
                        if ($IsADM_PRODIGE) {
                            $bAllow = true;
                        }
                    }
                } elseif ($objectType == "dataset" || $objectType == "series" || $objectType == "nonGeographicDataset") {
                    for ($rsCouche->First(); !$rsCouche->EOF(); $rsCouche->Next()) {
                        $domaine = $rsCouche->read(array_search("DOM_NOM", $tabFieldsCouche));
                        $sous_domaine = $rsCouche->read(array_search("SSDOM_NOM", $tabFieldsCouche));
                        $objetMetadonne = $rsCouche->read(array_search("PK_FICHE_METADONNEES", $tabFieldsCouche));
                        //vérification de l'autorisation sur le domaine et sur l'objet
                        $bAllow = $user->HasTraitement('PUBLICATION', html_entity_decode($domaine, ENT_QUOTES, 'UTF-8'), html_entity_decode($sous_domaine, ENT_QUOTES, 'UTF-8'), PRO_TRT_TYPE_PUBLICATION, $objetMetadonne, PRO_OBJET_TYPE_METADONNEE);
                        if ($bAllow)
                            break;
                    }
                }
                $tabRes["PUBLICATION"] = $bAllow;
            }

            if (array_search("GLOBAL_RIGHTS", $tabTraitement) !== FALSE) {
                $tabRes["CMS"] = $user->HasTraitement('CMS');
                $tabRes["PUBLICATION"] = $user->HasTraitement('PUBLICATION');
                $tabRes["PROPOSITION"] = $user->HasTraitement('PROPOSITION');
                $tabRes["PARAMETRAGE"] = $user->HasTraitement('PARAMETRAGE');
                $tabRes["MAJIC"] = $user->HasTraitement('MAJIC') && (PRO_IS_MAJIC_ACTIF == "on");
                $tabRes["ADMINISTRATION"] = $user->HasTraitement('ADMINISTRATION');
                $tabRes["PRO_IS_REQ_JOINTURES_ACTIF"] = PRO_IS_REQ_JOINTURES_ACTIF;
                $tabRes["PRO_INCLUDED"] = defined("PRO_INCLUDED") && PRO_INCLUDED;
                $tabRes["USER_GENERIC"] = $user->GetUserGeneric();
                $tabRes["USER_ID"] = $user->GetLogin();
            }

            if (array_search("PUBLIPOSTAGE", $tabTraitement) !== FALSE) {
                $tabRes["PUBLIPOSTAGE"] = PRO_PUBLIPOSTAGE == "on" && $user->HasTraitement('PUBLIPOSTAGE');
            }

            if (array_search("EDITION", $tabTraitement) !== FALSE) {
                $bAllowEdition = false;
                $bAllowEditionAjout = false;
                $bAllowEditionModif = false;
                
                if (PRO_EDITION == "on" && ($user->HasTraitement('EDITION EN LIGNE') || $user->HasTraitement('EDITION EN LIGNE (AJOUT)') || $user->HasTraitement('EDITION EN LIGNE (MODIF)'))) {
                    //droit d'édition d'une donnée

                    if ($objectType == "dataset") {
                        //vérification des droits sur un des domaines de la donnée
                        if ($rsCouche->GetNbRows() > 0) {
                            //vérifications droits EDITION
                            if ($user->HasTraitement('EDITION EN LIGNE')) {
                                for ($rsCouche->First(); !$rsCouche->EOF(); $rsCouche->Next()) {
                                    $domaine = $rsCouche->read(array_search("DOM_NOM", $tabFieldsCouche));
                                    $sous_domaine = $rsCouche->read(array_search("SSDOM_NOM", $tabFieldsCouche));
                                    $objetCouche = $rsCouche->read(array_search("PK_COUCHE_DONNEES", $tabFieldsCouche));
                                    $acces_adress_admin = $rsCouche->read(array_search("COUCHE_SRV_ADMIN", $tabFieldsCouche));
                                    $couche_table = $rsCouche->read(array_search("COUCHE_TABLE", $tabFieldsCouche));
                                    $couche_type = $rsCouche->read(array_search("COUCHE_TYPE", $tabFieldsCouche));
                                    $couche_statut = $rsCouche->read(array_search("STATUT", $tabFieldsCouche));
                                    //vérification de l'autorisation sur le domaine et sur l'objet
                                    $bAllowEdition = $user->HasTraitement('EDITION EN LIGNE', html_entity_decode($domaine, ENT_QUOTES, 'UTF-8'), html_entity_decode($sous_domaine, ENT_QUOTES, 'UTF-8'), PRO_TRT_TYPE_EDITION, $objetCouche, PRO_OBJET_TYPE_COUCHE);

                                    if ($bAllowEdition)
                                        break;
                                }
                            }
                            //vérifications droits EDITION_MODIFICATION
                            if ($user->HasTraitement('EDITION EN LIGNE (MODIF)')) {
                                for ($rsCouche->First(); !$rsCouche->EOF(); $rsCouche->Next()) {
                                    $domaine = $rsCouche->read(array_search("DOM_NOM", $tabFieldsCouche));
                                    $sous_domaine = $rsCouche->read(array_search("SSDOM_NOM", $tabFieldsCouche));
                                    $objetCouche = $rsCouche->read(array_search("PK_COUCHE_DONNEES", $tabFieldsCouche));
                                    $acces_adress_admin = $rsCouche->read(array_search("COUCHE_SRV_ADMIN", $tabFieldsCouche));
                                    $couche_table = $rsCouche->read(array_search("COUCHE_TABLE", $tabFieldsCouche));
                                    $couche_type = $rsCouche->read(array_search("COUCHE_TYPE", $tabFieldsCouche));
                                    $couche_statut = $rsCouche->read(array_search("STATUT", $tabFieldsCouche));
                                    
                                    //vérification de l'autorisation sur le domaine et sur l'objet
                                    $bAllowEditionModif = $user->HasTraitement('EDITION EN LIGNE (MODIF)', html_entity_decode($domaine, ENT_QUOTES, 'UTF-8'), html_entity_decode($sous_domaine, ENT_QUOTES, 'UTF-8'), PRO_TRT_TYPE_EDITION, $objetCouche, PRO_OBJET_TYPE_COUCHE);

                                    if ($bAllowEditionModif)
                                        break;
                                }
                            }
                            //vérifications droits EDITION_AJOUT
                            if ($user->HasTraitement('EDITION EN LIGNE (AJOUT)')) {
                                for ($rsCouche->First(); !$rsCouche->EOF(); $rsCouche->Next()) {
                                    $domaine = $rsCouche->read(array_search("DOM_NOM", $tabFieldsCouche));
                                    $sous_domaine = $rsCouche->read(array_search("SSDOM_NOM", $tabFieldsCouche));
                                    $objetCouche = $rsCouche->read(array_search("PK_COUCHE_DONNEES", $tabFieldsCouche));
                                    $acces_adress_admin = $rsCouche->read(array_search("COUCHE_SRV_ADMIN", $tabFieldsCouche));
                                    $couche_table = $rsCouche->read(array_search("COUCHE_TABLE", $tabFieldsCouche));
                                    $couche_type = $rsCouche->read(array_search("COUCHE_TYPE", $tabFieldsCouche));
                                    $couche_statut = $rsCouche->read(array_search("STATUT", $tabFieldsCouche));
                                    //vérification de l'autorisation sur le domaine et sur l'objet
                                    $bAllowEditionAjout = $user->HasTraitement('EDITION EN LIGNE (AJOUT)', html_entity_decode($domaine, ENT_QUOTES, 'UTF-8'), html_entity_decode($sous_domaine, ENT_QUOTES, 'UTF-8'), PRO_TRT_TYPE_EDITION_AJOUT, $objetCouche, PRO_OBJET_TYPE_COUCHE);
                                    if ($bAllowEditionAjout)
                                        break;
                                }
                            }
                            //la donnée doit exister
                            if (!$this->existData($couche_table, $acces_adress_admin, "couche", $couche_type)) {
                                $bAllowEdition = false;
                                $bAllowEditionAjout = false;
                                $bAllowEditionModif = false;
                            }

                            //la donnée doit être publiée
                            if (!($couche_statut == 4)) {
                                $bAllowEdition = false;
                                $bAllowEditionAjout = false;
                                $bAllowEditionModif = false;
                            }
                            if (!($couche_type == 1)) {
                                $bAllowEdition = false;
                                $bAllowEditionAjout = false;
                                $bAllowEditionModif = false;
                            }

                            //ajout d'informations sur les restrictions territoriales
                            //vérification des restrictions territoriales
                            $trTerr = $user->GetTraitementTerritoire('EDITION', $objetCouche);
                            //$trTerr= 1 : pas de restriction, $trTerr=1 : restriction mais aucun droit => pas de droit d'édition, sinon tableau des territoires autorisés
                            if (is_array($trTerr)) {
                                $tabRes["EDITION_AREA"] = new \stdClass();
                                $tabRes["EDITION_AREA"]->filter_table = 'prodige_perimetre';
                                //on suppose que le filtre est identique sur tous les périmètres
                                $tabRes["EDITION_AREA"]->filter_field = $trTerr[0][2];
                                foreach ($trTerr as $value) {
                                    $tabRes["EDITION_AREA"]->filter_values[] = $value[0];
                                }
                            } else {
                                if ($trTerr === false) {
                                    $bAllowEdition = false;
                                    $bAllowEditionAjout = false;
                                    $bAllowEditionModif = false;
                                }
                            }
                            
                        }
                    }
                    
                    $tabRes["EDITION"] = $bAllowEdition;
                    $tabRes["EDITION_AJOUT"] = $bAllowEditionAjout;
                    $tabRes["EDITION_MODIFICATION"] = $bAllowEditionModif;
                }
            }
        //TODO

        /** @todo set protocol depending of current protocol */
        $protocole = "http://";
        switch ($objectType) {
            case "dataset" :
            case "series" :
            case "nonGeographicDataset" :
                if ($rsCouche) {
                    $rsCouche->First();
                    $tabRes["pk_data"] = $rsCouche->read(array_search("PK_COUCHE_DONNEES", $tabFieldsCouche));
                    $tabRes["fmeta_id"] = $rsCouche->read(array_search("FMETA_ID", $tabFieldsCouche));
                    $tabRes["statut"] = $rsCouche->read(array_search("STATUT", $tabFieldsCouche));
                    $tabRes["couche_nom"] = $rsCouche->read(array_search("COUCHE_NOM", $tabFieldsCouche));

                    $tabRes["layer_table"] = $rsCouche->read(array_search("COUCHE_TABLE", $tabFieldsCouche));
                    $tabRes["couche_type"] = $rsCouche->read(array_search("COUCHE_TYPE", $tabFieldsCouche));
                                        
                    $tabRes["color_theme_id"] = PRO_COLOR_THEME_ID;

                    $tabRes["visualisable"] = ( $rsCouche->read(array_search("COUCHD_VISUALISABLE", $tabFieldsCouche)) == 1 ? true : false );
                    $tabRes["download"] = ( $rsCouche->read(array_search("COUCHD_DOWNLOAD", $tabFieldsCouche)) == 1 ? true : false );
                    $tabRes["wms"] = ( $rsCouche->read(array_search("COUCHD_WMS", $tabFieldsCouche)) == 1 ? true : false );
                    $tabRes["wfs"] = ( $rsCouche->read(array_search("COUCHD_WFS", $tabFieldsCouche)) == 1 ? true : false );

                    $strSql = "SELECT id FROM metadata WHERE istemplate='y' AND schemaid='iso19110'";
                    $rs = $dao->BuildResultSet($strSql);
                    if ($rs->GetNbRows() > 0) {
                        $rs->First();
                        $tabRes["id_template_catalogue"] = $rs->read(0);
                    }
                }
                break;
            case "service" :
                switch ($objectSType) {
                    case "invoke" :
                        if ($rsCarte) {
                            $rsCarte->First();
                            $tabRes["pk_data"] = $rsCarte->read(array_search("PK_CARTE_PROJET", $tabFieldsCarte));
                            $tabRes["pk_stockage_carte"] = $rsCarte->read(array_search("PK_STOCKAGE_CARTE", $tabFieldsCarte));
                            $tabRes["fmeta_id"] = $rsCarte->read(array_search("FMETA_ID", $tabFieldsCarte));
                            $tabRes["statut"] = $rsCarte->read(array_search("STATUT", $tabFieldsCarte));
                            $tabRes["carte_nom"] = $rsCarte->read(array_search("CARTP_NOM", $tabFieldsCarte));
                            $tabRes["path"] = $rsCarte->read(array_search("STKCARD_PATH", $tabFieldsCarte));
                            $tabRes["carte_type"] = $rsCarte->read(array_search("CARTP_FORMAT", $tabFieldsCarte));
                            $tabRes["carte_wms"] = $rsCarte->read(array_search("CARTP_WMS", $tabFieldsCarte));
                        }
                        break;
                    default :
                        if ($rsService) {
                            $rsService->First();
                            $tabRes["fmeta_id"] = $rsService->read(array_search("FMETA_ID", $tabFieldsService));
                            $tabRes["statut"] = $rsService->read(array_search("STATUT", $tabFieldsService));
                        }
                        break;
                }
                break;
        }
        $tabRes["isAdmProdige"] = $IsADM_PRODIGE;

        if (!is_null($user)) {
            $tabRes["isConnected"] = $user->isConnected();
        }

        $tabRes["userNom"] = $user->GetNom();
        $tabRes["userPrenom"] = $user->GetPrenom();
        $tabRes["userLogin"] = $user->GetLogin();
        $tabRes["userId"] = $user->GetUserId();
        $tabRes["userSignature"] = $user->GetUserSignature();
        $tabRes["userEmail"] = $user->GetEmail();

        $tabRes["success"] = true;
        
        if ( $resultAsJson ){
            return $tabRes;
        }
        //for cross domain case
        if ($callback != "")
            return new Response($callback . "(" . json_encode($tabRes) . ")");
        else
            return new JsonResponse($tabRes);
    }

    /**
     * détermine si une donnée existe (couche ou carte) / appel d'un service de l'admincarto 
     * 
     * @param $objectName
     * @param $acces_adress_admin
     * @param $objectType
     * @param $objectSubType
     * @return boolean
     */
    function existData($objectName, $acces_adress_admin, $objectType, $objectSubType) {
        /*
        if ($objectType == "couche") {
            if ($objectSubType == -1 || $objectSubType == -2)//lots de données ou majic, pas de vérification
                return true;
            elseif ($objectSubType == -3 || $objectSubType == -4) //données vecteur
                $objectSubType = 1;
        }

        return file_get_contents("http://" . $acces_adress_admin . "/PRRA/existData.php?objectName=" . urlencode($objectName) . "&objectType=" . $objectType . "&objectSubType=" . $objectSubType) == 1;
        */

        // PRRA/existData.php
        $data = $objectName; //(isset($_GET["objectName"]) ? $_GET["objectName"] : "");
        $type_data = $objectType; //(isset($_GET["objectType"]) ? $_GET["objectType"] : "");
        $type_stockage = $objectSubType; //(isset($_GET["objectSubType"]) ? $_GET["objectSubType"] : "");
        if($type_data=="couche"){
            switch($type_stockage){
                //raster
                case 0 :
                    return file_exists(PRO_MAPFILE_PATH.$data);
                //vector    
                case 1 :
                //view    
                case -4 :
                //table
                case -3 :
                    $conn = $this->getProdigeConnection();
                    $table = $conn->fetchAll("SELECT tablename FROM pg_tables where tablename=:data", array('data'=>$data));
                    if(count($table)==0) {
                        $views = $conn->fetchAll("SELECT viewname FROM pg_views where viewname=:data", array('data'=>$data));
                        return count($views)>0;
                    } else {
                        return true;
                    }
            }
        } else if($type_data=="carte"){
            switch($type_stockage){
                case 0 :
                    return file_exists(PRO_MAPFILE_PATH.$data);
                case 1 :
                    return file_exists(PRO_MAPFILE_PATH."CartesStatiques/".$data);
            }
        }
        
        return false;
    }

}
