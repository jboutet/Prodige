<?php
namespace ProdigeCatalogue\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use JMS\SecurityExtraBundle\Annotation\Secure;
use ProdigeCatalogue\WebBundle\Common\Ressources\Ressources;
use Prodige\ProdigeBundle\Controller\BaseController;
use Prodige\ProdigeBundle\DAOProxy\DAO;
use ProdigeCatalogue\AdminBundle\Common\Modules\BO\UtilisateurVO;

class UsersController extends BaseController
{

    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/catalogue/administration/users/setuser", name="catalogue_administration_profiles_setuser", options={"expose"=true})
     */
    public function setUserAction(Request $request)
    {
        $conn = $this->getCatalogueConnection('catalogue');
        $dao = new DAO($conn, 'catalogue');
        
        $mode = $request->get("mode", "");
        
        if ($mode == "") {
            $result['success'] = false;
            $result['msg'] = htmlentities("Le paramètre 'mode' est manquant.", ENT_QUOTES, "UTF-8");
            Return new JsonResponse($result);
        }
        
        $fields = $request->get("field", array());
        $updateUserPwdOnly = ($mode == 2 && count($fields) == 2 && isset($fields["AGENT_LOGIN"]) && isset($fields["AGENT_PWD"]) ? true : false);
        
        $pk_utilisateur = $request->get("agent_id", "");
        $usr_id = $fields["AGENT_LOGIN"][1];
        //$usr_id = $request->get("field[AGENT_LOGIN][1]", "");
        $usr_password =  (isset($fields["AGENT_PWD"][1]) ? $fields["AGENT_PWD"][1] : "");
        //$usr_password = $request->get("field[AGENT_PWD][1]", "");
        
        // verify parameter
        $bParamOk = true;
        if ($pk_utilisateur != "" && ! ctype_digit($pk_utilisateur))
            $bParamOk = false;
        
        if (! $updateUserPwdOnly) {
            $usr_nom =(isset($fields["AGENT_NOM"][1]) && $fields["AGENT_NOM"][1] !="" ? $fields["AGENT_NOM"][1] : ""); 
            $usr_prenom = (isset($fields["AGENT_PRENOM"][1]) && $fields["AGENT_PRENOM"][1] !="" ? $fields["AGENT_PRENOM"][1] : "");
            $usr_email = (isset($fields["AGENT_MAIL"][1]) && $fields["AGENT_MAIL"][1] !="" ? $fields["AGENT_MAIL"][1] : "");
            $usr_telephone = (isset($fields["AGENT_TEL"][1]) && $fields["AGENT_TEL"][1] !="" ? $fields["AGENT_TEL"][1] : "");
            $usr_telephone2 = (isset($fields["AGENT_MOBIL"][1]) && $fields["AGENT_MOBIL"][1] !="" ? $fields["AGENT_MOBIL"][1] : "");
            $profil_id = (isset($fields["PROFIL_ID"][1]) && $fields["PROFIL_ID"][1] !="" ? $fields["PROFIL_ID"][1] :  $request->get("profil_id", ""));
            $usr_generic = (isset($fields["AGTYPE_ID"][1]) && $fields["AGTYPE_ID"][1]!="" ?  $fields["AGTYPE_ID"][1] : "0");
            $usr_signature = (isset($fields["AGENT_SIGNATURE"][1]) && $fields["AGENT_SIGNATURE"][1]!="" ? $fields["AGENT_SIGNATURE"][1] : "0");
            
            if ($profil_id != "" && ! ctype_digit($profil_id))
                $bParamOk = false;
            if ($usr_generic != "" && ! ctype_digit($usr_generic))
                $bParamOk = false;
        }

        if (! $bParamOk) {
            $result['success'] = false;
            $result['msg'] = htmlentities("Paramètres non valides.", ENT_QUOTES, "UTF-8");
            // $result['param'] = $_REQUEST;
            Return new JsonResponse($result);
        }
        $usr_id = (mb_substr($usr_id, 0, 128));
        
        if( $usr_password!=""){
            $usr_password = $this->getPasswordEncoder()->encode($usr_password);
        }
       
        if (! $updateUserPwdOnly) {
            $usr_nom = (mb_substr($usr_nom, 0, 128));
            $usr_prenom = (mb_substr($usr_prenom, 0, 128));
            $usr_email = (mb_substr($usr_email, 0, 128));
            $usr_telephone = (mb_substr($usr_telephone, 0, 30));
            $usr_telephone2 = (mb_substr($usr_telephone2, 0, 30));
            $usr_signature = (mb_substr($usr_signature, 0, 4000));
        }
        
        switch ($mode) {
            case 1: // création utilisateur
                $query = "INSERT INTO utilisateur (pk_utilisateur, usr_id, usr_nom, usr_prenom, usr_email, usr_telephone, usr_telephone2, usr_password, usr_pwdexpire, usr_generic, usr_signature)" . " VALUES (:pk_utilisateur, :usr_id, :usr_nom, :usr_prenom, :usr_email, :usr_telephone, :usr_telephone2, :usr_password, now() + '1 year', :usr_generic, :usr_signature);";
                if ($dao->Execute($query,
                array("pk_utilisateur"=> $pk_utilisateur,
                          "usr_id"=> $usr_id,
                          "usr_nom" => $usr_nom,
                          "usr_prenom" => $usr_prenom,
                          "usr_email" => $usr_email,
                          "usr_telephone" => $usr_telephone,
                          "usr_telephone2" => $usr_telephone2,
                          "usr_generic" => $usr_generic,
                          "usr_signature" => $usr_signature,
                          "usr_password" => $usr_password)))
                {
                    $query = "INSERT INTO grp_regroupe_usr (grpusr_fk_groupe_profil, grpusr_fk_utilisateur, grpusr_is_principal) VALUES (:profil_id, :pk_utilisateur, 1);";
                  if ($dao->Execute($query, array("profil_id"=>$profil_id, "pk_utilisateur" => $pk_utilisateur))) 
                    {
                        $result['msg'] = htmlentities("L'utilisateur a été créé.", ENT_QUOTES, "UTF-8");
                    }
                }
                
                break;
            case 2: // modification utilisateur
                $query = (! $updateUserPwdOnly ?
                            //if usr_password is empty don't change it    
                            ($usr_password =="" ? "UPDATE utilisateur SET usr_id=:usr_id, usr_nom=:usr_nom, 
                                                  usr_prenom=:usr_prenom, usr_email=:usr_email, usr_telephone=:usr_telephone, 
                                                  usr_telephone2=:usr_telephone2, usr_generic=:usr_generic, 
                                                  usr_signature=:usr_signature WHERE pk_utilisateur=:pk_utilisateur"
                                                : "UPDATE utilisateur SET usr_id=:usr_id, usr_nom=:usr_nom, 
                                                  usr_prenom=:usr_prenom, usr_email=:usr_email, usr_telephone=:usr_telephone, 
                                                  usr_telephone2=:usr_telephone2, usr_password=:usr_password, usr_generic=:usr_generic, 
                                                  usr_signature=:usr_signature WHERE pk_utilisateur=:pk_utilisateur")
                            : 
                                                 "UPDATE utilisateur SET usr_id=:usr_id, usr_password=:usr_password WHERE pk_utilisateur=:pk_utilisateur");
                if ($dao->Execute($query,
                array("pk_utilisateur"=> $pk_utilisateur,
                      "usr_id"=> $usr_id,
                      "usr_nom" => $usr_nom,
                      "usr_prenom" => $usr_prenom,
                      "usr_email" => $usr_email,
                      "usr_telephone" => $usr_telephone,
                      "usr_generic" => $usr_generic,
                      "usr_signature" => $usr_signature,
                      "usr_telephone2" => $usr_telephone2,
                      "usr_password" => $usr_password)))
                {
                    $result['msg'] = htmlentities("L'utilisateur a été mis à jour.", ENT_QUOTES, "UTF-8");
                }
                break;
            case 3: // suppression utilisateur
                $query = "DELETE FROM grp_regroupe_usr WHERE grpusr_fk_utilisateur=:pk_utilisateur;";
                if ($dao->Execute($query,
                    array("pk_utilisateur"=> $pk_utilisateur)))
                {
                    $query = "DELETE FROM utilisateur WHERE pk_utilisateur=:pk_utilisateur;";
                    if ($dao->Execute($query,
                        array("pk_utilisateur"=> $pk_utilisateur)))
                    {
                        $result['msg'] = htmlentities("L'utilisateur a été supprimé.", ENT_QUOTES, "UTF-8");
                    }
                }
                break;
            
            case 13: // sélection profil principal
                $profil_id = $request->get("profil_id", "");
                if ($profil_id != "") {
                    $query = "DELETE FROM grp_regroupe_usr WHERE grpusr_fk_groupe_profil=:profil_id AND grpusr_fk_utilisateur=:pk_utilisateur AND grpusr_is_principal<>1;";
                    if ($dao->Execute($query,
                        array("pk_utilisateur"=> $pk_utilisateur, "profil_id"=> $profil_id)))
                    {
                        $query .= "UPDATE grp_regroupe_usr SET grpusr_fk_groupe_profil=:profil_id WHERE grpusr_fk_utilisateur=:pk_utilisateur AND grpusr_is_principal=1;";
                        if ($dao->Execute($query,
                            array("pk_utilisateur"=> $pk_utilisateur, "profil_id"=> $profil_id)))
                        {
                            $result['msg'] = htmlentities("Mise à jour du profil principal.", ENT_QUOTES, "UTF-8");
                        }
                    }
                }
                break;
            case 14: // ajout profil secondaire
                $profil_id = $request->get("profil_id", "");
                if ($profil_id != "") {
                    $query = "INSERT INTO grp_regroupe_usr (grpusr_fk_groupe_profil, grpusr_fk_utilisateur) VALUES (:profil_id, :pk_utilisateur);";
                    if ($dao->Execute($query,
                        array("pk_utilisateur"=> $pk_utilisateur, "profil_id"=> $profil_id)))
                    {
                        $result['msg'] = htmlentities("ajout d'un profil secondaire.", ENT_QUOTES, "UTF-8");
                    }
                    
                }
                break;
            case 15: // suppression profil secondaire
                $profil_id = $request->get("profil_id", "");
                if ($profil_id != "") {
                    $query = "DELETE FROM grp_regroupe_usr WHERE grpusr_fk_utilisateur=:pk_utilisateur AND grpusr_fk_groupe_profil=:profil_id;";
                    if ($dao->Execute($query,
                        array("pk_utilisateur"=> $pk_utilisateur, "profil_id"=> $profil_id)))
                    {
                        $result['msg'] = htmlentities("suppression d'un profil secondaire.", ENT_QUOTES, "UTF-8");
                    }
                
                }
                break;
            default:
                $result['success'] = false;
                $result['msg'] = htmlentities("Le paramètre 'mode' est incorrect.", ENT_QUOTES, "UTF-8");
                Return new JsonResponse($result);
                
                break;
        }
        
        //maj LDAP 
        $utilisateurVO = new UtilisateurVO();
        $utilisateurVO->setDao($dao);
        $utilisateurVO->ldapSyncAction($pk_utilisateur,($mode==3));
        $result['success'] = true;
        
        Return new JsonResponse($result);
        
    }

    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/catalogue/administration/users/users_list", name="catalogue_administration_users_list", options={"expose"=true})
     */
    public function catalogueAdministration_UsersListAction(Request $request)
    {
        $conn = $this->getCatalogueConnection('catalogue');
        include_once (__DIR__ . '/../Resources/templates/Administration/Users/UsersList.php');
        exit();
    }

    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/catalogue/administration/users/users_form", name="catalogue_administration_users_form", options={"expose"=true})
     */
    public function catalogueAdministration_UsersAccueilAction(Request $request)
    {
        $conn = $this->getCatalogueConnection('catalogue');
        $submitUrl = $this->generateUrl('catalogue_administration_submit_form');
        $delUserUrl = $this->generateUrl('catalogue_administration_services_delUser');
        include_once (__DIR__ . '/../Resources/templates/Administration/Users/UsersAccueil.php');
        exit();
    }

    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/catalogue/administration/users/profiles_form", name="catalogue_administration_users_profiles_form", options={"expose"=true})
     */
    public function catalogueAdministration_UsersProfilesAction(Request $request)
    {
        $conn = $this->getCatalogueConnection('catalogue');
        include_once (__DIR__ . '/../Resources/templates/Administration/Users/UsersProfiles.php');
        exit();
    }

    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/catalogue/administration/users/perimetres_form", name="catalogue_administration_users_perimetres_form", options={"expose"=true})
     */
    public function catalogueAdministration_UsersPerimetresAction(Request $request)
    {
        $conn = $this->getCatalogueConnection('catalogue');
        include_once (__DIR__ . '/../Resources/templates/Administration/Users/UsersPerimetres.php');
        return new Response("");
    }

    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/catalogue/administration/users/perimetres_edition_form", name="catalogue_administration_users_perimetres_edition_form", options={"expose"=true})
     */
    public function catalogueAdministration_UsersPerimetresEditionAction(Request $request)
    {
        $conn = $this->getCatalogueConnection('catalogue');
        include_once (__DIR__ . '/../Resources/templates/Administration/Users/UsersPerimetresEdition.php');
        return new Response("");
    }

    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/catalogue/administration/users/importer_form", name="catalogue_administration_users_importer_form", options={"expose"=true})
     */
    public function catalogueAdministration_UsersImporterAction(Request $request)
    {
        $conn = $this->getCatalogueConnection('catalogue');
        $params = $request->query->all();
        $routeSelf = $this->generateUrl('catalogue_administration_users_importer_form', $params);
        $importLDAP = $this->generateUrl('catalogue_administration_users_importer_ldap', $params);
        $importTerritoire = $this->generateUrl('catalogue_administration_users_perimetresimport', array_merge($params, array("tablePerimetre"=>"usr_accede_perimetre")));
        $importEdition = $this->generateUrl('catalogue_administration_users_perimetresimport', array_merge($params, array("tablePerimetre"=>"usr_accede_perimetre_edition")));
        $importAlertEdition = $this->generateUrl('catalogue_administration_users_perimetrescouchesimport', $request->query->all());
        include_once (__DIR__ . '/../Resources/templates/Administration/Users/UsersAdding.php');
        exit();
    }

    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/catalogue/administration/users/importer_ldap", name="catalogue_administration_users_importer_ldap", options={"expose"=true})
     */
    public function catalogueAdministration_UsersImporterLDAPAction(Request $request)
    {
        $conn = $this->getCatalogueConnection('catalogue');
        include_once (__DIR__ . '/../Resources/templates/Administration/Users/UsersImportLdap.php');
        exit();
    }

    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/catalogue/administration/users/perimetresimport/{tablePerimetre}", name="catalogue_administration_users_perimetresimport", options={"expose"=true})
     */
    public function catalogueAdministration_PerimetresUsersImportAction(Request $request, $tablePerimetre)
    {
        $conn = $this->getCatalogueConnection('catalogue');
        include_once (__DIR__ . '/../Resources/templates/Administration/Perimetres/PerimetresUsersImport.php');
        exit();
    }

    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/catalogue/administration/users/perimetrescouchesimport", name="catalogue_administration_users_perimetrescouchesimport", options={"expose"=true})
     */
    public function catalogueAdministration_PerimetresUsersCouchesImportAction(Request $request)
    {
        $conn = $this->getCatalogueConnection('catalogue');
        include_once (__DIR__ . '/../Resources/templates/Administration/Users/AlertEditionImport.php');
        exit();
    }

    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/catalogue/administration/users/exporter_form", name="catalogue_administration_users_exporter_form", options={"expose"=true})
     */
    public function catalogueAdministration_UsersExporterAction(Request $request)
    {
        $conn = $this->getCatalogueConnection('catalogue');
        $route = $this->generateUrl('catalogue_administration_users_exporter_form', $request->query->all());
        if ( $request->getMethod()=="POST" ){
            include_once (__DIR__ . '/../Resources/templates/Administration/Users/UsersExport_sql.php');
        } else {
            include_once (__DIR__ . '/../Resources/templates/Administration/Users/UsersExport.php');
        }
        exit();
    }
}
