<?php
  namespace ProdigeCatalogue\AdminBundle\Common\Modules\BO;
/**
   * @class GrpRegroupeUsrVO
   * @brief  Classe de gestion des regrouprements d'utilisateurs
   * @author Alkante
   */

	//require_once($AdminPath."DAO/ViewObject/ViewObject.php");
  use Prodige\ProdigeBundle\DAOProxy\ViewObject;

	class GrpRegroupeUsrVO extends ViewObject
	{
		static public $PK_GRP_REGROUPE_USR = 0;
		static public $TS = 1;
		static public $GRPUSR_FK_GROUPE_PROFIL = 2;
		static public $GRPUSR_FK_UTILISATEUR = 3;

		static public $GRPUSR_PK_UTILISATEUR = 4;
		static public $GRPUSR_USR_ID = 5;

		static public $GRPUSR_PK_GROUPE_PROFIL = 6;
		static public $GRPUSR_GRP_ID = 7;

		public function __construct( )
		{
			$this->AddProjection( GrpRegroupeUsrVO::$PK_GRP_REGROUPE_USR, "GRP_REGROUPE_USR", "PK_GRP_REGROUPE_USR" );
			$this->AddProjection( GrpRegroupeUsrVO::$TS, "GRP_REGROUPE_USR", "TS" );
			$this->AddProjection( GrpRegroupeUsrVO::$GRPUSR_FK_GROUPE_PROFIL, "GRP_REGROUPE_USR", "GRPUSR_FK_GROUPE_PROFIL" );
			$this->AddProjection( GrpRegroupeUsrVO::$GRPUSR_FK_UTILISATEUR, "GRP_REGROUPE_USR", "GRPUSR_FK_UTILISATEUR" );

			// linked fields.
			$this->AddProjection( GrpRegroupeUsrVO::$GRPUSR_PK_UTILISATEUR, "UTILISATEUR", "PK_UTILISATEUR" );
			$this->AddProjection( GrpRegroupeUsrVO::$GRPUSR_USR_ID, "UTILISATEUR", "USR_ID" );
			$this->AddProjection( GrpRegroupeUsrVO::$GRPUSR_PK_GROUPE_PROFIL, "GROUPE_PROFIL", "PK_GROUPE_PROFIL");
			$this->AddProjection( GrpRegroupeUsrVO::$GRPUSR_GRP_ID, "GROUPE_PROFIL", "GRP_ID");

			// relastionship building
			$this->AddEqualsRelation( GrpRegroupeUsrVO::$GRPUSR_FK_GROUPE_PROFIL, GrpRegroupeUsrVO::$GRPUSR_PK_GROUPE_PROFIL );
			$this->AddEqualsRelation( GrpRegroupeUsrVO::$GRPUSR_FK_UTILISATEUR, GrpRegroupeUsrVO::$GRPUSR_PK_UTILISATEUR );
		}
                
                /**
                * Force la synchronisation avec le LDAP edit/add
                * @param type $conn
                * @param array $ldapConf
                * @param array $ordinalList
                * @param array $valueList
                * @param string $pk
                * @param bool $delete
                */
                public function ldapSyncAction($pk,$delete = false){
                   $conn = $this->getDao()->getConnection();
                   include(__DIR__.'/../../../Resources/templates/Administration/Users/UsersSyncLdap.php');
                }
                
                /**
                 * Force la synchronisation avec le LDAP edit/add dans le cadre d'une selection
                 * @param type $conn
                 * @param array $ldapConf
                 * @param array $ordinalList
                 * @param array $valueList
                 * @param string $pk
                 * @param bool $delete
                */
                public function ldapSyncSelectionAction($pk,$delete = false){
                   $conn = $this->getDao()->getConnection();
                   include(__DIR__.'/../../../Resources/templates/Administration/Profiles/ProfilesTraitementsSyncLdap.php');
                }
            
               
                public function ldapSyncActionClean($ordinal,$pk) {
                    $conn = $this->getDao()->getConnection();
                    if ($ordinal == GrpRegroupeUsrVO::$GRPUSR_FK_GROUPE_PROFIL)
                    {
                        include(__DIR__.'/../../../Resources/templates/Administration/Profiles/ProfilesSyncLdapClean.php');
                    }
                }
	}
?>