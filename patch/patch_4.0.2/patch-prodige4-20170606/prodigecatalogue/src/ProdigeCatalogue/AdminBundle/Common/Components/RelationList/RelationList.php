<?php
  namespace ProdigeCatalogue\AdminBundle\Common\Components\RelationList;

	//require_once($AdminPath."/Administration/AccessRights/AccessRights.php");
  use ProdigeCatalogue\AdminBundle\Common\AccessRights\AccessRights;
  use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;

  use ProdigeCatalogue\AdminBundle\Controller\AlertSaveController;
use Symfony\Component\HttpFoundation\Response;
use Prodige\ProdigeBundle\DAOProxy\ViewObject;
  	/**
	 * Classe constituant les listes
	 * @author Alkante
	 *
	 */
	class RelationList
	{
		private $Name;
		private $RelationViewObject;
		private $RelationKeyOrdinal;
		private $RelationValueOrdinal;
		private $RelationTitle;
		private $RelationPKRestrictionOrdinal;
		private $RelationPKRestrictionValue;
		private $RelationFKTable;

		private $TableViewObject;
		private $TableTitle;
		private $TableKeyOrdinal;
		private $TableValueOrdinal;

		private $ViewObject;
		private $ResultSet;
		private $KeyOrdinal;
		private $ValueOrdinal;
		private $Target;
		private $Page;
		private $Width;
		private $Height;
		private $Form;

    private $Disabled;
    
    private $InsertMode;
    private $InverseFields;
		private $bCheckBox;
    
    private $strDetectError;

		/*
		public function __construct( $name, $viewObject, $keyOrdinal, $valueOrdinal, $target, $page, $width, $height )
		{
			$this->Name = $name;
			$this->ViewObject = $viewObject;
			$this->KeyOrdinal = $keyOrdinal;
			$this->ValueOrdinal = $valueOrdinal;
			$this->Target = $target;
			$this->Page = $page;
			$this->Width = $width;
			$this->Height = $height;

			$this->FillList();
		}
		*/
		// ############################################

		public function __construct( 	$name,
										$relationTitle,
										ViewObject $relationViewObject,
										$relationKeyOrdinal,
										$relationValueOrdinal,
										$relationPKRestrictionOrdinal,
										$relationPKRestrictionValue,
										$relationFKTable,
		 								ViewObject $tableViewObject,
		 								$tableTitle,
		 								$tableKeyOrdinal,
										$tableValueOrdinal,
										$page,
										$target,
										$form,
                    $insertMode = true,
                    $inverseFields = false,
                    $bCheckBox = true,
                    $strDetectError = "",
                    $readOnly = false
    ) {
      //global $AdminPath;
			$this->Name = $name;
			$this->RelationTitle = $relationTitle;
			$this->RelationViewObject = $relationViewObject;
			$this->RelationKeyOrdinal = $relationKeyOrdinal;
			$this->RelationValueOrdinal = $relationValueOrdinal;
			$this->RelationPKRestrictionOrdinal = $relationPKRestrictionOrdinal;
			$this->RelationPKRestrictionValue = $relationPKRestrictionValue;
			$this->RelationFKTable = $relationFKTable;

			$this->TableViewObject = $tableViewObject;
			$this->TableTitle = $tableTitle;
			$this->TableKeyOrdinal = $tableKeyOrdinal;
			$this->TableValueOrdinal = $tableValueOrdinal;
			$this->Form = $form;

      $this->Page = $_SERVER['REQUEST_URI'];
			$this->Target = Ressources::$IFRAME_NAME_MAIN;

      
      $this->InsertMode = $insertMode;
      $this->InverseFields = $inverseFields;
      
      $this->bCheckBox = $bCheckBox;
			
      $this->strDetectError = $strDetectError;
      
      $this->readOnly = $readOnly;
      
      $accessRights = new AccessRights();

      $this->canUpdate = true;
			if ( !$accessRights->CanUpdate( $this->RelationViewObject, $this->RelationPKRestrictionOrdinal, $this->RelationPKRestrictionValue ) || $this->readOnly ) {
				$this->Disabled = " DISABLED=\"DISABLED\" ";
        $this->canUpdate = false;
      }

      
      $iPart = (isset($_GET["iPart"]) ? $_GET["iPart"] : 1);
      if ( $iPart==Ressources::$RELATIONLIST_ADD_TO_RELATION_ACTION ){
        $this->SaveSelection();
        return new Response("");
      }
      
		  $this->FillLists();
		}
    
    private function SaveSelection()
    {
      $selection = (isset($_POST["selection"]) ? $_POST["selection"] : array());
      // update association in data table
      if ( !$this->InsertMode ){
        
        $ordinalList = array();
        $valueList = array();
        $ordinalList[] .= ( $this->InverseFields ? $this->RelationKeyOrdinal : $this->RelationPKRestrictionOrdinal );
        $valueList[]   .= "NULL";
        
        $ordinalRestriction = $this->RelationPKRestrictionOrdinal;
        $valueRestriction   = $this->RelationPKRestrictionValue;

        if(method_exists($this->RelationViewObject,"ldapSyncActionClean"))
            $this->RelationViewObject->ldapSyncActionClean($ordinalRestriction,$valueRestriction);
        
        $this->RelationViewObject->Update( $ordinalList, $valueList, $ordinalRestriction, $valueRestriction );
        $this->RelationViewObject->Commit();


  
        
        foreach ($selection as $selection_id){
          $ordinalList = array();
          $valueList = array();
          
          if ( $this->InverseFields ){
            // set data_key=<data_id>
            $ordinalList[] .= $this->RelationKeyOrdinal;
            $valueList[]   .= $selection_id;
            
            // where selection_key=<selection_id>
            $ordinalRestriction = $this->RelationPKRestrictionOrdinal;
            $valueRestriction   = $this->RelationPKRestrictionValue;
          }
          else {
            // set data_key=<data_id>
            $ordinalList[] .= $this->RelationPKRestrictionOrdinal;
            $valueList[]   .= $this->RelationPKRestrictionValue;
            
            // where selection_key=<selection_id>
            $ordinalRestriction = $this->RelationKeyOrdinal;
            $valueRestriction   = $selection_id;
          }

          $this->RelationViewObject->Update( $ordinalList, $valueList, $ordinalRestriction, $valueRestriction );
          $this->RelationViewObject->Commit();
          if(method_exists($this->RelationViewObject,"ldapSyncSelectionAction"))
            $this->RelationViewObject->ldapSyncSelectionAction($valueRestriction);
          
        }
      }
      // update association in join table
      else{
        
        // suppression de tous les enregistrements relatifs à la donnée de travail
        if(method_exists($this->RelationViewObject,"ldapSyncActionClean"))
            $this->RelationViewObject->ldapSyncActionClean($this->RelationPKRestrictionOrdinal,$this->RelationPKRestrictionValue);
        $this->RelationViewObject->DeleteRow( $this->RelationPKRestrictionOrdinal, $this->RelationPKRestrictionValue );
        $this->RelationViewObject->Commit();
        
        foreach ($selection as $selection_id){
          $ordinalList = array();
          $valueList = array();
          $ordinalList[] .= $this->RelationPKRestrictionOrdinal;
          $ordinalList[] .= $this->RelationFKTable;
          $valueList[] .= $this->RelationPKRestrictionValue;
          $valueList[] .= $selection_id;
    
          $this->RelationViewObject->InsertValues( $ordinalList, $valueList );
          $this->RelationViewObject->Commit();
          if(method_exists($this->RelationViewObject,"ldapSyncSelectionAction"))
            $this->RelationViewObject->ldapSyncSelectionAction($selection_id);
        }
      }
      $strJs = "parent.reloadPanel(parent.getActiveTabId('module_details'), parent.getActiveTabId('module_details').replace(/tab_(\d+)_\d+/, '\$1'));";
      if ( $this->InverseFields )
        $strJs .= "parent.reloadGrid(parent.getActiveTabId('module_details').replace(/tab_(\d+)_\d+/, '\$1'), ".$this->RelationPKRestrictionValue.");";
      //AlertSaveDone($strJs, false);
      if(method_exists($this->RelationViewObject,"ldapSyncAction"))
        $this->RelationViewObject->ldapSyncAction($this->RelationPKRestrictionValue);
      if(method_exists($this->RelationViewObject, "syncThesaurus"))
        $this->RelationViewObject->syncThesaurus($this->Form);
      AlertSaveController::AlertSaveDone($strJs, false);

    }
		// ############################################

		/**
		 * @brief Remplissage de la liste et affichage
		 * @return 
		 */
		private function FillLists()
		{
			$this->RelationClick();
			$this->TableClick();
      
      $tabData = array();
      if ( $this->bCheckBox && $this->canUpdate ){
        $tabData[-1] = array("name"=>"<b>".$this->RelationTitle."</b>", "id"=>"-1", "selection"=>"<a onclick=\"SelectAll(true)\">Tous</a>&nbsp;/&nbsp;<a onclick=\"SelectAll(false)\">Aucun</a>");
      }
      
      if ( false ){
        echo "<br>Relation (in) = <br><br>".$this->RelationViewObject->GetSQLStmt();
        echo "<br><br>Table (notin) = <br><br>".$this->TableViewObject->GetSQLStmt();
      }
      
      $tabTitle = array("name"=>$this->TableTitle, "selection"=>$this->RelationTitle);
      
      $ctrl_type = ($this->bCheckBox ? "checkbox" : "radio");
      

      $this->RelationViewObject->AddRestriction( $this->RelationPKRestrictionOrdinal, $this->RelationPKRestrictionValue );
      $this->RelationViewObject->AddOrder($this->RelationValueOrdinal);
      $this->RelationViewObject->Open();
      $this->RelationViewObject->GetResultSet()->First();
      
      if ( $this->RelationViewObject->GetResultSet()->EOF() ){
        //$tabData[-1]["name"] = "<b>".$this->TableTitle."</b>";
      }
  
      if ( $this->RelationViewObject->GetResultSet()->GetNbRows()>ViewObject::LIMIT_GROUPING ){
          $names = array();
          while ( !$this->RelationViewObject->GetResultSet()->EOF() )
          {
            $PK = $this->RelationViewObject->GetResultSet()->Read($this->RelationFKTable);
            $names[] = "<input type=\"".$ctrl_type."\" value=\"".$PK."\"" .
                                          " name=\"selection[]\"" .
                                          " checked=\"checked\"".
                                          " onclick=\"onClickSelection(this);\"" .
                                          $this->Disabled.
                                          "/>&nbsp;".$this->RelationViewObject->GetResultSet()->Read($this->RelationValueOrdinal);
            $this->RelationViewObject->GetResultSet()->Next();
            if ( count($names)>ViewObject::LIMIT_READING ) {
                $hiddens = array();
                while ( !$this->RelationViewObject->GetResultSet()->EOF() )
                {
                    $PK = $this->RelationViewObject->GetResultSet()->Read($this->RelationFKTable);
                    $hiddens[] = "<input type=\"".$ctrl_type."\" value=\"".$PK."\" name=\"selection[]\" checked=\"checked\"" .$this->Disabled."/>";
                    $this->RelationViewObject->GetResultSet()->Next();
                }
                if ( !empty($hiddens) )
                    $names[] = "<center>...".($this->RelationViewObject->GetResultSet()->GetNbRows()-count($names))." autres valeurs...</center><div style='display:none'>".implode("", $hiddens)."</div>";
                break;
            }
          }
          $tabData["in"]["id"] = "in";
          $tabData["in"]["name"] = implode("<br/>", $names);
          $tabData["in"]["selection"] = "";
      } else {
          while ( !$this->RelationViewObject->GetResultSet()->EOF() )
          {
            $PK = $this->RelationViewObject->GetResultSet()->Read($this->RelationFKTable);
            $tabData[$PK]["id"] = $PK;
            $tabData[$PK]["name"] = $this->RelationViewObject->GetResultSet()->Read($this->RelationValueOrdinal);
            $tabData[$PK]["selection"] = "<input type=\"".$ctrl_type."\" value=\"".$PK."\"" .
                                          " name=\"selection[]\"" .
                                          " checked=\"checked\"".
                                          " onclick=\"onClickSelection(this);\"" .
                                          $this->Disabled.
                                          "/>";
            $this->RelationViewObject->GetResultSet()->Next();
          }
      }
      
      $this->TableViewObject->AddOrder($this->TableValueOrdinal);
      $this->TableViewObject->Open();
      
      $this->TableViewObject->GetResultSet()->First();
      
      if ( ( $this->bCheckBox && !$this->TableViewObject->GetResultSet()->EOF() )
        || (!$this->bCheckBox && !$this->TableViewObject->GetResultSet()->EOF() && !empty($tabData)) 
      ){
        $tabData["-2"]["id"] = "-2";
        $tabData["-2"]["name"] = "<b>".$this->TableTitle."</b>"; 
        $tabData["-2"]["selection"] = "";
      }

      
      // FILL THE SELECT LIST
      if ( $this->TableViewObject->GetResultSet()->GetNbRows()>ViewObject::LIMIT_GROUPING ){
          $names = array();
          while ( !$this->TableViewObject->GetResultSet()->EOF() )
          {
            $PK = $this->TableViewObject->GetResultSet()->Read($this->TableKeyOrdinal);
            $names[] = "<input type=\"".$ctrl_type."\" value=\"".$PK."\"" .
                                          " name=\"selection[]\"" .
                                          " onclick=\"onClickSelection(this);\"" .
                                          $this->Disabled.
                                          "/>&nbsp;".$this->TableViewObject->GetResultSet()->Read($this->TableValueOrdinal);
            $this->TableViewObject->GetResultSet()->Next();
            if ( count($names)>ViewObject::LIMIT_READING ) {
                $hiddens = array();
                while ( !$this->TableViewObject->GetResultSet()->EOF() )
                {
                    $PK = $this->TableViewObject->GetResultSet()->Read($this->TableKeyOrdinal);
                    $hiddens[] = "<input type=\"".$ctrl_type."\" value=\"".$PK."\" name=\"selection[]\" " .$this->Disabled."/>";
                    $this->TableViewObject->GetResultSet()->Next();
                }
                if ( !empty($hiddens) )
                    $names[] = "<center>...".($this->TableViewObject->GetResultSet()->GetNbRows()-count($names))." autres valeurs...</center><div style='display:none'>".implode("", $hiddens)."</div>";
                break;
            }
          }
          $tabData["out"]["id"] = "out";
          $tabData["out"]["name"] = implode("<br/>", $names);
          $tabData["out"]["selection"] = "";
      } else {
          while ( !$this->TableViewObject->GetResultSet()->EOF() )
          {
            $PK = $this->TableViewObject->GetResultSet()->Read($this->TableKeyOrdinal);
            $tabData[$PK]["id"] = $PK;
            $tabData[$PK]["name"] = $this->TableViewObject->GetResultSet()->Read($this->TableValueOrdinal); 
            $tabData[$PK]["selection"] = "<input type=\"".$ctrl_type."\" value=\"".$PK."\"" .
                                          " name=\"selection[]\"" .
                                          " onclick=\"onClickSelection(this);\"" .
                                          $this->Disabled.
                                          "/>";
            $this->TableViewObject->GetResultSet()->Next();
          }
      }
      

      $strFields = "{name : 'id'}";
      $strColumns = "";
      $bNotTitle = false;
      foreach ($tabTitle as $dataIndex=>$title){
        $strFields .= ($strFields=="" ? "" : ", ").  "{name : '".$dataIndex."'}";
        $strColumns .= ($strColumns=="" ? "" : ", "). 
                       "{" .
                         " id: 'col_".$dataIndex."', dataIndex: '".$dataIndex."', " .
                         " header: \"<b>".addslashes($title)."</b>\"" .
                         ($bNotTitle ? ", align:'center', width:100" : "").
                       "}";
        if ( !$bNotTitle ) $bNotTitle = true;
      }
      $classSelection = "x-grid3-row-alt";
      ?>
      <script type='text/javascript'>
      function onClickSelection(oCheck)
      {
        <?php if ( !$this->bCheckBox ) echo "return;" ?>
        var element = new Ext.Element(adminTabPanelMain.getActiveTab().gridList.view.findRow(oCheck), true);
        if (oCheck.checked)
          element.addClass('<?php echo $classSelection ?>');
        else
          element.removeClass('<?php echo $classSelection ?>');
      }
      function SelectAll(bSelect)
      {
        var oForm = document.getElementById('form_<?php echo $this->Name ?>');
        var oChecks = oForm.elements["selection[]"];
        if ( !oChecks.item ){
          oChecks.checked = bSelect;
          oChecks.onclick();
          return;
        }
        for (var i=0; i <?php echo "<"?> oChecks.length; i++){
          oChecks[i].checked = bSelect;
          oChecks[i].onclick();
        }
      }
      function ChangeEnabled(oManage, OBJET_PK, listTraitement)
      {
        var tabTraitement = listTraitement.split(",");
        for (var i=0; i <?php echo "<"?> tabTraitement.length; i++){
          var oTraitement = document.getElementById('traitement['+OBJET_PK+']['+tabTraitement[i]+']');
          oTraitement.disabled = !oManage.checked;
        }
      }
      
      adminTabPanelMain.getActiveTab().add(new Ext.FormPanel({
        border: false,
        autoWidth : true,
        autoHeight : true,
        refOwner : adminTabPanelMain.getActiveTab(),
        ref : "../formPanel",
        renderTo : adminTabPanelMain.getActiveTab().id,
        standardSubmit: true,
        baseParams: {
            foo: 'bar'
        },
        id : 'panelform_<?php echo $this->Name ?>',
        formId : 'form_<?php echo $this->Name ?>',
        url: '<?php echo str_replace("iPart=1", "iPart=".Ressources::$RELATIONLIST_ADD_TO_RELATION_ACTION, $_SERVER["REQUEST_URI"])?>',
        items: [
          new Ext.grid.GridPanel({
            ref : "../../gridList",
            region : 'center',
            height : 400,
            border : true,
            store : new Ext.data.Store({
                reader : new Ext.data.JsonReader({
                idProperty: 'id',
                fields: [
                  <?php echo $strFields ?>
                ]
              }),
              data : <?php echo json_encode(array_values($tabData)); ?>
            }),
            id : 'grid_<?php echo $this->Name ?>',
            colModel: new Ext.grid.ColumnModel({
                defaults: {
                  sortable: false, hideable:false, menuDisabled:true, resizable: true
                },
                columns: [
                  <?php echo $strColumns ?>
                ]
            }),
            hideHeaders : true,
            
            view: new Ext.grid.GridView({
              // render rows as they come into viewable area.
              scrollDelay: false,
              //cleanDelay: 200,
              
              deferEmptyText : false,
              forceFit: true,
              showPreview: true, // custom property
              enableRowBody: true, // required to create a second, full-width row to show expanded Record data
              
              // for correlated sdrs
              getRowClass: function(record, index, rowParam, store) {
                if ( record.id=="-1" || record.id=="-2" ){
                  return "x-grid3-header";
                }
                <?php if ( $this->strDetectError!="" ){ ?>
                  if ( record.get("name").indexOf("<?php echo $this->strDetectError; ?>")==-1 )
                    return "data-error";
                <?php } ?>
                return "";
              }
            }),
            loadMask : {msg : "Chargement en cours..."},
            disableSelection : true,
            autoExpandColumn : 'col_name',
            iconCls: 'icon-grid',
            margins : '5 5 5 5',
            buttonAlign : 'center',
            border : true,
            buttons : [
              <?php if ( $this->canUpdate ) { ?>
              new Ext.Button({
                defaults : {buttonAlign:'center'}, 
                text : 'Enregistrer la configuration',
                margins : '5 5 5 5',
                handler : function(){
                  var oForm = document.getElementById('form_<?php echo $this->Name ?>');
                  oForm.action = '<?php echo str_replace("iPart=1", "iPart=".Ressources::$RELATIONLIST_ADD_TO_RELATION_ACTION, $_SERVER["REQUEST_URI"])?>';
                  oForm.target = '<?php echo Ressources::$IFRAME_NAME_MAIN; ?>';
                  oForm.submit();
                }
              })
              <?php } ?>
            ],
            listeners : {
              afterrender : {
                fn : function(grid){
                  grid.resizeEnable = true;
                  updateGridListSize(grid);
                  grid.on("afterrender", updateGridListSize);
                  Ext.getCmp('module_details').getActiveTab().gridList = grid;
                  tabGridList.push(grid);
                },
                delay : 100, 
                single : true
              }
            }
          })
        ]
      }));
      
      </script>
      <?php
		}

		// ############################################

		/**
     * @brief Remplissage de la liste de relation et affichage
     * @return 
     */
		private function FillRelationList()
		{
			$returnHTML = "";

			$this->RelationViewObject->AddRestriction( $this->RelationPKRestrictionOrdinal, $this->RelationPKRestrictionValue );

			$this->RelationViewObject->Open();
			$this->RelationViewObject->GetResultSet()->First();

			$returnHTML = '
				<select style="width:'.$this->Width.';height:'.$this->Height.';"  '.$this->Disabled.'
					 	multiple id="'.$this->Name.'Relation"
					 	language="javascript"
					 	ondblclick="'.$this->Name.'Relation_ondblclick(this);"
				>';

			// FILL THE SELECT LIST
			while ( !$this->RelationViewObject->GetResultSet()->EOF() )
			{
				$returnHTML = $returnHTML.'
					<option value="'.$this->RelationViewObject->GetResultSet()->Read($this->RelationKeyOrdinal).'">
						'.$this->RelationViewObject->GetResultSet()->Read($this->RelationValueOrdinal).'
					</option>';

				$this->RelationViewObject->GetResultSet()->Next();
			}

			 $returnHTML = $returnHTML.'</select>';

			 return $returnHTML;
		}

		// ############################################

		/**
     * @brief Affichage de la fonction javascript qui gère le clic sur une relation
     * @return 
     */
		private function RelationClick()
		{return;
			print('
				<script language="javascript" type="text/javascript">
					<!--
						function '.$this->Name.'Relation_ondblclick(selectList)
						{
		    				var myString="'.$this->Page.(strpos($this->Page, "?")===false ? "?" : "&").'Action='.Ressources::$RELATIONLIST_REMOVE_FROM_RELATION_ACTION.$this->strActionParam.'&Id=";
		    				myString = myString+"'.$this->RelationPKRestrictionValue.'";
		    				myString = myString+"&RR=";
		    				myString = myString + selectList.options[selectList.selectedIndex].value;
		    				window.open(myString,"'.$this->Target.'");
						}
					// -->
				</script>
			');
		}

		/**
     * @brief Affichage de la fonction javascript qui gère le clic sur une table
     * @return 
     */
		private function TableClick()
		{return;
			print('
				<script language="javascript" type="text/javascript">
					<!--
						function '.$this->Name.'Table_ondblclick(selectList)
						{
		    				var myString="'.$this->Page.(strpos($this->Page, "?")===false ? "?" : "&").'Action='.Ressources::$RELATIONLIST_ADD_TO_RELATION_ACTION.$this->strActionParam.'&Id=";
		    				myString = myString+"'.$this->RelationPKRestrictionValue.'";
		    				myString = myString+"&RR=";
		    				myString = myString + selectList.options[selectList.selectedIndex].value;

		    				window.open(myString,"'.$this->Target.'");
						}
					// -->
				</script>
			');
		}

		private function FillTableList()
		{
			$returnHTML = "";

			$this->TableViewObject->Open();
			$this->TableViewObject->GetResultSet()->First();

			$returnHTML = '
				<select style="width:'.$this->Width.';height:'.$this->Height.';"  '.$this->Disabled.'
					 	multiple id="'.$this->Name.'Table"
					 	language="javascript"
					 	ondblclick="'.$this->Name.'Table_ondblclick(this);"
				>';

			// FILL THE SELECT LIST
			while ( !$this->TableViewObject->GetResultSet()->EOF() )
			{
				$returnHTML = $returnHTML.'
					<option value="'.$this->TableViewObject->GetResultSet()->Read($this->TableKeyOrdinal).'">
						'.$this->TableViewObject->GetResultSet()->Read($this->TableValueOrdinal).'
					</option>';

				$this->TableViewObject->GetResultSet()->Next();
			}

			 $returnHTML = $returnHTML.'</select>';

			 return $returnHTML;
		}

		// ############################################
	}

?>
