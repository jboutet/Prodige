
-- TODO drop view and recreate to change type 
--ALTER TABLE fiche_metadonnees ALTER COLUMN fmeta_id TYPE bigint USING (fmeta_id::bigint);

set search_path='catalogue','public';

alter table fiche_metadonnees drop constraint un_fmeta_id;
drop index if exists un_fmeta_id;

drop view cartes_sdom_pub;
drop view cartes_sdom;
drop view couche_sdom;
drop view metadonnees_sdom_pub;
drop view metadata_list;
drop view statistique_dernieres_mises_a_jour;
drop view statistique_dernieres_nouveautes;
drop view statistique_liste_metadata_publiees;

drop view statistique_liste_carte_publiees;
drop view statistique_liste_couche_publiees;
drop view statistique_liste_metadonnees_prod_date_couche;
drop view statistique_metadata_date_revision;
drop view statistique_metadata_date_creation;
drop view statistique_metadata_date_publication;
drop view statistique_nombre_cartes_publiees_domaine;
drop view statistique_nombre_cartes_publiees_sousdomaine;
drop view statistique_nombre_couches_publiees_domaine;
drop view statistique_nombre_couches_publiees_sousdomaine;
drop view statistique_ogc;
drop view v_desc_metadata;


create view cartes_sdom as 
 SELECT carte_projet.cartp_format,
    carte_projet.pk_carte_projet,
    carte_projet.cartp_description,
    carte_projet.cartp_nom,
    sous_domaine.pk_sous_domaine,
    sous_domaine.ssdom_nom,
    stockage_carte.pk_stockage_carte,
    stockage_carte.stkcard_path,
    acces_serveur.accs_adresse,
    acces_serveur.path_consultation,
    acces_serveur.path_administration,
    acces_serveur.path_carte_statique,
    acces_serveur.accs_adresse_admin,
    acces_serveur.accs_login_admin,
    acces_serveur.accs_pwd_admin,
    acces_serveur.accs_service_admin,
    fiche_metadonnees.pk_fiche_metadonnees,
    fiche_metadonnees.fmeta_id,
    CASE WHEN operationid=0 THEN 4
         ELSE 1
    END as statut,
    domaine.pk_domaine,
    domaine.dom_rubric AS dom_coherence,
    domaine.dom_nom,
    carte_projet.cartp_wms
   FROM domaine
     JOIN sous_domaine ON domaine.pk_domaine = sous_domaine.ssdom_fk_domaine
     JOIN ssdom_visualise_carte ON sous_domaine.pk_sous_domaine = ssdom_visualise_carte.ssdcart_fk_sous_domaine
     JOIN carte_projet ON ssdom_visualise_carte.ssdcart_fk_carte_projet = carte_projet.pk_carte_projet
     LEFT JOIN fiche_metadonnees ON carte_projet.cartp_fk_fiche_metadonnees = fiche_metadonnees.pk_fiche_metadonnees
     inner join metadata on fiche_metadonnees.fmeta_id::bigint = metadata.id
     left join operationallowed on ( metadata.id = operationallowed.metadataid and operationallowed.operationid=0 and operationallowed.groupid=1)
     JOIN stockage_carte ON carte_projet.cartp_fk_stockage_carte = stockage_carte.pk_stockage_carte
     LEFT JOIN acces_serveur ON acces_serveur.pk_acces_serveur = stockage_carte.stk_server;

create view cartes_sdom_pub as      
     SELECT cartes_sdom.cartp_format,
    cartes_sdom.pk_carte_projet,
    cartes_sdom.cartp_description,
    cartes_sdom.cartp_nom,
    cartes_sdom.pk_sous_domaine,
    cartes_sdom.ssdom_nom,
    cartes_sdom.pk_stockage_carte,
    cartes_sdom.stkcard_path,
    cartes_sdom.accs_adresse,
    cartes_sdom.path_consultation,
    cartes_sdom.path_administration,
    cartes_sdom.path_carte_statique,
    cartes_sdom.accs_service_admin,
    cartes_sdom.pk_fiche_metadonnees,
    cartes_sdom.fmeta_id,
    cartes_sdom.statut,
    cartes_sdom.pk_domaine,
    cartes_sdom.dom_coherence,
    cartes_sdom.dom_nom,
    cartes_sdom.accs_adresse_admin,
    cartes_sdom.cartp_wms
   FROM cartes_sdom
  WHERE cartes_sdom.statut = 4;
  
  create view couche_sdom as 
   SELECT domaine.pk_domaine,
    domaine.dom_rubric AS dom_coherence,
    domaine.dom_nom,
    sous_domaine.pk_sous_domaine,
    sous_domaine.ssdom_nom,
    fiche_metadonnees.pk_fiche_metadonnees,
    fiche_metadonnees.fmeta_id,
    CASE WHEN operationid=0 THEN 4
         ELSE 1
    END as statut,
    couche_donnees.pk_couche_donnees,
    couche_donnees.couchd_nom,
    couche_donnees.couchd_description,
    couche_donnees.couchd_type_stockage,
    couche_donnees.couchd_emplacement_stockage,
    couche_donnees.couchd_wms,
    couche_donnees.couchd_wfs,
    couche_donnees.couchd_download,
    metadata.uuid,
    couche_donnees.couchd_restriction,
    couche_donnees.couchd_visualisable,
    couche_donnees.changedate
   FROM domaine
     JOIN sous_domaine ON domaine.pk_domaine = sous_domaine.ssdom_fk_domaine
     JOIN ssdom_dispose_couche ON sous_domaine.pk_sous_domaine = ssdom_dispose_couche.ssdcouch_fk_sous_domaine
     JOIN fiche_metadonnees ON ssdom_dispose_couche.ssdcouch_fk_couche_donnees = fiche_metadonnees.fmeta_fk_couche_donnees
     JOIN couche_donnees ON couche_donnees.pk_couche_donnees = fiche_metadonnees.fmeta_fk_couche_donnees
     JOIN metadata ON metadata.id = fiche_metadonnees.fmeta_id::bigint
     left join operationallowed on ( metadata.id = operationallowed.metadataid and operationallowed.operationid=0 and operationallowed.groupid=1);
     
 create view metadonnees_sdom_pub
 as  SELECT domaine.pk_domaine,
    domaine.dom_rubric AS dom_coherence,
    domaine.dom_nom,
    sous_domaine.pk_sous_domaine,
    sous_domaine.ssdom_nom,
    fiche_metadonnees.pk_fiche_metadonnees,
    fiche_metadonnees.fmeta_id,
    fiche_metadonnees.fmeta_fk_couche_donnees
   FROM domaine
     JOIN sous_domaine ON domaine.pk_domaine = sous_domaine.ssdom_fk_domaine
     JOIN ssdom_dispose_couche ON sous_domaine.pk_sous_domaine = ssdom_dispose_couche.ssdcouch_fk_sous_domaine
     JOIN fiche_metadonnees ON ssdom_dispose_couche.ssdcouch_fk_couche_donnees = fiche_metadonnees.fmeta_fk_couche_donnees
     JOIN metadata ON metadata.id = fiche_metadonnees.fmeta_id::bigint
     INNER JOIN operationallowed on ( metadata.id = operationallowed.metadataid and operationallowed.operationid=0 and operationallowed.groupid=1);

 create view  statistique_liste_carte_publiees as SELECT carte_projet.pk_carte_projet,
    carte_projet.cartp_nom AS "nom de la carte",
    carte_projet.cartp_description AS "résumé",
    carte_projet.cartp_format AS format,
    stockage_carte.stkcard_path AS stockage,
    metadata.changedate,
    metadata.createdate,
    metadata.id AS "id metadata",
    metadata.uuid AS "uuid metadata"
   FROM carte_projet
     JOIN stockage_carte ON carte_projet.cartp_fk_stockage_carte = stockage_carte.pk_stockage_carte
     JOIN fiche_metadonnees ON carte_projet.cartp_fk_fiche_metadonnees = fiche_metadonnees.pk_fiche_metadonnees
     JOIN metadata ON fiche_metadonnees.fmeta_id::text = metadata.id::text
     INNER JOIN operationallowed on ( metadata.id = operationallowed.metadataid and operationallowed.operationid=0 and operationallowed.groupid=1);
     
create view statistique_liste_couche_publiees as      
  SELECT couche_donnees.pk_couche_donnees,
    couche_donnees.couchd_nom AS "nom de la couche",
    couche_donnees.couchd_description AS "résumé",
    couche_donnees.couchd_type_stockage AS "couche vecteur ?",
    couche_donnees.couchd_emplacement_stockage AS "emplacement de stockage",
    couche_donnees.couchd_wms AS "Publiée en WMS ?",
    couche_donnees.couchd_wfs AS "Publiée en WFS ?",
    metadata.changedate,
    metadata.createdate,
    metadata.id AS "id metadata",
    metadata.uuid AS "uuid metadata"
   FROM couche_donnees
     JOIN fiche_metadonnees ON couche_donnees.pk_couche_donnees = fiche_metadonnees.fmeta_fk_couche_donnees
     JOIN metadata ON fiche_metadonnees.fmeta_id::text = metadata.id::text
     INNER JOIN operationallowed on ( metadata.id = operationallowed.metadataid and operationallowed.operationid=0 and operationallowed.groupid=1);
     

create view   statistique_metadata_date_revision as 
SELECT metadata.id,
    substr(metadata.data, strpos(metadata.data, '<gmd:date><gmd:CI_Date><gmd:date><gco:DateTime>'::text) + 47, strpos(metadata.data, '</gco:DateTime></gmd:date><gmd:dateType><gmd:CI_DateTypeCodecodeList="./resources/codeList.xml#CI_DateTypeCode"codeListValue="creation"/></gmd:dateType></gmd:CI_Date></gmd:date>'::text) - strpos(metadata.data, '<gmd:date><gmd:CI_Date><gmd:date><gco:DateTime>'::text) - 47) AS date
   FROM ( SELECT metadata_1.id,
            replace(metadata_1.data, ' '::text, ''::text) AS data
           FROM ( SELECT metadata_2.id,
                    replace(metadata_2.data, '

'::text, ''::text) AS data
                   FROM metadata metadata_2) metadata_1) metadata
     JOIN fiche_metadonnees ON metadata.id::text = fiche_metadonnees.fmeta_id::text
     INNER JOIN operationallowed on ( metadata.id = operationallowed.metadataid and operationallowed.operationid=0 and operationallowed.groupid=1)
  WHERE strpos(metadata.data, '<gmd:date><gmd:CI_Date><gmd:date><gco:DateTime>'::text) > 0 AND strpos(metadata.data, '</gco:DateTime></gmd:date><gmd:dateType><gmd:CI_DateTypeCodecodeList="./resources/codeList.xml#CI_DateTypeCode"codeListValue="creation"/></gmd:dateType>'::text) > 0 
AND length(substr(metadata.data, strpos(metadata.data, '<gmd:date><gmd:CI_Date><gmd:date><gco:DateTime>'::text) + 47, strpos(metadata.data, '</gco:DateTime></gmd:date><gmd:dateType><gmd:CI_DateTypeCodecodeList="./resources/codeList.xml#CI_DateTypeCode"codeListValue="creation"/></gmd:dateType></gmd:CI_Date></gmd:date>'::text) - strpos(metadata.data, '<gmd:date><gmd:CI_Date><gmd:date><gco:DateTime>'::text) - 47)) < 11;

create view statistique_metadata_date_creation as
SELECT metadata.id,
    substr(metadata.data, strpos(metadata.data, '<gmd:date><gmd:CI_Date><gmd:date><gco:DateTime>'::text) + 47, strpos(metadata.data, '</gco:DateTime></gmd:date><gmd:dateType><gmd:CI_DateTypeCodecodeList="./resources/codeList.xml#CI_DateTypeCode"codeListValue="creation"/></gmd:dateType></gmd:CI_Date></gmd:date>'::text) - strpos(metadata.data, '<gmd:date><gmd:CI_Date><gmd:date><gco:DateTime>'::text) - 47) AS date
   FROM ( SELECT metadata_1.id,
            replace(metadata_1.data, ' '::text, ''::text) AS data
           FROM ( SELECT metadata_2.id,
                    replace(metadata_2.data, '

'::text, ''::text) AS data
                   FROM metadata metadata_2) metadata_1) metadata
     JOIN fiche_metadonnees ON metadata.id::text = fiche_metadonnees.fmeta_id::text
    INNER JOIN operationallowed on ( metadata.id = operationallowed.metadataid and operationallowed.operationid=0 and operationallowed.groupid=1)
  WHERE strpos(metadata.data, '<gmd:date><gmd:CI_Date><gmd:date><gco:DateTime>'::text) > 0 AND strpos(metadata.data, '</gco:DateTime></gmd:date><gmd:dateType><gmd:CI_DateTypeCodecodeList="./resources/codeList.xml#CI_DateTypeCode"codeListValue="creation"/></gmd:dateType>'::text) > 0  AND length(substr(metadata.data, strpos(metadata.data, '<gmd:date><gmd:CI_Date><gmd:date><gco:DateTime>'::text) + 47, strpos(metadata.data, '</gco:DateTime></gmd:date><gmd:dateType><gmd:CI_DateTypeCodecodeList="./resources/codeList.xml#CI_DateTypeCode"codeListValue="creation"/></gmd:dateType></gmd:CI_Date></gmd:date>'::text) - strpos(metadata.data, '<gmd:date><gmd:CI_Date><gmd:date><gco:DateTime>'::text) - 47)) < 11;
  
  create view statistique_metadata_date_publication as
  SELECT metadata.id,
    substr(metadata.data, strpos(metadata.data, '<gmd:date><gmd:CI_Date><gmd:date><gco:DateTime>'::text) + 47, strpos(metadata.data, '</gco:DateTime></gmd:date><gmd:dateType><gmd:CI_DateTypeCodecodeList="./resources/codeList.xml#CI_DateTypeCode"codeListValue="publication"/></gmd:dateType></gmd:CI_Date></gmd:date>'::text) - strpos(metadata.data, '<gmd:date><gmd:CI_Date><gmd:date><gco:DateTime>'::text) - 47) AS date
   FROM ( SELECT metadata_1.id,
            replace(metadata_1.data, ' '::text, ''::text) AS data
           FROM ( SELECT metadata_2.id,
                    replace(metadata_2.data, '

'::text, ''::text) AS data
                   FROM metadata metadata_2) metadata_1) metadata
     JOIN fiche_metadonnees ON metadata.id::text = fiche_metadonnees.fmeta_id::text
  WHERE strpos(metadata.data, '<gmd:date><gmd:CI_Date><gmd:date><gco:DateTime>'::text) > 0 AND strpos(metadata.data, '</gco:DateTime></gmd:date><gmd:dateType><gmd:CI_DateTypeCodecodeList="./resources/codeList.xml#CI_DateTypeCode"codeListValue="publication"/></gmd:dateType>'::text) > 0 AND fiche_metadonnees.statut = 4 AND length(substr(metadata.data, strpos(metadata.data, '<gmd:date><gmd:CI_Date><gmd:date><gco:DateTime>'::text) + 47, strpos(metadata.data, '</gco:DateTime></gmd:date><gmd:dateType><gmd:CI_DateTypeCodecodeList="./resources/codeList.xml#CI_DateTypeCode"codeListValue="publication"/></gmd:dateType></gmd:CI_Date></gmd:date>'::text) - strpos(metadata.data, '<gmd:date><gmd:CI_Date><gmd:date><gco:DateTime>'::text) - 47)) < 11;
    
    
create view statistique_liste_metadonnees_prod_date_couche as 
  SELECT metadata.id,
    xpath('//gmd:contact/gmd:CI_ResponsibleParty/gmd:organisationName/gco:CharacterString/text()'::text, ('<?xml version="1.0" encoding="utf-8"?>'::text || metadata.data)::xml, ARRAY[ARRAY['gmd'::text, 'http://www.isotc211.org/2005/gmd'::text], ARRAY['gco'::text, 'http://www.isotc211.org/2005/gco'::text]])::text AS "Producteur",
    statistique_metadata_date_creation.date AS "date de création",
    statistique_metadata_date_revision.date AS "date de révision",
    statistique_metadata_date_publication.date AS "date de publication",
    couche_donnees.couchd_nom AS "nom de la couche",
    couche_donnees.couchd_description AS "Résumé"
   FROM metadata
     JOIN fiche_metadonnees ON metadata.id::text = fiche_metadonnees.fmeta_id::text
     INNER JOIN operationallowed on ( metadata.id = operationallowed.metadataid and operationallowed.operationid=0 and operationallowed.groupid=1)
     JOIN couche_donnees ON fiche_metadonnees.fmeta_fk_couche_donnees = couche_donnees.pk_couche_donnees
     LEFT JOIN statistique_metadata_date_creation ON statistique_metadata_date_creation.id = metadata.id
     LEFT JOIN statistique_metadata_date_revision ON statistique_metadata_date_revision.id = metadata.id
     LEFT JOIN statistique_metadata_date_publication ON statistique_metadata_date_publication.id = metadata.id
  WHERE NOT metadata.data IS NULL AND metadata.data <> ''::text;
    
    
  create view statistique_nombre_cartes_publiees_domaine as 
  SELECT rubric_param.rubric_name AS "nom de la rubrique",
    domaine.pk_domaine,
    domaine.dom_nom AS "nom du domaine",
    count(*) AS "Nombre de cartes publiées"
   FROM fiche_metadonnees
     JOIN carte_projet ON carte_projet.cartp_fk_fiche_metadonnees = fiche_metadonnees.pk_fiche_metadonnees
     JOIN ssdom_visualise_carte ON carte_projet.pk_carte_projet = ssdom_visualise_carte.ssdcart_fk_carte_projet
     JOIN sous_domaine ON sous_domaine.pk_sous_domaine = ssdom_visualise_carte.ssdcart_fk_sous_domaine
     JOIN domaine ON sous_domaine.ssdom_fk_domaine = domaine.pk_domaine
     JOIN rubric_param ON domaine.dom_rubric = rubric_param.rubric_id
     INNER JOIN metadata ON metadata.id = fiche_metadonnees.fmeta_id::bigint
     INNER JOIN operationallowed on ( metadata.id = operationallowed.metadataid and operationallowed.operationid=0 and operationallowed.groupid=1)
  WHERE fiche_metadonnees.fmeta_fk_couche_donnees IS NULL
  GROUP BY rubric_param.rubric_name, domaine.dom_nom, domaine.pk_domaine
  ORDER BY rubric_param.rubric_name, domaine.dom_nom;
  
 create view statistique_nombre_cartes_publiees_sousdomaine as  
   SELECT rubric_param.rubric_name AS "nom de la rubrique",
    domaine.dom_nom AS "nom du domaine",
    sous_domaine.pk_sous_domaine,
    sous_domaine.ssdom_nom AS "nom du sous-domaine",
    count(*) AS "Nombre de cartes publiées"
   FROM fiche_metadonnees
     JOIN carte_projet ON carte_projet.cartp_fk_fiche_metadonnees = fiche_metadonnees.pk_fiche_metadonnees
     JOIN ssdom_visualise_carte ON carte_projet.pk_carte_projet = ssdom_visualise_carte.ssdcart_fk_carte_projet
     JOIN sous_domaine ON sous_domaine.pk_sous_domaine = ssdom_visualise_carte.ssdcart_fk_sous_domaine
     JOIN domaine ON sous_domaine.ssdom_fk_domaine = domaine.pk_domaine
     JOIN rubric_param ON domaine.dom_rubric = rubric_param.rubric_id
     INNER JOIN metadata ON metadata.id = fiche_metadonnees.fmeta_id::bigint
     INNER JOIN operationallowed on ( metadata.id = operationallowed.metadataid and operationallowed.operationid=0 and operationallowed.groupid=1)
  WHERE fiche_metadonnees.fmeta_fk_couche_donnees IS NULL
  GROUP BY rubric_param.rubric_name, domaine.dom_nom, sous_domaine.ssdom_nom, sous_domaine.pk_sous_domaine
  ORDER BY rubric_param.rubric_name, domaine.dom_nom, sous_domaine.ssdom_nom;
  
  create view statistique_nombre_couches_publiees_domaine as
   SELECT rubric_param.rubric_name AS "nom de la rubrique",
    domaine.pk_domaine,
    domaine.dom_nom AS "nom du domaine",
    count(*) AS "Nombre de couches publiées"
   FROM fiche_metadonnees
     JOIN couche_donnees ON couche_donnees.pk_couche_donnees = fiche_metadonnees.fmeta_fk_couche_donnees
     JOIN ssdom_dispose_couche ON couche_donnees.pk_couche_donnees = ssdom_dispose_couche.ssdcouch_fk_couche_donnees
     JOIN sous_domaine ON sous_domaine.pk_sous_domaine = ssdom_dispose_couche.ssdcouch_fk_sous_domaine
     JOIN domaine ON sous_domaine.ssdom_fk_domaine = domaine.pk_domaine
     JOIN rubric_param ON domaine.dom_rubric = rubric_param.rubric_id
     INNER JOIN metadata ON metadata.id = fiche_metadonnees.fmeta_id::bigint
     INNER JOIN operationallowed on ( metadata.id = operationallowed.metadataid and operationallowed.operationid=0 and operationallowed.groupid=1)
  GROUP BY rubric_param.rubric_name, domaine.dom_nom, domaine.pk_domaine
  ORDER BY rubric_param.rubric_name, domaine.dom_nom; 
  
 create view statistique_nombre_couches_publiees_sousdomaine as 
   SELECT rubric_param.rubric_name AS "nom de la rubrique",
    domaine.dom_nom AS "nom du domaine",
    sous_domaine.pk_sous_domaine,
    sous_domaine.ssdom_nom AS "nom du sous-domaine",
    count(*) AS "Nombre de couches publiées"
   FROM fiche_metadonnees
     JOIN couche_donnees ON couche_donnees.pk_couche_donnees = fiche_metadonnees.fmeta_fk_couche_donnees
     JOIN ssdom_dispose_couche ON couche_donnees.pk_couche_donnees = ssdom_dispose_couche.ssdcouch_fk_couche_donnees
     JOIN sous_domaine ON sous_domaine.pk_sous_domaine = ssdom_dispose_couche.ssdcouch_fk_sous_domaine
     JOIN domaine ON sous_domaine.ssdom_fk_domaine = domaine.pk_domaine
     JOIN rubric_param ON domaine.dom_rubric = rubric_param.rubric_id
     INNER JOIN metadata ON metadata.id = fiche_metadonnees.fmeta_id::bigint
     INNER JOIN operationallowed on ( metadata.id = operationallowed.metadataid and operationallowed.operationid=0 and operationallowed.groupid=1)
  GROUP BY rubric_param.rubric_name, domaine.dom_nom, sous_domaine.pk_sous_domaine, sous_domaine.ssdom_nom
  ORDER BY rubric_param.rubric_name, domaine.dom_nom;

create view statistique_ogc as    
 SELECT couche_donnees.pk_couche_donnees,
    couche_donnees.couchd_nom,
    couche_donnees.couchd_type_stockage,
    couche_donnees.couchd_emplacement_stockage,
    couche_donnees.couchd_wms,
    couche_donnees.couchd_wfs,
    metadata.changedate,
    metadata.createdate,
    metadata.id,
    metadata.uuid
   FROM couche_donnees
     JOIN fiche_metadonnees ON couche_donnees.pk_couche_donnees = fiche_metadonnees.fmeta_fk_couche_donnees
     JOIN metadata ON fiche_metadonnees.fmeta_id::text = metadata.id::text
     INNER JOIN operationallowed on ( metadata.id = operationallowed.metadataid and operationallowed.operationid=0 and operationallowed.groupid=1)
  WHERE (couche_donnees.couchd_wms = 1 OR couche_donnees.couchd_wfs = 1);
  
create view v_desc_metadata as 
SELECT m.id AS metadataid,
    cd.couchd_nom AS couchenom,
    cd.couchd_type_stockage AS couchetype,
    acs.accs_adresse AS couchesrv,
    acs.accs_service_admin AS couchesrv_service,
    acs.accs_adresse_tele AS couchesrv_tele,
    cd.couchd_emplacement_stockage AS couchetable,
    m.data AS metadataxml
   FROM metadata m
     LEFT JOIN fiche_metadonnees fm ON m.id::text = fm.fmeta_id::text
     JOIN couche_donnees cd ON fm.fmeta_fk_couche_donnees = cd.pk_couche_donnees
     JOIN acces_serveur acs ON cd.couchd_fk_acces_server = acs.pk_acces_serveur
     INNER JOIN operationallowed on ( m.id = operationallowed.metadataid and operationallowed.operationid=0 and operationallowed.groupid=1);
 
 create view metadata_list as 
 SELECT statistique_liste_couche_publiees."nom de la couche" AS nom,
    statistique_liste_couche_publiees."uuid metadata" AS id
   FROM statistique_liste_couche_publiees
UNION
 SELECT statistique_liste_carte_publiees."nom de la carte" AS nom,
    statistique_liste_carte_publiees."uuid metadata" AS id
   FROM statistique_liste_carte_publiees;    
   
 create view statistique_dernieres_mises_a_jour as 
  SELECT statistique_liste_couche_publiees."nom de la couche" AS nom,
    statistique_liste_couche_publiees."résumé",
    statistique_liste_couche_publiees.changedate AS date,
    'donnée'::text AS type,
    statistique_liste_couche_publiees."uuid metadata" AS id,
    statistique_liste_couche_publiees."couche vecteur ?" AS data_type
   FROM statistique_liste_couche_publiees
UNION
 SELECT statistique_liste_carte_publiees."nom de la carte" AS nom,
    statistique_liste_carte_publiees."résumé",
    statistique_liste_carte_publiees.changedate AS date,
    'carte'::text AS type,
    statistique_liste_carte_publiees."uuid metadata" AS id,
    statistique_liste_carte_publiees.format AS data_type
   FROM statistique_liste_carte_publiees
  ORDER BY 3 DESC
 LIMIT 30; 

create view statistique_dernieres_nouveautes as 
 SELECT statistique_liste_couche_publiees."nom de la couche" AS nom,
    statistique_liste_couche_publiees."résumé",
    statistique_liste_couche_publiees.changedate AS date,
    statistique_liste_couche_publiees.createdate,
    'donnée'::text AS type,
    statistique_liste_couche_publiees."uuid metadata" AS id,
    statistique_liste_couche_publiees."couche vecteur ?" AS data_type
   FROM statistique_liste_couche_publiees
UNION
 SELECT statistique_liste_carte_publiees."nom de la carte" AS nom,
    statistique_liste_carte_publiees."résumé",
    statistique_liste_carte_publiees.changedate AS date,
    statistique_liste_carte_publiees.createdate,
    'carte'::text AS type,
    statistique_liste_carte_publiees."uuid metadata" AS id,
    statistique_liste_carte_publiees.format AS data_type
   FROM statistique_liste_carte_publiees
  ORDER BY 4 DESC
 LIMIT 30;   

create view statistique_liste_metadata_publiees as 
 SELECT statistique_liste_couche_publiees."nom de la couche" AS nom,
    statistique_liste_couche_publiees."résumé",
    statistique_liste_couche_publiees.changedate AS date,
    statistique_liste_couche_publiees.createdate,
    'donnée'::text AS type,
    statistique_liste_couche_publiees."id metadata" AS id,
    statistique_liste_couche_publiees."couche vecteur ?" AS data_type
   FROM statistique_liste_couche_publiees
UNION
 SELECT statistique_liste_carte_publiees."nom de la carte" AS nom,
    statistique_liste_carte_publiees."résumé",
    statistique_liste_carte_publiees.changedate AS date,
    statistique_liste_carte_publiees.createdate,
    'carte'::text AS type,
    statistique_liste_carte_publiees."id metadata" AS id,
    statistique_liste_carte_publiees.format AS data_type
   FROM statistique_liste_carte_publiees; 
 
 
 -- Ajout colonne pk sur la table prodige_help_group
CREATE SEQUENCE seq_prodige_help_group START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;
ALTER TABLE prodige_help_group add column pk_prodige_help_group integer DEFAULT nextval('seq_prodige_help_group'::regclass) NOT NULL;
ALTER TABLE ONLY prodige_help_group ADD CONSTRAINT pk_prodige_help_group PRIMARY KEY (pk_prodige_help_group);


-- Ajout table ssdom_dispose_metadata
CREATE SEQUENCE seq_ssdom_dispose_metadata
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE ssdom_dispose_metadata (
    pk_ssdom_dispose_metadata integer DEFAULT nextval('seq_ssdom_dispose_metadata'::regclass) NOT NULL,
    ts timestamp without time zone DEFAULT now(),
    uuid character varying(250) NOT NULL,
    ssdcouch_fk_sous_domaine integer
);

ALTER TABLE ONLY ssdom_dispose_metadata
    ADD CONSTRAINT pk_ssdom_dispose_metadata PRIMARY KEY (pk_ssdom_dispose_metadata);
ALTER TABLE ONLY ssdom_dispose_metadata
    ADD CONSTRAINT un_ssdom_dispose_metadata_fks UNIQUE (uuid, ssdcouch_fk_sous_domaine);
ALTER TABLE ONLY ssdom_dispose_metadata
    ADD CONSTRAINT ssdom_dispose_metadata_ssdcouch_fk_sous_domaine_fkey FOREIGN KEY (ssdcouch_fk_sous_domaine) REFERENCES sous_domaine(pk_sous_domaine) MATCH FULL ON DELETE CASCADE;
-- Migration des données existantes
INSERT INTO ssdom_dispose_metadata 
	(uuid, ssdcouch_fk_sous_domaine ) 
	select distinct uuid uuid,ssdcouch_fk_sous_domaine fk_sous_domaine
    from ssdom_dispose_couche disp
    inner join fiche_metadonnees fiche_meta
    on disp.ssdcouch_fk_couche_donnees = fiche_meta.fmeta_fk_couche_donnees
    inner join metadata meta 
    on cast(meta.id as text) = fiche_meta.fmeta_id
		union
	select distinct uuid uuid,ssdcart_fk_sous_domaine  fk_sous_domaine
    from ssdom_visualise_carte disp
inner join carte_projet on disp.ssdcart_fk_carte_projet = carte_projet.pk_carte_projet
inner join fiche_metadonnees on carte_projet.cartp_fk_fiche_metadonnees=fiche_metadonnees.pk_fiche_metadonnees
inner join metadata on cast(metadata.id as text) = fiche_metadonnees.fmeta_id ;

-- delete unused settings
delete from prodige_settings where prodige_settings_constant in ('PRO_GEONETWORK_DIRECTORY', 'PRO_GEOSOURCE_CSS', 'PRO_MODE_PROPOSITION_ACTIF',
                                                                 'PRO_GEONETWORK_ADMINISTRATEUR_PWD', 'PRO_GEONETWORK_EDITEUR_PWD', 'PRO_GEONETWORK_CARTE_PERSO_PWD' );

drop view incoherence_metadata_fiche_metadonnees;
drop view incoherence_carte_sous_domaines;

create view incoherence_carte_sous_domaines as SELECT carte_projet.pk_carte_projet,
    to_char(carte_projet.ts, 'DD Mon YYYY, HH24:MI:SS'::text) AS "date de création de la carte",
    carte_projet.cartp_nom AS "nom de la métadonnée",
    carte_projet.cartp_description AS "résumé de la métadonnée",
    carte_projet.cartp_format AS "format de la carte",
    carte_projet.cartp_fk_stockage_carte AS "identifiant stockage mapfile",
    carte_projet.cartp_fk_fiche_metadonnees AS "identifiant de la fiche métadonnée"
   FROM carte_projet
  WHERE NOT (carte_projet.pk_carte_projet IN ( SELECT ssdom_visualise_carte.ssdcart_fk_carte_projet
           FROM ssdom_visualise_carte
          WHERE ssdom_visualise_carte.ssdcart_fk_carte_projet IS NOT NULL)) AND (carte_projet.cartp_format = ANY (ARRAY[0, 1]));

-- Ajout table tache_import_donnees
set search_path='catalogue','public';
CREATE SEQUENCE seq_tache_import_donnees
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE tache_import_donnees (
    pk_tache_import_donnees integer DEFAULT nextval('seq_tache_import_donnees'::regclass) NOT NULL,
    uuid character varying(250) NOT NULL,
    pk_couche_donnees int,
    user_id integer NOT NULL,
    import_date_request   timestamp without time zone NOT NULL DEFAULT now(),
    import_date_execution timestamp without time zone,
    import_type varchar(20) not null default 'import_differe', /*available values : import_differe, synchronisation, automate*/
    import_frequency integer,
    import_table character varying(1024) NOT NULL,
    import_data_type character varying(250) NOT NULL default 'VECTOR',
    import_data_source text NOT NULL,
    import_extra_params text
);

ALTER TABLE ONLY tache_import_donnees
    ADD CONSTRAINT pk_tache_import_donnees PRIMARY KEY (pk_tache_import_donnees);
ALTER TABLE ONLY tache_import_donnees
    ADD CONSTRAINT tacheimportdonnees_fk_couchedonnees FOREIGN KEY (pk_couche_donnees) REFERENCES couche_donnees(pk_couche_donnees) ON DELETE CASCADE;


INSERT INTO traitement 
VALUES (13, now(), 'EDITION EN LIGNE (MODIF)', 'l''outil de modification de donnée (fonction de modification)', 'Permet de modifier en ligne des objets', true, true, true);

INSERT INTO trt_objet VALUES (11, 'EDITION EN LIGNE (MODIF)', 'Acc&egrave;s &agrave; l''outil modification de donnée (fonction de modification)');

insert into trt_autorise_objet(pk_trt_autorise_objet, trtautoobjet_fk_trt_id, trtautoobjet_fk_obj_type_id) values (17, 11, 1);

-- WFS link to metadata
alter table couche_donnees add column couchd_wfs_uuid varchar(250) default null;


--changement URL des métadonnées
--update geonetwork 3.2.1
update metadata set data = replace(data, 'xmlns:xlink_2="http://www.w3.org/1999/xlink"', '') where  data like '%xlink_2%' and isharvested='n';
update metadata set data = replace(data, 'xlink_2:href','xlink:href') where  data like '%xlink_2%' and isharvested='n';


-- liens entre métadonnées :
update metadata set data =replace(data, 'geonetwork/srv/fre/find?uuid=', 'geonetwork/srv/fre/catalog.search#/metadata/') where isharvested='n';
--migrate sourcedes
delete from sourcesdes;
insert into sourcesdes select sources.uuid, substr(sources.name, 0, 96), languages.id from sources, languages;
--disable map, other settings
update settings set value='false' where name='map/isMapViewerEnabled';
update settings set value='{"iso19110":{"defaultTab":"default","displayToolTip":false,"related":{"display":true,"readonly":true,"categories":["dataset"]},"validation":{"display":true}},"iso19139":{"defaultTab":"default","displayToolTip":false,"related":{"display":true,"categories":[]},"suggestion":{"display":true},"validation":{"display":true}},"dublin-core":{"defaultTab":"default","related":{"display":true,"readonly":false,"categories":["parent","onlinesrc"]}}}' where name='metadata/editor/schemaConfig';
update settings set value='false' where name='system/userFeedback/enable';
update settings set value='true' where name='system/xlinkResolver/enable';
insert into settings (name, value, datatype, position, internal) values ('system/hidewithheldelements/enableLogging','false','2','2320','y');
insert into settings (name, value, datatype, position, internal) values ('system/xlinkResolver/localXlinkEnable', 'true', '2', '2311', 'n');
insert into settings (name, value, datatype, position, internal) values ('metadata/workflow/allowPublishInvalidMd', 'true', '2', '100003','n');
insert into settings (name, value, datatype, position, internal) values ('metadata/workflow/automaticUnpublishInvalidMd', 'false', '2', '100004','n');
insert into settings (name, value, datatype, position, internal) values ('metadata/workflow/forceValidationOnMdSave', 'false', '2', '100005','n');
INSERT INTO settings (name, value, datatype, position, internal) VALUES ('system/metadatacreate/generateUuid', 'true', 2, 9100, 'n');

delete from settings where name='system/hidewithheldelements/keepMarkedElement';

--delete link series to catalogue
--delete from fiche_metadonnees where fmeta_fk_couche_donnees in (select pk_couche_donnees from couche_donnees where couchd_type_stockage=-1);
--delete from couche_donnees where couchd_type_stockage=-1;