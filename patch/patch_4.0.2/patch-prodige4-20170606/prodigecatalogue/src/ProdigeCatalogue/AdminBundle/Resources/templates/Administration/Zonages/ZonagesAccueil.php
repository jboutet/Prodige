<?php
/**
 * Accueil de la page compétences (Administration des droits)
 * @author Alkante
 */
  /*require_once($AdminPath."/Ressources/Administration/Ressources.php");
  require_once($AdminPath."/DAO/DAO/DAO.php");
  require_once($AdminPath."/Modules/BO/ZonageVO.php");
  require_once($AdminPath."/Components/EditForm/EditForm.php");
  require_once($AdminPath."/Administration/AccessRights/AccessRights.php");*/

  use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;
  use Prodige\ProdigeBundle\DAOProxy\DAO;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\ZonageVO;
  use ProdigeCatalogue\AdminBundle\Common\Components\EditForm\EditForm;
  use ProdigeCatalogue\AdminBundle\Common\AccessRights\AccessRights;

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title><?php echo Ressources::$PAGE_TITLE_MAIN; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  </head>
  <body style="margin:0px;">
<?php   
  // set a default action
  if ( !isset($_GET["Action"]) )
  {
    $Action = Ressources::$ACTION_OPEN_ZONAGES_ACCUEIL;
  }
  else
  {
    $Action = $_GET["Action"];
  }
  
  // set a default ID
  if ( !isset($_GET["Id"]) )
  {
    $PK = -1;
  }
  else
  {
    $PK = intval( $_GET["Id"] );  
  }
  
  if(!isset($_GET["bReturnFromCarto"]))
  {
    $bReturnFromCarto = false;
  }
  else
  {
    $bReturnFromCarto = true;
  }
  if(isset($_GET["bClose"])){
  	
  	if($_GET["bClose"] ==1){
  		echo "<script language='javascript'>parent.closeExtPopup(parent.tabExtPopup.length-1);</script>";exit();
  	}
  }
  if ( $PK==-1 )
  {
    exit;
  }

  $modeCreation = ($PK == -999);

  //$dao = new DAO();
  $dao = new DAO($conn, 'catalogue');
  $accessRights = new AccessRights( );
  
  $query = 'SELECT pk_zonage_id, zonage_field_id, zonage_minimal FROM zonage;';
      
  $rs = $dao->BuildResultSet($query);
  $pk_zonage_minimal_id = -1;
  $countZonages =0;
  for ($rs->First(); !$rs->EOF(); $rs->Next())
  {
    $zonage_minimal = $rs->Read(2);
    if($zonage_minimal==1){
	  	$pk_zonage_minimal_id = $rs->Read(0);
	    $zonage_minimal_field_id = $rs->Read(1);
    }
    $countZonages ++;
  }
  $premierNiveau = ($pk_zonage_minimal_id==-1 || $pk_zonage_minimal_id==$PK);
  
  //couches associées au zonage
  $query = 'SELECT couchd_nom FROM couche_donnees where couchd_zonageid_fk ='.$PK;
      
  $rs = $dao->BuildResultSet($query);
  $strHtmlCouche= "<div style='margin:10px'>Pour pouvoir supprimer un zonage, veuillez au préalable supprimer l'association entre les données ci-dessous et ce zonage : <ul>";
  $bHasDataLinked = false;  
  for ($rs->First(); !$rs->EOF(); $rs->Next())
  {
    $strHtmlCouche.= "<li><b>".$rs->Read(0)."</b></li>";
    $bHasDataLinked = true;
  }
  $strHtmlCouche.= "</ul></div>";

  $zonageVO = new ZonageVO();
  $zonageVO->setDao($dao);

  $ordinalList = array();
  $ordinalListDisp = array();
  $ordinalType = array();
  $ordinalSize = array();   
  $ordinalReadOnly = array(); 
  $FieldOnChangeEvent = array();
  $arraySelect = array();

  $ordinalList[].=ZonageVO::$ZONAGE_NOM;
  $ordinalListDisp[].="Nom :";
  $ordinalType[].=EditForm::$TYPE_TEXT;
  $ordinalSize[].=30;
  $ordinalReadOnly[].= false;
  $FieldOnChangeEvent[].="";
  $arraySelect[] = array();
  
  $ordinalList[].=ZonageVO::$ZONAGE_FK_COUCHE_DONNEES;
  $ordinalListDisp[].="Série de donnée associée :";
  $ordinalType[].=EditForm::$TYPE_SELECT;
  $ordinalSize[].=30;
  if($PK != -999){
    $ordinalReadOnly[].= true;
  }else{
    $ordinalReadOnly[].= false;
  }
  $FieldOnChangeEvent[].="javascript:console.log(EditFormZonagesValidate_onclick);zonageGetFields(this.value)";
  //$dao = new DAO();
  if ($dao)
  { 
  	if($premierNiveau)
  	 echo "<h1>Création d'un zonage de premier niveau à partir d'une série de données standard</h1>";
  	else
  	 echo "<h1>Création d'un zonage de second niveau à partir d'une série de données tabulaire</h1>";
  	
  	if($premierNiveau){
  	  $bTable = false;
  		$query = 'SELECT pk_couche_donnees, couchd_nom, couchd_emplacement_stockage FROM couche_donnees '.
               'inner join fiche_metadonnees on couche_donnees.pk_couche_donnees = fiche_metadonnees.fmeta_fk_couche_donnees'.
               ' inner join public.metadata meta on fiche_metadonnees.fmeta_id::bigint = meta.id'.
               ' inner join public.operationallowed on ( meta.id = operationallowed.metadataid and operationallowed.operationid=0 and operationallowed.groupid=1)'.
               ' where couchd_type_stockage=1 order by couchd_nom;';
  	}else{
  		$bTable = true;
  		$query = 'SELECT pk_couche_donnees, couchd_nom, couchd_emplacement_stockage FROM couche_donnees '.
  		          'inner join fiche_metadonnees on couche_donnees.pk_couche_donnees = fiche_metadonnees.fmeta_fk_couche_donnees '.
  		          ' inner join public.metadata meta on fiche_metadonnees.fmeta_id::bigint = meta.id'.
                ' inner join public.operationallowed on ( meta.id = operationallowed.metadataid and operationallowed.operationid=0 and operationallowed.groupid=1)'.
                ' where couchd_type_stockage=-3 order by couchd_nom;';
  	}
  	
    $rs = $dao->BuildResultSet($query);
    $tabData = array();
    $couch_list = array(-1 => "Veuillez sélectionner une série de données");
    for ($rs->First(); !$rs->EOF(); $rs->Next())
    {
      $pk_couche_donnees = $rs->Read(0);
      $couchd_nom = $rs->Read(1);
      $couch_list [$pk_couche_donnees]=$couchd_nom;
      $tabData[$pk_couche_donnees] = $rs->Read(2);
    } 
  }
  
  
  $arraySelect[] = $couch_list;
  
  $ordinalList[].=ZonageVO::$ZONAGE_MINIMAL;
  $ordinalListDisp[].="Zonage minimal :";
  $ordinalType[].=EditForm::$TYPE_HIDDEN;
  $ordinalReadOnly[].= false;
  $ordinalSize[].=10;
  $FieldOnChangeEvent[].="";
  $arraySelect[] = array();
  
  //champ id
  $ordinalList[].=ZonageVO::$ZONAGE_FIELD_ID;
  $ordinalListDisp[].="Champ identifiant unique :";
  /*if(!$bReturnFromCarto){
    */
  if($modeCreation){
  	$ordinalType[].=EditForm::$TYPE_SELECT;
    $ordinalReadOnly[].= false;
  }else{
    $ordinalType[].=EditForm::$TYPE_TEXT;
    $ordinalReadOnly[].= true;
  }
  $ordinalSize[].=10;
  $FieldOnChangeEvent[].="";
  $arraySelect[] = array();
  
  
  //champ libelle
  $ordinalList[].=ZonageVO::$ZONAGE_FIELD_NAME;
  $ordinalListDisp[].="Champ libellé :";
  if($modeCreation){
    $ordinalType[].=EditForm::$TYPE_SELECT;
    $ordinalReadOnly[].= false;
  }else{
    $ordinalType[].=EditForm::$TYPE_TEXT;
    $ordinalReadOnly[].= true;
  }
  $ordinalSize[].=10;
  $FieldOnChangeEvent[].="";
  $arraySelect[] = array();
  
  
  //champ de liaison
  if(!$premierNiveau){
	  $ordinalList[].=ZonageVO::$ZONAGE_FIELD_LIAISON;
	  $ordinalListDisp[].="Champ identifiant du zonage de premier niveau :";
	  if($modeCreation){
	    $ordinalType[].=EditForm::$TYPE_SELECT;
	    $ordinalReadOnly[].= false;
	  }else{
	    $ordinalType[].=EditForm::$TYPE_TEXT;
	    $ordinalReadOnly[].= true;
	  }
	  $ordinalSize[].=10;
	  $FieldOnChangeEvent[].="";
	  $arraySelect[] = array();
  }
  
  $canDelete  = true;
  
  print('<br><br>');
  

  echo "<script language=\"javascript\" type=\"text/javascript\">
         var tabData = ".json_encode($tabData).";
         
         function zonageGetFields(pk_couch_donnees){
           parent.Ext.Ajax.request({ 
	          url : '".$zonageGetFieldsUrl."',
	          method : 'GET',
	          params : {'table' : tabData[pk_couch_donnees]},
	          success: function(result){
	            tabFields = eval(result.responseText);
              var objSelect=document.getElementById('ORDINAL_3');  
              var objSelect2=document.getElementById('ORDINAL_4');
              var objSelect3=document.getElementById('ORDINAL_6');
	            RemoveAllFromSelect(objSelect,true);
	            RemoveAllFromSelect(objSelect2,true);
	            if(typeof(tabFields)!='undefined'){
	              for (var i=0; i<tabFields.length;i++){
	                AddOptionToSelect(objSelect, tabFields[i], tabFields[i], false, false);
	                AddOptionToSelect(objSelect2, tabFields[i], tabFields[i], false, false);
	                if(objSelect3)
	                  AddOptionToSelect(objSelect3, tabFields[i], tabFields[i], false, false);
	              }
	            }
	          },
	          failure: function(result){
	            parent.Ext.MessageBox.alert('Failed', result); 
	          } 
	         });
	       }</script>";

  $editForm = new EditForm( "EditFormZonages",
                $zonageVO,
                ZonageVO::$PK_ZONAGE_ID,
                $PK,
                $ordinalList,
                $ordinalListDisp,
                $ordinalType,
                $ordinalSize,
                $ordinalReadOnly,
                '', /*TODO hismail - argument ne sera pas utilisé ... $AdminPath."/Administration/Zonages/ZonagesAccueil.php",*/
                $Action,
                $FieldOnChangeEvent,
                false,
                $bHasDataLinked || ($premierNiveau && $countZonages>1),
                $arraySelect,
                $submitUrl);
  
  if($bHasDataLinked)
    echo $strHtmlCouche;  
  
  echo "<script language=\"javascript\" type=\"text/javascript\">
        //surcharge de la fonction de validation pour créer la table postgis
          ";
  if($modeCreation){
    if($premierNiveau){
        echo " function EditFormZonagesValidate_onclick() 
          {
            if(document.getElementById('ORDINAL_1').value=='' ){
                parent.Ext.Msg.alert('Création d\'un zonage', 'Veuillez saisir un intitulé au zonage');
                return;
            }
            if(document.getElementById('ORDINAL_5').value==-1 ){
                parent.Ext.Msg.alert('Création d\'un zonage', 'Veuillez sélectionner une série de données.');
                return;
            }
            if(document.getElementById('ORDINAL_3').value == document.getElementById('ORDINAL_4').value){
                parent.Ext.Msg.alert('Création d\'un zonage', 'Les noms des champs ne peuvent être identiques.');
                return;
            }
            //integration des champs en base PRODIGE
            parent.Ext.Ajax.request({
              url : '".$zonageAddTableUrl."',
              method : 'GET',
	            params : {
	              'table' : tabData[document.getElementById('ORDINAL_5').value], 
	              'fieldId' : document.getElementById('ORDINAL_3').value, 
                'fieldName' : document.getElementById('ORDINAL_4').value
              }, 
	            success: function(result){
	              var form = document.getElementById('EditFormZonages');
	              var inputId = document.getElementById('Id');
	              var inputAction = document.getElementById('Action');
	              form.action = '".$submitUrl."';
                form.submit();
	              //parent.closeExtPopup(parent.tabExtPopup.length-1);
	            },
	            failure: function(result){
	              parent.Ext.MessageBox.alert('Failed', 'erreur d\'enregistrement de la table'); 
	            } 
            });
          }
        ";
      }else{
          echo"/*ici*/ function EditFormZonagesValidate_onclick() 
        {
        //integration des champs en base PRODIGE   
          parent.Ext.Ajax.request({
	            url : '$ZonageAddFieldsUrl',
              method : 'GET',
              params : {
                'table' : tabData[document.getElementById('ORDINAL_5').value],
                'fieldId' : document.getElementById('ORDINAL_3').value,
                'fieldName' : document.getElementById('ORDINAL_4').value,
                'fieldLiaison' : document.getElementById('ORDINAL_6').value,
                'fieldIdNiveau1' : '".$zonage_minimal_field_id."'
              },
              success: function(result){
                var form = document.getElementById('EditFormZonages');
                var inputId = document.getElementById('Id');
                var inputAction = document.getElementById('Action');
                form.action = '".$submitUrl."',
                form.submit();
              },
              failure: function(result){
                parent.Ext.MessageBox.alert('Failed', 'erreur d\'enregistrement de la table'); 
              } 
          });
        }";
      }
  }
          
         echo "</script>";
    //surcharge de la fonction de validation
   echo "<script language='javascript' type='text/javascript'>
            ".
            (!$premierNiveau ? "document.getElementById('ORDINAL_2').value = 0;"
            : "document.getElementById('ORDINAL_2').value = 1;" ) ."
            function EditFormZonagesDelete_onclick() 
            {
              if (confirm('Êtes-vous sûr de vouloir supprimer cet enregistrement ?')){;              
		            parent.Ext.Ajax.request({
	                 ".(!$premierNiveau  ? 
	                //" url : window.location.protocol+'//".$acces_adress_admin."/PRRA/Administration/Administration/Zonages/ZonageDelFields.php',"
	                  " url : '$ZonageDelFieldsUrl',"
	                //: "url : window.location.protocol+'//".$acces_adress_admin."/PRRA/Administration/Administration/Zonages/ZonageDelTable.php',")."
	                  : "url : '$ZonageDelTableUrl',")."
	                method : 'GET',
	                params : {
	                  'fieldId' : document.getElementById('ORDINAL_3').value, 
	                  'fieldName' : document.getElementById('ORDINAL_4').value}, 
	                success: function(result){
	                  var form = document.getElementById('EditFormZonages');
	                    
	                  var inputId = document.getElementById('Id');
	                  var inputAction = document.getElementById('Action');
	                  
	                  inputAction.value = '".Ressources::$EDITFORM_DELETE."'; 
	                  form.action = '".$submitUrl."';
                    form.submit();
	                  
	                },failure: function(result){
	                  parent.Ext.MessageBox.alert('Failed', 'erreur d\'enregistrement de la table'); 
	                }
	              });  
	              }
            }
            
        </script>";
    
?>
  </body>
</html>