<?php
/**
 * Accueil de la page couches (Administration des droits)
 * @author Alkante
 */
	/*require_once($AdminPath."/Ressources/Administration/Ressources.php");
	require_once($AdminPath."/DAO/DAO/DAO.php");
	require_once($AdminPath."/Modules/BO/CoucheDonneesVO.php");
	require_once($AdminPath."/Components/EditForm/EditForm.php");
	require_once($AdminPath."/Administration/AccessRights/AccessRights.php");*/

  use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;
  use Prodige\ProdigeBundle\DAOProxy\DAO;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\CoucheDonneesVO;
  use ProdigeCatalogue\AdminBundle\Common\Components\EditForm\EditForm;
  use ProdigeCatalogue\AdminBundle\Common\AccessRights\AccessRights;

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title><?php echo Ressources::$PAGE_TITLE_MAIN; ?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	</head>
	<body style="margin:0px;">
<?php 	
	// set a default action
	if ( !isset($_GET["Action"]) )
	{
		$Action = Ressources::$ACTION_OPEN_LAYERS_ACCUEIL;
	}
	else
	{
		$Action = $_GET["Action"];
	}
	
	// set a default ID
	if ( !isset($_GET["Id"]) )
	{
		$PK = -1;
	}
	else
	{
		$PK = intval( $_GET["Id"] );	
	}
	
	if ( $PK==-1 )
	{
		exit;
	}

	$dao = new DAO($conn, 'catalogue');

	$coucheDonneesVO = new CoucheDonneesVO();
	$coucheDonneesVO->setDao($dao);

	$accessRights = new AccessRights( );

	$ordinalList = array();
	$ordinalListDisp = array();
	$ordinalType = array();
	$ordinalSize = array();		
	$ordinalReadOnly = array();
  $arraySelect = array();
  $FieldOnChangeEvent =array();

	$ordinalList[].=CoucheDonneesVO::$COUCHD_EMPLACEMENT_STOCKAGE;
	$ordinalType[].=EditForm::$TYPE_HIDDEN;
	$ordinalListDisp[].="";
	$ordinalSize[].=20;
	$ordinalReadOnly[].= true;
  $arraySelect[] = array();
  $FieldOnChangeEvent[]="";
	
	
	$ordinalList[].=CoucheDonneesVO::$COUCHD_ID;
	$ordinalListDisp[].="ID :";
	$ordinalType[].=EditForm::$TYPE_TEXT;
	$ordinalSize[].=20;
	$ordinalReadOnly[].= true;
  $arraySelect[] = array();
  $FieldOnChangeEvent[]="";

	$ordinalList[].=CoucheDonneesVO::$COUCHD_NOM;
	$ordinalListDisp[].="Nom :";
	$ordinalType[].=EditForm::$TYPE_TEXT;
	$ordinalSize[].=50;
	$ordinalReadOnly[].= true;
  $arraySelect[] = array();
  $FieldOnChangeEvent[]="";

	$ordinalList[].=CoucheDonneesVO::$COUCHD_DESCRIPTION;
	$ordinalListDisp[].="Description :";
	$ordinalType[].=EditForm::$TYPE_TEXT;
	$ordinalSize[].=50;
	$ordinalReadOnly[].= true;
  $arraySelect[] = array();
  $FieldOnChangeEvent[]="";

  $ordinalList[].=CoucheDonneesVO::$COUCHD_WMS;
  $ordinalListDisp[].="Publiée en WMS :";
  $ordinalType[].=EditForm::$TYPE_CB;
  $ordinalSize[].=50;
  $ordinalReadOnly[].= true;
  $arraySelect[] = array();
  $FieldOnChangeEvent[]="";

	$ordinalList[].=CoucheDonneesVO::$COUCHD_WFS;
  $ordinalListDisp[].="Publiée en WFS :";
  $ordinalType[].=EditForm::$TYPE_CB;
  $ordinalSize[].=50;
  $ordinalReadOnly[].= true;
  $arraySelect[] = array();
  $FieldOnChangeEvent[]="";

  $ordinalList[].=CoucheDonneesVO::$COUCHD_DOWNLOAD;
  $ordinalListDisp[].="En téléchargement libre :";
  $ordinalType[].=EditForm::$TYPE_CB;
  $ordinalSize[].=50;
  $ordinalReadOnly[].= true;
  $arraySelect[] = array();
  $FieldOnChangeEvent[]="";
  
  $ordinalList[].=CoucheDonneesVO::$COUCHD_RESTRICTION;
  $ordinalListDisp[].="Soumise à restriction territoriale :";
  $ordinalType[].=EditForm::$TYPE_CB;
  $ordinalSize[].=50;
  $ordinalReadOnly[].= false;
  $arraySelect[] = array();
  $FieldOnChangeEvent[]="changeRestriction(this.checked);";
  
  $ordinalList[].=CoucheDonneesVO::$COUCHD_RESTRICTION_EXCLUSIF;
  $ordinalListDisp[].="Restriction territoriale exclusive :";
  $ordinalType[].=EditForm::$TYPE_CB;
  $ordinalSize[].=50;
  $ordinalReadOnly[].= false;
  $arraySelect[] = array();
  $FieldOnChangeEvent[]="";
  
  //$dao = new DAO();
  $zonage = array("" => "Veuillez sélectionner un zonage");
  if ($dao)
  {
    $query = 'SELECT pk_zonage_id, zonage_nom FROM zonage;';
    $rs = $dao->BuildResultSet($query);
    for ($rs->First(); !$rs->EOF(); $rs->Next())
    {
      $zonage_id = $rs->Read(0);
      $zonage_nom = $rs->Read(1);
      $zonage[$zonage_id]=$zonage_nom;
    }
  }

  $ordinalList[].=CoucheDonneesVO::$COUCHD_ZONAGEID_FK;
  $ordinalListDisp[].="Zonage portant la restriction :";
  $ordinalType[].=EditForm::$TYPE_SELECT;
  $ordinalSize[].=50;
  $ordinalReadOnly[].= false;
  $arraySelect[] = $zonage;
  $FieldOnChangeEvent[]="";
  
  $ordinalList[].=CoucheDonneesVO::$COUCHD_RESTRICTION_BUFFER;
  $ordinalListDisp[].="Tolérance d'extraction(en mètres) de la restriction :";
  $ordinalType[].=EditForm::$TYPE_TEXT;
  $ordinalSize[].=10;
  $ordinalReadOnly[].= false;
  $arraySelect[] = array();
  $FieldOnChangeEvent[]="";
  
  $query = 'SELECT couchd_emplacement_stockage as table FROM catalogue.couche_donnees WHERE pk_couche_donnees = '.$PK.';';
  $rs = $dao->BuildResultSet($query);
  for ($rs->First(); !$rs->EOF(); $rs->Next())
    $extraction_table = $rs->Read(0);
  
  $ordinalList[].=CoucheDonneesVO::$COUCHD_EXTRACTION_ATTRIBUTAIRE;
  $ordinalListDisp[].="Extraction attributaire :";
  $ordinalType[].=EditForm::$TYPE_CB;
  $ordinalSize[].=50;
  $ordinalReadOnly[].= false;
  $arraySelect[] = array();
  $FieldOnChangeEvent[]="extractionGetFields('".$extraction_table."')";
  
  $ordinalList[].=CoucheDonneesVO::$COUCHD_EXTRACTION_ATTRIBUTAIRE_CHAMP;
  $ordinalListDisp[].="Extraction basée sur le critère :";
  $ordinalType[].=EditForm::$TYPE_SELECT;
  $ordinalSize[].=50;
  $ordinalReadOnly[].= false;
  $arraySelect[] = array("" => "Aucun");
  $FieldOnChangeEvent[]="";
  
  $ordinalList[].=CoucheDonneesVO::$COUCHD_ALERT_MSG_ID;
  $ordinalListDisp[].="Modèle de message pour l'envoi d'alertes dans le module édition :";
  $ordinalType[].=EditForm::$TYPE_SELECT;
  $ordinalSize[].=50;
  $ordinalReadOnly[].= false;
  $arraySelect[] = array("" => "Aucun");
  $FieldOnChangeEvent[]="";

  print('<br>');
	
	$editForm = new EditForm(	"EditFormCoucheDonneesVO",
								$coucheDonneesVO,
								CoucheDonneesVO::$PK_COUCHE_DONNEES,
								$PK,
								$ordinalList,
								$ordinalListDisp,
								$ordinalType,
								$ordinalSize,
								$ordinalReadOnly,
								'', /*TODO hismail - argument ne sera pas utilisé ... $AdminPath."/Administration/Layers/LayersAccueil.php",*/
								$Action,
                $FieldOnChangeEvent, 
                false,
                true, $arraySelect,
	              $submitUrl);
	
  print("<br><div style='text-align:left;margin:10px;'>");
  print("<p>Une couche <i>soumise à restriction territoriale</i> est resteinte<b> en téléchargement</b> pour un utilisateur sur un territoire donné. <b>En visualisation</b>, la carte est cadrée par défaut sur ce territoire.</p>");
  print("<p>Une couche <i>à restricition exclusive</i> ne peut être téléchargée que par les utilisateurs ayant un territoire issu du zonage associé. Dans ce cas, le téléchargement se fait automatiquement sur ce territoire.</p>");
  print("<p>Une couche <i>à restricition non exclusive</i> peut être téléchargée par les utilisateurs ayant un territoire contenant un territoire du zonage associé ou un territoire contenu dans un territoire du zonage associé. Dans ce cas, le téléchargement se fait sur le territoire associé au zonage choisi.");
	print("</div>");
?>

<script language="javascript">

// Fill values to liste of  modeles msg for alerts
var objSelect = document.getElementById("ORDINAL_<?php echo CoucheDonneesVO::$COUCHD_ALERT_MSG_ID;?>");;
RemoveAllFromSelect(objSelect, true);
AddOptionToSelect(objSelect, '', 'Aucun', false, false);
parent.Ext.Ajax.timeout = 90000;
parent.Ext.Ajax.request({
  /*url     : window.location.protocol + '//--- php echo $acces_adress_admin; --- php/PRRA/Services/getAlertsModeles.php',*/
  url:     '<?php echo $getAlertsModelesUrl; ?>',
  method  : 'GET',
  params  : {'layer_name' : document.getElementById("ORDINAL_<?php echo CoucheDonneesVO::$COUCHD_EMPLACEMENT_STOCKAGE;?>").value},
  success : function(result) {
    tabFields = eval(result.responseText);
    if(typeof(tabFields) != 'undefined'){
      for(var i=0; i<tabFields.length; i++){
        AddOptionToSelect(objSelect, tabFields[i]['pk_modele_id'], tabFields[i]['modele_name'], false, false);
      }
      objSelect.value = '<?php echo $coucheDonneesVO->Read($coucheDonneesVO::$COUCHD_ALERT_MSG_ID); ?>';
    }
  },
  failure : function(result){
    parent.Ext.Msg.alert("Erreur", result);
  }
});

changeRestriction(document.getElementById("ORDINAL_<?php echo CoucheDonneesVO::$COUCHD_RESTRICTION;?>").checked);
var el = document.getElementById("ORDINAL_15");
if("createEvent" in document){
	var e = document.createEvent("HTMLEvents");
	e.initEvent("change", false, true);
  el.dispatchEvent(e);	
}

function changeRestriction(bChecked){
  if(bChecked){
	  document.getElementById("ORDINAL_<?php echo CoucheDonneesVO::$COUCHD_RESTRICTION_EXCLUSIF;?>").disabled=false;
	  document.getElementById("ORDINAL_<?php echo CoucheDonneesVO::$COUCHD_ZONAGEID_FK;?>").disabled=false;
	  document.getElementById("ORDINAL_<?php echo CoucheDonneesVO::$COUCHD_RESTRICTION_BUFFER;?>").disabled=false;
  }else{
	  document.getElementById("ORDINAL_<?php echo CoucheDonneesVO::$COUCHD_RESTRICTION_EXCLUSIF;?>").disabled=true;
	  document.getElementById("ORDINAL_<?php echo CoucheDonneesVO::$COUCHD_ZONAGEID_FK;?>").disabled=true;
	  document.getElementById("ORDINAL_<?php echo CoucheDonneesVO::$COUCHD_RESTRICTION_BUFFER;?>").disabled=true;
	  document.getElementById("ORDINAL_<?php echo CoucheDonneesVO::$COUCHD_ZONAGEID_FK;?>").value="";
  }
}

function AddOptionToSelect(objSelect, val, txt, bMoveDefaultToEnd, bVerifExist)
{
  if( bVerifExist ) {
    var i = 0;
    var bFound = false;
    while( !bFound && i<objSelect.length ) {
      if( objSelect.options[i].value != val ) i++; else bFound = true;
    }
    if( bFound ) return false;
  }

  if( bMoveDefaultToEnd ) {
    objSelect.length=objSelect.length+1;
    var nbElt = objSelect.length;
    objSelect.options[nbElt-2].value = val;
    objSelect.options[nbElt-2].text = txt;
    objSelect.options[nbElt-1].value = "";
    objSelect.options[nbElt-1].text = "";
    if( objSelect.selectedIndex == (nbElt-2) )
      objSelect.selectedIndex = nbElt-1;
  } else {
    objSelect.length=objSelect.length+1;
    var nbElt = objSelect.length;
    objSelect.options[nbElt-1].value = val;
    objSelect.options[nbElt-1].text = txt;
    objSelect.selectedIndex = nbElt-1;
  }
  return true;
}

function RemoveAllFromSelect(objSelect, bRemoveFirst)
{
  objSelect.selectedIndex = -1;
  if( bRemoveFirst ) 
    objSelect.options.length = 0;
  else
    objSelect.options.length = 1;
}

function extractionGetFields(table){

	var objSelect = document.getElementById("ORDINAL_<?php echo CoucheDonneesVO::$COUCHD_EXTRACTION_ATTRIBUTAIRE_CHAMP;?>");
  var extraction_cb = document.getElementById("ORDINAL_<?php echo CoucheDonneesVO::$COUCHD_EXTRACTION_ATTRIBUTAIRE;?>").checked;
  RemoveAllFromSelect(objSelect, true);
	if(!extraction_cb)
		AddOptionToSelect(objSelect, '', 'Aucun', false, false);
	else{
    parent.Ext.Ajax.timeout = 900000;
    parent.Ext.Ajax.request({
      /*url     : window.location.protocol+'//--- php echo $acces_adress_admin; --- php /PRRA/Administration/Administration/Zonages/ZonageGetFields.php',*/
      url     : '<?php echo $zonageGetFieldsUrl; ?>',
      method  : 'GET',
      params  : {'table' : table},
      success : function(result){
        tabFields = eval(result.responseText);
        if(typeof(tabFields) != 'undefined'){
          for(var i=0; i<tabFields.length; i++){
            AddOptionToSelect(objSelect, tabFields[i], tabFields[i], false, false);
          }
          objSelect.value = '<?php echo $coucheDonneesVO->Read($coucheDonneesVO::$COUCHD_EXTRACTION_ATTRIBUTAIRE_CHAMP); ?>';
        }
      },
      failure : function(result){
        parent.Ext.Msg.alert("Erreur", result);
      }
    });
	}
}

function EditFormCoucheDonneesVOValidate_onclick() 
{
  var form = document.getElementById("EditFormCoucheDonneesVO");
  if(document.getElementById("ORDINAL_<?php echo CoucheDonneesVO::$COUCHD_RESTRICTION;?>").checked && 
		  document.getElementById("ORDINAL_<?php echo CoucheDonneesVO::$COUCHD_ZONAGEID_FK;?>").value=='' )
  {
    parent.Ext.Msg.alert("Validation", "Si la donnée est restreinte territorialement, vous devez sélectionner un zonage sur lequel porte cette restriction");
    return;
  }
  var inputId = document.getElementById("Id");
  var inputAction = document.getElementById("Action");
  if(!document.getElementById("ORDINAL_<?php echo CoucheDonneesVO::$COUCHD_RESTRICTION;?>").checked){
	  document.getElementById("ORDINAL_<?php echo CoucheDonneesVO::$COUCHD_ZONAGEID_FK;?>").disabled=false;
	  document.getElementById("ORDINAL_<?php echo CoucheDonneesVO::$COUCHD_RESTRICTION_BUFFER;?>").disabled=false;
	  if(isNaN(parseFloat(document.getElementById("ORDINAL_<?php echo CoucheDonneesVO::$COUCHD_RESTRICTION_BUFFER;?>").value))){
		  parent.Ext.Msg.alert("Validation", "La tolérance doit être de type numérique");
		  return;
	  } 
  }
  
		  
  /*form.action = "---php echo PRO_CATALOGUE_URLBASE; ---php droits.php?url=Administration/Components/EditForm/WriteBOV.php";*/
  form.action = "<?php echo $submitUrl; ?>";
  form.submit();
  if(!document.getElementById("ORDINAL_<?php echo CoucheDonneesVO::$COUCHD_RESTRICTION;?>").checked){
	  document.getElementById("ORDINAL_<?php echo CoucheDonneesVO::$COUCHD_ZONAGEID_FK;?>").disabled=true;
	  document.getElementById("ORDINAL_<?php echo CoucheDonneesVO::$COUCHD_RESTRICTION_BUFFER;?>").disabled=true;
  }
}

</script>
	</body>
</html>