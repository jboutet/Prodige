<?php

namespace ProdigeCatalogue\GeosourceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\JsonResponse;

use Prodige\ProdigeBundle\Controller\BaseController;
use Prodige\ProdigeBundle\Controller\User;
use Prodige\ProdigeBundle\DAOProxy\DAO;

use Prodige\ProdigeBundle\Controller\UpdatedomSdomController;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Author Alkante
 * Service de réinitialisation de la représentation par défaut <=> supprime le mapfile layers/
 */

/**
 * @Route("/geosource")
 */
class LayerMapInitController extends BaseController {
    
    
    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/mapDelete/{uuid}", name="catalogue_geosource_mapDelete", options={"expose"=true})
     */
    public function mapDeleteAction(Request $request) {
        
        $acces_adress_admin = rtrim($this->getParameter('PRODIGE_URL_ADMINCARTO'), "/");

        $uuid = $request->get("uuid");
        //first detect if map exists in carmen database
        $connection = $this->getProdigeConnection('carmen');
        $tabMaps = $connection->fetchAll("select map_id from map where map_wmsmetadata_uuid = :uuid", array ("uuid" => $uuid));
        $map_id = "";
        $bSucess = false;
        //print_r($tabMaps);
        foreach ($tabMaps as $val)
        {
            $map_id = $val['map_id'];
            //redirect to map identifier
            
            $carmenEditMapAction = $acces_adress_admin."/api/map/delete/".$map_id;
            
            $ticket = \Alk\Common\CasBundle\Security\Authentication\Provider\CasProvider::getPGT($carmenEditMapAction);
            $jsonResp = $this->curl($carmenEditMapAction, 'DELETE', array('ticket'=>$ticket), array(), array());
          
            $jsonObj = json_decode($jsonResp);
            if($jsonObj && $jsonObj->status && $jsonObj->status="200" ){
                $bSucess= true;
            }else{
                return new JsonResponse(array("success"=>false));
            }
            
        }
        if($bSucess){
            return new JsonResponse(array("success"=>true));
        }else{
            return new JsonResponse(array("success"=>false));
        }
        
        
        
    }
    
    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/mapInsert/{uuid}", name="catalogue_geosource_mapInsert", options={"expose"=true})
     */
    public function mapInsertAction(Request $request) {

        $conn = $this->getCatalogueConnection('catalogue');
        $dao = new DAO($conn, 'catalogue,public');
        $query = 'SELECT nextval(\'seq_stockage_carte\'),  nextval(\'seq_carte_projet\'), nextval(\'seq_fiche_metadonnees\')';
        
        $rs = $dao->BuildResultSet($query);
        if ($rs->GetNbRows() > 0)
        {
            $rs->First();
            $stockage_carte = $rs->Read(0);
            $carte = $rs->Read(1);
            $fiche_metadonnees = $rs->Read(2);
        }
        
        $format = $request->get("FORMAT");
        $carte_nom = $request->get("MAP");
        //$tabSdom = $request->get("sdom"); // lire sdom depuis ssdom_dispose_metadata
        $uuid = $request->get("uuid");
        $query = 'INSERT INTO FICHE_METADONNEES (PK_FICHE_METADONNEES, FMETA_ID, STATUT) select ?, id, 1 from metadata where uuid= ?';
        if($dao->Execute($query, array($fiche_metadonnees, $uuid)) == 1) {
            $query = 'INSERT INTO STOCKAGE_CARTE (PK_STOCKAGE_CARTE, STKCARD_ID, STKCARD_PATH, STK_SERVER) VALUES (?,?,?,?)';
            if ($dao->Execute($query, array($stockage_carte, "1_".$stockage_carte, $carte_nom.".map", 1)) == 1)
            {
                $query = 'INSERT INTO CARTE_PROJET (PK_CARTE_PROJET, CARTP_ID, CARTP_NOM, cartp_fk_fiche_metadonnees, CARTP_FORMAT, CARTP_FK_STOCKAGE_CARTE) VALUES (?,?,?,?,?,?)';
            
                if ($dao->Execute($query, array($carte, '1_'.$carte.'_', $carte_nom, $fiche_metadonnees, $format, $stockage_carte)) == 1)
                {
                    // copier les affectations ssdom_dispose_metadata dans SSDOM_VISUALISE_CARTE
                    $rs = $dao->buildResultSet("SELECT ssdcouch_fk_sous_domaine AS sdom FROM ssdom_dispose_metadata WHERE uuid = :uuid", array('uuid'=>$uuid));
                    for($rs->First(); !$rs->EOF(); $rs->Next()) {
                        $sdom = $rs->Read(0);
                        $dao->Execute("INSERT INTO SSDOM_VISUALISE_CARTE (ssdcart_fk_carte_projet, ssdcart_fk_sous_domaine) VALUES ((SELECT pk_carte_projet FROM carte_projet WHERE cartp_fk_fiche_metadonnees = ? LIMIT 1), ?)", array($fiche_metadonnees, $sdom));
                    }
                    //index metadata with URL
                    $rs = $dao->BuildResultSet("SELECT id FROM METADATA WHERE UUID=?", array($uuid));
                    $rs->First();
                    $metadataId = $rs->Read(0);
                    
                    //used to add links into metadata
                    $response = $this->forward('ProdigeProdigeBundle:UpdateDomSdom:updateDomSdom', array(
                        'fmeta_id'  => $metadataId,
                        'modeData' => 'carte'
                    ));
                    
                    if ( 0 === strpos($response->headers->get('Content-Type' ), 'application/json') ) {
                        $response = json_decode($response->getContent(), true);
                        if($response["success"]==true){
                            //redirect to map setting after insertion
                            return $this->redirectToRoute('catalogue_geosource_mapInit', array("uuid"=> $uuid));
                        }else{
                            throw $this->createHttpException('Echec de l\'initialisation de la carte');
                        }
                    
                    }
                }
            }
        
        }
    }
    
    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/mapInit/{uuid}", name="catalogue_geosource_mapInit", options={"expose"=true})
     */
    public function mapInitAction(Request $request) {
    
        $uuid = $request->get("uuid");
        
        //first detect if map exists in catalogue database
        $connection = $this->getCatalogueConnection('catalogue,public');
        
        $tabMaps = $connection->fetchAll("select cartp_format, cartp_nom, stkcard_path from carte_projet inner join fiche_metadonnees on". 
                                          " carte_projet.cartp_fk_fiche_metadonnees = fiche_metadonnees.pk_fiche_metadonnees ".
                                          " inner join stockage_carte on stockage_carte.pk_stockage_carte = carte_projet.cartp_fk_stockage_carte".
                                          " inner join metadata on  fiche_metadonnees.fmeta_id::bigint = metadata.id where metadata.uuid=:uuid", array ("uuid" => $uuid));
        
        if(!empty($tabMaps)){
            foreach ($tabMaps as $val)
            {
                $format =  $val["cartp_format"];
                if($format == 1){//static map
                    $url = $request->getSchemeAndHttpHost().$this->generateUrl('catalogue_geosource_administrationCartoChangeStaticCarte');
                    header("location:".$url);
                    exit();
                }elseif($format==0){//dynamic map
                    return $this->dynamicMapInit($request, $uuid, $val);
                }
            }
        }else{
            //IHM to declare map
            return $this->declareMap($uuid);
        }
    }
    
    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/cartepersoInit/{uuid}/{pk_carte_projet}", name="catalogue_geosource_cartepersoInit", options={"expose"=true})
     */
    public function cartepersoInitAction(Request $request, $uuid, $pk_carte_projet) {
    
        //first detect if map exists in catalogue database
        $connection = $this->getCatalogueConnection('catalogue,public');
    
        $tabMaps = $connection->fetchAll("select cartp_format, cartp_nom, stkcard_path from carte_projet  ".
            " inner join stockage_carte on stockage_carte.pk_stockage_carte = carte_projet.cartp_fk_stockage_carte".
            " where carte_projet.pk_carte_projet=:pk_carte_projet", array ("pk_carte_projet" => $pk_carte_projet));
        if(!empty($tabMaps)){
            foreach ($tabMaps as $val)
            {
                return $this->dynamicMapInit($request, $uuid, $val);
            }
        }else{
            throw $this->createHttpException('Impossible d\'ouvrir la carte');
        }
    }
    
    /**
     * IHM to declare map
     * @param unknown $uuid
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function declareMap($uuid){
        

        $strJs = "";
        $conn = $this->getCatalogueConnection('catalogue');
        $dao = new DAO($conn, 'catalogue');
        if($dao) 
        {   
            $strInitJs = "var tabData = new Array();\n";
            $strInitJs.= "tabData[1] = new Array();\n";
            $queryMaps = 'SELECT stkcard_path FROM stockage_carte LEFT JOIN carte_projet on stockage_carte.pk_stockage_carte = carte_projet.cartp_fk_stockage_carte'.
                         ' where  cartp_format = 0' ;
            $queryMaps .= ' order by stkcard_path';
            $rsMaps = $dao->BuildResultSet($queryMaps);
            $i = 0;
            for ($rsMaps->First(); !$rsMaps->EOF(); $rsMaps->Next()){
                $strInitJs.= "tabData[1][".$i."] ='".substr($rsMaps->Read(0), 0, -4)."';\n";
                $i ++;
            }
            // construit un tableau javascript contenant les sous-domaines
            $query = "SELECT distinct DOM_SDOM.PK_SOUS_DOMAINE, RUBRIC_PARAM.RUBRIC_ID, DOM_SDOM.PK_DOMAINE, DOM_SDOM.DOM_NOM, DOM_SDOM.SSDOM_NOM FROM DOM_SDOM ".
                "LEFT JOIN RUBRIC_PARAM ON DOM_SDOM.DOM_RUBRIC=RUBRIC_PARAM.RUBRIC_ID ".
                "Left join sous_domaine on sous_domaine.pk_sous_domaine=DOM_SDOM.pk_sous_domaine ".
                "ORDER BY RUBRIC_PARAM.RUBRIC_ID, DOM_NOM, SSDOM_NOM";
            $rs = $dao->BuildResultSet($query);
            $strInitJs .= "tabSdom = new Array();\n";
            for ($rs->First(); !$rs->EOF(); $rs->Next())
            {
                $sdom_id = $rs->Read(0);
                $rub_id  = $rs->Read(1);
                $dom_id  = $rs->Read(2);
                $strInitJs .= "tabSdom[".$sdom_id."] = new Object();\n";
                $strInitJs .= "tabSdom[".$sdom_id."].rubric_id = ".$rub_id.";\n";
                $strInitJs .= "tabSdom[".$sdom_id."].dom_id = ".$dom_id.";\n";
            }
        }
        $strInitJs .= "var domaineService= '".$this->generateUrl('catalogue_geosource_getDomaines')."';";
        $strCss = '<link rel="stylesheet" type="text/css" href="'.$this->generateUrl('catalogue_geosource_get_css_file', array('css_file'=>'administration_carto_ajout')).'">';
        $strCss.= '<link rel="stylesheet" type="text/css" href="/bundles/geosource/css/Administration.css">';
        $strCss.= '<link rel="stylesheet" type="text/css" href="/Scripts/ext3.0/resources/css/ext-all.css">';

        $strJs = '<script  src="/Scripts/ext3.0/adapter/ext/ext-base.js"> </script>';
        $strJs.= '<script  src="/Scripts/ext3.0/ext-all.js"> </script>';
        $strJs.= '<script  src="/Scripts/ext3.0/ext-lang-fr.js"> </script>';
        $strJs.= '<script  src="/Scripts/ext3.0/miframe.js"> </script>';
        $strJs.= /*'<script  src="/bundles/geosource/js/extarbosdom.js"> </script>'.*/
                 '<script  src="/bundles/geosource/js/domaines.js"> </script>'.
                 '<script  src="/bundles/geosource/js/administration_carto_ajout.js"> </script>'.
                 '<script language="javascript">'.$strInitJs.'</script>';
        
        $strHtml = "
        <!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">
        <html>
        	<head>
        		<title>Administration Cartographique</title>
        		<META HTTP-EQUIV=\"Content-Type\" CONTENT=\"text/html; charset=UTF-8\">
            <!-- IE specific : forcing IE9 Document model because ext 3.0 incompatibility with IE10 -->
            <META HTTP-EQUIV='X-UA-Compatible' CONTENT='IE=9'/>".
                		$strCss.$strJs."
        	</head>
        	<body>
        		<div class=\"titre\"><h2>Cr&eacute;er une carte</h2></div>
        		<div class=\"errormsg\" id=\"errormsg\">". (isset($errormsg) ? $errormsg : "")."</div>
        		<div class=\"formulaire\">
        			<form action=\"". $this->generateUrl('catalogue_geosource_mapInsert', array("uuid" =>$uuid))."\" method=\"POST\" enctype=\"multipart/form-data\">
                <input type=\"hidden\" name=\"SERVEUR\" ID=\"SERVEUR\" value=\"1\"/>
                <input type=\"hidden\" name=\"uuid\" ID=\"uuid\" value=\"".$uuid."\"/>
        				<table>
        					<tr>
        						<th>Type de carte</th>
        						<td>
        							<select name=\"FORMAT\" id=\"FORMAT\">
        									 <option value=\"0\" selected>Carte interactive</option>
        									 <option value=\"1\" >Autre Carte</option>
        							</select>
        						</td>
        					</tr>
    			        <tr id=\"map_item\" >
                    <th>Nom de la carte</th>
                    <td>
                      <input type='text' name=\"MAP\" id=\"MAP\" value=\"\" size=\"50\" maxlength=\"1024\">
                    </td>
                  </tr>
                  <!--
        	      <tr id=\"sdom_item\" >
                    <th>Sous-domaines</th>
                    <td>
                      <div id=\"treePanelSdom\"></div>
                      <select id=\"SDOM\" name=\"sdom[]\" style=\"display:none;\" multiple=\"true\"></select>
                    </td>
                  </tr>
                  -->
        					<tr>
        					 <td colspan=\"2\" class=\"validation\">
        					   <input type=\"button\" name=\"VALIDATION\" value=\"Suivant\" onclick=\"administration_carto_ajout_valider()\">
        					   <input type=\"button\" name=\"RETOUR\" value=\"Retour\" onclick=\"window.close()';\">
        				   </td>
        			   </tr>
        				</table>
        			</form>
        		</div>
        	</body>
        </html>";
        return new Response ($strHtml);
        
    }
    /**
     * 
     * @param unknown $uuid
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function dynamicMapInit(Request $request, $uuid, $mapParams =array()){
        $acces_adress_admin = rtrim($this->getParameter('PRODIGE_URL_ADMINCARTO'), "/");;
        $model = $request->request->get("model", "");
        
        //first detect if map exists in carmen database
        $connection = $this->getProdigeConnection('carmen');
        $tabMaps = $connection->fetchAll("select map_id from map where map_wmsmetadata_uuid = :uuid", array ("uuid" => $uuid));
        
        $map_id = "";
        foreach ($tabMaps as $val)
        {
            $map_id = $val['map_id'];
            //redirect to map identifier
            header("location:".$acces_adress_admin."/edit_map/".$uuid);
            exit();
        }
        
        //add layer to model and redirect to map created
        if($model!=""){
            
            // TODO @vlc à retester avec BFE
            
            //1 create new map from model (use cas proxy granting ticket)
            $carmenEditMapAction = $acces_adress_admin."/api/map/edit/".$model.'/resume?uuid='.$uuid; // uuid is required by admincarto !
            $ticket = \Alk\Common\CasBundle\Security\Authentication\Provider\CasProvider::getPGT($carmenEditMapAction);
            $headers = array();
            $jsonResp = $this->curl($carmenEditMapAction, 'GET', array('ticket'=>$ticket), array(), array(), $headers);
            
            $jsonObj = json_decode($jsonResp);
            if($jsonObj && $jsonObj->success && $jsonObj->map && $jsonObj->map->mapId){
                $map_id =  $jsonObj->map->mapId;
                //2 change map parameters
                $prodigeConnection = $this->getProdigeConnection("carmen");
                $prodigeConnection->executeUpdate("update map set published_id =:map_id, map_title =:title,  map_wmsmetadata_uuid=:uuid where map_id=:map_id",
                    array("map_id" => $map_id,
                          "uuid"   => $uuid,
                          "title"  => $mapParams["cartp_nom"]
                ));
                //3 save / publish map
                $carmenSaveMapAction = $acces_adress_admin."/api/map/publish/".$map_id."/".$map_id.'?uuid='.$uuid; // uuid is required by admincarto !
                $ticket = \Alk\Common\CasBundle\Security\Authentication\Provider\CasProvider::getPGT($carmenSaveMapAction);
                $jsonResp = $this->curl($carmenSaveMapAction, 'POST', array('ticket'=>$ticket),  array("mapModel"=>"false", "mapFile" => substr($mapParams["stkcard_path"], 0, -4)), array(), $headers);
                
                $jsonObj = json_decode($jsonResp);
                if($jsonObj && $jsonObj->success && $jsonObj->map){
                    //redirect to map
                    header("location:".$acces_adress_admin."/edit_map/".$uuid);
                    exit();
                } else {
                    if( isset($headers['http_code']) && preg_match('/(4|5)\d{2}/', $headers['http_code']) ) {
                        return new Response($jsonResp, $headers['http_code']);
                    } else {
                        //return new Response("ERROR : failed to load model", 500);
                        throw $this->createHttpException('Impossible d\'ouvrir la carte');
                    }
                }
            }else{
                if( isset($headers['http_code']) && preg_match('/(4|5)\d{2}/', $headers['http_code']) ) {
                    return new Response($jsonResp, $headers['http_code']);
                } else {
                    //return new Response("ERROR : failed to load model", 500);
                    throw $this->createHttpException('Impossible de charger le modèle');
                }
            }
        

        
        }else{
            //choose model
            $strCss1 = '<link rel="stylesheet" type="text/css" href="/bundles/geosource/css/Administration.css">';
            $strCss2 = '<link rel="stylesheet" type="text/css" href="/Scripts/ext3.0/resources/css/ext-all.css">';
        
            $strJs1 = '<script  src="/Scripts/ext3.0/adapter/ext/ext-base.js"> </script>';
            $strJs2 = '<script  src="/Scripts/ext3.0/ext-all.js"> </script>';
            $strJs3 = '<script  src="/Scripts/ext3.0/miframe-debug.js"> </script>';
            $strJs4 = '<script  src="/Scripts/extjs_overload.js"> </script>';
            $strJs4 .= '<script  src="/bundles/geosource/js/interface.js"> </script>';
        
            return new Response ("
                    <!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">
                    <html>
                        <head>
                        <title> </title>
                        <META HTTP-EQUIV=\"Content-Type\" CONTENT=\"text/html; charset=UTF-8\">
                        <!-- IE specific : forcing IE9 Document model because ext 3.0 incompatibility with IE10 -->
                        <META HTTP-EQUIV=\"X-UA-Compatible\" CONTENT=\"IE=9\"/>
        
                        <!-- <link rel='icon' href='../public/images/favicon.ico' type='image/x-icon'/>-->
                        <!-- <link rel='stylesheet' type='text/css' href='/bundles/geosource/css/Administration.css'/>-->
                        ".$strCss1."
                        <!-- ** CSS ** -->
                        <!-- base library -->
                        <!-- <link rel='stylesheet' type='text/css' href='/Scripts/ext3.0/resources/css/ext-all.css' /> -->
                        ".$strCss2."
                        <!-- overrides to base library -->
        
                        <!-- ** Javascript ** -->
                        <!-- ExtJS library: base/adapter -->
                        <!-- <script type='text/javascript' src='/Scripts/ext3.0/adapter/ext/ext-base.js'></script>
                        <script type='text/javascript' src='/Scripts/ext3.0/ext-all.js'></script>  -->
                        ".$strJs1.$strJs2."
                        <!-- ExtJS library: all widgets -->
                        ".$strJs3.$strJs4."
                        </head>
                        <body>
                            <div class=\"ChoixModele\">
                                <p>La carte est ouverte pour la premi&egrave;re fois.
                                <br> Il est possible de choisir un mod&egrave;le pour fond de plan.</p><br>
                                <form  id=\"choix_modele\" name=\"choix_modele\" action=\"".$request->getRequestUri()."\" method=\"POST\">
                                    <input type='hidden' name=\"model\" id=\"model\" value=\"Modele.map\">
                                    <input type=\"button\" value=\"Parcourir les cartes modèles...\" onclick=\"LoadModelMaps('".$request->getSchemeAndHttpHost().$this->generateUrl('catalogue_geosource_tSAjoutDeCarte_MapModelList')."', 'map_id')\">
                                    &nbsp;<input type=\"button\" value=\"Parcourir les cartes existantes...\" onclick=\"LoadModelMaps('".$request->getSchemeAndHttpHost().$this->generateUrl('catalogue_geosource_administrationCartoGetCarte')."', 'map_id')\">
                                </form>
                            </div>
                        </body>
                    </html>
                    ");
        }
        
        
    }
}
