<?php

namespace ProdigeCatalogue\GeosourceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\JsonResponse;
use Prodige\ProdigeBundle\Controller\BaseController;
use Prodige\ProdigeBundle\Controller\GetProjectionsController;
use Prodige\ProdigeBundle\DAOProxy\DAO;
use Prodige\ProdigeBundle\Services\GeonetworkInterface;

use Prodige\ProdigeBundle\Controller\User;
/**
 * Ajoute une couche au serveur WMS
 * 
 * @author Alkante
 */

/**
 * @Route("/geosource")
 */
class LayerAddToWebServiceController extends BaseController 
{
    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/layerAddToWebService", name="catalogue_geosource_layerAddToWebService", options={"expose"=true})
     */
    public function layerAddToWebServiceAction(Request $request) 
    {
        @mkdir(PRO_MAPFILE_PATH . "layers/WMS/");
        $tabParam = array();
        
        // TODO param obligatoire + check rights CMS on uuid
        $uuid = $request->query->get("uuid", 0);
        $metadataUrl = PRO_GEONETWORK_URLBASE."/srv/".$uuid;
        $iMode = $request->query->get("iMode", 0);
        
        // take metadata dom/sdom
        $conn = $this->getCatalogueConnection('catalogue,public');
        $query = "SELECT dom_nom || '/' ||  ssdom_nom as domsdom ".
                ", array_to_string(xpath('/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:citation/gmd:CI_Citation/gmd:title/gco:CharacterString/text()'::text, data::xml, ARRAY[ARRAY['gmd', 'http://www.isotc211.org/2005/gmd'], ARRAY['gco','http://www.isotc211.org/2005/gco']]), ' ') as metadata_title".
                " from ssdom_dispose_metadata ".
                " inner join sous_domaine on ssdom_dispose_metadata.ssdcouch_fk_sous_domaine = sous_domaine.pk_sous_domaine ".
                " inner join domaine on sous_domaine.ssdom_fk_domaine = domaine.pk_domaine  ".
                " inner join metadata on metadata.uuid = ssdom_dispose_metadata.uuid ".
                " where ssdom_dispose_metadata.uuid=:uuid";
        $tabDomSdom = $conn->fetchAll($query, array ("uuid" => $uuid));
        $couche_nom = $tabDomSdom [0] ["metadata_title"];
        if ( $iMode == 0 ) {
            $action = $request->query->get("ACTION");
            $data = urldecode($request->query->get("DATA"));
            $type_stokage = $request->query->get("TYPE_STOCKAGE");
            $coucheId = $request->query->get("couchd_id");
            $domaine = $request->query->get("domaine");
            $sousdomaine = $request->query->get("sousdomaine", "");
            // $couche_nom = (stripslashes($request->query->get("couche_nom")));
            $login = $request->query->get("login");
            $pass = $request->query->get("pass");
            $tabParam ["title"] = $couche_nom;
            
            if ( $type_stokage == 0 ) { // pour les raster, préremplissage avec le nom du fichier, pour les vecteurs, le nom de la table
                $layer_title = $this->removeAccents(str_replace(" ", "", stripslashes($data)));
                $layer_title = $this->neutralisationChaine($layer_title);
                $layer_title = preg_replace("/[^a-zA-Z0-9_]/", "", $layer_title);
                $layer_title = substr($layer_title, 0, 30);
                $tabParam ["layername"] = $layer_title;
            } else {
                $tabParam ["layername"] = $data;
            }
            
            $tabParam ["gml_include_items"] = array();
            $tabParam ["gml_featureid"] = "";
            $tabParam ["keywordslist"] = "";
            $tabParam ["abstract"] = "";
            $tabParam ["layer_group"] = "";
            $tabParam ["srs"] = array (
                "EPSG:" . PRO_IMPORT_EPSG 
            );
            
            $opener = ($request->query->get("OPENER" ));
        } else {
            $action = $request->request->get("ACTION");
            $title = $request->request->get("title");
            $layername = $request->request->get("layername");
            $data = $request->request->get("DATA");
            $login = $request->request->get("login");
            $pass = $request->request->get("pass");
            $domaine = $request->request->get("domaine");
            $sousdomaine = $request->request->get("sousdomaine", "");
            $coucheId = $request->request->get("couchd_id");
            $type_stokage = $request->request->get("TYPE_STOCKAGE");
            $abstract = $request->request->get("abstract");
            $keywords = $request->request->get("keywords");
            $srs = ($request->request->get("srs", false)? implode(" ", $request->request->get("srs")) : "");
            $feature_id = $request->request->get("gml_featureid", "");
            $feature_items = ($request->request->get("gml_include_items", false)? implode(",", $request->request->get("gml_include_items")) : "");
            $opener = $request->request->get("OPENER");
            
            $layer_group = $request->request->get("layer_group");
            // tableau des paramètres de la couche
            $tabMetadata = array (
                $abstract,
                $keywords,
                $srs,
                $feature_id,
                $feature_items,
                $layer_group 
            );
        }
        
        $oMap = ms_newMapObj(PRO_MAPFILE_PATH . "/" . ($action == "wfs" ? "wfs.map" : "wms.map"));
        
        $tabLayerName = $this->getMapLayerNames($oMap);
        
        $conn = $this->getProdigeConnection('public');
        $dao = new DAO($conn, 'public');
        
        $tabSelectFields = array();
        
        // traitements serveurs
        switch ($iMode) {
            case 0 :
                // vérfication de l'existence de la couche
                if ( $type_stokage == 0 ) { // raster
                    if ( !file_exists(PRO_MAPFILE_PATH . $data) ) {
                        $iMode = 5;
                    } else {
                        // if raster, take $layerTable and delete extension
                        $layerMapFile = substr($data, 0, strrpos($data, "."));
                        // get file name
                        $layerMapFile = substr($layerMapFile, strrpos($layerMapFile, "/")+ 1, strlen($layerMapFile));
                    }
                } else { // vecteur
                         // ConnectionFactory::BeginTransaction();
                         // $dao = new DAO();
                    $layerMapFile = $data;
                    if ( $dao ) {
                        $query = "SELECT tablename FROM pg_tables where tablename=?";
                        $rs = $dao->BuildResultSet($query, array ($data));
                        if ( $rs->GetNbRows() == 0 ) {
                            $query = "SELECT viewname FROM pg_views where viewname=?";
                            $rs = $dao->BuildResultSet($query, array ($data));
                            if ( $rs->GetNbRows() == 0 ) {
                                $iMode = 5;
                            }
                        }
                        
                        if ( $iMode != 5 ) {
                            // liste des champs
                            
                            $query = "SELECT a.attnum, a.attname " . " FROM pg_class c, pg_attribute a, pg_type t " . " WHERE c.relname = ?" . " and a.attnum > 0" . " and a.attname <> 'the_geom'" . " and a.attrelid = c.oid" . " and a.atttypid = t.oid" . " ORDER BY attnum";
                            $rs = $dao->BuildResultSet($query, array ($data));
                            for($rs->First(); !$rs->EOF(); $rs->Next()) {
                                $tabSelectFields [] = ($rs->read(1 ));
                            }
                        }
                    }
                    // ConnectionFactory::CloseConnection();
                }
                $bIsLayerInService = false;
                if ( $this->isLayerInService($oMap, $data, $type_stokage) ) {
                    $bIsLayerInService = true;
                }
                break;
            
            case 2 :
                // sauvegarde de la couche sur le serveur
                if ( $this->isLayerInService($oMap, $data, $type_stokage) ) {
                    $this->DelLayerFromService($oMap, $data, $type_stokage);
                }
                
                switch ($type_stokage) {
                    case 0 :
                        $type_data = "RASTER_PRODIGE";
                        // if raster, take $layerTable and delete extension
                        $layerMapFile = substr($data, 0, strrpos($data, "."));
                        // get file name
                        $layerMapFile = substr($layerMapFile, strrpos($layerMapFile, "/")+ 1, strlen($layerMapFile));
                        if ( strpos($data, ".shp") === false )
                            $couche_type = MS_LAYER_RASTER;
                        else
                            $couche_type = MS_LAYER_TILERASTER;
                        $LayerAddParams = $type_data . "|" . $title . "|" . $couche_type . "|" . $data . "|" . $layername;
                        break;
                    case 1 :
                    case - 4 : // vues
                        $type_data = "VECTOR_PRODIGE";
                        $layerMapFile = $data;
                        // ConnectionFactory::BeginTransaction();
                        // $dao = new DAO();
                        // recuperation de la projection de la couche courante
                        
                        // global $PRO_IMPORT_EPSG;
                        $srid_source = PRO_IMPORT_EPSG;
                        if ( $dao ) {
                            $query = "select distinct GeometryType(the_geom), ST_SRID(the_geom) from " . $data . " where GeometryType(the_geom) is not null";
                            
                            $rs = $dao->BuildResultSet($query);
                            for($rs->First(); !$rs->EOF(); $rs->Next()) {
                                $couche_type = $rs->read(0);
                                $srid_source = $rs->read(1);
                            }
                        }
                        switch ($couche_type) {
                            
                            case "MULTIPOLYGON" :
                            case "POLYGON" :
                                $couche_type = MS_LAYER_POLYGON;
                                break;
                            
                            case "MULTILINESTRING" :
                            case "LINESTRING" :
                                $couche_type = MS_LAYER_LINE;
                                break;
                            
                            case "MULTIPOINT" :
                            case "POINT" :
                                $couche_type = MS_LAYER_POINT;
                                break;
                            // erreur pas de reconnaissance du type
                            default :
                                $iMode = 3;
                                break;
                        }
                        
                        // ConnectionFactory::CloseConnection();
                        
                        $querydata = "the_geom from (select " . $data . ".* from " . $data . ") as foo using unique gid using srid=" . $srid_source;
                        
                        $pro_prodige_connexion = "user=" . $conn->getUsername() . " password=" . $conn->getPassword() . " dbname=" . $conn->getDatabase() . " host=" . $conn->getHost() . " port=" . $conn->getPort();
                        $LayerAddParams = $type_data . "|" . $title . "|" . $couche_type . "|" . $pro_prodige_connexion . "|" . $querydata . "|" . $srid_source . "|" . $layername;
                        
                        break;
                }
                
                $layerName = $this->AddLayerToMap($oMap, PRO_MAPFILE_PATH . strtolower($action). ".map", $LayerAddParams, $tabMetadata, $action, $metadataUrl);
                $urlWfs = $this->getParameter("PRODIGE_URL_DATACARTO"). "/WFS/" . $data;
                $uuidWFS = "";
                
                // create wfs Mapfile
                if ( $action == "wfs" ) {
                    //select wfs if already exists
                    $conn = $this->getCatalogueConnection('catalogue');
                    $daoCatalogue = new DAO($conn, 'catalogue');
                    
                    $query = 'select couchd_wfs_uuid, couchd_emplacement_stockage from COUCHE_DONNEES inner join FICHE_METADONNEES on FICHE_METADONNEES.fmeta_fk_couche_donnees = COUCHE_DONNEES.pk_couche_donnees'.
                        ' inner join public.metadata on FICHE_METADONNEES.fmeta_id::bigint = metadata.id where metadata.uuid = ? and couchd_wfs_uuid is not null and couchd_wfs_uuid <>\'\'';
                    $rs = $daoCatalogue->buildResultSet($query, array (
                        $uuid
                    ));
                    
                    for($rs->First(); !$rs->EOF(); $rs->Next()) {
                         $uuidWFS = $rs->Read(0);
                    }
                    //1 create wfs metadata
                    $uuidWFS = $this->createServiceWFSFiche($title, $uuid, $layerName, $daoCatalogue, $uuidWFS);
                    
                    //2 create specific mapfile
                    $wfs_map = "wfs_" . $uuidWFS . ".map";
                    $this->createWfsMapfile(PRO_MAPFILE_PATH.$wfs_map, $LayerAddParams, $tabMetadata, $urlWfs, $metadataUrl);
                }
                break;
            
            case 4 :
                $layerName = $this->DelLayerFromService($oMap, $data, $type_stokage);
               
                $oMap->save(PRO_MAPFILE_PATH . strtolower($action). ".map");

                $uuidWFS ="";
                if ( $action == "wfs" ) {
                    //select wfs if already exists
                    $conn = $this->getCatalogueConnection('catalogue');
                    $daoCatalogue = new DAO($conn, 'catalogue');
                
                    $query = 'select couchd_wfs_uuid, couchd_emplacement_stockage from COUCHE_DONNEES inner join FICHE_METADONNEES on FICHE_METADONNEES.fmeta_fk_couche_donnees = COUCHE_DONNEES.pk_couche_donnees'.
                        ' inner join public.metadata on FICHE_METADONNEES.fmeta_id::bigint = metadata.id where metadata.uuid = ? and couchd_wfs_uuid is not null and couchd_wfs_uuid <>\'\'';
                    $rs = $daoCatalogue->buildResultSet($query, array (
                        $uuid
                    ));
                    //get wfs specific mapfile
                    for($rs->First(); !$rs->EOF(); $rs->Next()) {
                        $uuidWFS = $rs->Read(0);
                    }
                    //delete layer from mapfile
                    $wfs_map = "wfs_" . $uuidWFS . ".map";
                    if ( file_exists(PRO_MAPFILE_PATH . $wfs_map) ) {
                        $oMapWfs = ms_newMapObj(PRO_MAPFILE_PATH . $wfs_map);
                        $this->DelLayerFromService($oMapWfs, $data, $type_stokage);
                        //completely delete mapfile and wfs metadata
                        $oMapWfs->save(PRO_MAPFILE_PATH . $wfs_map);
                        $oMapWfs = ms_newMapObj(PRO_MAPFILE_PATH . $wfs_map);
                        if($oMapWfs->numlayers==0){
                            unlink(PRO_MAPFILE_PATH . $wfs_map);
                            $geonetwork = new GeonetworkInterface(PRO_GEONETWORK_URLBASE, 'srv/fre/');
                            $geonetwork->get('md.delete?_content_type=json&uuid=' . $uuidWFS);
                            $uuidWFS ="";
                        }else{
                            $uuidWFS = $this->deleteFromWFSFiche($uuid, $data, $daoCatalogue, $uuidWFS);
                        }
                    }
                }
                
                $mapfile = "layers/WMS/" . $data;
                $connection = $this->getProdigeConnection('carmen');
                
                if ( $action == "wms" ) {
                    $tabMaps = $connection->fetchAll("select map_wmsmetadata_uuid from map where map_file = :map_file", array (
                        "map_file" => $mapfile 
                    ));
                    if ( !empty($tabMaps) ) {
                        foreach($tabMaps as $val){
                            $mapUuid = $val ['map_wmsmetadata_uuid'];
                            // call mapDelete service
                            $response = LayerMapInitController::mapDeleteAction(new Request(array (
                                'uuid' => $mapUuid 
                            )));
                            if ( 0 === strpos($response->headers->get('Content-Type' ), 'application/json') ) {
                                $response = json_decode($response->getContent(), true);
                            }
                        }
                    }
                }
                break;
        }
        
        // affichage
        $strOnUnload = "";
        
        if ( $opener  && $iMode!=0 ){
        	$strOnUnload = "window.onload = function(){
                		  window.location = '".urldecode($opener)."'; 
                    };";
        	
        }
        
        $strHtml = "
            <!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">
            <html>
              <head>
                <title> Ajouter une couche au serveur " . $action . "</title>
                <META HTTP-EQUIV=\"Content-Type\" CONTENT=\"text/html; charset=UTF-8\">
                <link rel='stylesheet' type='text/css' href='" . 
                $this->generateUrl('catalogue_geosource_get_css_file', array ('css_file' => 'administration_carto')). 
                "'>
                <script  src=\"/bundles/geosource/js/LayersAddToWebservice.js\"> </script>
                <script language='javascript'>
                
                  ".$strOnUnload."
                  var tabLayerName= new Array();
            ";
        for($i = 0; $i < count($tabLayerName); $i ++) {
            $strHtml .= "tabLayerName[" . $i . "] ='" . $tabLayerName [$i] . "';\n";
        }
        $strHtml .= "
            </script>
            </head>
            <body>";
        
        if ( $iMode != 0 ) {
            
            switch ($iMode) {
                case 1 :
                    $msg = "Cette couche est  déjà présente sur le serveur. Pour l'enlever, cliquez sur supprimer";
                    $buttonSupp = "<input type='button' name='supprimer' value='Supprimer' onclick=\"location.href = '" . $urlAction . "&bSupp=1';\">";
                    break;
                case 2 :
                    $this->updateMetadata($layerName, $action, 1, $uuid, $coucheId, $iMode, $title, $opener, $uuidWFS);
                    exit();
                    break;
                case 3 :
                    $msg = "Un problème est apparu lors de l'ajout de la couche, échec de l'opération";
                    break;
                case 4 :
                    $this->updateMetadata($layerName, $action, 0, $uuid, $coucheId, $iMode, $title, $opener, $uuidWFS);
                    exit();
                    break;
                case 5 :
                    $msg = "Impossible de réaliser la mise à disposition, la couche n'a pas été importée.";
                    break;
            }
            
            $strHtml .= "<div class=\"errormsg\">
                " . $msg . " <br> " . (isset($buttonSupp)? $buttonSupp : "") . "</div>
              </body>
          </html>";
        } else { // la couche existe
            $tabParam = $this->layerGetParams($oMap, $data, $type_stokage, $action, $tabParam, $tabSelectFields);
            $urlOpenMap ="";
            // chek if WMS representation exits
            $acces_adress_admin = rtrim($this->getParameter('PRODIGE_URL_ADMINCARTO' ), '/');
            $acces_catalogue = rtrim($this->getParameter('PRODIGE_URL_CATALOGUE' ), '/');
            $connection = $this->getProdigeConnection('carmen');
            $mapfile = "layers/WMS/" . $data;
            $tabMaps = $connection->fetchAll("select map_wmsmetadata_uuid from map where map_file = :map_file", array (
                "map_file" => $mapfile 
            ));
            if( !empty($tabMaps) ) {
                foreach($tabMaps as $val){
                    $mapUuid = $val ['map_wmsmetadata_uuid'];
                    // redirect to map identifier
                    $urlOpenMap = $acces_adress_admin . "/edit_map/" . $mapUuid;
                }
            } else { // WMS rep does not exists, but representation exists, copy it
                $tabMaps = $connection->fetchAll("select map_id from map where map_wmsmetadata_uuid = :uuid and published_id is null", array (
                    "uuid" => $uuid 
                ));
                if ( !empty($tabMaps) ) {
                    foreach($tabMaps as $val){
                        $mapId = $val ['map_id'];
                        // generate new uuid
                        $mapUuid = uniqid();
                        // 1 create new temporary_map from model
                        $carmenEditMapAction = $acces_adress_admin . "/api/map/edit/" . $mapId."/resume";
                        $ticket = \Alk\Common\CasBundle\Security\Authentication\Provider\CasProvider::getPGT($carmenEditMapAction);
                        $jsonResp = $this->curl($carmenEditMapAction, 'GET', array ('ticket' => $ticket), array());
                        
                        $jsonObj = json_decode($jsonResp);
                        if ( $jsonObj && $jsonObj->success && $jsonObj->map && $jsonObj->map->mapId ) {
                            $map_id = $jsonObj->map->mapId;
                            // 2 change map parameters
                            $prodigeConnection = $this->getProdigeConnection("carmen");
                            $prodigeConnection->executeUpdate("update map set published_id =:map_id,  map_wmsmetadata_uuid=:uuid where map_id=:map_id", array (
                                "map_id" => $map_id,
                                "uuid" => $mapUuid 
                            ));
                            
                            // 4 save/publish Map
                            $carmenSaveMapAction = $acces_adress_admin . "/api/map/publish/" . $map_id . "/" . $map_id;
                            $ticket = \Alk\Common\CasBundle\Security\Authentication\Provider\CasProvider::getPGT($carmenSaveMapAction);
                            $jsonResp = $this->curl($carmenSaveMapAction, 'POST', array ('ticket' => $ticket), array (
                                "mapModel" => "false",
                                "mapFile" => "layers/WMS/" . str_replace("layers/WMS/", "", $mapfile) 
                            ));
                            $jsonObj = json_decode($jsonResp);
                            if ( $jsonObj && $jsonObj->success && $jsonObj->map ) {
                                // redirect to map
                                $urlOpenMap = $acces_adress_admin . "/edit_map/" . $mapUuid;
                            }
                        }
                        // $urlOpenMap = $request->getSchemeAndHttpHost().$this->generateUrl('catalogue_geosource_layerAddToMap', array("uuid" =>$uuid))."?mapUuid=".$mapUuid."&model=".$mapId."&prefixMapfile=layers/WMS/";
                    }
                } else { // WMS and representation does not exists, create from model
                       // generate new uuid
                    $mapUuid = uniqid();
                    $urlOpenMap = $request->getSchemeAndHttpHost() . $this->generateUrl('catalogue_geosource_layerAddToMap', array (
                            "uuid" => $uuid 
                    )) . "?mapUuid=" . $mapUuid . "&prefixMapfile=layers/WMS/";
                }
            }
            
            $strHtml .= "<div class=\"titre\"><h2>Ajouter la couche \"" . $tabParam ["title"] . "\" sur le serveur " . $action . "</h2></div>
        <div class=\"formulaire\">
            <form method=\"POST\" name=\"administration_carto\" target=\"Cartographie_dynamique\" action =\"\">
                <input type=\"hidden\" name=\"login\" value = \"\"/>
                <input type=\"hidden\" name=\"pass\" value = \"\"/>
            </form>
            <form name = 'formService' action=\"" . $request->getRequestUri() . "\" method=\"POST\">
                <input type=\"hidden\" name=\"DATA\" id=\"DATA\" value=\"" . $data . "\"/>
                <input type=\"hidden\" name=\"TYPE_STOCKAGE\" id=\"TYPE_STOCKAGE\" value=\"" . $type_stokage . "\"/>
                <input type=\"hidden\" name=\"ACTION\" id=\"ACTION\" value=\"" . $action . "\"/>
                <input type=\"hidden\" name=\"OPENER\" id=\"OPENER\" value=\"" . $opener . "\"/>
                <input type=\"hidden\" name=\"domaine\" id=\"domaine\" value=\"" . $domaine . "\"/>
                <input type=\"hidden\" name=\"sousdomaine\" id=\"sousdomaine\" value=\"" . $sousdomaine . "\"/>
                <input type=\"hidden\" name=\"login\" id=\"login\" value=\"" . $login . "\"/>
                <input type=\"hidden\" name=\"pass\" id=\"pass\" value=\"" . $pass . "\"/>
                <input type=\"hidden\" name=\"couchd_id\" id=\"couchd_id\" value=\"" . $coucheId . "\"/>
                <input type=\"hidden\" name=\"metadataUrl\" id=\"metadataUrl\" value=\"" . $metadataUrl . "\"/>
            <table>
            <tr>
                <th width=\"500px\">Titre</th>
                 <td width=\"400px\"><input type='text' name=\"title\" id=\"title\" value=\"" . $tabParam ["title"] . "\" size=\"35\" maxlength=\"100\"></td>
            </tr>
            <tr>
                <th width=\"500px\">Nom de la couche</th>
                <td width=\"400px\"><input type='text' name=\"layername\" id=\"layername\" " . ($bIsLayerInService ? "readonly" : "") . " value=\"" . $tabParam ["layername"] . "\" size=\"35\" maxlength=\"100\">
                <br><i>Le nom de la couche n'est pas modifiable une fois enregistré.</i>
                </td>
             </tr>";
            if ( $type_stokage == "1" ) {
                $strHtml .= "
        <tr>
          <th>Champ identifiant</th>
              <td>
                <select name=\"gml_featureid\" id=\"gml_featureid\">";
                
                for($i = 0; $i < count($tabSelectFields); $i ++) {
                    $strHtml .= "<option " . ($tabSelectFields [$i] == $tabParam ["gml_featureid"] ? "selected=\"\"" : "") . " value=\"" . $tabSelectFields [$i] . "\">" . $tabSelectFields [$i] . "</option>";
                }
                $strHtml .= " </select>
        </td>
    </tr>
    <tr>
        <th>Champs mis à disposition</th>
        <td><select multiple=\"multiple\" size=\"10\" name=\"gml_include_items[]\" id=\"feature_items\">";
                for($i = 0; $i < count($tabSelectFields); $i ++) {
                    $strHtml .= "<option " . (in_array($tabSelectFields [$i], $tabParam ["gml_include_items"])? " selected=\"selected\"" : "") . " value=\"" . $tabSelectFields [$i] . "\">" . $tabSelectFields [$i] . "</option>";
                }
                $strHtml .= "</select>
        </td>
    </tr>";
            } elseif ( $type_stokage == "-4" ) {
                $strHtml .= "
    <tr>
        <th>Champ identifiant</th>
        <td>
            <select name=\"gml_featureid\" id=\"gml_featureid\">";
                for($i = 0; $i < count($tabSelectFields); $i ++) {
                    $strHtml .= "<option " . ($tabSelectFields [$i] == $tabParam ["gml_featureid"] ? "selected=\"\"" : "") . " value=\"" . $tabSelectFields [$i] . "\">" . $tabSelectFields [$i] . "</option>";
                }
                $strHtml .= "
        </td>
    </tr>
    <tr>
        <th>Champs mis à disposition</th>
        <td>
            <select multiple=\"multiple\" size=\"10\" name=\"gml_include_items[]\" id=\"feature_items\">";
                for($i = 0; $i < count($tabSelectFields); $i ++) {
                    $strHtml .= "<option " . (in_array($tabSelectFields [$i], $tabParam ["gml_include_items"])? "selected=\"selected\"" : "") . " value=\"" . $tabSelectFields [$i] . "\">" . $tabSelectFields [$i] . "</option>";
                }
                $strHtml .= "</td>
    </tr>";
            }
            
            $strHtml .= "
    <tr>
        <th>Mots clés</th>
        <td><input type='text' name=\"keywords\" id=\"keywords\" value=\"" . $tabParam ["keywordslist"] . "\" size=\"35\" maxlength=\"100\"></td>
    </tr>
    <tr>
        <th>résumé</th>
        <td><input type='text' name=\"abstract\" id=\"abstract\" value=\"" . $tabParam ["abstract"] . "\" size=\"35\" maxlength=\"100\"></td>
    </tr>
 ";
            // $tabDomSdom = explode("|", $strDomSdom);
            
            if ( $action == "wms" ) {
                $strHtml .= "<tr>
    <th>Groupe de publication " . $action . "</th>
    <td><select id=\"layer_group\" name=\"layer_group\">";
                for($i = 0; $i < count($tabDomSdom); $i ++) {
                    $strHtml .= "<option " . ("/" . $tabDomSdom [$i] ["domsdom"] == $tabParam ["layer_group"] ? "selected" : "") . " value=\"" . $tabDomSdom [$i] ["domsdom"] . "\">" . $tabDomSdom [$i] ["domsdom"] . "</option>";
                }
                $strHtml .= "</td></tr>";
            }
            $strHtml .= "<tr>
    <th>Projection</th>
    <td><select id=\"srs\" multiple size =5 name=\"srs[]\">
 ";
            $tabEPSGCodes = json_decode(GetProjectionsController::getProjectionsAction($request)->getContent(), true);
            
            for($i = 0; $i < count($tabEPSGCodes); $i ++) {
                $strHtml .= "<option value=\"EPSG:" . $tabEPSGCodes [$i] ["epsg"] . "\" " . (in_array("EPSG:" . $tabEPSGCodes [$i] ["epsg"], $tabParam ["srs"])? "selected=\"selected\"" : "") . ">" . $tabEPSGCodes [$i] ["nom"] . "[EPSG:" . $tabEPSGCodes [$i] ["epsg"] . "]</option>";
            }
            $strHtml .= "       </select>
        </td>
    </tr>";
            
            $strHtml .= "
                            " . ($action == "wms" ? "<tr>
                    <th>Représentation par défaut</th>
                        <td style='white-space:nowrap'>
                        <input type=\"button\" name=\"RETOUR\" value=\"Ouvrir la carte\" onclick=\"window.open('" . ($urlOpenMap) . "');\">
                        <i>Les modifications ne sont prises en compte qu'après validation.</i>
                    </td>
                </tr>" : "") . "
                 <tr>
                    <td class=\"validation\" style=\"text-align:right\">
                        <input type=\"button\" name=\"VALIDATION\" value=\"Valider\" onclick=\"administration_carto_services_valider()\">&nbsp;
                    </td><td class=\"validation\">" . ($bIsLayerInService ? "<input type='button' name='supprimer' value='Supprimer' onclick=\"administration_carto_services_supprimer()\">" : "") . "</td>
                </tr>
            </table>
         </form>
        </div>
    </body>
    </html>";
        }
        echo $strHtml;
        // die();
        exit();
    }
    
    /**
     * @Secure(roles="ROLE_USER")
     *
     * @Route("/mapAddToWebService/{uuid}", name="catalogue_geosource_mapAddToWebService", options={"expose"=true})
     */
    public function mapAddToWebServiceAction(Request $request, $uuid) {
        $conn = $this->getCatalogueConnection('catalogue');
        $dao = new DAO($conn, 'catalogue');
        
        $carte_id = $request->query->get("carteId");
        $query = 'SELECT stkcard_path, fmeta_id, pk_carte_projet, cartp_wms
                    FROM catalogue.cartes_sdom ' . ' where pk_carte_projet = ?';
        
        $rs = $dao->BuildResultSet($query, array (
                $carte_id 
        ));
        if ( $rs->GetNbRows() > 0 ) {
            $rs->First();
            $cartp_emplacement_stockage = $rs->Read(0);
            $data = $rs->Read(1);
            $pk_carte_projet = $rs->Read(2);
            $cartp_wms = $rs->Read(3); // can be 0 or id metadata service
        }
        if ( $cartp_wms == 0 ) {
            $tabLayer = $this->createWmsMapFile($cartp_emplacement_stockage, $uuid);
            list($idMetadataService, $uuidService)= $this->createServiceWMSFiche($data, $tabLayer, $pk_carte_projet, $dao, $cartp_emplacement_stockage);
            $this->modifyMapFiche($data, $uuidService, $dao);
            $enable = 1;
        } else {
            $this->deleteMapfile($cartp_emplacement_stockage);
            
            $geonetwork = new GeonetworkInterface(PRO_GEONETWORK_URLBASE, 'srv/fre/');
            $this->delServiceInMapFiche($data, $cartp_wms, $dao);
            $geonetwork->get('md.delete?_content_type=json&id=' . $cartp_wms);
            // don't throw exception, service metadata can be absent
            $enable = 0;
            $idMetadataService = 0;
        }
        
        $query = 'update CARTE_PROJET set cartp_wms = :cartp_wms where PK_CARTE_PROJET = :PK_CARTE_PROJET ;';
        $dao->Execute($query, array (
                "cartp_wms" => $idMetadataService,
                "PK_CARTE_PROJET" => $carte_id 
        ));
        
        return new JsonResponse(array (
            "success" => "true",
            "enable" => $enable == 1 
        ));
    }
    
    /**
     *
     * @param unknown $cartp_emplacement_stockage            
     */
    protected function deleteMapfile($cartp_emplacement_stockage) {
        if ( file_exists(PRO_MAPFILE_PATH . "wms_" . $cartp_emplacement_stockage . ".map") ) {
            unlink(PRO_MAPFILE_PATH . "wms_" . $cartp_emplacement_stockage . ".map");
        }
    }
    
    /**
     * suppression du tag gmd:onLine lié à une donnée WMS
     * 
     * @param $doc
     * @param $serviceUrl
     * @param $serviceProtocol
     * @return unknown_type
     */
    protected function delServiceInMapFiche($idMetadataCarte, $idMetadataService, $dao) {
        $query = 'SELECT uuid FROM public.metadata where id = ?';
        $rs = $dao->BuildResultSet($query, array (
                $idMetadataService 
        ));
        if ( $rs->GetNbRows() > 0 ) {
            $rs->First();
            $uuidMetadataService = $rs->Read(0);
            
            $query = 'SELECT data FROM public.metadata where id = ?';
            $rs = $dao->BuildResultSet($query, array (
                    $idMetadataCarte 
            ));
            if ( $rs->GetNbRows() > 0 ) {
                $rs->First();
                $metadata_data = $rs->ReadHtml(0);
            }
            $version = "1.0";
            $encoding = "UTF-8";
            $metadata_doc = new \DOMDocument($version, $encoding);
            $entete = "<?xml version=\"" . $version . "\" encoding=\"" . $encoding . "\"?>\n";
            $metadata_data = $entete . $metadata_data;
            
            if ( $metadata_doc->loadXML($metadata_data) !== false ) {
                $xpath = new \DOMXPath($metadata_doc);
                // caractérisation des noeuds via l'URL de service
                $objXPath = $xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/srv:containsOperations/srv:SV_OperationMetadata" . "/srv:connectPoint/gmd:CI_OnlineResource/gmd:linkage/gmd:URL");
                
                if ( $objXPath->length > 0 && $objXPath->item(0)&& $objXPath->item(0 )->nodeValue == PRO_GEONETWORK_URLBASE . '/srv/fre/catalog.search#/metadata/' . $uuidMetadataService ) {
                    $objXPath->item(0 )->parentNode->parentNode->parentNode->parentNode->parentNode->parentNode->removeChild($objXPath->item(0 )->parentNode->parentNode->parentNode->parentNode->parentNode);
                }
                
                $objXPath = $xpath->query("/gmd:MD_Metadata/gmd:distributionInfo/gmd:MD_Distribution/gmd:transferOptions/gmd:MD_DigitalTransferOptions/gmd:onLine/" . "gmd:CI_OnlineResource/gmd:linkage/gmd:URL");
                if ( $objXPath->length > 0 && $objXPath->item(0)&& $objXPath->item(0 )->nodeValue == PRO_GEONETWORK_URLBASE . '/srv/fre/catalog.search#/metadata/' . $uuidMetadataService ) {
                    $objXPath->item(0 )->parentNode->parentNode->parentNode->parentNode->removeChild($objXPath->item(0 )->parentNode->parentNode->parentNode);
                }
                $new_metadata_data = $metadata_doc->saveXML();
                
                $new_metadata_data = str_replace($entete, "", $new_metadata_data);
                
                // Prodige4
                $geonetwork = new GeonetworkInterface(PRO_GEONETWORK_URLBASE, 'srv/fre/');
                $urlUpdateData = "md.edit.save";
                $formData = array (
                        "id" => $idMetadataCarte,
                        "data" => $new_metadata_data 
                );
                // send to geosource
                $geonetwork->post($urlUpdateData, $formData);
            }
        }
    }
    
    /**
     * update map metadat to include link to service
     * 
     * @param unknown $idMetadataCarte            
     * @param unknown $uuidMetadataService            
     * @param unknown $dao            
     */
    protected function modifyMapFiche($idMetadataCarte, $uuidMetadataService, $dao) {
        // global $PRO_GEONETWORK_DIRECTORY;
        $query = 'SELECT data FROM public.metadata where id = ?';
        $rs = $dao->BuildResultSet($query, array (
                $idMetadataCarte 
        ));
        if ( $rs->GetNbRows() > 0 ) {
            $rs->First();
            $metadata_data = $rs->ReadHtml(0);
        }
        $version = "1.0";
        $encoding = "UTF-8";
        $metadata_doc = new \DOMDocument($version, $encoding);
        $entete = "<?xml version=\"" . $version . "\" encoding=\"" . $encoding . "\"?>\n";
        $metadata_data = $entete . $metadata_data;
        if ( $metadata_doc->loadXML($metadata_data) !== false ) {
            $xpath = new \DOMXPath($metadata_doc);
            $strXml = '<srv:containsOperations  xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns:srv="http://www.isotc211.org/2005/srv" xmlns:gco="http://www.isotc211.org/2005/gco">
              <srv:SV_OperationMetadata>
                  <srv:operationName>
                    <gco:CharacterString>Accès à la métadonnée de service WMS</gco:CharacterString>
                  </srv:operationName>
                  <srv:DCP>
                    <srv:DCPList codeList="http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#DCPList" codeListValue="WebServices"/>
                  </srv:DCP>
                  <srv:connectPoint>
                      <gmd:CI_OnlineResource>
                          <gmd:linkage>
                            <gmd:URL>' . PRO_GEONETWORK_URLBASE . '/srv/fre/catalog.search#/metadata/' . $uuidMetadataService . '</gmd:URL>
                          </gmd:linkage>
                          <gmd:protocol>
                            <gco:CharacterString>WWW:LINK-1.0-http--link</gco:CharacterString>
                          </gmd:protocol>
                      </gmd:CI_OnlineResource>
                  </srv:connectPoint>
              </srv:SV_OperationMetadata>
           </srv:containsOperations>';
            
            $tagRepport = new \DOMDocument();
            $tagRepport->loadXML($strXml);
            $nodeToImport = $tagRepport->getElementsByTagName("containsOperations" )->item(0);
            $coupledResource = $metadata_doc->getElementsByTagName("SV_ServiceIdentification");
            $objXPath = @$xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/*");
            if ( $objXPath->length > 0 ) {
                $element = $objXPath->item(0);
            }
            if ( $coupledResource && $coupledResource->length > 0 ) {
                $newNodeDesc = $metadata_doc->importNode($nodeToImport, true);
                $coupledResource->item(0 )->insertBefore($newNodeDesc, $element);
            }
            
            $strXml = '<gmd:onLine xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns:gco="http://www.isotc211.org/2005/gco">
              <gmd:CI_OnlineResource>
                  <gmd:linkage>
                    <gmd:URL>' . PRO_GEONETWORK_URLBASE . '/srv/fre/catalog.search#/metadata/' . $uuidMetadataService . '</gmd:URL>
                  </gmd:linkage>
                  <gmd:protocol>
                    <gco:CharacterString>WWW:LINK-1.0-http--link</gco:CharacterString>
                  </gmd:protocol>
                  <gmd:name>
                    <gco:CharacterString>Accès à la métadonnée de service</gco:CharacterString>
                  </gmd:name>
              </gmd:CI_OnlineResource>
          </gmd:onLine>';
            
            $tagRepport = new \DOMDocument();
            $tagRepport->loadXML($strXml);
            $nodeToImport = $tagRepport->getElementsByTagName("onLine" )->item(0);
            $coupledResource = $metadata_doc->getElementsByTagName("MD_DigitalTransferOptions");
            $objXPath = @$xpath->query("/gmd:MD_Metadata/gmd:distributionInfo/gmd:MD_Distribution/gmd:transferOptions/gmd:MD_DigitalTransferOptions/*");
            if ( $objXPath->length > 0 ) {
                $element = $objXPath->item(0);
            }
            if ( $coupledResource && $coupledResource->length > 0 ) {
                $newNodeDesc = $metadata_doc->importNode($nodeToImport, true);
                $coupledResource->item(0 )->insertBefore($newNodeDesc, $element);
            }
            
            $new_metadata_data = $metadata_doc->saveXML();
            $new_metadata_data = str_replace($entete, "", $new_metadata_data);
            // Prodige4
            $geonetwork = new GeonetworkInterface(PRO_GEONETWORK_URLBASE, 'srv/fre/');
            $urlUpdateData = "md.edit.save";
            $formData = array (
                    "id" => $idMetadataCarte,
                    "data" => $new_metadata_data 
            );
            // send to geosource
            $geonetwork->post($urlUpdateData, $formData);
        }
    }
    /**
     *
     * @param unknown $carte_id            
     * @param unknown $cartp_emplacement_stockage            
     * @param unknown $dao            
     */
    protected function createWmsMapFile($cartp_emplacement_stockage, $uuidMap) {
        $done = false;
        $mapName = basename($cartp_emplacement_stockage, ".map");
        $wms_online = $this->getParameter("PRODIGE_URL_DATACARTO"). "/WMS/" . $mapName;
        $wms_file = PRO_MAPFILE_PATH . "wms_" . $mapName . ".map";
        $map_file = PRO_MAPFILE_PATH . $mapName . ".map";
        
        if ( !empty($wms_online)&& !empty($map_file)&& is_file($map_file) ) {
            // Copie de la carte
            $done = false;
            $done = copy($map_file, $wms_file);
            
            // ouverture de la carte
            if ( $done ) {
                $m_mapWms = ms_newMapObj($wms_file);
                // autorisation des requêtes OGC WMS
                $m_mapWms->setMetadata("wms_enable_request", "*");
                $m_mapWms->set("maxsize", 10000);
                // positionnement de la ressource wms: wms_onlineresource
                $m_mapWms->setMetaData("wms_onlineresource", $wms_online);
                
                $wms_title = urldecode($m_mapWms->name);
                $m_mapWms->setMetaData("wms_title", $wms_title);
                $meta = preg_replace("[^a-zA-Z0-9]", "_", $wms_title);
                
                $m_mapWms->set('name', $meta);
                
                // récupération des projection WMS par défaut
                $tabInfoWMS = explode(";", PRO_PROJ_WMS);
                $epsg = "";
                for($i = 0; $i < count($tabInfoWMS); $i ++) {
                    $curEpsg = preg_split("/ - /", $tabInfoWMS [$i]);
                    $curEpsg = $curEpsg [0];
                    // echo trim($curEpsg[0],"EPSG:")."test\n";
                    $epsg .= $curEpsg . " ";
                }
                $epsg = rtrim($epsg, " ");
                $m_mapWms->setMetaData("wms_srs", $epsg);
                
                $m_mapWms->legend->label->set("encoding", "utf-8");
                $m_mapWms->legend->label->set("font", "Arial");
                $m_mapWms->legend->label->set("size", 10);
                
                $map_extent = $m_mapWms->extent;
                $wms_extent = $map_extent->minx . ' ' . $map_extent->miny . ' ' . $map_extent->maxx . ' ' . $map_extent->maxy;
                $tabLayers = array();
                for($i = 0; $i < $m_mapWms->numlayers; $i ++) {
                    $oLayer = $m_mapWms->getLayer($i);
                    if ( $oLayer ) {
                        // vérification de la présence du layer dans le service WMS de la plateforme
                        if ( $this->isLayerInServiceWMS($oLayer) ) {
                            // $name = $oLayer->getMetadata("LAYER_TITLE");
                            
                            $conn = $this->getProdigeConnection('carmen');
                            $query = "select layer_title, group_name  from layer inner join map on layer.map_id = map.map_id" . " left join map_tree on layer.layer_id = map_tree.node_id" . " left join map_group on map_tree.node_parent = map_group.group_id" . " where map.published_id is null and layer_msname  =:layer_msname and map_wmsmetadata_uuid     =:map_wmsmetadata_uuid ";
                            
                            $tabInfoLayer = $conn->fetchAll($query, array (
                                "layer_msname" => $oLayer->name,
                                "map_wmsmetadata_uuid" => $uuidMap 
                            ));
                            foreach($tabInfoLayer as $infos){
                                $title = $infos ["layer_title"];
                                $groupName = $infos ["group_name"];
                            }
                            $oLayer->setMetaData("wms_title", $title);
                            $oLayer->set("template", "consultable");
                            
                            $layer_name = $this->removeAccents(str_replace(" ", "", stripslashes($title)));
                            $layer_name = $this->neutralisationChaine($layer_name);
                            $layer_name = preg_replace("/[^a-zA-Z0-9_]/", "", $layer_name);
                            
                            $oLayer->set('name', $layer_name);
                            
                            $oLayer->setMetaData('wms_extent', $wms_extent);
                            $oLayer->setMetaData('wms_encoding', 'utf-8');
                            if ( $oLayer->type == 3 ) {
                                $tabLayers [$layer_name] = $oLayer->data;
                                // TODO à vérifier
                            } else if ( $oLayer->type == 2 && $oLayer->connectiontype == 6 ) {
                                $tabSelectFields = array();
                                $data = $oLayer->data;
                                $match = array();
                                preg_match("/\(.*\)/", $data, $match);
                                $match = preg_split("/ /", $match [0]);
                                $data = trim($match [count($match)- 1], ")");
                                $query = "SELECT a.attnum, a.attname " . " FROM pg_class c, pg_attribute a, pg_type t " . " WHERE c.relname = '" . $data . "'" . " and a.attnum > 0" . " and a.attname <> 'the_geom'" . " and a.attrelid = c.oid" . " and a.atttypid = t.oid" . " ORDER BY attnum";
                                $tabLayers [$layer_name] = $data;
                                
                                $conn = $this->getProdigeConnection('public');
                                $dao = new DAO($conn, 'public');
                                if ( $dao ) {
                                    $rs = $dao->BuildResultSet($query);
                                }
                                for($rs->First(); !$rs->EOF(); $rs->Next()) {
                                    $tabSelectFields [] = $rs->read(1);
                                }
                                $oLayer->setMetaData('wms_include_items', join(",", $tabSelectFields));
                                $oLayer->setMetaData("gml_include_items", join(",", $tabSelectFields));
                            }
                            /*
                             * $arbo = str_replace("[+]", "", $oLayer->getMetaData("GI_ARBO_GROUP"));
                             * $arbo = str_replace("[-]", "", $arbo);
                             * $oLayer->setMetaData('wms_layer_group',"/".$arbo);
                             */
                            
                            $oLayer->setMetaData('wms_layer_group', "/" . $groupName);
                            // TODO get uuid for each Layer from data
                            // $oLayer->setMetaData('wms_metadataurl_href', PRO_GEONETWORK_URLBASE."srv/".$uuidMap);
                            $oLayer->setMetaData('wms_metadataurl_format', 'text/html');
                            $oLayer->setMetaData('wms_abstract', '');
                            $oLayer->setMetaData('wms_metadataurl_type', '');
                            $oLayer->set('dump', MS_TRUE);
                        } else {
                            $oLayer->set("status", MS_DELETE);
                        }
                    }
                }
                $m_mapWms->save($wms_file);
                return $tabLayers;
            }
            
            if ( !$done ) {
                throw new \Exception("Impossible de publier en WMS");
            }
        } else {
            throw new \Exception("Impossible de publier en WMS (2)");
        }
    }
    
    /**
     * création de la métadonnée de service de carte et remplissage automatique de son contenu
     * 
     * @param unknown $data            
     * @param unknown $tabLayer            
     * @param unknown $pk_carte_projet            
     * @param unknown $dao            
     * @param unknown $cartp_emplacement_stockage            
     */
    protected function createServiceWMSFiche($data, $tabLayer, $pk_carte_projet, $dao, $cartp_emplacement_stockage) {
        // global $PRO_GEONETWORK_DIRECTORY, $accs_adresse_data;
        $urlWebservice = $this->getParameter("PRODIGE_URL_DATACARTO"). "/WMS/" . basename($cartp_emplacement_stockage, ".map")."?service=WMS&amp;request=GetCapabilities";
        
        $query = "select groupowner from public.metadata where id=:id";
        $rs = $dao->BuildResultSet($query, array (
                "id" => $data 
        ));
        //if groupowner is null, gives it to All(Internet)
        $group_id = 1;
        
        if ( $rs->GetNbRows() > 0 ) {
            $rs->First();
            $group_id = $rs->Read(0);
        } else {
            throw new \Exception("Impossible de trouver la métadonnée " . $data);
        }
        
        //if not groupowner for metadata, take first on wich user has rights to edit
        if($group_id==1){
            $query = "select groupid from public.users left join public.usergroups on usergroups.userid = users.id ".
                " where usergroups.profile >1 and username=:username limit 1";
            $userSingleton = User::GetUser();
            $rs = $dao->BuildResultSet($query, array (
                "username" => $userSingleton->getLogin()
            ));
            if ( $rs->GetNbRows() > 0 ) {
                $rs->First();
                $group_id = $rs->Read(0);
            }
        }
        
        $geonetwork = new GeonetworkInterface(PRO_GEONETWORK_URLBASE, 'srv/fre/');
        $jsonResp = $geonetwork->get('md.create?_content_type=json&id=' . $data . "&group=" . $group_id . "&template=n&child=n&fullPrivileges=false");
        
        $jsonObj = json_decode($jsonResp);
        if ( $jsonObj && $jsonObj->id ) {
            $metadataId = $jsonObj->id;
        } else {
            throw new \Exception("Impossible de créer la métadonnée à partir du modèle de fiche de carte");
        }
        
        // load metadata
        $metadata_data = "";
        $query = 'SELECT data, uuid
                  FROM public.metadata' . ' where id = ?';
        $rs = $dao->BuildResultSet($query, array (
                $metadataId 
        ));
        if ( $rs->GetNbRows() > 0 ) {
            $rs->First();
            $metadata_data = $rs->ReadHtml(0);
            $uuid = $rs->Read(1);
        }
        
        $version = "1.0";
        $encoding = "UTF-8";
        $metadata_doc = new \DOMDocument($version, $encoding);
        $entete = "<?xml version=\"" . $version . "\" encoding=\"" . $encoding . "\"?>\n";
        $metadata_data = $entete . $metadata_data;
        if ( @$metadata_doc->loadXML($metadata_data) !== false ) {
            $xpath = new \DOMXPath($metadata_doc);
            $objXPath = @$xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/gmd:citation/gmd:CI_Citation/gmd:title/gco:CharacterString");
            if ( $objXPath->length > 0 ) {
                $element = $objXPath->item(0);
                $title = $element->nodeValue;
                $newelement = $metadata_doc->createElement('gco:CharacterString', $title . " (service WMS)");
                $element->parentNode->replaceChild($newelement, $element);
            }
            $serviceType = $metadata_doc->getElementsByTagName("serviceType");
            $objXPath = @$xpath->query("//gco:LocalName", $serviceType->item(0));
            if ( $objXPath && $objXPath->length > 0 ) {
                $element = $objXPath->item(0);
                $newelement = $metadata_doc->createElement('gco:LocalName', "view");
                $element->parentNode->replaceChild($newelement, $element);
            }
            
            $strXml = '<srv:coupledResource xmlns:srv="http://www.isotc211.org/2005/srv" xmlns:gco="http://www.isotc211.org/2005/gco">';
            $strXmlOp = "<tag xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:srv=\"http://www.isotc211.org/2005/srv\">";
            if ( !empty($tabLayer) ) {
                foreach($tabLayer as $layerName => $layerData){
                    $query = "SELECT distinct uuid FROM catalogue.couche_sdom where couchd_emplacement_stockage =:couchd_emplacement_stockage";
                    $query = "select uuid from fiche_metadonnees" . " INNER JOIN couche_donnees ON couche_donnees.pk_couche_donnees = fiche_metadonnees.fmeta_fk_couche_donnees" . " INNER JOIN public.metadata ON public.metadata.id = fiche_metadonnees.fmeta_id::integer " . " where couche_donnees.couchd_emplacement_stockage = ?";
                    $rs = $dao->BuildResultSet($query, array (
                            $layerData 
                    ));
                    
                    if ( $rs->GetNbRows() > 0 ) {
                        $rs->First();
                        $uuidLayer = $rs->Read(0);
                        $strXml .= '<srv:SV_CoupledResource >
                        <srv:operationName>
                            <gco:CharacterString>GETMAP</gco:CharacterString>
                        </srv:operationName>
                        <srv:identifier>
                            <gco:CharacterString>' . $uuidLayer . '</gco:CharacterString>
                        </srv:identifier>
                        <gco:ScopedName>' . $layerName . '</gco:ScopedName>
                    </srv:SV_CoupledResource>';
                        $strXmlOp .= '<srv:operatesOn uuidref="' . $uuidLayer . '" xlink:href="' . PRO_GEONETWORK_URLBASE . '/srv/' . $uuidLayer . '" />';
                    }
                }
            }
            $strXmlOp .= "</tag>";
            $strXml .= "</srv:coupledResource>";
            $tagRepport = new \DOMDocument();
            $tagRepport->loadXML($strXml);
            $nodesToImport = $tagRepport->getElementsByTagName("SV_CoupledResource");
            $coupledResource = $metadata_doc->getElementsByTagName("SV_ServiceIdentification");
            if ( $coupledResource && $coupledResource->length > 0 ) {
                
                for($c = 0; $c < $nodesToImport->length; $c ++) {
                    $newNodeDesc = $metadata_doc->importNode($nodesToImport->item($c ), true);
                    $coupledResource->item(0 )->appendChild($newNodeDesc);
                }
            }
            
            $tagRepport = new \DOMDocument();
            $tagRepport->loadXML($strXmlOp);
            $nodes = $tagRepport->getElementsByTagName("operatesOn");
            $serviceIdentification = $metadata_doc->getElementsByTagName("SV_ServiceIdentification");
            
            if ( $serviceIdentification && $serviceIdentification->length > 0 ) {
                for($c = 0; $c < $nodes->length; $c ++) {
                    $nodeToImport = $nodes->item($c);
                    $newNodeDesc = $metadata_doc->importNode($nodeToImport, true);
                    $serviceIdentification->item(0 )->appendChild($newNodeDesc);
                }
            }
            
            $strXml = '<test xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns:srv="http://www.isotc211.org/2005/srv" xmlns:gco="http://www.isotc211.org/2005/gco">
             <srv:containsOperations>
              <srv:SV_OperationMetadata>
                <srv:operationName>
                  <gco:CharacterString>GetCapabilities</gco:CharacterString>
                </srv:operationName>
                <srv:DCP>
                  <srv:DCPList codeList="http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#DCPList" codeListValue="WebServices"/>
                </srv:DCP>
                <srv:connectPoint>
                  <gmd:CI_OnlineResource>
                    <gmd:linkage>
                      <gmd:URL>' . $urlWebservice . '</gmd:URL>
                    </gmd:linkage>
                    <gmd:protocol>
                      <gco:CharacterString>OGC:WMS-1.1.1-http-get-map</gco:CharacterString>
                    </gmd:protocol>
                  </gmd:CI_OnlineResource>
                </srv:connectPoint>
              </srv:SV_OperationMetadata>
            </srv:containsOperations>
            <srv:containsOperations>
              <srv:SV_OperationMetadata>
                <srv:operationName>
                  <gco:CharacterString>GetMap</gco:CharacterString>
                </srv:operationName>
                <srv:DCP>
                  <srv:DCPList codeList="http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#DCPList" codeListValue="WebServices"/>
                </srv:DCP>
                <srv:connectPoint>
                  <gmd:CI_OnlineResource>
                    <gmd:linkage>
                      <gmd:URL>' . $urlWebservice . '</gmd:URL>
                    </gmd:linkage>
                    <gmd:protocol>
                      <gco:CharacterString>OGC:WMS-1.1.1-http-get-map</gco:CharacterString>
                    </gmd:protocol>
                  </gmd:CI_OnlineResource>
                </srv:connectPoint>
              </srv:SV_OperationMetadata>
            </srv:containsOperations>
            <srv:containsOperations>
              <srv:SV_OperationMetadata>
                <srv:operationName>
                  <gco:CharacterString>GetFeatureInfo</gco:CharacterString>
                </srv:operationName>
                <srv:DCP>
                  <srv:DCPList codeList="http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#DCPList" codeListValue="WebServices"/>
                </srv:DCP>
                <srv:connectPoint>
                  <gmd:CI_OnlineResource>
                    <gmd:linkage>
                      <gmd:URL>' . $urlWebservice . '</gmd:URL>
                    </gmd:linkage>
                    <gmd:protocol>
                      <gco:CharacterString>OGC:WMS-1.1.1-http-get-map</gco:CharacterString>
                    </gmd:protocol>
                  </gmd:CI_OnlineResource>
                </srv:connectPoint>
              </srv:SV_OperationMetadata>
            </srv:containsOperations></test>';
            
            $tagRepport = new \DOMDocument();
            $tagRepport->loadXML($strXml);
            
            // suppression des tags containsOperations existant
            $nodeToDelete = $xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/*/srv:containsOperations");
            for($c = 0; $c < $nodeToDelete->length; $c ++) {
                $nodeToDelete->item($c )->parentNode->removeChild($nodeToDelete->item($c));
            }
            
            // création des tags containsOperations nécessaire (WMS protocole)
            $coupledResource = $metadata_doc->getElementsByTagName("SV_ServiceIdentification");
            if ( $coupledResource && $coupledResource->length > 0 ) {
                $nodes = $tagRepport->getElementsByTagName("containsOperations");
                for($c = 0; $c < $nodes->length; $c ++) {
                    $nodeToImport = $nodes->item($c);
                    $newNodeDesc = $metadata_doc->importNode($nodeToImport, true);
                    $coupledResource->item(0 )->appendChild($newNodeDesc);
                }
            }
        }
        
        $strXmlDistributionInfo = "<tag  xmlns:gmd=\"http://www.isotc211.org/2005/gmd\" xmlns:gco=\"http://www.isotc211.org/2005/gco\">
        <gmd:onLine>
            <gmd:CI_OnlineResource>
                <gmd:linkage>
                    <gmd:URL>" . $urlWebservice . "</gmd:URL>
                </gmd:linkage>
                <gmd:protocol>
                    <gco:CharacterString>application/vnd.ogc.wms_xml</gco:CharacterString>
                </gmd:protocol>
                <gmd:description>
                    <gco:CharacterString>Accès au service WMS</gco:CharacterString>
                </gmd:description>
            </gmd:CI_OnlineResource>
        </gmd:onLine></tag>";
        
        $tagDistInfo = new \DOMDocument();
        $tagDistInfo->loadXML($strXmlDistributionInfo);
        
        //first delete all onlineResource links
        $objXPath = @$xpath->query("/gmd:MD_Metadata/gmd:distributionInfo/gmd:MD_Distribution/gmd:transferOptions/gmd:MD_DigitalTransferOptions/gmd:onLine");
        if ( $objXPath->length > 0 ) {
            foreach($objXPath as $item){
                $item->parentNode->removeChild($item);
            }
        }
        
        $transferOptions = $metadata_doc->getElementsByTagName("MD_DigitalTransferOptions");
        if ( $transferOptions && $transferOptions->length > 0 ) {
            $nodes = $tagDistInfo->getElementsByTagName("onLine");
            for($c = 0; $c < $nodes->length; $c ++) {
                $nodeToImport = $nodes->item($c);
                $newNodeDesc = $metadata_doc->importNode($nodeToImport, true);
                $transferOptions->item(0 )->appendChild($newNodeDesc);
            }
        }
        
        $new_metadata_data = $metadata_doc->saveXML();
        $new_metadata_data = str_replace($entete, "", $new_metadata_data);
        
        // Prodige4
        $geonetwork = new GeonetworkInterface(PRO_GEONETWORK_URLBASE, 'srv/fre/');
        $urlUpdateData = "md.edit.save";
        $formData = array (
                "id" => $metadataId,
                "data" => $new_metadata_data 
        );
        // send to geosource
        $geonetwork->post($urlUpdateData, $formData);
        
        // publish metadata
        $jsonResp = $geonetwork->get('md.privileges.update?_content_type=json&_1_0=on&id=' . $metadataId);
        
        $jsonObj = json_decode($jsonResp);
        if ( $jsonObj && $jsonObj [0] ) {
            return array (
                $metadataId,
                $uuid 
            );
        } else {
            throw new \Exception("Impossible de publier la métadonnée WMS");
        }
    }
    
    /**
     * détermine si la couche est dans le service WMS de la plateforme
     * 
     * @param oLayer
     */
    protected function isLayerInServiceWMS($oLayer) {
        $oMap = @ms_newMapObj(PRO_MAPFILE_PATH . "wms.map");
        if ( $oMap ) {
            
            if ( $oLayer->type == MS_LAYER_RASTER ) {
                for($i = 0; $i < $oMap->numlayers; $i ++) {
                    $m_layerWms = $oMap->getLayer($i);
                    
                    if ( !(strrpos($m_layerWms->data, $oLayer->data) === FALSE) || !(strrpos($m_layerWms->tileindex, $oLayer->data) === FALSE) )
                        return true;
                }
            } elseif ( $oLayer->connectiontype == MS_POSTGIS ) {
                $table = substr($oLayer->data, strpos($oLayer->data, "the_geom from (select * from ")+ strlen("the_geom from (select * from " ), strpos($oLayer->data, ") as foo using unique gid")- (strpos($oLayer->data, "the_geom from (select * from ")+ strlen("the_geom from (select * from ")));
                
                for($i = 0; $i < $oMap->numlayers; $i ++) {
                    $m_layerWms = $oMap->getLayer($i);
                    
                    if ( strpos($m_layerWms->data, $table . " as foo using unique gid") !== false || (strpos($m_layerWms->data, $table . ") as foo using unique gid") !== false) )
                        return true;
                }
            }
        }
        return false;
    }
    
    /**
     * update metadata and service metadata
     * 
     * @param unknown $layerName            
     * @param unknown $action            
     * @param unknown $bUpdate            
     * @param unknown $uuid            
     * @param integer $pk_couche_donnees            
     */
    protected function updateMetadata($layerName, $action, $bUpdate, $uuid, $pk_couche_donnees, $iMode, $layerTitle, $opener=null, 
                    $uuidWFS = "") {
        $conn = $this->getCatalogueConnection('catalogue,public');
        $dao = new DAO($conn, 'catalogue,public');
        
        $urlFrontCarto = $this->getParameter("PRODIGE_URL_FRONTCARTO");
        
        $urlWMS = $urlFrontCarto . "/cgi-bin/mapserv?";
        $urlWFS = $urlFrontCarto . "/cgi-bin/mapservwfs?";
        
        // mise à jour de la base de données
        
        if ( $action == "wms" ) {
            $strField = "COUCHD_WMS";
        } else {
            $strField = "COUCHD_WFS";
        }
        
        // update metadata
        $strSql = "select data, id, uuid from public.metadata " . " where uuid = ?";
        $rs = $dao->BuildResultSet($strSql, array (
                $uuid 
        ));
        for($rs->First(); !$rs->EOF(); $rs->Next()) {
            $data = $rs->ReadHtml(0);
            $id = $rs->Read(1);
            $uuidData = $rs->Read(2);
            $version = "1.0";
            $encoding = "UTF-8";
            $metadata_doc = new \DOMDocument($version, $encoding);
            // chargement de la métadonnée
            $entete = "<?xml version=\"" . $version . "\" encoding=\"" . $encoding . "\"?>\n";
            $metadata_data = $entete . $data;
            $metadata_doc->loadXML(($metadata_data));
            if ( $bUpdate )
                $this->assignDistributionInfo($metadata_doc, ($action == "wms" ? $urlWMS : $urlWFS), ($action == "wfs" ? "OGC:WFS-1.0.0-http-get-capabilities" : "OGC:WMS-1.1.1-http-get-map"), $layerName);
            else
                $this->delDistributionInfo($metadata_doc, ($action == "wms" ? $urlWMS : $urlWFS), ($action == "wfs" ? "OGC:WFS-1.0.0-http-get-capabilities" : "OGC:WMS-1.1.1-http-get-map"), $layerName);
            
            $new_metadata_data = $metadata_doc->saveXML();
            $new_metadata_data = str_replace($entete, "", $new_metadata_data);
            
            // Prodige4
            $geonetwork = new GeonetworkInterface(PRO_GEONETWORK_URLBASE, 'srv/fre/');
            $urlUpdateData = "md.edit.save";
            $formData = array (
                    "id" => $id,
                    "data" => $new_metadata_data 
            );
            // send to geosource
            $geonetwork->post($urlUpdateData, $formData);
        }
        
        // update service metadata
        $strSql = "select data, id from public.metadata where id = ?";
        
        $rs = $dao->BuildResultSet($strSql, array (
                ($action == "wms" ? PRO_WMS_METADATA_ID : PRO_WFS_METADATA_ID) 
        ));

        for($rs->First(); !$rs->EOF(); $rs->Next()) {
            $data = $rs->ReadHtml(0);
            $id = $rs->Read(1);
            $version = "1.0";
            $encoding = "UTF-8";
            $metadata_doc = new \DOMDocument($version, $encoding);
            // chargement de la métadonnée
            $entete = "<?xml version=\"" . $version . "\" encoding=\"" . $encoding . "\"?>\n";
            $metadata_data = $entete . $data;
            // $metadata_data = str_replace("&", "&amp;", $metadata_data);
            $metadata_doc->loadXML(($metadata_data));
            if ( $bUpdate )
                $this->addMetadataInService($metadata_doc, $uuidData, $layerName);
            else
                $this->delMetadataInService($metadata_doc, $uuidData, $layerName);
            
            $new_metadata_data = $metadata_doc->saveXML();
            $new_metadata_data = str_replace($entete, "", $new_metadata_data);
            
            // connect as administrator since editors don't have rights for this metadata
            $geonetwork = $this->getGeonetworkInterface(true);
            $urlUpdateData = "md.edit.save";
            $formData = array (
                    "id" => $id,
                    "data" => $new_metadata_data 
            );
            // send to geosource
            $geonetwork->post($urlUpdateData, $formData);
            
            // specific WFS, create WFS service metadata
            /*if ( $action == "wfs" ) {
                if ( $iMode == 2 ) {
                    $uuidWFS = $this->createServiceWFSFiche($layerTitle, $uuid, $layerName, $dao, $uuidWFS);
                } else {
                    if($bForceDeleteWFSMetadata){
                        //delete wfs since theres's no more data in it
                        $geonetwork->get('md.delete?_content_type=json&uuid=' . $uuidWFS);
                        
                    }else{
                        $uuidWFS = $this->deleteFromWFSFiche($uuid, $layerName, $dao, $uuidWFS);
                    }
                }
            }*/
        }
        
        $query = 'UPDATE COUCHE_DONNEES set ' . $strField . ' = ?, couchd_wfs_uuid=? WHERE PK_COUCHE_DONNEES = ?';
        $dao->Execute($query, array (
                $bUpdate,
                $uuidWFS,
                $pk_couche_donnees 
        ));
        
    
        $secondes = "10";
        $jsEndEdition = "function endEdition(){window.close();}";
        if ( $opener  ){
        	$jsEndEdition = "function endEdition(){
                		  window.location = '".urldecode($opener)."'; 
                    };";
        	
        }
        $jsEndEdition .= "setTimeout(endEdition, ".$secondes."000);"; 
        $jsEndEdition .= "var interval; interval = setInterval(function(){var timer = document.getElementById('timer'); timer.innerHTML = parseInt(timer.innerHTML)-1; if (parseInt(timer.innerHTML)<=0 && interval) clearInterval(interval);}, 1000);"; 
        if ( $iMode == 2 )
            $msg = "La couche a été ajoutée au serveur avec succès";
        else
            $msg = "La couche a été supprimée du serveur avec succès";
            // TODO update opener and extract aff from tis function
        $strHtml = " <!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">
         <html>
          <head>
            <title>Ajouter une couche au serveur " . $action . "</title>
            <META HTTP-EQUIV=\"Content-Type\" CONTENT=\"text/html; charset=UTF-8\">
            <link rel='stylesheet' type='text/css' href='" . 
            $this->generateUrl('catalogue_geosource_get_css_file', array ('css_file' => 'administration_carto')). "'>
            <script type='text/javascript'>".$jsEndEdition."</script>
          </head>
          <body>";
        
        $strHtml .= "<div class=\"errormsg\">" . $msg . "<table align=\"center\"><tr><td class=\"validation\">
        <input type=\"button\" name=\"RETOUR\" value=\"Terminer\" onclick=\"endEdition();\">
        </td> </tr> </table>
        </div>
        <center>Fermeture automatique dans <span id='timer'>".$secondes."</span> secondes.</center>
        </body> </html>";
        echo $strHtml;
    }
    
    /**
     * suppression du tag gmd:onLine lié à une donnée WMS
     * 
     * @param $doc
     * @param $serviceUrl
     * @param $serviceProtocol
     * @return unknown_type
     */
    protected function delDistributionInfo(&$doc, $serviceUrl, $serviceProtocol, $layerName) {
        $xpath = new \DOMXPath($doc);
        $distributionInfo = $xpath->query("/gmd:MD_Metadata/gmd:distributionInfo");
        if ( $distributionInfo->length > 0 ) {
            $transferOptions = $xpath->query("/gmd:MD_Metadata/gmd:distributionInfo/gmd:MD_Distribution/gmd:transferOptions/gmd:MD_DigitalTransferOptions/gmd:onLine");
            if ( $transferOptions->length > 0 ) {
                foreach($transferOptions as $item){
                    $protocole ="";
                    $name = "";
                    
                    $protocol = $item->getElementsByTagName("CharacterString");
                    $url = $item->getElementsByTagName("URL");
                    
                    $names = $xpath->query("gmd:name/gco:CharacterString", $url->item(0)->parentNode->parentNode);
                    if ( $names->length > 0 ) {
                        $name = $names->item(0)->nodeValue;
                    }
                    $protocoles = $xpath->query("gmd:protocol/gco:CharacterString", $url->item(0)->parentNode->parentNode);
                    
                    if ( $protocoles->length > 0 ) {
                        $protocole = $protocoles->item(0)->nodeValue;
                    }
                    
                    if ( $protocole == $serviceProtocol && $name == $layerName && $url && $url->item(0) && $url->item(0)->nodeValue == $serviceUrl ) {
                        // un seul element => on supprime la balise complète
                        if ( $transferOptions->length == 1 ){
                            $distributionInfo->item(0)->parentNode->removeChild($distributionInfo->item(0));
                        }
                        else{
                            $item->parentNode->removeChild($item);
                        }
                    }
                }
            }
        }
    }
    
    /**
     * delete layer from metadata specific WFS sheet
     * @param unknown $uuidLayer
     * @param unknown $layerName
     * @param unknown $dao
     * @param unknown $uuidWfs
     * @return string
     */
    protected function deleteFromWFSFiche($uuidLayer, $layerName, $dao, $uuidWfs){
        
        $strSql = "select data, id from public.metadata where uuid = ?";
        
        $rs = $dao->BuildResultSet($strSql, array (
            $uuidWfs
        ));
        
        for($rs->First(); !$rs->EOF(); $rs->Next()) {
            $data = $rs->ReadHtml(0);
            $id = $rs->Read(1);
            $version = "1.0";
            $encoding = "UTF-8";
            $metadata_doc = new \DOMDocument($version, $encoding);
            // chargement de la métadonnée
            $entete = "<?xml version=\"" . $version . "\" encoding=\"" . $encoding . "\"?>\n";
            $metadata_data = $entete . $data;
            // $metadata_data = str_replace("&", "&amp;", $metadata_data);
            $metadata_doc->loadXML(($metadata_data));
            $this->delMetadataInService($metadata_doc, $uuidLayer, $layerName);
            
            $new_metadata_data = $metadata_doc->saveXML();
            $new_metadata_data = str_replace($entete, "", $new_metadata_data);
            
            $geonetwork = new GeonetworkInterface(PRO_GEONETWORK_URLBASE, 'srv/fre/');
            $urlUpdateData = "md.edit.save";
            $formData = array (
                "id" => $id,
                "data" => $new_metadata_data
            );
            // send to geosource
            $geonetwork->post($urlUpdateData, $formData);
            
        }
        return "";
            
    }
    
    
    /**
     * création de la métadonnée de service wfs et remplissage automatique de son contenu
     * 
     * @param unknown $tabLayer            
     * @param unknown $layerTitle            
     * @param unknown $uuidLayer            
     * @param unknown $layerName            
     * @param unknown $dao            
     * @param unknown $urlWebservice            
     * @return unknown
     */
    protected function createServiceWFSFiche($layerTitle, $uuidLayer, $layerName, $dao, $uuidWfs) {
        
        $geonetwork = new GeonetworkInterface(PRO_GEONETWORK_URLBASE, 'srv/fre/');

        $metadata_data = "";
        if($uuidWfs==""){
            //default value, gives it to All(Internet)
            $group_id = 1;
            
            $query = "select groupowner from public.metadata where uuid=:uuid";
            $rs = $dao->BuildResultSet($query, array (
                "uuid" => $uuidLayer
            ));
            if ( $rs->GetNbRows() > 0 ) {
                $rs->First();
                $group_id = $rs->Read(0);
            } else {
                throw new \Exception("Impossible de trouver la métadonnée " . $uuidLayer);
            }
            
            //if not groupowner for metadata, take first on wich user has rights to edit
            if($group_id==="" || $group_id===1){    
                $query = "select groupid from public.users left join public.usergroups on usergroups.userid = users.id ".
                    " where usergroups.profile >1 and username=:username limit 1";
                $userSingleton = User::GetUser();
                $rs = $dao->BuildResultSet($query, array (
                    "username" => $userSingleton->getLogin()
                ));
                if ( $rs->GetNbRows() > 0 ) {
                    $rs->First();
                    $group_id = $rs->Read(0);
                }else{
                    $group_id = 1;
                }
            }
            
           
            $jsonResp = $geonetwork->get('md.create?_content_type=json&id=' . PRO_WFS_METADATA_ID . "&group=" . $group_id . "&template=n&child=n&fullPrivileges=false");
            
            $jsonObj = json_decode($jsonResp);
            if ( $jsonObj && $jsonObj->id ) {
                $metadataId = $jsonObj->id;
            } else {
                throw new \Exception("Impossible de créer la métadonnée à partir du modèle de fiche WFS");
            }

            $query = 'SELECT data, uuid, id
                  FROM public.metadata' . ' where id = ?';
            $rs = $dao->BuildResultSet($query, array (
                $metadataId
            ));
        }else{
            $query = 'SELECT data, uuid, id
                  FROM public.metadata' . ' where uuid = ?';
            $rs = $dao->BuildResultSet($query, array (
                $uuidWfs
            ));
        }
        
        if ( $rs->GetNbRows() > 0 ) {
            $rs->First();
            $metadata_data = $rs->ReadHtml(0);
            $uuid = $rs->Read(1);
            $metadataId = $rs->Read(2);
            $urlWebservice = $this->getParameter("PRODIGE_URL_DATACARTO"). "/WFS/" . $uuid."?service=WFS&amp;request=GetCapabilities";
            
        }
        
        $version = "1.0";
        $encoding = "UTF-8";
        $metadata_doc = new \DOMDocument($version, $encoding);
        $entete = "<?xml version=\"" . $version . "\" encoding=\"" . $encoding . "\"?>\n";
        $metadata_data = $entete . $metadata_data;
        if ( @$metadata_doc->loadXML($metadata_data) !== false ) {

            if($uuidWfs==""){
                // First remove coupledResource and Operateson, ContainsOperations,DistributionInfo
                $xpath = new \DOMXPath($metadata_doc);
                $objXPath = @$xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/srv:coupledResource");
                if ( $objXPath->length > 0 ) {
                    foreach($objXPath as $item){
                        $item->parentNode->removeChild($item);
                    }
                }
                $objXPath = @$xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/srv:operatesOn");
                if ( $objXPath->length > 0 ) {
                    foreach($objXPath as $item){
                        $item->parentNode->removeChild($item);
                    }
                }
                $objXPath = @$xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/srv:containsOperations");
                if ( $objXPath->length > 0 ) {
                    foreach($objXPath as $item){
                        $item->parentNode->removeChild($item);
                    }
                }
                $objXPath = @$xpath->query("/gmd:MD_Metadata/gmd:distributionInfo/gmd:MD_Distribution/gmd:transferOptions/gmd:MD_DigitalTransferOptions/gmd:onLine");
                if ( $objXPath->length > 0 ) {
                    foreach($objXPath as $item){
                        $item->parentNode->removeChild($item);
                    }
                }
                
                // replace title
                $objXPath = @$xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/gmd:citation/gmd:CI_Citation/gmd:title/gco:CharacterString");
                if ( $objXPath->length > 0 ) {
                    $element = $objXPath->item(0);
                    $newelement = $metadata_doc->createElement('gco:CharacterString', $layerTitle . " (service WFS)");
                    $element->parentNode->replaceChild($newelement, $element);
                }
                // TODO replace abstract
            }                
            // add coupledresource, operatesOn
            $strXml = '<tag xmlns:srv="http://www.isotc211.org/2005/srv" xmlns:gco="http://www.isotc211.org/2005/gco">';
            $strXmlOp = "<tag xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:srv=\"http://www.isotc211.org/2005/srv\">";
            
            $strXml .= '<srv:coupledResource>
            <srv:SV_CoupledResource>
                <srv:operationName>
                    <gco:CharacterString>GetCapabilities</gco:CharacterString>
                </srv:operationName>
                <srv:identifier>
                    <gco:CharacterString>' . $uuidLayer . '</gco:CharacterString>
                </srv:identifier>
                <gco:ScopedName>' . $layerName . '</gco:ScopedName>
            </srv:SV_CoupledResource>
            </srv:coupledResource>
            <srv:coupledResource>
                <srv:SV_CoupledResource>
                    <srv:operationName>
                        <gco:CharacterString>DescribeFeatureType</gco:CharacterString>
                    </srv:operationName>
                    <srv:identifier>
                        <gco:CharacterString>' . $uuidLayer . '</gco:CharacterString>
                </srv:identifier>
                <gco:ScopedName>' . $layerName . '</gco:ScopedName>
                </srv:SV_CoupledResource>
            </srv:coupledResource>
            <srv:coupledResource>
                <srv:SV_CoupledResource>
                    <srv:operationName>
                        <gco:CharacterString>GetFeature</gco:CharacterString>
                    </srv:operationName>
                    <srv:identifier>
                        <gco:CharacterString>' . $uuidLayer . '</gco:CharacterString>
                </srv:identifier>
                <gco:ScopedName>' . $layerName . '</gco:ScopedName>
                </srv:SV_CoupledResource>
            </srv:coupledResource>';
            $strXmlOp .= '<srv:operatesOn uuidref="' . $uuidLayer . '" xlink:href="' . PRO_GEONETWORK_URLBASE . '/srv/' . $uuidLayer . '" />';
            
            $strXmlOp .= "</tag>";
            $strXml .= "</tag>";
            
            $tagRepport = new \DOMDocument();
            $tagRepport->loadXML($strXml);
            $nodeToImport = $tagRepport->getElementsByTagName("coupledResource" )->item(0);
            $coupledResource = $metadata_doc->getElementsByTagName("SV_ServiceIdentification");
            if ( $coupledResource && $coupledResource->length > 0 ) {
                $newNodeDesc = $metadata_doc->importNode($nodeToImport, true);
                $coupledResource->item(0 )->appendChild($newNodeDesc);
            }
            
            $tagRepport = new \DOMDocument();
            $tagRepport->loadXML($strXmlOp);
            $nodes = $tagRepport->getElementsByTagName("operatesOn");
            $erviceIdentification = $metadata_doc->getElementsByTagName("SV_ServiceIdentification");
            for($c = 0; $c < $nodes->length; $c ++) {
                $nodeToImport = $nodes->item($c);
                if ( $erviceIdentification && $erviceIdentification->length > 0 ) {
                    $newNodeDesc = $metadata_doc->importNode($nodeToImport, true);
                    $erviceIdentification->item(0 )->appendChild($newNodeDesc);
                }
            }
            
            if($uuidWfs==""){
                // add containsOperations
                
                $strXml = '<test xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns:srv="http://www.isotc211.org/2005/srv" xmlns:gco="http://www.isotc211.org/2005/gco">
                 <srv:containsOperations>
                  <srv:SV_OperationMetadata>
                    <srv:operationName>
                      <gco:CharacterString>GetCapabilities</gco:CharacterString>
                    </srv:operationName>
                    <srv:DCP>
                      <srv:DCPList codeList="http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#DCPList" codeListValue="WebServices"/>
                    </srv:DCP>
                    <srv:connectPoint>
                      <gmd:CI_OnlineResource>
                        <gmd:linkage>
                          <gmd:URL>' . $urlWebservice . '</gmd:URL>
                        </gmd:linkage>
                        <gmd:protocol>
                          <gco:CharacterString>OGC:WFS</gco:CharacterString>
                        </gmd:protocol>
                      </gmd:CI_OnlineResource>
                    </srv:connectPoint>
                  </srv:SV_OperationMetadata>
                </srv:containsOperations>
                <srv:containsOperations>
                  <srv:SV_OperationMetadata>
                    <srv:operationName>
                      <gco:CharacterString>DescribeFeatureType</gco:CharacterString>
                    </srv:operationName>
                    <srv:DCP>
                      <srv:DCPList codeList="http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#DCPList" codeListValue="WebServices"/>
                    </srv:DCP>
                    <srv:connectPoint>
                      <gmd:CI_OnlineResource>
                        <gmd:linkage>
                          <gmd:URL>' . $urlWebservice . '</gmd:URL>
                        </gmd:linkage>
                        <gmd:protocol>
                          <gco:CharacterString>OGC:WFS</gco:CharacterString>
                        </gmd:protocol>
                      </gmd:CI_OnlineResource>
                    </srv:connectPoint>
                  </srv:SV_OperationMetadata>
                </srv:containsOperations>
                <srv:containsOperations>
                  <srv:SV_OperationMetadata>
                    <srv:operationName>
                      <gco:CharacterString>GetFeature</gco:CharacterString>
                    </srv:operationName>
                    <srv:DCP>
                      <srv:DCPList codeList="http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#DCPList" codeListValue="WebServices"/>
                    </srv:DCP>
                    <srv:connectPoint>
                      <gmd:CI_OnlineResource>
                        <gmd:linkage>
                          <gmd:URL>' . $urlWebservice . '</gmd:URL>
                        </gmd:linkage>
                        <gmd:protocol>
                          <gco:CharacterString>OGC:WFS</gco:CharacterString>
                        </gmd:protocol>
                      </gmd:CI_OnlineResource>
                    </srv:connectPoint>
                  </srv:SV_OperationMetadata>
                </srv:containsOperations></test>';
                
                $tagRepport = new \DOMDocument();
                $tagRepport->loadXML($strXml);
                
                // création des tags containsOperations nécessaire (WFS protocole)
                $coupledResource = $metadata_doc->getElementsByTagName("SV_ServiceIdentification");
                if ( $coupledResource && $coupledResource->length > 0 ) {
                    $nodes = $tagRepport->getElementsByTagName("containsOperations");
                    for($c = 0; $c < $nodes->length; $c ++) {
                        $nodeToImport = $nodes->item($c);
                        $newNodeDesc = $metadata_doc->importNode($nodeToImport, true);
                        $coupledResource->item(0 )->appendChild($newNodeDesc);
                    }
                }
                
                $strXmlDistributionInfo = "<tag  xmlns:gmd=\"http://www.isotc211.org/2005/gmd\" xmlns:gco=\"http://www.isotc211.org/2005/gco\">
                <gmd:onLine>
                    <gmd:CI_OnlineResource>
                        <gmd:linkage>
                            <gmd:URL>" . $urlWebservice . "</gmd:URL>
                        </gmd:linkage>
                        <gmd:protocol>
                            <gco:CharacterString>OGC:WFS-1.0.0-http-get-capabilities</gco:CharacterString>
                        </gmd:protocol>
                    </gmd:CI_OnlineResource>
                </gmd:onLine></tag>";
                
                $tagDistInfo = new \DOMDocument();
                $tagDistInfo->loadXML($strXmlDistributionInfo);
                
                $transferOptions = $metadata_doc->getElementsByTagName("MD_DigitalTransferOptions");
                if ( $transferOptions && $transferOptions->length > 0 ) {
                    $nodes = $tagDistInfo->getElementsByTagName("onLine");
                    for($c = 0; $c < $nodes->length; $c ++) {
                        $nodeToImport = $nodes->item($c);
                        $newNodeDesc = $metadata_doc->importNode($nodeToImport, true);
                        $transferOptions->item(0 )->appendChild($newNodeDesc);
                    }
                }
            }
        }
        
        $new_metadata_data = $metadata_doc->saveXML();
        $new_metadata_data = str_replace($entete, "", $new_metadata_data);
        
        $geonetwork = new GeonetworkInterface(PRO_GEONETWORK_URLBASE, 'srv/fre/');
        $urlUpdateData = "md.edit.save";
        $formData = array (
                "id" => $metadataId,
                "data" => $new_metadata_data 
        );
        // send to geosource
        $geonetwork->post($urlUpdateData, $formData);
        
        if($uuidWfs==""){
            // publish metadata
            $jsonResp = $geonetwork->get('md.privileges.update?_content_type=json&_1_0=on&id=' . $metadataId);
            
            $jsonObj = json_decode($jsonResp);
            if ( $jsonObj && $jsonObj [0] ) {
                return $uuid;
            } else {
                throw new \Exception("Impossible de publier la métadonnée WFS");
            }
        }else{
             return $uuid;
        }
    }
    
    /**
     * Création du tage distributionInfo et passage de l'url de service WMS/WFS
     * 
     * @param $doc
     * @param $serviceUrl
     * @param $serviceProtocol
     * @param $layerName nom du layer dans le service web
     * @return unknown_type
     */
    protected function assignDistributionInfo(&$doc, $serviceUrl, $serviceProtocol, $layerName) {
        $xpath = new \DOMXPath($doc);
        $distributionInfo = $xpath->query("/gmd:MD_Metadata/gmd:distributionInfo");
        if ( $distributionInfo->length > 0 ) {
            $transferOptions = $xpath->query("/gmd:MD_Metadata/gmd:distributionInfo/gmd:MD_Distribution/gmd:transferOptions/gmd:MD_DigitalTransferOptions");
            if ( $transferOptions->length > 0 ) {
                $url = $transferOptions->item(0 )->getElementsByTagName("URL");
                foreach($url as $item){
                    if ( $item && $item->nodeValue == $serviceUrl ) {
                        
                        $names = $xpath->query("gmd:name/gco:CharacterString", $item->parentNode->parentNode);
                        if ( $names->length > 0 ) {
                            $name = $names->item(0)->nodeValue;
                            //(name,URL) is already declared
                            if($name==$layerName){
                                return true;
                            }
                            
                        }
                       
                    }
                }
                // mode création
                $strXML = '<gmd:MD_Metadata xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns="http://www.isotc211.org/2005/gmd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:fra="http://www.cnig.gouv.fr/2005/fra" xmlns:gco="http://www.isotc211.org/2005/gco" xmlns:gts="http://www.isotc211.org/2005/gts" xmlns:gml="http://www.opengis.net/gml" xmlns:gfc="http://www.isotc211.org/2005/gfc" xmlns:gmx="http://www.isotc211.org/2005/gmx" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:gmi="http://www.isotc211.org/2005/gmi">
                            <gmd:onLine>
                              <gmd:CI_OnlineResource>
                                <gmd:linkage>
                                  <gmd:URL>' . $serviceUrl . '</gmd:URL>
                                </gmd:linkage>
                                <gmd:protocol>
                                  <gco:CharacterString>' . $serviceProtocol . '</gco:CharacterString>
                                </gmd:protocol>
                                <gmd:name>
                                  <gco:CharacterString>' . $layerName . '</gco:CharacterString>
                                </gmd:name>
                              </gmd:CI_OnlineResource>
                            </gmd:onLine>
                           </gmd:MD_Metadata>';
                
                $orgDoc = new \DOMDocument();
                $orgDoc->loadXML($strXML);
                $nodeToImport = $orgDoc->getElementsByTagName("onLine" )->item(0);
                $newNodeDesc = $doc->importNode($nodeToImport, true);
                $transferOptions->item(0 )->appendChild($newNodeDesc);
            } else {
                //$this->write_log("ECHEC : le tag distributionInfo est mal construit");
            }
        } else {
            // mode création
            $racine = $doc->getElementsByTagName("MD_Metadata");
            if ( $racine ) {
                $strXML = '<gmd:MD_Metadata xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns="http://www.isotc211.org/2005/gmd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:fra="http://www.cnig.gouv.fr/2005/fra" xmlns:gco="http://www.isotc211.org/2005/gco" xmlns:gts="http://www.isotc211.org/2005/gts" xmlns:gml="http://www.opengis.net/gml" xmlns:gfc="http://www.isotc211.org/2005/gfc" xmlns:gmx="http://www.isotc211.org/2005/gmx" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:gmi="http://www.isotc211.org/2005/gmi">
                <gmd:distributionInfo xmlns:srv="http://www.isotc211.org/2005/srv" xmlns:date="http://exslt.org/dates-and-times">
                <gmd:MD_Distribution>
                <gmd:distributionFormat>
                  <gmd:MD_Format>
                    <gmd:name>
                      <gco:CharacterString>ZIP</gco:CharacterString>
                    </gmd:name>
                    <gmd:version gco:nilReason="missing">
                      <gco:CharacterString/>
                    </gmd:version>
                  </gmd:MD_Format>
                </gmd:distributionFormat>
                <gmd:transferOptions>
                  <gmd:MD_DigitalTransferOptions>
                    <gmd:onLine>
                      <gmd:CI_OnlineResource>
                        <gmd:linkage>
                          <gmd:URL>' . $serviceUrl . '</gmd:URL>
                        </gmd:linkage>
                        <gmd:protocol>
                          <gco:CharacterString>' . $serviceProtocol . '</gco:CharacterString>
                        </gmd:protocol>
                        <gmd:name>
                          <gco:CharacterString>' . $layerName . '</gco:CharacterString>
                        </gmd:name>
                      </gmd:CI_OnlineResource>
                    </gmd:onLine>
                  </gmd:MD_DigitalTransferOptions>
                </gmd:transferOptions>
                </gmd:MD_Distribution>
                </gmd:distributionInfo></gmd:MD_Metadata>';
                
                $orgDoc = new \DOMDocument();
                $orgDoc->loadXML($strXML);
                $nodeToImport = $orgDoc->getElementsByTagName("distributionInfo" )->item(0);
                $newNodeDesc = $doc->importNode($nodeToImport, true);
                $dataQualityInfo = $racine->item(0 )->getElementsByTagName("dataQualityInfo");
                if ( $dataQualityInfo && $dataQualityInfo->length > 0 ) {
                    $racine->item(0 )->insertBefore($newNodeDesc, $dataQualityInfo->item(0));
                } else {
                    $racine->item(0 )->appendChild($newNodeDesc);
                }
            }
        }
    }
    
    /**
     * ajoute les références à la donnée dans la métadonnée de service
     * 
     * @param $doc
     * @param $layerUuid
     * @param $layerName
     * @return unknown_type
     */
    protected function addMetadataInService(&$doc, $layerUuid, $layerName) {
        $xpath = new \DOMXPath($doc);
        $srvIdentificationInfo = $xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification");
        
        if ( $srvIdentificationInfo->length > 0 ) {
            $srvIdentifier = $xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/srv:coupledResource/srv:SV_CoupledResource/srv:identifier/gco:CharacterString");
            foreach($srvIdentifier as $item){
                $ScopedNames = $item->parentNode->parentNode->getElementsByTagName("ScopedName");
                if ($ScopedNames && $ScopedNames->length > 0 ) {
                    $ScopedName = $ScopedNames->item(0)->nodeValue;
                    if ( $item->nodeValue == $layerUuid && $ScopedName == $layerName) {
                        // mode modification, suppression puis recréation
                        $item->parentNode->parentNode->parentNode->parentNode->removeChild($item->parentNode->parentNode->parentNode);
                    }
                }
            }
            $strXML = "<gmd:MD_Metadata xmlns:gmd=\"http://www.isotc211.org/2005/gmd\" xmlns:gfc=\"http://www.isotc211.org/2005/gfc\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:gco=\"http://www.isotc211.org/2005/gco\" xmlns:gml=\"http://www.opengis.net/gml\" xmlns:srv=\"http://www.isotc211.org/2005/srv\" xmlns:gts=\"http://www.isotc211.org/2005/gts\" xmlns:gmx=\"http://www.isotc211.org/2005/gmx\" xmlns:geonet=\"http://www.fao.org/geonetwork\"><srv:coupledResource>
              <srv:SV_CoupledResource>
                <srv:operationName>
                  <gco:CharacterString>GETMAP</gco:CharacterString>
                </srv:operationName>
                <srv:identifier>
                  <gco:CharacterString>" . $layerUuid . "</gco:CharacterString>
                </srv:identifier>
                <gco:ScopedName>" . $layerName . "</gco:ScopedName>
              </srv:SV_CoupledResource>
            </srv:coupledResource></gmd:MD_Metadata>";
            
            $orgDoc = new \DOMDocument();
            $orgDoc->loadXML($strXML);
            $nodeToImport = $orgDoc->getElementsByTagName("coupledResource" )->item(0);
            $newNodeDesc = $doc->importNode($nodeToImport, true);
            $srvIdentificationInfo->item(0 )->appendChild($newNodeDesc);
            
            $srvOperatesOn = $xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/srv:operatesOn");
            if ( $srvOperatesOn->length > 0 ) {
                foreach($srvOperatesOn as $item){
                    $layerIdentifier = $item->getAttribute("uuidref");
                    if ( $layerIdentifier == $layerUuid ) {
                        return true; // le lien existe déjà
                    }
                }
            }
            $operatesOnElt = $doc->createElement("srv:operatesOn");
            $operatesOnElt->setAttribute("uuidref", $layerUuid);
            $srvIdentificationInfo->item(0 )->appendChild($operatesOnElt);
        }
    }
    
    /**
     * supprime les références à la donnée dans la métadonnée de service
     * 
     * @param $doc
     * @param $layerUuid
     * @param $layerName
     * @return unknown_type
     */
    protected function delMetadataInService(&$doc, $layerUuid, $layerName) {
        $xpath = new \DOMXPath($doc);
        $bHasOtherLayerForUuid = false;
        $srvIdentificationInfo = $xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/srv:coupledResource/srv:SV_CoupledResource/srv:identifier/gco:CharacterString");
        if ( $srvIdentificationInfo->length > 0 ) {
            foreach($srvIdentificationInfo as $item){
                $ScopedNames = $item->parentNode->parentNode->getElementsByTagName("ScopedName");
                if ($ScopedNames && $ScopedNames->length > 0 ) {
                    $ScopedName = $ScopedNames->item(0)->nodeValue;
                    if ( $item->nodeValue == $layerUuid) {
                        if($ScopedName == $layerName){
                            $item->parentNode->parentNode->parentNode->parentNode->removeChild($item->parentNode->parentNode->parentNode);
                        }else{
                            $bHasOtherLayerForUuid=true;
                        }
                    }
                }
            }
        }
        
        if(!$bHasOtherLayerForUuid){
            $srvOperatesOn = $xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/srv:operatesOn");
            if ( $srvOperatesOn->length > 0 ) {
                foreach($srvOperatesOn as $item){
                    $layerIdentifier = $item->getAttribute("uuidref");
                    if ( $layerIdentifier == $layerUuid ) {
                        $item->parentNode->removeChild($item);
                    }
                }
            }
        }
    }
    
    /**
     * brief détermine si la couche est déjà dans le service
     * 
     * @param oMap objet Mapserver
     * @param data nom de la couche
     * @param type stockage 0 raster 1 Postgis
     * @return vrai si la couche est dans le service, faux sinon
     */
    protected function isLayerInService($oMap, $data, $type_stokage) {
        for($i = 0; $i < $oMap->numlayers; $i ++) {
            $oLayer = $oMap->getLayer($i);
            switch ($type_stokage) {
                case 0 :
                    // looking for dta filename in layer data or layer tileindex in case of tiled layer
                    if ( !(strrpos($oLayer->data, $data) === FALSE) || !(strrpos($oLayer->tileindex, $data) === FALSE) )
                        return true;
                    break;
                case 1 :
                case - 4 : // vues
                    if ( strpos($oLayer->data, $data . " as foo using unique gid") !== false || (strpos($oLayer->data, $data . ") as foo using unique gid") !== false) )
                        return true;
                    break;
            }
        }
        return false;
    }
    
    /**
     * brief supprime une couche d'un service
     * 
     * @param oMap objet Mapserver
     * @param data nom de la couche
     * @param type stockage 0 raster 1 Postgis
     */
    protected function DelLayerFromService($oMap, $data, $type_stokage) {
        for($i = 0; $i < $oMap->numlayers; $i ++) {
            $oLayer = $oMap->getLayer($i);
            switch ($type_stokage) {
                case 0 :
                    if ( !(strrpos($oLayer->data, $data) === FALSE) || !(strrpos($oLayer->tileindex, $data) === FALSE) ) {
                        $oLayer->set("status", MS_DELETE);
                        return $oLayer->name;
                    }
                    break;
                case 1 :
                case - 4 : // vues
                    if ( strpos($oLayer->data, $data . " as foo using unique gid") !== false || (strpos($oLayer->data, $data . ") as foo using unique gid") !== false) ){
                        $oLayer->set("status", MS_DELETE);
                        return $oLayer->name;
                    }
                    break;
            }
        }
    }
    
    /**
     *
     * @param unknown $wfsFile            
     * @param unknown $LayerAddParams            
     * @param unknown $tabMetadata            
     * @param unknown $serverUrl            
     */
    protected function createWfsMapfile($wfsFile, $LayerAddParams, $tabMetadata, $serverUrl, $metadataUrl) {
        $params = explode("|", ($LayerAddParams));
        $bExists = false;
        if(file_exists($wfsFile)){
            $oMap = ms_newMapObj($wfsFile);
            $bExists = true;
        }else{
            $oMap = ms_newMapObj(PRO_MAPFILE_PATH . "template.map");
        }
        
        if ( isset($oMap)&& !empty($oMap) ) {
            
            if(!$bExists){
                $oMap->setMetadata("wfs_onlineresource", $serverUrl);
                $oMap->setMetaData("wfs_title", (stripslashes($params [1])));
                $oMap->setMetaData("wfs_abstract", (stripslashes($tabMetadata [0])));
                $oMap->setMetaData("wfs_keywordslist", (stripslashes($tabMetadata [1])));
                $oMap->setMetadata("wfs_srs", $tabMetadata [2]); // # Recommended
                $oMap->setMetadata("wfs_enable_request", "*"); // necessary
                
                $oMap->setProjection("init=epsg:" . $params [5]);
            }
            
            $p_oLayer = ms_newLayerObj($oMap);
            
            $p_oLayer->set("type", $params [2]);
            $p_oLayer->setConnectionType(MS_POSTGIS);
            $p_oLayer->set("connection", stripslashes($params [3]));
            $p_oLayer->set("data", $params [4]);
            $p_oLayer->set("status", MS_ON);
            $p_oLayer->set("dump", MS_TRUE);
            $p_oLayer->set("template", "consultable");
            $p_oLayer->set("opacity", 50);
            $p_oClass = ms_newClassObj($p_oLayer);
            $p_oClass->set("name", str_replace(" ", "", stripslashes($params [1])));
            $p_oStyle = ms_newStyleObj($p_oClass);
            switch ($p_oLayer->type) {
                case MS_LAYER_POINT :
                    $p_oStyle->set("size", "5");
                    $p_oStyle->set("angle", "360");
                    $p_oStyle->color->setRGB(255, 0, 0);
                    $p_oStyle->set("symbolname", "Cercle");
                    break;
                case MS_LAYER_LINE :
                    $p_oStyle->set("size", "2");
                    $p_oStyle->set("angle", "360");
                    $p_oStyle->outlinecolor->setRGB(75, 75, 75);
                    $p_oStyle->set("symbolname", "Continue");
                    break;
                default :
                    $p_oStyle->set("size", "10");
                    $p_oStyle->set("angle", "360");
                    $p_oStyle->outlinecolor->setRGB(75, 75, 75);
                    $p_oStyle->color->setRGB(128, 128, 128);
                    break;
            }
            
            $p_oLayer->set("name", $params [6]);
            $p_oLayer->setProjection("init=epsg:" . $params [5]);
            $p_oLayer->setMetaData("wfs_srs", ($tabMetadata [2]));
            
            $p_oLayer->setMetaData("wfs_title", (stripslashes($params [1])));
            $p_oLayer->setMetaData("wfs_abstract", (stripslashes($tabMetadata [0])));
            $p_oLayer->setMetaData("wfs_keywordslist", (stripslashes($tabMetadata [1])));
            
            // $p_oLayer->setMetaData($action."_extent", ($tabMetadata[3]));
            if ( isset($tabMetadata [3]) )
                $p_oLayer->setMetaData("gml_featureid", ($tabMetadata [3]));
            if ( isset($tabMetadata [4]) ) {
                $p_oLayer->setMetaData("gml_include_items", ($tabMetadata [4]));
                // for text/plain format
                $p_oLayer->setMetaData("wms_include_items", ($tabMetadata [4]));
            }
            $p_oLayer->setMetaData("wfs_metadataurl_format", "text/html");
            $p_oLayer->setMetaData("wfs_metadataurl_href", ($metadataUrl));
            $p_oLayer->setMetaData("wfs_metadataurl_type", "");
            
            $p_oLayer->setMetaData("gml_types", "auto");
            
            $oMap->save($wfsFile);
        }
    }
    /**
     * brief ajoute une couche dans un mapfile
     * 
     * @param oMap objet Mapfile
     * @param MapPath chemin du mapfile
     * @param paramètres de la couche ajoutée
     * @param tabMetadata tableau des métadonnées du service
     * @param action wfs ou wms
     */
    protected function AddLayerToMap($oMap, $MapPath, $LayerAddParams, $tabMetadata, $action, $metadataUrl) {
        // global $PRO_MAPFILE_PATH;
        // global $urlBack;
        $bResult = null;
        
        if ( isset($oMap)&& !empty($oMap) ) {
            if ( !is_null($LayerAddParams) ) {
                $params = explode("|", ($LayerAddParams));
                
                if ( count($params)> 0 ) {
                    switch ($params [0]) {
                        case "RASTER_PRODIGE" :
                            if ( count($params) == 5 ) {
                                
                                $last_pos = strrpos($params [3], "/");
                                $layer_name = substr($params [3], $last_pos + 1);
                                $tablayer_name = explode(".", $layer_name);
                                $layer_name = $tablayer_name [0];
                                if ( $layer_name != "" ) {
                                    $layer_wms_mapfile = PRO_MAPFILE_PATH . "/layers/WMS/" . $layer_name . ".map";
                                    $layer_mapfile = PRO_MAPFILE_PATH . "/layers/" . $layer_name . ".map";
                                    
                                    /*
                                     * $layer_title = preg_replace("!(\W)!", " ", $layer_title);//for other chars
                                     * $layer_title = removeAccents(str_replace(" ", "",stripslashes(($params[1]))));
                                     * $layer_title = neutralisationChaine($layer_title);
                                     * $layer_title = preg_replace("/[^a-zA-Z0-9_]/", "", $layer_title);
                                     * $layer_title = substr($layer_title, 0, 30);
                                     */
                                    $layer_title = $params [4];
                                    
                                    // if layer already has a symbology for WMS, take it from layers/WMS/layer_name.map
                                    if ( file_exists($layer_wms_mapfile) ) {
                                        $oLayerMap = ms_newMapObj($layer_wms_mapfile);
                                        $p_oLayer_temp = @$oLayerMap->getLayer($oLayerMap->numlayers - 1); // take last layer
                                        $p_oLayer = ms_newLayerObj($oMap, $p_oLayer_temp);
                                        $p_oLayer->set("dump", MS_TRUE);
                                    }                                    // else if has a symbology for prodige, take it from layers/layer_name.map
                                    elseif ( file_exists($layer_mapfile) ) {
                                        $oLayerMap = ms_newMapObj($layer_mapfile);
                                        $p_oLayer_temp = @$oLayerMap->getLayer($oLayerMap->numlayers - 1); // take last layer
                                        $p_oLayer = ms_newLayerObj($oMap, $p_oLayer_temp);
                                        $p_oLayer->set("dump", MS_TRUE);
                                    } else { // else : default symbology
                                        $p_oLayer = ms_newLayerObj($oMap);
                                        $p_oLayer->setMetaData("CLASS_LEGENDE", "ON");
                                        $p_oLayer->setMetaData("ECHELLE_LEGENDE", "ON");
                                        // $p_oLayer->setProjection($tabMetadata[2]);
                                        if ( $params [2] == "MS_LAYER_TILERASTER" ) {
                                            $params [2] = "MS_LAYER_RASTER";
                                            $p_oLayer->set("tileindex", PRO_MAPFILE_PATH . "/" . stripslashes($params [3]));
                                            $p_oLayer->set("tileitem", "location");
                                        } else
                                            $p_oLayer->set("data", PRO_MAPFILE_PATH . "Publication/" . stripslashes($params [3]));
                                        
                                        eval("\$params[2] = " . $params [2] . ";");
                                        $p_oLayer->set("type", $params [2]);
                                        $p_oLayer->set("status", MS_ON);
                                        $p_oLayer->set("dump", MS_TRUE);
                                        /*
                                         * if ( ctype_digit($params[5]) )
                                         * $p_oLayer->setMetaData("GEONETWORK_METADATA_ID", stripslashes($params[5]));
                                         */
                                    }
                                    if ( $p_oLayer ) {
                                        $p_oLayer->set("name", $layer_title);
                                        $p_oLayer->setMetaData("wms_srs", $tabMetadata [2]);
                                        $p_oLayer->setMetaData("wms_title", stripslashes($params [1]));
                                        $p_oLayer->setMetaData("wms_abstract", $tabMetadata [0]);
                                        $p_oLayer->setMetaData("wms_keywordslist", $tabMetadata [1]);
                                        
                                        // Ajout d'une classe pour prise en compte de la légende WMS
                                        $p_oClass = ms_newClassObj($p_oLayer);
                                        $p_oClass->set("name", str_replace(" ", "", $layer_title));
                                        if ( $action == "wms" )
                                            $p_oLayer->setMetaData($action . "_layer_group", ("/" . stripslashes($tabMetadata [5])));
                                    }
                                } else { // error
                                    echo $strHtml = "       <div class=\"errormsg\">Une erreur s'est produite, veuillez réessayer.<br>" . "       </div>
                                                        </body>
                                                    </html>";
                                    // die();
                                    exit();
                                }
                            }
                            break;
                        
                        case "VECTOR_PRODIGE" :
                            
                            if ( count($params) == 7 ) {
                                $tabmatch_select = array();
                                $tabmatch = array();
                                preg_match("!from\s+\(*([\w\s\.\*]*)\)*\s+as!", $params [4], $tabmatch_select);
                                $layer_table = $tabmatch_select [1];
                                
                                preg_match("!from\s+([\w\s\.\*]*)!", $layer_table, $tabmatch);
                                $layer_table = $tabmatch [1];
                                
                                if ( $layer_table != "" ) {
                                    $layer_wms_mapfile = PRO_MAPFILE_PATH . "/layers/WMS/" . $layer_table . ".map";
                                    $layer_mapfile = PRO_MAPFILE_PATH . "/layers/" . $layer_table . ".map";
                                    // $layer_name = preg_replace("!(\W)!", " ", $layer_name);//for other chars
                                    /*
                                     * $layer_name = removeAccents(str_replace(" ", "",stripslashes(($params[1]))));
                                     * $layer_name = neutralisationChaine($layer_name);
                                     * $layer_name = preg_replace("/[^a-zA-Z0-9_]/", "", $layer_name);
                                     * $layer_name = substr($layer_name, 0, 30);
                                     */
                                    $layer_name = $params [6];
                                    // if layer already has a symbology, take it from layers/WMS/layer_name.map
                                    if ( file_exists($layer_wms_mapfile) ) {
                                        $oLayerMap = ms_newMapObj($layer_wms_mapfile);
                                        // take layer witch data is matching
                                        for($i = 0; $i < $oLayerMap->numlayers; $i ++) {
                                            $oLayer = $oLayerMap->getLayer($i);
                                            preg_match("!from\s+\(*([\w\s\.\*]*)\)*\s+as!", $oLayer->data, $tabmatch_select);
                                            $layerWMS_table = $tabmatch_select [1];
                                            
                                            preg_match("!from\s+([\w\s\.\*]*)!", $layerWMS_table, $tabmatch);
                                            $layerWMS_table = $tabmatch [1];
                                            if ( $layerWMS_table == $layer_table || $layerWMS_table == "public." . $layer_table ) {
                                                $p_oLayer = ms_newLayerObj($oMap, $oLayer); // add the layer to the map object
                                                $p_oLayer->set("dump", MS_TRUE);
                                                $p_oLayer->set("template", "consultable");
                                                break;
                                            }
                                        }
                                    }                                     /*
                                     * //else if has a symbology for prodige, take it from layers/layer_name.map
                                     * else if(file_exists($layer_mapfile)) {
                                     * $oLayerMap = ms_newMapObj($layer_mapfile);
                                     * //take layer witch data is matching
                                     * for ($i = 0; $i < $oLayerMap->numlayers; $i++){
                                     * $oLayer = $oLayerMap->getLayer($i);
                                     * preg_match("!from\s+\(*([\w\s\.\*]*)\)*\s+as!", $oLayer->data, $tabmatch_select);
                                     * $layerWMS_table=$tabmatch_select[1];
                                     *
                                     * preg_match("!from\s+([\w\s\.\*]*)!", $layerWMS_table, $tabmatch);
                                     * $layerWMS_table=$tabmatch[1];
                                     * if($layerWMS_table == $layer_table){
                                     * $p_oLayer_temp=@$oLayerMap->getLayer($oLayerMap->numlayers - 1); //take last layer
                                     * $p_oLayer = ms_newLayerObj($oMap,$p_oLayer_temp);
                                     * $p_oLayer->set("dump", MS_TRUE);
                                     * $p_oLayer->set("template", "consultable");
                                     * break;
                                     * }
                                     * }
                                     * }
                                     */
                                    if(!isset($p_oLayer)) { // else : default symbology
                                        $p_oLayer = ms_newLayerObj($oMap);
                                        $p_oLayer->setMetaData("CLASS_LEGENDE", "ON");
                                        $p_oLayer->setMetaData("ECHELLE_LEGENDE", "ON");
                                        
                                        $p_oLayer->set("name", $layer_name);
                                        // $p_oLayer->setProjection("init=".$tabMetadata[2]);
                                        eval("\$params[2] = " . $params [2] . ";");
                                        $p_oLayer->set("type", $params [2]);
                                        $p_oLayer->setConnectionType(MS_POSTGIS);
                                        $p_oLayer->set("connection", stripslashes($params [3]));
                                        $p_oLayer->set("data", $params [4]);
                                        $p_oLayer->set("status", MS_ON);
                                        $p_oLayer->set("dump", MS_TRUE);
                                        $p_oLayer->set("template", "consultable");
                                        $p_oLayer->set("opacity", 50);
                                        /*
                                         * if ( ctype_digit($params[5]) )
                                         * $p_oLayer->setMetaData("GEONETWORK_METADATA_ID", stripslashes($params[5]));
                                         */
                                        $p_oClass = ms_newClassObj($p_oLayer);
                                        $p_oClass->set("name", str_replace(" ", "", stripslashes($params [1])));
                                        $p_oStyle = ms_newStyleObj($p_oClass);
                                        switch ($p_oLayer->type) {
                                            case MS_LAYER_POINT :
                                                $p_oStyle->set("size", "5");
                                                $p_oStyle->set("angle", "360");
                                                $p_oStyle->color->setRGB(255, 0, 0);
                                                $p_oStyle->set("symbolname", "Cercle");
                                                break;
                                            case MS_LAYER_LINE :
                                                $p_oStyle->set("size", "2");
                                                $p_oStyle->set("angle", "360");
                                                $p_oStyle->outlinecolor->setRGB(75, 75, 75);
                                                $p_oStyle->set("symbolname", "Continue");
                                                break;
                                            default :
                                                $p_oStyle->set("size", "10");
                                                $p_oStyle->set("angle", "360");
                                                $p_oStyle->outlinecolor->setRGB(75, 75, 75);
                                                $p_oStyle->color->setRGB(128, 128, 128);
                                                // $p_oStyle->set("symbolname", "Carre");
                                                break;
                                        }
                                    }
                                } else { // error
                                    echo $strHtml = "       <div class=\"errormsg\">Une erreur s'est produite, veuillez réessayer.<br>" . 
                                    "       </div>
                                            </body>
                                            </html>";
                                    // die();
                                    exit();
                                }
                                if ( $p_oLayer ) {
                                    
                                    $p_oLayer->set("name", $layer_name);
                                    $p_oLayer->setProjection("init=epsg:" . $params [5]);
                                    $p_oLayer->setMetaData($action . "_srs", ($tabMetadata [2]));
                                    $p_oLayer->setMetaData($action . "_title", (stripslashes($params [1])));
                                    $p_oLayer->setMetaData($action . "_abstract", (stripslashes($tabMetadata [0])));
                                    $p_oLayer->setMetaData($action . "_keywordslist", (stripslashes($tabMetadata [1])));
                                    // $p_oLayer->setMetaData($action."_extent", ($tabMetadata[3]));
                                    if ( isset($tabMetadata [3]) )
                                        $p_oLayer->setMetaData("gml_featureid", ($tabMetadata [3]));
                                    if ( isset($tabMetadata [4]) ) {
                                        $p_oLayer->setMetaData("gml_include_items", ($tabMetadata [4]));
                                        // for text/plain format
                                        $p_oLayer->setMetaData("wms_include_items", ($tabMetadata [4]));
                                    }
                                    if ( $action == "wms" )
                                        $p_oLayer->setMetaData($action . "_layer_group", ("/" . stripslashes($tabMetadata [5])));
                                    
                                    $p_oLayer->setMetaData($action . "_metadataurl_format", "text/html");
                                    $p_oLayer->setMetaData($action . "_metadataurl_href", ($metadataUrl));
                                    $p_oLayer->setMetaData($action . "_metadataurl_type", "");
                                    if ( $action == "wfs" )
                                        $p_oLayer->setMetaData("gml_types", "auto");
                                }
                            }
                            break;
                    }
                }
            }
            $oMap->save($MapPath);
        }
        return $p_oLayer->name;
    }
    /**
     * brief Récupération des paramètres stockés dans le mapfile
     * 
     * @param $oMap objet Mapscript
     * @param $data nom de la donnée
     * @param $type_stokage 0 raster, 1 vecteur, -4 vues
     * @param $action wms ou wfs
     * @param $tabParam tableau de paramètres initialisés
     * @param $tabSelectFields tableau de champs sélectionnés
     * @return tableau de paramètres
     */
    protected function layerGetParams($oMap, $data, $type_stokage, $action, $tabParam, $tabSelectFields) {
        
        // global $tabSelectFields;
        $tabParam ["gml_include_items"] = $tabSelectFields;
        if ( isset($oMap)&& !empty($oMap) ) {
            $bLayerExist = false;
            for($i = 0; $i < $oMap->numlayers; $i ++) {
                $oLayer = $oMap->getLayer($i);
                switch ($type_stokage) {
                    case 0 :
                        if ( strrpos($oLayer->data, $data) !== false || strrpos($oLayer->tileindex, $data) !== false ) {
                            $bLayerExist = true;
                        }
                        break;
                    case 1 :
                    case - 4 : // vues
                        if ( strpos($oLayer->data, $data . " as foo using unique gid") !== false || (strpos($oLayer->data, $data . ") as foo using unique gid") !== false) )
                            $bLayerExist = true;
                        break;
                }
                if ( $bLayerExist ) {
                    $tabParam ["srs"] = explode(" ", ($oLayer->getMetaData($action . "_srs")));
                    $tabParam ["title"] = ($oLayer->getMetaData($action . "_title"));
                    $tabParam ["layername"] = ($oLayer->name);
                    $tabParam ["abstract"] = ($oLayer->getMetaData($action . "_abstract"));
                    $tabParam ["keywordslist"] = ($oLayer->getMetaData($action . "_keywordslist" ));
                    $tabParam ["gml_featureid"] = ($oLayer->getMetaData("gml_featureid"));
                    $tabParam ["gml_include_items"] = explode(",", ($oLayer->getMetaData("gml_include_items")));
                    $tabParam ["layer_group"] = ($oLayer->getMetaData($action . "_layer_group"));
                    break;
                }
            }
            return $tabParam;
        }
    }
    
    /**
     * brief Récupération les noms de layer du map
     * oMap Objet Mapfile
     */
    protected function getMapLayerNames($oMap) {
        $tabLayername = array();
        if ( isset($oMap)&& !empty($oMap) ) {
            for($i = 0; $i < $oMap->numlayers; $i ++) {
                $oLayer = $oMap->getLayer($i);
                $tabLayername [] = addslashes(($oLayer->name));
            }
        }
        return $tabLayername;
    }
    
    /**
     * brief Supprime les caractères spèciaux
     * 
     * @param $string
     * @return unknown_type
     */
    protected function neutralisationChaine($string) {
        $rslt = stripslashes($string);
        $rslt = str_replace("'", "", $rslt);
        $rslt = str_replace("\"", "", $rslt);
        $rslt = str_replace('(', '', $rslt);
        $rslt = str_replace(')', '', $rslt);
        return $rslt;
    }
    
    /**
     * Retire tous les accents ou lettres accolées de la chaine en maintenant les lettres
     * Retourne la chaine transformée
     * 
     * @param string $str     chaine à traiter
     * @param string $charset encodage de la chaine, =utf-8 par défaut
     * @return string
     */
    protected function removeAccents($str, $charset = 'utf-8') {
        $str = htmlentities($str, ENT_NOQUOTES, $charset);
        
        $str = preg_replace('#&([A-za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
        $str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str); // pour les ligatures e.g. '&oelig;'
        $str = preg_replace('#&[^;]+;#', '', $str); // supprime les autres caractères
        
        return $str;
    }
}
