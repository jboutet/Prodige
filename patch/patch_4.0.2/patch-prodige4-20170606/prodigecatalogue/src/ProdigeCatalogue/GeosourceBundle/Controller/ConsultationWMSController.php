<?php
namespace ProdigeCatalogue\GeosourceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Prodige\ProdigeBundle\Controller\BaseController;
use Prodige\ProdigeBundle\Controller\User;
use Prodige\ProdigeBundle\Common\Util;
use Prodige\ProdigeBundle\DAOProxy\DAO;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Page de consultation des couches présentes sur le serveur wms
 * 
 * @author Alkante
 *        
 */

/**
 * @Route("/geosource")
 */
class ConsultationWMSController extends BaseController
{

    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/consultationWMS", name="catalogue_geosource_consultationWMS", options={"expose"=true})
     */
    public function consultationWMSAction(Request $request)
    {
        
        // if(!isset($AdminPath))
        // $AdminPath = "./Administration/";
        // require_once("include/ClassUser.php");
        $user = User::GetUser();
        $userId = User::GetUserId();
        if (! is_null($userId))
            User::SetUserCookie($userId);
            /*
         * @deprecated prodige4 * if(!$user->PasswordIsValid()) { //header('location:administration_utilisateur.php'); exit(0); }
         */
        
        /*if (! $user->HasTraitement('NAVIGATION')) {
            // header('location:Erreurs/Droits.php');
            throw new \Exception("Vous n'avez pas les droits suffisants pour accéder à cette fonctionnalité.");
        }*/

        // for TMP representation
        $layerMapFile = "TMP_" . uniqid();
        $mapUrl = $this->getParameter('PRODIGE_URL_FRONTCARTO') . "/1/layers/" . $layerMapFile . ".map";
        $couchd_emplacement_stockage="";
        $DATA = array();
        $TITLE = array();
        $METADATAID = array();
        $couche_nom ="";
        $tabTerr = array();
        $TERR = array();
        $couchd_type_stockage = 1;
        
        $m_IdtMetadata = null;
        if ($request->get("IDT", false))
            $m_IdtMetadata = $request->get("IDT");

        $conn = $this->getCatalogueConnection('public');
        
        $dao = new DAO($conn, 'catalogue');
        $query = 'SELECT couchd_emplacement_stockage, couchd_nom, couchd_type_stockage '.
                 ' FROM couche_donnees '.
                 ' LEFT JOIN fiche_metadonnees on fiche_metadonnees.fmeta_fk_couche_donnees  = couche_donnees.pk_couche_donnees ' . 
                 ' where fmeta_id = ?';
        $rs = $dao->BuildResultSet($query, array(
            $m_IdtMetadata
        ));
        
        $indice = 0;
        //1 check rights
        for($rs->First(); !$rs->EOF(); $rs->Next()) {
            $couchd_emplacement_stockage = $rs->Read(0);
            $DATA[] = $couchd_emplacement_stockage;
            $TITLE[] = ($rs->GetNbRows()>1 ? $couchd_emplacement_stockage : (html_entity_decode($rs->read(1), ENT_QUOTES, 'UTF-8')));
            $METADATAID[] = $m_IdtMetadata;
            $couchd_type_stockage = $rs->Read(2);
            /*switch ($couchd_type_stockage) {
                case 0:
                    // raster prodige
                    // if raster, take $couchd_emplacement_stockage and delete extension
                    $layerMapFile = substr($couchd_emplacement_stockage, 0, strrpos($couchd_emplacement_stockage, "."));
                    // get file name
                    $layerMapFile = substr($layerMapFile, strrpos($layerMapFile, "/") + 1, strlen($layerMapFile));
                    break;
                case 1:
                default:
                    // vector prodige
                    $layerMapFile = $couchd_emplacement_stockage;
                    break;
            }*/
            
            // vérification du caractère de visualisation libre de la donnée
            $query = 'SELECT couchd_wfs, couchd_wms FROM couche_donnees WHERE pk_couche_donnees in (select fmeta_fk_couche_donnees from fiche_metadonnees where  FMETA_ID =?)';
            $rsVisu = $dao->BuildResultSet($query, array(
                $m_IdtMetadata
            ));
            $rsVisu->First();
            $bFreeVisu = $rsVisu->Read(0) || $rsVisu->Read(1);
            
            if (! $bFreeVisu) {
                // vérification des droits sur un des domaines de la donnée
                $bAllow = false;
                $query = "SELECT DOM_NOM, SSDOM_NOM, PK_COUCHE_DONNEES  FROM COUCHE_SDOM WHERE FMETA_ID =?";
                $rsRight = $dao->BuildResultSet($query, array(
                    $m_IdtMetadata
                ));
                for ($rsRight->First(); ! $rsRight->EOF(); $rsRight->Next()) {
                    $domaine = $rsRight->read(0);
                    $sous_domaine = $rsRight->read(1);
                    $objetCouche = $rsRight->read(2);
                    // vérification de l'autorisation sur le domaine et sur l'objet
                    $bAllow = $user->HasTraitement('NAVIGATION', html_entity_decode($domaine, ENT_QUOTES, 'UTF-8'), html_entity_decode($sous_domaine, ENT_QUOTES, 'UTF-8'), PRO_TRT_TYPE_CONSULTATION, $objetCouche, PRO_OBJET_TYPE_COUCHE);
                    if ($bAllow)
                        break;
                }
                // vérification des restricitions territoriales
                $tabTerr = $user->GetTraitementTerritoire('NAVIGATION', $objetCouche);
                
                if ($tabTerr === false){//pas d'autorisation sur territoire
                    $bAllow = false;
                }elseif($tabTerr=== true){//pas de restriction territoriale
                    //do nothing
                }else{//tableau des informations du territoire
                    $TERR[$indice] = base64_encode(serialize($tabTerr));
                }
                if (! $bAllow) {
                     throw new \Exception("Vous n'avez pas les droits suffisants pour accéder à la visualisation de cette donnée.");
                }
            }
            $indice++;
        } 
        
        
        $layerParam = array();
        $layerParam ["DATA"] = implode(";", $DATA) ; 
        $layerParam ["TYPE_STOCKAGE"] = $couchd_type_stockage ;
        $layerParam ["title"] = implode(";", $TITLE)  ;
        $layerParam ["metadataId"] = implode(";", $METADATAID)  ;
    
        
        $tabUuid = $conn->fetchAll("select uuid from public.metadata where id=:id", array ("id" => $m_IdtMetadata));
        foreach ($tabUuid as $val)
        {
            $connProdige = $this->getProdigeConnection('carmen');
            $tabMaps = $connProdige->fetchAll("select map_id, map_file from map where map_wmsmetadata_uuid = :uuid and published_id is null", array ("uuid" => $val["uuid"]));
            $map_id = "";
            foreach ($tabMaps as $val)
            {
                $map_file = $val['map_file'];
                $map_id = $val['map_id'];
            }
        
        }
        // tableau des données restreintes territorialement avec leurs territoires
        if (is_array($TERR) && count($TERR) > 0) {
            // ajout des paramètres de perimetre
            $layerParam ["model"]= $map_id;
            $layerParam ["perimetre_table"]="prodige_perimetre";
            $layerParam ["perimetre_info"] = implode(";", $TERR);
            //coming front frontcarto with object parameter, parameter is passed             
            $object = $request->get("object", "");
            if($object!=""){
                $mapUrl.="?object=".$object;
            }
        }else{
            //2 for non limited area layers default view exits ? redirect
            header("location:" . $this->getParameter('PRODIGE_URL_FRONTCARTO') . "/1/".$map_file.".map");
            exit();
        }

        //$mapUrl = urlencode($mapUrl);
        $layerParam["mapUrl"]= $mapUrl;
        
        //case Harvested data
        if ($request->query->get("Mode") == "WMS") {
            
            $protocol = $request->query->get("protocol", "");
            $connection = $request->query->get("connection", "");
            $layer = $request->query->get("layer", "");
            
            // cas récupération des informations dans la métadonnée de service
            if ($protocol == "" || $connection == "" || $layer == "") {
                // Fonctionnement par métadonnée de service associée à la métadonnée de donnée
                $uuidData = $request->query->get("uuidData");
                $uuidService = $request->query->get("uuidService");
                $tabUUidService = explode("|", $uuidService);
                $struuidService = "";
                foreach ($tabUUidService as $key => $uuid) {
                    if ($uuid != "")
                        $struuidService .= "'" . $uuid . "',";
                }
                // service de recherche des données WMS/WFS associées au service
                $struuidService = substr($struuidService, 0, - 1);
                $strSql = "select data from public.metadata where metadata.uuid in (?)";
                $rs = $dao->BuildResultSet($strSql, array(
                    $struuidService
                ));
                for ($rs->First(); ! $rs->EOF(); $rs->Next()) {
                    $data = $rs->Read(0);
                    $version = "1.0";
                    $encoding = "UTF-8";
                    $metadata_doc = new \DOMDocument($version, $encoding);
                    // chargement de la métadonnée
                    $entete = "<?xml version=\"" . $version . "\" encoding=\"" . $encoding . "\"?>\n";
                    $metadata_data = $entete . $data;
                    $metadata_data = str_replace("&", "&amp;", $metadata_data);
                    // echo $metadata_data;
                    $metadata_doc->loadXML(($metadata_data));
                    $xpath = new \DOMXpath($metadata_doc);
                    $linkedUUid = $xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/srv:coupledResource/srv:SV_CoupledResource/srv:identifier/gco:CharacterString");
                    foreach ($linkedUUid as $key => $node) {
                        if ($node->nodeValue == $uuidData) {
                            $layerNode = $node->parentNode->parentNode->getElementsByTagName("ScopedName");
                            $tablayer[] = $layerNode->item(0)->nodeValue;
                            break;
                        }
                    }
                    $linkedUrl = $xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/srv:containsOperations/srv:SV_OperationMetadata/srv:operationName/gco:CharacterString");
                    foreach ($linkedUrl as $key => $node) {
                        if ($node->nodeValue == "DescribeFeatureType" || $node->nodeValue == "GetMap") {
                            $UrlService = $xpath->query("srv:connectPoint/gmd:CI_OnlineResource/gmd:linkage/gmd:URL", $node->parentNode->parentNode);
                            $tabConnection[] = $UrlService->item(0)->nodeValue;
                            $tabprotocol[] = $node->nodeValue;
                            break;
                        }
                    }
                }
                if (count($tablayer) == 1) {
                    $layer = $tablayer[0];
                    $connection = $tabConnection[0];
                    $protocol = $tabprotocol[0];
                } else {
                    for ($i = 0; $i < count($tablayer); $i ++) {
                        
                        $layer = $tablayer[$i];
                        $connection = $tabConnection[$i];
                        $protocol = $tabprotocol[$i];
                        // priorité au service WMS associé vis à vis du service WFS, sinon on prend le dernier service associé
                        if ($tabprotocol[$i] == "GetMap") {
                            break;
                        }
                    }
                }
            }
            // fin cas récupération des informations dans la métadonnée de service
            
            $layerTitle = $request->query->get("layerTitle");
            if ($protocol == "GetMap") {
                $version = "1.1.1";
                $typeStockage = 2;
            } else {
                $version = "1.0.0";
                $typeStockage = 3;
            }
            $format = "image/png";
            // global $tabEPSG;
            $srs = "epsg:" . PRO_IMPORT_EPSG;
            
            $layerMapFile = "TMP_" . uniqid();
            $mapUrl = $this->getParameter('PRODIGE_URL_FRONTCARTO') . "/1/layers/" . $layerMapFile . ".map";
            //$mapUrl = urlencode($mapUrl);
            
            $layerParam["connection"]= $connection;
            $layerParam["DATA"]= $layer;
            $layerParam["version"]= $version;
            $layerParam["format"]= $format;
            $layerParam["srs"]= $srs;
            $layerParam["TYPE_STOCKAGE"]= $typeStockage; 
            $layerParam["mapUrl"]= $mapUrl ;
            $layerParam["title"]= $layerTitle;
            $layerParam["metadataId"]= $m_IdtMetadata;
            
        }
        
        //$m_carteVirtuel = $request->getSchemeAndHttpHost() . $this->generateUrl('catalogue_geosource_layerAddToTempmap') . "?layerMapFile=" . $layerMapFile . ".map";
        
        //$url = $m_carteVirtuel . $layerParam;
        
        $request->request->set("layerMapFile", $layerMapFile.".map");
        foreach($layerParam as $key => $value){
            $request->request->set($key, $value);
        }
        
        $response = $this->forward('GeosourceBundle:LayerAddToMap:layerAddToTempmap', array("request"=>$this->getRequest()));
        return $response;
        // error_log($url);
        /*header('location:' . $url);
        exit(0);*/
    }
}
