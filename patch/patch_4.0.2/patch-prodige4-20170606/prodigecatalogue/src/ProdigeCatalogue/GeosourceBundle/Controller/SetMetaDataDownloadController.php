<?php

namespace ProdigeCatalogue\GeosourceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\JsonResponse;

use Prodige\ProdigeBundle\Controller\BaseController;
use Prodige\ProdigeBundle\Controller\User;
use Prodige\ProdigeBundle\DAOProxy\DAO;
use Prodige\ProdigeBundle\Services\GeonetworkInterface;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * @abstract service qui permet d'inscrire ou désinscrire une donnée comme publique (en téléchargement libre)
 * @author Alkante
 * @param metadata_id   identifiant de la métadonnée (REQUEST)
 * @param mode mode add/remove
 */

/**
 * @Route("/geosource")
 */
class SetMetaDataDownloadController extends BaseController {
    
    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/setMetaDataDownload", name="catalogue_geosource_setMetaDataDownload", options={"expose"=true})
     */
    public function setMetaDataDownloadAction(Request $request) {

        $conn = $this->getCatalogueConnection('catalogue');
        $dao = new DAO($conn, 'catalogue');
        $user = User::GetUser();
        $userId = User::GetUserId();
        $geonetwork = new GeonetworkInterface(PRO_GEONETWORK_URLBASE, 'srv/fre/');
        
        // récupère l'identifiant de la métadonnée
        $mode = $request->get("mode", "");
        $metadata_id = $request->get("metadata_id", "");
        $type = $request->get("type", "dataset");
        if($mode != "" &&  $metadata_id != "") {
            //maj couche_donnees
            $dao->setSearchPath("catalogue");
            $strSql = "update couche_donnees set couchd_download=? where pk_couche_donnees in (select fmeta_fk_couche_donnees from fiche_metadonnees where fmeta_id =?);";
            $rs = $dao->Execute($strSql, array(($mode == "add" ? "1" : "0"), $metadata_id));
            if(!$rs) {
                //TODO send ERROR
            }
            $dao->setSearchPath("public");
             //pour les ensembles pas de lien de téléchargement
            $strSql = "SELECT data, uuid FROM metadata WHERE id=?";
            $rs = $dao->BuildResultSet($strSql, array($metadata_id));
            if($rs->GetNbRows() > 0) {
                $rs->First();
                $data = $rs->Read(0);
                $uuid = $rs->Read(1);
                $newData = $this->updateData($data, $mode, $metadata_id, $uuid, $type, $request);
                
                if($newData != "") {
                    $urlUpdateData = "md.edit.save";
                    $formData = array(
                       "id"=> $metadata_id,
                       "data" => $newData
                    );
                    //send to geosource
                    $geonetwork->post($urlUpdateData, $formData);
                }
            }
            

            if($mode == "add") { 

                /*********************************************************************************************
                 * Ajout du tag : sous srv:SV_ServiceInformation dans la fiche de métadonnnée de service id=-8
                 *********************************************************************************************/
                $dao->setSearchPath("public, catalogue");
                $strSql = "SELECT data from metadata where id=?";
                $rs = $dao->BuildResultSet($strSql, array(PRO_DOWNLOAD_METADATA_ID));
                if($rs->GetNbRows() > 0) {
                    $rs->First();
                    $metadata_data = $rs->Read(0);
                }
                $version  = "1.0";
                $encoding = "UTF-8";
                $metadata_doc = new \DOMDocument($version, $encoding);
                //chargement de la métadonnée
                $entete = "<?xml version=\"".$version."\" encoding=\"".$encoding."\"?>\n";
                $metadata_data = $entete.$metadata_data;
                if(@$metadata_doc->loadXML($metadata_data) !== false) {
                    // save new metadata_data
                    $xpath = new \DOMXpath($metadata_doc);
                    //serach the data
                    $objXPath = @$xpath->query("/gmd:MD_Metadata");
                    if($objXPath && $objXPath->length > 0) {
                        $strXml = "<srv:operatesOn xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:srv=\"http://www.isotc211.org/2005/srv\"  uuidref=\"".$uuid."\" xlink:href=\"".PRO_GEONETWORK_URLBASE."/srv/metadata/".$uuid."\"/>";
                        $tagSrvOperates = new \DOMDocument;
                        $tagSrvOperates->loadXML($strXml);
                        $nodeToImport = $tagSrvOperates->getElementsByTagName("operatesOn")->item(0);
                        $tagIdentificationInfo = $objXPath->item(0)->getElementsByTagName("SV_ServiceIdentification");
                        if($tagIdentificationInfo && $tagIdentificationInfo->length > 0) {
                            $newNodeDesc = $metadata_doc->importNode($nodeToImport, true);
                            $tagIdentificationInfo->item(0)->appendChild($newNodeDesc);
                        }

                        // save new metadata_data
                        $new_metadata_data = $metadata_doc->saveXML();
                        $new_metadata_data = str_replace($entete, "", $new_metadata_data);
                        if($new_metadata_data != "") {
                            $urlUpdateData = "md.edit.save";
                            $formData = array(
                               "id"=> PRO_DOWNLOAD_METADATA_ID,
                               "data" => $new_metadata_data
                            );
                            //send to geosource
                            $geonetwork->post($urlUpdateData, $formData);
                        }
                    } else {
                        $result['update_metaService_success2'] = false;
                        //write_log(" aucun traitement effectué pour métadonnée de service id=-8  /gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification");
                    }
                } else {
                    $result['update_metaService_success3'] = false;
                    //write_log("ECHEC : Impossible de charger la métadonnée de service id=-8 \n");
                }
                  
            } else { // non téléchargeable (remove)
                /********************************************************************************************
                * Si données de série non téléchargeable : on retire les tags des deux fiches de métadonnées de série et de service
                *********************************************************************************************/
                //ConnectionFactory::BeginTransaction();
                //$dao = new DAO();
                $dao->setSearchPath("public, catalogue");
                // on ne sélectionne que les metadata où il n'y a pas la balise <gmd:referenceSystemeInfo
                $strSql = "SELECT data from metadata where id=?";
                $rs = $dao->BuildResultSet($strSql, array(PRO_DOWNLOAD_METADATA_ID));
                for($rs->First(); !$rs->EOF(); $rs->Next()) {
                    $metadata_data_Service   = $rs->ReadHtml(0);
                }
                
                /*****************************************
                 * cas des métadonnées de service
                 ******************************************/
                $version  = "1.0";
                $encoding = "UTF-8";
                $metadata_doc_service = new \DOMDocument($version, $encoding);
                //chargement de la métadonnée
                $entete = "<?xml version=\"".$version."\" encoding=\"".$encoding."\"?>\n";
                $metadata_data_Service = $entete.$metadata_data_Service;
                if(@$metadata_doc_service->loadXML($metadata_data_Service) !== false) {
                    $xpath_service = new \DOMXpath($metadata_doc_service);
                    $nodeToDelete = $xpath_service->query("/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/srv:operatesOn[@uuidref=\"".$uuid."\"]");
                    if($nodeToDelete->length > 0) {
                        foreach($nodeToDelete as $key => $node) {
                            foreach($node->attributes as $attr) {
                                //$tabAttr[$key][$attr->localName] = $attr->nodeValue;
                                if($attr->localName == "uuidref" && $attr->nodeValue == $uuid) {
                                    $node->parentNode->removeChild($node);
                                }
                            }
                        }
                    }
                }
                // save
                $new_metadata_data = $metadata_doc_service->saveXML();
                $new_metadata_data = str_replace($entete, "", $new_metadata_data);
                if($new_metadata_data != "") {
                    $urlUpdateData = "md.edit.save";
                    $formData = array(
                       "id"=> PRO_DOWNLOAD_METADATA_ID,
                       "data" => $new_metadata_data
                    );
                    //send to geosource
                    $geonetwork->post($urlUpdateData, $formData);
                }

               
            }
            $result['update_success'] = true;
        } else {
            $result['update_success'] = false;
            $result['msg']            = htmlentities("Le service est mal construit.", ENT_QUOTES, "UTF-8");
        }

        return new JsonResponse($result);
    }

    /**
     * @abstract met à jour la date de validité et la date de mise à jour dans le XML de la métadonnée
     * @param data                contenu XML de la métadonnée
     * @param mode mode add ou remove
     * @return contenu XML de la métadonnée mis à jour, chaîne vide si une erreur est survenue
     */
    protected function updateData($data, $mode, $metadata_id, $uuid, $type, $request) {

        $newData = "";
        $version  = "1.0";
        $encoding = "UTF-8";
        $entete   = "<?xml version=\"".$version."\" encoding=\"".$encoding."\"?>\n";

        $data = str_replace("&", "&amp;", $data);
        $data = $entete.$data;

        $doc = new \DOMDocument($version, $encoding);

        if(@$doc->loadXML($data) ) {
            $serviceUrl = $this->generateUrl('catalogue_geosource_panierDownloadFrontalParametrage', array('LAYERIDTS'=>$metadata_id), self::ABSOLUTE_URL);
            $serviceAtomUrl = $this->generateUrl('catalogue_atomfeed_atomdataset',array("uuid" => $uuid ), self::ABSOLUTE_URL);
            $serviceProtocol = "WWW:DOWNLOAD-1.0-http--download";
            $serviceAtomProtocol = "WWW:LINK-1.0-http--link";
            $xpath = new \DOMXpath($doc);
            if($mode == "add") {
                $distributionInfo = $xpath->query("/gmd:MD_Metadata/gmd:distributionInfo/gmd:MD_Distribution");
                //pour les ensembles, pas d'inscription du lien vers l'interface de téléchargement
                if($type != "series") {
                    if($distributionInfo->length > 0) {
                        //mode ajout
                        $strXML =
                            '<gmd:MD_Metadata xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns="http://www.isotc211.org/2005/gmd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:fra="http://www.cnig.gouv.fr/2005/fra" xmlns:gco="http://www.isotc211.org/2005/gco" xmlns:gts="http://www.isotc211.org/2005/gts" xmlns:gml="http://www.opengis.net/gml" xmlns:gfc="http://www.isotc211.org/2005/gfc" xmlns:gmx="http://www.isotc211.org/2005/gmx" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:gmi="http://www.isotc211.org/2005/gmi">
                                <gmd:transferOptions>
                                    <gmd:MD_DigitalTransferOptions>
                                        <gmd:onLine>
                                            <gmd:CI_OnlineResource>
                                                <gmd:linkage>
                                                    <gmd:URL>'.$serviceUrl.'</gmd:URL>
                                                </gmd:linkage>
                                                <gmd:protocol>
                                                    <gco:CharacterString>'.$serviceProtocol.'</gco:CharacterString>
                                                </gmd:protocol>
                                            </gmd:CI_OnlineResource>
                                        </gmd:onLine>
                                    </gmd:MD_DigitalTransferOptions>
                                </gmd:transferOptions>
                            </gmd:MD_Metadata>';
    
                        $orgDoc = new \DOMDocument;
                        $orgDoc->loadXML($strXML);
                        $nodeToImport = $orgDoc->getElementsByTagName("transferOptions")->item(0);
                        $newNodeDesc = $doc->importNode($nodeToImport, true);
                        $distributionInfo->item(0)->appendChild($newNodeDesc);
                    } else {
                        //mode création complète
                        $racine = $doc->getElementsByTagName("MD_Metadata");
                        if($racine){
                            $strXML =
                                    '<gmd:MD_Metadata xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns="http://www.isotc211.org/2005/gmd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:fra="http://www.cnig.gouv.fr/2005/fra" xmlns:gco="http://www.isotc211.org/2005/gco" xmlns:gts="http://www.isotc211.org/2005/gts" xmlns:gml="http://www.opengis.net/gml" xmlns:gfc="http://www.isotc211.org/2005/gfc" xmlns:gmx="http://www.isotc211.org/2005/gmx" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:gmi="http://www.isotc211.org/2005/gmi">
                                        <gmd:distributionInfo>
                                            <gmd:MD_Distribution>
                                                <gmd:distributionFormat>
                                                    <gmd:MD_Format>
                                                        <gmd:name>
                                                            <gco:CharacterString>ZIP</gco:CharacterString>
                                                        </gmd:name>
                                                        <gmd:version gco:nilReason="missing">
                                                            <gco:CharacterString/>
                                                        </gmd:version>
                                                    </gmd:MD_Format>
                                                </gmd:distributionFormat>
                                                <gmd:transferOptions>
                                                    <gmd:MD_DigitalTransferOptions>
                                                        <gmd:onLine>
                                                            <gmd:CI_OnlineResource>
                                                                <gmd:linkage>
                                                                    <gmd:URL>'.$serviceUrl.'</gmd:URL>
                                                                </gmd:linkage>
                                                                <gmd:protocol>
                                                                    <gco:CharacterString>'.$serviceProtocol.'</gco:CharacterString>
                                                                </gmd:protocol>
                                                            </gmd:CI_OnlineResource>
                                                        </gmd:onLine>
                                                    </gmd:MD_DigitalTransferOptions>
                                                </gmd:transferOptions>
                                            </gmd:MD_Distribution>
                                        </gmd:distributionInfo>
                                    </gmd:MD_Metadata>';
    
                            $orgDoc = new \DOMDocument;
                            $orgDoc->loadXML($strXML);
                            $nodeToImport = $orgDoc->getElementsByTagName("distributionInfo")->item(0);
                            $newNodeDesc = $doc->importNode($nodeToImport, true);
                            $dataQualityInfo = $racine->item(0)->getElementsByTagName("dataQualityInfo");
    
                            if($dataQualityInfo && $dataQualityInfo->length > 0) {
                                $racine->item(0)->insertBefore($newNodeDesc, $dataQualityInfo->item(0));
                            } else {
                                $racine->item(0)->appendChild($newNodeDesc);
                            }
                        }
                    }
                }
                /*********************************************************************************************
                 * Ajout du tag : sous gmd:MD_Distribution dans la fiche de métadonnnée de série de données
                *********************************************************************************************/
                
                //serach the data<gmd:distributionInfo>
                $objXPath2 = @$xpath->query("/gmd:MD_Metadata");
                if($objXPath2 && $objXPath2->length > 0) {
                    $strXml2 = "<gmd:transferOptions xmlns:gmd=\"http://www.isotc211.org/2005/gmd\" xmlns:gco=\"http://www.isotc211.org/2005/gco\">
                                 <gmd:MD_DigitalTransferOptions>
                                   <gmd:onLine>
                                     <gmd:CI_OnlineResource>
                                       <gmd:linkage>
                                         <gmd:URL>".$serviceAtomUrl."</gmd:URL>
                                       </gmd:linkage>
                                       <gmd:protocol>
                                      	 <gco:CharacterString>WWW:LINK-1.0-http--link</gco:CharacterString>
                                       </gmd:protocol>
                                       <gmd:name>
                                         <gco:CharacterString>Téléchargement direct des données</gco:CharacterString>
                                       </gmd:name>
                                     </gmd:CI_OnlineResource>
                                   </gmd:onLine>
                                 </gmd:MD_DigitalTransferOptions>
                               </gmd:transferOptions>";
            
                    $tagTransferOption = new \DOMDocument;
                    $tagTransferOption->loadXML($strXml2);
                    $nodeToImport = $tagTransferOption->getElementsByTagName("transferOptions")->item(0);
                    $tagMD_Distribution = $objXPath2->item(0)->getElementsByTagName("MD_Distribution");
                    if($tagMD_Distribution && $tagMD_Distribution->length > 0) {
                        $newNodeDesc = $doc->importNode($nodeToImport, true);
                        $tagMD_Distribution->item(0)->appendChild($newNodeDesc);
                    } else { // le noeud n'existe pas, création
            
                        //mode création complète
                        $strXML = '<gmd:MD_Metadata xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns="http://www.isotc211.org/2005/gmd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:fra="http://www.cnig.gouv.fr/2005/fra" xmlns:gco="http://www.isotc211.org/2005/gco" xmlns:gts="http://www.isotc211.org/2005/gts" xmlns:gml="http://www.opengis.net/gml" xmlns:gfc="http://www.isotc211.org/2005/gfc" xmlns:gmx="http://www.isotc211.org/2005/gmx" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:gmi="http://www.isotc211.org/2005/gmi">
                                      <gmd:distributionInfo>
                                        <gmd:MD_Distribution>
                                          <gmd:distributionFormat>
                                            <gmd:MD_Format>
                                              <gmd:name>
                                                <gco:CharacterString>ZIP</gco:CharacterString>
                                              </gmd:name>
                                              <gmd:version gco:nilReason="missing">
                                                <gco:CharacterString/>
                                              </gmd:version>
                                            </gmd:MD_Format>
                                          </gmd:distributionFormat>
                                          <gmd:transferOptions>
                                            <gmd:MD_DigitalTransferOptions>
                                                 <gmd:onLine>
                                                   <gmd:CI_OnlineResource>
                                                     <gmd:linkage>
                                                       <gmd:URL>'.$serviceAtomUrl.'</gmd:URL>
                                                     </gmd:linkage>
                                                     <gmd:protocol>
                                                       <gco:CharacterString>'.$serviceAtomProtocol.'</gco:CharacterString>
                                                     </gmd:protocol>
                                                     <gmd:name>
                                                       <gco:CharacterString>Téléchargement direct des données</gco:CharacterString>
                                                     </gmd:name>
                                                   </gmd:CI_OnlineResource>
                                                 </gmd:onLine>
                                               </gmd:MD_DigitalTransferOptions>
                                          </gmd:transferOptions>
                                        </gmd:MD_Distribution>
                                      </gmd:distributionInfo></gmd:MD_Metadata>';
            
                        $orgDoc = new \DOMDocument;
                        $orgDoc->loadXML($strXML);
                        $nodeToImport = $orgDoc->getElementsByTagName("distributionInfo")->item(0);
                        $newNodeDesc = $doc->importNode($nodeToImport, true);
                        $dataQualityInfo = $objXPath2->item(0)->getElementsByTagName("dataQualityInfo");
                        if($dataQualityInfo && $dataQualityInfo->length > 0) {
                            $objXPath2->item(0)->insertBefore($newNodeDesc, $dataQualityInfo->item(0));
                        } else {
                            $objXPath2->item(0)->appendChild($newNodeDesc);
                        }
                    }
                }
            } elseif($mode == "remove") {
                
                $xpath = new \DOMXpath($doc);
                $distributionInfo = $xpath->query("/gmd:MD_Metadata/gmd:distributionInfo");
                if($distributionInfo->length > 0) {
                    $transferOptions = $xpath->query("/gmd:MD_Metadata/gmd:distributionInfo/gmd:MD_Distribution/gmd:transferOptions/gmd:MD_DigitalTransferOptions/gmd:onLine");
                    if($transferOptions->length > 0) {
                        foreach($transferOptions as $key => $item){
                            $protocol = $item->getElementsByTagName("CharacterString");
                            $url = $item->getElementsByTagName("URL");
                            /*echo "protocol:".$protocol->item(0) && $protocol->item(0)->nodeValue." ".$protocol;
                            echo "\n url".$url->item(0)->nodeValue." ". $serviceUrl;*/
                            if($protocol && $protocol->item(0)){
                                if (($protocol->item(0)->nodeValue == $serviceProtocol && $url && $url->item(0) && $url->item(0)->nodeValue == $serviceUrl )|| 
                                   ($protocol->item(0)->nodeValue == $serviceAtomProtocol &&  $url && $url->item(0)  && $url->item(0)->nodeValue == $serviceAtomUrl )) {
                                    //un seul element => on supprime la balise complète
                                    if($transferOptions->length == 1){
                                        $distributionInfo->item(0)->parentNode->removeChild($distributionInfo->item(0));
                                    }else{
                                        $item->parentNode->removeChild($item);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            $newData = $doc->saveXML();
            $newData = str_replace($entete, "", $newData);
            return $newData;
        }
    }
}