<?php
namespace ProdigeCatalogue\GeosourceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\JsonResponse;
use ProdigeCatalogue\GeosourceBundle\Common\MetadataDesc;
use Prodige\ProdigeBundle\Controller\BaseController;
use Prodige\ProdigeBundle\Controller\User;
use Prodige\ProdigeBundle\Common\Util;
use Prodige\ProdigeBundle\DAOProxy\DAO;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Get layer list from postgresql catalogue database
 *
 * @author Alkante
 *         Prodige V3 ---> V4
 */

/**
 * @Route("/geosource")
 */
class AdministrationCartoCoucheController extends BaseController
{

    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/administrationCartoCouche", name="catalogue_geosource_administrationCartoCouche", options={"expose"=true})
     */
    public function administrationCartoCoucheAction(Request $request)
    {
        
        //TODO Rights
        $callback = $request->get("callback");
        
        $user = User::GetUser();
        $userId = User::GetUserId();
        /*
         * @deprecated prodige4 * if(!is_null($userId)) User::SetUserCookie($userId); if(is_null($userId)) { echo $callback."(Session Catalogue expirée.)"; exit(); }
         */
        
        $coucheType = 1;
        $m_srv = "";
        if ($request->query->get("coucheType", false)) {
            $coucheType = $request->query->get("coucheType");
        }
        if ($request->request->get("SRV_CARTO", false)) {
            $m_srv = $request->request->get("SRV_CARTO");
        } else 
            if ($request->query->get("SRV_CARTO", false)) {
                $m_srv = $request->query->get("SRV_CARTO");
            }
        
        $domain = $request->query->get("domain", "");
        $subdomain = $request->query->get("subdomain", "");
        
        $returnJSON = $this->getLayerList($user, $coucheType, $m_srv, $domain, $subdomain);
        
        return new Response($callback . "(" . $returnJSON . ")");
    }

    /**
     * brief Returns a json object containing the list of the layers availaible in the catalog
     * The pattern is :
     * {"name" : "name", "type" : "raster or vector (O or 1)",
     * "table_name" : "postgis_table or raster file", "id" : "metadata id", "text" : "metadata title and text for extjs tree",
     * "leaf" : true or false, "cls" : style, "children" : null}
     *
     * @param
     *            $dbconn
     * @return json text
     */
    protected function getLayerList(User $user, $coucheType, $m_srv, $domain, $subdomain)
    {
        // $start = time();
        // global $coucheType, $m_srv, $domain, $subdomain;
        $errMessage = null;
        $tabLayers = array();
        
        // recup des couche carto du serveur
        if (! is_null($m_srv)) {
            if ($m_srv != "") {
                $conn_catalogue = $this->getCatalogueConnection('catalogue');
                /*
                 * MetadataDesc::setConnection($this->getCatalogueConnection('catalogue')); MetadataDesc::setContainer($this->container); $lstIdts = MetadataDesc::GetMetadataOfServeur($m_srv); $mdid_list = ""; for($idx=0;$idx<count($lstIdts);$idx++) { if($mdid_list != "") { $mdid_list .= ","; } $mdid_list.=$lstIdts[$idx]; }
                 */
                $m_sSql = "SELECT distinct fmeta_id
                        , xpath('//gmd:identificationInfo/gmd:MD_DataIdentification/gmd:citation/gmd:CI_Citation/gmd:title/gco:CharacterString/text()'::text, " . 
                              "('<?xml version=\"1.0\" encoding=\"utf-8\"?>'::text || data)::xml, " . 
                              "ARRAY[ARRAY['gmd'::text, 'http://www.isotc211.org/2005/gmd'::text], " . 
                              "ARRAY['gco'::text, 'http://www.isotc211.org/2005/gco'::text]])::text AS \"metadatatitle\"
                        , couchd_type_stockage
                        , couchd_emplacement_stockage
                        , metadata.uuid
                        , couchd_nom
                                      
                        from couche_sdom 
                        inner join public.metadata on (couche_sdom.fmeta_id::bigint = metadata.id) 
                        inner join public.operationallowed opallow ON (metadata.id = opallow.metadataid AND opallow.operationid = 0 AND opallow.groupid = 1) 
                        where couchd_type_stockage in (" . $coucheType . ") and couchd_visualisable=1 ";
                
                if ($domain != "" && $subdomain != "") {
                    if (strpos($domain, "rubric_") === false) {
                        $subdomain = ($subdomain == - 1 ? "(SELECT pk_sous_domaine FROM sous_domaine" . ($domain == - 1 ? "" : " WHERE ssdom_fk_domaine = " . $domain) . ")" : $subdomain);
                    } else {
                        $rubric = str_replace("rubric_", "", $domain);
                        $subdomain = "(SELECT pk_sous_domaine FROM sous_domaine WHERE ssdom_fk_domaine IN (SELECT pk_domaine FROM domaine WHERE dom_rubric=" . $rubric . "))";
                    }
                    $m_sSql .=                     // "SELECT cd.couchd_nom as coucheNom, cd.couchd_emplacement_stockage as coucheTable, cd.couchd_type_stockage as coucheType, fm.fmeta_id as metadataId
                    " and pk_sous_domaine IN (" . $subdomain . ")";
                }
                $m_sSql .= " order by metadatatitle";
                $dao_catalogue = new DAO($conn_catalogue, 'catalogue');
                
                $rs = $dao_catalogue->BuildResultSet($m_sSql);
                $tabEmplacementStockage = array();
                
                // list of layers possible
                if ($rs->GetNbRows() > 0) {
                    for ($rs->First(); ! $rs->EOF(); $rs->Next()) {
                        // vérification des droits sur un des domaines de la donnée
                        $couchd_emplacement_stockage = $rs->Read(3);
                        $uuid = $rs->read(4);
                        $couchd_nom = $rs->Read(5);
                        $coucheType = $rs->read(2);
                        $bAllow = false;
                        
                        $query = "SELECT DOM_NOM, SSDOM_NOM, PK_COUCHE_DONNEES, COUCHD_RESTRICTION, UUID, couchd_emplacement_stockage FROM COUCHE_SDOM WHERE couchd_emplacement_stockage =?";
                        $rs2 = $dao_catalogue->BuildResultSet($query, array($couchd_emplacement_stockage));
                        
                        for ($rs2->First(); ! $rs2->EOF(); $rs2->Next()) {
                            $domaine = $rs2->read(0);
                            $sous_domaine = $rs2->read(1);
                            $objetCouche = $rs2->read(2);
                            $restriction = $rs2->read(3);
                            
                            if ($restriction == 1) { // données restreintes ne peuvent être ajoutées dans les cartes
                                break;
                            }
                            // vérification de l'autorisation sur le domaine et sur l'objet
                            $bAllow = $user->HasTraitement('NAVIGATION', html_entity_decode($domaine, ENT_QUOTES, 'UTF-8'), html_entity_decode($sous_domaine, ENT_QUOTES, 'UTF-8'), PRO_TRT_TYPE_CONSULTATION, $objetCouche, PRO_OBJET_TYPE_COUCHE);
                            if ($bAllow)
                                break;
                        }
                        // vérification des restrictions de compétences
                        $bAllow = $bAllow && $user->HasTraitementCompetence('NAVIGATION', $objetCouche);
                        
                        if ($bAllow) {
                            
                            $tabEmplacementStockage[$uuid]["title"] = str_replace(array(
                                "{",
                                "\"",
                                "}"
                            ), "", $rs->Read(1));
                            $tabEmplacementStockage[$uuid]["type"] = $coucheType;
                            $tabEmplacementStockage[$uuid]["couchd_nom"][] = $couchd_nom;
                            $tabEmplacementStockage[$uuid]["layers"][] = $couchd_emplacement_stockage;
                        }
                    }
                }
                // list of layers allowed
                $i = 0;
                
                $sql_orderlayers_from_tree = 
<<<SQL
with recursive 
node_tree(node_id, map_id, node_parent, node_pos, node_opened, node_is_layer, node_depth, group_id, layer_id) 
as (
  select map_tree.node_id, map_tree.map_id, map_tree.node_parent, map_tree.node_pos, map_tree.node_opened, map_tree.node_is_layer
  , 1
  , (case when map_tree.node_is_layer then null::int else map_tree.node_id end) as group_id
  , (case when map_tree.node_is_layer then map_tree.node_id else null::int end) as layer_id
  from map_tree
  where map_tree.map_id=:map_id
  and map_tree.node_parent is null

  union all

  select map_tree.node_id, map_tree.map_id, map_tree.node_parent, map_tree.node_pos, map_tree.node_opened, map_tree.node_is_layer
  , node_tree.node_depth+1
  , (case when map_tree.node_is_layer then null::int else map_tree.node_id end) as group_id
  , (case when map_tree.node_is_layer then map_tree.node_id else null::int end) as layer_id
  from map_tree 
  inner join node_tree on (node_tree.map_id=map_tree.map_id and node_tree.node_id=map_tree.node_parent)
  where map_tree.map_id=:map_id
) 
SQL;
                foreach ($tabEmplacementStockage as $uuid => $tabInfo) {
                    
                    $tabLayers[$i]["text"] = $tabInfo["title"];
                    $tabLayers[$i]["id"] = $uuid; // id for postprocessing and for extjs tree
                    $tabLayers[$i]["leaf"] = true; // leaf for extjs tree
                    $tabLayers[$i]["cls"] = "folder"; // cls for extjs tree
                    $tabLayers[$i]["children"] = ""; // children for extjs tree
                    $tabLayers[$i]["layers_id"] = array();
                    $tabLayers[$i]["layers"] = array();
                    $tabLayers[$i]["icon"] = "";
                    
                    $conn_prodige = $this->getProdigeConnection('prodige');
                    $dao_prodige = new DAO($conn_prodige, 'carmen');
                    $rsMap = $dao_prodige->buildResultSet("select map_id, map_file from map where map_wmsmetadata_uuid=:map_wmsmetadata_uuid and published_id is null", array(
                        "map_wmsmetadata_uuid" => $uuid
                    ));
                    
                    // default representation exists, present map_id and layers_id in service
                    if (! $rsMap->EOF()) {
                        $rsMap->First();
                        $map_id = $rsMap->Read(0);
                        $map_file = $rsMap->Read(1);
                        $tabLayers[$i]["icon"] = "/images/multiple.gif";
                        $tabLayers[$i]["map_id"] = $map_id;
                        
                        $oMap = null;
                        if (file_exists(PRO_MAPFILE_PATH . $map_file . ".map") ) {
                            $oMap = ms_newMapObj(PRO_MAPFILE_PATH . $map_file . ".map");
                        
                            $rsLayers = $dao_prodige->buildResultSet(
                                $sql_orderlayers_from_tree.
                                " select layer_id
                                     from layer
                                     inner join node_tree using(layer_id)
                                     where layer_metadata_uuid=:layer_metadata_uuid
                                     order by node_depth, node_pos
                                    ", array(
                                "map_id" => $map_id,
                                "layer_metadata_uuid" => $uuid
                            ));
                            for ($rsLayers->First(); ! $rsLayers->EOF(); $rsLayers->Next()) {
                                $tabLayers[$i]["layers_id"][] = $rsLayers->Read(0);
                            }
                        }
                        if ( count($tabLayers[$i]["layers_id"])==1 ){
                        
                            foreach($tabInfo["layers"] as $key => $layer_data){
                                $layerDef = array();
                                if ($tabInfo["type"] == 0) { // raster
                                    $const_typeRasterTuile = ".shp";
                                    $const_typeRasterUnique = ".ecw|.tif";
    
                                    $typeGeom = strtolower(substr($layer_data, strrpos($layer_data, ".")));
                                    $typerasterTuile = explode("|", strtolower($const_typeRasterTuile));
                                    $typerasterUnique = explode("|", strtolower($const_typeRasterUnique));
                                    if (in_array($typeGeom, $typerasterUnique)) {
                                        $tabLayers[$i]["icon"]= "/images/uniq_raster.gif";
                                    } else  if (in_array($typeGeom, $typerasterTuile)) {
                                        $tabLayers[$i]["icon"]= "/images/tiled_raster.gif";
                                    } else {
                                        $tabLayers[$i]["icon"]= "/images/geometry.gif";
                                    }
                                } else { // vector
                                    $layerDef["type"] = "VECTOR"; // type for postprocessing
                                    $sql_get_infos_for_table_name = "SELECT 1 from information_schema.tables where table_name = ? union ".
                                                                    " SELECT 1 from information_schema.views where table_name = ? ";
                                    $table_exists = !$dao_prodige->buildResultSet($sql_get_infos_for_table_name, array(
                                        $layer_data, $layer_data
                                    ))->EOF();
                                    if ( !$table_exists ) continue;
                                    $sql_get_infos_for_table_name = "SELECT srid, type from public.geometry_columns where f_table_name = ?";
                                    $rs3 = $dao_prodige->buildResultSet($sql_get_infos_for_table_name, array(
                                        $layer_data
                                    ));
                                    if ( strtoupper($rs3->Read(1))=="GEOMETRY" ){
                                        $sql_get_infos_for_table_name = "SELECT distinct public.st_srid(the_geom), public.geometrytype(the_geom) from public.".$layer_data." limit 1";
                                        $rs3 = $dao_prodige->buildResultSet($sql_get_infos_for_table_name);
                                    }
                                    switch (strtoupper($rs3->Read(1))) {
                                        case "POINT":
                                        case "MULTIPOINT":
                                            $tabLayers[$i]["icon"] = "/images/point.gif";
                                            break;
                                        case "LINESTRING":
                                        case "MULTILINESTRING":
                                            $tabLayers[$i]["icon"] = "/images/linestring.gif";
                                            $layerDef["geometry"] = "LINE";
                                            break;
                                        case "MULTIPOLYGON":
                                        case "POLYGON":
                                            $tabLayers[$i]["icon"] = "/images/polygon.gif";
                                            $layerDef["geometry"] = "POLYGON";
                                            break;
                                        default:
                                            $tabLayers[$i]["icon"] = "/images/geometry.gif";
                                            break;
                                    }
                                }
                            }
                        }
                    } 
                    
                    if ( empty($tabLayers[$i]["layers_id"]) ) {
                        $dao_prodige = new DAO($conn_prodige, 'public');
                        $tabLayers[$i]["map_id"] = "";
                        $numLayers = 0;
                        $tabLayers[$i]["layers"] = array();
                        foreach($tabInfo["layers"] as $key => $layer_data){
                            $layerDef = array();
                            if ($tabInfo["type"] == 0) { // raster
                                $const_typeRasterTuile = ".shp";
                                $const_typeRasterUnique = ".ecw|.tif";

                                $typeGeom = strtolower(substr($layer_data, strrpos($layer_data, ".")));
                                $typerasterTuile = explode("|", strtolower($const_typeRasterTuile));
                                $typerasterUnique = explode("|", strtolower($const_typeRasterUnique));
                                if (in_array($typeGeom, $typerasterUnique)) {
                                    $tabLayers[$i]["type"][$numLayers] = "RASTER_UNIQUE"; // "TYPE_UNIQUE";
                                    $tabLayers[$i]["icon"]= "/images/uniq_raster.gif";
                                } else  if (in_array($typeGeom, $typerasterTuile)) {
                                    $layerDef["type"] = "RASTER_DALLE"; // "TYPE_TUILE";
                                    $tabLayers[$i]["icon"]= "/images/tiled_raster.gif";
                                }
                            } else { // vector
                                $layerDef["type"] = "VECTOR"; // type for postprocessing
                                $sql_get_infos_for_table_name = "SELECT 1 from information_schema.tables where table_name = ? union ".
                                                                " SELECT 1 from information_schema.views where table_name = ? ";
                                $table_exists = !$dao_prodige->buildResultSet($sql_get_infos_for_table_name, array(
                                    $layer_data, $layer_data
                                ))->EOF();
                                if ( !$table_exists ) continue;
                            
                                $sql_get_infos_for_table_name = "SELECT srid, type from geometry_columns where f_table_name = ?";
                                $rs3 = $dao_prodige->buildResultSet($sql_get_infos_for_table_name, array(
                                    $layer_data
                                ));
                                if ( strtoupper($rs3->Read(1))=="GEOMETRY" ){
                                    $sql_get_infos_for_table_name = "SELECT distinct public.st_srid(the_geom), public.geometrytype(the_geom) from public.".$layer_data." limit 1";
                                    $rs3 = $dao_prodige->buildResultSet($sql_get_infos_for_table_name);
                                }
                                $layerDef["srid"] = $rs3->Read(0) ?: null;
                                $layerDef["geometry"] = $rs3->Read(1) ?: null;
                                switch ($rs3->Read(1)) {
                                    case "POINT":
                                    case "MULTIPOINT":
                                        $tabLayers[$i]["icon"] = "/images/point.gif";
                                        $layerDef["geometry"] = "POINT";
                                        break;
                                    case "LINESTRING":
                                    case "MULTILINESTRING":
                                        $tabLayers[$i]["icon"] = "/images/linestring.gif";
                                        $layerDef["geometry"] = "LINE";
                                        break;
                                    case "MULTIPOLYGON":
                                    case "POLYGON":
                                        $tabLayers[$i]["icon"] = "/images/polygon.gif";
                                        $layerDef["geometry"] = "POLYGON";
                                        break;
                                    default:
                                        $tabLayers[$i]["icon"] = "/images/geometry.gif";
                                        break;
                                }
                            }
                            $layerDef["table_name"]= $layer_data; // table for postprocessing
                            $layerDef["title"] = $tabInfo["couchd_nom"][$key]; // table for postprocessing
                           
                            $tabLayers[$i]["layers"][] = $layerDef;
                            $numLayers++;
                        }
                        if($numLayers>1){
                            $tabLayers[$i]["icon"] = "/images/multiple.gif";
                        }
                    }
                    
                    if ( empty($tabLayers[$i]["layers"]) && empty($tabLayers[$i]["layers_id"]) ){
                        unset($tabLayers[$i]);
                    }
                    $i ++;
                }
                $tabLayers = array_values($tabLayers);
                if (empty($tabLayers)) {
                    $errMessage = "Le serveur cartographique ne dispose d\'aucune couche de données.";
                }
            } else {
                
                $errMessage = "Erreur interne: ";
                if (is_null($m_srv)) {
                    $errMessage .= "\\nAucun serveur cartographique n'est défini [SRV_CARTO].";
                }
            }
            
            if (! is_null($errMessage)) {
                return json_encode(array());
            } else {
                return (json_encode($tabLayers));
            }
        }
    }
}
