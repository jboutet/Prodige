<?php

namespace ProdigeCatalogue\RSSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Prodige\ProdigeBundle\Controller\BaseController;

use ProdigeCatalogue\RSSBundle\Common\Synd\AlkFSyndXmlWriter;
use ProdigeCatalogue\RSSBundle\Common\Synd\AlkFSyndXmlItem;

use Prodige\ProdigeBundle\DAOProxy\DAO;

/**
 * RSS Controller
 * @author alkante <support@alkante.com>
 * @Route("/rss")
 *
 */

class RSSController extends BaseController {

    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/generate_flux_rss/{categorie_rss}/{type}", name="catalogue_generate_flux_rss", options={"expose"=true} )
     * #Method({"GET", "POST"})
     * */
    public function generateFluxRssAction(Request $request, $categorie_rss, $type=null) {

        //require_once('include/ClassSyndXml.php');
        //require_once('parametrage.php');

        /**
         * Génération des flux RSS
         * @author Alkante
         */

        //$AdminPath = "Administration/";
        //require_once($AdminPath."DAO/DAO/DAO.php");

        //$categorie_rss = (isset($_GET["categorie_rss"]) ? $_GET["categorie_rss"] : "");
        //$type = (isset($_GET["type"]) ? $_GET["type"] : "");

        //$pk_dom = (isset($_GET["pk_dom"]) && is_numeric($_GET["pk_dom"]) ? $_GET["pk_dom"] : "-1");
        $pk_dom = $request->get("pk_dom", -1);
        $pk_dom += 0; // converted to numeric type

        $conn = $this->getDoctrine()->getConnection('catalogue');
        $dao = new DAO($conn, 'catalogue');
        if($dao) {
            $oSyndWriter = new AlkFSyndXmlWriter();
            if($categorie_rss == "MISE_A_JOUR") {
                $oSyndWriter->setTitle("Mise à jour du catalogue");
                $oSyndWriter->setDescription("Flux RSS listant les mises à jour du catalogue");
                //$oSyndWriter->setLink( (defined("PRO_PRINCIPAL_URLBASE")? PRO_PRINCIPAL_URLBASE : PRO_CATALOGUE_URLBASE)."/generateFluxRss.php?categorie_rss=MISE_A_JOUR");
                $oSyndWriter->setLink($request->getSchemeAndHttpHost()."/generate_flux_rss/MISE_A_JOUR");
                $rs = $dao->BuildResultSet("SELECT \"nom\", \"résumé\", \"data_type\", \"date\", \"id\", image from STATISTIQUE_DERNIERES_MISES_A_JOUR".
                    " left join (select uuid, xpath('//gmd:graphicOverview/gmd:MD_BrowseGraphic/gmd:fileName/gco:CharacterString/text()',".
                    "('<?xml version=\"1.0\" encoding=\"utf-8\"?>' || data)::xml,".
                    " ARRAY[ARRAY['gmd','http://www.isotc211.org/2005/gmd'],".
                    " ARRAY['gco','http://www.isotc211.org/2005/gco']])::text as image from public.metadata where ((not data is NULL) and data!='' and isharvested='n')) metadata on ".
                    "metadata.uuid = STATISTIQUE_DERNIERES_MISES_A_JOUR.id");
            } elseif($categorie_rss == "NOUVEAUTE") {
                $oSyndWriter->setDescription("Flux RSS listant les nouveautés du catalogue");
                //$oSyndWriter->setLink((defined("PRO_PRINCIPAL_URLBASE")? PRO_PRINCIPAL_URLBASE : PRO_CATALOGUE_URLBASE)."/generateFluxRss.php?categorie_rss=NOUVEAUTE&type=".$type);
                $oSyndWriter->setLink($request->getSchemeAndHttpHost()."/generate_flux_rss/NOUVEAUTE/".$type);
                switch($type){
                    case "dataset" :
                        $oSyndWriter->setTitle("Nouvelles séries de données du catalogue");

                        $rs = $dao->BuildResultSet('SELECT statistique_liste_couche_publiees."nom de la couche" AS nom, statistique_liste_couche_publiees."résumé",'.
                            ' statistique_liste_couche_publiees.changedate AS date, statistique_liste_couche_publiees.createdate, '.
                            ' statistique_liste_couche_publiees."uuid metadata" AS id, image, statistique_liste_couche_publiees."couche vecteur ?" as type'.
                            ' FROM statistique_liste_couche_publiees '.
                            " left join (select uuid, xpath('//gmd:graphicOverview/gmd:MD_BrowseGraphic/gmd:fileName/gco:CharacterString/text()',".
                            "('<?xml version=\"1.0\" encoding=\"utf-8\"?>' || data)::xml,".
                            " ARRAY[ARRAY['gmd','http://www.isotc211.org/2005/gmd'],".
                            " ARRAY['gco','http://www.isotc211.org/2005/gco']])::text as image from public.metadata where ((not data is NULL) and data!='' and isharvested='n')) metadata on ".
                            "metadata.uuid = statistique_liste_couche_publiees.\"uuid metadata\"".
                            ($pk_dom!=-1 ? "left join couche_sdom on couche_sdom.pk_couche_donnees = statistique_liste_couche_publiees.pk_couche_donnees " : "").
                            'where "couche vecteur ?" in (0,1,-3)'.
                            ($pk_dom!=-1 ? "and pk_domaine =".$pk_dom: "").
                            'order by createdate desc limit 20;');
                            break;
                    case "series" :
                        $oSyndWriter->setTitle("Nouveaux ensembles de séries de données du catalogue");
                        $rs = $dao->BuildResultSet('SELECT statistique_liste_couche_publiees."nom de la couche" AS nom, statistique_liste_couche_publiees."résumé",'.
                            ' statistique_liste_couche_publiees.changedate AS date, statistique_liste_couche_publiees.createdate, '.
                            ' statistique_liste_couche_publiees."uuid metadata" AS id, image'.
                            ' FROM statistique_liste_couche_publiees '.
                            " left join (select uuid, xpath('//gmd:graphicOverview/gmd:MD_BrowseGraphic/gmd:fileName/gco:CharacterString/text()',".
                            "('<?xml version=\"1.0\" encoding=\"utf-8\"?>' || data)::xml,".
                            " ARRAY[ARRAY['gmd','http://www.isotc211.org/2005/gmd'],".
                            " ARRAY['gco','http://www.isotc211.org/2005/gco']])::text as image from public.metadata where ((not data is NULL) and data!='' and isharvested='n')) metadata on ".
                            "metadata.uuid = statistique_liste_couche_publiees.\"uuid metadata\"".
                            ($pk_dom!=-1 ? "left join couche_sdom on couche_sdom.pk_couche_donnees = statistique_liste_couche_publiees.pk_couche_donnees " : "").
                            'where "couche vecteur ?" =-1'.
                            ($pk_dom!=-1 ? "and pk_domaine =".$pk_dom: "").
                            'order by createdate desc limit 20;');
                            break;
                    case "services" :
                        $oSyndWriter->setTitle("Nouvelles cartes du catalogue");
                        $rs = $dao->BuildResultSet('SELECT statistique_liste_carte_publiees."nom de la carte" AS nom, statistique_liste_carte_publiees."résumé",'.
                            'statistique_liste_carte_publiees.changedate AS date, statistique_liste_carte_publiees.createdate, '.
                            'statistique_liste_carte_publiees."uuid metadata" AS id, image  FROM statistique_liste_carte_publiees '.
                            " left join (select uuid, xpath('//gmd:graphicOverview/gmd:MD_BrowseGraphic/gmd:fileName/gco:CharacterString/text()',".
                            "('<?xml version=\"1.0\" encoding=\"utf-8\"?>' || data)::xml,".
                            " ARRAY[ARRAY['gmd','http://www.isotc211.org/2005/gmd'],".
                            " ARRAY['gco','http://www.isotc211.org/2005/gco']])::text as image from public.metadata where ((not data is NULL) and data!='' and isharvested='n')) metadata on ".
                            "metadata.uuid = statistique_liste_carte_publiees.\"uuid metadata\"".
                            ($pk_dom!=-1 ? " left join cartes_sdom on cartes_sdom.pk_carte_projet 	 = statistique_liste_carte_publiees.pk_carte_projet  " : "").
                            ($pk_dom!=-1 ? "and pk_domaine =".$pk_dom: "").
                            'order by createdate desc limit 20');
                            break;
                    default:
                        $oSyndWriter->setTitle("Nouveautés du catalogue");
                        $rs = $dao->BuildResultSet("SELECT \"nom\", \"résumé\", \"data_type\", \"createdate\", \"id\", image from statistique_dernieres_nouveautes".
                            " left join (select uuid, xpath('//gmd:graphicOverview/gmd:MD_BrowseGraphic/gmd:fileName/gco:CharacterString/text()',".
                            "('<?xml version=\"1.0\" encoding=\"utf-8\"?>' || data)::xml,".
                            " ARRAY[ARRAY['gmd','http://www.isotc211.org/2005/gmd'],".
                            " ARRAY['gco','http://www.isotc211.org/2005/gco']])::text as image from public.metadata where ((not data is NULL) and data!='' and isharvested='n')) metadata on ".
                            "metadata.uuid = statistique_dernieres_nouveautes.id");

                        break;
                }
            }
            for($rs->First(); !$rs->EOF(); $rs->Next()) {
                $resume = $rs->Read(1);
                $resume = htmlspecialchars($resume, ENT_QUOTES, "UTF-8");
                $newItem = $oSyndWriter->createNewItem();
                $title = $rs->Read(0);
                $title = htmlspecialchars($title, ENT_QUOTES, "UTF-8");
                $newItem->setTitle($title);
                $image = str_replace(array("{", "}"), "", $rs->Read(5));
                $tabImage = explode(",", $image);
                $image = "";
                $pattern = "/\.[png|gif|jpg|jpeg]/";
                foreach($tabImage as $image_name) {
                    if(preg_match($pattern, $image_name)){
                        $image_name = str_replace('"', '', $image_name);
                        $tabParseUrl = parse_url($image_name);
                        //if($tabParseUrl && isset($tabParseUrl["scheme"]) && ($tabParseUrl["scheme"] == "http"))
                            $image = $image_name;
                        /*    else
                                $image = PRO_GEONETWORK_URLBASE."/srv/fre/resources.get?uuid=".$rs->Read(4)."&fname=".$image_name."&access=public";*/
                        break;
                    }
                }
                if($image != "")
                    $newItem->setEncloser($image, @filesize($image), "image/png");
                    $newItem->setDescription($resume);
                    //$newItem->setLink((defined("PRO_PRINCIPAL_URLBASE")? PRO_PRINCIPAL_URLBASE : PRO_CATALOGUE_URLBASE)."?uuid=".$rs->Read(4));
                    $newItem->setLink(PRO_GEONETWORK_URLBASE."?uuid=".$rs->Read(4));
                    $newItem->setDate($rs->Read(3), '%Y-%m-%eT%H:%M:%S');
                    $oSyndWriter->addItem($newItem);
            }
            $oSyndWriter->genarate();
            exit;
        }  //end if($dao)
    }
}