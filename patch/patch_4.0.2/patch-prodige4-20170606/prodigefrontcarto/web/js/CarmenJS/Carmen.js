

/**
 * Inspired from OpenLayers main loader class
 */ 
(function() {
    
    var singleFile = (typeof Carmen == "object" && Carmen.singleFile);
    
    /**
     * Namespace: Carmen
     */
    window.Carmen = {
        
        /**
         * Property: _scriptName
         * {String} Relative path of this script.
         */
        _scriptName: "Carmen.js",

        /**
         * Function: _getScriptLocation
         * Return the path to this script.
         *
         * Returns:
         * {String} Path to this script
         */
        _getScriptLocation: function () {
            var scriptLocation = "";
            var scriptName = Carmen._scriptName;
         
            var scripts = document.getElementsByTagName('script');
            for (var i=0, len=scripts.length; i<len; i++) {
                var src = scripts[i].getAttribute('src');
                if (src) {
                    var index = src.lastIndexOf(scriptName); 
                    // set path length for src up to a query string
                    var pathLength = src.lastIndexOf('?');
                    if (pathLength < 0) {
                        pathLength = src.length;
                    }
                    // is it found, at the end of the URL?
                    if ((index > -1) && (index + scriptName.length == pathLength)) {
                        scriptLocation = src.slice(0, pathLength - scriptName.length);
                        break;
                    }
                }
            }
            return scriptLocation;
         }
    };
  
    /**
     * 
     * When we *are* part of a SFL build we do not dynamically include the 
     * Carmen library code as it will be appended at the end of this file.
     */
 
    var geopalLoader = function(){
      /**
       * Namespace: Geoportal
       * The Geoportal API based on OpenLayers 2.12.
       *      Aims at providing fullfledge OpenLayers and Geoportail's
       *      capabilities.
       * The Geoportal object provides a namespace for all things Geoportal.
       * The Geoportal extended API provides all OpenLayers, Proj4JS and
       * Geoportal features.
       */
      window.Geoportal= {};
    	var geoportal = [
        "Geoportal/Format/XLS.js",
        "Geoportal/OLS.js",
        "Geoportal/OLS/XLS.js",
        "Geoportal/OLS/AbstractHeader.js",
        "Geoportal/OLS/Error.js",
        "Geoportal/OLS/ErrorList.js",
        "Geoportal/OLS/ResponseHeader.js",
        "Geoportal/OLS/AbstractBody.js",
        "Geoportal/OLS/AbstractResponseParameters.js",
        "Geoportal/OLS/Response.js",
        "Geoportal/Format/XLS/v1_1.js",
        "Geoportal/OLS/AbstractRequestParameters.js",
        "Geoportal/OLS/LUS.js",
        "Geoportal/OLS/AbstractLocation.js",
        "Geoportal/OLS/AbstractAddress.js",
        "Geoportal/OLS/AbstractStreetLocator.js",
        "Geoportal/OLS/Street.js",
        "Geoportal/OLS/Building.js",
        "Geoportal/OLS/StreetAddress.js",
        "Geoportal/OLS/Place.js",
        "Geoportal/OLS/PostalCode.js",
        "Geoportal/OLS/Address.js",
        "Geoportal/OLS/LUS/GeocodeRequest.js",
        "Geoportal/OLS/GeocodeMatchCode.js",
        "Geoportal/OLS/LUS/GeocodedAddress.js",
        "Geoportal/OLS/LUS/GeocodeResponseList.js",
        "Geoportal/OLS/LUS/GeocodeResponse.js",
        "Geoportal/OLS/AbstractPosition.js",
        "Geoportal/OLS/UOM.js",
        "Geoportal/OLS/UOM/AbstractMeasure.js",
        "Geoportal/OLS/UOM/Distance.js",
        "Geoportal/OLS/UOM/Angle.js",
        "Geoportal/OLS/HorizontalAcc.js",
        "Geoportal/OLS/VerticalAcc.js",
        "Geoportal/OLS/QualityOfPosition.js",
        "Geoportal/OLS/UOM/TimeStamp.js",
        "Geoportal/OLS/UOM/Time.js",
        "Geoportal/OLS/UOM/Speed.js",
        "Geoportal/OLS/Position.js",
        "Geoportal/OLS/LUS/ReverseGeocodePreference.js",
        "Geoportal/OLS/LUS/ReverseGeocodeRequest.js",
        "Geoportal/OLS/LUS/SearchCentreDistance.js",
        "Geoportal/OLS/LUS/ReverseGeocodedLocation.js",
        "Geoportal/OLS/LUS/ReverseGeocodeResponse.js",
        
        "Geoportal/Format/XLS/v1_1/LocationUtilityService.js",
        "Geoportal/Format/XLS/v1_2.js",
        "Geoportal/Format/XLS/v1_2/LocationUtilityService.js"
      ];
      var srcs = [];
      for(var i=0; i<geoportal.length; i++){
      	srcs.push('Carmen/'+geoportal[i]);
      }
      return srcs;
    };
    
    if(CARMEN_DEBUG && !singleFile) {
      //alert("Mode Debug (this alert enables firefox2 to fully load scripts...)");
    var jsfiles = [
        "Carmen/Application.js",
        "consultation2.js",
        "WMSGetFeatureInfo.js"
        
    ].concat(geopalLoader(), [

        "Carmen/Ext/RowLayout.js",
        "Carmen/Ext/BrowseButton.js",
        //"Carmen/Ext/FileUploadField.js",
        "Carmen/Ext/SliderTip.js",
        "Carmen/Ext/CompositeField.js",
        "Carmen/Ext/ColorChooser.js",
        "Carmen/Ext/Selector.js",
        "Carmen/Ext/IFrame.js",
        "Carmen/Util.js",
        "Carmen/Layer.js",
        "Carmen/Map.js",
        "Carmen/Handler/Circle.js",
        "Carmen/Control.js",
        "Carmen/Control/AdvancedAnnotation.js",
        "Carmen/Control/AreaExtractor.js",
        "Carmen/Control/AttributionCarmen.js",
        "Carmen/Control/AddOGC.js",
        "Carmen/Control/Buffer.js",
        "Carmen/Control/ContextManager.js",
        "Carmen/Control/EditorManager.js",
        "Carmen/Control/Coordinate.js",
        "Carmen/Control/FavoriteAreas.js",
        "Carmen/Control/Info.js",
        "Carmen/Control/InfoCircle.js",
        "Carmen/Control/InfoPolygon.js",
        "Carmen/Control/InfoPolyline.js",
        "Carmen/Control/InfoURL.js",
        "Carmen/Control/Inlay.js",
        "Carmen/Control/LayerTreeManager.js",
        "Carmen/Control/Legend.js",
        "Carmen/Control/LocateByAddress.js",
        "Carmen/Control/Measure.js",
        "Carmen/Control/PanArrows.js",
        "Carmen/Control/QueryAttributes.js",
        "Carmen/Control/QuerySpatial.js",
        "Carmen/Control/AdvancedQueryAttributes.js",
        "Carmen/Control/MapserverScale.js",
        "Carmen/Control/ExportImg.js",
        "Carmen/Control/ExportPdf.js",
        "Carmen/Control/ExportCarte.js", //hismail
        "Carmen/Control/Print.js",
        "Carmen/Control/ReferenceMap.js",
        "Carmen/Control/Refresh.js",
        "Carmen/Control/ScaleChooser.js",
        "Carmen/Control/ToolTip.js",
        "Carmen/Control/ZoomHistory.js",        
        "Carmen/Control/ZoomToPlace.js",
        "Carmen/Control/ZoomToPlaceFromDB.js",
        "Carmen/Layer/MapServerGroup.js",
        "Carmen/Layer/WMSGroup.js",
        "Carmen/Layer/WMTSGroup.js",
        "Carmen/Layer/Vector.js",
        //"Carmen/Ext/TreeNodeUI.js",
        //"Carmen/Ext/TreeNodeInfoUI.js",
        "Carmen/Ext/ApplicationUI.js",
        "Carmen/Control/EditorManager2.js"
    ]);
    //On recupere la valeur de la variable en base PRO_ALLOWED_LOCAL_DATA_VISUALISATION
    //pour savoir si l'utilisateur est authorisé à visualiser ses fichiers cartographiques
    for (var epsg in tabEPSG)
      jsfiles.push("Carmen/proj4Defs/EPSG"+epsg+".js");
    var docWrite = true;
    if(docWrite) {
      var allScriptTags = new Array(jsfiles.length);
    }
    var host = Carmen._getScriptLocation();    
    for (var i=0, len=jsfiles.length; i<len; i++) {
      if (docWrite) {
        allScriptTags[i] = "<script src='" + host + jsfiles[i] + "'></script>"; 
      } else {
          var s = document.createElement("script");
          s.src = host + jsfiles[i];
          var h = document.getElementsByTagName("head").length ? 
                           document.getElementsByTagName("head")[0] : 
                           document.body;
          h.appendChild(s);
      }
    }
    if (docWrite) {
      document.write(allScriptTags.join(""));
    }  
  }
 
  }
)();


/**
 * Constant: VERSION_NUMBER
 */
Carmen.VERSION_NUMBER="0.1beta";

