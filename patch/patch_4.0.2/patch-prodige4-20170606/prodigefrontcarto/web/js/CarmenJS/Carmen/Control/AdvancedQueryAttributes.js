/**
 * @_requires OpenLayers/Controljs
 */

/**
 * Class: Carmen.Control.AdvancedQueryAttributes
 * 
 * Inherits from: - <OpenLayers.Control>
*/
var AQA_patternField = '(\\S+)\\s+';
var AQA_patternValue = '\\s*[\\s"\']?([^\\s"]+)[\\s"\']?';
Carmen.Control.AdvancedQueryAttributes = new OpenLayers.Class(OpenLayers.Control, {

  operatorMap :  [[ AQA_patternField+'COMMENCE\\s+PAR'+AQA_patternValue       , "[$1] ~ '$2.*'"        , 'COMMENCE PAR' ,'commence par ' , '\'$1\' =~ \/\^$2\/'],
                  [ AQA_patternField+'NE\\s+CONTIENT\\s+PAS'+AQA_patternValue , "not ([$1] ~ '.*$2.*')" , 'NE CONTIENT PAS' ,'ne contient pas ' , 'NOT\(\'$1\' =~ \/\^$2\/\)'],
                  [ AQA_patternField+'CONTIENT'+AQA_patternValue              , "[$1] ~ '.*$2.*'"      , 'CONTIENT' ,'contient ' , '\'$1\' =~ \/\$2\/'],
                  [ AQA_patternField+'\\=' , '[$1] =' , '=' ,'est égal (=)' , '$1 ='],
                  [ AQA_patternField+'<>' , '[$1]1 <>' , '<>' ,'est différent (<>)' , '$1 !='],
                  [ AQA_patternField+'>\\=' , '[$1] >=' , '>=' ,'supérieur ou égal (>=)' , '$1 >='],
                  [ AQA_patternField+'>' , '[$1] >' , '>' ,'strictement supérieur (>)' , '$1 >'],
                  [ AQA_patternField+'<\\=' , '[$1] <=' , '<=' ,'inférieur ou égal (<=)' , '$1 <=' ],
                  [ AQA_patternField+'<' , '[$1] <' , '<' ,'strictement inférieur (<)' , '$1 <']],
		              
  logicalOperatorMap  : [[ 'AND', 'ET', 'ET', '&&' ],
                         [ 'OR', 'OU', 'OU', '||' ],
                         [ 'NOT', 'NON', 'NON', 'NOT' ],
                         [ '(', '(', '(', '(' ],
                         [ ')', ')', ')', ')' ]],   

  // UI components
  btn: Ext.create('Ext.Button',{
      tooltip: 'Requêtes attributaires',      
      tooltipType: 'title',
      cls: 'x-btn-icon', /* cmn-tool-requete',*/
      enableToggle: true,
      toggleGroup: 'mainMapControl',
      disabled: false,
      text : '<i class="fa fa-binoculars fa-2x"></i>',
      width: ctrlWidth,
      height: ctrlHeight
  }),



  win : Ext.create('Ext.Window', {
    layout:'anchor',
    //width: 682,
    //height: 300,
    constrain : true,
    bodyPadding: 10,
    //bodyStyle : 'max-height:500px',
    //plain: true,
    title: 'Requêtes attributaires',
    autoDestroy :true,
    resizable:true,
    closeAction: 'hide'
    //shadow : false
  }),
  
  winInfo : Ext.create('Ext.Window', {
	    layout:'anchor',
	    title: 'Information',
	    width:400,
	    height: 400,
      constrain : true,
	    plain: true,
	    title: '',
	    modal:false,
	    autoDestroy :true,
	    resizable:true,
	    closeAction: 'hide',
	    shadow : false,
      layout: 'accordion'
  }),
  
  comboLayer : null,
  comboField : null,
  comboOperator : null,
  comboLogical : null,
  valueField : null,
  valueBtn : null,
  requestArea : null,
  requestPanel : null,
  requestTimeout : 360000,
  fullExtent : null,
   
  resizeComboToFitContent : function(combo){
    if (combo.el){
      if (!combo.elMetrics)
      {
        combo.elMetrics = Ext.util.TextMetrics.create(combo.el);
      }
      var m = combo.elMetrics, width = 0, el = combo.el, s = combo.getSize();
      combo.store.each(function (r) {
        var text = r.get(combo.displayField);
        width = Math.max(width, m.getWidth(text));
      }, combo);
      if (el) {
        width += el.getBorderWidth('lr');
        width += el.getPadding('lr');
      }
      if (combo.trigger) {
        width += combo.trigger.getWidth();
      }
      s.width = width;
      combo.setSize(s);
      /*combo.store.on({
          'datachange': this.resizeComboToFitContent,
          'add': this.resizeComboToFitContent,
          'remove': this.resizeComboToFitContent,
          'load': this.resizeComboToFitContent,
          'update': this.resizeComboToFitContent,
          buffer: 10,
          scope: combo
        });*/
    }
  },
  
  
  resizePanelToFitContent : function(panel){
    
  },

  gridInfoFormPanel : Ext.create('Ext.form.Panel', {
      id: 'CmnControlAdvancedQueryAttributes' +'_' + 'gridInfoFormPanel',
      hidden : true,
      fileUpload : true,
      waitTitle : "Traitement en cours..."
  }),

  
  /* Properties */ 

  // data of the layer combo box
  layerData : [], 
  // array of data of the field combo box
  fieldData : [], 
  
  currentFieldData : [],
  // array of layer field desc objets (one sub aray per layer)
  layerFieldsDesc : [], 
  // reference to the layerTreeManager of the app
  layerTreeManager : null,
  // reference to the layerTree of queryable layer
  layerTree : null,
  // mapfile of the app
  mapfile : null,
  // url of the queryAttributes service 
  serviceUrl : null,
  
  //variables de mémorisation
  comboFieldValue : "",
  opFieldValue : "",
  valueFieldValue : "",
   

  // methods
  initialize: function(serviceUrl, mapfile, options) {
    OpenLayers.Control.prototype.initialize.apply(this, [options]);    
    
    this.mapfile = mapfile;
    this.serviceurl = serviceUrl;

    this.initializeUIComponents();
  },




  initializeUIComponents : function() {
    // create the Grid
    /*this.gridInfoPanel = Ext.create('Ext.grid.Panel', {
      id : 'gridInfoPanel',
      store: new Ext.data.ArrayStore({
        fields: [],
        data: []
      }),
      columns: [],
      title:'Résultats sélectionnés',
      hidden: true,
      autoScroll: true,
      fitToFrame: true,
      border: false,
      iconCls : 'cmnInfoGridZoomIcon',
      tools:[{
        id:'xls',
        qtip: 'Export de la sélection au format Excel',
        scope : this
      },
      {
        id:'csv',
        qtip: 'Export de la sélection au format CSV'
      }]
    });*/      
   this.comboLayer = Ext.create('Ext.form.ComboBox',{   
        xtype: 'combo',
        hideLabels:false,
        name: 'layer',
        id: 'comboLayerAdvanced',
        fieldLabel: 'Couche',
        queryMode: 'local',
        editable: false,
        listConfig : {
          loadingText: 'Chargement',
          getInnerTpl: function() {return '<tpl for="."><div ext:qtip="{layerTitle}" class="x-combo-list-item">{layerTitle}</div></tpl>';},
          minWidth : 150
        },
        emptyText: 'Sélectionnez une couche',
        triggerAction: 'all',
        minChars: 1,
        queryDelay: 250,     
        store: new Ext.data.SimpleStore({
          fields: ['layerName', 'layerTitle'],
          data : []}
        ),
        validator: function(v) {
           return (v!='Sélectionnez une couche');
          },   
        displayField: 'layerTitle',
        valueField: 'layerName',
        width: 400          
      });
    
    this.comboField = Ext.create('Ext.form.ComboBox', { 
     	  xtype: 'combo',
        name: 'qitem',
        id: 'comboFieldAdvanced',
        fieldLabel: 'Champ',
        queryMode: 'local',
        editable: false,
        listConfig: {
          loadingText: 'Chargement',
          getInnerTpl: function() { return '<tpl for="."><div ext:qtip="{label}" class="x-combo-list-item">{label}</div></tpl>';}
        },
        emptyText: 'Sélectionnez',
        triggerAction: 'all',

        queryDelay: 250,     
        disabled : true,
        store: new Ext.data.SimpleStore({
          fields: ['value', 'labelRequest', 'label'],
          data : []}
        ),/*   
        validator: function(v) {
        	 return (v!='Sélectionnez un champ');
          },*/
        displayField: 'label',
        valueField: 'value',
        width: 400,
        style: {
          width: '400px'
        }
      });
      
    this.comboOperator = Ext.create('Ext.form.ComboBox', {
        xtype: 'combo',
        id: 'comboOperatorAdvanced',
        name: 'qstring',
        fieldLabel: 'Opérateur',
        editable: false,
        disabled : true,
        listConfig : {
          loadingText: 'Chargement',
          repeatTriggerClick : true
        },
        emptyText: 'Sélectionnez',
        triggerAction: 'all',
        queryDelay: 250,
        queryMode: 'local',
        store: new Ext.data.ArrayStore({
  	        fields: ['pattern','replacement', 'labelRequest', 'label'],
  	        data : this.operatorMap}
  	    ),
        displayField: 'label',
        valueField: 'labelRequest',
        width: 400
      });
      
    this.valueField = Ext.create('Ext.form.TextField',{
      	xtype: 'textfield',
      	name: 'value',
        id: 'fieldValueAdvanced',    		
      	fieldLabel: 'Valeur',
        disabled : true,
      	mode: 'local',
      	allowBlank: false,
      	width: 270,
      	margin: '0 5 0 0'
      });
    
    this.valueBtn = Ext.create('Ext.Button', {
      tooltip: 'Ajouter la valeur dans la requête',
      tooltipType: 'title',
      //cls: 'x-btn-icon cmn-tool-addValue',
      //defaultRenderer: Carmen.Control.AddOGC.buildActionRender('fa fa-fw fa-file-o fa-1x', 'color: #000; font-size: 16px;'),
      xtype: 'button',
      cls: 'x-btn-icon',
      text : '<i class="fa fa-level-down"></i>',
      //scale: 'medium',
      listeners : {
        'click' : {
           fn : function(b,evt) {
             this.addValueToRequest(this.valueField.getValue());
             //mémorisation 
             this.valueFieldValue = this.valueField.getValue();
            },
           scope : this
        }
      }//,
      //width: 16,
      //height: 16
  	}); 
    
    
    this.listvalueBtn = Ext.create('Ext.Button', {
        tooltip: 'Lister les valeurs possibles',
        tooltipType: 'title',
        //cls: 'x-btn-icon cmn-tool-addValue',
        //defaultRenderer: Carmen.Control.AddOGC.buildActionRender('fa fa-fw fa-file-o fa-1x', 'color: #000; font-size: 16px;'),
        xtype: 'button',
        cls: 'x-btn-icon',
        text : '<i class="fa fa-search"></i>',
        //scale: 'medium',
        listeners : {
          'click' : {
             fn : function(b,evt) {
               this.doValueList(this.valueField.getValue());

              },
             scope : this
          }
        }//,
    	});    
    
    this.comboPossibleValue = Ext.create('Ext.form.ComboBox',{   
        xtype: 'combo',
        hideLabels:true,
        name: 'comboPossibleValue',
        id: 'comboPossibleValue',
        fieldLabel: 'Valeur',
        queryMode: 'local',
        hidden: true,
        editable: false,
        listConfig : {
          loadingText: 'Chargement',
          getInnerTpl: function() {return '<tpl for="."><div ext:qtip="{valeurTitle}" class="x-combo-list-item">{valeurTitle}</div></tpl>';},
          minWidth : 150
        },
        emptyText: 'Sélectionnez une valeur',
        triggerAction: 'all',
        
        queryDelay: 240,     
        store: new Ext.data.ArrayStore({
          fields: ['valeurName', 'valeurTitle'],
          data : [],
          idProperty : 'valeurName'
        }),
                    
        displayField: 'valeurTitle',
        valueField: 'valeurName',
        width: 240          

    	});

    
    this.comboLogical = Ext.create('Ext.form.ComboBox',{
        xtype: 'combo',
        id: 'logicalOperatorAdvanced',
        name: 'logicalOperator',
        fieldLabel: 'Opérateur logique',
        editable: false,
        disabled : true,
        listConfig: {
          loadingText: 'Chargement'
        },
        emptyText: 'Sélectionnez',
        triggerAction: 'all',
        minChars: 1,
        queryDelay: 250,     
        queryMode: 'local',
        store: new Ext.data.ArrayStore({
  	        fields: ['value', 'labelRequest' , 'label'],
  	        data : this.logicalOperatorMap          
  	        }
  	    ),
        displayField: 'label',
        valueField: 'labelRequest',
        width: 400
        
      });
    
    this.requestArea = Ext.create('Ext.form.TextArea',{
        xtype: 'textarea',
        id: 'whereClauseAdvanced',
        name: 'whereClause',
        fieldLabel: 'Requête',
        disabled : true,
        hideLabel : true,
        editable: true,
        emptyText: 'Saisissez votre requête ou construisez la au moyen de l\'interface...',
        width: 400
      });

    this.valueLineLimit = Ext.create('Ext.form.field.Number',{
      	xtype: 'numberfield',
      	name: 'value',
        id: 'fieldLineLimit', 
      	fieldLabel: 'Limiter le nombre de lignes à :',
        disabled : true,
      	mode: 'local',
      	allowBlank: true,
      	width: 270,
      	margin: '0 5 0 0'
      });
    
    this.lineLimitCB = Ext.create('Ext.form.Checkbox', {
        tooltip: 'Activer le nombre limite de résultats',
        tooltipType: 'title',
        xtype: 'checkbox',
        id : 'CBLimit',
        disabled : true,
        listeners : {
        	change : {
             fn : function(b,evt) { 
            	 this.activateNbLimit();
              },
             scope : this
          }
        }
    	}); 

    this.requestPanel = Ext.create('Ext.form.Panel',{
      id: 'requestPanelAdvanced',
      autoScroll: true,
      fitToFrame: true,
      defaults: {
        hideLabels:false
      },
      items: [{
        items : [this.comboLayer]
      }
      ,
      {
      xtype: 'fieldset',
      id: 'fieldsetRequest',
      title: 'Paramètres de la requête',
      items: [
        
        {
         
          items : [this.comboField]
        },{
         
          items : [this.comboOperator]
        },{
         
              
            xtype: 'fieldcontainer',
            fieldLabel: 'Valeur',
            disabled :true,
            id : 'valueFieldContainer',
            combineErrors: true,
            msgTarget : 'side',
            layout: 'hbox',
            defaults: {
                flex: 1,
                hideLabel: true
            },
            items: [
      this.valueField, this.comboPossibleValue, this.valueBtn, this.listvalueBtn ]
          //}]
        },{
        	
          //layout : 'form',
          items : [this.comboLogical]
        }
          //layout : 'form',
         // colspan: 2,  
       
        ]
        }    
        ,{
          xtype: 'fieldset',
          id: 'fieldsetSqlArea',
          title: 'Expression',
          items : [this.requestArea,
                   {
              xtype: 'fieldcontainer',
              fieldLabel: 'Limiter le nombre de lignes à',
              //disabled :false,
              id : 'limitContainer',
              combineErrors: true,
              msgTarget : 'side',
              layout: 'hbox',
              defaults: {
                  flex: 1,
                  hideLabel: true
              },
              items: [this.lineLimitCB, this.valueLineLimit]
                   }          
        ]
        }    
      ]
      ,
      
      buttons: [{
              id: 'requestSubmitAdvanced',
              text: 'Interroger'
          }, 
          {
              id: 'reinitializeSubmit',
              text: 'Réinitialiser'
          }]
    });
  },



  setMap: function(map) {
    OpenLayers.Control.prototype.setMap.apply(this, arguments);
        
    this.btn.addListener('toggle', Carmen.Util.buildExt2olHandlerToggle(this));
    
    this.map.app.ui.addToToolPanel(this.btn, this.toolbarTarget, this.toolbarPos);
    
    
    // adding listeners to link window closing and control deactivation   
    var control = this;
    /*this.win.on(
      'hide',
      function(w) { control.btn.toggle(false); },
      this.win);*/
    
    var comboLO = Ext.getCmp('logicalOperatorAdvanced');
    comboLO.on({
      'select' : {
        fn : function (){
          this.addToRequest(comboLO.getValue());
          Ext.getCmp('logicalOperatorAdvanced').clearValue();
          },
        scope : this
      }
    });
    
    var comboO = Ext.getCmp('comboOperatorAdvanced');
    comboO.on({
      'select' : {
        fn : function(){
          this.opFieldValue = comboO.getValue();
          this.addToRequest(this.opFieldValue);
          //enable next fields
          var combofieldValue = Ext.getCmp('fieldValueAdvanced');
          combofieldValue.enable();
          var combofieldValueContainer = Ext.getCmp('valueFieldContainer');
          combofieldValueContainer.enable();
          //mémorisation du champ sélectionné
          //effacement
          Ext.getCmp('comboOperatorAdvanced').clearValue();
        },
        scope : this
      }
    });
    
    var comboField = Ext.getCmp('comboFieldAdvanced');
    comboField.on({
      'select' : {
        fn : function(){
        	this.comboFieldValue = comboField.getValue();
          this.addToRequest(this.comboFieldValue);
          //this.resizeComboToFitContent(comboField);
          //enable next fields
          var comboO = Ext.getCmp('comboOperatorAdvanced');
          var comboLO = Ext.getCmp('logicalOperatorAdvanced');
          comboO.enable();
          comboLO.enable();   
          //mémorisation du champ sélectionné
          //effacement
          Ext.getCmp('comboFieldAdvanced').clearValue();
        },
        scope : this
      }
    });

    
    var comboPossibleValue = Ext.getCmp('comboPossibleValue');
    comboPossibleValue.on({
      'select' : {
        fn : function(){
          //this.addToRequest(comboPossibleValue);
          this.updateToField(comboPossibleValue);
        },
        scope : this
      }
    });
    this.map.app.ui.doLayout();    
  },

  
  activate: function() {
  	if (this.getLayerTreeManager==null) {
      Ext.MessageBox.alert("Warning", "The control " + 
       this. CLASS_NAME + 
       " requires the LayerTreeManager control to work.");
      return null;
    }
    OpenLayers.Control.prototype.activate.apply(this, arguments);
    this.map.toolTipDeactivate();
    this.showWindow();    
  },
  
  deactivate: function() {
    // removing highlighting...
    this.map.clearSelection();
    OpenLayers.Control.prototype.deactivate.apply(this, arguments);
    if (this.win.rendered)
      this.win.hide();
    this.map.toolTipActivate();  	
  },


  addToRequest : function(contents) {
    var requestAreaStr = this.requestArea.getValue();
    requestAreaStr += ' ' + contents;
    this.requestArea.setValue(String(requestAreaStr).trim()); 
  },
   
  addValueToRequest : function(str) {
    var requestAreaStr = this.requestArea.getValue();
    requestAreaStr += ' "' + str + '"';      
    this.requestArea.setValue(String(requestAreaStr).trim()); 
  },
  
  doValueList : function(str) {
	  var control = this;   
  	var comboPossibleValue = Ext.getCmp('comboPossibleValue');
  	var fieldValueAdvanced = Ext.getCmp('fieldValueAdvanced');  	
  	var queryParams;
  	var idField = -1;
  	
  	comboPossibleValue.mask();
  	// requ ajax pour obtenir la liste des valeurs possibles et maj la liste des val
    if(false && str!="")
      queryParams = control.buildParams(control.layerFieldsDesc, this.comboFieldValue+'::text ilike \'%'+str+'%\'', this.comboFieldValue+'::text ilike \'%'+str+'%\'', false)
    else
      queryParams = control.buildParams(control.layerFieldsDesc, '1=1', '1=1', true)
       		
    var nbLimitResultats = 100;
  	var valueField = this.comboFieldValue;
  	queryParams.valueLineLimit = queryParams.valueLineLimit || nbLimitResultats;
  	// req ajax pour mettre a jour la liste des valeurs possibles
  	Ext.Ajax.request({
        url: Routing.generate('frontcarto_query_advancedqueryattributes'),
        params: queryParams,
        //PRODIGE40 '/services/AdvancedQueryAttributes/index.php?',
        //timeout: this.requestTimeout,
        success: function(response) { 
        	response = Ext.decode(response.responseText); 
        	
        	if (response.totalCount > 0) {
          	  for(lname in response.results)  {       	    		  
          		  for(fieldsKey in response.results[lname].fields){
		  			      if(response.results[lname].fields[fieldsKey]==valueField){ 	    				  
          				  idField = fieldsKey;// id du chmp recherche dans la combo "champs"
          			  }	  
          		  }   	        
          		  if(idField!=-1){// rq. si idField!=-1 : il y a au moins un resultat        			  
                  var Tab=new Array();         	
                  var readed = {};
                  
                  if(response.results[lname].data.length>=nbLimitResultats){
                    comboPossibleValue.emptyText = 'Liste limitée aux '+nbLimitResultats+' premières val.';
                  
                  }else{
                      comboPossibleValue.emptyText = 'Sélectionnez';
                  }
                  for(var dataId=0; dataId<Math.min(response.results[lname].data.length, nbLimitResultats); dataId++){
                  	if ( readed[response.results[lname].data[dataId][idField] ] ) continue;
                    Tab.push([response.results[lname].data[dataId][idField], response.results[lname].data[dataId][idField]]);
                    readed[response.results[lname].data[dataId][idField]] = true;
                  }  				
                  comboPossibleValue.getStore().removeAll();
                  comboPossibleValue.getStore().loadData(Tab);  
                  comboPossibleValue.applyEmptyText();
                }else{
                  comboPossibleValue.getStore().removeAll();
                  comboPossibleValue.emptyText = 'Aucune valeur';
                  comboPossibleValue.applyEmptyText();
                }
          	  }	        	
          }else{
        	  comboPossibleValue.getStore().removeAll();
            comboPossibleValue.emptyText = 'Aucune valeur';
            comboPossibleValue.applyEmptyText();
          }	
          comboPossibleValue.unmask();
        },
        failure: function (response) {
        	response = Ext.decode(response.responseText); 	
        	Carmen.Util.handleAjaxFailure(response, "Requêtes attributaires"); 
          comboPossibleValue.unmask();
        }
    });
  	if(fieldValueAdvanced.isVisible()){
  		comboPossibleValue.show(); 
  		fieldValueAdvanced.hide();
      this.listvalueBtn.setText('<i class="fa fa-edit"></i>');
  	}else{
  		comboPossibleValue.setValue('');
  		comboPossibleValue.hide(); 
  		fieldValueAdvanced.show();
      this.listvalueBtn.setText('<i class="fa fa-search"></i>');
  	} 	
  },

  
  updateToField : function(c, r, i) {
	    var valueFieldStr = this.valueField.getValue();
	    var valueField = Ext.getCmp('valueField');
	    
	    if(valueFieldStr = valueFieldStr.length>0){
	    	this.valueField.setValue('');
	    } 
	    this.valueField.setValue(c.getValue()); 
  },
  
  activateNbLimit : function() {	   
  	var CBLimit = Ext.getCmp('CBLimit');	
  	var fieldLineLimit = Ext.getCmp('fieldLineLimit');  	
  	if(CBLimit.getValue()==true){
  		fieldLineLimit.enable();
  	}else{
  		fieldLineLimit.reset();//let enabled  
  		fieldLineLimit.disable();
  	}	  
  },
  
  rewriteRequest : function(str) {
 // rewriting request according to SQL Syntax and field alias names...
    // protecting space in string
    str = str.replace(/\"([^\"]+)\"/g, function(x) { return x.replace(/\s/g,'^^').replace(/'/g,'@@');});

    // rewriting field alias with field names
    for (var i=0; i<this.currentFieldData.length; i++) {
      var pattern = new RegExp('\s*' + this.escapeSpecialchars(this.currentFieldData[i][1]) + '\s*','g');
      str = str.replace(pattern, this.currentFieldData[i][0]);
    }
    // rewriting logical operator
    for (var i=0; i<this.logicalOperatorMap.length; i++) {
      var pattern = new RegExp('\\s+' + this.escapeSpecialchars(this.logicalOperatorMap[i][1]) + '\\s+','g');
      str = str.replace(pattern, ' ' + this.logicalOperatorMap[i][0] + ' ');
      
    }

    var found = false;
    // rewriting operator rules...
    for(var i = 0; i < this.operatorMap.length; i++){
      var pattern = new RegExp(this.operatorMap[i][0], 'g');
      var patternStr = this.operatorMap[i][2];
      if(str.search(pattern) != -1){
        found = true;
        str = str.replace(pattern, this.operatorMap[i][1]);
      }
    }
    if ( !found ){
    	return null;
    }

    // rewriting field alias with field names
    for (var i=0; i<this.currentFieldData.length; i++) {
      var pattern = new RegExp('\s*' + this.escapeSpecialchars(this.currentFieldData[i][0]) + '\s*','g');

    // str = str.replace(pattern, ' CAST(' + this.currentFieldData[i][0] + ' AS integer) ');
    }
    //In case of string constraint, we remove (CAST ... ) sentence
    /*str = str.replace(/'\s*CAST\(/g, "");
    str = str.replace(/\s+AS\sinteger\)\s*'/g, "");*/



    str = str.replace(/\'\"/g, '\'');
    str = str.replace(/\"\'/g, '\'');
    str = str.replace(/%\"/g, '%');
    str = str.replace(/\"%/g, '%');
    str = str.replace(/\"/g, '\'');
    // removing space protection
    str = str.replace(/\^\^/g, ' ');
    str = str.replace(/@@/g, '\W');

    return str;
  }, 
  

  
  rewriteOgrRequest : function(str) {
	//Rewriting request according to OGR syntax
	str = str.replace(/\"([^\"]+)\"/g, function(x) { return x.replace(/\s/g,'^^').replace(/'/g,'@@');});
  str = str.replace(/\"/g, '\'');
	// rewriting field alias with field names
    for (var i=0; i<this.currentFieldData.length; i++) {
      var pattern = new RegExp('\\s*' + this.escapeSpecialchars(this.currentFieldData[i][0]) + '\\s*','g');
      str = str.replace(pattern, ' [' + this.currentFieldData[i][0] + '] ');
    }
	//Rewriting logical operator
	for(var i = 0; i < this.logicalOperatorMap.length; i++){
	  var pattern = new RegExp('\\s+' + this.escapeSpecialchars(this.logicalOperatorMap[i][1]) + '\\s+', 'g');
	  str = str.replace(pattern, ' ' + this.logicalOperatorMap[i][3] + ' ');
	}
	//Rewriting operator rules
	for(var i = 0; i < this.operatorMap.length; i++){
	  var pattern = new RegExp(this.operatorMap[i][0], 'g');
	  var patternStr = this.operatorMap[i][2];
	  if(str.search(pattern) != -1){
		if(patternStr == "COMMENCE PAR" || patternStr == "CONTIENT" || patternStr == "NE CONTIENT PAS"){
		  str = str.replace(pattern, this.operatorMap[i][4]);
		}else{
		  str = str.replace(pattern, this.operatorMap[i][4]);
		}
		break;
	  }
	}
  
  // removing space protection
  str = str.replace(/\^\^/g, ' ');
  str = str.replace(/@@/g, '\W');
	str = "(" + str + ")";
	
	return str;
  },

 escapeSpecialchars : function(str) {
   var specialChars=['(',')','<','>','=','[',']'];
   for (var i=0;i<specialChars.length;i++) {
     var pattern = new RegExp("\\" + specialChars[i],'g');
     str = str.replace(pattern,'\\' + specialChars[i]);
   }
   return str;
 },

  // displaying results
  showWindow : function() {
    var control = this;
    this.layerTree = this.getLayerTreeManager().getLayerTree().clone(this.transformNodeInfo);

    if (!this.win.rendered) {		
      this.win.add(this.requestPanel);
      
	    // adding listeners to the form
	    var submitBtn = Ext.getCmp('requestSubmitAdvanced');
	    submitBtn.on(
	      'click',
	      function(btn, evt) {
	    	  //if (this.comboLayer.getValue()=='' || this.requestArea.getValue()==this.requestArea.originalValue){
	    	if (this.comboLayer.getValue()== 'none'){        
	           Ext.Msg.alert('Requêtes attributaires',
              'Vous n\'avez pas saisi de requête. Veuillez former une requête ou sélectionner une couche.');
            return;
	        }	    	
	    	if (this.requestArea.getValue()!='' || this.requestArea.getValue()!=this.requestArea.originalValue ){// dans le cas où il y a une req attributaire   		
		        var qstring = this.rewriteRequest(this.requestArea.getValue());
		        var qOgrString = this.rewriteOgrRequest(this.requestArea.getValue());
		        
		        var form = control.requestPanel.getForm();
		        if ( qstring===null || !form.isValid() )
		          Ext.Msg.alert('Requêtes attributaires',
		            'La requête saisie n\'est pas valide.');
		        else {
		          control.performQuery(control.buildParams(control.layerFieldsDesc, qstring, qOgrString, false));
		        }
	    	}else{// cas où il y a uniquement une couche de sélectionnée
	    		control.performQuery(control.buildParams(control.layerFieldsDesc, '1=1', '1=1', true));	
	    	}   
	      }, 
	      this);
	    
	    var submitReinitializeBtn = Ext.getCmp('reinitializeSubmit');
	    submitReinitializeBtn.on(
	      'click',
	      function(btn, evt) {
	    	  	// loading data in combos
	    	  	var comboLayer = Ext.getCmp('comboLayerAdvanced');
	    	  	var comboField = Ext.getCmp('comboFieldAdvanced');
	    	  	var comboLO = Ext.getCmp('logicalOperatorAdvanced');
	    	  	var comboO = Ext.getCmp('comboOperatorAdvanced');
	    	  	var combofieldValue = Ext.getCmp('fieldValueAdvanced');
	    	    var fieldValueContainer = Ext.getCmp('valueFieldContainer');
	    	  	var textAreaWhereClause = Ext.getCmp('whereClauseAdvanced');
	    	  	var textAreaWhereClause = Ext.getCmp('whereClauseAdvanced');
	    	  	var CBLimit = Ext.getCmp('CBLimit');
	    	  	var fieldLineLimit = Ext.getCmp('fieldLineLimit');

	    	  	for(var i = 0; i < this.layerData.length; i++){
	    	  	  if(this.layerData[i][1].match(/<table.*<\/table>/)){
	    	        this.layerData[i][1] = this.layerData[i][1].replace(/<table.*<\/table>/, "");
	    	  	  }
	    	  	} 
	    	  	comboLayer.setValue('none')
    			var v = comboLayer.getValue();
    			var record = comboLayer.findRecord(comboLayer.valueField, v);
    			var i = comboLayer.store.indexOf(record);
    	        //clear values and disable fields
    	        comboField.clearValue();
    	        comboField.disable();
    	        comboLO.clearValue();
    	        comboLO.disable();
    	        comboO.clearValue();
    	        comboO.disable();
    	        combofieldValue.reset();
    	        combofieldValue.disable();
    	        fieldValueContainer.disable();
    	        textAreaWhereClause.reset();//let enabled  
    	        CBLimit.setValue(false);
    	        CBLimit.disable();  
    	        fieldLineLimit.reset();
    	        fieldLineLimit.disable();
    	        if (i!=0) {
    	          comboField.setValue('');  
    	          comboField.getStore().removeAll();
    	          //console.log(control.layerField);
    	          comboField.getStore().loadData(control.fieldData[i-1]);
    	          this.currentFieldData = control.fieldData[i-1]; 
    	          comboField.enable();
    	          textAreaWhereClause.enable();
    	        }
	        
	        
	      }, 
	      this);
	    
  
	    
      this.win.doLayout();
      
    } 

    if (!this.win.isVisible()) {
      this.initFields();
      var mainPanel = this.map.app.ui.getMainPanel();
      // automatic positionning don't work well after export tools are called... 
      /*
      var x = mainPanel.getEl().getX();
      var y = mainPanel.getEl().getY() + 27;
      //console.log("X,Y : " + x + " , " + y);
      this.win.setPagePosition(mainPanel.getEl().getX(), mainPanel.getEl().getY() + 27);
      */
      this.win.show();
      //this.gridInfoPanel.setWidth(this.win.getWidth()-15);
    }
  },


  getLayerTreeManager : function() {
    if (this.layertreeManager == null) {
      var controls = this.map.getControlsByClass('Carmen.Control.LayerTreeManager');
      this.layerTreeManager = controls.length==0 ? null : controls[0];
    }
    return this.layerTreeManager;
  },

  getLayerTree : function() {
  	
    this.layerTree = this.getLayerTreeManager().getLayerTree().clone(this.transformNodeInfo);
    this.layerTree.filter(this.filterNodeLayerQueryable);
  	return this.layerTree;
  },

  getLayerRecursive : function(qLayerAtts){
	  var qLayerAtts1 = new Array();
	  for (var i=0; i<qLayerAtts.length; i++) {
	   if (qLayerAtts[i].attributes.type == 1){
			 qLayerAtts1 = qLayerAtts1.concat(this.getLayerRecursive(qLayerAtts[i].childNodes));
		 } else if (qLayerAtts[i].attributes.type == 0) {
			 qLayerAtts1.push(qLayerAtts[i]);
		 }
	  }
	  return qLayerAtts1;
  },
  // init the requestPanel fields, 
  // i.e the combos with data from the queryable layers
  initFields : function() {
  	var qLayerAtts = this.getLayerTree().childNodes/*.collect('attributes', true)*/;
    qLayerAtts = this.getLayerRecursive(qLayerAtts);
    console.log(qLayerAtts);
    
  	this.layerData = [['none','Sélectionnez une couche']];
  	this.fieldData = [];
  	for (var i=0; i<qLayerAtts.length; i++) {
    
		  var layerAtt = qLayerAtts[i].attributes;
  		var layerData = qLayerAtts[i].data;
  		this.layerData.push([layerAtt.layerName,layerData.text]);
      layerFieldsData = [];
      fieldsCarmenDesc = Carmen.Util.chompAndSplit(layerAtt.infoFields,';');
      //console.log(fieldsCarmenDesc);
      this.layerFieldsDesc[layerAtt.layerName] = [];        		
  		for (var j=0; j<fieldsCarmenDesc.length; j++) {
  			fieldDesc = Carmen.Util.decodeCarmenFieldDesc(fieldsCarmenDesc[j], layerAtt.baseQueryURL);
  			if (fieldDesc.type=='TXT')             
  			  layerFieldsData.push([fieldDesc.name, '[' + fieldDesc.alias +']', fieldDesc.alias]);
  			this.layerFieldsDesc[layerAtt.layerName].push(fieldDesc);
  		}
      this.fieldData.push(layerFieldsData);    
      
  	}
  	
  	// loading data in combos
  	var comboLayer = Ext.getCmp('comboLayerAdvanced');
  	var comboField = Ext.getCmp('comboFieldAdvanced');
  	var comboLO = Ext.getCmp('logicalOperatorAdvanced');
  	var comboO = Ext.getCmp('comboOperatorAdvanced');
  	var combofieldValue = Ext.getCmp('fieldValueAdvanced');
    var fieldValueContainer = Ext.getCmp('valueFieldContainer');
  	var textAreaWhereClause = Ext.getCmp('whereClauseAdvanced');
  	var textAreaWhereClause = Ext.getCmp('whereClauseAdvanced');
  	var CBLimit = Ext.getCmp('CBLimit');
  	var fieldLineLimit = Ext.getCmp('fieldLineLimit');
 	
  	for(var i = 0; i < this.layerData.length; i++){
  	  if(this.layerData[i][1].match(/<table.*<\/table>/)){
        this.layerData[i][1] = this.layerData[i][1].replace(/<table.*<\/table>/, "");
  	  }
  	}
    comboLayer.getStore().loadData(this.layerData);
    
    // adding listener on layer combo
    var control = this;
    comboLayer.on(
      'change', 
      function() {
		var v = comboLayer.getValue();
		var record = comboLayer.findRecord(comboLayer.valueField, v);
		var i = comboLayer.store.indexOf(record);
        //clear values and disable fields
        comboField.clearValue();
        comboField.disable();
        comboLO.clearValue();
        comboLO.disable();
        comboO.clearValue();
        comboO.disable();
        combofieldValue.reset();
        combofieldValue.disable();
        fieldValueContainer.disable();
        textAreaWhereClause.reset();//let enabled  
        CBLimit.disable();  
        fieldLineLimit.disable();
        if (i!=0) {
          comboField.setValue('');  
          comboField.getStore().removeAll();
          //console.log(control.layerField);
          comboField.getStore().loadData(control.fieldData[i-1]);
          this.currentFieldData = control.fieldData[i-1]; 
          comboField.enable();
          textAreaWhereClause.enable();
          CBLimit.enable();  
          
          //this.resizeComboToFitContent(comboField);//resize combo field 	
        }	
      },
      this);   
  },

  // build the parameters of the request
  buildParams : function(layerFieldsDesc, qstring, qOgrString, noParam) {
    var qitem = null;
    var form = Ext.getCmp('requestPanelAdvanced').getForm();
    var values = form.getValues();
    
    //var comboLayer = Ext.getCmp('comboLayer');
    var layerName = this.comboLayer.getValue();


    // nb limite lie à la cb
    var valueLineLimit = this.valueLineLimit.getValue();
   
    if (noParam==true){// initialisation à -1 pour avoir des valeurs !="" (cf. dans code php)
    	var qitem = this.comboFieldValue;
    	qstring = "1=1";
    	qOgrString = "1=1";
    	var op = -1;
    	var pattern = -1;
   
    }else{
      var qitem =  this.comboFieldValue;
      var op = this.opFieldValue;
      var pattern = this.valueFieldValue;
    }
    
    var node = this.layerTree.findChildByAttribute('layerName', layerName);
    var mapfile = (node!=null && node.attributes.OlLayer.mapfile!=null) ?
      node.attributes.OlLayer.mapfile :
      this.mapfile;
   
    var params = {
      layer : layerName,
      qstring : qstring,
      qOgrString : qOgrString,
      pattern : pattern,
      op : op,
      qitem : qitem,
      fields : Ext.util.JSON.encode(layerFieldsDesc[layerName]),
      valueLineLimit : valueLineLimit,
      map : mapfile 
     
    }

    //console.log(params);
    return params;    
  },


  // launch the request to the server
  performQuery : function (queryParams) {
    var control = this;
    
    var received = function (response) {
      response = Ext.decode(response.responseText);      
      if (response.totalCount == 0) {
        //console.log('no results...');
        control.clearGridPanel();
        Ext.Msg.alert("Requêtes attributaires", "Aucun résultat ne correspond à votre recherche.");
      }else{    	  
    	 
    	  var nbResults = (response.totalCount)*1;
    	  var nbLimit = (PRO_REQUETEUR_NB_LIMIT_OBJ)*1;   	    
    	    
    	  if(nbResults > nbLimit){// msg d'alerte 
        	  Ext.Msg.confirm('Requêtes attributaires','Le nombre d\'informations à afficher est important, souhaitez-vous continuer?', function(btn, text){
        		  if (btn == 'yes'){ 
        	    	  for(lname in response.results)  {       	    		  
	        	    		var node = control.layerTree.findChildByAttribute('layerName', lname);
	        		        if (node!=null) { 
	        		          var data = response.results[lname].data;
	        		          node.attributes.infoFieldDesc = Ext.decode(queryParams.fields);
	        		          node.attributes.infoData = response.results[lname];
	        		          node.attributes.infoData.qfile = response.queryfile;
	        		          node.attributes.infoData.ogrMapfile = response.mapfileOgr;
	        		          control.updateGridPanel(node, response.queryfile);

	        		        }
        	    	  }
    	      	  }else{
    	      		  return;
    	      	  }
        	  });
    	  }else{  		  
	    	  for(lname in response.results)  {
  		        var node = control.layerTree.findChildByAttribute('layerName', lname);
  		        if (node!=null) { 
  		          var data = response.results[lname].data;
  		          node.attributes.infoFieldDesc = Ext.decode(queryParams.fields);
  		          node.attributes.infoData = response.results[lname];
  		          node.attributes.infoData.qfile = response.queryfile;
  		          node.attributes.infoData.ogrMapfile = response.mapfileOgr;
  		          control.updateGridPanel(node, response.queryfile);
  		        }
  		      } 
    	  }
      }   
    }
    Ext.Ajax.timeout = this.requestTimeout;
    Ext.Ajax.request({
      url: Routing.generate('frontcarto_query_advancedqueryattributes'),
        //PRODIGE40 '/services/AdvancedQueryAttributes/index.php',
      //timeout: this.requestTimeout,
      success: function(response) { 
      	 received(response); },
      failure: function (response) { 
         Carmen.Util.handleAjaxFailure(response, "Requêtes attributaires"); 
      },
      params: queryParams
    });
  }, 

  clearGridPanel : function() {
  	var gPanel = this.gridInfoPanel;
    if(gPanel!=undefined){
      gPanel.setTitle('Aucun résultat');
      //var xlsTool = this.gridInfoPanel.getTool('xls');
      var xlsTool = Ext.getCmp('queryPanel_xls');
      xlsTool.hide();
      //var csvTool = this.gridInfoPanel.getTool('csv');
      var csvTool = Ext.getCmp('queryPanel_csv');
      csvTool.hide();
      gPanel.reconfigure(Carmen.Util.emptyStore, Carmen.Util.emptyColumnModel);
      if (!gPanel.isVisible()){
        gPanel.show();
        gPanel.setWidth(this.win.getWidth()-15);
      }
    }
  },
  
  updateGridPanel : function(layerNode, queryfile) {
    var control = this;  
    
    var panel_id = this.displayClass +'_' + 'gridInfoPanel_' +  Ext.id();
    this.gridInfoPanel = Ext.create('Ext.grid.Panel', {
      id : panel_id,
      store: new Ext.data.ArrayStore({
        fields: [],
        data: []
      }),
      columns: [],
      title:'Résultats sélectionnés',
      hidden: true,
      autoScroll: true,
      fitToFrame: true,
      border: false,
      iconCls : 'cmnInfoGridZoomIcon',
      bounds: null,
      tools:[{
            id: 'queryPanel_zoom',
            qtip: 'Zoom sur les résultats',
            renderTpl: [
              '<i id="{id}-toolEl" data-ref="toolEl" class="fa fa-search fa-1x"' +
              '" style="color: #FFF; font-size: 16px;" role="presentation"/></i>'
            ],
            handler: function(){
              // show help here
				var bounds = this.toolOwner.bounds;
				control.map.zoomToSuitableExtent(bounds);
			}
      },{
        id:'queryPanel_xls',
        qtip: 'Export de la sélection au format Excel',
        scope : this,
		renderTpl: [
		  '<i id="{id}-toolEl" data-ref="toolEl" class="fa fa-file-excel-o fa-1x"' +
		  '" style="color: #FFF; font-size: 16px;" role="presentation"/></i>'
		]
      },
      {
        id:'queryPanel_csv',
        qtip: 'Export de la sélection au format CSV',
		renderTpl: [
		  '<i id="{id}-toolEl" data-ref="toolEl" class="fa fa-file-text-o fa-1x"' +
		  '" style="color: #FFF; font-size: 16px;" role="presentation"/></i>'
		]
      },
      {
        // Add new publippstage icon here
        //id: panel_id + '_mail',
        id: 'publipostage_mail',
        qtip: 'Utiliser le module de publipostage',
        hidden: (user_connected_details && !user_connected_details.PUBLIPOSTAGE),
        renderTpl: [
                    '<i id="{id}-toolEl" data-ref="toolEl" class="fa fa-envelope-o"' +
                    '" style="color: #FFF; font-size: 16px;" role="presentation"/></i>'
                    ]
        //handler: function() {
        //}
      }]
    });     

    var gPanel = this.gridInfoPanel; 
    this.map.app.ui.delAllInfoPanel();
    this.map.app.ui.addToInfoPanel(gPanel);
    this.map.app.ui.infoPanel.setTitle('Résultats');
    // clearing selection
    this.map.clearSelection();
    // removing listeners...
    //gPanel.purgeListeners();    
    // updating panel title

    gPanel.setTitle(layerNode.data.text + ' ' + 
      layerNode.attributes.infoData.data.length + 
      ' résultats');
    var extentStr = layerNode.attributes.infoData.extent;
    var minScale = layerNode.attributes.minScale;
    var maxScale = layerNode.attributes.maxScale;
    gPanel.bounds = Carmen.Util.strExtentToOlBounds(extentStr, Carmen.Control.AdvancedQueryAttributes.PUNCTUAL_RADIUS);
    this.map.app.ui.infoPanel.bounds = gPanel.bounds;
    
    var lnode = layerNode;
    /*gPanel.header.on('click', 
        function() {
          control.map.zoomToSuitableExtent(bounds, minScale, maxScale);
          lnode.attributes.OlLayer.setDisplayMode(
             Carmen.Layer.MapServerGroup.DISPLAY_NQUERYMAP, 
             lnode.attributes.layerName, null, 
             lnode.attributes.infoData.qfile);
        });*/
    control.map.zoomToSuitableExtent(gPanel.bounds, minScale, maxScale);
    
    layerNode.attributes.OlLayer.setDisplayMode(
       Carmen.Layer.MapServerGroup.DISPLAY_NQUERYMAP, 
       layerNode.attributes.layerName, null, 
       layerNode.attributes.infoData.qfile,
       layerNode.attributes.infoData.ogrMapfile);
     // configuring and showing the result table panel
     var gConfig = this.buildGridConfig(layerNode.attributes.infoFields, layerNode.attributes.infoData);

     // configuring exports...
	 //var xlsTool = gPanel.getTool('xls');
	 var xlsTool = Ext.getCmp('queryPanel_xls');
	 //xlsTool.removeAllListeners();
	 xlsTool.show();
	 xlsTool.on('click',
	  function() {
	    var url = Routing.generate('frontcarto_export_xls');
          //PRODIGE40 '/services/GetXLS/index.php';
		var f = this.control.gridInfoFormPanel; 
		queryParams = { 
		  field : Ext.encode(this.node.attributes.infoFieldDesc),
		  data : Ext.encode(this.node.attributes.infoData.data),
		  selectionName : this.node.text,
		  output : "XLS"
		};
		Carmen.Util.doFormUpload(f, url, queryParams);
	  }, 
	  { control : control, node : layerNode } // function scope
	 );
	 
	 
	 
	 // configuring exports...
	 var xlsTool = Ext.getCmp('queryPanel_csv');
	 xlsTool.show();
	 xlsTool.on('click',
	  function() {
	    var url = Routing.generate('frontcarto_export_xls');
          //PRODIGE40 '/services/GetXLS/index.php';
		var f = this.control.gridInfoFormPanel; 
		queryParams = { 
		  field : Ext.encode(this.node.attributes.infoFieldDesc),
		  data : Ext.encode(this.node.attributes.infoData.data),
		  selectionName : this.node.text,
		  output : "CSV"
		};
		Carmen.Util.doFormUpload(f, url, queryParams);
	  }, 
	  { control : control, node : layerNode } // function scope
	 );

	 //
	 var mailTool = Ext.getCmp('publipostage_mail');
	 mailTool.on('click',
	     function() {
         //console.log('this.node is :',this.node );
         var fieldsDesc = this.node.attributes.infoFieldDesc;
         var infoData = this.node.attributes.infoData;
         var ISADMIN = user_connected_details && user_connected_details.isAdmProdige;
         var oWind = null;
         var windowWidth = 900;
         var labelWidth  = 170;
         var buttonWidth = 100;
         var sLabelWidth = 70;
         var sCtrlWidth  = Math.floor((windowWidth-labelWidth-2*sLabelWidth-6)/2)+sLabelWidth;

       // Localy store for the ComboBox (Champ courriel destinataire - Champ nom destinataire) 
       store_mail = new Ext.data.JsonStore({
         fields: ['name', 'alias', 'type', 'url'],
         data: fieldsDesc
       });

       // Call web service method
       var url = Routing.generate('frontcarto_export_mailing');
       //PRODIGE40 '/services/GetMailing/index.php';
       var mailingFields = "";
       var defaultMailingFields = "\n{$signature} : signature de l'utilisateur connecté,"+
         "{$messageAuto} : Message de bas de page";
       var glu = "";
       for(var i=0; i<fieldsDesc.length; i++) {
         mailingFields = mailingFields + glu + "{$"+fieldsDesc[i].name+"} : "+fieldsDesc[i].alias;
         glu = ( (i+1)%3==0 ? ",\n" : ", " );
       }

       var store_modele = Ext.create('Ext.data.JsonStore', {
         autoDestroy: true,
         fields: ['modele_id', 'modele_name', 'msg_title', 'msg_body'],
         proxy: {
           type: 'ajax',
           actionMethods: 'POST',
           url: url,
           extraParams: {
             action     : 'getJsonStore_Models',
             layerTable : infoData.layerTable
           },
           reader: {
             type: 'json'
           }
         }
       });

       // fidsArray contains all gid for concerned layers
       var fidsArray = [];
       for(var i=0; i<infoData.data.length; i++) {
         fidsArray.push(infoData.data[i][10]);
       }
       // fidsArray to object 
       var fids = {'fids': fidsArray};

       // Form Panel
       var formPanel = Ext.create('Ext.form.Panel', {
         me: this,
         id: 'id_formePanel',
         layout: 'form',
         width: windowWidth-20,
         fieldDefaults : {
           labelAlign : 'right',
           labelWidth: labelWidth
         },
         items : [{
             xtype: 'displayfield',
             fieldLabel: 'Couche sélectionnée',
             value: infoData.layerTable
           }, {
             xtype: 'fieldcontainer',
             fieldLabel:'Champ destinataire',
             layout: 'hbox',
             defaults: { 
               labelWidth: sLabelWidth
             },
             items: [{
               id:'cmb_nom_destinataire',
               xtype: 'combo',
               fieldLabel:'Nom*',
               valueField: 'name',
               displayField:'name',
               queryMode:'local',
               allowBlank: false,
               maxLength: 80,
               autoSelect: true,
               editable: false,
               width: sCtrlWidth,
               store: store_mail
             }, {
               id:'cmb_courriel_destinatatire',
               xtype: 'combo',
               fieldLabel:'Courriel*',
               valueField: 'name',
               displayField:'name',
               queryMode:'local',
               allowBlank: false,
               autoSelect: true,
               editable: false,
               width: sCtrlWidth,
               store: store_mail
             }]
           }, {
             xtype: 'fieldcontainer',
             fieldLabel:'Expéditeur',
             layout: 'hbox',
             defaults: { 
               labelWidth: sLabelWidth
             },
             items: [{
               id:'txt_nom_expediteur',
               xtype: 'textfield',
               name: 'nom_expediteur',
               fieldLabel:'Nom*',
               allowBlank: false,
               maxLength: 80,
               width: sCtrlWidth,
               value: user_connected_details ? user_connected_details.userPrenom+" "+user_connected_details.userNom : ""
             }, {
               id:'txt_courriel_expediteur',
               xtype: 'textfield',
               name: 'courriel_expediteur',
               fieldLabel:'Courriel*',
               allowBlank: false,
               maxLength: 80,
               width: sCtrlWidth,
               width: sCtrlWidth,
               vtype: 'email',
               value: user_connected_details ? user_connected_details.userEmail  : ""
             }]
           }, {
             xtype: 'fieldcontainer',
             fieldLabel:'Modèle enregistré',
             layout: 'hbox',
             items: [{
                 id:'cmb_modele',
                 xtype: 'combo',
                 valueField: 'modele_id',
                 displayField: 'modele_name',
                 autoSelect: true,
                 editable: false,
                 width: windowWidth-labelWidth-buttonWidth-10,
                 store: store_modele,
                 listeners: {
                   'select' : {
                     fn: function(combo, records, eOpts) {
                       Ext.getCmp('txt_objet').setValue(records[0].get('msg_title'));
                       Ext.getCmp('htmlEditor_message').setValue(records[0].get('msg_body'));
                     }
                   }
                 }
               }, {
                 id: 'btn_supprimer',
                 xtype: 'button',
                 margin: '0 0 0 4',
                 width: buttonWidth,
                 text: 'Supprimer',
                 hidden: ( ISADMIN ? false : true ), // Show checkBox if the user is an admin
                 handler: function(){
                   var modelId = Ext.getCmp('cmb_modele').getValue();
                   if( modelId!="" && modelId!="-1" ) {
                     Ext.Msg.confirm("Confirmation", "Veuillez confirmer la suppression du modèle sélectionné ?", function(btn){
                       if( btn=="yes" ) {
                         Ext.Ajax.request({
                           url: url,
                           method: 'POST',
                           params: {
                             action: 'supprimer',
                             modeleId:  modelId
                           },
                           success: function(response){
                             var oRes = eval("("+response.responseText+")");
                             if( oRes.success ) {
                               Ext.getCmp('cmb_modele').getStore().load();
                               Ext.getCmp('cmb_modele').setValue('');
                               Ext.getCmp('txt_objet').setValue('');
                               Ext.getCmp('htmlEditor_message').setValue('');
                             }
                             Ext.Msg.alert((oRes.success ? 'Notification' : 'Attention'), oRes.msg); 
                           },
                           failure: function(response) {
                             Ext.Msg.alert('Erreur', 'Impossible de supprimer le modèle sélectionné.');
                           }
                         });
                       }
                     }).icon = Ext.Msg.WARNING;
                   } else {
                     Ext.Msg.alert('Attention', 'Vous devez sélectionner un modèle au préalable.');
                   }
                 }
               }]
           }, {
             id:'txt_objet',
             xtype: 'textfield',
             name: 'objet',
             fieldLabel:'Objet*',
             allowBlank: false,
             maxLength: 80
           }, {
             id:'htmlEditor_message',
             xtype: 'htmleditor',
             fieldLabel: 'Message*',
             allowBlank: false,
             width: windowWidth,
             height: 280,
             enableAlignments: true,
             enableColors: true,
             enableFont: true,
             enableFontSize: true,
             enableFormat: true,
             enableLinks: true,
             enableLists: true,
             enableSourceEdit: true
           }, {
             xtype: 'textareafield',
             //xtype: 'displayfield',
             id: 'mailingFields',
             fieldLabel: 'Champs de publipostage<br>disponibles',
             readOnly: true,
             scrollable: 'y',
             style: {
               height: '48px'
             },
             value: mailingFields+defaultMailingFields
           }, {
             id:'chk_enregistre_modele',
             xtype: 'checkboxfield',
             fieldLabel:'Enregistré comme modèle',
             name: 'checkBox',
             //inputValue:'1'
             hidden: (ISADMIN)?false:true, // Show checkBox if the user is an admin
             listeners: {
               change: function (checkbox, newVal, oldVal) {
                 var ctrlModelName = Ext.getCmp("txt_name_modele");
                 if( !ctrlModelName ) return;
                 if(newVal == '1' && oldVal == '0') {
                   ctrlModelName.setVisible(true);
                   ctrlModelName.allowBlank = false;
                   ctrlModelName.validate();
                 } else {
                   ctrlModelName.setVisible(false);
                   ctrlModelName.setValue('');
                   ctrlModelName.allowBlank = true;
                   ctrlModelName.validate();
                 }
               }
             }
           }, {
             id:'txt_name_modele',
             name: 'name_modele',
             fieldLabel:'Nom du modèle',
             xtype: 'textfield',
             maxLength: 80,
             hidden: true
           }],
         buttons: [{
             id: 'btn_tester',
             xtype: 'button',
             text: 'Tester',
             width: buttonWidth,
             margin: '0 4 0 0',
             handler: function() {
               var form = Ext.getCmp('id_formePanel');
               if ( form.isValid() ) {
                 Ext.MessageBox.wait({ 
                   msg: "Veuillez patienter...",
                   width:300, 
                   wait:true, 
                   waitConfig: {interval:200}
                 });
                 Ext.Ajax.request({
                   url: url,
                   method: 'POST',
                   params: {
                     action                   : 'tester',
                     champCourrielDestinataire: Ext.getCmp('cmb_courriel_destinatatire').getValue(),
                     champNomDestinataire     : Ext.getCmp('cmb_nom_destinataire').getValue(),
                     nomExpediteur            : Ext.getCmp('txt_nom_expediteur').getValue(),
                     courrielExpediteur       : Ext.getCmp('txt_courriel_expediteur').getValue(),
                     objectMessage            : Ext.getCmp('txt_objet').getValue(),
                     bodyMessage              : Ext.getCmp('htmlEditor_message').getValue(),
                     nomModele                : '',
                     userGiven                : user_connected_details.userPrenom,
                     userName                 : user_connected_details.userNom,
                     userLogin                : user_connected_details.userLogin,
                     userSignature            : user_connected_details.userSignature,
                     layerTable               : infoData.layerTable,
                     fidsList                 : fids
                   },
                   success: function(response){
                     Ext.MessageBox.hide();
                     var oRes = eval("("+response.responseText+")");
                     Ext.Msg.alert((oRes.success ? 'Notification' : 'Attention'), oRes.msg);
                   },
                   failure: function(response) {
                     Ext.MessageBox.hide();
                     Ext.Msg.alert('Erreur', 'L\'envoi du message test a échoué.');
                   }
                 });
               } else {
                 Ext.Msg.alert('Attention', 'Vous devez renseigner tous les champs obligatoires<br>et fournir une addresse courriel valide pour l\'expéditeur.');
               }
             }
           }, {
             id: 'btn_envoyer',
             xtype: 'button',
             text: 'Envoyer',
             width: buttonWidth,
             margin: '0 4 0 0',
             handler: function() {
               var form = Ext.getCmp('id_formePanel');
               if ( form.isValid() ) {
                 Ext.MessageBox.wait({ 
                   msg: "Veuillez patienter...", 
                   width:300, 
                   wait:true, 
                   waitConfig: {interval:200}
                 });
                 Ext.Ajax.request({
                   url: url,
                   method: 'POST',
                   params: {
                     action                   : 'envoyer',
                     champCourrielDestinataire: Ext.getCmp('cmb_courriel_destinatatire').getValue(),
                     champNomDestinataire     : Ext.getCmp('cmb_nom_destinataire').getValue(),
                     nomExpediteur            : Ext.getCmp('txt_nom_expediteur').getValue(),
                     courrielExpediteur       : Ext.getCmp('txt_courriel_expediteur').getValue(),
                     objectMessage            : Ext.getCmp('txt_objet').getValue(),
                     bodyMessage              : Ext.getCmp('htmlEditor_message').getValue(),
                     nomModele                : Ext.getCmp('txt_name_modele').getValue(),
                     userGiven                : user_connected_details.userPrenom,
                     userName                 : user_connected_details.userNom,
                     userLogin                : user_connected_details.userLogin,
                     userSignature            : user_connected_details.userSignature,
                     layerTable               : infoData.layerTable,
                     fidsList                 : fids
                   },
                   success: function(response){
                     Ext.MessageBox.hide();
                     var oRes = eval("("+response.responseText+")");
                     Ext.Msg.alert((oRes.success ? 'Notification' : 'Attention'), oRes.msg, function() {
                       if( oRes.success ) {
                         oWind.close();
                       }
                     });
                   },
                   failure: function(response) {
                     Ext.MessageBox.hide();
                     Ext.Msg.alert('Erreur', 'L\'envoi du message a échoué.');
                   }
                 });
               } else {
                 Ext.Msg.alert('Attention', 'Vous devez renseigner tous les champs obligatoires<br>et fournir une addresse courriel valide pour l\'expéditeur.');
               }
             }
           }, {
             id: 'btn_fermer',
             xtype: 'button',
             text: 'Fermer',
             width: buttonWidth,
             margin: '0 4 0 0',
             handler: function() {
               oWind.close();
             }
           }]
       });

     // Window (contains form panel ...)
     oWind = Ext.create('Ext.window.Window', {
       title: 'Publipostage',
       width: windowWidth,
       minWidth: windowWidth,
       minHeight: 550,
       layout: 'fit',
       items: formPanel // formPanel
     });

     oWind.show();

	 },
	 { control : control, node : layerNode});
	 //

     gPanel.reconfigure(gConfig.store, gConfig.columnModel);
     gPanel.on('rowclick', 
      function(grid, record, tr, rowIndex, e, eOpts ){
        var rec = grid.store.getAt(rowIndex);
        // zooming to the right extent
        var extentStr = rec.get('extent');
        var bounds = Carmen.Util.strExtentToOlBounds(extentStr, Carmen.Control.AdvancedQueryAttributes.PUNCTUAL_RADIUS);
        //control.map.zoomToSuitableExtent(bounds, minScale, maxScale);

        // highlighting the feature linked to the row
        var fid = rec.get('fid');
        lnode.attributes.OlLayer.setDisplayMode(
          Carmen.Layer.MapServerGroup.DISPLAY_QUERYMAP, 
          lnode.attributes.layerName, fid);

      });
      
      gPanel.on('cellclick', 
        function( grid, td, cellIndex, record, tr, rowIndex, e, eOpts){
			if (cellIndex==1) {
				var rec = grid.getStore().getAt(rowIndex); 
				var extentStr = rec.get('extent');
				var bounds = Carmen.Util.strExtentToOlBounds(extentStr, Carmen.Control.Info.PUNCTUAL_RADIUS);
				control.map.zoomToSuitableExtent(bounds, minScale, maxScale);
			 }
			 if (cellIndex==2) {
				var rec = grid.getStore().getAt(rowIndex); 
				var columns = grid.ownerCt.columnManager.columns;
				control.buildInfoWindow(rec, columns, rowIndex);					
				
			 }
      });
      
      
      gPanel.on('selectionchange', 
        function(grid, selected, eOpts){
			if (selected.length == 0){
					var minScale = lnode.attributes.minScale;
					var maxScale = lnode.attributes.maxScale;
					control.map.clearGraphicalSelection();
					lnode.attributes.OlLayer.setDisplayMode(
						Carmen.Layer.MapServerGroup.DISPLAY_NQUERYMAP, 
						lnode.attributes.layerName, null,
						queryfile);
			 }
      });
      // registering selection
      olLayer = lnode.attributes.OlLayer;
      lname = lnode.attributes.layerName;
      if (olLayer.handleSelection(lname)) { 
  	    this.map.addToSelectionLayers(olLayer);
  	    /*olLayer.setSelectionStore(lname, 
  	      new Ext.data.SimpleStore({
            fields: lnode.attributes.infoData.fields,
            data :  lnode.attributes.infoData.data
          }));*/
      } 
     if (!gPanel.isVisible()) {
       //console.log('isnotvisible');
       gPanel.show();
       gPanel.setWidth(this.win.getWidth()-15);
       //resize grid
       gPanel.setHeight(eval(this.win.getEl().getHeight(true)-this.requestPanel.getHeight()));
     }
     else{
       //console.log('isalreadyvisible');
       gPanel.setWidth(this.win.getWidth()-15);
       //resize grid
       gPanel.setHeight(eval(this.win.body.getStyle('max-height').replace(/px/,'')-this.requestPanel.getHeight()));
     }
  }, 
  
  extentRenderer : function(v) {
  	return '<i class="fa fa-fw fa-search fa-1x"' +
              '" style="color: #000; font-size: 16px;"></i>';
  },
  
  infoRenderer : function(v) {
  	return '<i class="fa fa-fw fa-info-circle fa-1x"' +
              '" style="color: #000; font-size: 16px;"></i>';
  },


  buildGridConfig: function (fieldsDesc, result) {
    var fields = new Array();
    var columns = new Array();
    
    columns.push(new Ext.grid.RowNumberer());
    columns.push({
      id : 'extent',
      header : '',
      width : 30,
      resizable: true,
      renderer: this.extentRenderer,
      dataIndex: 'extent'
    });
    columns.push({
	  //id : 'info',
	  header : '',
	  width : 40,
	  resizable: true,
	  renderer: this.infoRenderer,
	  dataIndex: 'info'
	});
    
    var arrFieldsDesc =  Carmen.Util.chompAndSplit(fieldsDesc,';');
    //console.log(arrFieldsDesc);
    for (var i=0; i<arrFieldsDesc.length; i++) {
    	//console.log(arrFieldsDesc[i]);
    	var desc = Carmen.Util.decodeCarmenFieldDesc(arrFieldsDesc[i]);
    	//console.log(desc);
    	var fDesc = {name : desc.name + i.toString(), type : Carmen.Util.carmenTypeToExtType(desc.type)};
    	if (desc.type=='date') 
    	 fDesc['dateFormat'] = Carmen.Util.carmenDateTypeToExtDateFormat(desc.type);
    	fields.push(fDesc);
    	
    	var colDesc = {
    		id: Ext.id(), //desc.name + i.toString(),
    		header: desc.alias,
    		width: 200,
    		sortable: true,
    		resizable: true,
    		renderer: Carmen.Util.carmenTypeToExtRenderer(desc.type),
    		dataIndex: desc.name + i.toString()
    	};
    	columns.push(colDesc);
    }
    fields.push({name : 'extent'});
    fields.push({name : 'fid'});
    //console.log(fields);
    var config = {
	  	store : new Ext.data.SimpleStore({
	        fields: fields,
	        data : result.data
	    }),
	    columnModel : columns 
	  };

	  return config;
  },


  // Filter and transform function used on tree
  filterNodeLayerQueryable :  function(node) {
  	var res = true;
    res = ('type' in node.attributes) && 
      ((node.attributes.type == Carmen.Control.LayerTreeManager.NODE_GROUP && node.hasChildNodes()) ||
       (node.attributes.type == Carmen.Control.LayerTreeManager.NODE_LAYER && node.attributes.OlLayer.getVisibility(node.attributes.layerName)
       && node.attributes.infoFields && node.attributes.infoFields.length>0));
    return res;
  },

  filterNodeLayerWithResults : function(node) {
  	var res = true;
    res = ('type' in node.attributes) && 
      ((node.attributes.type == Carmen.Control.LayerTreeManager.NODE_GROUP && node.hasChildNodes()) ||
       (node.attributes.type == Carmen.Control.LayerTreeManager.NODE_LAYER && node.attributes.infoData!=null) ||
       (node.attributes.type == Carmen.Control.LayerTreeManager.NODE_FEATURE));    
    return res;
  },

  transformNodeInfo : function(atts) {
  	var cloneAtts = Carmen.Util.clone(atts);
/*
  	if (cloneAtts.text!=Carmen.Control.LayerTreeManager.TREE_NODE_ROOT_LABEL)
  	   cloneAtts.uiProvider = Carmen.TreeNodeInfoUI;
  	else
  	   cloneAtts.uiProvider = Carmen.RootTreeNodeInfoUI;  	   
*/
  	cloneAtts.cls = 'none';
    cloneAtts.infoData = null;  	
  	cloneAtts.listeners = {};
  	cloneAtts.expandable = true;
  	if (cloneAtts.type == Carmen.Control.LayerTreeManager.NODE_LAYER) {
  		cloneAtts.icon = '/IHM/images/NoTreeIcon.gif';
  		cloneAtts.iconCls = 'cmnInfoGridZoomIcon';
  	}
    cloneAtts.text = atts.layerAlias!=null ? atts.layerAlias : atts.text;
  	return cloneAtts;
  },
  
    buildInfoWindow : function(rec, columns, rowIndex){
	  
	    var data = [];
		for(var i=3;i<columns.length;i++){
		  var value = {};
		  value['key'] = columns[i].text;
		  value['value'] = columns[i].renderer.apply(null,[rec.get(columns[i].dataIndex)]);
		  data.push(value);
		}
		
		Ext.create('Ext.data.Store', {
			storeId:'infoStore',
			fields:['key', 'value'],
			data:{'items':
				data
			},
			proxy: {
				type: 'memory',
				reader: {
					type: 'json',
					rootProperty: 'items'
				}
			}
		});

		var gridInfoPanel = Ext.create('Ext.grid.Panel', {
			store: Ext.data.StoreManager.lookup('infoStore'),
			columns: [
				{ text: 'Libellé',  dataIndex: 'key', flex: 1 },
				{ text: 'Donnée', dataIndex: 'value', flex: 1 }
			],
			height: 200,
			width: 400
		});
		this.winInfo.setTitle("Information");
		this.winInfo.removeAll();
		this.winInfo.add(gridInfoPanel);
		this.winInfo.show();
  },
  
  CLASS_NAME: "Carmen.Control.AdvancedQueryAttributes"

});

Carmen.Control.AdvancedQueryAttributes.PUNCTUAL_RADIUS = 100.0;

