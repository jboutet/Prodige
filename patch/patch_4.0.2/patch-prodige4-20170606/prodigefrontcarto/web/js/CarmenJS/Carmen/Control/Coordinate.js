/**
 * @_requires OpenLayers/Control/MousePosition.js
 */

/**
 * Class: Carmen.Control.Coordinate
 
 * Inherits from:
 *  - <OpenLayers.Control.MousePosition>
 */


Carmen.Control.Coordinate = new OpenLayers.Class(OpenLayers.Control.MousePosition, {

  // Todo add display config option to allow or not scale box display
  // displayConfig: 0,
 

	projName : 'unknown',

  coordinateLabel : Ext.create('Ext.form.Label', {
    width: 210,
    id: 'coordLabel'
    //flex : 1
  }),
  
  
  coordinateBtn : Ext.create('Ext.button.Button', {
    iconCls: '',/*'button_coord', */
    id: 'coordButton',
    text: '<i class="fa icon-projection1"></i>',
    tooltip : 'choix du système de projection des coordonnées affichées'
  }),
  
   
  initialize: function(options) {
    
    var tabStoreProj = new Array();
    for (var index in tabEPSG){
      
      data = new Array("EPSG:"+index, tabEPSG[index]);
      tabStoreProj.push(data);
    }

    this.coordinateSysBox = Ext.create('Ext.form.ComboBox', {
      queryMode: 'local',
      triggerAction: 'all',
      emptyText: 'Sélectionnez un sytème de coordonnées',
      editable : false,
      hidden: true,
      hideMode: 'visibility',
      width: 250,
      valueField: 'projName',
      displayField: 'projPrettyName',
      store:
        new Ext.data.ArrayStore({
          fields: ['projName', 'projPrettyName'],
          data : tabStoreProj
          }
        )
    }),
    
    options = OpenLayers.Util.extend(options, 
      {
      	prefix: " Position : ",
      	separator: ', '
      });
    
    OpenLayers.Control.MousePosition.prototype.initialize.apply(this, [options]);
  },
  
  setMap: function(map) {
    this.map = map;
    //console.log(this.map.getProjection());
    //console.log(this.map.projection);
     
    //var tb = this.map.app.ui.getBottomToolBar();
    
    var currentProjIdx = this.initializeCombo(this.map.projection);
    if (currentProjIdx!=-1) {
    	
      this.map.app.ui.getMainPanel().add(this.coordinateSysBox);
			this.coordinateSysBox.setValue(this.projName);
    	this.displayProjection = new OpenLayers.Projection(this.projName);
    	this.coordinateSysBox.on('select', this.comboSelect, this);
    }
    else {
      Ext.Msg.alert("Projection inconnue", "Le système de coordonnées de la carte n'est pas connu. " +
          "Les informations fournies par les outils de mesure et de lecture de " +
          "coordonnées ne sont pas garanties.");
      //this.coordinateSysBox.setValue('unknown');
    }
    
    
    this.map.app.ui.addToBottomToolbar(this.coordinateBtn,0);
    this.map.app.ui.addToBottomToolbar(this.coordinateLabel,3);
    /*
    tb.add(this.coordinateBtn);
    tb.add({ xtype: 'tbfill' });
    tb.add(this.coordinateLabel);
    */ 
    
    this.coordinateBtn.setHandler(this.showHideCoordBox,this);
    
    
    // add to init the div here, once the extPanel have been rendered  
    //obedel debug extjs 2.2.1 -> 3.0.0
    this.map.app.ui.doLayout();
    this.div = this.coordinateLabel.getEl().dom;
    // Call parent constructor then...
    OpenLayers.Control.MousePosition.prototype.activate.apply(this, arguments);
  },
 
 	comboSelect : function(combo, rec, idx) {
 	  var newProjName = combo.getValue();

 	  if (newProjName != this.projName) {
			//console.log(newProjName);
			//console.log(this);
			this.projName = newProjName; 
			this.displayProjection = new OpenLayers.Projection(this.projName);
			this.redraw();
 	  }
 	},
 	
 	initializeCombo : function(strProj) {
 	
 	  var projName = strProj.toLocaleUpperCase();
		var projIndex = this.coordinateSysBox.getStore().find('projName', projName);
		this.projName = projIndex==-1 ? 'unknown' : projName;
		return projIndex;
 	},
 
   /**
   * Method: formatOutput
   * Provides custom display output
   *
   * Parameters:
   * lonLat - {<OpenLayers.LonLat>} Location to display
   */
  formatOutput: function(lonLat) {
      var isGeoProj = (this.displayProjection.getUnits()==null);
      var digits = isGeoProj ? parseInt(this.numDigits) : 2;
      var newHtml =
          this.prefix +
          lonLat.lon.toFixed(digits) +
          this.separator + 
          lonLat.lat.toFixed(digits) +
          this.suffix;
      return newHtml;
   },
  
  showHideCoordBox : function(btn,e) {
    if (this.coordinateSysBox.isVisible()) {
      this.coordinateSysBox.hide();
    }
    else {
	  app.map.hideControls();
      var x = this.coordinateBtn.getX();
      var y = this.coordinateBtn.getY();
      this.coordinateSysBox.setX(x);
      this.coordinateSysBox.setY((y-35));
      this.coordinateSysBox.show();
    }
  },
  hide : function(btn,e) {
    this.coordinateSysBox.hide();
  },
 
 
  CLASS_NAME: "Carmen.Control.Coordinate"
});
