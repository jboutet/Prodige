Ext.data.Connection.prototype.withCredentials= true;
/**
 * @_requires OpenLayers/Control.js
 */

/**
 * Class: Carmen.Control.EditorManager
 *
 * Inherits from: - <OpenLayers.Control>
 */
Ext.define('Carmen.EditorManagerModel', {
	extend : 'Ext.data.Model',
	convertOnSet : true,
	fields : [{
    name : 'new_session', type : 'boolean', defaultValue : true
  }, {
    name :'layerId',
    convert : function(value, record){
      //record.data.new_session = record.data.new_session || record.get('layerId')!=value;
      return value;
    }
  }, {
    name :'from_session',
    convert : function(value, record){
      //record.data.new_session = record.data.new_session || record.get('from_session')!=value;
      return value;
    }
  }, {
    name :'snap_layer',
    convert : function(value, record){
      record.data.new_session = record.data.new_session || record.get('snap_layer')!=value;
      return value;
    }
  }, {
    name :'snap_tolerance',
    convert : function(value, record){
      record.data.new_session = record.data.new_session || record.get('snap_tolerance')!=value;
      return value;
    }
  }]
});

Carmen.Control.EditorManager2 = new OpenLayers.Class(Carmen.Control.EditorManager , {

  STYLE_INVALID : {fillColor: "red"},

  editStyleDefault : new OpenLayers.Style({
    fillColor: '#f00',
    fillOpacity: 0.8,
    strokeColor: '#800',
    strokeWidth: 2,
    graphicZIndex: 1,
    pointRadius: 5
  }),

  snapStyleDefault : new OpenLayers.Style({
    fillColor: '#00f',
    fillOpacity: 0.8,
    strokeColor: '#029',
    strokeWidth: 1
  }),

  verticesStyleDefault : new OpenLayers.Style({
    fillColor: '#00f',
    fillOpacity: 0.8,
    strokeColor: '#029',
    pointRadius: 5,
    strokeWidth: 2
  }),


  // properties
  mapfile :null,
  serviceUrl :null,
  //TODO
  workingScale : 10000000, //10000
  snappingTolerance : 15, // tolerance for snapping in pixels
  title : ('Outils de saisie'),
  user_id : -1,
  emprise_id : -1,
  emprise_tablename : '',
  sessionActive : false,
  oleControl : null,
  modify_object_url : null,
  
  layerTreeRoot : null,
  listDisplayField : [],
  
  //hismail
  numerisationWind: null,
  infoWind: null,
  editionLayerNode: null,
  jsonAllColumnsTypesForWorkingTableName: null,
  recuperateSessionState: false,
  editionAjoutMode: false,
  nbr_of_add: 0,
  current_gid: [],
  isActiveSelectFeature: true,
  featureTypeForEdition: 'point',
  ar_enums_name: [],
  ar_all_enums_names_and_values: [],
  _blockedFields: [],
  notNullField: null,

  // methods
  initialize : function(serviceUrl, mapfile, options) {
      OpenLayers.Control.prototype.initialize.apply(this, options);
      this.mapfile = mapfile;
      this.serviceurl = serviceUrl;
      if(options.workingScale)
        this.workingScale = options.workingScale;
      if(options.callbackUrl)
        this.callbackUrl = options.callbackUrl;
      if(options.user_id)
        this.user_id = options.user_id;
      if (options.emprise_id)
        this.emprise_id = options.emprise_id;
      if (options.emprise_tablename)
        this.emprise_tablename = options.emprise_tablename;
      if (options.modify_object_url)
        this.modify_object_url = options.modify_object_url;
      this.geojson_format = new OpenLayers.Format.GeoJSON();
  },

  initializeUIComponents : function() {
    //obedel here
    //console.log("initialisation of ui components of " + this.CLASS_NAME);

    // Snap layer
    var snapLayerNodes = this.map.getLayerTreeManager().layerTreeStore.data.items;
    
    this.comboSnapLayer = Ext.getCmp('_comboSnapLayer');
    var snapLayerStore = this.comboSnapLayer.getStore();
    var data = [];
    for(var i=0; i<snapLayerNodes.length; i++) {

      if(typeof(snapLayerNodes[i].attributes.OlLayer) !== "undefined") {
        data.push([i, snapLayerNodes[i].data.text, snapLayerNodes[i]]);
      }
    }
    snapLayerStore.loadData(data);

    this.txtEditionLayer = Ext.getCmp('_txtEditionLayer');
    this.fieldSnappingTolerance = Ext.getCmp('_snappingTolerance');
    this.comboPreviousSessions = Ext.getCmp('_comboPreviousSessions');
    this.btnApplyOptions = Ext.getCmp('btnApplyOptions');
    this.btnStartEdition = Ext.getCmp('_btn_startEdition');
    this.btnEndEdition = Ext.getCmp('_btn_endEdition');
    this.btnRetrieveSession = Ext.getCmp('_btn_retrieveSession');
    this.toolsEdition = Ext.getCmp('toolsEdition');


    this.comboSnapLayer.on('select', function() {
      this.fieldSnappingTolerance.setValue(this.snappingTolerance);
    }, this);

    this.comboPreviousSessions.on('select', function() {
      this.btnRetrieveSession.setVisible(true);
    }, this);

    this.btnStartEdition.setHandler(function() {
      if(this.checkLock()) {
        //
      }
    }, this);

    this.btnRetrieveSession.setHandler(function() {
      if(this.recuperateSession()) {
        //
      }
    }, this);

    this.btnEndEdition.setHandler(function() {
      if(this.endEdition()) {
        //
      }
    }, this);
  },
  
  record : null,
  
  loadRecord : function(record){
  	if ( !record ){
  		record = Ext.create('Carmen.EditorManagerModel', {});
  	}
  	this.record = record;
  	this.numerisationWind.down('form').loadRecord(record);
  },

  refreshStoreForPreviousSessionsCombo: function(layerId) {
    if (!user_connected_details.isConnected) return;
    this.comboPreviousSessions.setValue(null); //hismail
    // Recuperate previous sessions
    Ext.Ajax.request({
      url: Routing.generate('editionmanager_allprevioussessions', {
        mapfile : this.mapfile,
        editionLayername : layerId
      }),
      success: function(response){
        //console.log('getAllPreviousSessions is : ', response.responseText);
        var response = Ext.decode(response.responseText);
        var previousSessionsStore = this.comboPreviousSessions.getStore();
        var data = [];
        for(var i = 0 ; i< response.length ; i++) {
          data.push([i, response[i].displaySession, response[i].valueSession]);
        }
        previousSessionsStore.loadData(data);
      },
      failure: function (response) { 
        Carmen.Util.handleAjaxFailure(response, this.title, true);
        this.current_gid = []; 
      },
      scope : this
    });
  },

  setMap : function(map) {
    OpenLayers.Control.prototype.setMap.apply(this, arguments);
  //  this.btn.addListener('toggle', Carmen.Util.buildExt2olHandlerToggle(this));
    this.map.app.ui.doLayout();

    // adding listeners to link window closing and control deactivation
    this.map.events.register('layerTreeLoaded', this, this.onLayerTreeLoaded);
  },
  
  editableLayers : {},
  isLayerEditable : function(layerId, withRight){
  	var rights = this.editableLayers[layerId];
  	if ( !rights ) return false;
  	if ( withRight ) return rights[withRight];
  	return true;
  },
  
  editInformation : function(layerId, record){
  	var config = this.editableLayers[layerId];
  	/*TODO*/
  	
  	var aliases = [];
  	var temp = {};
  	Ext.iterate(record.data, function(field, value){
  	  if ( /^\w+_\d+$/.test(field) ){
  	  	if ( !Ext.isDefined(temp[field.replace(/_\d+$/, '')]) )
  	  	  temp[field.replace(/_\d+$/, '')] = value;
  	  }
      aliases[field.replace(/_\d+$/, '')] = aliases[field.replace(/_\d+$/, '')] || [];
      aliases[field.replace(/_\d+$/, '')].push(field);
  	});
  	temp.gid = record.data.gid || record.data.fid;
  	//data = temp;
  	var feature = {
  		feature : {
  			attributes : temp,
  			record : record
  		}
  	};
    Ext.Ajax.request({
      url: Routing.generate('editionmanager_gettablecolumns', {
        mapfile : this.mapfile,
        editionLayername : layerId,
        gid : temp.gid
      }),
      success: function(response){
        response = Ext.decode(response.responseText);
      	this.setTableColumns(response.columns);
        this._blockedFields = [];
        if(user_connected_details['couche_type'] == -4) {
          Ext.Ajax.request({
            url: Routing.generate('editionmanager_getblockedfields', {
              mapfile : this.mapfile,
              editionLayername : layerId
            }),
            success: function(response){
              //console.log('response (getBlockedFields) is : ', response.responseText);
              response = Ext.decode(response.responseText);
              if(response.success) {
                for(var i=0; i< response.values_not_updates.length ; i++) {
                  this._blockedFields.push(response.values_not_updates[i]);
                }
              }
            },
            failure: function (response) { 
              Carmen.Util.handleAjaxFailure(response, this.title, true); 
            },
            scope : this
          });
        }
        this._blockedFields.push('gid');
        this.listDisplayField = this.getFieldName(config.node.attributes.infoFields);;
        
        var callbackSave = function(gPanelInformation_store, _feature, fieldsToModifiy){
          Ext.Ajax.request({
            url: Routing.generate('editionmanager_saveattributes', {
              mapfile : this.mapfile,
              editionLayername : layerId
            }),
            scope : this,
            success: function(response){
              var response = Ext.decode(response.responseText);
              //console.log('saveAttributes - response is : ', response);
              if(response.saveAttributes) {
                for(var i=0; i<gPanelInformation_store.data.length; i++) {
                  var field = gPanelInformation_store.getAt(i);
                  var fieldName = this.getColumnName(field.data['Libelle']);
                  if ( aliases[fieldName] ){
                  	for(var a=0; a<aliases[fieldName].length; a++){
                  		record.set(aliases[fieldName][a], field.data['Donnee']);
                  	}
                  }
                  
                }
                record.commit();

                gPanelInformation_store.removeAll();
                this.infoWind.removeAll();
                this.infoWind.close();
              }
            },
            failure: function (response) { 
              Carmen.Util.handleAjaxFailure(response, this.title, true); 
            },
            method : 'POST',
            params: {
              operation : 'saveAttributes',
              user_id : this.user_id,
              fieldsToModifiy: Ext.encode(fieldsToModifiy)
            } 
          });
        };
        
        
        ///////////////////////////////////////////////
        
        this.selectCallback(feature, Ext.Function.bind(callbackSave, this));
        
      },
      failure: function (response) { 
        Carmen.Util.handleAjaxFailure(response, this.title, true); 
      },
      scope : this
    });
    
  },
  
  isEnabled : false,
  onLayerTreeLoaded : function(){
  	var me = this;
  	var layerTreeManager = this.map.getControlsByClass('Carmen.Control.LayerTreeManager');
  	if ( !layerTreeManager.length ) return;
  	layerTreeManager = layerTreeManager[0];
  	if ( !layerTreeManager ) return;
  	var metadatas = [], ids = {};
  	var nodes = layerTreeManager.getNodesBy(function(node){
  		var result = (node.attributes.jsonLayerDesc) && (node.attributes.jsonLayerDesc.Extension.GEONETWORK_METADATA_ID);
  		if ( result ) {
    		metadatas.push(Ext.encode({
          ID : node.attributes.jsonLayerDesc.Extension.GEONETWORK_METADATA_ID,
          OBJET_TYPE : "dataset",
          OBJET_STYPE : "vector"
        }));
        ids[node.attributes.jsonLayerDesc.Extension.GEONETWORK_METADATA_ID] = node.attributes.layerName;
  		}
  		return result;
    });
    if ( metadatas.length ){
      var url = PRODIGE_VERIFY_RIGHTS_URL +'_multiple'//+ '?metadatas=' + GEONETWORK_METADATA_ID + '&TRAITEMENTS=EDITION&OBJET_TYPE=dataset';
      Ext.data.JsonP.request({
        url: url,
        params : {
        	"metadatas[]" : metadatas,
        	"TRAITEMENTS" : "EDITION",
        	"OBJET_TYPE" : "dataset"
        },
        method: 'GET',
        scope : this,
        success: function(response) {
          var editorButton = Ext.getCmp('editorButton');
          if ( !editorButton ) return;
        	var added = 0;
        	var metadata;
        	Ext.iterate(response, function(metadata_id, rights){
        		metadata = nodes.findBy(function(node){return node.attributes.jsonLayerDesc.Extension.GEONETWORK_METADATA_ID==metadata_id});
        		if ( !metadata ) return;
            if(rights['EDITION'] || rights['EDITION_AJOUT'] || rights['EDITION_MODIFICATION']) {
            	me.editableLayers[ ids[metadata_id] ] = Ext.apply({metadata_id:metadata_id, node : metadata}, rights);
              metadata.attributes.isEditable = true;
              metadata.attributes.infoEdition = rights;
              metadata.commit();
            	added++;
              editorButton.addLayer(metadata, rights);
              me.isEnabled = true;
            }
        	});
          editorButton.setVisible(added>0);
        },
        failure: function(response) {
          //console.log('failure - response is : ', response);
        }
      });
    }
  },
  
  loadLayers: function(layers) {
  	var me = this;
  	var temp = [];
  	layers.forEach(function(layer){
  		//if ( temp.length==0 )/*TODO REMOVE*/
  		temp.push(Ext.apply({}, layer.attributes));
  	})
  	this.layerStore = new Ext.data.JsonStore({
  		convertOnSet : false,
      fields: [{
        name : 'layer',
        convert : function(value, record){
          return record.data;
        }
      }, {
        name : 'layerId',
        convert : function(value, record){
          return record.data.layerName;
        }
      }, {
        name : 'data',
        convert : function(value, record){
          return record.data.linkedNodes[0].data.text;
        }
      }, {
        name : 'columns',
        convert : function(value, record){
          return me.getFieldName(record.data.infoFields);
        }
      }, {
        name : 'edition_ajout',
        convert : function(value, record){
          return !!record.data.infoEdition.EDITION || !!record.data.infoEdition.EDITION_AJOUT;
        }
      }, {
        name : 'edition_modif',
        convert : function(value, record){
          return !!record.data.infoEdition.EDITION || !!record.data.infoEdition.EDITION_MODIFICATION || !!record.data.infoEdition.EDITION_AJOUT;
        }
      }],
      
      idProperty : 'layerId',
      rootProperty : 'attributes',
      data : temp
    });

    // user_id
    this.user_id = (user_connected_details.userId != '') ? user_connected_details.userId : -1;
    if(this.user_id == -1) {
      Ext.Msg.alert(this.title, ("Vous n'êtes pas authentitifié. Les fonctionnalités de saisie en ligne ne sont pas disponibles"));
      return false;
    }

    this.openWindow(temp.length);

    if ( temp.length==1 ){
    	this.loadRecord(Ext.create('Carmen.EditorManagerModel', {layerId : temp[0].layerId}));
    	this.checkLock();
    } else {
      this.loadRecord();
    }
  },
  
  openWindow : function(nbEditables)
  {
  	if ( this.numerisationWind ) this.numerisationWind.close();
  	this.numerisationWind = null;
    this.numerisationWind = new Ext.Window({
      width: 500,
      x: document.body.clientWidth-500-10,
      y: 15,
      resizable:true,
      closeAction: 'destroy',
      listeners: {
      	scope : this,
        close: function(){
          if ( this.btnOpener ){
            this.btnOpener.toggle(false);
          }
        }
      },
      title : 'Saisie en ligne'+(nbEditables==1 ? ' (1 couche éditable)' :  ''),
      buttonAlign : 'center',
      buttons: [{
        id: '_btn_retrieveSession',
        text: ('Récupérer la session'),
        hidden: true
      }, {
        id: '_btn_startEdition',
        text: ('Débuter la saisie')
      }, {
        id: '_btn_endEdition',
        text: ('Terminer la saisie'),
        hidden: true
      }],
      items : [{
      	xtype : 'form',
        width: 500,
        bodyPadding: 10,
        fieldDefaults : {
        	labelSeparator : '',
          labelWidth : 200,
          width : '100%'
        },
        
        
        items : [{
          id:'_txtEditionLayer',
          xtype: 'combo',
          name : 'layerId',
          fieldLabel: ("Couche éditée"),
          loadingText: ('Chargement'),
          emptyText: ('Choississez une couche'),
          displayField: 'data',
          valueField: 'layerId',
          queryMode: 'local',
          editable : true,
          store : this.layerStore,
          listeners : {
          	change : this.onSelectLayer,
          	scope : this
          }
        }, {
        	hidden : !user_connected_details.isConnected,
          xtype: 'combo',
          id: '_comboPreviousSessions',
          name : 'from_session',
          fieldLabel: ("Repartir d'une précédente saisie"),
          loadingText: ('Chargement'),
          emptyText: ('Choississez une session'),
          listConfig : {emptyText: ('Aucune session précedente')},
          displayField: 'sessionName',
          valueField: 'sessionValue',
          queryMode: 'local',
          store : new Ext.data.SimpleStore({
            fields: ['id', 'sessionName', 'sessionValue'],
            data : [['', '',null]]
          })
        }, {
        	xtype : 'fieldset',
        	checkboxToggle  : true,
        	collapsed : true,
        	id : '_options',
        	title : 'Options',
          fieldDefaults : {
            labelSeparator : '',
            labelWidth : 200,
            width : 'auto'
          },
          onCheckChange: function (cmp, checked) {
            this.setExpanded(checked);
            if ( !checked ) this.cascade(function(elt){elt.reset && elt.reset()});
          },
        	items : [{
            width : '100%',
            xtype: 'combo',
            id: '_comboSnapLayer',
            name : 'snap_layer',
            fieldLabel: ("Couche d'accroche"),
            loadingText: ('Chargement'),
            emptyText: ('Choississez une couche'),
            displayField: 'title',
            valueField: 'node',
            queryMode: 'local',
            editable : true,
            store : new Ext.data.SimpleStore({
              fields: ['id', 'title', 'node'],
              data : [['', '',null]]
            })
          }, {
            width : '100%',
            xtype: 'numberfield',
            id:  '_snappingTolerance',
            name : 'snap_tolerance',
            fieldLabel: ("Tolérance d'accrochage")
//            listeners : {
//              change : this.onChangeSnapTolerance,
//              scope : this
//            }
          },{
          	width : '100%',
          	xtype : 'panel',
              hidden : true,
              id : 'btnApplyOptions',
          	tbar : ['->', {
              xtype : 'button',
              text : 'Appliquer les options',
              handler : this.onChangeSnapLayer,
              scope : this
          	}]
          }]
        }]

      }, {
      	hidden : true,
        id: 'toolsEdition',
        bodyStyle: "background: transparent;",
        width: '100%',
        height: 'auto',
        layout: {
        	type : 'vbox',
        	align : 'center',
        	pack : 'center'
        },
        tbar : ["->",{
          id: 'editorManager_button_Navigation',
          xtype: 'button',
          enableToggle: true,
//TODO            hidden : !edit_modif,
          tooltip: 'Déplacer la carte',
          toggleGroup: 'EditorManagerControl',
          dock: 'bottom',
          text: '<i class="fa icon-hand fa-2x"> </i>',
          toggleHandler:  function(btn, pressed, eopts) {
            if(pressed) {
              if(this.oleControl != null) {
                if(this.oleControl) {
                  var oleNavigationControl = this.oleControl.controls["Navigation"];
                  this.oleControl.editorPanel.activateControl(oleNavigationControl);
                }
              }
            }
          },
          scope : this
        }, {
          id: 'editorManager_button_selectFeature',
          xtype: 'button',
          enableToggle: true,
//TODO            hidden : !edit_modif,
          tooltip: 'Sélectionner',
          toggleGroup: 'EditorManagerControl',
          dock: 'bottom',
          text: '<i class="fa fa-pencil-square-o fa-2x"> </i>',
          toggleHandler : function(btn, pressed, eopts) {
            if ( !this.editLayer ) return;
            if(pressed) {
              this.editLayer.events.register('featureselected',
                  this,
                  this.editionSelectCallback
                );
              this.editLayer.events.register('featureunselected',
                  this,
                  this.editionUnSelectCallback
                );
              if(this.oleControl != null) {
                if(this.oleControl) {
                  var oleDrawPointControl = this.oleControl.controls["SelectFeature"];
                  this.oleControl.editorPanel.activateControl(oleDrawPointControl);
                }
              }
            }
            else {
              this.editLayer.events.unregister('featureselected',
                  this,
                  this.editionSelectCallback
                );
              this.editLayer.events.unregister('featureunselected',
                  this,
                  this.editionUnSelectCallback
                );
            }
          },
          scope : this
        }, {
          id: 'editorManager_button_modifyFeature',
          xtype: 'button',
          enableToggle: true,
//TODO            hidden : !edit_modif,
          tooltip: 'Modifier / Déplacer',
          toggleGroup: 'EditorManagerControl',
          dock: 'bottom',
          text: '<i class="fa fa-pencil fa-2x"> </i>',
          toggleHandler:  function(btn, pressed, eopts) {
            if(pressed) {
              if(this.oleControl != null) {
                if(this.oleControl) {
                  var oleModifyFeatureControl = this.oleControl.controls["ModifyFeature"];
                  this.oleControl.editorPanel.activateControl(oleModifyFeatureControl);
                }
              }
            }
          },
          scope : this
        }, {
          id: 'editorManager_button_deleteFeature',
          xtype: 'button',
          enableToggle: true,
//TODO            hidden : !edit_add,
          tooltip: 'Supprimer',
          toggleGroup: 'EditorManagerControl',
          dock: 'bottom',
          text: '<i class="fa fa-trash-o fa-2x"> </i>',
          toggleHandler:  function(btn, pressed, eopts) {
            if(pressed) {
              if(this.oleControl != null) {
                if(this.oleControl) {
                  var oleDeleteFeatureControl = this.oleControl.controls["DeleteFeature"];
                  this.oleControl.editorPanel.activateControl(oleDeleteFeatureControl);
                }
              }
            }
          },
          scope : this
        }, {
          id: 'editorManager_button_drawHole',
          xtype: 'button',
          enableToggle: true,
//TODO            hidden : !edit_modif,
          tooltip: 'Créer un trou',
          toggleGroup: 'EditorManagerControl',
          dock: 'bottom',
          text: '<i class="fa fa-scissors fa-2x"> </i>',
          toggleHandler:  function(btn, pressed, eopts) {
            if(pressed) {
              if(this.oleControl != null) {
                if(this.oleControl) {
                  //var oleModifyFeatureControl = this.oleControl.editorPanel.getControlsByClass("OpenLayers.Control.DrawHole")[0];
                  var oleDrawHoleControl = this.oleControl.controls["DrawHole"];
                  this.oleControl.editorPanel.activateControl(oleDrawHoleControl);
                }
              }
            }
          },
          scope : this
        }, {
          id: 'editorManager_button_drawPoint',
          xtype: 'button',
          enableToggle: true,
          //TODO type linéaire?
          //tooltip: ((this.featureTypeForEdition) && (this.featureTypeForEdition == 'polygon')) ? 'Dessiner un polygone' : 'Dessiner un point',
          tooltip: 'Ajouter',//(this.featureTypeForEdition) ? ( (this.featureTypeForEdition == 'polygon') ? "Dessiner un polygone" : ( (this.featureTypeForEdition == 'path') ? "Dessiner une polyligne" : "Dessiner un point") ) : "Dessiner un point",
          toggleGroup: 'EditorManagerControl',
          dock: 'bottom',
          text: '<i class="fa icon-point-thin fa-2x"> </i>',
          toggleHandler : function(btn, pressed, eopts) {
            if(this.oleControl != null) {
              if(this.oleControl) {
                var currentControl;
                if(this.featureTypeForEdition) {
                  if(this.featureTypeForEdition == 'polygon') {
                    currentControl = this.oleControl.controls["DrawPolygon"];
                  }
                  else if(this.featureTypeForEdition == 'path') {
                    currentControl = this.oleControl.controls["DrawPath"];
                  }
                  else {
                    currentControl = this.oleControl.controls["DrawPoint"];
                  }
                }
                else {
                  currentControl = this.oleControl.controls["DrawPoint"];
                }
                if ( currentControl ){
                	pressed && this.oleControl.editorPanel.activateControl(currentControl);
                	Carmen.Util.buildExt2olHandlerToggle(currentControl)(btn, pressed, eopts);
                }
              }
            }
          },
          scope : this
        }, "->"]
      }]
    }).show();
    this.initializeUIComponents();
  },
  
  onSelectLayer : function(combo, layerId){
  	var layer = this.layerStore.findRecord('layerId', layerId);
    this.editionLayerNode = null;
  	if ( !layer ) return;
  	this.editionLayerNode = layer.get('layer');

    // Initialisze snap combo
    this.comboSnapLayer.setValue(null);

    // Fill previous sessions combo
    this.refreshStoreForPreviousSessionsCombo(layerId);

    this._blockedFields = [];
    if(user_connected_details['couche_type'] == -4) {
      Ext.Ajax.request({
        url: Routing.generate('editionmanager_getblockedfields', {
          mapfile : this.mapfile,
          editionLayername : this.editionLayerNode.layerName
        }),
        success: function(response){
          //console.log('response (getBlockedFields) is : ', response.responseText);
          response = Ext.decode(response.responseText);
          if(response.success) {
            for(var i=0; i< response.values_not_updates.length ; i++) {
              this._blockedFields.push(response.values_not_updates[i]);
            }
          }
        },
        failure: function (response) { 
          Carmen.Util.handleAjaxFailure(response, this.title, true); 
        },
        scope : this
      });
    }
    this.listDisplayField = layer.get('columns');

    this._blockedFields.push('gid');

    
    var toolbar = {
      'Navigation' : 'edition_modif',
      'deleteFeature' : 'edition_ajout',
      'drawHole' : 'edition_modif',
      'modifyFeature' : 'edition_modif',
      'selectFeature' : 'edition_modif',
      'drawPoint' : 'edition_ajout'
    };
    Ext.iterate(toolbar, function(btnId, property){
    	var btn = Ext.getCmp('editorManager_button_'+btnId);
    	if ( !btn ) return;
    	btn.setVisible( layer.get(property) );
    })
  },

  deactivate: function() {
    if (this.sessionActive) {
      this.endEdition( {nextCallBack : this.finishDeactivate });
    }
    return this.finishDeactivate();
  },

  finishDeactivate : function() {
    var res = !this.sessionActive;
    if (!this.sessionActive) {
      res = OpenLayers.Control.prototype.deactivate.apply(this, arguments);
      this.sessionParamPanel.disable();
      this.sessionParamPanel.collapse();
      this.numerisationWind.close();
    }
    return res;
  },

  checkCurrentScale : function () {
    var res = true;
    if (!this.map || this.map.getScale() > this.workingScale) {
        Ext.Msg.alert(this.title, ("La saisie en ligne n'est possible qu'au delà du ") + '1/' + this.workingScale);
        res = false;
    }
    return res;
  },

  checkLock : function() {
    //retrieving parameters
    if (this.editionLayerNode == '' || this.editionLayerNode == null) {
      Ext.Msg.alert(this.title, ("Veuillez choisir une couche"));
      return false;
    }
    this.editionLayer = this.editionLayerNode.OlLayer;
    this.editionLayername = this.editionLayerNode.layerName;

    Ext.MessageBox.isHidden() && Ext.MessageBox.show({
      msg: 'Chargement en cours, merci de patienter...',
      progressText: 'Chargement...',
      width: 150,
      wait: true,
      waitConfig: {interval:20}
    });

    var extent = this.map.getExtent();
    Ext.Ajax.request({
    	method : 'POST',
      url: Routing.generate('editionmanager_checklock', {
        mapfile : this.mapfile,
        editionLayername : this.editionLayername
      }),
      success: this.callback_checkLock,
      /*success: function(response) {
        console.log('checkLock is : ', response.responseText);
      },*/
      failure: function (response) {
        Carmen.Util.handleAjaxFailure(response, this.title, true); 
      },
      scope : this,
      params: {
        isView: (user_connected_details['couche_type'] == -4) ? true:false,
        extent : extent.toString()
      }
    });
    return true;
  },

  callback_checkLock : function(response) {
    response = Ext.decode(response.responseText);

    if(!response.success) {
      Ext.Msg.alert(this.title, response.errmsg || "Problème dans l'accès aux données");
      return;
    }

    this.startEdition();
  },
  
  transformNodeInfo : function(atts) {
    var cloneAtts = Carmen.Util.clone(atts);
    cloneAtts.cls = 'none';
    cloneAtts.infoData = null;  	
    cloneAtts.listeners = {};
    cloneAtts.expandable = true;
    if (cloneAtts.type == Carmen.Control.LayerTreeManager.NODE_LAYER) {
      cloneAtts.icon = '/IHM/images/NoTreeIcon.gif';
      cloneAtts.iconCls = 'cmnInfoGridZoomIcon';
    }
    cloneAtts.text = atts.layerAlias!=null ? atts.layerAlias : atts.text;
    return cloneAtts;
  },
  getLayerTreeManager : function() {
    if (this.layertreeManager == null) {
      var controls = this.map
      .getControlsByClass('Carmen.Control.LayerTreeManager');
      this.layerTreeManager = controls.length == 0 ? null : controls[0];
    }
    return this.layerTreeManager;
  },
  filterNodeLayerQueryable :  function(node) {
    var res = true;
    //console.log(node.attributes.text);
    res = (node.attributes!=null) && 
    ('type' in node.attributes) && 
    ((node.attributes.type == Carmen.Control.LayerTreeManager.NODE_GROUP && node.hasChildNodes() && node.getDepth()>0) ||
        (node.attributes.type == Carmen.Control.LayerTreeManager.NODE_LAYER && node.attributes.OlLayer.getVisibility(node.attributes.layerName)
            && ((node.attributes.infoFields && node.attributes.infoFields.length>0) ||
                (node.attributes.OlLayer instanceof Carmen.Layer.WMSGroup)) 
        ));   
    return res;
  },
  performQueries : function() {
    var list_params=[];
    var control = this;
    
    this.layerTreeRoot = this.getLayerTreeManager().getLayerTree().clone(this.transformNodeInfo);
    this.layerTreeRoot.filter(this.filterNodeLayerQueryable);


    var isLayerNode = function(n) { 
      return ('type' in n.attributes && (n.attributes.type == Carmen.Control.LayerTreeManager.NODE_LAYER)); 
    }; 
    var nodes = this.layerTreeRoot.collectChildNodes(isLayerNode);

    if (nodes.length==0) {
      //this.headerPanel.setTitle('Aucun résultat');
      //this.gridInfoPanel.hide();
      Ext.Msg.alert("Outil d'information", "Aucune donnée n'est interrogeable avec cet outil");
    } else {
      var ms_nodes = {};
      var wms_nodes = [];
      var wms_layers = [];

      for (var i=0; i<nodes.length; i++) {
        var layer = nodes[i].attributes.OlLayer;
        if (layer instanceof Carmen.Layer.WMSGroup) { 
          wms_nodes.push(nodes[i]);
          Carmen.Util.set_add(wms_layers, layer);
        } else {
          var mapfile = nodes[i].attributes.OlLayer.mapfile;
          if (mapfile) {
            if (!ms_nodes[mapfile]){
              ms_nodes[mapfile] = [];
            }
            ms_nodes[mapfile].push(nodes[i]);  
          }
        }
      }
      // cancel all pending requests
      Ext.Ajax.abort();
      this.queriesSent = 0;
      this.totalCount = 0;
      this.fullExtent = null;
      // launching requests

      this.map.app.ui.delAllInfoPanel();
      
      
      for (var m in ms_nodes) {
        if (ms_nodes[m].length>0){ 
          this.getFieldName(ms_nodes[m]);
        }
      }
    }
  },
  
  getFieldName : function(fieldsList){
    var briefFieldsList = [];
    var baseQueryURLList = [];
    var list = Carmen.Util.chompAndSplit(fieldsList,';');
    fieldsDesc = new Array();
    for (var j=0; j<list.length; j++) {
      var desc  = Carmen.Util.decodeCarmenFieldDesc(list[j], baseQueryURLList[i]);
      fieldsDesc[j]=desc;
    }

    return fieldsDesc;
    
      this.listDisplayField.push(fieldsDesc);
    

  },
  logvar : function(value, name, tab) {
    var logv = (value==null) ? "null" : value;
    tab[name] = logv;
  },

  printlog : function(tab) {
    Ext.Ajax.request({
      url: '/services/GetInformation/log.php',
      method: 'POST',
      success : function (response) { 
        Ext.Msg.alert(control.windowTitle, response.msg ? response.msg : "Commande log enregistrée");
      },
      failure: function (response) { 
        Ext.Msg.alert(control.windowTitle, 'Problème dans l\'écriture du fichier Log');
      },
      params : {
        "log_array" : Ext.encode(tab)
      }
    });
  },
  
  startEdition : function() {
    
    //retrieving parameters
    if (this.editionLayerNode == '') {
      Ext.Msg.alert(this.title, ("Veuillez renseigner la couche"));
      return false;
    }

    this.editionLayer = this.editionLayerNode.OlLayer;
    this.editionLayername = this.editionLayerNode.layerName;

    var snapLayerNode = this.comboSnapLayer.getValue();
    if((snapLayerNode != null) && (snapLayerNode != '')) {
      this.snapLayername = snapLayerNode.attributes.layerName;
    } else {
      this.snapLayername = null;
    }

    if((this.snapLayername != null) && (this.editionLayername == this.snapLayername)) {
      Ext.Msg.alert('Attention', 'La couche d\'accroche doit être différente de la couche à éditer.');
      return false;
    }
    else {
      var extent = this.map.getExtent();

      // launch patience
      Ext.MessageBox.isHidden() && Ext.MessageBox.show({
        msg: 'Chargement en cours, merci de patienter...',
        progressText: 'Chargement...',
        width: 150,
        wait: true,
        waitConfig: {interval:20}
      });
      this.lastSession =  {
        url : Routing.generate('editionmanager_startedition', {
          mapfile : this.mapfile,
          editionLayername : this.editionLayername
        }),
        params : {
          editionTableSession : this.recuperateSessionState ? this.comboPreviousSessions.getValue():"",
          snapLayername : (this.snapLayername != null) ? this.snapLayername : "",
          extent : extent.toString()
        },
        success : true
      };
      
      Ext.Ajax.request({
      	timeout : 60*60*1000,
        url: this.lastSession.url,
        success: this.callback_startEdition,
        /*success: function(response){
          console.log('startEdition - response is : ', response.responseText);
        },*/
        failure: function (response) { 
        	this.lastSession.success = false;
          // end patience
          Ext.MessageBox.hide();
          Carmen.Util.handleAjaxFailure(response, this.title, true); 
        },
        scope : this,
        method : 'POST',
        params: this.lastSession.params
      });
      return true;
    }
  },

  // startedition answer callback
  setTableColumns : function (columns) {
  	
      this.jsonAllColumnsTypesForWorkingTableName = columns;
      for(var i=0; i<this.jsonAllColumnsTypesForWorkingTableName.length; i++) {
        if(this.jsonAllColumnsTypesForWorkingTableName[i]['data_type'] == "USER-DEFINED") {
          this.ar_enums_name.push(this.jsonAllColumnsTypesForWorkingTableName[i]['udt_name']);
        }
      }
      //console.log('this.ar_enums_name is : ', this.ar_enums_name);

      var str_enums_names = "";
      if ( this.ar_enums_name.length ){
        str_enums_names = this.ar_enums_name.join(',')
      }
      //console.log('str_enums_names is : ', str_enums_names);

      this.ar_all_enums_names_and_values = [];
      if ( str_enums_names ){
        // get all values from enums_names
        Ext.Ajax.request({
        	async : false,
          url: Routing.generate("editionmanager_allenumsvaluesbynames", {enums_names:str_enums_names}),
          success: function(response) {
            var response = Ext.decode(response.responseText);
            if(response.success) {
              this.ar_all_enums_names_and_values = response.enums_values;
            }
          },
          failure: function (response) { 
            Carmen.Util.handleAjaxFailure(response, this.title, true); 
          },
          scope : this
        });
      }
  },

  // startedition answer callback
  callback_startEdition : function (response) {

    response = Ext.decode(response.responseText);

    if(!response.edition) {
      // end patience
      Ext.MessageBox.hide();
      this.workingTablename = null;
      Ext.Msg.alert('Attention', 'Les données de l\'emprise courante sont trop importantes pour démarrer la saisie. Merci de démarrer la session à plus grande échelle.', function(){
        if ( this.sessionActive ){
          this.endEdition();
        }
      }, this);
      return false;
    }
    else {

      // openning session
      this.sessionActive = true;
      
      // remove cross for numerisation window
      this.numerisationWind.tools.close.setStyle({display:"none"});

      this.setTableColumns(response.allColumnsTypesForWorkingTableName);

      if(!response.editionData || response.editionData == "") {
        Carmen.Util.handleAjaxFailure(response, this.title, true);
        return false;
      }

      this.workingTablename = response.workingTablename;

/*
      if(this.snapLayer) {
        this.snappingControl && this.snappingControl.removeTargetLayer(this.snapLayer);
        this.map.removeLayer(this.snapLayer);
        this.snapLayer.destroy();
        this.snapLayer = null;
      }
  
      if(this.verticesLayer) {
        this.verticesLayer.destroy();
        this.verticesLayer = null;
      }
  
      if(this.snappingControl) {
        this.snappingControl.deactivate();
        this.snappingControl.targets = null;
      	this.map.removeControl(this.snappingControl);
      	this.snappingControl = null;
      }
    */
      var sourceLayers = [];
      if(response.snapData && response.snapData != "") {
        this.snapLayer = new OpenLayers.Layer.Vector('Couche d\'accroche');
        this.snapLayer.addFeatures(this.geojson_format.read(response.snapData)); 
        sourceLayers.push(this.snapLayer);
      }

      if(this.btnDrawPoint == null) {
        this.btnDrawPoint = Ext.getCmp('editorManager_button_drawPoint');
      }

      this.featureTypeForEdition = response.featureTypeEdition;

      switch(this.featureTypeForEdition||"point"){
        case "polygon" :
          this.btnDrawPoint.setText("<i class=\"fa icon-polygon fa-2x\"> </i>");
          //this.btnDrawPoint.setTooltip("Dessiner un polygone");
        break;
        case "path" :
          this.btnDrawPoint.setText("<i class=\"fa icon-polyline fa-2x\"> </i>");
          //this.btnDrawPoint.setTooltip("Dessiner une polyligne");
        break;
        default :
          this.btnDrawPoint.setText("<i class=\"fa icon-point-thin fa-2x\"> </i>");
          //this.btnDrawPoint.setTooltip("Dessiner un point");
        break;
      }

      this.oleControl = new OpenLayers.Editor(this.map, {
        activeControls: (response.featureTypeEdition == 'polygon') ?
            ['Navigation', 'Separator', 'SelectFeature', 'ModifyFeature', 'DeleteFeature', 'DrawHole'] :
            ['Navigation', 'Separator', 'SelectFeature', 'ModifyFeature', 'DeleteFeature'],
        featureTypes: [response.featureTypeEdition],
        sourceLayers: sourceLayers//,
        //All availables activeControls & featureTypes
        //activeControls: ['Navigation', 'SnappingSettings', 'CADTools', 'Separator', 'DeleteFeature', 'TransformFeature', 'SelectFeature', 'Separator', 'DrawHole', 'ModifyFeature', 'Separator'],
        //featureTypes: ['polygon', 'path', 'point'],
      });

      this.oleControl.startEditMode();
      this.editLayer = this.oleControl.editLayer;

      // Blocké l'ajout dans le cas des vues pour l'instant
      if(user_connected_details['couche_type'] == -4) {
        this.btnDrawPoint.setVisible(false);
      }
      //  Bloqué l'option du "draw hole" si le "featureTypeForEdition" n'est pas un polygon
      if(this.btnDrawHole == null) {
        this.btnDrawHole = Ext.getCmp('editorManager_button_drawHole');
      }
      if(this.featureTypeForEdition != 'polygon') {
        this.btnDrawHole.setVisible(false);
      }

      // changing default style so that edit -> red, snap -> blue
      this.editLayer.styleMap.styles['default'] = this.editStyleDefault;



      if(this.snapLayer != null) {
        this.snapLayer.styleMap.styles['default'] = this.snapStyleDefault;
        this.snapLayer.styleMap.styles.select = this.snapStyleDefault;
        // redraw snap layer to apply the new style
        this.snapLayer.redraw();

        var arrayOfPointsInWKT = [];
        this.verticesLayer = new OpenLayers.Layer.Vector('Vertice');
        //this.verticesLayer.styleMap.styles.defaults = this.verticesStyleDefault;

        var vert = this.snapLayer.features;
        var point, vertices;
        var features = new Array();
        for(var i = 0; i < vert.length; i++) {
          vertices = vert[i].geometry.getVertices();
          for(var j = 0; j < vertices.length; j++) {
            features.push(new OpenLayers.Feature.Vector(new OpenLayers.Geometry.Point(vertices[j].x,vertices[j].y)));
          }
        }
        this.verticesLayer.addFeatures(features); 
        this.map.addLayer(this.verticesLayer);

        this.configureSnapping(
          this.editLayer,
          [this.editLayer, this.snapLayer],
          this.fieldSnappingTolerance.getValue()
        );
        /*var snapControle = new OpenLayers.Control.SelectFeature( this.snapLayer, {vertexRenderIntent: "vertex"});
        this.map.addControl(snapControle);
        snapControle.activate();
          var snapControle = new OpenLayers.Control.ModifyFeature( this.snapLayer, {vertexRenderIntent: "vertex"});
          this.map.addControl(snapControle);
          snapControle.activate();*/
      }
      //TODO privilégier plutot de ne pas charger les données
      this.editionAjoutMode = (this.editableLayers[this.editionLayername].EDITION_AJOUT==1 && this.editableLayers[this.editionLayername].EDITION==false);
      if(!this.editionAjoutMode) {
        this.editLayer.addFeatures(this.geojson_format.read(response.editionData));  
      }

      // end patience
      Ext.MessageBox.hide();

      /*// hooking on edition steps
      this.editLayer.events.register('afterfeaturemodified',
        this,
        this.editionModifyCallback
      );*/
      // hooking on edition steps
      this.editLayer.events.register('beforefeaturemodified',
        this,
        function(arg){
        	arg.feature.oldGeometry = arg.feature.geometry.clone(null);
        }
      );
      this.editLayer.events.register('featuremodified',
        this,
        this.editionModifyCallback
      );
      this.editLayer.events.register('featureadded',
        this, 
        this.editionAddCallback
      );
      this.editLayer.events.register('featureremoved', 
        this,
        this.editionDelCallback
      );

      this.txtEditionLayer.setReadOnly(true);
      if ( !this.comboPreviousSessions.getValue() )this.comboPreviousSessions.emptyText = ("Nouvelle session d'édition");
      this.comboPreviousSessions.applyEmptyText();
      this.comboPreviousSessions.setReadOnly(this.layerStore.getCount()>1);
      this.toolsEdition.show();
      this.btnEndEdition.show();
      this.btnApplyOptions.show();
      this.btnStartEdition.hide();

      //Active the new shape tool by default
      var activeBtn = Ext.ComponentQuery.query('[id^=editorManager_button]{isVisible()}:last');
      if (activeBtn.length){
        activeBtn[0].toggle(false, true);
        activeBtn[0].toggle(true);
      }
      

      Carmen.Util.handleAjaxFailure(response, this.title, true);
    }
  },
  
  onChangeSnapTolerance : function(input, newValue, oldValue){
  	if ( this.snappingControl ){
  		var targets = [].concat(this.snappingControl.targets);
  		for(var i=0; i<targets.length; i++){
  			var target = targets[i];
  			this.snappingControl.removeTarget(target);
  			this.snappingControl.addTarget({layer : target.layer, tolerance : newValue});
  		}  		
  	}
  },
  
  onChangeSnapLayer : function(input, newValue, oldValue){
  	if ( !this.editLayer ) return;
    var _comboSnapLayer = Ext.getCmp('_comboSnapLayer');
  	var _snappingTolerance = Ext.getCmp('_comboSnapLayer');
  	Ext.Msg.confirm("Changement de couche d'accroche", "Une saisie est en cours. " +
  			"<br>Le changement de couche d'accroche aura pour effet" +
  			(user_connected_details.isConnected 
  			? " de : <li>sauvegarder la saisie en cours</li><li>créer une nouvelle session d'édition</li>"
  			: " d'enregistrer définitivement la saisie en cours.")
  			+
  			"<br>Confirmez-vous la prise en compte de ce changement ?",
  			function(confirm){
  				if ( confirm!="yes" ) {
  					_comboSnapLayer.suspendEvent('change');_comboSnapLayer.setValue(oldValue); _comboSnapLayer.resumeEvent('change');
  				  return;
  				}
  				this.saveEdition(true, true);
  			}, this);
  },

  recuperateSession: function() {

    this.btnStartEdition.setVisible(false);
    this.btnRetrieveSession.setVisible(false);
    this.btnEndEdition.setVisible(true);

    this.recuperateSessionState = true;

    if(this.checkLock()) {
      //
    }
  },

  configureSnapping : function (editionLayer, snapLayers, tolerance) {
    var targets = [];
    for (var i = 0; i <  snapLayers.length; i++) {
        targets.push({
            layer:  snapLayers[i],
            tolerance: tolerance
        });
    }
    // get the ole snapping control if defined
    var controls = this.map.getControlsByClass('OpenLayers.Control.Snapping');
    var snappingControl = controls.length == 0 ? null : controls[0];
    if (snappingControl != null) {
      this.snappingControl = snappingControl;
      snappingControl.deactivate();
      snappingControl.setLayer(editionLayer);
      snappingControl.setTargets(targets);
      snappingControl.activate();
      return;
    }
    snappingControl = new OpenLayers.Control.Snapping({
      layer: editionLayer,
      targets: targets
    });
    this.snappingControl = snappingControl;
    
    this.map.addControl(snappingControl);
    return snappingControl.activate();
  },

  contraintTypeFromGeometry : function(geom) {
    var res = 'Point';
    var geomClassName = geom.CLASS_NAME.replace("OpenLayers.Geometry.","");
    switch(geomClassName) {
      case 'Polygon', 'MultiPolygon':
        res = 'Polygon';
      break;
      case 'LineString', 'MultiLineString':
        res = 'LineString';
      break;
      default :
        res = 'Point';
      break;
    }
    return res;
  },

  // checkFeature answer callback
  callback_checkFeature : function (response) {

    response = Ext.decode(response.responseText);

    var is_valid = response.check;
    var reject = response.reject;
    var gid = parseInt(response.gid);

    var feature = this.editLayer.getFeaturesByAttribute('gid', gid);
    if(feature.length > 0) {
      feature = feature[0];
      feature.attributes['is_valid'] = is_valid;
      this.nextCallBackParams = this.nextCallBackParams || {};
      this.nextCallBackParams.feature = feature;
    }
    else {
      if(this.nextCallBackParams != null && this.nextCallBackParams.feature) {
        this.nextCallBackParams.feature.attributes['is_valid'] = is_valid;
      }
    }

    if(!is_valid) {
      Ext.Msg.alert(this.title, response.msg);
    }
      if(this.nextCallBack && !reject) {
        this.nextCallBack(this.nextCallBackParams);
      }
      if ( reject && this.nextCallBackParams != null && this.nextCallBackParams.feature ){
      	if ( !gid ) this.editLayer.removeFeatures([this.nextCallBackParams.feature])
      	else if ( this.nextCallBackParams.feature.oldGeometry ){
      		this.nextCallBackParams.feature.move(this.nextCallBackParams.feature.oldGeometry);
      		this.nextCallBackParams.feature.oldGeometry = null;
      	}
      }
  },

  editionAddCallback : function(obj) {
    var feature = obj.feature;
    //console.log('editionAddCallback - feature is : ', feature);

    this.nextCallBack = this.addCallback;
    this.nextCallBackParams = {feature : feature};

    Ext.Ajax.request({
    	method : 'POST',
      url: Routing.generate('editionmanager_checkfeature', {
        mapfile : this.mapfile,
        editionLayername : this.editionLayername,
        snapLayername : this.snapLayername||"null"
      }),
      success: this.callback_checkFeature,
      failure: function (response) { 
        Carmen.Util.handleAjaxFailure(response, this.title, true); 
      },
      scope : this,
      params: {
        gid : null,
        geometry : feature.geometry.toString() || null,
        constraintType : this.contraintTypeFromGeometry(feature.geometry)
      }
    });
  },
  showUploadPicture : function(store, record){
    var me=this;
    if (this.imageWind && this.imageWind.isVisible()){
    	this.imageWind.close();
      this.imageWind = null;
    }
    var gid = store.findRecord('column_name', 'gid');
    if ( gid ){
    	gid = gid.get('Donnee');
    }
    var image_name = [record.get('column_name'), gid].join('_');
    /*
    for(var index in store.data.items){
      if(store.data.items[index].data.Type=='image'){
        this.name_image=store.data.items[index].data.Donnee; 
        this.name_image=this.name_image==undefined?"":this.name_image;
      }
    }*/
    this.imageWind = new Ext.Window({
      id : 'image_window',
      width:400,
      height: 150,
      constrain : true,
      plain: true,
      title: '',
      autoDestroy :true,
      resizable:true,
      layout: 'accordion',
      closeAction :'destroy'
    });

    var form = Ext.create('Ext.form.Panel', {
      height: 145,
      bodyPadding: 10,
      defaultType: 'textfield',
      value:"test",
      text:'filename',
      items: [{
        xtype: 'filefield',
        name: image_name,
        text:'filename',
        value:"test 2",
        fieldLabel: 'Image',
        labelWidth: 50,
        msgTarget: 'side',
        allowBlank: false,
        anchor: '100%',
        inputAttrTpl :'value=\"'+(record.get('Donnee') || '')+'"',  
        buttonText: 'Parcourir ...'
        
      }],

      // Reset and Submit buttons
      buttons: [{
        text: 'Effacer',
        handler: function() {
          this.up('form').getForm().reset();
        }
      }, {
        text: 'Charger',
        handler: function() {
          var form = this.up('form').getForm();
          if(form.isValid()){
            form.submit({
              url: Routing.generate('editionmanager_uploadpicture', {
                mapfile : me.mapfile,
                editionLayername : me.editionLayername,
                workingTablename : me.workingTablename,
                post_name : image_name
              }),
              waitMsg: 'Chargement en cours...',
              success: function(fp, o) {
              	
                Ext.Msg.alert('Chargement', 'Votre image "' + o.result.name.replace(image_name+'_', "") + '" a bien été chargée.');
                me.imageWind.close();
                
                var field = Ext.getCmp(record.get('column_name'));
                field && field.setValue(o.result.file);
                record.set('Donnee', o.result.file);
                record.set('DonneeName', o.result.name);
                record.commit();
              },
              failure: function(form, action) {
                //console.log(action.result);
                Ext.Msg.alert('Failed', action.result ? action.result.error : 'No response');
                me.imageWind.close();
              }
            });
          }
        }
      }
      ]
    });
    this.imageWind.setTitle('Chargement de l\'image');
    this.imageWind.removeAll();
    this.imageWind.add(form);
    this.imageWind.show();
  },
  addCallback : function(params) {
    var feature = params.feature;
    this.newFeature = feature;
    Ext.Ajax.request({
      url: Routing.generate('editionmanager_editstate', {
        mapfile : this.mapfile,
        editionLayername : this.editionLayername,
        workingTablename : this.workingTablename,
        state : 'new'
      }),
      success: function (response) {
        this.callback_add(response, params);
      },
      /*success: function(response){
        console.log('addCallback - response is : ', response.responseText);
      },*/
      failure: function (response) { 
        Carmen.Util.handleAjaxFailure(response, this.title, true); 
      },
      scope : this,
      params: {
        nbr_of_add: ++ this.nbr_of_add,
        geometry : feature.geometry.toString(),
        constraintType : this.contraintTypeFromGeometry(feature.geometry)
      }
    });
  },

  callback_add : function (response, params) {
    response = Ext.decode(response.responseText);
    //console.log('callback_add - response is : ', response);
    if(response.success) {
      this.newFeature.data.gid = response.gid;
      this.newFeature.attributes.gid = response.gid;
      this.selectCallback(params);
    }
    else {
      Ext.Msg.alert(this.title, ("Problème dans l'enregistrement de la géométrie"));
      }
  },

  editionUnSelectCallback: function(obj) {
    this.isActiveSelectFeature = true;
  },

  editionSelectCallback: function(obj) {
    if(this.isActiveSelectFeature) {
      var feature = obj.feature;
      // console.log('editionSelectCallback - feature is : ', feature);
      this.isActiveSelectFeature = false;
      this.selectCallback(obj);
    }
    else {
      Ext.Msg.alert('Attention', 'Le premier objet sélectionné est seulement pris en compte ...');
    }
  },
  isVisibleColumn : function(column_name){
    if(column_name=="gid"){
      return column_name;
    }
    for(var i=0; i<this.listDisplayField.length; i++){
        if(this.listDisplayField[i].name==column_name){
          return this.listDisplayField[i].alias;
        }
    }
    return null;
  },
  /**
  * Rècupère le nom de colonne sql en fonction de l'alias
  */
  getColumnName : function(alias){
    if(alias=="gid"){
      return alias;
    }
    for(var i=0; i <this.listDisplayField.length; i++){
      if(this.listDisplayField[i].alias==alias){
        return this.listDisplayField[i].name;
      }
    }
    return null;
  },
  selectCallback : function(params, callbackSave) {
   
    var feature = params.feature;
    var me = this;

    var gPanelInformation_store = Ext.create('Ext.data.JsonStore', {
      fields:['Libelle', 'Donnee', 'Type', 'Default_Value', 'Udt_Name', 'Arity', 'Length', 'Nullable'],
      data: []
    });
    for(var i=0; i<this.jsonAllColumnsTypesForWorkingTableName.length; i++) {
      var column_name = this.jsonAllColumnsTypesForWorkingTableName[i]['column_name'];

      var libelle=this.isVisibleColumn(column_name);
      var infosNotIncluded = ['the_geom', 'test', 'locked', 'is_valid'];
      
      if(infosNotIncluded.indexOf(column_name) == -1 && libelle != null) {
        var type=this.jsonAllColumnsTypesForWorkingTableName[i]['data_type'];
        if(this.jsonAllColumnsTypesForWorkingTableName[i]['domain_name']!=null){
          type=this.jsonAllColumnsTypesForWorkingTableName[i]['domain_name'];
        }
        gPanelInformation_store.add({
          'column_name': column_name,
          'Libelle': libelle,
          'Donnee': feature.attributes[column_name],
          'Type': type,
          'Default_Value': this.jsonAllColumnsTypesForWorkingTableName[i]['column_default'],
          'Udt_Name': this.jsonAllColumnsTypesForWorkingTableName[i]['udt_name'],
          'Arity': null,//this.jsonAllColumnsTypesForWorkingTableName[i]['object_type'],/*TODO colonnes à choix multiple : activer cette valeur*/
          'Length': this.jsonAllColumnsTypesForWorkingTableName[i]['character_maximum_length'],
          'Nullable': this.jsonAllColumnsTypesForWorkingTableName[i]['is_nullable']
        });
      }
    }
    
    //console.log('gPanelInformation_store is : ', gPanelInformation_store);
    
    Ext.define('test', {
      extend: 'Ext.button.Button',
      xtype: 'pressmebutton',
      text: 'Press Me'
    });
   
    var getItem = function(record){
    	var itemValue;
    	var fieldLabel = record.get('Libelle');
      //if(record.get('Libelle') == 'gid') {
      if(me._blockedFields.indexOf(fieldLabel) != -1) {
        var activeField = {
          xtype: 'displayfield',
          name : 'readonly',
          width: 150,
          value : record.get('Donnee')
        };
      }
      else {
        switch(record.get('Type')) {
          case 'image' :
          {
            activeField = {
              src : ((record.get('Donnee') != null) ? record.get('Donnee').trim() : ''),
            	xtype : 'fieldcontainer',
            	setValue : function(value){
            		var img = this.down('image');
            		img && img.setSrc(value);
            	},
            	id : record.get('column_name'),
            	items : [{
                xtype : 'button',
                text : 'Charger une image',
                listeners:{
                  'click':{
                    fn:function(){
                      me.showUploadPicture(gPanelInformation_store, record);
                    }
                  }
                }
              }, {
              	 src : ((record.get('Donnee') != null) ? record.get('Donnee').trim() : ''),
            	   xtype : 'image', autoEl : {tag : 'img', style: 'max-height:100px'}, id : 'img_'+record.get('column_name'), maxWidth : 100, maxHeight : 100
            	}]
            };
          };
          break;
          
          case 'character varying' :
          case 'character' :
          case 'text' :                 
          {
            activeField = {
              xtype: 'textfield',
              name : 'varchar',
              width: 150,
              maxLength : (record.get('Length') != null) ? record.get('Length') : 255
              
            };
            itemValue = ((record.get('Donnee') != null) ? record.get('Donnee').trim() : ( (record.get('Default_Value') != null) ? record.get('Default_Value').split('::')[0].substr(1, (record.get('Default_Value').split('::')[0]).length -2) : ''));
            record.set('Donnee', itemValue);
            record.commit();
          }; 
          break;
          
          case "integer":
          case "double precision" :
          case "numeric":
          case "smallint":
          case "real" :
          case "bigint" : 
          {
            activeField = {
              xtype : 'numberfield',
              name : 'number',
              width: 150
            };
            if ( record.get('Length') ) activeField.maxLength = record.get('Length');
            
            itemValue = (record.get('Donnee') != null) ? record.get('Donnee') : ( (record.get('Default_Value') != null) ? record.get('Default_Value').split('::')[0].substr(1, (record.get('Default_Value').split('::')[0]).length -2) : '');
            record.set('Donnee', itemValue);
            record.commit();
          }; 
          break;
          
          case 'date': 
          {
            activeField = {
              xtype : 'datefield',
              name : 'date',
              width: 150,
              format: 'd/m/Y'
            };
            
            if((record.get('Default_Value') != null) && (record.get('Default_Value') == 'now()')) {
              var dateNow = '';
              var dateObj = new Date();
              var month = dateObj.getUTCMonth() + 1;
              if(month<10) month = '0' + month;
              var day = dateObj.getUTCDate();
              if(day<10) day = '0' + day;
              var year = dateObj.getUTCFullYear();
              dateNow = day + "/" + month + "/" + year;
            }
            
            itemValue = ( (record.get('Donnee') != null) ? record.get('Donnee') : ( (record.get('Default_Value') != null) ? ( (record.get('Default_Value') == 'now()') ? dateNow : record.get('Default_Value').split('::')[0].substr(1, (record.get('Default_Value').split('::')[0]).length -2)) : '' ) );;
            record.set('Donnee', itemValue);
            record.commit();
          }; 
          break;
          
          case 'USER-DEFINED': 
          {
            // console.log('me.ar_all_enums_names_and_values
              // is : ',
              // me.ar_all_enums_names_and_values);
            activeField = {
              xtype: (record.get('Arity')=="TABLE" ? "tagfield" : 'combo'),
              name : 'combo',
              filterPickList: true,
              store: {},
              queryMode: 'local', 
              valueField: 'value',
              displayField:'text',
              width: 150
            };
            if(activeField) {
              if(me.ar_all_enums_names_and_values.length > 0) {
                var enum_store = Ext.create('Ext.data.Store', {
                  fields:['text', 'value'],
                  data : []
                });
                for(var i=0 ; i < me.ar_all_enums_names_and_values.length ; i++) {
                  if(me.ar_all_enums_names_and_values[i]["enum_name"] == record.get('Udt_Name')) {
                    var split_enum_values = me.ar_all_enums_names_and_values[i]["enum_value"].split(',');
                    for(var j=0 ; j < split_enum_values.length ; j++) {
                      enum_store.add({
                        'text': split_enum_values[j].trim(),
                        'value': split_enum_values[j].trim()
                      });
                    }
                    break;
                  } 
                }
                activeField.store = (enum_store);
              }
              if ( record.get('Arity')=="TABLE" ){
              	var value = ((record.get('Donnee') != null) ? record.get('Donnee') : ( (record.get('Default_Value') != null) ? record.get('Default_Value').split('::')[0].substr(1, (record.get('Default_Value').split('::')[0]).length -2) : '' ) );
              	var array = (value ? Ext.decode(value.replace(/\{/, "[").replace(/\}/, "]")) : []);
              	var values = [];
              	Ext.isArray(array) && Ext.each(array, function(avalue){
              		values.push(enum_store.findRecord('value', avalue));
              	});
              	itemValue = values;
              } else {
              	itemValue = ((record.get('Donnee') != null) ? record.get('Donnee') : ( (record.get('Default_Value') != null) ? record.get('Default_Value').split('::')[0].substr(1, (record.get('Default_Value').split('::')[0]).length -2) : '' ) );
              }
              record.set('Donnee', itemValue);
              record.commit();
            }
          }; 
          break;
  
          default:break;
        }
        if ( !activeField ) return;
        activeField.listeners = activeField.listeners || {};
        activeField.listeners.change = function(field){
          record.set('Donnee', field.getValue());
          record.commit();
        };
        activeField.allowBlank = record.get('Nullable')=="YES" && (record.get('Type')!='USER-DEFINED' || record.get('Arity')=="TABLE");
      }
      
      
      activeField.name = record.get('column_name');
      activeField.fieldLabel = fieldLabel;
      return activeField;
    };//getItem
    
    var record = {};
    var items = [];
    gPanelInformation_store.each(function(recordField){
    	items.push(getItem(recordField));
    	record[recordField.get('column_name')] = recordField.get('Donnee');
    })
    
    var gridInfoPanel;
    if ( this.infoWind ) this.infoWind.close();
    this.infoWind = new Ext.Window({
      width:420,
      layout : 'fit',
      id : 'InfoWindow',
      title: 'Information',
      autoDestroy :true,
      resizable:true,
      closeAction : 'destroy',
      items : [
      gridInfoPanel = Ext.create('Ext.form.Panel', {
        overflowY : 'auto',
        maxHeight: 450,
      	width : '100%',
        id: 'InfoPanel',
        fieldDefaults : {
        	margin : 10,
          anchor : '100%',
          labelWidth : 150,
          labelSeparator : '',
          msgTarget : 'qtip'
        },
        items : items
      })],
      
      buttons : [{
        id : 'btn_save_id',
        text : 'Enregistrer',
        width: 85,
        handler : function() {
            var fieldsToModifiyArray = [];
            var valid = gridInfoPanel.isValid();
            var fields  = gridInfoPanel.form.getFields().getRange();
            var errors = [];
            for(var i=0; !valid && i<fields.length; i++) {
            	var field = fields[i];
              if( !field.validate() ) {
                var anerror = field.getErrors(field.getValue());
                if ( anerror.length ){
                	if (anerror.length==1){
                		errors.push('<li>'+field.getFieldLabel()+' : '+anerror.join('')+'</li>');
                	}
                  else {
                    errors.push('<li>'+field.getFieldLabel()+' : <li>'+anerror.join('</li><li>')+'</li></li>');
                  }
                }
              }
            }
            if ( errors.length ){
            	Ext.Msg.alert('Erreurs détectées sur le formulaire', errors.join(''));
            	return;
            }
            
            var fieldsToModifiyArray = [];
            var valid = true;
            for(var i=0; i<gPanelInformation_store.data.length; i++) {
              if(((gPanelInformation_store.getAt(i).data['Donnee'] == null)
                  ||(gPanelInformation_store.getAt(i).data['Donnee'] == '')) 
                  && (gPanelInformation_store.getAt(i).data['Nullable'] == 'NO')
                  ) {
                valid = false;
                this.notNullField =  gPanelInformation_store.getAt(i).data['Libelle'];
                
                break;
              }
              if( typeof gPanelInformation_store.getAt(i).data['Donnee'] != 'undefined'){
              fieldsToModifiyArray.push(
                  {
                    'Libelle': me.getColumnName(gPanelInformation_store.getAt(i).data['Libelle']),
                    'Donnee':  ( typeof gPanelInformation_store.getAt(i).data['Donnee'] != 'undefined') ? gPanelInformation_store.getAt(i).data['Donnee']:null,
                    'Type': gPanelInformation_store.getAt(i).data['Type'],
                    'Default_Value': gPanelInformation_store.getAt(i).data['Default_Value'],
                    'Arity': gPanelInformation_store.getAt(i).data['Arity'],
                    'Udt_Name': gPanelInformation_store.getAt(i).data['Udt_Name'],
                    'Length': gPanelInformation_store.getAt(i).data['Length'],
                    'Nullable': gPanelInformation_store.getAt(i).data['Nullable']
                  }
              );
              }
            }
            if(valid) {
              if ( callbackSave ){
                callbackSave(gPanelInformation_store, feature, fieldsToModifiyArray);
                return;
              }
             
              Ext.Ajax.request({
                url: Routing.generate('editionmanager_setinformation', {
                  mapfile : this.mapfile,
                  editionLayername : this.editionLayername,
                  workingTablename : this.workingTablename
                }),
                scope : this,
                success: function(response){
                  try {
                    var response = Ext.decode(response.responseText);
                  } catch(error){
                    Ext.Msg.alert('Erreur de saisie', "L'enregistrement des valeurs a échoué");
                    return;
                  }
                  //console.log('setInformation - response is : ', response);
                  if(response.setInformation) {
                    this.newFeature = feature;
                    for(var i=0; i<gPanelInformation_store.data.length; i++) {
                      var field = gPanelInformation_store.getAt(i);
                      this.newFeature.data[this.getColumnName(field.data['Libelle'])] = field.data['Donnee'];
                      this.newFeature.attributes[this.getColumnName(field.data['Libelle'])] = field.data['Donnee'];
                    }
                    if(me.current_gid.indexOf(feature.attributes.gid) == -1) {
                      me.current_gid.push(feature.attributes.gid);
                    }
                    
                    gPanelInformation_store.removeAll();
                    me.infoWind.removeAll();
                    me.infoWind.close();
                  }
                },
                failure: function (response) { 
                  Carmen.Util.handleAjaxFailure(response, this.title, true); 
                },
                method : 'POST',
                params: {
                  operation : 'setInformation',
                  user_id : this.user_id,
                  geometry : feature.geometry.toString(),
                  fieldsToModifiy: Ext.encode(fieldsToModifiyArray)
                } 
              });
            }
            else {
              Ext.Msg.alert('Attention', 'Le champ ' + this.notNullField + ' doit être non nul.');
            }

          }, 
          scope: this
      }, {
        id : 'btn_close_id',
        text : 'Fermer',
        width: 85,
        listeners: {
          click: function() {
            gPanelInformation_store.removeAll();
            this.infoWind.removeAll();
            this.infoWind.close();
          },
          scope: this
        }
      }]
    });
    
    
    gridInfoPanel.loadRecord(Ext.create('Ext.data.Model', record))
    this.infoWind.show();
  },

  editionModifyCallback : function(obj) {
    var feature = obj.feature;
    if(this.current_gid.indexOf(feature.attributes.gid) == -1) {
      this.current_gid.push(feature.attributes.gid);
    }
    //console.log('editionModifyCallback - feature : ', feature);

    this.nextCallBack = this.modifyCallback;
    this.nextCallBackParams = {feature : feature};

    Ext.Ajax.request({
    	method : 'POST',
      url: Routing.generate('editionmanager_checkfeature', {
        mapfile : this.mapfile,
        editionLayername : this.editionLayername,
        snapLayername : this.snapLayername || "null"
      }),
      success: this.callback_checkFeature,
      /*success: function(response) {
        console.log('editionModifyCallback - response is : ', response.responseText);
      },*/
      failure: function (response) {
        Carmen.Util.handleAjaxFailure(response, this.title, true);
      },
      scope : this,
      params: {
        gid : feature.data.gid,
        geometry : feature.geometry.toString(),
        constraintType : this.contraintTypeFromGeometry(feature.geometry)
      }
    });
  },

  modifyCallback : function(params) {
    var feature = params.feature;
    Ext.Ajax.request({
      url: Routing.generate('editionmanager_editstate', {
        mapfile : this.mapfile,
        editionLayername : this.editionLayername,
        workingTablename : this.workingTablename,
        state : 'modified'
      }),
      success: this.callback_modify,
      /*success: function(response) {
        console.log('modifyCallback - response is : ', response.responseText);
      },*/
      failure: function (response) { 
        Carmen.Util.handleAjaxFailure(response, this.title, true); 
      },
      scope : this,
      params: {
        gid : feature.data.gid,
        geometry : feature.geometry.toString()
      }
    });
  },

  callback_modify : function (response) {
    response = Ext.decode(response.responseText);
    if (!response.success) {
      Ext.Msg.alert(this.title, ("Problème dans l'enregistrement de la géométrie"));
    }
  },

  editionDelCallback : function(obj) {
    var feature = obj.feature;
    //console.log('editionDelCallback - feature is : ', feature);

    if ( !feature.data.gid ) return;
    
    Ext.Ajax.request({
      url: Routing.generate('editionmanager_editstate', {
        mapfile : this.mapfile,
        editionLayername : this.editionLayername,
        workingTablename : this.workingTablename,
        state : 'deleted'
      }),
      success: this.callback_delete,
      /*success: function(response) {
        console.log('editionDelCallback - response is : ', response.responseText);
      },*/
      failure: function (response) {
        Carmen.Util.handleAjaxFailure(response, this.title, true);
      },
      scope : this,
      params: {
        gid : feature.data.gid,
        geometry : feature.geometry.toString(),
        constraintType : this.contraintTypeFromGeometry(feature.geometry)
      }
    });
  },

  callback_delete : function (response) {
    response = Ext.decode(response.responseText);
    if (!response.success) {
      Ext.Msg.alert(this.title, ("Problème dans la suppression de la géométrie"));
    }
  },

  endEdition : function() {

    this.recuperateSessionState = false;

    var handleConfirm = function(btnId) {
      if (btnId == 'cancel') {
      	if ( this.btnOpener ) {
          this.btnOpener.toggle(true, true);
      	}
        //console.log('btnId == cancel');
      }
      else {
        // console.log('Not btnId == cancel');

        var saveChanges = (btnId == 'yes');
        this.saveEdition(saveChanges, false);
      }
    };

    if ( !this.sessionActive ) {
    	handleConfirm.call(this, 'no');
      return true;
    }
    msgBox = Ext.Msg.show({
      title : ("Terminer la session d'édition"), 
      msg : ("Souhaitez vous enregistrer les modifications que vous avez apportées ?"),
      fn : handleConfirm,
      buttons : Ext.Msg.YESNOCANCEL,
      scope : this
    });
    return true;
  },

  saveEdition : function(saveChanges, restart) {
    Ext.Ajax.request({
      url: Routing.generate('editionmanager_endedition', {
        editionLayername : this.editionLayername,
        mapfile : this.mapfile
      }),
      success: Ext.Function.bindCallback(this.callback_endEdition, this, [restart]),
      /*success: function(response){
        console.log('endEdition - response is : ', response.responseText);
      },*/
      failure: function (response) { 
        Carmen.Util.handleAjaxFailure(response, this.title, true); 
      },
      scope : this,
      params: {
        operation : 'endedition',
        isView: (user_connected_details['couche_type'] == -4) ? true:false,
        user_id : this.user_id,
        workingTablename :  this.workingTablename,
        userEmail: user_connected_details.userEmail,
        userLogin: user_connected_details.userLogin,
        userNom: user_connected_details.userNom,
        userPrenom: user_connected_details.userPrenom,
        userSignature: user_connected_details.userSignature,
        alerts : (this.edition_alerts ? JSON.stringify(this.edition_alerts) : ""),
        gid: this.current_gid,
        saveChanges : saveChanges,
        donnee: this.txtEditionLayer.getValue()
      }
    });
  },

  callback_endEdition : function(restart, response) {
    this.current_gid = []; 
    response = Ext.decode(response.responseText);
    //console.log('callback_endEdition - response is : ', response);
    if(!response.success) {
      var handleResponse = function(btnId) {
        if(btnId == 'yes') {
          if ( restart ) {
            this.refreshStoreForPreviousSessionsCombo(this.txtEditionLayer.getValue());
          	this.startEdition();
          } else {
            this.closeSession();
          }
        }
      };
      msgBox = Ext.Msg.show({
        title : ("Terminer la session d'édition"), 
        msg : ("La session d'édition ne s'est pas terminée correctement, vos modifications n'ont peut être pas été prises en compte. Souhaitez vous forcer la fin de session ?"),
        fn : handleResponse,
        buttons : Ext.Msg.YESNO,
        scope : this
      });
    }
    else {
      if ( restart ) {
        this.refreshStoreForPreviousSessionsCombo(this.txtEditionLayer.getValue());
        this.closeEdition();
        this.startEdition();
      } else {
        this.closeSession();
      }
    }
  },

  closeEdition : function() {
    // closing session
    this.oleControl && this.oleControl.stopEditMode();

    if(this.editLayer) {
      this.map.removeLayer(this.editLayer);
      this.editLayer = null;
      /*this.editLayer.destroy();
      this.editLayer = null;*/
    }

    if(this.snapLayer) {
      this.snappingControl && this.map.removeControl(this.snappingControl);
      this.snappingControl && this.snappingControl.destroy && this.snappingControl.destroy();
      this.snappingControl = null;
      this.map.removeLayer(this.snapLayer);
      this.snapLayer.destroy();
      this.snapLayer = null;
    }

    if(this.verticesLayer) {
      this.map.removeLayer(this.verticesLayer);
      this.verticesLayer.destroy();
      this.verticesLayer = null;
    }
  },

  closeSession : function() {
    this.closeEdition();
    this.sessionActive = false;
    /*
    // unregistering listeners on edit layer
    this.editLayer.events.un({"afterfeaturemodified": this.editionModifyCallback});
    this.editLayer.events.un({"featureadded": this.editionAddCallback});
    this.editLayer.events.un({"featureremoved": this.editionDelCallback});
    this.editLayer.events.un({"featureselected": this.editionSelectCallback});
    this.editLayer.events.un({"featureunselected": this.editionUnSelectCallback});*/

    this.txtEditionLayer.setReadOnly(false);
    if ( !this.comboPreviousSessions.getValue() )this.comboPreviousSessions.emptyText = ('Choisissez une session précédente');
    this.comboPreviousSessions.applyEmptyText();
    this.comboPreviousSessions.setReadOnly(false);
    this.toolsEdition.hide();
    this.btnStartEdition.show();
    this.btnApplyOptions.hide();
    this.btnEndEdition.hide();

    //this.numerisationWind = null;

    // closing session
    this.sessionActive = false;

    // refreshing the editionlayer
    this.editionLayer.mergeNewParams({'uniqueName': Math.random()});
    this.editionLayer.redraw();
    
    this.btnDrawPoint.toggle(false);
      
    this.editionLayerNode = null;
    this.numerisationWind.close();
    this.numerisationWind = null;
    

  },

  setEditLayerStyles : function (styleMap) {
      // setting style
      // create a lookup table with different symbolizers for the different
      // is_valid values
      var lookup = {};
      lookup[false] = this.STYLE_INVALID;
      lookup[true] = OpenLayers.Util.extend({}, styleMap.styles['default']);
      styleMap.addUniqueValueRules("default", "is_valid", lookup);
  },
  CLASS_NAME : "Carmen.Control.EditorManager2"
});
