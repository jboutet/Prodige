  /**
 * @_requires OpenLayers/Controljs
 */

/**
 * Class: Carmen.Control.QueryAttributes
 
 * Inherits from:
 *  - <OpenLayers.Control>
 */

Carmen.Control.QueryAttributes = new OpenLayers.Class(OpenLayers.Control, {

  // UI components
  btn: new Ext.Button({
      tooltip: 'Requêtes attributaires',      
      tooltipType: 'title',
      cls: 'x-btn-icon cmn-tool-requete',
      enableToggle: true,
      toggleGroup: 'mainMapControl',
      disabled: false
  }),

  win : new Ext.Window({
    layout:'anchor',
//    layout:'fit',
    width:400,
    height: 400,
    constrain : true,
    plain: true,
    title: 'Requêtes attributaires',
    modal:false,
    autoDestroy :true,
    resizable:true,
    closeAction: 'hide',
    shadow : false
  }),

  requestPanel : Ext.create('Ext.form.Panel', {
    id: 'requestPanel',
    autoScroll: true,
    fitToFrame: true,
    //anchor : '100% 50%',
    //url:'save-form.php',   
    fitToFrame: true,
    title: 'Requête',
    bodyStyle:'padding:5px 5px 0; color: #000000;',
    labelStyle: 'color: #000000;',
    items: [{
      xtype: 'combo',    
      name: 'layer',
      id: 'comboLayer',
      labelWidth: 75, 
      fieldLabel: 'Couche',
      queryMode: 'local',
      editable: true,
      listConfig : { 
        loadingText: 'Chargement',
        getInnerTpl: function() { return '<tpl for="."><div ext:qtip="{layerTitle}" class="x-combo-list-item">{layerTitle}</div></tpl>';} 
      },
      emptyText: 'Sélectionnez une couche',
      typeAhead : true,
      forceSelection: true,
      triggerAction: 'all',
      minChars: 1,
      queryDelay: 250,     
      store: new Ext.data.SimpleStore({
        fields: ['layerName', 'layerTitle'],
        data : []}
      ),
      validator: function(v) {
         return (v!='Sélectionnez une couche');
        },   
      displayField: 'layerTitle',
      valueField: 'layerName',
      width: 240     	
    }, { 
   	  xtype: 'combo',
      name: 'qitem',
      id: 'comboField',
      labelWidth: 75, 
      fieldLabel: 'Champ',
      queryMode: 'local',
      editable: true,
      listConfig: {
        loadingText: 'Chargement',
        getInnerTpl: function() { return '<tpl for="."><div ext:qtip="{fieldLabel}" class="x-combo-list-item">{fieldLabel}</div></tpl>'; }
      }, 
      emptyText: 'Sélectionnez un champ',
      typeAhead : true,
      forceSelection: true,
      triggerAction: 'all',
      minChars: 1,
      queryDelay: 250,     
      disabled : true,
      store: new Ext.data.SimpleStore({
        fields: ['fieldName', 'fieldLabel'],
        data : []}
      ),   
      validator: function(v) {
      	 return (v!='Sélectionnez un champ');
        },
      displayField: 'fieldLabel',
      valueField: 'fieldName',
      allowBlank: false,
      width: 240
    }, {
      xtype: 'combo',
      id: 'comboOperator',
      name: 'qstring',
      labelWidth: 75,       
      fieldLabel: 'Opérateur',

      editable: true,
      listconfig: {
        loadingText: 'Chargement'
      },
      emptyText: 'Sélectionnez un opérateur',
      typeAhead : true,
      forceSelection: true,
      triggerAction: 'all',
      minChars: 1,
      queryDelay: 250,     
      queryMode: 'local',
      store: new Ext.data.ArrayStore({
	        fields: ['qstring', 'label'],
	        data : [[ 'FIELD like \'PATTERN%\'', 'commence par ' ],
  	        [ 'FIELD like \'%PATTERN%\'', 'contient ' ],
            [ 'not (FIELD like \'%PATTERN%\')', 'ne contient pas ' ],
	          [ 'FIELD = PATTERN', 'est égal (=)' ],
	          [ 'FIELD <> PATTERN', 'est différent (<>)' ],
	          [ 'FIELD >= PATTERN', 'supérieur ou égal (>=)' ],
  	        [ 'FIELD > PATTERN', 'strictement supérieur (>)' ],
            [ 'FIELD <= PATTERN', 'inférieur ou égal (<=)' ],
            [ 'FIELD < PATTERN', 'strictement inférieur (<)' ]          
	        ]}
	    ),
	    validator: function(v) {
         return (v!='Sélectionnez un opérateur');
        },   
      displayField: 'label',
      valueField: 'qstring',
      width: 240
    }, {
    	xtype: 'textfield',
    	name: 'value',
      id: 'fieldValue',    		
    	labelWidth: 75, 
      fieldLabel: 'Valeur',
    	mode: 'local',
    	allowBlank: false,
    	width: 240
    }],
    
    buttons: [{
            id: 'requestSubmit',
            text: 'Interroger'
        }]
  }), 


  /* Properties */ 

  // data of the layer combo box
  layerData : [], 
  // array of data of the field combo box
  fieldData : [], 
  // array of layer field desc objets (one sub aray per layer)
  layerFieldsDesc : [], 
  // reference to the layerTreeManager of the app
  layerTreeManager : null,
  // reference to the layerTree of queryable layer
  layerTree : null,
  // mapfile of the app
  mapfile : null,
  // url of the queryAttributes service 
  serviceUrl : null,


  // methods
  initialize: function(serviceUrl, mapfile, options) {
    OpenLayers.Control.prototype.initialize.apply(this, arguments);
    
    // create the Grid
    this.gridInfoPanel = Ext.create('Ext.grid.Panel', {
      store: new Ext.data.ArrayStore({
        fields: [],
        data: []
      }),
      columns: [],
      title:'Résultats sélectionnés',
      hidden: true,
      autoScroll: true,
      collapsible: true,
      border: false,
      iconCls : 'cmnInfoGridZoomIcon',
      anchor : '100% 50%'
   });
    
    
    this.mapfile = mapfile;
    this.serviceurl = serviceUrl;
  },

  setMap: function(map) {
    OpenLayers.Control.prototype.setMap.apply(this, arguments);

    this.map.app.ui.addToCycleButtonGroup('advanced', 'info', this.btn, 4);
    this.btn.addListener('toggle', Carmen.Util.buildExt2olHandlerToggle(this));    
    
    // adding listeners to link window closing and control deactivation   
    var control = this;
    this.win.on(
      'hide',
      function(w) { control.btn.toggle(false); },
      this.win);
    
    
  },
  
  activate: function() {
  	if (this.getLayerTreeManager==null) {
      Ext.MessageBox.alert("Warning", "The control " + 
       this. CLASS_NAME + 
       " requires the LayerTreeManager control to work.");
      return null;
    }
    OpenLayers.Control.prototype.activate.apply(this, arguments);
    this.map.toolTipDeactivate();
    this.showWindow();    
  },
  
  deactivate: function() {
    // removing highlighting...
    this.map.clearSelection();
    OpenLayers.Control.prototype.deactivate.apply(this, arguments);
    if (this.win.rendered)
      this.win.hide();
    this.map.toolTipActivate();  	
  },


  // displaying results
  showWindow : function() {
    var control = this;
    this.layerTree = this.getLayerTreeManager().getLayerTree().clone(this.transformNodeInfo);

    if (!this.win.rendered) {
      this.win.add(this.requestPanel);
      this.win.add(this.gridInfoPanel);
	    // adding listeners to the form
	    var submitBtn = Ext.getCmp('requestSubmit');
	    submitBtn.on(
	      'click',
	      function(btn, evt) {
	        var form = control.requestPanel.getForm();
	        if (!form.isValid())
	          Ext.Msg.alert('Requêtes attributaires',
	            'La requête saisie n\'est pas valide.');
	        else {
	          control.performQuery(control.buildParams(control.layerFieldsDesc));
	        }
	      }, 
	      submitBtn);
      this.win.doLayout();
      
    } 

    if (!this.win.isVisible()) {
      this.initFields();
      this.win.show();
    }
  },


  getLayerTreeManager : function() {
    if (this.layertreeManager == null) {
      var controls = this.map.getControlsByClass('Carmen.Control.LayerTreeManager');
      this.layerTreeManager = controls.length==0 ? null : controls[0];
    }
    return this.layerTreeManager;
  },

  getLayerTree : function() {
  	this.layerTree = this.getLayerTreeManager().getLayerTree().clone(this.transformNodeInfo);
  	this.layerTree.filter(this.filterNodeLayerQueryable);
  	return this.layerTree;
  },

  // init the requestPanel fields, 
  // i.e the combos with data from the queryable layers
  initFields : function() {
  	var qLayerAtts = this.getLayerTree().collect('attributes', true);
  	//console.log(qLayerAtts);
  	this.layerData = [['none','Sélectionnez une couche']];
  	this.fieldData = [];
  	for (var i=0; i<qLayerAtts.length; i++) {
  		var layerAtt = qLayerAtts[i];
  		this.layerData.push([layerAtt.layerName,layerAtt.text]);
      layerFieldsData = [];
      fieldsCarmenDesc = Carmen.Util.chompAndSplit(layerAtt.infoFields,';');
      //console.log(fieldsCarmenDesc);
      this.layerFieldsDesc[layerAtt.layerName] = [];        		
  		for (var j=0; j<fieldsCarmenDesc.length; j++) {
  			fieldDesc = Carmen.Util.decodeCarmenFieldDesc(fieldsCarmenDesc[j], layerAtt.baseQueryURL);
  			if (fieldDesc.type=='TXT')             
  			  layerFieldsData.push([fieldDesc.name, fieldDesc.alias]);
  			this.layerFieldsDesc[layerAtt.layerName].push(fieldDesc);
  		}
      this.fieldData.push(layerFieldsData);
  	}
  	
  	// loading data in combos
  	var comboLayer = Ext.getCmp('comboLayer');
  	var comboField = Ext.getCmp('comboField');
    comboLayer.getStore().loadData(this.layerData);
    
    // adding listener on layer combo
    var control = this;
    comboLayer.on(
      'select', 
      function(f,r,i) {
        if (i==0) {
        	comboField.clearValue();
        	comboField.disable();
        }
        else {
        	comboField.setValue('');  
        	comboField.getStore().removeAll();
        	//console.log(control.layerField);
        	comboField.getStore().loadData(control.fieldData[i-1]);
        	comboField.enable();
        }	
      },
      comboLayer);
  },

  
  // build the parameters of the request
  buildParams : function(layerFieldsDesc) {
    var form = Ext.getCmp('requestPanel').getForm();
    var values = form.getValues();
    
    var comboLayer = Ext.getCmp('comboLayer');
    var layerName = comboLayer.getValue();
    var qitem =  Ext.getCmp('comboField').getValue();
    var op = Ext.getCmp('comboOperator').getValue();
    var pattern = Ext.getCmp('fieldValue').getValue();
    var qstring = op;
		// obedel : qtring building server side to manage encoding pbs...
		/*      .replace(/PATTERN/, pattern)
      			.replace(/FIELD/, qitem); 
		*/      
    var params = {
      layer : layerName,
      qstring : qstring,
      pattern : pattern,
      qitem : qitem,
      fields : Ext.util.JSON.encode(layerFieldsDesc[layerName]),
      map : this.mapfile }

    //console.log(params);
    return params;    
  },


  // launch the request to the server
  performQuery : function (queryParams) {
    var control = this;
    
    var received = function (response) {
      response = Ext.decode(response.responseText);

      if (response.totalCount == 0) {
        //console.log('no results...');
        control.clearGridPanel();
      }
      else {
	      for(lname in response.results)  {
	        var node = control.layerTree.findChildByAttribute('layerName', lname);
	        if (node!=null) { 
	          var data = response.results[lname].data;
	          node.attributes.infoData = response.results[lname];
	          node.attributes.infoData.qfile = response.queryfile; 
	          control.updateGridPanel(node);
	        }
	      }
      }   
    }
    
    Ext.Ajax.request({
      url: '/services/QueryAttributes/index.php',
      success: function(response) { 
      	 received(response); },
      failure: function (response) { 
         Carmen.Util.handleAjaxFailure(response, "Requêtes attributaires"); 
      },
      	 
      	 
      	 
      params: queryParams
    });
  }, 

  clearGridPanel : function() {
  	var gPanel = this.gridInfoPanel;
  	gPanel.setTitle('Aucun résultat');
  	gPanel.reconfigure(Carmen.Util.emptyStore, Carmen.Util.emptyColumnModel);
  	if (!gPanel.isVisible())
      gPanel.show();

  },
  
  updateGridPanel : function(layerNode) {
    var control = this;
    var gPanel = this.gridInfoPanel; 
    
    // clearing selection
    this.map.clearSelection();
    // removing listeners...
    gPanel.purgeListeners();    
    // updating panel title
    gPanel.setTitle(layerNode.text + ' ' + 
      layerNode.attributes.infoData.data.length + 
      ' résultats');
    
    var extentStr = layerNode.attributes.infoData.extent;
    var minScale = layerNode.attributes.minScale;
    var maxScale = layerNode.attributes.maxScale;
    var bounds = Carmen.Util.strExtentToOlBounds(extentStr, Carmen.Control.QueryAttributes.PUNCTUAL_RADIUS);
    var lnode = layerNode;
    gPanel.header.on('click', 
        function() {
          control.map.zoomToSuitableExtent(bounds, minScale, maxScale);
          lnode.attributes.OlLayer.setDisplayMode(
             Carmen.Layer.MapServerGroup.DISPLAY_NQUERYMAP, 
             lnode.attributes.layerName, null, 
             lnode.attributes.infoData.qfile);
        });
    control.map.zoomToSuitableExtent(bounds, minScale, maxScale);
    layerNode.attributes.OlLayer.setDisplayMode(
       Carmen.Layer.MapServerGroup.DISPLAY_NQUERYMAP, 
       layerNode.attributes.layerName, null, 
       layerNode.attributes.infoData.qfile);
               
     // configuring and showing the result table panel
     var gConfig = this.buildGridConfig(layerNode.attributes.infoFields, layerNode.attributes.infoData);
     gPanel.reconfigure(gConfig.store, gConfig.columnModel)
     
     gPanel.on('rowclick', 
      function(grid, rowIndex, e){
        var rec = grid.store.getAt(rowIndex);
        // zooming to the right extent
        var extentStr = rec.get('extent');
        var bounds = Carmen.Util.strExtentToOlBounds(extentStr, Carmen.Control.QueryAttributes.PUNCTUAL_RADIUS);
        control.map.zoomToSuitableExtent(bounds, minScale, maxScale);

        // highlighting the feature linked to the row
        var fid = rec.get('fid');
        lnode.attributes.OlLayer.setDisplayMode(
          Carmen.Layer.MapServerGroup.DISPLAY_QUERYMAP, 
          lnode.attributes.layerName, fid);
      });
      
      // registering selection
      olLayer = lnode.attributes.OlLayer;
      lname = lnode.attributes.layerName;
      if (olLayer.handleSelection(lname)) { 
  	    this.map.addToSelectionLayers(olLayer);
  	    olLayer.setSelectionStore(lname, 
  	      new Ext.data.SimpleStore({
            fields: lnode.attributes.infoData.fields,
            data :  lnode.attributes.infoData.data
          }));
      }
      
      
     if (!gPanel.isVisible()) {
       gPanel.show();
       this.win.setSize(control.win.getSize().width, this.win.getSize().height+1);
     }
  }, 
  
  extentRenderer : function(v) {
  	return '<div class="cmnInfoGridZoomIcon"> </div>';
  },


  buildGridConfig: function (fieldsDesc, result) {
    var fields = new Array();
    var columns = new Array();
    
    columns.push(new Ext.grid.RowNumberer());
    columns.push({
      id : 'extent',
      header : '',
      width : 20,
      resizable: true,
      renderer: this.extentRenderer,
      dataIndex: 'extent'
    });
    
    var arrFieldsDesc =  Carmen.Util.chompAndSplit(fieldsDesc,';');
    //console.log(arrFieldsDesc);
    for (var i=0; i<arrFieldsDesc.length; i++) {
    	//console.log(arrFieldsDesc[i]);
    	var desc = Carmen.Util.decodeCarmenFieldDesc(arrFieldsDesc[i]);
    	//console.log(desc);
    	var fDesc = {name : desc.name + i.toString(), type : Carmen.Util.carmenTypeToExtType(desc.type)};
    	if (desc.type=='date') 
    	 fDesc['dateFormat'] = Carmen.Util.carmenDateTypeToExtDateFormat(desc.type);
    	fields.push(fDesc);
    	
    	var colDesc = {
    		id: Ext.id(), //desc.name + i.toString(),
    		header: desc.alias,
    		width: 200,
    		sortable: true,
    		resizable: true,
    		renderer: Carmen.Util.carmenTypeToExtRenderer(desc.type),
    		dataIndex: desc.name + i.toString()
    	};
    	columns.push(colDesc);
    }
    fields.push({name : 'extent'});
    fields.push({name : 'fid'});
    //console.log(fields);
    var config = {
	  	store : new Ext.data.SimpleStore({
	        fields: fields,
	        data : result.data
	    }),
	    columnModel : new Ext.grid.ColumnModel({
	      columns: columns 
	    })
	  };

	  return config;
  },


  // Filter and transform function used on tree
  filterNodeLayerQueryable :  function(node) {
  	var res = true;
    res = ('type' in node.attributes) && 
      ((node.attributes.type == Carmen.Control.LayerTreeManager.NODE_GROUP && node.hasChildNodes()) ||
       (node.attributes.type == Carmen.Control.LayerTreeManager.NODE_LAYER && node.attributes.OlLayer.getVisibility(node.attributes.layerName)
       && node.attributes.infoFields && node.attributes.infoFields.length>0));
    return res;
  },

  filterNodeLayerWithResults : function(node) {
  	var res = true;
    res = ('type' in node.attributes) && 
      ((node.attributes.type == Carmen.Control.LayerTreeManager.NODE_GROUP && node.hasChildNodes()) ||
       (node.attributes.type == Carmen.Control.LayerTreeManager.NODE_LAYER && node.attributes.infoData!=null) ||
       (node.attributes.type == Carmen.Control.LayerTreeManager.NODE_FEATURE));    
    return res;
  },

  transformNodeInfo : function(atts) {
  	var cloneAtts = Carmen.Util.clone(atts);
  	//console.log(cloneAtts);
/*  	if (cloneAtts.text!="root")
  	   cloneAtts.uiProvider = Carmen.TreeNodeInfoUI;
  	else
  	   cloneAtts.uiProvider = Carmen.RootTreeNodeInfoUI;  	   
*/
  	cloneAtts.cls = 'none';
    cloneAtts.infoData = null;  	
  	cloneAtts.listeners = {};
  	cloneAtts.expandable = true;
  	if (cloneAtts.type == Carmen.Control.LayerTreeManager.NODE_LAYER) {
  		cloneAtts.icon = '/IHM/images/NoTreeIcon.gif';
  		cloneAtts.iconCls = 'cmnInfoGridZoomIcon';
  	}  	  
  	return cloneAtts;
  },
  
  CLASS_NAME: "Carmen.Control.QueryAttributes"

});

Carmen.Control.QueryAttributes.PUNCTUAL_RADIUS = 100.0;

