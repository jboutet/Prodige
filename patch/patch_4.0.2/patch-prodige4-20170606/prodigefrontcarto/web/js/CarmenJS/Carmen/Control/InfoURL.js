  /**
 * @_requires Carmen/Control/Info.js
 */

/**
 * Class: Carmen.Control.InfoURL
 
 * Inherits from:
 *  - <Carmen.Control.Info>
 */

Carmen.Control.InfoURL = new OpenLayers.Class(Carmen.Control.Info, {
  // UI components
  btn: new Ext.Button({
      tooltip: 'URL Informatives',      
      tooltipType: 'title',
      cls: 'x-btn-icon', /* cmn-tool-infourl',*/
      enableToggle: true,
      toggleGroup: 'mainMapControlInfo',
      disabled: false,
      text : '<i class="fa fa-flip-horizontal fa-external-link"></i>&nbsp;<i class="fa fa-info fa-2x"></i>',
      width: ctrlWidth,
      height: ctrlHeight
  }),
  
  btnGrpDefaultActiv : false,

  windowTitle: 'URL informatives',
  
  showGraphicalSelection : true,
  //fieldsFilter indispensable car surcharge de Carmen.Control.Info et appel dans performQueries
  buildParams : function(nodes, position, fieldsFilter, mapfile) {
  	return Carmen.Control.Info.prototype.buildParams.apply(this, [nodes, position, this.fieldFilter_URL, mapfile]);
  },

  // Filter node that have an URL type in their info fields...
  filterNodeLayerQueryable :  function(node) {
    var res = Carmen.Control.Info.prototype.filterNodeLayerQueryable.apply(this, arguments);
    // TODO: build a better test
    res = res && (
      (node.attributes.type == Carmen.Control.LayerTreeManager.NODE_GROUP && node.hasChildNodes() && node.getDepth()>0) || 
      (node.attributes.infoFields && node.attributes.infoFields.match(/.*URL.*/)!=null));
    return res;
  },



  // TODO: see for reorganising code to extract 
  // received handler and facilitate subclassing
  performQuery : function (queryParams) {
    var control = this;
    
    var received = function (response) {
      response = Ext.decode(response.responseText);
      if (!("totalCount" in response)) {
        //control.win.hide();
        control.map.app.ui.delAllInfoPanel();
        control.map.app.ui.infoPanel.collapse();
        Carmen.Util.handleAjaxFailure(response, control.windowTitle, false); 
      }
      else {
        uniqueURL = null; 
        if (response.totalCount==1) {
          // one resulting feature...
          for (var l in response.results) {
            nbFields = response.results[l].fields.length;
          }
          if (nbFields==1) {
            //... with one url field
            uniqueURL = response.results[l].data[0][0];  
          }
        }
        if (uniqueURL) {
          control.map.app.ui.delAllInfoPanel();
          control.map.app.ui.infoPanel.collapse();
          //Carmen.Util.openWindowIframe('Information détaillée', uniqueURL.href);	
          window.open(uniqueURL.href,'_blank');	
        }
        else {
          //updating result panel
          control.registerResults(response, Carmen.Control.Info.SRC_MS);
          //hiding the panel if no results
          if (response.totalCount==0) {
            control.map.app.ui.delAllInfoPanel();
            control.map.app.ui.infoPanel.collapse();
          }
        }
      }
    }
    
    Ext.Ajax.request({
      url: this.getRouteQuery(),
      //PRODIGE40 '/services/GetInformation/index.php',
      success: function(response) { 
         received(response); },
      failure: function (response) { 
         
         Carmen.Util.handleAjaxFailure(response, control.windowTitle, true); 
      }, 

      headers: {
        'my-header': 'foo'
      },
      params: queryParams,
      scope: this
    });
  }, 

  CLASS_NAME: "Carmen.Control.InfoURL"

});
