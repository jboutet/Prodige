  /**
 * @_requires OpenLayers/Controljs
 */

/**
 * Class: Carmen.Control.Info
 
 * Inherits from:
 *  - <OpenLayers.Control>
 */

Carmen.Control.InfoCircle = new OpenLayers.Class(Carmen.Control.Info, {

  // UI components
  btn: new Ext.Button({
      tooltip: 'Information sur zone circulaire',      
      tooltipType: 'title',
      cls: 'x-btn-icon', /* cmn-tool-infocircle',*/
      enableToggle: true,
      toggleGroup: 'mainMapControlInfo',
      disabled: false,
      text : '<span class="fa-stack fa-lg"><i class="fa icon-circle fa-stack-2x"></i><i class="fa fa-info fa-stack-1x"></i></span>',
      width: ctrlWidth,
      height: ctrlHeight
  }),
  btnGrpDefaultActiv : false,
  /**
   * Method: draw
   */    
  draw: function() {
      this.handler = new Carmen.Handler.Circle(this,
                          { 
                            done: function(res) { 
                              //console.log("circle finished"); 
                              //console.log(res);
                              this.info(res);
                            }
                          }, 
                          {keyMask: this.keyMask, persist: false} );
  },  
  
  performQueries : function(position) {
    var control = this;
    
	  // filter the node
    this.layerTreeRoot.filter(this.filterNodeLayerQueryable);
          
    var isLayerNode = function(n) { 
    	return ('type' in n.attributes && (n.attributes.type == Carmen.Control.LayerTreeManager.NODE_LAYER)); 
    }; 
    var nodes = this.layerTreeRoot.collectChildNodes(isLayerNode);

    if (nodes.length==0) {
    	//this.headerPanel.setTitle('Aucun résultat');
      Ext.Msg.alert("Outil d'information", "Aucun résultat ne correspond à votre recherche.");
    	//this.gridInfoPanel.hide();
    }
    else {
	    var ms_nodes = {};
  	  var wms_nodes = [];
    	var wms_layers = [];

	    for (var i=0; i<nodes.length; i++) {
	    	var layer = nodes[i].attributes.OlLayer;
		    if (layer instanceof Carmen.Layer.WMSGroup) { 
					wms_nodes.push(nodes[i]);
					Carmen.Util.set_add(wms_layers, layer);
		    }
				else {
		    	var mapfile = nodes[i].attributes.OlLayer.mapfile;
				  if (mapfile) {
				   if (!ms_nodes[mapfile])
				     ms_nodes[mapfile] = [];
				   ms_nodes[mapfile].push(nodes[i]);  
				  }
                                }
	    }
		 	// cancel all pending requests
		 	Ext.Ajax.abort();
			this.queriesSent = 0;
			this.totalCount = 0;
		    this.fullExtent = null;
		    
		  this.map.app.ui.delAllInfoPanel();  
		    
		    // launching requests
			for (var m in ms_nodes) {
  			if (ms_nodes[m].length>0) 
  				this.performQuery(this.buildParams(ms_nodes[m], position, null, m));
			}
			if (position instanceof OpenLayers.Pixel) {
				for (var i=0; i<wms_layers.length; i++) { 
					this.performWMSQuery(wms_layers[i], position);
				}
			}
    }
  },
  
  // traditionnal query on mapserver layer
  performQuery : function (queryParams) {
    var control = this;
    // prepraring answer callback
    var received = function (response) {
      response = Ext.decode(response.responseText);
	  	control.registerResults(response, Carmen.Control.Info.SRC_MS);
    }
    
    // launching the request
    this.queriesSent++;
    
    //PRODIGE40 var url = '/services/GetInformation/index.php';
    var url = this.getRouteQuery();
    //console.log(url);
    //console.log(queryParams);
    Ext.Ajax.timeout = this.requestTimeout;
    Ext.Ajax.request({
      url: url,
      success: received,
      //timeout: this.requestTimeout,
      failure: function (response) { 
         Carmen.Util.handleAjaxFailure(response, control.windowTitle, true); 
      },
      headers: {
        'my-header': 'foo'
      },
      params: queryParams
    });
  }, 


  buildParams : function(nodes, position, fieldsFilter, mapfile) {
    
    var layerNames = [];
    var fieldsList = [];
    var briefFieldsList = [];
    var baseQueryURLList = [];
    
    for (var i=0; i<nodes.length; i++) {
      
  		if(typeof(nodes[i].attributes.infoFields)!="undefined"){
  		  layerNames.push(nodes[i].attributes.layerName);
  		  fieldsList.push(nodes[i].attributes.infoFields);
  		  if (typeof(nodes[i].attributes.briefFields)!="undefined" ){ 
  		    briefFieldsList.push(nodes[i].attributes.briefFields);
  		  }
        baseQueryURLList.push(nodes[i].attributes.baseQueryURL);
  		}
	    
    }
    
    var fieldsDesc = new Array();
    for (var i=0; i<fieldsList.length; i++) {
      var list = Carmen.Util.chompAndSplit(fieldsList[i],';');
      fieldsDesc[i] = new Array();
      for (var j=0; j<list.length; j++) {
        var desc  = Carmen.Util.decodeCarmenFieldDesc(list[j], baseQueryURLList[i]);
        fieldsDesc[i][j]=desc;   
      }
      if (fieldsFilter) 
        fieldsDesc[i] = Carmen.Util.array_filter(fieldsDesc[i], fieldsFilter);
    }

    var briefFieldsDesc = new Array();
    for (var i=0; i<briefFieldsList.length; i++) {
      var list = Carmen.Util.chompAndSplit(briefFieldsList[i],';');
      briefFieldsDesc[i] = new Array();
      for (var j=0; j<list.length; j++) {
        var desc  = Carmen.Util.decodeCarmenFieldDesc(list[j], baseQueryURLList[i]);
        briefFieldsDesc[i][j]=desc;   
      }
      if (fieldsFilter) 
		briefFieldsDesc[i] = Carmen.Util.array_filter(briefFieldsDesc[i], fieldsFilter);
    }

    // linking kept info fields and brief fields 
    // to the corresponding layer node in the tree
    for (var i=0; i<nodes.length; i++) {
   	  nodes[i].attributes.infoFieldsDesc = fieldsDesc[i];
   	  nodes[i].attributes.briefFieldsDesc = briefFieldsDesc[i];
    }

    var params = {
      layers : Ext.util.JSON.encode(layerNames),
      fields : Ext.util.JSON.encode(fieldsDesc),
      briefFields : Ext.util.JSON.encode(briefFieldsDesc),
      shape : Carmen.Util.circleToStr(position),
      map : mapfile,
      showGraphicalSelection : this.showGraphicalSelection,
      shapeType : 'circle' }
    return params;    
  },


  performWMSQuery : function(layer, position) {
    var control = this;
   	var received = function(response) {
   	  var features = [];
			var contentStr = response.getResponseHeader["Content-Type"]
					|| response.getResponseHeader["Content-type"];
	   	//console.log(contentStr);
	   	if (contentStr!=null) { 
	   	  contentStr = (contentStr.split(";")[0]).trim();
	   	  //console.log(contentStr);
	   	  var fmt = control.WMSResponseFormats[contentStr];
    	  if (fmt) { 
     	     var rep = response.responseXml || response.responseText;
     	     //console.log(Url.encode(rep));
     	     //console.log(Url.decode(rep));
     	     //console.log(Url._utf8_encode(rep));
     	     //console.log(Url._utf8_decode(rep));
     	     //console.log(response.responseXml || response.responseText);
     	     features = fmt.read(response.responseXml || response.responseText);
     	     //console.log(features);
    	   }
	   	}
			
			var results = {};
			var extentStr = Carmen.Util.positionToStr(control.map.getExtent());

			for (var i=0; i<features.length; i++) {
				var layerName = layer.name;//features[i].type;
				if (!(features[i].type in results)) {
					results[features[i].type] = {
						"fields" : [],
						"briefFields" : [],
						"data" : [],
						"briefData" : []
					}
					for (var f in features[i].attributes) {
						results[layerName].fields.push(f);				
						results[layerName].briefFields.push(f);
					}
				}
				var featureData = [];
				for (var f in features[i].data) 
					featureData.push(features[i].data[f]);	
				results[layerName].briefData.push(featureData);
				results[layerName].data.push(featureData);
			}
			
			var responseJS = {
				"totalCount" : features.length,
				"results" : results
			};
			
			control.registerResults(responseJS, Carmen.Control.Info.SRC_WMS);
    };
    
    
    this.queriesSent++;
    
    Ext.Ajax.request({
      url: Routing.generate('frontcarto_query_getinformation_proxy'),
      //PRODIGE40 '/services/GetInformation/proxy.php',
      method: 'GET',
  	  success : function (response) { 
      	 received(response);
      },
  	  failure: function (response) { 
      	 // uncomment to display wms querying errors
      	 //var msg = Ext.valueFrom(response.responseText, "Service WMS indisponible");
      	 //Ext.Msg.alert(control.windowTitle, msg);
      	 control.registerResults(
      	   Carmen.Control.Info.noResultsResponse,
      	   Carmen.Control.Info.SRC_WMS);
      },
      headers: {
        'my-header': 'foo'
      },
      params : { 
        "proxy_url" : layer.getFeatureInfoURL('', position, this.map.size, this.map.getExtent()),
        "force_utf8_output" : 1 // needed because ExtJS waits for UTF8 encoded response...
      } 
    });
    //console.log(layer.getFeatureInfoURL('', position, this.map.size, this.map.getExtent()));
  }, 
  
  CLASS_NAME: "Carmen.Control.InfoCircle"

});
