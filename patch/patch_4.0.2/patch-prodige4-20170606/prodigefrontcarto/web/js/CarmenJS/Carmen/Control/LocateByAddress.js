/**
 * @_requires OpenLayers/Control.js
 */

/**
 * Class: LocateByAddress
 
 * Inherits from:
 *  - <OpenLayers.Control>
 */

Carmen.Control.LocateByAddress = new OpenLayers.Class(OpenLayers.Control, {
  
  mainPanel : Ext.create('Ext.form.Panel', {
    id: 'addressAreaPanel',
    title: 'Zones par adresse',
    itemCls: 'areasClass',
    width: '100%',
//    height : 70,
    buttons: [{
      id: 'btn_addressArea',
      text: 'Localiser',
      type : 'submit'
    }],
    tabConfig: {
      title: 'Adresses',
      tooltip: {
          text: 'Localisation par saisie d\'adresse'
      }
    },
    border: true,
    toFrontOnShow : false
  }),
  
  initialize : function(contextInit,options) {
    OpenLayers.Control.prototype.initialize.apply(this, arguments);
    this.locateAdresse = "";
    if(contextInit && (typeof contextInit["locateaddress"] !== "undefined"))
      this.locateAdresse = contextInit["locateaddress"];
    this.locateAdresse = this.locateAdresse.replace('?', '');
    this.areaText = Ext.create('Ext.form.TextField', {
    //fieldLabel: 'Veuillez saisir l\'adresse recherchée',
    labelAlign: 'top',
      emptyText: 'Veuillez saisir l\'adresse recherchée',
      width: '95%',
      listeners: {
        specialkey: function(field, e){
          if (e.getKey() == e.ENTER) {
            Ext.getCmp("btn_addressArea").fireHandler();
          }
        }, scope :this
      }
    });
    this.mainPanel.add(
    {
      xtype: 'fieldcontainer',
      layout: 'hbox',
      items : [this.areaText]
    });
  },

  requestSuccess : function(response){
    var format = new OpenLayers.Format.XLS();
    try{
      var output = format.read(response.responseXML);
      
      if ( output.version=="1.2" ){
      	//Lecture de la structure de réponse
        var addresses = [];
        Ext.each(output.getBodies(), function(body){
        	var responseParameters = body.getResponseParameters();
        	Ext.each(responseParameters.getGeocodeResponseList(), function(geocodeResponse){
        		Ext.each(geocodeResponse.getGeocodedAddresses(), function(geocodedAddress){
        			var address = {};
        			Ext.each(geocodedAddress.address.getPlaces(), function(place){
       					address[place.classification] = place.name;
        			}, this);
              addresses.push(address);
        		}, this);
        	}, this);
        }, this);
        
        //Lecture de la / des adresse(s) collectées
        if (addresses.length){
        	if (addresses.length==1){
        		var address = addresses[0];
        		var geometry = address.Bbox.split(';');
            var foundPosition = new OpenLayers.LonLat(geometry[0], geometry[1]).transform(
              new OpenLayers.Projection("EPSG:4326"),
              this.map.getProjectionObject()
            );
            this.map.setCenter(foundPosition, 13, true, true);
        	} else {
            var c_data = [];
        		Ext.each(addresses, function(address, i){
              var geometry = address.Bbox.split(';');
        		  
              var info = ["{sep1}", 
                address.Municipality || '',
                "{sep2}", address.Departement || '', '{sep3}', address.Commune || '', "{sep4}" 
              ].join('');
              var loop = 0;
              while(loop<10 && /(\{sep\d\})(\{sep\d\})/.test(info) ){info = info.replace(/(\{sep\d\})(\{sep\d\})/g, '$2'); loop++;}//suppression des valeur vide
              info = info.replace("{sep1}", '"').replace("{sep2}", '" (').replace("{sep3}", " - ").replace("{sep4}", ")");
              
              c_data.push({
                "place_id"   : i,
                "x": geometry[0],
                "y": geometry[1],
                "name" : info
              });
        		}, this);
        		
            if(Ext.getCmp("resu_cb")){
              this.mainPanel.remove(Ext.getCmp("resu_cb"));
            }
            var c_store = Ext.create('Ext.data.Store', {
              fields: ['place_id', 'name'],
              data  : c_data,
              width : '100%'
            });
            var cb = Ext.create('Ext.form.ComboBox', {
            	emptyText : 'Adresses répondant à la recherche',
                fieldLabel  : 'Choisissez une des adresses ci-dessous, ou précisez votre recherche ci-dessus',
              labelAlign  : 'top',
              labelWidth  : '100%',
              store       : c_store,
              displayField: 'name',
              mode        : 'local',
              valueField  : 'place_id',
              id: 'resu_cb',
              itemId: 'resu_cb2',
              listeners   : {
                'select' : {
                  fn : function(cmb, records, eopts){
                    if(records && records[0] && records[0].data && records[0].data.x){
                    var foundPosition = new OpenLayers.LonLat(records[0].data.x, records[0].data.y).transform(
                    new OpenLayers.Projection("EPSG:4326"),
                    this.map.getProjectionObject()
                    );
                    
                    this.map.setCenter(foundPosition,13, true, true);
            
                    }
                  },
                  scope : this
                }
              }
            });
            this.mainPanel.add(cb);
            this.mainPanel.doLayout();
        	}
        } else {
        	Ext.MessageBox.alert("Localiser", "L'adresse saisie n'a retournée aucun résultat.");  
        }
      }
      else {
        if (output.responseLists[0].features.length!=0) {
      
          if(output.responseLists[0].features.length == 1){
            var geometry = output.responseLists[0].features[0].geometry;
            var foundPosition = new OpenLayers.LonLat(geometry.x, geometry.y).transform(
                    new OpenLayers.Projection("EPSG:4326"),
                    this.map.getProjectionObject()
                    );
            this.map.setCenter(foundPosition, 13, true, true);
          }else{
            var c_data = [];
            for(var i = 0; i < output.responseLists[0].features.length; i++){
              
              c_data.push({
                "place_id"   : i,
                "x": output.responseLists[0].features[i].geometry.x,
                "y": output.responseLists[0].features[i].geometry.y,
                "name"       : this.areaText.value + " - " +output.responseLists[0].features[i].attributes.address.place.Municipality +
                              (typeof(output.responseLists[0].features[i].attributes.address.place.MunicipalitySubdivision) !="undefined"  ? 
                               " ("+output.responseLists[0].features[i].attributes.address.place.MunicipalitySubdivision+")" : "")
              });
            }
            var c_store = Ext.create('Ext.data.Store', {
              fields: ['place_id', 'name'],
              data  : c_data,
              width : '100%'
            });
            var cb = Ext.create('Ext.form.ComboBox', {
                fieldLabel  : 'Choisissez une des adresses ci-dessous, ou précisez votre recherche ci-dessus',
              labelAlign  : 'top',
              labelWidth  : '100%',
              store       : c_store,
              displayField: 'name',
              mode        : 'local',
              valueField  : 'place_id',
              id: 'resu_cb',
              itemId: 'resu_cb2',
              listeners   : {
                'select' : {
                  fn : function(cmb, records, eopts){
                    if(records && records[0] && records[0].data && records[0].data.x){
                    var foundPosition = new OpenLayers.LonLat(records[0].data.x, records[0].data.y).transform(
                    new OpenLayers.Projection("EPSG:4326"),
                    this.map.getProjectionObject()
                    );
                    
                    this.map.setCenter(foundPosition,13, true, true);
            
                    }
                  },
                  scope : this
                }
              }
            });
            if(Ext.getCmp("resu_cb"))
              this.mainPanel.remove(Ext.getCmp("resu_cb"));
            this.mainPanel.add(cb);
            this.mainPanel.doLayout();
            
          }
          
        } else {
          Ext.MessageBox.alert("Localiser", "L'adresse saisie n'a retournée aucun résultat.");  
        }
      }
    }catch (e) {
       Ext.MessageBox.alert("Localiser", "Le service n'a pu être correctement utilisé.");  
    }
    
  },
  
  requestFailure : function(){
    Ext.MessageBox.alert("Localiser", "Le service n'a pu être correctement utilisé.");  
  },
  
  setMap: function(map) {
    OpenLayers.Control.prototype.setMap.apply(this, arguments);
      
    this.map.app.ui.addToLocatorPanel(this.mainPanel, 1);
 
    var map = this.map;
    var control = this;
    
    Ext.getCmp("btn_addressArea").setHandler(
      function(event, toolEl, panelHeader) {
    	if(this.areaText.value == null || this.areaText.value == ""){
    	  Ext.MessageBox.alert("Localiser", "Merci de saisir une adresse");
    	}else{
    		
    	  var providerUrl = Routing.generate('frontcarto_query_getbyaddress');
    	  //PRODIGE40 window.CARMEN_URL_SERVER_FRONT + '/services/GetByAddress/index.php';
    	  /*var params = {
    	    'serviceurl' : this.locateAdresse,
    	    'service'    : 'locate',
    	    'adresse'    : this.areaText.value
    	  };*/
        // configuring proxy for XHR call
        //PRODIGE40 OpenLayers.ProxyHost = Routing.generate('frontcarto_query_getinformation_proxyhost')
        
        /*var xmlData = '<?xml version="1.0" encoding="UTF-8"?> '+
                        '<xls:XLS xmlns:xls="http://www.opengis.net/xls" xmlns:sch="http://www.ascc.net/xml/schematron" '+ 
                        'xmlns:gml="http://www.opengis.net/gml" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/xls http://schemas.opengis.net/ols/1.1.0/LocationUtilityService.xsd" version="1.1"> '+
                          '<xls:RequestHeader/> '+
                          '<xls:Request maximumResponses="5" methodName="GeocodeRequest" requestID="123456789" version="1.1"> '+
                          '  <xls:GeocodeRequest> '+
                           '   <xls:Address countryCode="PositionOfInterest"> '+
                            '    <xls:freeFormAddress>'+this.areaText.value+'</xls:freeFormAddress> '+
                            '  </xls:Address> '+
                          '  </xls:GeocodeRequest> '+
                         ' </xls:Request> '+
                        '</xls:XLS>'
*/
    	  var xmlData = '<?xml version="1.0" encoding="UTF-8"?>'+
                         '<XLS'+
                          ' xmlns:xls="http://www.opengis.net/xls"'+
                          ' xmlns:gml="http://www.opengis.net/gml"'+
                          ' xmlns="http://www.opengis.net/xls"'+
                          ' xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"'+
                          ' version="1.2"'+
                          ' xsi:schemaLocation="http://www.opengis.net/xls http://schemas.opengis.net/ols/1.2/olsAll.xsd">'+
                          ' <RequestHeader/>'+
                          '    <Request requestID="1" version="1.2" methodName="LocationUtilityService">'+
                          '       <GeocodeRequest returnFreeForm="false">'+
                          '         <Address countryCode="StreetAddress">'+
                          '               <freeFormAddress>'+this.areaText.value+'</freeFormAddress>'+
                          '         </Address>'+
                          '       </GeocodeRequest>'+
                          '    </Request>'+
                          ' </XLS>';
        //"http://www.openrouteservice.org/php/OpenLSLUS_Geocode.php"
        OpenLayers.Request.POST({
                url: Routing.generate('frontcarto_query_locatebyaddress_proxyhost', {proxy_url : this.locateAdresse}),
                scope: this,
                failure: this.requestFailure,
                success: this.requestSuccess,
                headers: {"Content-Type": "application/xml"},
                data: xmlData
        });
        
        
        
        
        /*
    	  Ext.Ajax.request({ 
            url: providerUrl,
            params: params,
            scope: this,
            success: function(response){
              var tab_resultat = eval("("+response.responseText+")");
              if(tab_resultat.length > 0 && (typeof tab_resultat[0] !== "undefined")){
            	if(tab_resultat.length == 1){
            	  //On zoom sur cette localisation
            	  var extent = new OpenLayers.Bounds(
            	    tab_resultat[0].boundingbox[2],
            	    tab_resultat[0].boundingbox[0],
            	    tab_resultat[0].boundingbox[3],
            	    tab_resultat[0].boundingbox[1]
            	  );
            	  extent = extent.transform("EPSG:4326", "EPSG:2154");
            	  console.log(extent);
            	  map.zoomToExtent(extent);
            	}else{
                  //On construit une liste déroulante avec l'ensemble des résultats
            	  var c_data = [];
            	  for(var i = 0; i < tab_resultat.length; i++){
            		  c_data.push({
            	      "place_id"   : tab_resultat[i].place_id,
            	      "boundingbox": tab_resultat[i].boundingbox,
            	      "name"       : tab_resultat[i].display_name
            	    });
            	  }
            	  var c_store = Ext.create('Ext.data.Store', {
            	    fields: ['place_id', 'name'],
            	    data  : c_data,
            	    width : '100%'
            	  });
            	  var cb = Ext.create('Ext.form.ComboBox', {
            	    fieldLabel  : 'Choisissez une des adresses ci-dessous, ou précisez votre recherche ci-dessus',
            	    labelAlign  : 'top',
            	    labelWidth  : '100%',
            	    store       : c_store,
            	    displayField: 'name',
            	    mode        : 'local',
            	    valueField  : 'place_id',
            	    id: 'resu_cb',
            	    itemId: 'resu_cb2',
            	    listeners   : {
            	      'select' : {
            	        fn : function(cmb, records, eopts){
            	          if(records && records[0] && records[0].data && records[0].data.boundingbox){
            	        	var extent = new OpenLayers.Bounds(
            	        	  records[0].data.boundingbox[2],
            	        	  records[0].data.boundingbox[0],
            	        	  records[0].data.boundingbox[3],
            	        	  records[0].data.boundingbox[1]
            	            );
            	            extent = extent.transform("EPSG:4326", "EPSG:2154");
            	            console.log(extent);
            	            map.zoomToExtent(extent);
            	          }
            	        },
            	        scope : this
            	      }
            	    }
            	  });
            	  this.mainPanel.add(cb);
            	  this.mainPanel.doLayout();
            	}
              }else{
                Ext.MessageBox.alert("Localiser", "L'adresse saisie n'a retournée aucun résultat.");  
              }
            },
            failure: function(response){
              Ext.MessageBox.alert("Localiser", "Le service n'a pu être correctement utilisé.");  
            }
    	  });
        */
    	}
      },
      this
    );
  },

  CLASS_NAME: "Carmen.Control.LocateByAddress"

});

Carmen.Control.LocateByAddress.defaultName = 'zone_';
Carmen.Control.LocateByAddress.uniqueId = 0;
Carmen.Control.LocateByAddress.getUniqueId = function() {
	 Carmen.Control.LocateByAddress.uniqueId++;
	 return (Carmen.Control.LocateByAddress.uniqueId - 1);
};
// misc constants
Carmen.Control.LocateByAddress.emptyAddressAreaTxt = 'Zones par adresse';
