 /**
   * 
   */
   
Ext.define('Carmen.ApplicationUI', {
  extend: 'Ext.Viewport',
  layout: 'border',
  titlePanel : Ext.create('Ext.Panel', {
    id: 'titlePanel',
    height: 20,
    width: '50%',
    padding: '0 0 0 0',
    margin: '0 0 0 0',
    border: true,
    items : [{
      id: 'titleLabel',
      padding: '15 15 15 15',
      xtype: 'label'
    }]
  }),
  
  infoPanel : Ext.create('Ext.Panel', {
	region: 'south',
    height: 300,
    autoScroll: true,

    border: true,
    collapsible : true,
    title : 'Résultats <span class="loading hidden"></span>',
	bounds: null,
	resizable: true,
  resizeHandles: 'n s',
	  id : 'infoPanel',
	  layout : {
		    type: 'accordion',
            titleCollapse: false,
            animate: true,
            hideCollapseTool: true,
            activeOnTop: true/*,
            activeOnTop: true*/
		},
	  tools:[{
      id: 'infoPanel_zoom',
      renderTpl: [
        '<i id="{id}-toolEl" data-ref="toolEl" class="fa fa-search fa-1x"' +
            '" style="color: #FFF; font-size: 16px;" role="presentation"/></i>'
      ],
      qtip: 'Zoom sur les résultats'		
	  }]
  }),
  
  toolPanel : Ext.create('Ext.Window', {
    id: 'toolBox',
    constrain: true,
    closable: false,
    resizable: false,
    title: '<i class="fa fa-arrows fa-1x"></i>',
    maxWidth: 42,
    header: {
      titleAlign : 'center',
      height: 25,
      padding: 3
    },
    layout: 'fit',
    items :[{
      id: 'toolPanel',
      bodyStyle: "background: transparent;",
      width: 40,
      height: 'auto',
      //border: true,
      layout: 'hbox',
      /*listeners: {
        afterrender: function(cpt, eopts) {
          console.log(this);
          var el = Ext.fly('toolPanel-innerCt');
          console.log(el);
          if (el)
            console.log(el.getHeight());
        }
      },*/
      items: [
      {
        xtype: 'panel',
        id: 'toolAdvancedToolBar',
        width: 40,
        height: 'auto',
        //border: true,
        margin: 1,
        padding: 0,
        hidden: true,
        dockedItems: [{
          id: 'toolPanelCollapseButton',
          xtype: 'button',
          dock: 'bottom',
          text: '<i class="fa fa-minus"></i>'
          //enableToggle: true
        }]
      },
      {
        xtype: 'panel',
        id: 'toolSimpleToolBar',
        width: 40,
        height: 'auto',
        //border: true,
        margin: 1,
        padding: 0,
        dockedItems: [{
          id: 'toolPanelExpandButton',
          xtype: 'button',
          dock: 'bottom',
          text: '<i class="fa fa-plus"></i>'
          //enableToggle: true
        }]
      }]
    }]
   
  }),

  mapPanel : Ext.create('Ext.Panel', {
    layout: 'fit',
    id: 'mapPanel',
    x: 0,
    y: 0
//obedel: if there is a way to force 100% height in a extjs way, cos with layout absolute, the only way seems using  !important in external css.
/*
    style : {
      width : '100%  !important',
      height : '100%  !important'
    },
    bodyStyle : {
      width : '100% !important',
      height : '100% !important'
    }
*/
  }),

  bottomPanel : Ext.create('Ext.toolbar.Toolbar', {
    id: 'bottomPanel',
    xtype: 'toolbar',
    layout: {
        type: 'hbox',
        align: 'stretch'
    },
    width: '50%',
    height: 20,
    padding: 0,
    border: true
  }), 

  
  locatorButton : Ext.create('Ext.Button', {
    id: 'locatorButton',
    text: '<i class="fa fa-fw fa-map-marker fa-2x"></i>Localiser',
    scale: 'large',
    height: 80,
    layout: {
        align: 'center'
    },
    enableToggle: true
  }),
  
  locatorPanel : Ext.create('Ext.tab.Panel', {
    //layout: 'vbox',
    id: 'locatorPanel',
    border: true,
    fitToFrame: true,
    title: '<i class="fa fa-fw fa-map-marker"></i>&nbsp;Localiser',
    width: 450,
    closable: true,
    closeAction : 'hide',
    hidden : true,
    activeTab: 0
  }),

  dataButton : Ext.create('Ext.Button', {
    id: 'dataButton',
    text: '<i class="fa fa-fw fa-database fa-2x"></i>Données',
    scale: 'large',
    height: 80,
    enableToggle: true
  }),
  
  dataPanel : Ext.create('Ext.Panel', {
    id: 'dataPanel',
    border: true,
    fitToFrame: true,
    title: '<i class="fa fa-fw fa-database"></i>Données',
    width: 450,
    overflowY: 'auto',
    closable: true,
    hidden : true,
    closeAction : 'hide'
  }),

  editorButton : Ext.create('Ext.Button', {
    id: 'editorButton',
    text: '<i class="fa fa-fw fa-pencil fa-2x"></i><br/>Edition',
    scale: 'large',
    height: 80,
    hidden : true,
    layers : [],
    idlayers : {},
    enableToggle : true,
    addLayer : function(node){
    	var id = node.attributes.layerName;
      if ( !this.idlayers[id] ){
        this.layers.push(node);
        this.idlayers[id] = true;
      }
    }
  }),
  

  
  
  mainPanel : Ext.create('Ext.Panel', {
    region: 'center',
    id: 'mainPanel',
    //title: encode_utf8(jsOws.OWSContext.General.Title),
    //title: jsOws.OWSContext.General.Title,
    fitToFrame: true,
    dockedItems: [
    ],  
    layout: 'absolute',
    mousein : false
  }),
  

  
	// should the page have a header banner
	displayHeader : false,
	
	// should the left navigation panel be shown
	displayNavPanel : false,

  // constructor function
  constructor: function(ctx, options) {
  	
    var me = this;
    
    defaultConfig = {
      layout: 'border',
      items: [],
      suspendLayout: true
    };

    //this.displayHeader = (ctx!=null) && (Ext.valueFrom(ctx.getGeneralConfig('display_header'),"OFF", false)=="ON");
    /*
    if (this.displayHeader) {
	      var srcBanner = window.CARMEN_URL_SERVER_FRONT + window.IHMUrl + "BannerModels/" + Ext.valueFrom(ctx.mdataMap.header_model, 'bandeau.html', false);
	      var eltBanner = new Ext.ux.IFrameComponent({ 
	      	id: 'banner', 
	      	name:'banner', 
	      	url: srcBanner
	      });
	      this.headerPanel.add(eltBanner);
	      defaultConfig.items.push(this.headerPanel);
        this.headerPanel.on('beforeexpand', 
          function(p) { 
              top.initBanner(); 
          }, this);
    }
    */
    //defaultConfig.items.push(this.navigationPanel);
    defaultConfig.items.push(this.mainPanel,this.infoPanel);
    //Ext.getCmp("infoPanel").collapse();
    this.infoPanel.collapse();
    
    
    
    Ext.apply(this, options, defaultConfig);
    Carmen.ApplicationUI.superclass.constructor.apply(this, arguments);
    
    // map panel
    this.mainPanel.add(this.mapPanel);
    
    // title panel
    if(Ext.valueFrom(ctx.getGeneralConfig('display_header'),"OFF", false)=="ON")
      this.mainPanel.add(this.titlePanel);
    
      
    var dataLocatorPanel = Ext.create('Ext.panel.Panel', {layout : 'vbox', plain:true, bodyStyle : 'background-color:transparent', minWidth : 450, maxWidth : 450, defaults : {margin:'5 0 5 0'}, x:15, y:160});
    
    // locator panel
    //this.locatorPanel.setPosition(15,130);
    this.mainPanel.add(this.locatorButton);
    dataLocatorPanel.add(this.locatorPanel);
    
    this.locatorButton.on('toggle',
      function(elt, pressed, eOpts) {
        this.locatorPanel.setVisible(pressed);
      },
      this
    );
    
    this.locatorPanel.on('hide',
      function(elt, eOpts) {
        this.locatorButton.setPressed(false);/*
        //this.dataPanel.removeCls("dataPanelBottom").addCls("dataPanelTop");
        this.dataPanel.setPosition(15, 160);*/
        this.dataPanel.setMaxHeight(document.body.clientHeight -(
        //marge haute
        160+
        //hauteur résultats
        35) -
        //marge en haut des résultats
        50);

      },
      this
    );
    
    this.locatorPanel.on('show',
      function(elt, eOpts) {/*
        //this.dataPanel.removeCls("dataPanelTop").addCls("dataPanelBottom");
        this.dataPanel.setPosition(15, 
          160+
          this.locatorPanel.getHeight()+
          5 //marge entre les blocs
          );*/
        this.dataPanel.setMaxHeight(document.body.clientHeight -(
        //marge haute
        160+
        //hauteur résultats
        35+
        //hauteur bloc localiser
        this.locatorPanel.getHeight()
        //espace entre blocs
        +50) -
        //marge en haut des résultats
        50);
      },
      this
    );
    
    // data panel
    //this.dataPanel.setPosition(15,350);
    this.mainPanel.add(this.dataButton);
    dataLocatorPanel.add(this.dataPanel);
        
    this.dataButton.on('toggle',
      function(elt, pressed, eOpts) {
        this.dataPanel.setVisible(pressed);
      },
      this
    );
    this.dataPanel.on('hide',
      function(elt, eOpts) {
        this.dataButton.setPressed(false);
      },
      this
    );
    
    //editorPanel
    this.mainPanel.add(this.editorButton);
    this.editorButton.on('toggle', 
      function(elt, pressed, eOpts) {
        if ( !this.editorPanel ){
          var ctrls = this.app.map.getControlsByClass('Carmen.Control.EditorManager2');
          if ( !ctrls.length ) return;
          
          this.editorPanel = ctrls[0];
          
        }
        if ( !this.editorPanel )return;
        this.editorPanel.btnOpener = elt;
        if ( pressed ){
          if ( !elt.layers.length ) return;
        	this.editorPanel.activate();
        	this.editorPanel.loadLayers(elt.layers);
        } else {
          this.editorPanel.deactivate();
        }
        return;
      },
      this
    );
    
    //common behaviours
    var panels = [this.locatorPanel, this.dataPanel];
    var buttons = [this.locatorButton, this.dataButton];
    panels.forEach(function(panel, index){
      panel.on({
        mouseenter: function(e){
          this.mainPanel.mousein=true;
        }
      , mouseleave:function(e){
          this.mainPanel.mousein=false;
        }
      , element: 'body',
        scope : me
      });
      panel.setVisible(me.displayNavPanel);
      buttons[index].setPressed(me.displayNavPanel);
    });
    this.mainPanel.add(dataLocatorPanel);
    
    
    //this.mainPanel.add(this.navigationPanel);
    //this.navigationPanel.setVisible(this.displayNavPanel);    
    
    // tool panel
    //this.mainPanel.add(this.toolPanel);
    
    
    var viewSize = Ext.getBody().getViewSize();
    this.toolPanel.showAt(
      viewSize.width-42-20, 
      Math.floor(Ext.getBody().getViewSize().height*0.3));
    //this.toolPanel.alignTo(this.mainPanel.getEl(), "c-c", [0, 0]);
    var toolPanelExpandBtn = this.toolPanel.queryById('toolPanelExpandButton');
    toolPanelExpandBtn.on('click', 
      function(elt, evt, eOpts) {
          var advancedTb = this.queryById('toolAdvancedToolBar');
          advancedTb.setVisible(true);
          var newWidth = 82 + 2;
          this.setMaxWidth(newWidth);
          this.setWidth(newWidth);
          this.setX(Math.max(0,this.getX()-(newWidth/2)));
          var toolPanelBtn = this.queryById('toolPanelExpandButton');
          toolPanelBtn.hide();
        },
        this.toolPanel
      );
    
    var toolPanelCollapseBtn = this.toolPanel.queryById('toolPanelCollapseButton');
    toolPanelCollapseBtn.on('click', 
      function(elt, evt, eOpts) {
          var advancedTb = this.queryById('toolAdvancedToolBar');
          advancedTb.setVisible(false);
          var newWidth = 42;
          this.setMaxWidth(newWidth);
          this.setWidth(newWidth);
          this.setX(Math.max(0,this.getX()+newWidth));
          var toolPanelBtn = this.queryById('toolPanelExpandButton');
          toolPanelBtn.show();
        },
        this.toolPanel
      );

    // info panel
    var toolInfoZoomBtn = this.infoPanel.queryById('infoPanel_zoom');
    toolInfoZoomBtn.on('click', 
     function(){		
				this.app.map.zoomToSuitableExtent(this.infoPanel.bounds);
			},
			this);

    // bottom panel
    this.mainPanel.add(this.bottomPanel);
    
    this.suspendLayout = false;
    
    /*this.on('resize', 
      function(elt, width, height, oldWidth, oldHeight, eOpts) {
        console.log('resizing');
        this.mapPanel.setHeight(this.mainPanel.getHeight());
      },
      this
    );*/
    
    this.mainPanel.on('resize', 
      function(elt, width, height, oldWidth, oldHeight, eOpts) {
        this.mapPanel.setHeight(this.mainPanel.getHeight());
      },
      this
    );
    
    this.doLayout();
    
    this.dataPanel.setMaxHeight(document.body.clientHeight -(
    //marge haute
    160+
    //hauteur résultats
    35+
    //hauteur bloc localiser
    this.locatorPanel.getHeight()
    //espace entre blocs
    +50) -
    //marge en haut des résultats
    50);
    
  },

  getMapDiv: function() {
  	return this.mapPanel.body;
  },

  getTopToolBar: function() {
    //return this.mainPanel.getDockedComponent('toptoolbar');
    return this.toolPanel.queryById('toolAdvancedToolBar');
  },
  
  
  addToToolPanel: function(elt, toolbarId, pos) {
    
    var panel = toolbarId=='simple' ?
      this.toolPanel.queryById('toolSimpleToolBar') :
      this.toolPanel.queryById('toolAdvancedToolBar');
    
    // managing position
    if (pos>panel.items.items.length) {
        panel.add(elt);
    }
    else
      panel.insert(pos,elt);
  },

  addToCycleButtonGroup: function(toolbarId, title, grpId, elt, pos, active, toolbarPos) {
    var toolbar = toolbarId=='simple' ?
      this.toolPanel.queryById('toolSimpleToolBar') :
      this.toolPanel.queryById('toolAdvancedToolBar');
    var btnCycle = toolbar.queryById('grpBtn-'+grpId);
    
    if (btnCycle==null) {
      btnCycle = Ext.create('Ext.button.Selector', {
        id:'grpBtn-'+grpId,
        "class":'grpBtn',
        menuAlign: 'tr-tl?',
        arrowVisible: true,
        arrowCls: 'selector',
        arrowAlign: 'left', 
        text : '',
        tooltip : title,     
        tooltipType: 'title',
        showText: true,
        width: 40,
        height: 40,
        enableToggle: true,
        toggleGroup: 'mainMapControl',
        //allowDepress: false,
        menu: {
          id: 'grpBtn-menu-'+grpId,
          layout: { 
            type : 'hbox',
            align: 'stretch'
          },
          plain: true,
          minWidth: 40,
          defaults: {
              enableToggle: false,
              layout: 'fit',
              width: 40,
              height: 40,
              margin: 1,
              padding: 0,
              text: '',
              renderTpl : '<span id="{id}-btnWrap" data-ref="btnWrap" role="presentation" unselectable="on" style="{btnWrapStyle}" ' +
                    'class="{btnWrapCls} {btnWrapCls}-{ui} {splitCls}{childElCls} buttonItem">' +
                      '<span id="{id}-btnEl" data-ref="btnEl" role="presentation" unselectable="on" style="{btnElStyle}" ' +
                      'class="{btnCls} {btnCls}-{ui} {textCls} {noTextCls} {hasIconCls} ' +
                      '{iconAlignCls} {textAlignCls} {btnElAutoHeightCls}{childElCls}">' +
                        '<tpl if="iconBeforeText">{[values.$comp.renderIcon(values)]}</tpl>' +
                          '<span id="{id}-btnInnerEl" data-ref="btnInnerEl" unselectable="on" ' +
                              'class="{innerCls} {innerCls}-{ui}{childElCls}">{text}</span>' +
                        '<tpl if="!iconBeforeText">{[values.$comp.renderIcon(values)]}</tpl>' +
                      '</span>' +
                    '</span>' +
                    '{[values.$comp.getAfterMarkup ? values.$comp.getAfterMarkup(values) : ""]}' +
                    // if "closable" (tab) add a close element icon
                    '<tpl if="closable">' +
                      '<span id="{id}-closeEl" data-ref="closeEl" class="{baseCls}-close-btn">' +
                          '<tpl if="closeText">' +
                              ' {closeText}' +
                          '</tpl>' +
                      '</span>' +
                    '</tpl>'
            },
          items : []
        }
      });
      if (toolbarPos) {
        if (toolbarPos>toolbar.items.items.length) 
          toolbar.add(btnCycle);
        else
          toolbar.insert(toolbarPos,btnCycle);
      }
      else
        toolbar.add(btnCycle);
        
    }
    elt.setWidth(40);
    if (pos>btnCycle.menu.items.items.length) {
        btnCycle.menu.add(elt);
    }
    else {
      btnCycle.menu.insert(pos,elt); 
    }
    if (active || btnCycle.getActiveItem()==null)
      btnCycle.setActiveItem(elt); 
    
      
  },
  addToBottomToolbar: function(elt, pos) {
    var panel = this.bottomPanel;
    // managing position
    if (pos>panel.items.items.length) {
        panel.add(elt);
    }
    else
      panel.insert(pos,elt);
  },
  
  getBottomToolBar : function() {
    //return this.mainPanel.getDockedComponent('bottomtoolbar');
    return this.bottomPanel;
  },
  
  getNavigationPanel: function() {
  	return this.navigationPanel;
  },
  
  getLocatorPanel: function() {
  	return this.locatorPanel;
  },
  
  addToLocatorPanel: function(elt, pos, activeTab) {
    var panel = this.locatorPanel;
    // managing position
    if (pos>panel.items.items.length) {
        panel.add(elt);
    }
    else
      panel.insert(pos,elt);
    
    if (activeTab) 
      panel.setActiveTab(elt);
    /*
    // managing elt collapsing/expanding
    elt.on('collapse',
      function(p, eopts) {
        this.doLayout();
      },
      panel);
    elt.on('expand',
      function(p, eopts) {
        this.doLayout();
      },
      panel);
      */ 
  },
  
  addToDataPanel: function(elt, pos, activeTab) {
    var panel = this.dataPanel;
    // managing position
    if (pos>panel.items.items.length) {
        panel.add(elt);
    }
    else
      panel.insert(pos,elt);
    /*if (activeTab) 
      panel.setActiveTab(elt);*/
  },
  
  addToInfoPanel: function(elt) {
    // console.log(toolbarId);
	this.infoPanel.expand();
    var panel = this.infoPanel;  
	panel.add(elt);
  },
  
  delAllInfoPanel: function() {
    // console.log(toolbarId);
    var panel = this.infoPanel;  
	panel.removeAll();
  },
  
  
  getDataPanel: function() {
  	return this.dataPanel;
  },
  
  
  getMainPanel: function() {
    return this.mainPanel;
  },
  
  getHeaderPanel: function() {
    return this.headerPanel;
  },
  
  
  setMapTitle : function(title) {
    var label = this.titlePanel.queryById('titleLabel');
    label.setText(title);
  },

  doLayout: function() {
  	Carmen.ApplicationUI.superclass.doLayout.apply(this, arguments);
  	// hack needed to correctly display bottom toolbar
  	this.getMainPanel().updateBox(this.getMainPanel().getBox());
    this.updateBox(this.getBox());
  },

  /**
   *  @brief : return the screen resolution in Pixel per Inch
   *  @return : a float value 
   */
  getScreenResolution : function() {
    var testDiv = document.createElement("div");
    testDiv.style.position = 'absolute';
    testDiv.style.width='1in';
		testDiv.style.padding = '0px';
		testDiv.id = "testDiv";
		this.doLayout();
		this.getMapDiv().appendChild(testDiv);
    return testDiv.offsetWidth;
 },
  /**
  * @brief :  posistionne les composants en mode d'affichage classique
  * c-à-d., minimize les panels data et locator
  */ 
  standardLayout : function() {
    this.locatorButton.toggle();
    this.dataButton.toggle();
  },
 
  /**
  * @brief :  posistionne les composants en mode d'affichage extracteur de données
  * c-à-d., minimize le panel data, redimensionne le bouton panel, cache le panel et le bouton locator
  */ 
  extractorLayout : function() {
    this.locatorButton.hide();
    this.dataButton.toggle();
    this.dataPanel.hide();
    this.dataButton.setText('<i class="fa fa-database fa-1x"></i>');
    this.dataButton.setScale('small');
    this.dataButton.setHeight(40);
    this.dataButton.setWidth(40);
    this.dataButton.setPosition(
      20,
      20
    );
    var elt= Ext.get('dataButton-btnInnerEl').child('i.fa');
    if (elt)
      elt.setStyle({'margin-left' : '0px' });
  },
  
  CLASS_NAME : "Carmen.ApplicationUI"

});

//Carmen.ApplicationUI.CLASS_NAME = "Carmen.ApplicationUI";
 

