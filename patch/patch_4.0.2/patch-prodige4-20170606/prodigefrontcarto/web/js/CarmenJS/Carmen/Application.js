/**
 * @_requires OpenLayers/Basetypes/Class.js
 * @_requires Carmen/Map.js
 */


Ext.define('cmn.layerTreeModel', {
      extend: 'Ext.data.TreeModel',
      fields: [{ name: 'text', mapping: 'name'}
              ,{name: 'disabled', type:'bool', defaultValue:false}
      ],
      
      
      // new functions
      clone: function(transform) {
        var clone = this.copy(Ext.id());
        if (this.attributes) {
          clone.attributes = {};
          for(var key in this.attributes) {
            if (typeof(this.attributes[key]) !== 'object') {
              clone.attributes[key] = this.attributes[key];
            }
            else {
              if (key != 'linkedNodes') {
                clone.attributes[key] = this.attributes[key];
              }
            }
          }
          //clone.attributes = this.attributes;
        }
        if (transform) 
          clone = transform.apply(this,[clone]);
        for(var i=0; i<this.childNodes.length; i++){
          clone.appendChild(this.childNodes[i].clone(transform));
        }
        return clone;
      },

     // filters the current and its child acording to the test function
     // return true if the current node has been removed, else in other cases
     filter: function(test) {
        var res = false;
        var i=0; 
        while (i < this.childNodes.length) {
          var child = this.childNodes[i];
          if (!child.filter(test))
            i++;
        }
        
        if (!test(this) && this.getDepth()>0) {
          res = true;
          this.remove();
        }
      
        return res;
      },
      
      collect : function(attributeName, onlyLeaf) {
        var collected = new Array();

        if ((onlyLeaf && this.isLeaf()) || !onlyLeaf) {
          if (attributeName in this) 
             collected.push(this[attributeName]);
          else if (attributeName in this.attributes)
             collected.push(this.attributes[attributeName]);
        }
        for (var i=0; i< this.childNodes.length; i++) {
          var childCollected = this.childNodes[i].collect(attributeName, onlyLeaf);
          collected = collected.concat(childCollected);
        }
        
        return collected;
      },
      
      collectChildNodes : function(test) {
        var collected = new Array();
        for (var i=0; i< this.childNodes.length; i++) {
          if (test(this.childNodes[i]))
            collected.push(this.childNodes[i]);
          collected = collected.concat(this.childNodes[i].collectChildNodes(test));
        }
        return collected;
      },
      
      findChildByAttribute: function(att, value) {
        var res = null;
        //console.log(this);
        if (this.hasChildNodes()) {
          var i=0;
          while (i<this.childNodes.length && res==null) {
            var child = this.childNodes[i];
            if (att in child && child[att]==value)
              res = child;
            else if (att in child.attributes && child.attributes[att]==value)
              res = child;
            else
              res = child.findChildByAttribute(att, value);
            i++;
          }
          return res;
        }
      },

      updateIndex : function(applyToChildNodes) {
        this.attributes.index =  this.parentNode==null ? 0 :
          this.parentNode.indexOf(this);
        if (applyToChildNodes) {
          for (var i=0; i<this.childNodes.length; i++) {
            this.childNodes[i].updateIndex(applyToChildNodes);
          }
        } 
      }      
    });


Carmen.Application = new OpenLayers.Class({

  ui : null,
  
  map : null,

  context : null,

  initialize: function(jsonContext) {
    // CARMEN specific parameters
    // set custom resolution
    OpenLayers.DOTS_PER_INCH = Carmen.DOTS_PER_INCH;
    // switch disgusting pink onImageLoadError in IE to transparent
    OpenLayers.Util.onImageLoadErrorColor = "transparent";
    
    // custom vtypes...
    Ext.apply(Ext.form.field.VTypes, {
      
      //  vtype validation function
      owsContext : function(val, field) {
         return /^.*\.(ows)$/.test(val);
      },
      // vtype Text property to display error Text
      // when the validation function returns false
      owsContextText: 'Seuls les fichiers de contexte ows sont autorisés',
      // vtype Mask property for keystroke filter mask
      fileMask : /[a-z_\.]/i
 
    });
   

    if (jsonContext!=null) {
      this.context = new Carmen.JSONContext(jsonContext);
    }

    if (this.context!=null) {
      var displayNavPanel = this.context.isToolActiv('referenceMap') ||
        this.context.isToolActiv('zoomToPlace') ||
        this.context.isToolActiv('legend');
      
      this.ui = new Carmen.ApplicationUI(this.context, 
        {displayNavPanel : displayNavPanel, app : this}
      );
      //alert("computed dpi : "  + this.ui.getScreenResolution());
      this.loadContext(this.context);
      
      // setting maxExtent to initial window extent to prevent wrong zoom operations
      //console.log('setting initial extent ', this.map.getExtent(), this.map.getMaxExtent());
      this.map.initialZoom = this.map.getZoom();
      this.map.maxExtent = this.map.getMaxExtent();
      
      // set initial extent
      this.zoomToInitialExtent();   
      
    }
  },
  
  initMap: function(config) {
    //this.map = new Carmen.Map(this.ui.getMapDiv().id, config);
    //obedel migration 4 -> 5
    //var id_mapDiv = this.ui.getMapDiv().id.replace('-body','-innerCt');
    var id_mapDiv = this.ui.getMapDiv().id;
    this.map = new Carmen.Map(id_mapDiv, config);
    this.map.app = this;
    return this.map;
  }, 
  
  getMap: function() {
    if (this.map==null) {
      this.initMap([]);
    } 
    return this.map;
  },
  
  zoomToInitialExtent: function() {
    var initialExtent = this.context.getExtent();
    var adjusted = false;
    // looking for contextual parameters in URL
    var urlParams = Ext.urlDecode(window.location.search.replace("?", ""));
    var extent = Ext.valueFrom(urlParams.extent, null, false);
    var object = Ext.valueFrom(urlParams.object, null, false);
    if (extent==null && object!=null) {
      layername = object.split(';')[0];
      var layerDesc = this.context.getLayerDescByName(layername);
      if (layerDesc!=null) {
        var minScale = Ext.valueFrom(layerDesc.MinScaleDenominator, null, false);
        var maxScale = Ext.valueFrom(layerDesc.MaxScaleDenominator, null, false);
        if (minScale!=null && minScale>0.0 && maxScale!=null && maxScale>0.0) {
          adjusted = true;
          this.map.zoomToSuitableExtent(initialExtent, minScale, maxScale);
        }
      }
    }
    if (!adjusted) {
      this.map.zoomToExtent(initialExtent);
                  //console.log(initialExtent);

        }

  },
   
  loadContext: function(jsOws) {
    //alert("application 4 1");
    var ctx = this.context;
   
    var mapOptions = {
      maxExtent: ctx.getMaxExtent(),
      projection: ctx.getProjection(),
      maxResolution : 'auto',
      fractionalZoom : false,
      numZoomLevels : 128,
      //allOverlays : true,
      units : ctx.getProjectionUnits(), 
      resolutions : Carmen.Util.MAP_RESOLUTIONS,  
      controls: []
    };
    
    // checking for Tile Cache (aka WMS-C layers)
    // if tile cache --> predefined zoom levels
    // so chganging default mapOptions
    if (ctx.hasLayerWMSC()) {
      mapOptions.fractionalZoom = false;
      mapOptions.numZoomLevels = null;
      mapOptions.maxResolution = null;
      mapOptions.resolutions = Carmen.Util.WMSC_RESOLUTIONS; 
    }
   
    // retrieving map min/max scale from context
    var map_minScaleDenom = ctx.getMinScaleDenom();
    var map_maxScaleDenom = ctx.getMaxScaleDenom();
    
    // filtering resolutions with respect to map min/max scale options
    if (map_minScaleDenom!=null && map_maxScaleDenom!=null) {
      if (mapOptions.resolutions!=null) {
            var res_filtered = [];
            for (var i=0; i<mapOptions.resolutions.length; i++) {
              var sc = OpenLayers.Util.getScaleFromResolution(mapOptions.resolutions[i], "m").toFixed();
              if (sc>=map_minScaleDenom && sc<=map_maxScaleDenom) {
                res_filtered.push(mapOptions.resolutions[i]);
              }
            }
            mapOptions.resolutions = res_filtered;
            mapOptions.numZoomLevels = mapOptions.resolutions.length;
            mapOptions.minScale = map_minScaleDenom;
            mapOptions.maxScale = map_maxScaleDenom;
            mapOptions.maxResolution = mapOptions.resolutions[0];
            mapOptions.minResolution = mapOptions.resolutions[mapOptions.resolutions.length-1];
      }
      else {
        mapOptions.minScale = map_minScaleDenom;
        mapOptions.maxScale = map_maxScaleDenom;  
      }
    }

    this.initMap(mapOptions);
       
    // setting document title
    var title = ctx.getTitle();
    this.ui.setMapTitle(title);
    this.ui.map = this.map;
    document.title = title;
    
 
 
    // adding toolbar tools
    var map = this.map;
    map.addControl(new OpenLayers.Control.Navigation({
      'zoomWheelEnabled': true,
       dragPanOptions: {
            enableKinetic: true
        }
    }));
  
    if (ctx.isToolActiv('fitall')) 
      map.addControl(new Carmen.Control.ZoomFitAll({
          toolbarTarget : ctx.getToolbarTarget('fitall'),
          toolbarPos : 0
        }));

    if (ctx.isToolActiv('zoom')) {
      map.addControl(
        new Carmen.Control.ZoomIn({
          toolbarTarget : ctx.getToolbarTarget('zoom'),
          toolbarPos : 0
        })
      );
      map.addControl(
        new Carmen.Control.ZoomOut({
          toolbarTarget : ctx.getToolbarTarget('zoom'),
          toolbarPos : 1
        })
      );
      map.addControl(
        new Carmen.Control.ZoomHistory({
          displayConfig: Carmen.Control.ZoomHistory.PREV_BTN,
          toolbarTarget : ctx.getToolbarTarget('zoom'),
          toolbarPos : 2
        })
      );
    }
    if (ctx.isToolActiv('pan')) {
      map.addControl(
        new Carmen.Control.DragPan({
          toolbarTarget : ctx.getToolbarTarget('pan'),
          toolbarPos : 3
        })
      );
      // pan arrow not active anymore...
      //map.addControl(new Carmen.Control.PanArrows());
    }
    
    if (ctx.isToolActiv('info')) {
		
    	var urlGetInformation = Routing.generate("frontcarto_query_getinformation");
        //PRODIGE40 window.CARMEN_URL_SERVER_FRONT + '/services/GetInformation/index.php';
        
      map.addControl(new Carmen.Control.Info(
        urlGetInformation,
        window.mapfile, { 
          toolbarTarget : ctx.getToolbarTarget('info'),
          toolbarPos : 5
      }));
      
      map.addControl(new Carmen.Control.InfoCircle(urlGetInformation, window.mapfile));
      map.addControl(new Carmen.Control.InfoURL(urlGetInformation, window.mapfile));
      map.addControl(new Carmen.Control.InfoPolygon(urlGetInformation, window.mapfile));
      if (Carmen.Util.FORCE_SPATIAL_SELECTION_TOOLS) {
        map.addControl(new Carmen.Control.InfoPolygon(urlGetInformation, window.mapfile));
        map.addControl(new Carmen.Control.InfoPolyline(urlGetInformation, window.mapfile));
      }
    }
  
  
    if (ctx.isToolActiv('buffer')) {
      map.addControl(new Carmen.Control.Buffer(
        Routing.generate('frontcarto_buffer_create'),
        //PRODIGE40 window.CARMEN_URL_SERVER_FRONT + '/services/Buffer/index.php',
        window.mapfile));
    }
    

    
    
    
    if (ctx.isToolActiv('measure')) 
       map.addControl(new Carmen.Control.Measure());
    // not used anymore
    /*  
    if (ctx.isToolActiv('refresh'))
       map.addControl(new Carmen.Control.Refresh());
    */

  var controlScaleChooser = null;
  if (ctx.isToolActiv('scale')) {
    controlScaleChooser = new Carmen.Control.ScaleChooser();
  map.addControl(controlScaleChooser);
  }
  
  if (ctx.isToolActiv('geobookmark')) {   
    geoBookmarkConfig = ctx.getFavoriteAreas();
  map.addControl(new Carmen.Control.FavoriteAreas(geoBookmarkConfig));
  }
  
  if (ctx.isToolActiv('context')) {
    map.addControl(new Carmen.Control.ContextManager(
      Routing.generate('frontcarto_context_savecontext'),
      //PRODIGE40 window.CARMEN_URL_SERVER_FRONT + '/services/SaveContext/index.php',
      window.mapfile,
      {
        toolbarTarget : ctx.getToolbarTarget('context'),
        toolbarPos : 5
        } 
      ));
  }
  //console.log("application 4 2b");
  
  this.getLocateByAddress();
    
    if (ctx.isToolActiv('annotation')) {  
      var annotationConfig = ctx.getGeneralConfig('Annotation');
      var labelConfig = ctx.getGeneralConfig('Label');
      var annotationOptions = {
        bufferOptions : {
          serviceUrl : Routing.generate('frontcarto_buffer_create'),
        //PRODIGE40 window.CARMEN_URL_SERVER_FRONT + '/services/Buffer/index.php',
          mapfile : window.mapfile 
        }
      };
      config = annotationConfig && labelConfig ?
       { Annotation  : annotationConfig,
         Label : labelConfig } : null;
      
      Annotations = new Carmen.Control.AdvancedAnnotation(config, annotationOptions);
      map.addControl(Annotations);
    }
    
    if (window.AreaExtractorHandlerURL && window.AreaExtractorHandlerCallback) {
      //window.AreaExtractorHandlerURL = '';
      //window.AreaExtractorHandlerCallback = function(){};
      var areaExtractor = new Carmen.Control.AreaExtractor({
          activeAtStartup : true,
          processResponseURL: AreaExtractorHandlerURL,
          processResponseCallback: AreaExtractorHandlerCallback,
          sizeLimit : window.AreaExtractorSizeLimit ? window.AreaExtractorSizeLimit : null,
          rectOnly : window.AreaExtractorRectOnly ? window.AreaExtractorRectOnly : false
        });
      map.addControl(areaExtractor);
      this.inAreaExtractor = true;
    }
    
    //alert("application 4 3");
    if (ctx.isToolActiv('queryAttributes')) {
      map.addControl(new Carmen.Control.AdvancedQueryAttributes(
        Routing.generate('frontcarto_query_advancedqueryattributes'),
        //PRODIGE40 window.CARMEN_URL_SERVER_FRONT + '/services/AdvancedQueryAttributes/index.php',
        window.mapfile,
        {
          toolbarTarget : ctx.getToolbarTarget('queryAttributes'),
          toolbarPos : 4
          } 
      
      ));
    }
    if (Carmen.Util.FORCE_SPATIAL_QUERY_TOOL || ctx.isToolActiv('querySpatial')) {
      map.addControl(new Carmen.Control.QuerySpatial(
        Routing.generate("frontcarto_query_getinformation"),
      //PRODIGE40 window.CARMEN_URL_SERVER_FRONT + '/services/GetInformation/index.php',
        window.mapfile
      ));
    }

    if (ctx.isToolActiv('help')) 
      map.addControl(new Carmen.Control.Help());
    
    /*if (ctx.isToolActiv('exportPdf')) {
      var layerTreeNode = ctx.mdataMap.LayerTree.LayerTreeNode.LayerTreeNode;
      //map.addControl(new Carmen.Control.ExportPdf());
      //console.log('this.map is : ', this.map);
      //console.log('ctx : ', ctx);
      map.addControl(new Carmen.Control.ImpressionCarte(layerTreeNode, this.map));
    }*/
    var bModelPredefined = false;
    if (ctx.jsOws.ViewContext.General.Extension.tool_exportPdf){
      switch(ctx.jsOws.ViewContext.General.Extension.tool_exportPdf) {
        case "predefinedModels":
          var layerTreeNode = ctx.mdataMap.LayerTree.LayerTreeNode.LayerTreeNode;
          //if(ctx.isToolActiv('exportPdf')) {
            map.addControl(new Carmen.Control.ExportCarte(layerTreeNode, this.map, {
              toolbarTarget : ctx.getToolbarTarget('print'),
              toolbarPos : 6
            }));
          bModelPredefined = true;
        break;
        case "layoutModels":
          /*map.addControl(new Carmen.Control.ExportPdf({
            toolbarTarget : ctx.getToolbarTarget('print'),
            toolbarPos : 6
            }));*/
          map.addControl(new Carmen.Control.ExportPdf());
        break; 
        case "simple"://old version
        case "advanced"://old version
          map.addControl(new Carmen.Control.ExportPdf());
          
        break;
      }
  }
    
    if (!bModelPredefined && ctx.isToolActiv('exportImg')){
      map.addControl(new Carmen.Control.ExportImg());
    }
    if (!bModelPredefined && ctx.isToolActiv('print')){
      map.addControl(new Carmen.Control.Print({
        toolbarTarget : ctx.getToolbarTarget('print'),
        toolbarPos : 6
        })
      );
    }
    if (ctx.isToolActiv('download'))      
      map.addControl(new Carmen.Control.DataDownload());

    if (ctx.isToolActiv('addLayer'))
      map.addControl(new Carmen.Control.AddOGC());
    
    //TODO à modifier
    //if (ctx.isToolActiv('edition')) {
      map.addControl(new Carmen.Control.EditorManager2(
    		window.CARMEN_URL_SERVER_FRONT + '/services/EditionManager/index.php',
    		window.mapfile,
        {
          workingScale: 10000000,//Carmen.Util.EDITION_TOOL_WORKING_SCALE,
    		  callbackUrl: "",//window.computeNumerisationUrl ? window.computeNumerisationUrl : '',
          user_id: 0,//parseInt(Ext.valueFrom(ctx.mdataMap['userIdNumerisation'], -1,false)),
          emprise_id: 0,//parseInt(Ext.valueFrom(ctx.mdataMap['empriseIdNumerisation'], -1, false)),
          emprise_tablename : "",//window.empriseTablename ? window.empriseTablename : '',
          modify_object_url : ""//window.computeSelectionUrl ? window.computeSelectionUrl : ''
        }));
  	//}
// obedel : prodige 3.4 --> localData to be included in addOGC --> addData
/*
    Ext.Ajax.timeout = 120000;
    Ext.Ajax.request({
      url: '/services/AddLocalData/AddLocalData.php',
      params: {'service':'isLocalDataAllowed'},
      failure: function (response) {
        Carmen.Util.handleAjaxFailure(response, control.windowTitle, true); 
      },
      callback: function (options, success, response){
      response = (JSON.parse(response.responseText) || $.parseJSON(response.responseText));
      if(success && response && response.isAllowed == "on"){
        map.addControl(new Carmen.Control.AddLocalData());
      }
      }
    });
*/    
    map.addControl(new Carmen.Control.Coordinate());
    
    
    // referenceMap
    if (ctx.isToolActiv('referenceMap')) {
      var rm_config = ctx.mdataMap.ReferenceMap;
      var rm_imgUrl = rm_config.OnlineResource.attributes.href;
      var rm_size = new OpenLayers.Size(rm_config.attributes.width, rm_config.attributes.height);
      var rm_lc = rm_config.BoundingBox.LowerCorner.split(" ");
      var rm_uc = rm_config.BoundingBox.UpperCorner.split(" ");
      var rm_maxExtent = new OpenLayers.Bounds(rm_lc[0],
                  rm_lc[1], rm_uc[0], rm_uc[1]);
      var rm_projection = ctx.getProjection();
      var rm_projectionUnits = ctx.getProjectionUnits();
      
      // Warning a layer used for an overviewmap should not be used as a base layer 
      // for the general map or for another overviewmap control
      var layerReference =  new OpenLayers.Layer.Image(
        "Reference Map",
        rm_imgUrl,
        rm_maxExtent,
        rm_size, 
        {
          projection : rm_projection,
          units : rm_projectionUnits,
          singleTile : true
        });
      var RmOptions = {
        size: new OpenLayers.Size(180,100),
        layers : [layerReference],
        minRectSize : 5,
        mapOptions : {
          numZoomLevels: 1,
          maxExtent : rm_maxExtent,
          projection : rm_projection,
          units : rm_projectionUnits
        } 
      };
      map.addControl(new Carmen.Control.ReferenceMap(layerReference, RmOptions));
    } 
    //console.log("application 4 10");
    //obedel migration
    
    // Zoom to place
    if (ctx.isToolActiv('zoomToPlace')) {
      // looking for a map specific zoomtoPlace config
      if (ctx.mdataTool.zoomToPlaceConfig != null &&
       ctx.mdataTool.zoomToPlaceConfig != "") {
        var ztp_config = Url.decode(ctx.mdataTool.zoomToPlaceConfig);
        var combosConfig = Carmen.Util.chompAndSplit(ztp_config,';');
        
        for (var i=0; i<combosConfig.length; i++) {
          combosConfig[i] = Carmen.Util.chompAndSplit(combosConfig[i],'|'); 
        }
   
        ztp_configs = [];
        
        for (var i=0; i<combosConfig.length; i++) {
          var config = {
            labelText : combosConfig[i][0],
            fieldNameId: combosConfig[i][2],
            providerUrl: 
               //PRODIGE40 '/services/GetAreas/index.php',
               Routing.generate("frontcarto_query_getareas"),
            providerBaseParams: { 
              'map' : window.mapfile, 
              'layer':  combosConfig[i][1], 
              'fieldId': combosConfig[i][2], 
              'fieldName': combosConfig[i][3],
              'fieldEchelle': combosConfig[i][4]
            }
          };
          // Testing if the combo is linked
          if (combosConfig[i].length>5) {
            if(combosConfig[i].length==7){//new version with fieldEchelle
            var linkedCombo = combosConfig[i][5];
            config['linkedCombo'] = linkedCombo;
            config['linkedFieldName'] = 'filter_id';
            config['providerLinkParams'] = {
                'filter_item': combosConfig[i][6],
                'filter_id' : ''
              };
            }else {//old version without fieldEchelle
              var linkedCombo = combosConfig[i][4];
              config['linkedCombo'] = linkedCombo;
              config['linkedFieldName'] = 'filter_id';
              config['providerLinkParams'] = {
                'filter_item': combosConfig[i][5],
                'filter_id' : ''
              };
            }
          }
          ztp_configs.push(config);
        }

        map.addControl(new Carmen.Control.ZoomToPlace({configs : ztp_configs}));
      }
      else {
        // if the map has no specific zoomtoPlace config, we take the default one form DB 
        var config = {
            providerUrl: 
              //PRODIGE40 window.CARMEN_URL_SERVER_FRONT + '/services/GetAreasFromDB/index.php',
               Routing.generate("frontcarto_query_getareasfromdb"),
            providerBaseParams: { 
              tablename : 'prodige_search_param', 
              service:  '0' // fake one for compatibility reason... 
            }
          };
      
        map.addControl(new Carmen.Control.ZoomToPlaceFromDB({config : config}));
      }
    };
    
    // Layer Tree Manager
    var ctx = this.context;
    var map = this.map;

    var layerTreeManager = new Carmen.Control.LayerTreeManager(
      { visible: ctx.isToolActiv('legend') }
    );
    
    var jsonLayerTree = ctx.mdataMap.LayerTree;
    var jsonLayerList = ctx.layer;
    layerTreeManager.buildTree(jsonLayerTree, jsonLayerList);
    
// obedel debug ext 2.2.1 -> 3.0.0
    this.map.addControl(layerTreeManager);  
    
    var ctx = this.context;
    var map = this.map;
    //var layerTreeManager = map.getLayerTreeManager();
    var layerTree =  layerTreeManager.getLayerTree();
    // need a base layer, get one from mapserver with no layers selected...
    console.log(ctx.mdataMap);
    var baseLayer = new Carmen.Layer.MapServerGroup(
      "Base Map", 
      window.CARMEN_URL_SERVER_DATA + "/cgi-bin/mapserv?map=" + window.mapfile, 
      {
        map_imagetype: ctx.mdataMap.outputFormat,
        map_transparent: ctx.mdataMap.backgroundTransparency=="ON" ? "TRUE" : "FALSE",
        LAYERS: ''
      },
      {
        maxExtent: ctx.getMaxExtent(),
        //maxResolution: 'auto',
        projection: ctx.getProjection(),
        units: ctx.getProjectionUnits(),  // Used in WMS/WFS requests.
        //transitionEffect: 'resize',
        isBaseLayer: true,
        singleTile: true
      });
    map.addLayer(baseLayer);
    // inlays: scale, attributions... 
    if (Ext.valueFrom(ctx.mdataMap.inlay, "0") == "1") {
      // parameters for map inlays 
      var inlayParams = {
        displayTxt : Ext.valueFrom(ctx.mdataMap.scaleTxt, "0") == "1",
        displayImg : Ext.valueFrom(ctx.mdataMap.scaleImg, "0")  == "1",
        displayLogo : Ext.valueFrom(ctx.mdataMap.logo, "0")  == "1",
        fontSize : Ext.valueFrom(ctx.mdataMap.fontSize, null),
        color : Ext.valueFrom(ctx.mdataMap.fontColor, null),
        backgroundColor : Ext.valueFrom(ctx.mdataMap.backgroundColor, null),
        logoPath : (typeof (ctx.jsOws.ViewContext.General.LogoURL)!="undefined" ? Ext.valueFrom(Url.decode(ctx.jsOws.ViewContext.General.LogoURL.OnlineResource.attributes.href), null) : null) 
      };
      
      // incrustation
      if (ctx.mdataMap.font) 
        OpenLayers.Util.extend(inlayParams,     
         Carmen.Util.carmenFontDescToFontStyles(ctx.mdataMap.font)); 
      // Setting map attribution to the base layer
      if (Ext.valueFrom(ctx.mdataMap.copyright, "0") == "1") {
        baseLayer.attribution = Url.decode(ctx.mdataMap.copyrightTxt);
        map.addControl(new OpenLayers.Control.AttributionCarmen(inlayParams));
      }else if(Ext.valueFrom(ctx.mdataMap.logo, "0") == "1"){
			baseLayer.attribution = "";
			map.addControl(new OpenLayers.Control.AttributionCarmen(
							inlayParams));
      }
      if (inlayParams.displayTxt || inlayParams.displayImg) {
        
        //inlayParams.displayImg = false;
        
        var options = OpenLayers.Util.extend(inlayParams, {    
          serverUrl: window.CARMEN_URL_SERVER_DATA + '/cgi-bin/mapserv',
          mapfile: window.mapfile });  
        map.addControl(new OpenLayers.Control.MapserverScale(options));
        //map.addControl(new OpenLayers.Control.ScaleLine());
      }
    }
    // classic overlay layer
    // label layer
    var labelLayer = new Carmen.Layer.MapServerGroup(
      "Label layer", 
      window.CARMEN_URL_SERVER_DATA + "/cgi-bin/mapserv?map=" + window.mapfile,  
      {
        LAYERS : '',
        map_imagetype: 'png',
        map_transparent: true
      },
      {
        maxExtent: ctx.getMaxExtent(),
        projection: ctx.getProjection(),
        units: ctx.getProjectionUnits(),  
        isBaseLayer: false,
        singleTile: true
      });
  
    // build and add layers
    var layers = this._buildLayers2(layerTree, ctx, labelLayer);
    for (var i=layers.length-1; i>=0; i--) {
      var symbolscaledenom = -1;
      var isProportional = false;
      for(var a = 0; a < ctx.layer.length; a++){
        if(ctx.layer[a].Title == layers[i].name){
          if(ctx.layer[a].Extension.TYPE_SYMBO && ctx.layer[a].Extension.TYPE_SYMBO == "PROPORTIONAL"){
            isProportional = true;
            symbolscaledenom = (ctx.layer[a].Extension.layerSettings_symbolScaleDenom ? ctx.layer[a].Extension.layerSettings_symbolScaleDenom : -1);  
          }
        }
      }
      if(isProportional){
        layers[i].params["ISPROPORTIONAL"] = isProportional;
        layers[i].params["SYMBOLSCALEDENOM"] = symbolscaledenom;
      }
      if (layers[i].maxExtent==null)
        layers[i].maxExtent = ctx.getMaxExtent();
        layers[i].displayOutsideMaxExtent = true;
      // handling resolutions for WMSC layers
      if (layers[i].resolutions!=null && map.resolutions!=null) {
        // taking default WMS-C resolutions in map and inserting new ones from WMS-C layers 
        // or replacing existing which are close to new resolutions by new resolutions 
        var newRes = [];
        // if map min/max resolutions defined, filtering layer resolutions
        //  before merging them with map resolutions
        if (map_minScaleDenom!=null && map_maxScaleDenom!=null) {
          for (var k=0; k< layers[i].resolutions.length; k++) {
            var res = layers[i].resolutions[k];
            var sc = OpenLayers.Util.getScaleFromResolution(res, "m").toFixed();
            if (sc>=map_minScaleDenom && sc<=map_maxScaleDenom) {
              newRes.push(res);
            }
          }
        }
        else 
          newRes = [].concat(layers[i].resolutions);
        for (var j=0; j<map.resolutions.length; j++) {
          newRes = Carmen.Util.set_add(
            newRes, 
            map.resolutions[j], 
            Carmen.Util.resNearTester);
        }
        newRes.sort(Carmen.Util.descendingOrder);
        map.resolutions = newRes;
        
        
        //layers[i].maxScale = OpenLayers.Util.getScaleFromResolution(layers[i].resolutions[0]);
        //layers[i].minScale = OpenLayers.Util.getScaleFromResolution(layers[i].resolutions[nbRes-1]);
      }
      // layers considered as overlay
      layers[i].setIsBaseLayer(false);
      
      // testing if layers are responding
      //if (layers[i].checkLayerWellResponding)
      //layers[i].checkLayerWellResponding(map);
    
    map.addLayer(layers[i]);
    }
    map.addLayer(labelLayer);
    map.setLabelLayer(labelLayer);
    
    
    // ToolTip
    map.addControl(new Carmen.Control.ToolTip(
      Routing.generate("frontcarto_query_getinformation"),
      //PRODIGE40 window.CARMEN_URL_SERVER_FRONT + '/services/GetInformation/index.php',
      window.mapfile));
        
    
    
    // other approach : delay processing
    Ext.defer(layerTreeManager.treeLayoutInit, 250, layerTreeManager);
    //layerTreeManager.treeLayoutInit.defer(250, layerTreeManager);
    // new Ext.util.DelayedTask(layerTreeManager.treeLayoutInit, layerTreeManager).delay(500);
    // updating scaleChooser scales wrt to resolutions in case of WMSC layers
    if (map.resolutions!=null && controlScaleChooser!=null) {
      var scales = [];
        for (var i=0; i<map.resolutions.length; i++) {
          var sc = OpenLayers.Util.getScaleFromResolution(map.resolutions[i], "m").toFixed();
            scales.push(Carmen.Util.roundIntToDigits(sc));
        }
      controlScaleChooser.updateScaleValues(scales);
    }

    
    // LegendControl is used by Print, ExportPdf and ExportImg controls
    // Should always be present with a map   
    this.map.legendControl = new Carmen.Control.Legend({
      visible : ctx.isToolActiv('symbology')
     });
    map.addControl(this.map.legendControl);
    
    //preventing mouse offset when resizing border regions of the viewport
    this.ui.getMainPanel().on('resize',
      function(p,w,h) {
        this.map.events.clearMouseCache();
        this.map.updateSize();
      }, this);

    
    // hiding the panel data once it has been rendered
    //this.map.app.ui.locatorButton.toggle();
    //this.map.app.ui.dataButton.toggle();
    
    
    
    return this.context;
 
  },
  
  getLocateByAddress : function(){
    Ext.Ajax.timeout = 120000;
    Ext.Ajax.request({
      url    : Routing.generate('frontcarto_query_getbyaddress'),
      //PRODIGE40 '/services/GetByAddress/index.php',
      params : { service : 'getconfig'},
      scope  : this,
      success: function(response){
        var c_res = eval("("+response.responseText+")");
        var c_config = c_res["prodige_settings_value"] || "";
        if(c_config != ""){
          this.map.addControl(new Carmen.Control.LocateByAddress({"locateaddress" : c_config}));
          return true;
        }
        return false;
      },
      failure: function(response){
        return false;
      }
    });
  },
      
  updateContext : function(bPrint) {
    // extent
    this.context.setExtent(this.map.getExtent());
    
    // layers visibility
    var layerMgr = this.map.getLayerTreeManager();
    var layersVisibility = layerMgr.getLayersVisibility();
    for (var i=0; i<layersVisibility.length; i++) {
      layerVis = layersVisibility[i];
      this.context.setLayerVisibility(layerVis.layerIdx, layerVis.visible);
    }
    
    

    //console.log(layerMgr.serializeForContext());
    this.context.setGeneralConfig(layerMgr.serializeForContext());
    
    
    // favorite areas
    var favoriteAreas = this.map.getControlsByClass('Carmen.Control.FavoriteAreas');
    if (favoriteAreas.length>0) {
      favoriteAreas = favoriteAreas[0];
      this.context.setGeneralConfig(favoriteAreas.serializeForContext());
    }
    
    // annotations
    var annotation = this.map.getControlsByClass('Carmen.Control.AdvancedAnnotation');
    if (annotation.length>0) {
      annotation = annotation[0];
      if(bPrint){
        this.context.setGeneralConfig({Annotation:annotation.exportFeatures()});
        this.context.setGeneralConfig(annotation.exportLabels());
      }
      else {
        this.context.setGeneralConfig(annotation.serializeForContext());
      }
    }
    return this.context.getObj();
  },
  
/*********************************************************************************************
 * Building Layers....
 *********************************************************************************************/   
  
  NODE_LAYER : 0,
  NODE_GROUP : 1,
  
  _buildLayers2 : function(layerTree, ctx, labelLayer) {
    var layers = new Array();
    var providerGrp = new Array();
    window.shiftWMTS = 0;
    this._getProvider2layers(layerTree, providerGrp);
    
    for(var k in providerGrp) {
      var layerNames = '';
      if (providerGrp[k] instanceof Array) {
        for (var j=0; j< providerGrp[k].length; j++) {
         var nodes =  providerGrp[k];
         layerNames = layerNames + '+' + providerGrp[k][j].attributes.jsonLayerDesc.attributes.name;
        }
        var layer = this._buildLayer2(providerGrp[k], k, ctx, labelLayer);
        if (layer!=null)
          layers[layers.length] = layer;
      }
    }
    
    // we descend the tree a second time to get an array 
    // of well ordered layer objects
    layerArray = new Array();
    this._getLayersArray(layerTree, layerArray);
    //console.log(layerArray);
    return layerArray;
  },

  /*
   * @brief descend the tree to fill the layer array so that 
   * layerArray is filled in the layer stack order
   */
  _getLayersArray : function(node, layerArray) {
    if (node.attributes.type == this.NODE_LAYER) {
      var olLayer = node.attributes.OlLayer;
      // we check that the last layer in array doesn't correspond to this node because of groups
      if ((layerArray.length==0) || (layerArray.length>0 && layerArray[layerArray.length-1]!=olLayer)) {
        layerArray.push(olLayer);
      }
    }
    else if (node.attributes.type == this.NODE_GROUP) {
      //obedel migration
      for (var i=0; i<node.childNodes.length; i++) {
        this._getLayersArray(node.childNodes[i], layerArray);
      }
    }
  },

  /*
   * @brief descend the tree to fill the provider2layers array so that 
   * provider2layers[providerGrpId]=[list of layers node...]
   * @param shiftWMTS is used to separate WMTS layer with the same provider 
   *        into differents sources
   */
  _getProvider2layers : function(node, provider2layers) {
    if (node.attributes.type == this.NODE_LAYER) {
      var isWMTS = Ext.valueFrom(node.attributes.jsonLayerDesc.Extension.layerSettings_isWmts,"OFF")=="YES";
      var providerId = node.attributes.providerId;
      if (!(provider2layers[parseInt(providerId)+window.shiftWMTS] instanceof Array)) {
        provider2layers[parseInt(providerId)+window.shiftWMTS] = new Array();
      }
      provider2layers[parseInt(providerId)+window.shiftWMTS][provider2layers[parseInt(providerId)+window.shiftWMTS].length] = node;
      if (isWMTS) {
        // traitement particulier WMTS, on considere un provider different par couche
        window.shiftWMTS = window.shiftWMTS + 1;
      }
      //console.log('/*adding layer ' + node.attributes.jsonLayerDesc.Title + ' to group ' + providerId);
    }
    else if (node.attributes.type == this.NODE_GROUP) {
      //obedel migration
      for (var i=0; i<node.childNodes.length; i++) {
        this._getProvider2layers(node.childNodes[i],provider2layers);
      }
    }
  },  
  
  _buildMapserverLayer2 : function(layerNodes, providerUrl, ctx, labelLayer) {
    var msParams = {
        LAYERS : '',
        map_imagetype : ctx.mdataMap.outputFormat,
        map_transparent: 'TRUE'
    };    
    var layerNames = "";
    var handleSelection = false;
    for (var i =0; i<layerNodes.length; i++) {
      var node = layerNodes[i];
      var lname = node.attributes.jsonLayerDesc.attributes.name;
      layerNames = layerNames + '+' + lname;
      /*
      if (Ext.valueFrom(node.attributes.minScale, null, false)!=null) 
       msParams['map.layer[' + lname + ']']= 'MINSCALEDENOM ' + (parseFloat(node.attributes.minScale) * 0.999999);
      if (Ext.valueFrom(node.attributes.maxScale, null, false)!=null) { 
       msParams['map.layer[' + lname + ']']= 'MAXSCALEDENOM ' + (parseFloat(node.attributes.maxScale) * 1.000001);
      }*/
      handleSelection = handleSelection ||
        (layerNodes[i].attributes.jsonLayerDesc.Extension.layerSettings_infoFields!='')
    }
    layerNames = layerNames.slice(1);
    
    // little hack to retrieve mapfile from wfs...
    var mapfile = node.attributes.jsonLayerDesc.Extension.layerSettings_mapfile;
    mapfile = mapfile!=null ? mapfile : window.mapfile;
    var layer = new Carmen.Layer.MapServerGroup(
          layerNames,  
          providerUrl,
          msParams,  
          {
            projection: ctx.getProjection(), 
            units: ctx.getProjectionUnits(), 
            singleTile: true, 
            mapfile: mapfile,
            selectionSupport: handleSelection  
          });
    layer.setVisibility(false);
    for (var i =0; i<layerNodes.length; i++) {
      var desc = layerNodes[i].attributes.jsonLayerDesc;
      var lname = desc.attributes.name;
      var lvis =  !(desc.attributes.hidden == "1");
      layer.addSubLayer(lname, lvis);
      var hasLabel = desc.Extension.hasLabel=="1";
      layerNodes[i].attributes.hasLabel = hasLabel;
      
      console.log(lname);
      console.log(layerNodes[i].attributes.opacity);
      
      if(layerNodes[i].attributes.opacity && layerNodes[i].attributes.opacity!=1){
        layer.params['map.layer[' + lname + ']'] = "OPACITY "+layerNodes[i].attributes.opacity*100;
      }
      //layer.params['map.layer[' + lname + ']'] = "OPACITY "+;
      //&map.layer[layer29]=OPACITY 
      
      if (hasLabel) {
        /*
        // first, switch off labels for each class of the layer
        var classes = [];
        // TODO put this in Util : arrayFromAttribute...
        if ('StyleList' in desc) {
          classes = desc.StyleList.Style instanceof Array ? 
            desc.StyleList.Style : 
            [].concat(desc.StyleList.Style);
        }
        var params = {};
        
        for (var j=0; j< classes.length; j++) {
          console.log(classes[j]);
          layer.params['map.layer[' + lname + '].class[' + j + '].label[0]'] = 'SIZE -1 END';
        }*/
        
        layer.params['map.layer[' + lname + ']'] += " LABELREQUIRES '0'";
        // then, add a copy of this layer in the composite label layer,
        // and switch off its symbols
        labelLayer.addSubLayer(lname, lvis);
        labelLayer.params['map.layer[' + lname + ']'] = "OPACITY 1 LABELREQUIRES '1'";
      }
    }
    return layer;         
  },
  
  _buildWMSLayer2 : function (layerNodes, providerUrl, ctx) {
    var layerNames = "";
    for (var i =0; i<layerNodes.length; i++) {
      var name = layerNodes[i].attributes.jsonLayerDesc.Title;
      layerNames = layerNames + ',' + name;
    }
    
    layerNames = layerNames.slice(1);
    firstLayerDesc = layerNodes[0].attributes.jsonLayerDesc;
    var version =  Url.decode(firstLayerDesc.Server.attributes.version);
    var outputFormat = Url.decode(firstLayerDesc.Extension.wms_format);
    var projection = Ext.valueFrom(Url.decode(firstLayerDesc.Extension.wms_srs), ctx.getProjection(), false);
    var projUnits = Carmen.Util.getProjectionUnits(projection);
    var opacity = layerNodes[0].attributes.opacity;
    
    var layer = new Carmen.Layer.WMSGroup(
          layerNames,  
          providerUrl,
          {
            LAYERS : '',
            version: version,
            format : outputFormat,
            transparent: true
          }, 
          {
            isBaseLayer : false,
            singleTile : true,
            projection : projection, 
            units : projUnits,
            opacity: opacity
          });

    //console.log("version " + version + " outputFormat " + outputFormat + " projection " + projection + " providerURL " + providerUrl);

    layer.setVisibility(false);
    for (var i =0; i<layerNodes.length; i++) {
      var lname = layerNodes[i].attributes.jsonLayerDesc.attributes.name;
      var lwmsName = layerNodes[i].attributes.jsonLayerDesc.Name
      var lvis =  !(layerNodes[i].attributes.jsonLayerDesc.attributes.hidden == "1");
      layer.addSubLayer(lname, lvis, lwmsName);
      //var domIconEl = layerNodes[i].getUI().getIconEl();
      //domIconEl.src = layer.getLegendGraphicURL(lwmsName);
      //console.log("name " + lname + " lwmsname " + lwmsName );
    }
    return layer;
  },
  
  _buildWMSCLayer2 : function (layerNodes, providerUrl, ctx) {
    var layerNames = "";
    for (var i =0; i<layerNodes.length; i++) {
      var name = layerNodes[i].attributes.jsonLayerDesc.Title;
      layerNames = layerNames + ',' + name;
    } 
    layerNames = layerNames.slice(1);
    firstLayerDesc = layerNodes[0].attributes.jsonLayerDesc;
    var version =  Url.decode(firstLayerDesc.Server.attributes.version);
    var outputFormat = Url.decode(firstLayerDesc.Extension.wms_format);
    var projection = Ext.valueFrom(Url.decode(firstLayerDesc.Extension.wms_srs), ctx.getProjection(), false);
    var projUnits = Carmen.Util.getProjectionUnits(projection);
    var opacity = layerNodes[0].attributes.opacity;
    
    layerOptions = {
      isBaseLayer : false,
      buffer : 0,
      tileSize : new OpenLayers.Size(256,256),
      projection : projection,
      resolutions : null,
      units : projUnits,
      opacity : opacity };
   
    // getting wmsc boundingbox if defined
    var str_boundingbox = Ext.valueFrom(firstLayerDesc.Extension.wmsc_boundingbox, null, false);
    
    layerOptions.maxExtent = ctx.getMaxExtent();
    if (str_boundingbox) {
      var sep = str_boundingbox.indexOf(',') != -1 ? ',' : ' ';
      var bb = Carmen.Util.chompAndSplit(Url.decode(str_boundingbox), sep);
      layerOptions.maxExtent = new OpenLayers.Bounds(bb[0], bb[1], bb[2], bb[3]);
    }
    // getting wmsc resolution if defined
    var str_res = Ext.valueFrom(firstLayerDesc.Extension.wmsc_resolution, null, false);
    if (str_res!=null) {
      str_res = Url.decode(str_res);
      var sep = str_res.indexOf(',') != -1 ? ',' : ' ';
      var res = Carmen.Util.chompAndSplit(str_res, sep);
      // kind of defensive prog to prevent wrong format...
      if (!isNaN(new Number(res[0]))) {
        layerOptions.resolutions = Carmen.Util.array_map(res, parseFloat);
      }
    }
          //this.context.mapExtent = layerOptions.maxExtent;
    var layer = new Carmen.Layer.WMSGroup(
          layerNames,  
          providerUrl,
          {
            LAYERS : '',
            version: version,
            format : outputFormat 
          }, 
          layerOptions);
    layer.setVisibility(false);
    for (var i =0; i<layerNodes.length; i++) {
      var lname = layerNodes[i].attributes.jsonLayerDesc.attributes.name;
      var lwmsName = layerNodes[i].attributes.jsonLayerDesc.Name;
      var lvis =  !(layerNodes[i].attributes.jsonLayerDesc.attributes.hidden == "1");
      layer.addSubLayer(lname, lvis, lwmsName);
      // updating wms icon
      //var domIconEl = layerNodes[i].getUI().getIconEl();
      //domIconEl.src = layer.getLegendGraphicURL(lwmsName);
    }

          //console.log(layer);
    return layer;
  },

    _buildWMTSLayer2 : function (layerNodes, providerUrl, ctx) {
      var layerNames = "";
      for (var i =0; i<layerNodes.length; i++) {
        var name = layerNodes[i].attributes.jsonLayerDesc.Title;
        layerNames = layerNames + ',' + name;
      } 
      layerNames = layerNames.slice(1);
      firstLayerDesc = layerNodes[0].attributes.jsonLayerDesc;
      //var version =  firstLayerDesc.Server.attributes.version;
      var outputFormat = firstLayerDesc.Extension.wms_format;
      var style = firstLayerDesc.Extension.wmts_style;
      var tileset = firstLayerDesc.Extension.wmts_tileset;
      var wmts_layer_name = firstLayerDesc.Extension.wmts_wms_layer;
      // resolution
      var serverResolution = null;
      var str_res = Url.decode(firstLayerDesc.Extension.wmts_resolutions);
      var res = Carmen.Util.chompAndSplit(str_res, ' ');
      if (!isNaN(new Number(res[0]))) {
        serverResolution = Carmen.Util.array_map(res, parseFloat);
      }
      for(var i=0;i<serverResolution.length;i++){
        serverResolution[i]=(serverResolution[i]/0.75)*0.7500000000000001;
      }
      
      // matrixIds
      var matrixIds = [];
      var str_matrixids = Url.decode(firstLayerDesc.Extension.wmts_matrixids);
      var ids_matrix = Carmen.Util.chompAndSplit(str_matrixids, ' ');
      var tileOrigins = [];
      var str_tileOrigins = Url.decode(firstLayerDesc.Extension.wmts_tileorigin);                  
      var tileorigins_coords = Carmen.Util.chompAndSplit(str_tileOrigins, ' ');
      for (var i=0; i<ids_matrix.length; i++) {
          
          var coords = Carmen.Util.chompAndSplit(tileorigins_coords[i], ',');
          var tlc = new OpenLayers.LonLat(
            parseFloat(coords[0]),
            parseFloat(coords[1]));
          var scaleDenom = Carmen.Util.getScaleFromResolution(serverResolution[i], 'm');
          matrixIds.push({
            identifier : ids_matrix[i],
            topLeftCorner : tlc ,
            scaleDenominator: scaleDenom});

      }
      
      // tileOrigin
      var str_tileorigin =  Url.decode(firstLayerDesc.Extension.wmts_tileorigin);
      var tileorigin_coords2 = Carmen.Util.chompAndSplit(str_tileorigin, ' ');
      var tileorigin_coords = [];
      for(var i=0; i<tileorigin_coords2.length;i++){	 
    	  tab=Carmen.Util.chompAndSplit(tileorigin_coords2[i].toString(),',')
    	  tileorigin_coords.push(tab[0],tab[1]);
      }
      var tileorigin = new OpenLayers.LonLat(
        parseFloat(tileorigin_coords[0]),
        parseFloat(tileorigin_coords[1]));
      var opacity = layerNodes[0].attributes.opacity;
      
      var layer = new Carmen.Layer.WMTSGroup({
        name: wmts_layer_name,
        url: providerUrl,
        layer: wmts_layer_name,
        matrixSet: tileset,
        matrixIds : matrixIds,
        format: (typeof(firstLayerDesc.Extension.wms_format)!="undefined"  ? decodeURIComponent(firstLayerDesc.Extension.wms_format) : "image/png"),
        style: style,
        opacity: opacity,
        isBaseLayer: false,
        //singleTile: false,
        serverResolutions : serverResolution,
        resolutions : serverResolution,
        visibility: false                  
      });
      //layer.setVisibility(false);
      //layer.setVisibility(false);
        
        for (var i =0; i<layerNodes.length; i++) {
          var lname = layerNodes[i].attributes.jsonLayerDesc.attributes.name;
          var lwmtsName = layerNodes[i].attributes.jsonLayerDesc.Extension.wmts_wms_layer;
          var lvis =  !(layerNodes[i].attributes.jsonLayerDesc.attributes.hidden == "1");
          layer.addSubLayer(lname, lvis, lwmtsName);
          // updating wms icon
          //var domIconEl = layerNodes[i].getUI().getIconEl();
          //domIconEl.src = layer.getLegendGraphicURL(lwmsName);
        }


      return layer;
    },

  _buildVectorLayer2 : function(node, ctx) {
    var desc = node.attributes.jsonLayerDesc;
    var layer = new Carmen.Layer.Vector(desc.Title, 
      { context_kml : desc.Document, 
        context_style : desc.Extension.layerSettings_olStyle,
        useStyleMap : true });
    layer.setVisibility(true);
    return layer;         
  },

  _buildLayer2 : function(layerNodes, providerGrpId, ctx, labelLayer) {
    var layer = null;
    var providerUrl = null;
    if (!layerNodes)
      return;
  
    firstLayerDesc = layerNodes[0].attributes.jsonLayerDesc;
    if ('Server' in firstLayerDesc) {
      
    //TODO élément de différenciation à améliorer
      if (!('wms_layer' in firstLayerDesc.Extension)) {
        // Mapserver-CGI layer ? 
        providerUrl = firstLayerDesc.Server.OnlineResource.attributes.href;
        // @Todo: add better mapserver detection 
        var isMapserverLayer = (providerUrl.search(/mapserv/i)>0);
        if (isMapserverLayer) {
          layer = this._buildMapserverLayer2(layerNodes, providerUrl, ctx, labelLayer);
        }
      }
      else if ('Server' in firstLayerDesc) {
        // WMS layer ?
        var service = firstLayerDesc.Server.attributes.service;

        var providerUrl = firstLayerDesc.Server.OnlineResource.attributes.href;
        var isWMS = (service.search(/WMS/i)>-1);
        if (isWMS) {
          layer = this._buildWMSLayer2(layerNodes, providerUrl, ctx);
        }
        var isWMSC = Ext.valueFrom(firstLayerDesc.Extension.layerSettings_isWmsc,"OFF")=="YES";
        if (isWMSC) {
          layer = this._buildWMSCLayer2(layerNodes, providerUrl, ctx);
        }
        var isWMTS = Ext.valueFrom(firstLayerDesc.Extension.layerSettings_isWmts,"OFF")=="YES";
        if (isWMTS) {
            layer = this._buildWMTSLayer2(layerNodes, providerUrl, ctx);
          }
      }
      
      if (layer != null) {
        // linking nodes with OpenLayers layer 
        for (var i=0; i< layerNodes.length; i++) {
          var node = layerNodes[i];
          var layerDesc = layerNodes[i].attributes.jsonLayerDesc;
          node.attributes.OlLayer = layer;
          node.attributes.labelLayer = labelLayer;
          // linking check control to layer display
      /*
          node.addListener('checkchange', 
            function(n, cb_state) {
              var layer = n.attributes.OlLayer;
              var lname = n.attributes.jsonLayerDesc.Title;
  
                layer.setSubLayerVisibility(lname, n.attributes.checked);
                // switching off label if necessary...
                if (n.attributes.hasLabel) {
                  labelLayer.setSubLayerVisibility(lname, n.attributes.checked);
                }
  
            }, 
          node); 
      */    
        }
      }
    }
    else if ('Document' in firstLayerDesc) {
      node = layerNodes[0];
      layer = this._buildVectorLayer2(node, ctx);
      if (layer != null) {
        // linking nodes with OpenLayers layer 
        node.attributes.OlLayer = layer;
        // linking check control to layer display
      /*  node.addListener('checkchange', 
          function(n, cb_state) {
            var layer = n.attributes.OlLayer;
            layer.setVisibility(n.attributes.checked);
          }, 
        node);      */
      }
    } 
    return layer;
  }  
  

});

Carmen.Application.CLASS_NAME = "Carmen.Application";

/*********************************************************************************************
 * Context Class definition
 *********************************************************************************************/   


Carmen.JSONContext = new OpenLayers.Class({
  jsOws : null,
  generalExt : null,
  layer : [],
  mapProjection : null,
  
  mdataTool : null,
  mdataMap : null,
  mdataLayer : null,
  mapExtent : null,
  maxExtent : null,
  mapMinScaleDenom : null,
  mapMaxScaleDenom : null,

  initialize: function(context) {
    this.jsOws = context;
    this.generalExt = this.jsOws.ViewContext.General.Extension; 
    if (this.jsOws.ViewContext.LayerList && this.jsOws.ViewContext.LayerList.Layer) {
     this.layer = this.jsOws.ViewContext.LayerList.Layer instanceof Array ?
      this.jsOws.ViewContext.LayerList.Layer :
      [].concat(this.jsOws.ViewContext.LayerList.Layer);
     this.jsOws.ViewContext.LayerList.Layer = this.layer
    }
    this.parseMdata();
    
    //console.log(this.mdataTool);
    //console.log(this.mdataMap);
    //console.log(this.mdataLayer);
    
  },
  
  getObj : function () {
    return this.jsOws;  
  },
  
  getProjection :  function() {
    if (this.mapProjection==null)
      this.mapProjection= this.jsOws.ViewContext.General.BoundingBox.attributes.crs.split('=')[1].toLocaleUpperCase();
    
    return this.mapProjection;
  },
  
  getProjectionUnits : function() {
    if (this.mapProjectionUnits==null)
      this.mapProjectionUnits= Carmen.Util.getProjectionUnits(this.getProjection());
    return this.mapProjectionUnits;
  },
  
  getExtent :  function() {
    if (this.mapExtent==null) {
      this.mapExtent = Carmen.Util.convertContextBBtoolBounds(
        this.jsOws.ViewContext.General.BoundingBox);
    }
    return this.mapExtent;
  },
  
  getMaxExtent :  function() {
    if (this.maxExtent==null) {
      this.maxExtent = Carmen.Util.convertContextBBtoolBounds(
        this.jsOws.ViewContext.General.Extension.MaxBoundingBox);
    }
    return this.maxExtent;
  },
  
  getMaxScaleDenom : function() {
      if (this.mapMaxScaleDenom==null && 
          this.jsOws.ViewContext.General.Extension.MaxScaleDenom!=null) {
        this.mapMaxScaleDenom = parseFloat(this.jsOws.ViewContext.General.Extension.MaxScaleDenom);
        if (this.mapMaxScaleDenom ==-1) 
           this.mapMaxScaleDenom=null;
      }
      return this.mapMaxScaleDenom;
  },
  
  getMinScaleDenom : function() {
      if (this.mapMinScaleDenom==null && 
          this.jsOws.ViewContext.General.Extension.MinScaleDenom!=null) {
        this.mapMinScaleDenom = parseFloat(this.jsOws.ViewContext.General.Extension.MinScaleDenom);
        if (this.mapMinScaleDenom == -1)   
           this.mapMinScaleDenom=null;
      }
      return this.mapMinScaleDenom;
  },
  
  
  getTitle :  function() {
    return Url.decode(this.jsOws.ViewContext.General.Title);
  }, 

  getFavoriteAreas : function() {
    return this.generalExt.GeoBookmark ? this.generalExt.GeoBookmark : null;
  },
  
  setExtent : function(bounds) {
    this.jsOws.ViewContext.General.BoundingBox.attributes.minx = bounds.left;
    this.jsOws.ViewContext.General.BoundingBox.attributes.miny = bounds.bottom;
    this.jsOws.ViewContext.General.BoundingBox.attributes.maxx = bounds.right;
    this.jsOws.ViewContext.General.BoundingBox.attributes.maxy = bounds.top;
  },
  
  setLayerVisibility : function(layerIdx, visible) {
    this.jsOws.ViewContext.LayerList.Layer[layerIdx].attributes.hidden = visible ? "0" : "1";
  },
  
  setGeneralConfig : function(config) {
    for(p in config) 
     this.generalExt[p] = config[p];
  },
  
  getGeneralConfig : function(configName) {
    var config = null;
    if (configName in this.generalExt)
      config = this.generalExt[configName];
    return config;
  },
  
  isToolActiv : function(toolName) {
    var toolValue = Ext.valueFrom(this.mdataTool[toolName], "0") 
    return (toolValue=="1" || toolValue=="ON" || toolValue== "simple"|| toolValue=="advanced");
  },
  
  getToolbarTarget : function(toolName) {
    var toolValue = Ext.valueFrom(this.mdataTool[toolName], "0") 
    return toolValue;
  },

  hasLayerWMSC : function() {
    var found = false;
    for (var k in this.mdataLayer) {
      if (Ext.valueFrom(this.mdataLayer[k].wmscLayer, "OFF", false)=="ON"
         && Ext.valueFrom(this.mdataLayer[k].isWmts, "NO", false)=="NO" ) {
        found = true;
        break;
      }
    }
    return found;
  }, 

  // fill hash mdataTool, mdataMap and mdataLayer 
  // with mdata from Extension parts of the context
  parseMdata : function () {
    this.mdataTool = {};
    this.mdataMap = {};
//    console.log(this.generalExt);
    for (var k in this.generalExt) {
      //console.log(k);
      var matched = k.match(/^tool_(.+)$/); 
      if (matched!=null) {
        var toolName = matched[1];
        //console.log(toolName);
        this.mdataTool[toolName] = this.generalExt[k];
      }
      else {
        matched = k.match(/^mapSettings_(.+)$/);
        if (matched!=null) {
          var setting = matched[1];
          this.mdataMap[setting] = this.generalExt[k];
        }
        else
          this.mdataMap[k] = this.generalExt[k];
      }
    }
    this.mdataLayer = {};
    if (this.layer) {
      if (this.layer instanceof Array) {
        for (var i=0; i<this.layer.length; i++) {
          var layerName =  this.layer[i].Title;
          this.mdataLayer[layerName] = {};
          for (var k in this.layer[i].Extension) {
            var matched = k.match(/^layerSettings_(.+)$/);
            var setting = matched!=null ? matched[1] : k;
            this.mdataLayer[layerName][setting] = this.layer[i].Extension[k];     
          }
        }
      }
      else {
        var layerName =  this.layer.Title;
        this.mdataLayer[layerName] = {};
        for (var k in this.layer.Extension) {
          var matched = k.match(/^layerSettings_(.+)$/);
          var setting = matched!=null ? matched[1] : k;
          this.mdataLayer[layerName][setting] = this.layer.Extension[k];     
        } 
      }
    }
  }, 
  
  
  getNextLayerId : function() {
    return this.layer.length;
  },
  
  addLayerDescs :  function(descs) {
    for (var i=0; i< descs.length; i++)
      this.layer.push(descs[i]);
    return (this.layer.length-1);
  },
  
  getLayerDescByName : function(value) {
    var i=0;
    for (var k in this.layer) {
      var v = Ext.valueFrom(Url.decode(this.layer[k].Extension.LAYER_ID), null, false);
      if (v==value) {
        break;
      }
      i++;
    }
    return i <= (this.layer.length-1) ? this.layer[i] : null;
  },
  
  getLayerDescFromMdataValue : function(attributeName, value) {
    var i=0;
    for (var k in this.mdataLayer) {
      var v = Ext.valueFrom(Url.decode(this.mdataLayer[k][attributeName]), null, false);
      if (v==value) {
        break;
      }
      i++;
    }
    return i <= (this.layer.length-1) ? this.layer[i] : null;
  }
});


Carmen.Application.debugDD = function(layerTree, map) {
    var layerNodes = layerTree.collectChildNodes(
        function (n) {
          return ('type' in n.attributes && 
            (n.attributes.type == Carmen.Control.LayerTreeManager.NODE_LAYER ))
        });
    for (var i =0; i < layerNodes.length; i++) {
      var node = layerNodes[i];
      node.setText(map.getLayerIndex(node.attributes.OlLayer) +  '  ' + 
        node.attributes.OlLayer.getSubLayerInfo(node.attributes.layerName).index + ' ' +
        node.attributes.layerName);
    }
};

Carmen.JSONContext.CLASS_NAME = "Carmen.JSONContext";
