  /**
 * @_requires OpenLayers/Controljs
 */

/**
 * Class: Carmen.Control.Info
 
 * Inherits from:
 *  - <OpenLayers.Control>
 */

Ext.Ajax.setTimeout(120000); // 120 seconds

Carmen.Control.Info = new OpenLayers.Class(OpenLayers.Control, {

  // UI components
  btn: new Ext.Button({
      tooltip: 'Information',      
      tooltipType: 'title',
      cls: 'x-btn-icon', /* cmn-tool-info',*/
      enableToggle: true,
      toggleGroup: 'mainMapControl',
      disabled: false,
      text : '<i class="fa fa-info fa-2x"></i>',
      width: ctrlWidth,
      height: ctrlHeight
  }),
  win : null,
  headerPanel : null,
  //layerTreeInfoPanel : null,
  gridInfoPanel : null,

  //TODO temporaire
  inGroup : true,

  // properties 
  /**
   * Property: type
   * {OpenLayers.Control.TYPE}
   */
  type: OpenLayers.Control.TYPE_TOOL,

  toolbarTarget: 'advanced',

  toolbarPos: 5,

  btnGrpDefaultActiv : true,

  queryInProgress : false,

  layerTreeManager : null,

  layerTreeRoot : null,

  infoTreeStore : null,

  mapfile : null,

  serviceUrl : null,

  windowTitle : 'Information',

  showGraphicalSelection : true,

  // count of queries which response is still being waited
  queriesSent : 0,
  totalCount : 0,
  fullExtent : null,

  requestTimeout : 120000, //set to 120s for long requests

  WMSResponseFormats : {
  	'text/plain': new OpenLayers.Format.Text(),
  	'application/vnd.ogc.gml': new OpenLayers.Format.WMSGetFeatureInfo(),
  	'text/xml': new OpenLayers.Format.WMSGetFeatureInfo()
  	//'application/vnd.ogc.gml': new OpenLayers.Format.GML()
  },


  // methods
  initialize: function(serviceUrl, mapfile, options) {
	OpenLayers.Control.prototype.initialize.apply(this, [options]);    
    this.mapfile = mapfile;
    this.serviceurl = serviceUrl;

  },

  initializeUIComponents : function() {
	  this.win = new Ext.Window({
	    id : this.displayClass +'_' +'window',
	    layout:'anchor',
	    width:400,
	    height: 400,
      constrain : true,
	    plain: true,
	    modal:false,
	    autoDestroy :true,
	    resizable:true,
	    closeAction: 'hide',
	    shadow : false,
      layout: 'fit'
	  });
  },

  setMap: function(map) {
    OpenLayers.Control.prototype.setMap.apply(this, arguments);

    this.infoTreeStore = Ext.create('Ext.data.TreeStore', {
      id : 'infoTreeStore',
      root: {
        expanded: true,
        text : 'Information',
        children: []
      },
      model : 'cmn.layerTreeModel'
    });

    this.initializeUIComponents();

    this.btn.addListener('toggle', Carmen.Util.buildExt2olHandlerToggle(this));

    if (this.toolbarTarget=='simple')
      this.map.app.ui.addToToolPanel(this.btn, this.toolbarTarget, this.toolbarPos);
    else
      this.map.app.ui.addToCycleButtonGroup('advanced', 'Interrogations' ,'info', this.btn, 0, this.btnGrpDefaultActiv);

    this.map.events.register('emptySelection', this,
    function(evt) {
      if (!this.queryInProgress) {
        this.clearWindow(); 
      }
    });
  },
  
  /**
   * Method: draw
   */    
  draw: function() {
      this.handler = new OpenLayers.Handler.Box( this,
                          {done: this.info }, {keyMask: this.keyMask} );
  },

  activate: function() {
  	if (this.getLayerTreeManager==null) {
      Ext.MessageBox.alert("Warning", "The control " +
       this. CLASS_NAME +
       " requires the LayerTreeManager control to work.");
      return null;
    }

    OpenLayers.Control.prototype.activate.apply(this, arguments);
    this.map.toolTipDeactivate();
    
    var editorManager = this.map.getControlsByClass("Carmen.Control.EditorManager2");
    if ( editorManager && editorManager.length ){
      this.editorManager = editorManager[0];
    } else {
      this.editorManager = null;
    }
  },


  deactivate: function() {
  	// removing highlighting...
    this.map.clearGraphicalSelection();
  	this.map.clearSelection();
  	OpenLayers.Control.prototype.deactivate.apply(this, arguments);
  	if (this.win.rendered)
      this.win.hide();
    this.map.toolTipActivate();
  },


  /**
   * Method: info
   *
   * Parameters:
   * position - {<OpenLayers.Bounds>} or {<OpenLayers.Pixel>}
   */
  info : function (position) {
    this.showInfoWindow(position);
  },

  // displaying results
  showInfoWindow : function(position) {
    waitMessage= Ext.MessageBox.wait('Interrogation en cours, merci de patienter...');

    this.layerTreeRoot = this.getLayerTreeManager().getLayerTree().clone(this.transformNodeInfo);

    if (!this.win.rendered) {
      for(var i = 0; i < this.layerTreeRoot.childNodes.length; i++){
    	var currentChild = this.layerTreeRoot.childNodes[i];
        if(currentChild.data.text.match(/<table.*<\/table>/)){
          currentChild.data.text = currentChild.data.text.replace(/<table.*<\/table>/, "");
        }
      }
    } 

    if (this.showGraphicalSelection) 
      this.map.clearGraphicalSelection();
    this.queryInProgress = true;
    this.map.clearSelection();
    this.queryInProgress = false;
    this.performQueries(position);


  },

  clearWindow : function() {
    if (this.win.rendered) {
      //nop
    }
  },

  getLayerTreeManager : function() {
    if (this.layertreeManager == null) {
      var controls = this.map.getControlsByClass('Carmen.Control.LayerTreeManager');
      this.layerTreeManager = controls.length==0 ? null : controls[0];
    }
    return this.layerTreeManager;
  },

  performQueries : function(position) {
    var control = this;
    
	  // filter the node
    this.layerTreeRoot.filter(this.filterNodeLayerQueryable);
//           
//     
    var isLayerNode = function(n) { 
    	return ('type' in n.attributes && (n.attributes.type == Carmen.Control.LayerTreeManager.NODE_LAYER)); 
    }; 
    var nodes = this.layerTreeRoot.collectChildNodes(isLayerNode);
    
    if (nodes.length==0) {
    	//this.headerPanel.setTitle('Aucun résultat');
    	//this.gridInfoPanel.hide();
      Ext.Msg.alert("Outil d'information", "Aucune donnée n'est interrogeable avec cet outil");
    } else {
	    var ms_nodes = {};
  	  var wms_nodes = [];
    	var wms_layers = [];

	    for (var i=0; i<nodes.length; i++) {
	    	var layer = nodes[i].attributes.OlLayer;
		    if (layer instanceof Carmen.Layer.WMSGroup) { 
					wms_nodes.push(nodes[i]);
					Carmen.Util.set_add(wms_layers, layer);
		    } else {
				  var mapfile = nodes[i].attributes.OlLayer.mapfile;
				  if (mapfile) {
				   if (!ms_nodes[mapfile])
				     ms_nodes[mapfile] = [];
				   ms_nodes[mapfile].push(nodes[i]);  
				  }
   			}
	    }
  	 	// cancel all pending requests
  	 	Ext.Ajax.abort();
  		this.queriesSent = 0;
  		this.totalCount = 0;
  		this.fullExtent = null;
  		// launching requests
  		
  		this.map.app.ui.delAllInfoPanel();
  		
  		for (var m in ms_nodes) {
    		if (ms_nodes[m].length>0) {
    			this.performQuery(this.buildParams(ms_nodes[m], position, null, m));
    		}
  		}
  		if (position instanceof OpenLayers.Pixel) {
  			for (var i=0; i<wms_layers.length; i++) { 
  				this.performWMSQuery(wms_layers[i], position);
  			}
  		}
    }
  },

  registerResults : function(response, queryType) {


	  var control = this;
	  this.queriesSent--;
	  this.totalCount = this.totalCount + parseInt(response.totalCount);

    if(this.queriesSent == 0 && this.totalCount == 0)
      Ext.Msg.alert("Outil d'information", "Aucun résultat ne correspond à votre recherche.");
	  this.map.app.ui.infoPanel.setTitle(this.totalCount + ' résultats');
	  if (queryType==Carmen.Control.Info.SRC_MS) {
		  var currentExtent = Carmen.Util.strExtentToOlBounds(response.extent, Carmen.Control.Info.PUNCTUAL_RADIUS);
		  this.fullExtent = this.fullExtent == null ? currentExtent : this.fullExtent.extend(currentExtent); 
		  this.map.app.ui.infoPanel.bounds = this.fullExtent;
      
      
      
		  for(lname in response.results)  {
			  var node = control.layerTreeRoot.findChildByAttribute('layerName', lname);
			  if (node!=null) {
				  var olLayer = node.attributes.OlLayer; 
				  if(response.results[lname].queryfile !=""){
            olLayer.setDisplayMode(
                Carmen.Layer.MapServerGroup.DISPLAY_NQUERYMAP, 
                node.attributes.layerName, null,
                response.results[lname].queryfile);
          }
          
          //obedel migration, pb registering selection...
          
				  // registering selection
				  if (olLayer.handleSelection(lname)) { 
					  this.map.addToSelectionLayers(olLayer);
					  olLayer.setSelectionStore(lname, 
							  new Ext.data.SimpleStore({
								  fields: response.results[lname].fields.concat(['extent', 'fid']),
								  data :  response.results[lname].data
							  }));
				  }
			  }
		  }
	  }
  control.buildResultPanels(control.layerTreeRoot, response.results, queryType, response.queryfile);

  },

  getRouteQuery : function(){
  	var editableLayers = [];
  	if ( this.editorManager ){
  		Ext.iterate(this.editorManager.editableLayers, function(layerId, metadata){if ( metadata.EDITION || metadata.EDITION_MODIFICATION ) editableLayers.push(layerId)});
  	}
    return Routing.generate("frontcarto_query_getinformation", {editableLayers : editableLayers.join('/')});
  },

  // traditionnal query on mapserver layer
  performQuery : function (queryParams) {

    // prepraring answer callback
    var received = function (response) {
      response = Ext.decode(response.responseText);
	  	this.registerResults(response, Carmen.Control.Info.SRC_MS); 	
    }
    
    // launching the request
    this.queriesSent++;
    
    //PRODIGE40 var url = '/services/GetInformation/index.php';
    var url = this.getRouteQuery();
    //console.log(url);
    //console.log(queryParams);
    Ext.Ajax.request({
	  
      url: url,
      success: received,
      failure: function (response) { 
         this.queriesSent--;
         Carmen.Util.handleAjaxFailure(response, this.windowTitle, true); 
      },
      headers: {
        'my-header': 'foo'
      },
      scope : this,
      params: queryParams
    });
  }, 


  buildParams : function(nodes, position, fieldsFilter, mapfile) {

    var nodesIndex = {};
    var layerNames = [];
    var fieldsList = [];
    var briefFieldsList = [];
    var baseQueryURLList = [];

    for (var i=0; i<nodes.length; i++) {
      if(typeof(nodes[i].attributes.infoFields)!="undefined"){
		    nodesIndex[nodes[i].attributes.layerName] = i;
        layerNames.push(nodes[i].attributes.layerName);
        if(typeof(nodes[i].attributes.briefFields)!="undefined"){ 
          briefFieldsList.push(nodes[i].attributes.briefFields);
        }
  		  fieldsList.push(nodes[i].attributes.infoFields);
  	    baseQueryURLList.push(nodes[i].attributes.baseQueryURL);
		  }
    }

    var fieldsDesc = new Array();
    for (var i=0; i<fieldsList.length; i++) {
      var list = Carmen.Util.chompAndSplit(fieldsList[i],';');
      fieldsDesc[i] = new Array();
      for (var j=0; j<list.length; j++) {
        var desc  = Carmen.Util.decodeCarmenFieldDesc(list[j], baseQueryURLList[i]);
        fieldsDesc[i][j]=desc;
      }
      if (fieldsFilter) 
        fieldsDesc[i] = Carmen.Util.array_filter(fieldsDesc[i], fieldsFilter);
    }

    var briefFieldsDesc = new Array();
    for (var i=0; i<briefFieldsList.length; i++) {
      var list = Carmen.Util.chompAndSplit(briefFieldsList[i],';');
      briefFieldsDesc[i] = new Array();
      for (var j=0; j<list.length; j++) {
        var desc  = Carmen.Util.decodeCarmenFieldDesc(list[j], baseQueryURLList[i]);
        briefFieldsDesc[i][j]=desc;   
      }
      if (fieldsFilter) 
        briefFieldsDesc[i] = Carmen.Util.array_filter(briefFieldsDesc[i], fieldsFilter);
    }

    // linking kept info fields and brief fields 
    // to the corresponding layer node in the tree
    for (var i=0; i<layerNames.length; i++) {
   	  nodes[nodesIndex[layerNames[i]]].attributes.infoFieldsDesc = fieldsDesc[i];
   	  nodes[nodesIndex[layerNames[i]]].attributes.briefFieldsDesc = briefFieldsDesc[i];
    }

    var params = {
      layers : Ext.util.JSON.encode(layerNames),
      fields : Ext.util.JSON.encode(fieldsDesc),
      briefFields : Ext.util.JSON.encode(briefFieldsDesc),
      shape : Carmen.Util.positionToStr(this.positionToBounds(position)),
      map : mapfile,
      showGraphicalSelection : this.showGraphicalSelection }
    return params;
  },

  logvar : function(value, name, tab) {
  	var logv = (value==null) ? "null" : value;
  	tab[name] = logv;
  },

  printlog : function(tab) {

	Ext.Ajax.request({
      url: '/services/GetInformation/log.php',
      method: 'POST',
  	  success : function (response) { 
      	 Ext.Msg.alert(control.windowTitle, response.msg ? response.msg : "Commande log enregistrée");
      },
  	  failure: function (response) { 
      	 Ext.Msg.alert(control.windowTitle, 'Problème dans l\'écriture du fichier Log');
      },
      params : {
        "log_array" : Ext.encode(tab)
      }
    });
  },
	
  performWMSQuery : function(layer, position) {
    var control = this;

    var received = function(response) {

      //var logs = {}; //obedel log
      var features = [];
        //control.logvar(response, 'response',logs); //obedel log
      var contentStr = response.getResponseHeader("Content-Type")
            || response.getResponseHeader("Content-type");
      
      //control.logvar(contentStr, 'contentStr',logs); //obedel log
      if (contentStr!=null) { 
          contentStr = (contentStr.split(";")[0]).trim();
          //control.logvar(contentStr, 'contentStrSplit',logs); //obedel log
          var fmt = control.WMSResponseFormats[contentStr];
          //control.logvar(fmt, 'fmt',logs); //obedel log
          if (fmt) { 
             var rep = response.responseXml || response.responseText;
             //control.logvar(rep, 'rep',logs); //obedel log
             //console.log(Url.decode(rep));
             //console.log(Url._utf8_encode(rep));
             //console.log(Url._utf8_decode(rep));
             //console.log(response.responseXml || response.responseText);
             features = fmt.read(response.responseXml || response.responseText);
             //control.logvar(features, 'features',logs);
             //console.log(features);
           }
        }
      //control.printlog(logs);//obedel log	

      var results = {};
      var extentStr = Carmen.Util.positionToStr(control.map.getExtent());
      for (var i=0; i<features.length; i++) {
    	  //TODO check when same WMS URL is in same group
        //TODO why is it different beetween addOGC and init
    	  var layerName =  (typeof(layer.subLayers[0])!="undefined" ? layer.subLayers[0].name : layer.name);//layer.name;//features[i].type;
        //var layerName = layer.name;//features[i].type;
    	  /*  for(var j=0; j<control.layerTree.childNodes.length; j++){
    		  if(features[i].type == control.layerTree.childNodes[j].attributes.OlLayer.params.LAYERS){
    			  layerName = control.layerTree.childNodes[j].attributes.layerName;
    			  break;
    		  }
    	  }*/
        if (!(features[i].type in results)) {
          results[layerName] = {
            "fields" : [],
            "briefFields" : [],
            "data" : [],
            "briefData" : []
          }
          for (var f in features[i].attributes) {
            results[layerName].fields.push(f);				
            results[layerName].briefFields.push(f);
          }
        }
        var featureData = [];
        for (var f in features[i].data) 
          featureData.push(features[i].data[f]);	
        results[layerName].briefData.push(featureData);
        results[layerName].data.push(featureData);
      }

      var responseJS = {
        "totalCount" : features.length,
        "results" : results
      };
      control.registerResults(responseJS, Carmen.Control.Info.SRC_WMS);
    };
    
    
    this.queriesSent++;
    Ext.Ajax.request({
      url: Routing.generate('frontcarto_query_getinformation_proxy'),
      //PRODIGE40 '/services/GetInformation/proxy.php',
      method: 'GET',
  	  success : function (response) {
      	 received(response);
      },
  	  failure: function (response) {
      	 // uncomment to display wms querying errors
      	 //var msg = Ext.valueFrom(response.responseText, "Service WMS indisponible");
      	 //Ext.Msg.alert(control.windowTitle, msg);
      	control.registerResults(
      	   Carmen.Control.Info.noResultsResponse,
      	   Carmen.Control.Info.SRC_WMS
      	 );
      },
      headers: {
        'my-header': 'foo'
      },
      params : { 
        "proxy_url" : layer.getFeatureInfoURL('', position, this.map.size, this.map.getExtent()),
        "force_utf8_output" : 1 // needed because ExtJS waits for UTF8 encoded response...
      } 
    });
    //console.log(layer.getFeatureInfoURL('', position, this.map.size, this.map.getExtent()));
  },

  buildResultPanels : function(tree, results, queryType, queryfile) {
  	var me = this;
	  	//this.map.app.ui.delAllInfoPanel();

		// obedel migration
		for(lname in results)  {
			var editable = (this.editorManager ? this.editorManager.isLayerEditable(lname) : false);
		 	var node = tree.findChildByAttribute('layerName', lname);
      
		 	if (node==null)
		 		node = tree.findChildByAttribute('text', lname);
		 	if (node==null)
		 		node = tree.findChildByAttribute('layerAlias', lname);
			
      if (node!=null) {
			  
			  var panelTitle = node.data.text;
			  node.attributes.layerAlias= node.text!="" ? node.text : node.attributes.layerAlias;
			  node.text = node.data.text + '  (' + results[lname].data.length + ' résultat'+(results[lname].data.length > 1 ? 's' : '')+')';
			  node.attributes.infoData = results[lname];
			  node.attributes.queryType = queryType;
			  node.attributes.queryfile = results[lname].queryfile;
			  node.attributes.extent = results[lname].extent;
			  
		   
			  // add click listener to the node  	 	   
			  var control = this;

			  var panel_id = this.displayClass +'_' + 'gridInfoPanel_' +  Ext.id();
        var gPanel =  Ext.create( 'Ext.grid.GridPanel', {
          id: panel_id,
          store: new Ext.data.SimpleStore({
            fields: [],
            data: []
          }),
          columns: [],
          title: panelTitle,
          hidden: false,
          autoScroll: true,
          border: true,
          stripeRows: true,
          bounds: null,
          viewConfig: {
            forceFit:true
          },
          tools:[
                 {
                     id: panel_id + '_zoom',
                     qtip: 'Zoom sur les résultats',
                     renderTpl: [
                                 '<i id="{id}-toolEl" data-ref="toolEl" class="fa fa-search fa-1x"' +
                                 '" style="color: #FFF; font-size: 16px;" role="presentation"/></i>'
                                 ],
                     handler: function(){
                     // show help here
                     var bounds = this.toolOwner.bounds;
                     control.map.zoomToSuitableExtent(bounds);
                     }
                 },
                 {
                     id: panel_id + '_xls',
                     qtip: 'Export de la sélection au format Excel (XLS)',
                     renderTpl: [
                                 '<i id="{id}-toolEl" data-ref="toolEl" class="fa fa-file-excel-o fa-1x"' +
                                 '" style="color: #FFF; font-size: 16px;" role="presentation"/></i>'
                                 ]
                 },
                 {
                     id: panel_id + '_csv',
                     qtip: 'Export de la sélection au format CSV',
                     renderTpl: [
                                 '<i id="{id}-toolEl" data-ref="toolEl" class="fa fa-file-text-o fa-1x"' +
                                 '" style="color: #FFF; font-size: 16px;" role="presentation"/></i>'
                                 ]
                 },
                 {
                   // Add new publippstage icon here
                   id: panel_id + '_mail',
                   qtip: 'Utiliser le module de publipostage',
                   hidden: !user_connected_details.PUBLIPOSTAGE,
                   renderTpl: [
                               '<i id="{id}-toolEl" data-ref="toolEl" class="fa fa-envelope-o"' +
                               '" style="color: #FFF; font-size: 16px;" role="presentation"/></i>'
                               ]
                   //handler: function() {
                   //}
                 }
                ], // end tools
          listeners : {
            afterrender:function(){
              var menu = this.headerCt.getMenu();
              menu.addCls('menuBackgroundWhite');
              menu.addListener('afterrender',
                function(menu){
                  var subitems = menu.items.items;
                  for(var i = 0; i < subitems.length; i++){
                    if(typeof subitems[i].menu !== "undefined"){ 
                     subitems[i].menu.addCls('menuBackgroundWhite');
                    }
                  }
                }
              );
            },
			rowclick:function(grid, record, tr, rowIndex, e, eOpt) {

			  var rec = grid.store.getAt(rowIndex);
				//var extentStr = rec.get('extent');
				//var bounds = Carmen.Util.strExtentToOlBounds(extentStr, Carmen.Control.Info.PUNCTUAL_RADIUS);
				//control.map.zoomToSuitableExtent(bounds, minScale, maxScale);
				var fid = rec.get('fid');
				var layerName = rec.get('layerName');
				// retrieving the openLayer layer linked with the selected row
				var n = control.layerTreeRoot.findChildByAttribute('layerName', layerName);
				var olLayer = n.attributes.OlLayer;
				// changing symbology to highlight selection
			  olLayer.setDisplayMode(
			  Carmen.Layer.MapServerGroup.DISPLAY_QUERYMAP, 
			  layerName, fid);
			},
			cellclick:function( grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
				if (cellIndex==1) {
					var rec = grid.getStore().getAt(rowIndex); 
					var extentStr = rec.get('extent');
					var bounds = Carmen.Util.strExtentToOlBounds(extentStr, Carmen.Control.Info.PUNCTUAL_RADIUS);
					control.map.zoomToSuitableExtent(bounds, minScale, maxScale);
				 }
        if (cellIndex==2) {
          var rec = grid.getStore().getAt(rowIndex); 
          var columns = grid.ownerCt.columnManager.columns;
          control.buildInfoWindow(rec, columns, rowIndex);
          
         }
				if (td.getAttribute ('data-columnid')=='__edit__' && me.editorManager) {
          var rec = grid.getStore().getAt(rowIndex); 
					var lname = rec.get('layerName');      
          me.editorManager.editInformation(lname, rec)
				}
			},
			selectionchange: function( grid, selected, eOpts ){
				if (selected.length == 0){
					if (this.node.attributes.queryType==Carmen.Control.Info.SRC_MS) {
						var minScale = this.node.attributes.minScale;
						var maxScale = this.node.attributes.maxScale;
						control.map.clearGraphicalSelection();
						if (control.showGraphicalSelection && this.node.attributes.queryfile!="") {
              this.node.attributes.OlLayer.setDisplayMode(
                  Carmen.Layer.MapServerGroup.DISPLAY_NQUERYMAP,
                  this.node.attributes.layerName, null, 
                  this.node.attributes.queryfile);
            }
					}
					else if (this.node.attributes.queryType==Carmen.Control.Info.SRC_WMS) {
						var infoFieldsDesc = [];
						for (var i=0; i<this.node.attributes.infoData.fields.length; i++) {
							var fld = this.node.attributes.infoData.fields[i];
							infoFieldsDesc.push({
									name : fld,
									alias : fld,
									type : 'TXT'
								});
						}
						this.node.attributes.infoFieldsDesc = infoFieldsDesc;
					}
				}
			}
    },
  scope : this
  }); // end gPanel

        this.map.app.ui.addToInfoPanel(gPanel);

        //control.headerPanel.add(gPanel);
			  //gPanel.addListener('click', 
			  //  function() {
        if (node.attributes.queryType==Carmen.Control.Info.SRC_MS) {
          // zoom on the layer selection extent
          //var extentStr = node.attributes.infoData.extent;
          var minScale = node.attributes.minScale;
          var maxScale = node.attributes.maxScale;
          //console.log(node.attributes.infoData); 
          //var bounds = Carmen.Util.strExtentToOlBounds(extentStr, Carmen.Control.Info.PUNCTUAL_RADIUS);
          
          //obedle migration here
          // obedel patch : no highlight mode for wfs mapfile layer
          //control.map.clearGraphicalSelection();
          if (control.showGraphicalSelection) {
              /*node.attributes.OlLayer.setDisplayMode(
            Carmen.Layer.MapServerGroup.DISPLAY_NQUERYMAP, 
            node.attributes.layerName, null, 
            node.attributes.queryfile);*/
            }
            //control.map.zoomToSuitableExtent(bounds, minScale, maxScale);
        }else if (node.attributes.queryType==Carmen.Control.Info.SRC_WMS) {
            // building default fieldsDesc from fields name
            var infoFieldsDesc = [];
            for (var i=0; i<node.attributes.infoData.fields.length; i++) {
              var fld = node.attributes.infoData.fields[i];
              infoFieldsDesc.push({
                  name : fld,
                  alias : fld,
                  type : 'TXT'
                });
            }
            node.attributes.infoFieldsDesc = infoFieldsDesc; 
        }
        // configuring and showing the result table panel
        var gConfig = control.buildGridConfig(
          editable,
          node.attributes.infoFieldsDesc,
          node.attributes.infoData,
          node.attributes.layerName,
          node.attributes.olLayer,
          node.attributes.queryType
        );
        // configuring xls export...
        var xlsTool = gPanel.getHeader().getComponent(panel_id + '_xls');
        xlsTool.on('click',
          function() {
          var url = Routing.generate('frontcarto_export_xls');
          //PRODIGE40 '/services/GetXLS/index.php';
          var f = this.control.gridInfoFormPanel; 
          var queryParams = {
            field : JSON.stringify(this.node.attributes.infoFieldsDesc),
            data : JSON.stringify(this.node.attributes.infoData.data),
            selectionName : this.node.attributes.layerAlias,
            output : "XLS"
          };
          Carmen.Util.doFormUpload(f, url, queryParams);
          },
          { control : control, node : node }
        );

        var csvTool = gPanel.getHeader().getComponent(panel_id + '_csv');
        csvTool.on('click',
          function() {
          var url = Routing.generate('frontcarto_export_xls');
          //PRODIGE40 '/services/GetXLS/index.php';
          var f = this.control.gridInfoFormPanel; 
          var queryParams = { 
            field : Ext.encode(this.node.attributes.infoFieldsDesc),
            data : Ext.encode(this.node.attributes.infoData.data),
            selectionName : this.node.attributes.layerAlias,
            output : "CSV"
          };
          Carmen.Util.doFormUpload(f, url, queryParams);
          },
          { control : control, node : node }
        );

        var mailTool = gPanel.getHeader().getComponent(panel_id + '_mail');
        mailTool.on('click',
          function() {
            var fieldsDesc = this.node.attributes.infoFieldsDesc;
            var infoData = this.node.attributes.infoData;
            var ISADMIN = user_connected_details.isAdmProdige;
            var oWind = null;
            var windowWidth = 900;
            var labelWidth  = 170;
            var buttonWidth = 100;
            var sLabelWidth = 70;
            var sCtrlWidth  = Math.floor((windowWidth-labelWidth-2*sLabelWidth-6)/2)+sLabelWidth;
            
            // Localy store for the ComboBox (Champ courriel destinataire - Champ nom destinataire) 
            store_mail = new Ext.data.JsonStore({
              fields: ['name', 'alias', 'type', 'url'],
              data: fieldsDesc
            });
            
            // Call web service method
            var url = Routing.generate('frontcarto_export_mailing');
       //PRODIGE40 '/services/GetMailing/index.php';
            var mailingFields = "";
            var defaultMailingFields = "\n{$signature} : signature de l'utilisateur connecté,"+
              "{$messageAuto} : Message de bas de page";
            var glu = "";
            for(var i=0; i<fieldsDesc.length; i++) {
              mailingFields = mailingFields + glu + "{$"+fieldsDesc[i].name+"} : "+fieldsDesc[i].alias;
              glu = ( (i+1)%3==0 ? ",\n" : ", " );
            }
      
            var store_modele = Ext.create('Ext.data.JsonStore', {
              autoDestroy: true,
              fields: ['modele_id', 'modele_name', 'msg_title', 'msg_body'],
              proxy: {
                type: 'ajax',
                actionMethods: 'POST',
                url: url,
                extraParams: {
                  action     : 'getJsonStore_Models',
                  layerTable : infoData.layerTable
                },
                reader: {
                  type: 'json'
                }
              }/*,
              listeners: {
                'load': {
                  fn: function(){
                    Ext.Msg.alert('debug', 'load ...');
                  }
                }
              }*/
            });

            // fidsArray contains all gid for concerned layers
            var fidsArray = [];
            
            //check key for gid 
            var gidKey = infoData.fields.indexOf("fid");
            
            for(var i=0; i<infoData.data.length; i++) {
              fidsArray.push(infoData.data[i][gidKey]);
            }
            // fidsArray to object 
            var fids = {'fids': fidsArray};

            // Form Panel
            var formPanel = Ext.create('Ext.form.Panel', {
              me: this,
              id: 'id_formePanel',
              layout: 'form',
              width: windowWidth-20,
              fieldDefaults : {
                labelAlign : 'right',
                labelWidth: labelWidth
              },
              items : [{
                  xtype: 'displayfield',
                  fieldLabel: 'Couche sélectionnée',
                  value: infoData.layerTable
                }, {
                  xtype: 'fieldcontainer',
                  fieldLabel:'Champ destinataire',
                  layout: 'hbox',
                  defaults: { 
                    labelWidth: sLabelWidth
                  },
                  items: [{
                    id:'cmb_nom_destinataire',
                    xtype: 'combo',
                    fieldLabel:'Nom*',
                    valueField: 'name',
                    displayField:'name',
                    queryMode:'local',
                    allowBlank: false,
                    maxLength: 80,
                    autoSelect: true,
                    editable: false,
                    width: sCtrlWidth,
                    store: store_mail
                  }, {
                    id:'cmb_courriel_destinatatire',
                    xtype: 'combo',
                    fieldLabel:'Courriel*',
                    valueField: 'name',
                    displayField:'name',
                    queryMode:'local',
                    allowBlank: false,
                    autoSelect: true,
                    editable: false,
                    width: sCtrlWidth,
                    store: store_mail
                  }]
                }, {
                  xtype: 'fieldcontainer',
                  fieldLabel:'Expéditeur',
                  layout: 'hbox',
                  defaults: { 
                    labelWidth: sLabelWidth
                  },
                  items: [{
                    id:'txt_nom_expediteur',
                    xtype: 'textfield',
                    name: 'nom_expediteur',
                    fieldLabel:'Nom*',
                    allowBlank: false,
                    maxLength: 80,
                    width: sCtrlWidth,
                    value: user_connected_details.userPrenom+" "+user_connected_details.userNom
                  }, {
                    id:'txt_courriel_expediteur',
                    xtype: 'textfield',
                    name: 'courriel_expediteur',
                    fieldLabel:'Courriel*',
                    allowBlank: false,
                    maxLength: 80,
                    width: sCtrlWidth,
                    width: sCtrlWidth,
                    vtype: 'email',
                    value: user_connected_details.userEmail 
                  }]
                }, {
                  xtype: 'fieldcontainer',
                  fieldLabel:'Modèle enregistré',
                  layout: 'hbox',
                  items: [{
                      id:'cmb_modele',
                      xtype: 'combo',
                      valueField: 'modele_id',
                      displayField: 'modele_name',
                      autoSelect: true,
                      editable: false,
                      width: windowWidth-labelWidth-buttonWidth-10,
                      store: store_modele,
                      listeners: {
                        'select' : {
                          fn: function(combo, records, eOpts) {
                            Ext.getCmp('txt_objet').setValue(records[0].get('msg_title'));
                            Ext.getCmp('htmlEditor_message').setValue(records[0].get('msg_body'));
                          }
                        }
                      }
                    }, {
                      id: 'btn_supprimer',
                      xtype: 'button',
                      margin: '0 0 0 4',
                      width: buttonWidth,
                      text: 'Supprimer',
                      hidden: ( ISADMIN ? false : true ), // Show checkBox if the user is an admin
                      handler: function(){
                        var modelId = Ext.getCmp('cmb_modele').getValue();
                        if( modelId!="" && modelId!="-1" ) {
                          Ext.Msg.confirm("Confirmation", "Veuillez confirmer la suppression du modèle sélectionné ?", function(btn){
                            if( btn=="yes" ) {
                              Ext.Ajax.request({
                                url: url,
                                method: 'POST',
                                params: {
                                  action: 'supprimer',
                                  modeleId:  modelId
                                },
                                success: function(response){
                                  var oRes = eval("("+response.responseText+")");
                                  if( oRes.success ) {
                                    Ext.getCmp('cmb_modele').getStore().load();
                                    Ext.getCmp('cmb_modele').setValue('');
                                    Ext.getCmp('txt_objet').setValue('');
                                    Ext.getCmp('htmlEditor_message').setValue('');
                                  }
                                  Ext.Msg.alert((oRes.success ? 'Notification' : 'Attention'), oRes.msg); 
                                },
                                failure: function(response) {
                                  Ext.Msg.alert('Erreur', 'Impossible de supprimer le modèle sélectionné.');
                                }
                              });
                            }
                          }).icon = Ext.Msg.WARNING;
                        } else {
                          Ext.Msg.alert('Attention', 'Vous devez sélectionner un modèle au préalable.');
                        }
                      }
                    }]
                }, {
                  id:'txt_objet',
                  xtype: 'textfield',
                  name: 'objet',
                  fieldLabel:'Objet*',
                  allowBlank: false,
                  maxLength: 80
                }, {
                  id:'htmlEditor_message',
                  xtype: 'htmleditor',
                  fieldLabel: 'Message*',
                  allowBlank: false,
                  width: windowWidth,
                  height: 280,
                  enableAlignments: true,
                  enableColors: true,
                  enableFont: true,
                  enableFontSize: true,
                  enableFormat: true,
                  enableLinks: true,
                  enableLists: true,
                  enableSourceEdit: true
                }, {
                  xtype: 'textareafield',
                  //xtype: 'displayfield',
                  id: 'mailingFields',
                  fieldLabel: 'Champs de publipostage<br>disponibles',
                  readOnly: true,
                  scrollable: 'y',
                  style: {
                    height: '48px'
                  },
                  value: mailingFields+defaultMailingFields
                }, {
                  id:'chk_enregistre_modele',
                  xtype: 'checkboxfield',
                  fieldLabel:'Enregistré comme modèle',
                  name: 'checkBox',
                  //inputValue:'1'
                  hidden: (ISADMIN)?false:true, // Show checkBox if the user is an admin
                  listeners: {
                    change: function (checkbox, newVal, oldVal) {
                      var ctrlModelName = Ext.getCmp("txt_name_modele");
                      if( !ctrlModelName ) return;
                      if(newVal == '1' && oldVal == '0') {
                        ctrlModelName.setVisible(true);
                        ctrlModelName.allowBlank = false;
                        ctrlModelName.validate();
                      } else {
                        ctrlModelName.setVisible(false);
                        ctrlModelName.setValue('');
                        ctrlModelName.allowBlank = true;
                        ctrlModelName.validate();
                      }
                    }
                  }
                }, {
                  id:'txt_name_modele',
                  name: 'name_modele',
                  fieldLabel:'Nom du modèle',
                  xtype: 'textfield',
                  maxLength: 80,
                  hidden: true
                }],
              buttons: [{
                  id: 'btn_tester',
                  xtype: 'button',
                  text: 'Tester',
                  width: buttonWidth,
                  margin: '0 4 0 0',
                  handler: function() {
                    var form = Ext.getCmp('id_formePanel');
                    if ( form.isValid() ) {
                      Ext.MessageBox.wait({ 
                        msg: "Veuillez patienter...", 
                        width:300, 
                        wait:true, 
                        waitConfig: {interval:200}
                      });
                      Ext.Ajax.request({
                        url: url,
                        method: 'POST',
                        params: {
                          action                   : 'tester',
                          champCourrielDestinataire: Ext.getCmp('cmb_courriel_destinatatire').getValue(),
                          champNomDestinataire     : Ext.getCmp('cmb_nom_destinataire').getValue(),
                          nomExpediteur            : Ext.getCmp('txt_nom_expediteur').getValue(),
                          courrielExpediteur       : Ext.getCmp('txt_courriel_expediteur').getValue(),
                          objectMessage            : Ext.getCmp('txt_objet').getValue(),
                          bodyMessage              : Ext.getCmp('htmlEditor_message').getValue(),
                          nomModele                : '',
                          userGiven                : user_connected_details.userPrenom,
                          userName                 : user_connected_details.userNom,
                          userLogin                : user_connected_details.userLogin,
                          userSignature            : user_connected_details.userSignature,
                          layerTable               : infoData.layerTable,
                          fidsList                 : fids
                        },
                        success: function(response){
                          Ext.MessageBox.hide();
                          var oRes = eval("("+response.responseText+")");
                          Ext.Msg.alert((oRes.success ? 'Notification' : 'Attention'), oRes.msg);
                        },
                        failure: function(response) {
                          Ext.MessageBox.hide();
                          Ext.Msg.alert('Erreur', 'L\'envoi du message test a échoué.');
                        }
                      });
                    } else {
                      Ext.Msg.alert('Attention', 'Vous devez renseigner tous les champs obligatoires<br>et fournir une addresse courriel valide pour l\'expéditeur.');
                    }
                  }
                }, {
                  id: 'btn_envoyer',
                  xtype: 'button',
                  text: 'Envoyer',
                  width: buttonWidth,
                  margin: '0 4 0 0',
                  handler: function() {
                    var form = Ext.getCmp('id_formePanel');
                    if ( form.isValid() ) {
                      Ext.MessageBox.wait({ 
                        msg: "Veuillez patienter...", 
                        width:300, 
                        wait:true, 
                        waitConfig: {interval:200}
                      });
                      Ext.Ajax.request({
                        url: url,
                        method: 'POST',
                        params: {
                          action                   : 'envoyer',
                          champCourrielDestinataire: Ext.getCmp('cmb_courriel_destinatatire').getValue(),
                          champNomDestinataire     : Ext.getCmp('cmb_nom_destinataire').getValue(),
                          nomExpediteur            : Ext.getCmp('txt_nom_expediteur').getValue(),
                          courrielExpediteur       : Ext.getCmp('txt_courriel_expediteur').getValue(),
                          objectMessage            : Ext.getCmp('txt_objet').getValue(),
                          bodyMessage              : Ext.getCmp('htmlEditor_message').getValue(),
                          nomModele                : Ext.getCmp('txt_name_modele').getValue(),
                          userGiven                : user_connected_details.userPrenom,
                          userName                 : user_connected_details.userNom,
                          userLogin                : user_connected_details.userLogin,
                          userSignature            : user_connected_details.userSignature,
                          layerTable               : infoData.layerTable,
                          fidsList                 : fids
                        },
                        success: function(response){
                          Ext.MessageBox.hide();
                          var oRes = eval("("+response.responseText+")");
                          Ext.Msg.alert((oRes.success ? 'Notification' : 'Attention'), oRes.msg, function() {
                            if( oRes.success ) {
                              oWind.close();
                            }
                          });
                        },
                        failure: function(response) {
                          Ext.MessageBox.hide();
                          Ext.Msg.alert('Erreur', 'L\'envoi du message a échoué.');
                        }
                      });
                    } else {
                      Ext.Msg.alert('Attention', 'Vous devez renseigner tous les champs obligatoires<br>et fournir une addresse courriel valide pour l\'expéditeur.');
                    }
                  }
                }, {
                  id: 'btn_fermer',
                  xtype: 'button',
                  text: 'Fermer',
                  width: buttonWidth,
                  margin: '0 4 0 0',
                  handler: function() {
                    oWind.close();
                  }
                }]
            });

          // Window (contains form panel ...)
          oWind = Ext.create('Ext.window.Window', {
            title: 'Publipostage',
            width: windowWidth,
            minWidth: windowWidth,
            minHeight: 550,
            layout: 'fit',
            items: formPanel // formPanel
          });

          oWind.show();

        },
        { control : control, node : node });

        gPanel.reconfigure(gConfig.store, gConfig.columns);

        if(node.attributes.extent !== undefined){
          var bounds = Carmen.Util.strExtentToOlBounds(node.attributes.extent, Carmen.Control.Info.PUNCTUAL_RADIUS);
          gPanel.bounds = bounds;
        }
        gPanel.setTitle(node.text);
        gPanel.node = node;
        gPanel.queryfile = queryfile;

        if (!gPanel.isVisible()) {
          gPanel.show();
         //control.win.setSize(control.win.getSize().width, control.win.getSize().height+1);
        }
 
        if (!Ext.getCmp(this.displayClass +'_' + 'gridInfoFormPanel')) {
          this.gridInfoFormPanel = Ext.create('Ext.form.Panel', {
            title: "upload",
            id: this.displayClass +'_' + 'gridInfoFormPanel',
            hidden : true,
            fileUpload : true,
            waitTitle : "Traitement en cours..."
          });
          this.map.app.ui.infoPanel.add(this.gridInfoFormPanel);
        }
        setTimeout(function(){ waitMessage.hide()}, 250);

     } // end if (node!=null)

		} // end for obedel migration 

  }, // end buildResultPanels function

  buildResNode : function(fields, data, nodeIndex, queryType, briefFieldDesc) {
  	// building desc line
  	var html_title = "";
  	var fieldDesc = briefFieldDesc[0];
  	var f_render = Carmen.Util.carmenTypeToExtRenderer(fieldDesc.type);
  	var first = data.length>2 ? data[0] : data[data.length-1];
  	html_title += (f_render== '') ? '<span>'  + first  + '</span>': f_render(first);
  	if (briefFieldDesc.length>1) {
  		html_title += '<span>(</span>'  ;
  		for (var i=1; i<briefFieldDesc.length; i++) {
  			fieldDesc = briefFieldDesc[i];
  			f_render = Carmen.Util.carmenTypeToExtRenderer(fieldDesc.type);
  			html_title += '<span>' + fieldDesc.alias + ' :</span>'  ;
  			html_title += (f_render== '') ? '<span>' + data[i] + '</span>' : f_render(data[i]);
  			html_title += '<span>,</span>'  ;
  		}
  		html_title = briefFieldDesc.length<=1 ?
  			html_title :
  			html_title.substring(0, html_title.length - '<span>,</span>'.length);
  		html_title += '<span>)</span>'  ;
  	}
  	var control = this;
  },

  extentRenderer : function(v) {
  	return '<i class="fa fa-fw fa-search fa-1x"' +
              '" style="color: #000; font-size: 16px;"></i>';
  },

  infoRenderer : function(v) {
    return '<i class="fa fa-fw fa-info-circle fa-1x"' +
              '" style="color: #000; font-size: 16px;"></i>';
  },

  editableRenderer : function(v) {
  	return (v ? '<i class="fa fa-fw fa-pencil fa-1x"' +
              '" style="color: #000; font-size: 16px;"></i>' : "");
  },

  buildGridConfig: function (editable, fieldsDesc, result, layerName, olLayer, queryType) {
    var fields = new Array();
    var columns = new Array();
    columns.push(new Ext.grid.RowNumberer());
    if (queryType==Carmen.Control.Info.SRC_MS) {
	    columns.push({
	      //id : 'extent',
	      header : this.extentRenderer(true).replace("<i", "<i title='Zoomer sur'"),
	      width : 40,
	      resizable: true,
	      renderer: this.extentRenderer,
	      dataIndex: 'extent'
	    });
	    columns.push({
	      //id : 'info',
	      header : this.infoRenderer(true).replace("<i", "<i title='Informations'"),
	      width : 40,
	      resizable: true,
	      renderer: this.infoRenderer,
	      dataIndex: 'info'
	    });
		}
		if ( editable ){
      columns.push({
        id : '__edit__',
        header : this.editableRenderer(true).replace("<i", "<i title='Donnée éditable'"),
        width : 40,
        resizable: true,
        renderer: this.editableRenderer,
        dataIndex: 'editable'
      });
		}
		if(typeof(fieldsDesc)!="undefined"){	
      for (var i=0; i<fieldsDesc.length; i++) {
      	var desc = fieldsDesc[i];
      	var fDesc = {name : desc.name + '_' + i.toString(), type : Carmen.Util.carmenTypeToExtType(desc.type)};
      	switch (desc.type) {
          case 'date' : 
      	    fDesc['dateFormat'] = Carmen.Util.carmenDateTypeToExtDateFormat(desc.type);
      	    break;
      	  default:
      	    break;
      	}
      	fields.push(fDesc);
  
      	var colDesc = {
      		id: Ext.id(), //desc.name + '_' + layerName + '_' + i.toString()),
      		header: desc.alias,
      		width: 200,
      		sortable: true,
      		resizable: true,
      		renderer: Carmen.Util.carmenTypeToExtRenderer(desc.type),
      		dataIndex: desc.name + '_' + i.toString()
      	};
      	columns.push(colDesc);
      }
		}
		if (queryType==Carmen.Control.Info.SRC_MS) {
	    fields.push({name : 'extent'});
      fields.push({name : 'fid'});
	    fields.push({name : 'editable'});
		}
    fields.push({name : 'layerName'});
    
    result.fields = [];
    for (var i=0; i<fields.length; i++) {
      result.fields.push(fields[i].name);
    }
    
    for (var i=0; i<result.data.length; i++) {
    	result.data[i] = result.data[i].concat([layerName]);
    }

    var config = {
	  	store : new Ext.data.SimpleStore({
	        fields: fields,
	        data : result.data
	    }),

	    /*columnModel : new Ext.grid.ColumnModel({
	      columns: columns 
	    })*/
      columns : columns
	  };
	  return config;
  },

    queryStatsOnColumn : function(stats, layerAlias, colAlias) {
	var labelMapping = {
		"count" : "Effectif",
		"min" : "Minimum",
		"max" : "Maximum",
		"sum" : "Somme",
		"variance" : "Variance",
		"average" : "Moyenne",
		"stddeviation" : "Ecart type"
	};
	var statWin = new Ext.Window({
		id : this.displayClass +'_' +'StatsWindow' + Ext.id(),
		layout:'anchor',
		width:340,
		height: 232,
    constrain : true,
		plain: true,
		title: 'Statistiques pour le champ ' + colAlias + ' de la couche ' + layerAlias,
		modal:false,
		autoDestroy :true,
		resizable:false,
		autoScroll: true,
		closeAction: 'hide',
		shadow : false
	  });
	  var infoPanel = new Ext.form.FormPanel({
		id: this.displayClass +'_' +'StatsInfoPanel' + Ext.id(),
		autoScroll: true,
		fitToFrame: true,
		defaults : {
			labelStyle: 'padding:5px;',
			style: 'font-weight:bold; padding:5px;'
		}
		//labelWidth: 80,
		//labelPad: 3
	  });
	  for (var field in stats) {
		if (labelMapping[field]) {
			var infoField = new Ext.form.DisplayField({
				fieldLabel : labelMapping[field],
				value : stats[field]
			});
			infoPanel.add(infoField);
		}
	  }
	statWin.add(infoPanel);
	statWin.show();
  },


  // Filter and transform function used on tree
  filterNodeLayerQueryable :  function(node) {
  	var res = true;
    //console.log(node.attributes.text);
  	res = (node.attributes!=null) && 
      ('type' in node.attributes) && 
      ((node.attributes.type == Carmen.Control.LayerTreeManager.NODE_GROUP && node.hasChildNodes() && node.getDepth()>0) ||
       (node.attributes.type == Carmen.Control.LayerTreeManager.NODE_LAYER && node.attributes.OlLayer.getVisibility(node.attributes.layerName)
       && ((node.attributes.infoFields && node.attributes.infoFields.length>0) ||
       	   (node.attributes.OlLayer instanceof Carmen.Layer.WMSGroup)) 
      )); 	
    return res;
  },

  filterNodeLayerWithResults : function(node) {
  	var res = true;
    res = ('type' in node.attributes) && 
      ((node.attributes.type == Carmen.Control.LayerTreeManager.NODE_GROUP && node.hasChildNodes() && node.getDepth()>0) ||
       (node.attributes.type == Carmen.Control.LayerTreeManager.NODE_LAYER && node.attributes.infoData!=null) ||
       (node.attributes.type == Carmen.Control.LayerTreeManager.NODE_FEATURE));
    return res;
  },

  transformNodeInfo : function(atts) {
  	var cloneAtts = Carmen.Util.clone(atts);
/*
  	if (cloneAtts.text!=Carmen.Control.LayerTreeManager.TREE_NODE_ROOT_LABEL)
  	   cloneAtts.uiProvider = Carmen.TreeNodeInfoUI;
  	else
  	   cloneAtts.uiProvider = Carmen.RootTreeNodeInfoUI;  	   
*/
  	cloneAtts.cls = 'none';
    cloneAtts.infoData = null;  	
  	cloneAtts.listeners = {};
  	cloneAtts.expandable = true;
  	if (cloneAtts.type == Carmen.Control.LayerTreeManager.NODE_LAYER) {
  		cloneAtts.icon = '/IHM/images/NoTreeIcon.gif';
  		cloneAtts.iconCls = 'cmnInfoGridZoomIcon';
  	}
    cloneAtts.text = atts.layerAlias!=null ? atts.layerAlias : atts.text;
  	return cloneAtts;
  },
  
  fieldFilter_URL : function(field) {
    return field.type=='URL';
  },
  
  fieldFilter_TXT : function(field) {
    return field.type=='TXT';
  },
  
  positionToBounds : function(position) {
		var bounds = null;
  	if (position instanceof OpenLayers.Bounds) {
      var minXY = this.map.getLonLatFromPixel(
        new OpenLayers.Pixel(position.left, position.bottom));
      var maxXY = this.map.getLonLatFromPixel(
        new OpenLayers.Pixel(position.right, position.top));
      bounds = new OpenLayers.Bounds(minXY.lon, minXY.lat,
          maxXY.lon, maxXY.lat);
    } 
    else { // it's a pixel
      // if no tolerance
      // this.showInfoWindow(this.map.getLonLatFromPixel(position));
      // with tolerance
      var tol = Carmen.Util.INFO_TOOL_XY_TOLERANCE;
      var minXY = this.map.getLonLatFromPixel(
        new OpenLayers.Pixel(position.x-tol, position.y-tol));
      var maxXY = this.map.getLonLatFromPixel(
        new OpenLayers.Pixel(position.x+tol, position.y+tol));
      bounds = new OpenLayers.Bounds(minXY.lon, minXY.lat,
          maxXY.lon, maxXY.lat);
    }
	return bounds;
  },
  
  buildInfoWindow : function(rec, columns, rowIndex){
	  var editable = rec.get('editable');
	  var data = [];
		for(var i=4;i<columns.length;i++){
		  var value = {};
		  value['key'] = columns[i].text;
		  value['value'] = columns[i].renderer.apply(null,[rec.get(columns[i].dataIndex)]);
		  data.push(value);
		}
		
		Ext.create('Ext.data.Store', {
			storeId:'infoStore',
			fields:['key', 'value'],
			data:{'items':
				data
			},
			proxy: {
				type: 'memory',
				reader: {
					type: 'json',
					rootProperty: 'items'
				}
			}
		});
		var gridInfoPanel = Ext.create('Ext.grid.Panel', {
			tbar : (editable && this.editorManager ? [
			{
         //hidden : true,
         text: '<i id="'+rec.get('fid')+'-toolEl" data-ref="toolEl" class="fa fa-pencil fa-1x"' +
           '" style="font-size: 16px;" role="presentation"/></i>&nbsp;Editer la donnée',
         scope : this,
         handler: function(){
            this.win.hide();
            var lname = rec.get('layerName');      
            this.editorManager.editInformation(lname, rec)
         }
      }] : undefined),
			store: Ext.data.StoreManager.lookup('infoStore'),
			columns: [
				{ text: 'Libellé',  dataIndex: 'key', flex: 1},
				{ text: 'Donnée', dataIndex: 'value', flex: 1}
			],
			height: '100%',
			width: 400
		});

		this.win.setTitle(this.windowTitle);
		this.win.removeAll();
		this.win.add(gridInfoPanel);
		this.win.show();
  },
  CLASS_NAME: "Carmen.Control.Info"

});


// Constant used to distinguish request type
Carmen.Control.Info.SRC_MS = 0;
Carmen.Control.Info.SRC_WMS = 1;

Carmen.Control.Info.PUNCTUAL_RADIUS = 100.0;

Carmen.Control.Info.noResultsResponse = {
 "totalCount" : 0,
	"results" : []
};