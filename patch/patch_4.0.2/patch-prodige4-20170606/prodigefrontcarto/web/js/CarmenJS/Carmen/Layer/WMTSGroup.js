
/**
 * @_requires OpenLayers/Layer/Mapserver.js
 */

/**
 * Class: Carmen.Layer.MapServer
 * Instances of Carmen.Layer.MapServer are used to display
 * data from a MapServer CGI instance and allow handle several 
 * mapserver layer in the same OpenLayers layer.
 *
 * Inherits from:
 *  - <OpenLayers.Layer.Mapserver>
 */

 
Carmen.Layer.WMTSGroup = new OpenLayers.Class(OpenLayers.Layer.WMTS, {

  subLayers : [],  
  mainLayers : '',
  
  wmtsName : '',
    
  initialize: function(options) {
    OpenLayers.Layer.WMTS.prototype.initialize.apply(this, arguments);
    
    this.ratio = 1.0;
    
    this.mainLayers = this.options.layer;
    this.subLayers = [];
    if (!this.wmtsName && this.mainLayers && this.mainLayers.length>0)
    	this.wmtsName= this.mainLayers;
	if (this.mainLayers.length==0 && this.wmtsName) 
		this.mainLayers = this.wmtsName;
  },
  
  
  hasSubLayers : function() {
  	return this.subLayers.length>0;
  },
  
  addSubLayer : function(layerName, visible, wmtsName) {
		var info = this.getSubLayerInfo(layerName); 
  	if (info == null) {
  		this.subLayers.push({ name: layerName, visible: visible, wmsName: wmtsName, index: this.subLayers.length});
  	}
  	else { 
  		info.visible = visible;
  		info.wmtsName = wmtsName;
  	}
  	
  	var layerVisibility = false;
    for (var i=0; i<this.subLayers.length; i++) {
      layerVisibility = layerVisibility || this.subLayers[i].visible;  
    } 
    this.visibility = layerVisibility;
  },
	
	moveSubLayer : function(layerName, index) {
		var info = this.getSubLayerInfo(layerName);
		//console.log("moving SubLayer " + layerName + " from " + info.index + " to  " + index);
		if (info != null) {
			// removing sublayer
			var toBeUpdated = this.subLayers.splice(info.index + 1);
			//console.log(toBeUpdated);
			this.subLayers.pop();
			//console.log(this.subLayers);
			for (var i=0; i<toBeUpdated.length; i++)
				toBeUpdated[i].index = toBeUpdated[i].index -1;
			this.subLayers = this.subLayers.concat(toBeUpdated);			
			
			// inserting subLayer at the rightPlace
			var toBeUpdated = this.subLayers.splice(index);
			info.index = index;
			this.subLayers.push(info);
			for (var i=0; i<toBeUpdated.length; i++)
				toBeUpdated[i].index = toBeUpdated[i].index + 1;
			this.subLayers = this.subLayers.concat(toBeUpdated);

		}
	},
  
  		/**
     * Method: clone
     * Create a clone of this layer
     *
     * Returns:
     * {<OpenLayers.Layer.WMSGroup>} An exact clone of this layer
     */
    clone: function (obj) {
      if (obj == null) {
          obj = new Carmen.Layer.WMTSGroup(this.name,
                                         this.url,
                                         this.params,
                                         this.options);
      }
      //get all additions from superclasses
      obj = OpenLayers.Layer.Grid.prototype.clone.apply(this, [obj]);

      // copy/set any non-init, non-simple values here
      obj.setVisibility(this.visibility);
      obj.setIsBaseLayer(this.isBaseLayer);
      
      obj.subLayers = [].concat(this.subLayers);
      obj.mainLayers = this.mainLayers;
      obj.displayMode = this.DISPLAY_MAP;
			obj.qstring = '';
			obj.qitem = ''; 

      return obj;
	},
  
	/**
	 * @brief split the current layer in two layers.
	 * 	The first layer is composed of the [0.. index] sub layers 
	 * 	of the current layer, the second layer of the 
	 * 	[index+1.. subLayersCount-1] sub layers...   
	 * @param index : the index of the sublayer to split the  
	 * @return an array composed of the two splitted layers, the 
	 * first layer returned is a reference to current layer changed 
	 * in place, the second is a new layer 
	 * */

	split : function(index) {
		var res = [];
		if (index> this.subLayers.length) {
			res.push(this);
			res.push(null);
		}
		else if (index<0) {
			this.subLayers = [];
			newLayer = this.clone();
			res.push(this);
			res.push(newLayer);
		} 
		else {
			var newLayer = this.clone();
			newLayer.subLayers = new Array();
			var subLayers2 = new Array();
			for (var i=0; i<this.subLayers.length; i++) {
				if (i<=index) 
					subLayers2.push(this.subLayers[i]);
				else
					newLayer.subLayers.push(this.subLayers[i]);
			}
			delete this.subLayers;
			this.subLayers = subLayers2;


			//console.log(subLayers2);
			//console.log(newLayer.subLayers);
			
			// updating sub layer index
			for (var i=0; i<this.subLayers.length; i++) {
				this.subLayers[i].index = i;
			}
			for (var i=0; i<newLayer.subLayers.length; i++) {
				newLayer.subLayers[i].index = i;
			}
			
			res.push(this);
			res.push(newLayer); 
		}
		return res;
	},
	
  
  getSubLayerInfo : function(layerName) {
  	var i=0;
  	while (i<this.subLayers.length && this.subLayers[i].name!=layerName) {
  		i++;
  	}
  	return (i<this.subLayers.length) ? this.subLayers[i] : null;
  },

  setSubLayerVisibility: function(layerName, isVisible) {
    //console.log('setSubLayerVisibility  ' + layerName + ' ' + isVisible.toString());
    // updating sublayer visibility
  	var subLayer = this.getSubLayerInfo(layerName);
  	
  	if (subLayer!=null)
      subLayer.visible = isVisible;
  	//updating layer visibility
  	
  	var layerVisibility = false;
  	for (var i=0; i<this.subLayers.length; i++) {
  		layerVisibility = layerVisibility || this.subLayers[i].visible;  
  	} 
  	
  	this.setVisibility(layerVisibility);
  	this.display(layerVisibility);
    this.redraw();
    if (this.map != null) {
      this.map.events.triggerEvent("changelayer", {
          layer: this,
          property: "visibility"
        });
    }
    this.events.triggerEvent("visibilitychanged");
  },
	  

  getVisibility: function(layerName) {
		var vis = true;
		if (layerName==null || layerName==this.mainLayers) 
			vis = OpenLayers.Layer.WMTS.prototype.getVisibility.apply(this,[]);
		else
			vis = this.getSubLayerVisibility(layerName);
		return vis;
  },

  getSubLayerVisibility: function(layerName) {
    var subLayer = this.getSubLayerInfo(layerName);
    return subLayer.visible;
  },

 _getLayersParam : function() {
    var str = this.mainLayers;
    for (var i=0; i<this.subLayers.length; i++) {
      if (this.subLayers[i].visible) {
        str = str + "," + this.subLayers[i].wmsName; 
      }
    }
    str = (str.length > 0) && (str.charAt(0)==',') ? str.slice(1) : str;
    
    return str;
  },

  
  redraw: function() {
    var redrawn = false;
    this.params.LAYERS = this._getLayersParam();
    redrawn = OpenLayers.Layer.WMTS.prototype.redraw.apply(this, arguments);
    this.updateMatrixProperties();
    return redrawn;
  },
  
  // hack to not limit tiles to max Extent
  updateMatrixProperties: function() {
    var res = OpenLayers.Layer.WMTS.prototype.updateMatrixProperties.apply(this, arguments);
    this.tileFullExtent = null;
    return res;
  },
  
/*  
  // overides of WMS getURL to take layer projection into account
  getURL: function (bounds) {
    bounds = this.adjustBounds(bounds);
    var imageSize = this.getImageSize(); 
    var newParams = {
        'BBOX': this.encodeBBOX ?  bounds.toBBOX() : bounds.toArray(),
        'WIDTH': imageSize.w,
        'HEIGHT': imageSize.h
    };
    if (this.projection)
      newParams.SRS = this.projection;
    var requestString = this.getFullRequestString(newParams);
    //console.log(requestString);
    return requestString;
  },


  // Warning : here the layerName is the wmsName and not the layerName
  // Todo: make layerName be the unique layer id and retrieve wmsName from this id 	
  getLegendGraphicURL : function(layerName) {
  	if (layerName == null || layerName.length==0) {
  		layerName = this.params.LAYERS.split(' ')[0];
  	}
  	if (layerName.length==0) {
  		Ext.msg.alert('Problème dans la récupération du caisson de légende de couches WMS.');
  		return null;
  	}
  	
  	var getMapParams = Carmen.Util.clone(this.params);
    //console.log(this.params);  	
  	delete(this.params['LAYERS']);
  	this.params.LAYER = layerName;
  	this.params.REQUEST = 'GetLegendGraphic';
  	this.params.EXCEPTIONS = 'application/vnd.ogc.se_xml';  	
  	var url = OpenLayers.Layer.HTTPRequest.prototype.getFullRequestString.apply(this, []);    
  	this.params = getMapParams;

  	return url;
  },

  // Warning : here the layerName is the wmsName and not the layerName
  // Todo: make layerName be the unique layer id and retrieve wmsName from this id 	
  getLegendGraphicInfo : function(layerName) {
  	if (layerName == null || layerName.length==0) {
  		layerName = this.params.LAYERS.split(' ')[0];
  	}
  	if (layerName.length==0) {
  		Ext.msg.alert('Problème dans la récupération du caisson de légende de couches WMS.');
  		return null;
  	}

  	var getMapParams = Carmen.Util.clone(this.params);
  	delete(this.params['LAYERS']);
  	this.params.LAYER = layerName;
  	this.params.REQUEST = 'GetLegendGraphic';
  	this.params.EXCEPTIONS = 'application/vnd.ogc.se_xml';
  	var url = OpenLayers.Layer.HTTPRequest.prototype.getFullRequestString.apply(this, []);    
  	this.params = getMapParams;
  	return url;
  },
*/

/*  // Warning : here the layerName is the wmsName and not the layerName
  // Todo: make layerName be the unique layer id and retrieve wmsName from this id 	
  getFeatureInfoURL : function(layerName,position, imgSize, mapExtent) {
  	if (layerName == null || layerName.length==0) {
  		layerName = this.params.LAYERS.split(' ')[0];
  	}
  	if (layerName.length==0) {
  		Ext.msg.alert('Wrong parameters in GetFeatureInfoURL.');
  		return null;
  	}
  	
  	var getMapParams = Carmen.Util.clone(this.params);
  	mapExtent = this.adjustBounds(mapExtent);
    var newParams = {
    	'BBOX' : this.encodeBBOX ?  mapExtent.toBBOX() : mapExtent.toArray(),
  		'LAYERS' : layerName,
	  	'QUERY_LAYERS' : layerName,
	  	'REQUEST' : 'GetFeatureInfo',
	  	'FEATURE_COUNT' : 1,
	  	'X' : position.x,
	  	'Y' : position.y,
	  	'HEIGHT' : imgSize.h,
	  	'WIDTH' : imgSize.w,
	  	// TODO : may need to look for supported info format in layer def
	  	'INFO_FORMAT' : 'application/vnd.ogc.gml' // default format    	
    };
  	var url = OpenLayers.Layer.HTTPRequest.prototype.getFullRequestString.apply(this, [newParams]);
  	return url;
  },
*/


/*  // Warning : here the layerName is the wmsName and not the layerName
  // Todo: make layerName be the unique layer id and retrieve wmsName from this id 	
  getFeatureInfoParams : function(layerName, position, imgSize, mapExtent) {
  	if (layerName == null || layerName.length==0) {
  		layerName = this.params.LAYERS.split(' ')[0];
  	}
  	if (layerName.length==0) {
  		Ext.msg.alert('Wrong parameters in GetFeatureInfoURL.');
  		return null;
  	}
  	mapExtent = this.adjustBounds(mapExtent);

    var params = OpenLayers.Util.extend({}, this.params);
  	params = OpenLayers.Util.extend(this.params, 
  	{
      	'BBOX' : mapExtent.toBBOX(), //this.encodeBBOX ?  , : mapExtent.toArray(),
		'LAYERS' : layerName,
  	  	'QUERY_LAYERS' : layerName,
  	  	'REQUEST' : 'GetFeatureInfo',
  	  	'FEATURE_COUNT' : 1,
  	  	'X' : position.x,
  	  	'Y' : position.y,
  	  	'HEIGHT' : imgSize.h,
  	  	'WIDTH' : imgSize.w,
  	  	// TODO : may need to look for supported info format in layer def
  	  	'INFO_FORMAT' : 'application/vnd.ogc.gml' // default format    	
      });
	return params;
  },

*/  //////////////////////////////////
  // Selection handling part
  //////////////////////////////////
  handleSelection : function(lname) {
    return true;   
  },
  
  layerNotRespondingHandler : function() {
	//console.log(this);
	if (this.idImg)
		var domImg = document.getElementById(this.idImg);
		if (domImg)
			document.body.removeChild(domImg);
	
	Ext.MessageBox.alert("Couche WMTS", "Les couches " + this.control._getLayersParam() + " ne semblent pas joignables. Elles ne seront sans doute ni visibles, ni interrogeables.");
  },
  
  checkLayerWellResponding : function(map, failure) {
	failure = failure==null ? 
		//function () {alert("pb dans le chargement de l'image");} :
		this.layerNotRespondingHandler : 
		failure;
	var oldMap = this.map;
	this.map=map;
	this.imageSize = map.getCurrentSize();
	this.params.EXCEPTIONS = 'application/vnd.ogc.se_xml';

	
	var testImg = document.createElement('img');
	testImg.src = this.getURL(map.getMaxExtent()) + "&test" ;
	testImg.style.display='none';
	testImg.id = "testImg";
	var scope = {idImg : "testImg", control : this};
	testImg.onerror = function() { failure.apply(scope); }; 
	document.body.appendChild(testImg);
	
	
	this.imageSize = null;
	this.map = oldMap;
	delete this.params.EXCEPTIONS;
  },
  
  CLASS_NAME : "Carmen.Layer.WMTSGroup"	
});










