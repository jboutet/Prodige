
// fix for column widget
// http://www.sencha.com/forum/showthread.php?288460-TreePanel-and-widgetcolumn
Ext.define('Ext.grid.column.WidgetTree', {
    extend: 'Ext.grid.column.Widget',

    privates: {

        onItemUpdate: function(record, recordIndex, oldItemDom) {
            if (this.isVisible()) {
              // new code culled from onViewRefresh method
              var me = this,
                  widget = me.liveWidgets[record.internalId],
                  view =   me.up('treepanel').getView(),
                  row =    view.getRowById(record.internalId),
                  cell =   row.cells[me.getVisibleIndex()].firstChild,
                  el, lastBox, width;

              lastBox = me.lastBox;
              if (lastBox && !me.isFixedSize && width === undefined) {
                  width = lastBox.width - parseInt(me.getCachedStyle(cell, 'padding-left'), 10) - parseInt(me.getCachedStyle(cell, 'padding-right'), 10);
              }

              Ext.fly(cell).empty();
              if (el = (widget.el || widget.element)) {
                  cell.appendChild(el.dom);
                  if (!me.isFixedSize) {
                      widget.setWidth(width);
                  }
              } else {
                  if (!me.isFixedSize) {
                      widget.width = width;
                  }
                  widget.render(cell);
              }
              // end new code
                              
              this.updateWidget(record);
            }
        }
        
    }
});


/**
 * A plugin that provides the ability to visually indicate to the user that a node is disabled.
 * 
 * Notes:
 * - Compatible with Ext 4.x
 * - If the view already defines a getRowClass function, the original function will be called before this plugin.
 * 
        var tree = Ext.create('Ext.tree.Panel',{
            plugins: [{
                ptype: 'nodedisabled'
            }]   
            ...
        });
 * 
 * @class Ext.ux.tree.plugin.NodeDisabled
 * @extends Ext.AbstractPlugin
 * @author Phil Crawford
 * @license Licensed under the terms of the Open Source <a href="http://www.gnu.org/licenses/lgpl.html">LGPL 3.0 license</a>.  Commercial use is permitted to the extent that the code/component(s) do NOT become part of another Open Source or Commercially licensed development library or toolkit without explicit permission.
 * @version 0.2 (August 10, 2011)
 * @constructor
 * @param {Object} config 
 */

Ext.define('Ext.ux.tree.plugin.NodeDisabled', {
    alias: 'plugin.nodedisabled'
    ,extend: 'Ext.AbstractPlugin'
    
    
    //configurables
    /**
     * @cfg {String} disabledCls
     * The CSS class applied when the {@link Ext.data.Model} of the node has a 'disabled' field with a true value.
     */
    ,disabledCls: 'dvp-tree-node-disabled'
    /**
     * @cfg {Boolean} preventSelection 
     * True to prevent selection of a node that is disabled. Default true.
     */
    //,preventSelection: true

    //properties


    /**
     * @private
     * @param {Ext.tree.Panel} tree
     */
    ,init: function(tree) {
        var me = this
            ,view = tree.getView()
            ,origFn
            ,origScope;

        me.callParent(arguments);

        origFn = view.getRowClass;
        if (origFn){
            origScope = view.scope || me;
            Ext.apply(view,{
                getRowClass: function(){
                    var v1,v2;
                    v1 = origFn.apply(origScope,arguments) || '';
                    v2 = me.getRowClass.apply(me,arguments) || '';
                    return (v1 && v2) ? v1+' '+v2 : v1+v2;
                }
                ,onCheckboxChange: me.onCheckboxChange
            });
        } else {
            Ext.apply(view, {
                getRowClass: Ext.Function.bind(me.getRowClass,me)
                ,onCheckboxChange: me.onCheckboxChange
            });
        }

        if (me.preventSelection){
            tree.getSelectionModel().on('beforeselect',me.onBeforeNodeSelect,me);
        }
        if (me.preventCheckChange){
            view.on('checkchange',me.onCheckChange,me);
        }
    } // eof init

    /**
     * Returns a properly typed result.
     * @return {Ext.tree.Panel}
     */
    ,getCmp: function() {
        return this.callParent(arguments);
    } //eof getCmp 

    /**
     * @private
     * @param {Ext.data.Model} record
     * @param {Number} index
     * @param {Object} rowParams
     * @param {Ext.data.Store} ds
     * @return {String}
     */
    ,getRowClass: function(record,index,rowParams,ds){
        return record.get('disabled') ? this.disabledCls : '';
    }//eof getRowClass

    /**
     * @private
     * @param {Ext.selection.TreeModel} sm
     * @param {Ext.data.Model} node
     * @return {Boolean}
     */
    ,onBeforeNodeSelect: function(sm,node){
        if (node.get('disabled')){
            return false;
        }
    }//eof onBeforeNodeSelect
    
    /**
     * @OVERRIDES {@link Ext.tree.View#onCheckboxChange}
     * @private
     * @param {Ext.EventObject} e
     * @param {Object} target
     */
    ,onCheckboxChange: function(e, t) {
        var item = e.getTarget(this.getItemSelector(), this.getTargetEl()),
            record, value;
            
        if (item) {
            record = this.getRecord(item);
            if (record.get('disabled')){ return; }
            
            value = !record.get('checked');
            record.set('checked', value, {silent : false});
            this.fireEvent('checkchange', record, value);
        }
    }//eof onCheckboxChange

    
});//eo class


//end of file

  /**
 * @_requires OpenLayers/Control.js
 */

/**
 * Class: Carmen.Control.LayerTreeManager
 
 * Inherits from:
 *  - <OpenLayers.Control>
 */

var actualNodeId = "";
/*
var menuLayerTreePanel = new Ext.menu.Menu({
  height: 50,
  width: 125,
  itemId: 'panel_consult_dl',
  items: [
    {
      text:"Métadonnées",
      iconCls:"consulter",
      id:"item_consulter"
    },
    {
      text:"T&eacute;l&eacute;charger",
      iconCls:"telecharger",
      id:"item_telecharger"
    }
  ]
});

var menuLayerTreePanelLocalData = new Ext.menu.Menu({
  height: 25,
  width: 125,
  itemId: 'panel_annotation',
  items: [{
  text:"Annotation",  
  iconCls:"consulter",
  id:"item_annotation"
  }]
});
*/
Carmen.Control.LayerTreeManager = new OpenLayers.Class(OpenLayers.Control, {

  layerManagerPanel : Ext.create('Ext.form.Panel', {
    title: 'Legende',
    overflowY: 'auto',
    margins: '0 0 0 0',
    fitToFrame: true,
    header : false,

    tabConfig: {
      title: 'Calques',
      tooltip: 'Gestion des calques'
    }
  }),

  layerTree: null,

  visible : true,

  styleControler : null,

  initialize : function(options) {

    //alert("layerTreeManagser 1");
    OpenLayers.Control.prototype.initialize.apply(this, arguments);

    this.layerTreeStore = Ext.create('Ext.data.TreeStore', {
      id : 'layerTreeStore',
      root: {
        expanded: true,
        text : Carmen.Control.LayerTreeManager.TREE_NODE_ROOT_LABEL,
        children: []
      },
      model : 'cmn.layerTreeModel'
    });
    /*
    Ext.define('cmn.layerTreeView', {
      extend: 'Ext.tree.view'
    });
    */
    // the left navigation panel composed of three sub-panels

    this.layerTreePanel = Ext.create('Ext.tree.Panel',{
      id: 'treePanel',
      margins: '5 0 5 5',
      /*maxHeight: 300,*/
      width: 450,
      useArrows: true,
      border: false,
      hideHeaders: true,
      //disableSelection: true,
      //containerScroll:true,
      //fitToFrame: true,
      rootVisible: true,
      //rootVisible: false,

      viewConfig: {
        animate: false,
        plugins: { 
          ptype: 'treeviewdragdrop',
          appendOnly: false,
          ddGroup: 'layerTreePanel'
          //containerScroll: true,
          //,
          //dragZone : {
            //onInitDrag: function(x, y) {
              //var me = this,
                  //data = me.dragData,
                  //view = data.view,
                  //selectionModel = view.getSelectionModel(),
                  //record = view.getRecord(data.item);
              //console.log('here !!!');

              //if (!selectionModel.isSelected(record)) {
                  //selectionModel.selectWithEvent(record, me.DDMInstance.mousedownEvent);
              //}
              //data.records = selectionModel.getSelection();
              ////data.records = [record];
              //me.ddel.setHtml(me.getDragText());
              //me.proxy.update(me.ddel.dom);
              //me.onStartDrag(x, y);
              //return true;
            //}
          //}
        },
        listeners: {
          nodedragover : Carmen.Control.LayerTreeManager.isDropValid
          // obedel alternatives to node 'move' event
          //,
          //beforedrop: Carmen.Control.LayerTreeManager.handleDrop
        }
      },
      columns: [
      {
          hideMode: 'visibility',
          xtype: 'treecolumn',
          locked: false,
          dataIndex: 'text',
          width: 300,
          cellWrap: true,
          cellTpl : [
                '<div class="row0" style="width:300px;">',
                '<div class="row0_first" style="width:{[20+(values.record.getDepth()*20)+20]}px;" >',
                '<tpl for="lines">',
                    '<img src="{parent.blankUrl}" class="{parent.childCls} {parent.elbowCls}-img ',
                    '{parent.elbowCls}-<tpl if=".">line<tpl else>empty</tpl>" role="presentation"/>',
                '</tpl>',
                '<img src="{blankUrl}" class="{childCls} {elbowCls}-img {elbowCls}',
                    '<tpl if="isLast">-end</tpl>',
                    '<tpl if="expandable">-plus {expanderCls}</tpl>',
                    '" role="presentation"/>',
                '<tpl if="checked !== null">',
                '<input type="button" {ariaCellCheckboxAttr}',
                    ' class="{childCls} {checkboxCls}<tpl if="checked"> {checkboxCls}-checked</tpl>"/>',
                '</tpl>',
                '</div>',

                '<div class="row0_second',
                '<tpl if="values.record.attributes.bottomLegend">', 
                  ' legend-expandable x-grid-tree-node-expanded', 
                  '">',
                  '<img src="{blankUrl}" class="{childCls} {elbowCls}-img {elbowCls}-end {expanderCls}" role="presentation">',
                '<tpl else>', 
                  '">',
                '</tpl>', 

                '<tpl if="!values.record.attributes.classLegend">',
                  '<tpl if="!values.record.attributes.iconDisplayStrategy && !values.record.attributes.bottomLegend"/>', 
                    '<img src="{blankUrl}" role="presentation" class="{childCls} {baseIconCls} ',
                        '{baseIconCls}-<tpl if="leaf">leaf<tpl else>parent</tpl> {iconCls}"',
                        '<tpl if="icon">style="background-image:url({icon})',
                          '<tpl if="values.record.attributes.iconStyle"> {values.record.attributes.iconStyle}</tpl>',
                        '"',  
                        '<tpl elseif="values.record.attributes.iconStyle"> style="{values.record.attributes.iconStyle}"',
                        '</tpl>/>',
                  '</tpl>',
                '</tpl>',

                '<tpl if="href">',
                    '<a href="{href}" role="link" target="{hrefTarget}" class="{textCls} {childCls}">{value}</a>',
                '<tpl else>',
                    '<span class="{textCls} {childCls} layerTreeNode">{value}',
                    '</span>',

                    '<tpl if="values.record.attributes.classLegend">', 
                        '<tpl for="values.record.attributes.classLegend">',
                          '<br/>',
                          '<img src="{parent.blankUrl}" role="presentation" class=" x-tree-elbow-img x-tree-elbow-empty"',
                          'style="" ></img>',
                          '<img src="{parent.blankUrl}" role="presentation" class=" x-tree-icon x-tree-icon-leaf cmnLayerTreeLayerNodeIcon"',
                          'style="background-image:url({icon})" ></img>',
                          '<span class="x-tree-node-text" >{text}</span>',
                        '</tpl>',  
                    '<tpl elseif="values.record.attributes.bottomLegend"/>',
                        '<div>',
                        '<img id="legendNode-{record.internalId}_iconDisplay" src="<tpl if="icon">{icon}<tpl else>{blankUrl}</tpl>" role="presentation" />',
                        '</div>',
                    '</tpl>',
                 '</tpl>', 
                '<tpl if="record.attributes.forceOpacityControl">',
                '</tpl>',
                '</div>',
                '</div>'
          ],
          listeners : {
            render : function(col, eopts) {
              col.variableRowHeight = false;
              return true;
            }
          }
      },
      {
        xtype: 'actioncolumn',
        width: 60,
        items: [{
          icon: '/IHM/images/fiche_meta.png',
          tooltip : 'Visualiser la métadonnée associée',
          handler: function(grid, rowIndex, colIndex) {
            var rec = grid.getStore().getAt(rowIndex); 
            this.showMetadata(rec);
            },
          getClass: function(v, metadata, r, rowIndex, colIndex, store) {
            if(typeof(r.attributes.metadata_url) == "undefined" || r.attributes.metadata_url==null) {
                  return "x-hidden-display";
            }else{
              return "";
            }
          },
          scope : this
        },
        {
          icon: '/IHM/images/download.png',
          tooltip : 'Télécharger la donnée',
          handler: function(grid, rowIndex, colIndex) {
            var rec = grid.getStore().getAt(rowIndex); 
            this.downloadData(rec);
            },
          getClass: function(v, metadata, r, rowIndex, colIndex, store) {
            if(typeof(r.attributes.download_url) == "undefined" || r.attributes.download_url==null) {
                  return "x-hidden-display";
            }else{
              return "";
            }
          },
          scope : this
        },
        {
          icon: '/IHM/images/delete.png',
          tooltip : 'Supprimer la donnée de la carte',
          handler: function(grid, rowIndex, colIndex) {
            var rec = grid.getStore().getAt(rowIndex); 
            this.removeLayerNode(rec);
            },
          getClass: function(v, metadata, r, rowIndex, colIndex, store) {
            if(typeof(r.attributes.removable) == "undefined") {
                  return "x-hidden-display";
            }else{
              return "";
            }
          },
          scope : this
        },
        {
          icon: '/IHM/images/style.png',
          tooltip : 'Modifier les styles',
          handler: function(grid, rowIndex, colIndex) {
            var rec = grid.getStore().getAt(rowIndex); 
            this.styleLayerNode(rec);
            },
          getClass: function(v, metadata, r, rowIndex, colIndex, store) {
            if(typeof(r.attributes.stylable) == "undefined") {
              return "x-hidden-display";
            }else{
              return "";
            }
          },
          scope : this

        },
        {
          icon: '/IHM/images/pencil.png',
          tooltip : 'Editer la donnée',
          handler: function(grid, rowIndex, colIndex) {
            var rec = grid.getStore().getAt(rowIndex);
            var editorCtl = this.getControlByName('Carmen.Control.EditorManager2');
            //console.log(editorCtl);
            if(editorCtl) {
              if(editorCtl.activate(rec.attributes)) {
                //
              }
            }
          },
          getClass: function(v, metadata, r, rowIndex, colIndex, store) {
            if(typeof(r.attributes.OlLayer) == "undefined") {
              return "x-hidden-display";
            }
            else {
              // TODO
              return "x-hidden-display";
            }
          },
          scope : this
        }
        ]
      },
      
      Ext.create('Ext.grid.column.WidgetTree',{
        locked: false,
        dataIndex: 'text',
        widget: {
          xtype: 'slider',
          width: 70,
          minValue: 0,
          maxValue: 100,
          useTips: true,
          tipText: function(thumb){
            return Ext.String.format('{0} %', thumb.value);
          }
        },
        onWidgetAttach: function(widget, record) {
              if (record.attributes && record.attributes.forceOpacityControl) {
                record.attributes.transparencySlider = widget;
                widget.show();
                // transparence initiale
                var initialOpacity = record.attributes.initialOpacity ? 
                  record.attributes.initialOpacity :
                  (record.attributes.opacity ? record.attributes.opacity : 1.0);
                var val = 100 - (initialOpacity * 100);
                
                widget.on(
                  'change',
                  function(slider, newVal, thumb, eOpts) {
                    //console.log('slider change !!! ' + newVal);
                    if (!isNaN(newVal)) {
                      //console.log('updating visibility...');
                      var opacity = 1.0 - (newVal/100);
                      var node = this.node;
                      //node.attributes.opacity = opacity;
                      var olLayers = Carmen.Util.set_from_array(node.collect('OlLayer'));
                      for (var i=0; i < olLayers.length; i++) { 
                          olLayers[i].setOpacity(opacity);
                      }
                      var layerOrClassNodes = node.collectChildNodes( 
                        function (n) {
                          return ('type' in n.attributes && 
                            (n.attributes.type == Carmen.Control.LayerTreeManager.NODE_LAYER ||
                             n.attributes.type == Carmen.Control.LayerTreeManager.NODE_CLASS));
                        });
                      var sliderNodes = node.collectChildNodes( 
                        function (n) {
                          return n.attributes.forceOpacityControl;
                        });
                    }
                  }, 
                  {
                    node : record
                  }
                ); 
                widget.setValue(val);
              } else {
                  widget.hide();
              }
          }
      })

      ],
      store : this.layerTreeStore,
      scope: this,
      listeners:{
        checkchange: function(node, checked, eOpts) {
          //console.log('check ! ' + checked + ' for ' + node.data.text);
          if (this.nodeInitialCheck==null) {
            this.nodeInitialCheck=node;
          }
          this.updateLegendNodeVisibility(node);
          // changing layer visibility   
          if (node.isLeaf()) {
            var layer = node.attributes.OlLayer;
            if (node.attributes.jsonLayerDesc) {
              var lname = node.attributes.jsonLayerDesc.attributes.name;
              layer.setSubLayerVisibility(lname, checked);
              // switching off label if necessary...
              if (node.attributes.hasLabel) {
                node.attributes.labelLayer.setSubLayerVisibility(lname, checked);
              }
            }
            else {
              layer.setVisibility(checked);
            }
          }
          // populating to children
          node.eachChild(
            function(n) {
              n.set('checked', this.parentState);
              n.getOwnerTree().fireEvent('checkchange', n, this.parentState);
              return true;
            },
            { parentState: checked }
          );
          // advertising parent node
          node.bubble(
            function(n) {
              if (n==this.startNode) {
                return true;
              }
              var checked = this.currentState;
              for (var i=0; i<n.childNodes.length; i++) {
                checked = checked || (n.childNodes[i].get('checked')===true);
              }
              n.set('checked', checked);
              this.ctl.updateLegendNodeVisibility(n);
              return true;
            },
            { currentState: checked, startNode : node , ctl : this}
          );
          if (this.nodeInitialCheck==node) {
            this.nodeInitialCheck=null;
            // forcing legend update
            var legendControl = this.getLegendControl();
            legendControl.refreshLegend();
          }
        },
        
        itemclick : function ( tree , record , htmlEl , index , e , eOpts ) {
        	htmlEl = Ext.get(htmlEl);
        	
        	var node = htmlEl.down('.legend-expandable');
        	if ( !node ) return;
          node.toggleCls('x-grid-tree-node-expanded');
        	node.toggleCls('x-grid-tree-node-collapsed');
        },
        scope : this
      },
      plugins: [{
        ptype: 'nodedisabled'
      }]
    });

  },

  setMap: function(map) {
    OpenLayers.Control.prototype.setMap.apply(this, arguments);

    // Adding Panel
    this.layerManagerPanel.add(this.layerTreePanel);
    this.map.app.ui.addToDataPanel(this.layerManagerPanel,0, true);

    // adding the sliderColum visibility toolButton
    /*this.map.app.ui.dataPanel.addTool(
      qtip: 'Gestion de la transparence',
      xtype: 'tool',
      renderTpl: [
                '<i id="{id}-toolEl" data-ref="toolEl" class="fa fa-sliders  fa-1x"' +
                '" style="color: #FFF; font-size: 16px;" role="presentation"/></i>'
              ],
      scope:this,
      handler : function() {
        var view = this.layerTreePanel.getView();
        var sliderColumn = view.grid.columns[2];
        sliderColumn.setVisible(!sliderColumn.isVisible());
      }
    }
    );*/


    // ???
    this.layerManagerPanel.show();

    // ???
    this.layerTreePanel.expand();

    // ???
    this.map.app.ui.doLayout();

    this.map.events.register('zoomend', this, function(){
      var mapScale = this.map.getScale();
        var layers = this.map.app.context.layer;
        //TODO BF à reprendre
        /*
        for(var i = 0; i < layers.length; i++){
          if(layers[i].Extension && layers[i].Extension.TYPE_SYMBO && layers[i].Extension.TYPE_SYMBO == "PROPORTIONAL"){
          //if(layers[i].params && layers[i].params.ISPROPORTIONAL){
            //var symbolscaledenom = layers[i].params.SYMBOLSCALEDENOM;
            var symbolscaledenom = layers[i].Extension.layerSettings_symbolScaleDenom;
            //Update High value
            var highvalue = Math.ceil((30*mapScale)/(parseFloat(symbolscaledenom)+((parseFloat(symbolscaledenom)*15)/100)));
            var tdSymboHigh = document.getElementById(layers[i].Title+"_valueSymboHigh");
          if(tdSymboHigh)
              tdSymboHigh.innerHTML = highvalue;
            //Update Low Value
            var lowvalue  = Math.ceil((18*mapScale)/(parseFloat(symbolscaledenom)+((parseFloat(symbolscaledenom)*15)/100)));
            var tdSymboLow = document.getElementById(layers[i].Title+"_valueSymboLow");
            if(tdSymboLow)
              tdSymboLow.innerHTML = lowvalue;
          //Mise à jour du layerTreePanel
          
            var layerTreePanelChilds = this.layerTreePanel.root.childNodes;
          for(var j = 0; j < layerTreePanelChilds.length; j++){
          var subChilds = layerTreePanelChilds[j].childNodes;
          if(subChilds.length > 0){
            for(var k = 0; k < subChilds.length; k++){
             if(subChilds[k].attributes.layerName == layers[i].Title){
            var oldText = subChilds[k].text;
              var patternHigh = /<span class='valueSymboHigh'>.*<\/span><\/td><\/tr>.*<tr>/;
              var newText = oldText.replace(patternHigh, "<span class='valueSymboHigh'>"+highvalue+"</span></td></tr><tr>");
              var patternLow = /<span class='valueSymboLow'>.*<\/span>/;
              var newText2 = newText.replace(patternLow, "<span class='valueSymboLow'>"+lowvalue+"</span>");
              subChilds[k].text = newText2;
             }
            }
          }else{
            if(layerTreePanelChilds[j].attributes.layerName == layers[i].Title){
              var oldText = layerTreePanelChilds[j].text;
              var patternHigh = /<span class='valueSymboHigh'>.*<\/span><\/td><\/tr>.*<tr>/;
              var newText = oldText.replace(patternHigh, "<span class='valueSymboHigh'>"+highvalue+"</span></td></tr><tr>");
              var patternLow = /<span class='valueSymboLow'>.*<\/span>/;
              var newText2 = newText.replace(patternLow, "<span class='valueSymboLow'>"+lowvalue+"</span>");
              layerTreePanelChilds[j].text = newText2;
            };
          }
          }
          }
        }
        */
    });
    // to force a render of all node at the loading of the component
    // necessary for checkbox to work well
    //obedel debug 2.1.1 -> 3.0.0
    //this.layerTree.expand(true);
  },


  treeLayoutInit : function() {
    // Drag & Drop
    /*
    this.layerTreePanel.dropZone = new Carmen.Control.LayerTreeManager.TreeDropZone(
      this.layerTreePanel,
      this.layerTreePanel.dropConfig || 
        {
          ddGroup: this.layerTreePanel.ddGroup || "TreeDD", 
          appendOnly: this.layerTreePanel.ddAppendOnly === true,
          containerScroll: true
        }
    );
    */

    // deploiement initial. necessary cos closed children are not visible from the store by using each !!!
    /*
    this.layerTreePanel.expandAll(
      this.initNodes,
      this);
    */

    this.layerTreePanel.expandAll(
      function() {
        this.layerTreeStore.each(
          function(n) {
            // depliement du noeud
            // ???
            //console.log("depliment de la noeud : ", n);
            n.expand();
            
            //debug
            n.on('expand', function() {
              console.log('expanding');
              console.log(n);
            });
            
            
            this._updateWMSNodeIcon(n);
            // ajout des traitements sur les icônes de l'arbre...
            /*if (n.attributes.metadata_url!=null) {
              var elt_meta = this.layerTreePanel.getEl().getById('layerTreeNode-'+n.internalId+'_meta');
              if (elt_meta) elt_meta.on('click', this.showMetadata, n);
            }
            if (n.attributes.download_url!=null) {
              var elt_download = this.layerTreePanel.getEl().getById('layerTreeNode-'+n.internalId+'_download');
              if (elt_download) elt_download.on('click', this.downloadData, n);
            }
            if (n.attributes.stylable) {
                var elt_style = this.layerTreePanel.getEl().getById('layerTreeNode-'+n.internalId+'_style');
                if (elt_style) elt_style.on('click', this.styleLayerNode, n);
            }*/
            // visibilite selon l'echelle
            this._updateNodeVisibilityFromScale(n);
            // propriétés de la couche
            this._updateLayerProperties(n);
            // numérisation de la couche
            this._editLayer(n);

          },
          this
        )}
      , this);
    // ???
    // affichage de l'arbre
    this.layerManagerPanel.show();
    this.layerTreePanel.getView().refresh();

    var delayedTask = new Ext.util.DelayedTask(
      function() {
        // etat initial : coche/decoche
        this.layerTreeStore.each(
          function(n) {
            if (n) {
              this._updateNodeCheck(n);
            }
          }, this
        );
        // etat initial : ouvert/ferme        
        this.layerTreeStore.each(
          function(n) {
            if (n) {
              this._updateNodeCollapse(n);
            }
          }, this
        );

        // in area extractor context
        // reshaping the ui components
        if (this.map.app.inAreaExtractor) {
          this.map.app.ui.extractorLayout();
          //var el = dataButton-btnInnerEl
          //this.map.app.ui.dataButton.setStyle('padding-left',-16);
          //this.map.app.ui.dataButton.setStyle('padding-right',10);
        }
        else
          this.map.app.ui.standardLayout();

        // masquage de la colonne gestion de la visibilité :
        //var sliderColumn = this.layerTreePanel.getView().grid.columns[2];
        //sliderColumn.setVisible(false);
        
        // notification tree is loaded
        this.map._launchLayerTreeLoaded();
      },
      this
    );

    delayedTask.delay(250);

    // General UI init
    // hiding locator and data panels
    //this.map.app.ui.locatorButton.toggle();
    //this.map.app.ui.dataButton.toggle();
  },
/*
  initNodes : function() {
    this.layerTreeStore.each(
      function(n) {
        if (n) {
          // depliement du noeud
          n.expand();
          this._updateWMSNodeIcon(n);
          // ajout des traitements sur les icônes de l'arbre...
          if (n.attributes.metadata_url!=null) {
            var elt_meta = this.layerTreePanel.getEl().getById('layerTreeNode-'+n.internalId+'_meta');
            if (elt_meta) elt_meta.on('click', this.showMetadata, n);
          }
          if (n.attributes.download_url!=null) {
            var elt_download = this.layerTreePanel.getEl().getById('layerTreeNode-'+n.internalId+'_download');
            if (elt_download) elt_download.on('click', this.downloadData, n);
          }

          if (n.attributes.stylable) {
            var elt_style = this.layerTreePanel.getEl().getById('layerTreeNode-'+n.internalId+'_style');
            if (elt_style) elt_style.on('click', this.styleLayerNode, n);
          }
          //  ajout du slider de transparence

          if (n.attributes.forceOpacityControl) {

            n.attributes.transparencySlider = Ext.create('Ext.slider.Single', {
              id: 'layerTreeNode-' +n.internalId + '_slider',
              renderTo: 'layerTreeNode-' + n.internalId + '_ext',
              width: 75,
              minValue: 0,
              maxValue: 100,
              useTips: true,
              tipText: function(thumb){
                return Ext.String.format('{0} %', thumb.value);
              }
            });
            // transparence initiale
            if (n.attributes.initialOpacity) {
              var val = 100 - (initialOpacity * 100);
              n.attributes.transparencySlider.setValue(val,true);
            }
            n.attributes.transparencySlider.on(
              'change', 
              this.sliderChange, 
              {
                control : this,
                node : n
              }
            ); 
          }
          // visibilite selon l'echelle
          this._updateNodeVisibilityFromScale(n);
          // propriétés de la couche
          this._updateLayerProperties(n);
        }
      },
      this
    );

    this.layerTreeStore.each(
      function(n) {
        if (n) {
          // etat initial : ouvert/ferme
          this._updateNodeCollapseCheck(n);
        }
      }, this);
  },
*/

  initNode: function(n) {
    // ???
    //console.log("depliement du noeud dans la fonction initNode ...");
    // depliement du noeud
    n.expand();
    this._updateWMSNodeIcon(n);
    // ajout des traitements sur les icônes de l'arbre...
    /*if (n.attributes.metadata_url!=null) {
      var elt_meta = this.layerTreePanel.getEl().getById('layerTreeNode-'+n.internalId+'_meta');
      if (elt_meta) elt_meta.on('click', this.showMetadata, n);
    }
    if (n.attributes.download_url!=null) {
      var elt_download = this.layerTreePanel.getEl().getById('layerTreeNode-'+n.internalId+'_download');
      if (elt_download) elt_download.on('click', this.downloadData, n);
    }
    if (n.attributes.removable) {
      var elt_remove = this.layerTreePanel.getEl().getById('layerTreeNode-'+n.internalId+'_remove');
      if (elt_remove) elt_remove.on('click', this.removeLayerNode, n);
    }
    if (n.attributes.stylable) {
      var elt_style = this.layerTreePanel.getEl().getById('layerTreeNode-'+n.internalId+'_style');
      if (elt_style) elt_style.on('click', this.styleLayerNode, n);
    }
    }*/
   /*
    //  ajout du slider de transparence
    if (n.attributes.forceOpacityControl) {
      n.attributes.transparencySlider = Ext.create('Ext.slider.Single', {
        id: 'layerTreeNode-' +n.internalId + '_slider',
        renderTo: 'layerTreeNode-' + n.internalId + '_ext',
        width: 75,
        minValue: 0,
        maxValue: 100,
        useTips: true,
        tipText: function(thumb){
          return Ext.String.format('{0} %', thumb.value);
        }
      });
      // transparence initiale
      if (n.attributes.initialOpacity) {
        var val = 100 - (n.attributes.initialOpacity * 100);
        n.attributes.transparencySlider.setValue(val,true);
      }
      n.attributes.transparencySlider.on(
        'change', 
        this.sliderChange, 
        {
          control : this,
          node : n
        }
      ); 
    }
    */
  },

  buildTree: function(jsonLayerTree, jsonLayerList) {
    this.layerTree = this._buildLayerTree(this.layerTreeStore.getRoot(), jsonLayerTree.LayerTreeNode, jsonLayerList);
    //obedel migration in progress
    this.layerTreeStore.setRoot(this.layerTree);
  },

  getLayerTree : function() {
    return this.layerTree;
  },

  getNodesBy: function(filter) {
    return this.layerTreeStore.queryBy(filter);
  },

  getSubLayerTreeBy: function(filter) {
    var tree = this.getLayerTree().clone();
    tree.filterChildNodes(filter);
    return tree;
  },

  _collectVisibilityInfo : function(node) {
     var collector = [];
     if ('type' in node.attributes && node.attributes.type== Carmen.Control.LayerTreeManager.NODE_LAYER) {
       collector.push({
        layerName : node.attributes.layerName,
        layerIdx : node.attributes.layerIdx,
        visible : node.attributes.OlLayer.getVisibility(node.attributes.layerName)
       });
     }
     else if ('type' in node.attributes && node.attributes.type== Carmen.Control.LayerTreeManager.NODE_GROUP) {
       for (var i=0; i<node.childNodes.length; i++) {
         var infos = this._collectVisibilityInfo(node.childNodes[i]);
         collector = collector.concat(infos);
       }
     }
     return collector;
  },
  // return an array of layer description
  // { layerName, layerIdx, visible }
  getLayersVisibility : function() {
    return this._collectVisibilityInfo(this.layerTree);
  },
  _filterVisibleNode : function(node) {
    var res = true;
    res = ('type' in node.attributes) && 
      ((node.attributes.type == this.NODE_GROUP && node.hasChildNodes()) ||
       (node.attributes.type == this.NODE_LAYER && node.attributes.OlLayer.getSubLayerVisibility(node.attributes.jsonLayerDesc.attributes.name)));
    return res;
  },

  ////////////////////////////////////////////////////// 
  // misc layer nodes functions
  ////////////////////////////////////////////////////// 
  _filterWMSLayerNodes : function (n) {
    var type = Ext.valueFrom(n.attributes.type, -1, false);
    var res = (type == Carmen.Control.LayerTreeManager.NODE_LAYER &&
      n.attributes.OlLayer instanceof Carmen.Layer.WMSGroup);
    return res;
  },
  _filterMapserverLayerNodes : function (n) {
    var type = Ext.valueFrom(n.attributes.type, -1, false);
    var res = (type == Carmen.Control.LayerTreeManager.NODE_LAYER &&
      n.attributes.OlLayer instanceof Carmen.Layer.MapServerGroup);
    return res;
  },
  _groupNodeFilter : function(n) {
    var type = Ext.valueFrom(n.attributes.type, -1, false);
     return (type == Carmen.Control.LayerTreeManager.NODE_GROUP);
  },

  _updateWMSNodeIcon : function(n) {
    if (this._filterWMSLayerNodes(n) ) {
      var layer = n.attributes.OlLayer;
      var layerName = n.attributes.layerName;
      var info = layer.getSubLayerInfo(layerName);
      var wmsName = 'jsonLayerDesc' in n.attributes  ?
        n.attributes.jsonLayerDesc.Name :
        layerName;
      var layerSettings_isWmsc = 'jsonLayerDesc' in n.attributes  ?
          n.attributes.jsonLayerDesc.Extension.layerSettings_isWmsc =="YES" :
            false;
      if(!layerSettings_isWmsc){
        n.data.icon = layer.getLegendGraphicURL(wmsName);
        n.data.iconCls =  'cmnLayerTreeLayerWMSNodeIcon';
        n.attributes.iconDisplayStrategy=true;
        if (n.attributes.linkedNodes) {
          var legendNode = n.attributes.linkedNodes[0];
          if (legendNode && info) {
            n.attributes.iconDisplayStrategy = true;
            legendNode.data.icon = layer.getLegendGraphicURL(wmsName);
            legendNode.data.iconCls = 'cmnLayerTreeLayerWMSNodeIcon';
          }
        }
      }
      // obedel forcing view refresh for layout update on chrome
      // should be done better way or else where to prevent tree view update for each wms node
      /*var view = n.getOwnerTree().getView();
      view.refresh();*/
    }
  },

  _getLegendNode: function(n) {
    var res = n.attributes && n.attributes.linkedNodes && n.attributes.linkedNodes.length>0 ?
      n.attributes.linkedNodes[0] : null;
    return res;
  },

  ////////////////////////////////////////////////////// 
  // misc opacity functions
  ////////////////////////////////////////////////////// 

  _filterOpacityHandlingNodes : function (n) {
    // check specific flag
    return n.attributes.forceOpacityControl;
    // enhancement... 
    // && (n.getDepth()<=1 || (n.getDepth()>1 && !(n.isFirst() && n.isLast())));
    /*
    // nodes with opacity control are group nodes and 
    // layer nodes which direct children of the root
    // and wms layer nodes with transparency not null
    // note : should be parametered in by a flag (coming from ows context) 
    // instead of testing special cases
    //console.log(this);
    var type = Ext.valueFrom(n.attributes.type, -1, false);
    var res = (
     (type == Carmen.Control.LayerTreeManager.NODE_GROUP && n.getDepth()>=1) ||
     (type == Carmen.Control.LayerTreeManager.NODE_LAYER && n.getDepth()==1) ||
     (type == Carmen.Control.LayerTreeManager.NODE_LAYER && n.attributes.OlLayer instanceof Carmen.Layer.WMSGroup 
      && n.attributes.opacity!=1.0)
     );
    //console.log('in filter -> ' +n.attributes.text + ' ' + res + ' (' + n.attributes.opacity +')')

    return res;
    */ 
  },

  CLASS_NAME: "Carmen.Control.LayerTreeManager",

  ////////////////////////
  // MISC EVENT FUNCTIONS
  ////////////////////////
  // Tree node functions
  // fonction d'affichage de la fiche de métadaonnées
  // scope : this -> treenode courant

  showMetadata : function(node) {
    if (node.attributes.metadata_url!=null){
      var url = node.attributes.metadata_url;
      window.open(url);
      //Carmen.Util.openWindowIframe("M&eacute;tadonn&eacute;es de la couche ", url, false, 700, 500);
    }
  },
  // téléchargement des données
  // scope : this -> treenode courant
   downloadData : function(node) {
    if (node.attributes.download_url!=null)
      var url = node.attributes.download_url;
      window.open(url, "download", "location=no, toolbar=no, scrollbars=yes");
  },

  removeLayerNode : function(node) {
    // removing the layer from the map
    var layer = node.attributes.OlLayer;
    var map = layer.map;
    map.removeLayer(layer);
    // removing the node from the tree
    node.remove();
    // and in the legend...
    if (node.attributes && node.attributes.linkedNodes) {
      var legendNode = node.attributes.linkedNodes[0];
      legendNode.remove();
    }
    //evt.stopPropagation();
    return false;
  },
  styleLayerNode : function(node) {
    //console.log('style Layer node');
    /*
    if(typeof Ext.getCmp("styleWindow_CmnControlLocalStyler") !== "undefined"){
      var styleControler = Ext.getCmp("styleWindow_CmnControlLocalStyler").scope;
    }else{
      var styleControler = new Carmen.Control.LocalStyler();
    }
    */
    if (!this.styleControler)
      this.styleControler= new Carmen.Control.LocalStyler();
    actualNodeId = node.attributes.layerName;
    var layer = node.attributes.OlLayer;
    this.styleControler.activate(layer, node);
    // evt.stopPropagation();
    return false;
  },

  // action sur slider --> modification transparence
  // scope : this -> treenode courant
  sliderChange : function(slider, newVal, thumb, eOpts) {
    var opacity = 1.0 - (newVal/100);
    var node = this.node;
    //node.attributes.opacity = opacity;
    var olLayers = Carmen.Util.set_from_array(node.collect('OlLayer'));
    for (var i=0; i < olLayers.length; i++) { 
        olLayers[i].setOpacity(opacity);
    }
    var layerOrClassNodes = node.collectChildNodes( 
      function (n) {
        return ('type' in n.attributes && 
          (n.attributes.type == Carmen.Control.LayerTreeManager.NODE_LAYER ||
           n.attributes.type == Carmen.Control.LayerTreeManager.NODE_CLASS));
      });
    var sliderNodes = node.collectChildNodes( 
      function (n) {
        return n.attributes.forceOpacityControl;
      });
  },

  ////////////////////////
  // layerTree building 
  ////////////////////////
  NODE_LAYER : 0,
  NODE_GROUP : 1,
  NODE_CLASS : 2,
  NODE_FEATURE : 3,
  /**
   * @return a json layer description from its name 
   * @Todo to be tested.. 
  */
  _getLayer : function(jsonLayerList, layerName) {
    var layer = null;
    if (jsonLayerList instanceof Array) {
      var i=0;
      while (i<jsonLayerList.length && jsonLayerList[i].attributes.name!=layerName) {
        i++;
      }
      if (i<jsonLayerList.length) {
        layer = jsonLayerList[i];
      }
    }
    else {
      if (jsonLayerList.attributes.name!=layerName) {
        layer = jsonLayerList;
      }
    }
    return layer;
  },

  _getSingleClassLegendUrl : function(jsonLayerDesc) {
    var url = '/IHM/images/NoTreeIcon.gif';

    if ('StyleList' in jsonLayerDesc) {
        // Here only deals with one class legend per layer
        if ('Style' in jsonLayerDesc.StyleList 
          && !(jsonLayerDesc.StyleList.Style instanceof Array)
          && 'LegendURL' in jsonLayerDesc.StyleList.Style  ) {
          //var legendUrl = jsonLayerDesc.StyleList.Style.LegendURL.attributes;
          var url = jsonLayerDesc.StyleList.Style.LegendURL.OnlineResource.attributes.href;
        }
    }

    return url;
  },

  _buildMutliClassLegend : function(jsonLayerDesc, displayClass) {
    var classeNodes = [];
    for (var i=0; i<jsonLayerDesc.StyleList.Style.length; i++) {
      if (displayClass!=null && displayClass[i]!="OFF") {
        var style = jsonLayerDesc.StyleList.Style[i];
        //var legendUrl = style.LegendURL.attributes;
        var url = style.LegendURL.OnlineResource.attributes.href;
        /*var url = legendUrl.server + '?map=' + legendUrl.mapfile + 
          '&mode=LEGENDICON&icon=' + escape(legendUrl.layer) + 
          ',' + legendUrl.classIdx;*/
        if (style.Name != "") {
          var classNode = {
            text : style.Title,
            icon : url
          };
          classeNodes.push(classNode);
        }
      }
    }

    return classeNodes;
  },

  _buildLayerTree : function(parentNode, jsonNode, jsonLayerList) {
    var tree = this._buildLayerTree_aux_ext5(parentNode, jsonNode, jsonLayerList);
    /*
    // Find nodes that should have a bottom elt for slider...
    var nodesWOpacityCtl = tree.collectChildNodes(
      this._filterOpacityHandlingNodes
    );    
    // and updates bottom elt ui property
    for (var i=0; i< nodesWOpacityCtl.length; i++) {
      //console.log(nodesWOpacityCtl[i].attributes.text + " " + nodesWOpacityCtl[i].attributes.forceOpacityControl);
      nodesWOpacityCtl[i].attributes.hasBottomElt = true; 
    } */ 
    return tree;
  },

  _buildLayerTree_aux_ext5 : function(parentNode, jsonNode, jsonLayerList) {
    var att = jsonNode.attributes;
    var groupNode = (att.type == "group");

    var jsonLayerDesc = jsonLayerList[att.layerIdx];
    var multiClass = !groupNode 
      &&  ('StyleList' in jsonLayerDesc)
      && ('Style' in jsonLayerDesc.StyleList) 
      && (jsonLayerDesc.StyleList.Style instanceof Array);

    /********************* Symbole proportionnel *******************/
    var nameSymbo = "";
    var tmp_layer = this._getLayer(jsonLayerList, jsonNode.attributes.mapName);
    if(tmp_layer && tmp_layer.Extension.TYPE_SYMBO && tmp_layer.Extension.TYPE_SYMBO == "PROPORTIONAL"){
      var scale = (this.map ? this.map.getScale() : 1);
      var symbolsscaledenom = (tmp_layer.Extension.layerSettings_symbolScaleDenom ? tmp_layer.Extension.layerSettings_symbolScaleDenom : -1);
      nameSymbo = "<table style='display:inline'>"+
                      " <tr><td id='"+tmp_layer.Title+"_valueSymboHigh'><span class='valueSymboHigh'>&nbsp;</span></td></tr>"+
                      " <tr><td id='"+tmp_layer.Title+"_valueSymboLow'><span class='valueSymboLow'>&nbsp;</span></td></tr>"+
                      "</table> "+att.name;
    }else{
      nameSymbo = att.name;
    }
    /********************* Symbole proportionnel *******************/

    var node = parentNode.createNode({
      text : att.name == "root" ? Carmen.Control.LayerTreeManager.TREE_NODE_ROOT_LABEL : nameSymbo,
      leaf : !groupNode,
      expandable : true,
      expanded : (groupNode && att.open=="1") || multiClass,
      checked : (groupNode ? false : jsonLayerDesc.attributes.hidden=="0"),

      cls : (!groupNode && 'layerSettings_MetadataFile' in jsonLayerDesc.Extension) ? 
        'cmnLayerTreeLayerNodeMetadata': '',
      iconCls : groupNode ? 'cmnLayerTreeGroupNodeIcon' :
       !jsonNode.iconStyle ? 'cmnLayerTreeLayerNodeIcon' : null,
/*
      icon : jsonNode.iconStyle ? null : 
        groupNode || multiClass ? '/IHM/images/empty.gif' : this._getSingleClassLegendUrl(jsonLayerDesc),

      iconCls : groupNode ? 'cmnLayerTreeGroupNodeIcon' : 
       !jsonNode.iconStyle ? 'cmnLayerTreeLayerNodeIcon' : null,

      iconStyle : jsonNode.iconStyle ? jsonNode.iconStyle : null, 
*/
      icon : jsonNode.iconStyle ? null : 
        groupNode || multiClass ? '/IHM/images/empty.gif' : this._getSingleClassLegendUrl(jsonLayerDesc),
      /*iconCls : groupNode ? 'cmnLayerTreeGroupNodeIcon' : 
       !jsonNode.iconStyle ? 'cmnLayerTreeLayerNodeIcon' : null,
*/
      allowDrop : groupNode
      //iconDisplayStrategy : !groupNode && !multiClass//,
    });


    // explicit marking of the visibility of the node in the tree
    node.attributes= {};
    /*
    node.attributes.icon = jsonNode.iconStyle ? null : 
        groupNode || multiClass ? '/IHM/images/empty.gif' : this._getSingleClassLegendUrl(jsonLayerDesc),
    */
    node.attributes.iconCls = groupNode ? 'cmnLayerTreeGroupNodeIcon' : 
       (!jsonNode.iconStyle ? 'cmnLayerTreeLayerNodeIcon' : null);
    
    node.attributes.visible = true;
    node.attributes.opacity = parseFloat(Ext.valueFrom(att.opacity, "1.0", false));

    //only show slider for level 1 (group or alone layers) or WxS layer shown on one call
    var isWxs = (jsonLayerDesc && jsonLayerDesc.Extension.wms_layer && jsonLayerDesc.Extension.wms_layer!="");
    node.attributes.forceOpacityControl = (att.depth==1 || isWxs ? 1 : 0);
    
    node.attributes.iconDisplayStrategy = parseInt(Ext.valueFrom(att.iconDisplayStrategy, "0", false))==1;
    // should legend be displayed under the title ? for complex analysis only 
    node.attributes.bottomLegend = (jsonLayerDesc && ('Extension' in jsonLayerDesc) && 
     ('layerSettings_analyseType' in jsonLayerDesc.Extension) && (
      jsonLayerDesc.Extension.layerSettings_analyseType=="UNIQVALUE" ||
      jsonLayerDesc.Extension.layerSettings_analyseType=="GRADUATECOLOR" ||
      jsonLayerDesc.Extension.layerSettings_analyseType=="GRADUATESYMBOL" ||
      jsonLayerDesc.Extension.layerSettings_analyseType=="PROPORTIONAL" ||
      jsonLayerDesc.Extension.layerSettings_analyseType=="PIECHART" ||
      jsonLayerDesc.Extension.layerSettings_analyseType=="BARCHART"));
    
    if (groupNode) {
      node.attributes.type = this.NODE_GROUP;
      if ('LayerTreeNode' in jsonNode) {
        if (jsonNode.LayerTreeNode instanceof Array) {
          for (var i = 0; i < jsonNode.LayerTreeNode.length; i++) {
            var child = this._buildLayerTree_aux_ext5(parentNode,jsonNode.LayerTreeNode[i], jsonLayerList);
            if (child!=null)
              node.appendChild(child);
          }
        }
        else {
          var child = this._buildLayerTree_aux_ext5(parentNode,jsonNode.LayerTreeNode, jsonLayerList)
            if (child!=null)
              node.appendChild(child);
        }
        }
      node.attributes.expanded = (att.open=="1");
      node.attributes.checked =  this._hasChildNodeChecked(node) > 0; 
    }
    else {
      // node visible or not
      var displayClassLegend = Ext.valueFrom(Url.decode(jsonLayerDesc.Extension.layerSettings_ActivThemes), 
        'ON', false);
//      console.log(displayClassLegend);
      if (displayClassLegend == "OFF") {
        node.attributes.visible = false; 
      }
      // display subclasses in legend
      else if (multiClass) {
        var displayClass = displayClassLegend.split('|');
        displayClass = displayClass.length>0 ? displayClass : null;
        var classLegend = this._buildMutliClassLegend(jsonLayerDesc, displayClass);
        node.attributes.classLegend = classLegend;
      }

      //linked layer attributes to node
      // console.log(att.mapName);
      // console.log(att.layerIdx);
      // console.log(jsonLayerList[att.layerIdx]);
      node.attributes.jsonLayerDesc = this._getLayer(jsonLayerList, att.mapName);
      node.attributes.jsonLayerDesc = jsonLayerList[att.layerIdx];
      node.attributes.layerIdx = att.layerIdx;
      node.attributes.layerName = node.attributes.jsonLayerDesc.attributes.name;
      if (node.attributes.jsonLayerDesc.Extension.layerSettings_infoFields)
        node.attributes.infoFields = decodeURIComponent(node.attributes.jsonLayerDesc.Extension.layerSettings_infoFields);
      if (node.attributes.jsonLayerDesc.Extension.layerSettings_briefFields)
        node.attributes.briefFields = decodeURIComponent(node.attributes.jsonLayerDesc.Extension.layerSettings_briefFields);
      if (node.attributes.jsonLayerDesc.Extension.layerSettings_ToolTipFields)
        node.attributes.toolTipFields = decodeURIComponent(node.attributes.jsonLayerDesc.Extension.layerSettings_ToolTipFields);
      node.attributes.infoData = "";
      node.attributes.baseQueryURL = decodeURIComponent(node.attributes.jsonLayerDesc.Extension.layerSettings_queryURL);
      var minScale = Ext.valueFrom(node.attributes.jsonLayerDesc.MinScaleDenominator, null, false);
      minScale = minScale!=null ? parseFloat(minScale) : minScale;
      if (!isNaN(minScale))  
        node.attributes.minScale = minScale;
      var maxScale = Ext.valueFrom(node.attributes.jsonLayerDesc.MaxScaleDenominator, null, false);
      maxScale = maxScale!=null ? parseFloat(maxScale) : maxScale;
      if (!isNaN(maxScale))  
        node.attributes.maxScale = maxScale;
      
      node.attributes.type = this.NODE_LAYER;
      node.attributes.providerId = att.providerGrpId;
      
      node.attributes.initialChecked = jsonLayerDesc.attributes.hidden=="0";
      
      node.attributes.metadata_url = 'MetadataURL' in jsonLayerDesc ? 
        Url.decode(jsonLayerDesc.MetadataURL.OnlineResource.attributes.href) : null;
      
      node.attributes.download_url = 'DataURL' in jsonLayerDesc ? 
                Url.decode(jsonLayerDesc.DataURL.OnlineResource.attributes.href) : null;  
      
      //obedel test
      //node.attributes.stylable = true;
    }

    // remenbering node expand state
    node.attributes.open = (groupNode && att.open=="1") || multiClass;

    node.addListener('move', 
      function(node , oldParent, newParent, index) { 
        //console.log('before moving node');
        var legendNode = this._getLegendNode(node);
        var legendControl = this.getLegendControl();
        switch(node.attributes.type) {
          case (this.NODE_GROUP) : 
            this.moveGroupNode(node , oldParent, newParent, index);
            if (legendNode && legendControl) {
                legendControl.moveNode(legendNode, index);
            }
            break;
          case (this.NODE_LAYER) :
            this.moveLayerNode(node , oldParent, newParent, index);
            if (legendNode && legendControl) {
                legendControl.moveNode(legendNode, index);
            }
            break;
          default :
            break;
        } 
      },
      this,
      {delay: 100}
    );
    return node;
  },

  //obedel migration : not used anymore
  /*
  // check node handler
  // used to cascade and bubble node check/uncheck between 
  // node and its child nodes and its parent nodes
  onNodeCheck : function(n, state) {
    // use of tag ascending to mark buble propagation to parent nodes
    // and of tag descending for cascade propagation to child nodes
    if (n.ascending) {
       // counting checked child nodes
       var c = 0;
       for (var i=0; i< n.childNodes.length; i++) {
         c = (n.childNodes[i].attributes.checked === true) ? c + 1 : c; 
       }
       
       var cb = n.getUI().checkbox;
       if (cb) {
         cb.checked = (c>0);
         cb.readOnly = (c < n.childNodes.length);
         n.attributes.checked = cb.checked; 
       }
       //console.log('ascending phase... '+ n.attributes.text   + '  ' + c.toString() +'/' + n.childNodes.length.toString());
       if (n.getDepth()>0) {
         n.parentNode.ascending = true
         n.parentNode.fireEvent('checkchange', n.parentNode, state);
       }
       n.ascending = false;
    }
    else {
      //console.log ('this ' + this.attributes.text );
      if (!n.descending) {
        //console.log ('descending phase... ' + n.attributes.text  + '  ' +state);
        n.descending = true;
        // At the moment only activate or desactivate sub nodes...
        if (n.attributes.type == Carmen.Control.LayerTreeManager.NODE_GROUP) {
           for (var i=0; i<n.childNodes.length;i++) {
              //console.log ('childNode ' + this.childNodes[i].attributes.text);
              var cb = n.childNodes[i].getUI().checkbox;
              if (cb) {
                cb.checked = state;
                cb.defaultChecked = state;
                n.childNodes[i].attributes.checked = state;
              }
              n.childNodes[i].fireEvent('checkchange', n.childNodes[i], state);
            }
          }
       n.descending=false;
       if (n.getDepth()>0 && !n.parentNode.descending) {
           n.parentNode.ascending=true;   
           n.parentNode.fireEvent('checkchange', n.parentNode, state);
           var cb = n.getUI().checkbox;
           if (cb) cb.readOnly = false;
        }
      }
    }
  },
  */
  

  updateTreeNodes : function(node, f, ctx, applyToChildrenFilter) {
    ctx = Ext.valueFrom(ctx, null, false);
    if (ctx==null)
      f(node);
    else
      f.call(this, node);

    // split when possible
    if (applyToChildrenFilter!=null 
       && !applyToChildrenFilter(node))
       return;

    // recursivity
    for (var i=0; i<node.childNodes.length;i++) {
      this.updateTreeNodes(node.childNodes[i], f, ctx, applyToChildrenFilter);
    }
  },

  _updateNodeVisibilityFromScale : function(node) {
    // adding a listener to handle node aspect wrt to scale
    if (node.attributes.type == this.NODE_LAYER) {
      if (node.attributes.minScale &&
        node.attributes.maxScale) {
          //console.log(node.attributes.text);
          this.map.events.registerPriority('zoomend', 
            {
              node: node,
              view: this.layerTreePanel.getView(),
              control: this 
            }, 
            this.updateNodeFromScale);
          // initial update
          this.updateNodeFromScale.apply(
            {
              node: node,
              view: this.layerTreePanel.getView(),
              control: this
            });
      }
    }
  },

  // count the childNodes that are checked
  _countChecked : function(n) { 
      var res = 0;
      if (n.isLeaf()) {
        res = n.get('checked') ? 1 :0;
      }
      else {
        for (var i=0;i<n.childNodes.length;i++) {
          res = res + this._countChecked(n.childNodes[i]);
        }
      }
      return res;
  },
  
  _updateNodeCheck : function(node) {
    // lookinq for the check status of group node
   if (!node.isLeaf())
     node.set('checked', this._hasChildNodeChecked(node));
  },

  _updateNodeCollapse : function(node) {
    // expanding/collapsing nodes now that they've been rendered
    if (node.attributes.open)
      node.expand();
    else
      node.collapse();
  },


  _hasChildNodeChecked : function(node) {
      var res = false;
      var i = 0;
      // parcours en largeur d'abord
      while ((!res) && i<node.childNodes.length) {
        res = res || (node.childNodes[i].data.checked === true);
        i++;
      }
      // parcours en profondeur ensuite au besoin
      if (!res) {
        var i=0;
        while ((!res) && i<node.childNodes.length) {
          res = res || this._hasChildNodeChecked(node.childNodes[i]);
          i++;
        }
      }
      return res;
  },

  _updateLayerProperties : function(node) {
    if (this._filterMapserverLayerNodes(node)) {
      var olLayer = node.attributes.OlLayer;
      var lname = node.attributes.layerName;
      if (olLayer.handleSelection(lname) && node.attributes.infoFields) {
        var fieldsDesc = Carmen.Util.buildFieldsDesc(node.attributes.infoFields, node.attributes.baseQueryURL);
        var colModel = Carmen.Util.buildColumnModel(fieldsDesc, false, true);
        olLayer.setSelectionConfig(lname, { store : null, cm : colModel });
      }
    }
  },

  _editLayer: function(node) {
    //traitements déplacés dans EditorManager2 pour effectuer des appels groupés sur tous les noeuds
    return;
//    
//    if((node.attributes.jsonLayerDesc) && (node.attributes.jsonLayerDesc.Extension.GEONETWORK_METADATA_ID)) {
//      var GEONETWORK_METADATA_ID = node.attributes.jsonLayerDesc.Extension.GEONETWORK_METADATA_ID;
//      var url = PRODIGE_VERIFY_RIGHTS_URL + '?ID=' + GEONETWORK_METADATA_ID + '&TRAITEMENTS=EDITION&OBJET_TYPE=dataset';
//      Ext.data.JsonP.request({
//      url: url,
//      method: 'GET',
//      success: function(response) {
//        if(response['EDITION'] || response['EDITION_AJOUT'] || response['EDITION_MODIFICATION']) {
//          node.attributes.isEditable = true;
//          node.attributes.infoEdition = response;
//          node.commit();
//          
//          var editorButton = Ext.getCmp('editorButton');
//          if ( editorButton ){
//          	editorButton.setVisible(true);
//          	editorButton.addLayer(node, response);
//          }
//        }
//      },
//      failure: function(response) {
//        //console.log('failure - response is : ', response);
//      }
//    });
//    // https://www.sencha.com/forum/showthread.php?299055-Change-action-column-icon-for-single-row
//    }
  },

  // Update node aspect in the tree wrt to 
  // layer min/max scale property  
  // The context of the function is  { view, node }
  updateNodeFromScale : function() {
    node =  this.node;
    view = this.view;
    control = this.control;
    var desc = node.attributes.jsonLayerDesc;
    var olLayer = node.attributes.OlLayer;
    var scaleDependant = (desc.Extension && desc.Extension.layerSettings_legendScaleDisplay &&
      desc.Extension.layerSettings_legendScaleDisplay=='ON');
    
    if (olLayer!=null && olLayer.map!=null) {
      var scale = olLayer.map.getScale();
      scale = scale > 0 ? Math.round(scale) : scale;
      var outOfScale = (scale > desc.MaxScaleDenominator || 
          scale < desc.MinScaleDenominator);
      var checked = node.get('checked');
      var layerVisible = (checked === true) && !outOfScale;    
      olLayer.setSubLayerVisibility(node.attributes.layerName, layerVisible);
      
      //obedel todo check where this is used
      //node.attributes.visible = layerVisible;
      
      var el = Ext.fly(view.getNode(node));
      // update of node ui display
      if (outOfScale) {
       if (scaleDependant) {
          if (el) { el.setDisplayed(false); }
        }
        else {
          node.set('disabled', true);
          node.cascadeBy(
            { after : function() { 
              this.set('disabled', true);
            }}
          );
        }
      }
      else {
        if (scaleDependant) {
          if (el) el.setDisplayed(true);
        }
        else {
          node.set('disabled', false);
          if (el) el.show();
          //console.log('enabling ' + node.data.text);
          node.cascadeBy(
            {after: function() { this.set('disabled', false); }}
          );
        }
      }

      //updating legend node visibility
      control.updateLegendNodeVisibility(node);
    } 
  },

  updateLegendNodeVisibility: function(node, noRefresh) {

    if (node.attributes && node.attributes.linkedNodes) {
      var legendNode = node.attributes.linkedNodes[0];
      var checked = node.get('checked');
      var visibility = checked && (node.get('disabled')!=true) && node.attributes.visible;
      var legendTreePanel = legendNode.getOwnerTree();
      if (legendTreePanel) {
        //console.log("updating " + node.data.text + " " + visibility );
        legendNode.attributes.visibility = visibility;
        // old version
        //var legendView = legendTreePanel.getView();
        //var el = Ext.get(legendView.getNode(legendNode));
        //console.log("updated " + legendNode.data.text + " " + legendNode.attributes.visible);
        //console.log("el is : ", el);
        //if (el)
        //  el.setDisplayed(visibility);
      }
    }
  },
  // onExtend or onCollapse
  cloneLayerTree: function(newRoot, transform) {
    //console.log("here function cloneLayerTree ...");
    var root = this.layerTreePanel.getRootNode();
    if (!newRoot) {
      newRoot = root.copy();
      if (root.attributes)
        newRoot.attributes = root.attributes;
      if (transform!=null) 
        newRoot = transform.apply(this,[newRoot]);
    }
    for (var i=0; i<root.childNodes.length; i++) {
      newRoot.appendChild(root.childNodes[i].clone(transform));
    }
    return newRoot;
  },

  ////////////////////////
  // Drag'n Drop part 
  ////////////////////////

  findPreviousLayer : function(groupNode, index) {
    //console.log(groupNode);
    var res = null;
    if (index==0) {
      res = groupNode.getDepth()==0 ?
        null :
        this.findPreviousLayer(groupNode.parentNode, groupNode.parentNode.indexOf(groupNode));
    }
    else {
      var brother = groupNode.childNodes[index-1];
      var layers = brother.collect("OlLayer",true);
      //console.log(layers);
      res = layers[layers.length-1];
    }
    return res;
  },


  // new version

  linkLayerToNode : function(OLlayer, parentNode) {
    // updating nodes which were linked to the split OL layer     
    
    var layerNames = [];
    for (var i=0; i< OLlayer.subLayers.length; i++) {
      layerNames.push(OLlayer.subLayers[i].name);
    }
    var layerNodesToUpdate = parentNode.collectChildNodes(
      function (n) {
        return ('layerName' in n.attributes && 
          Carmen.Util.set_member(layerNames, n.attributes.layerName));  
      });
    
    for (var i=0; i<layerNodesToUpdate.length; i++) {
      var n = layerNodesToUpdate[i];
      //console.log("Updating " + n.attributes.layerName + "...");
      n.attributes.OlLayer = OLlayer; 
    }   
  },

  moveGroupNode : function(node , oldParent, newParent, indexNode) {
    listNodes = oldParent.childNodes;
    var prevIndex = node.attributes.index;
    var bb = node.previousSibling; // node before insertion point
    var ba = node.nextSibling; // node after insertion point

    //console.log("moving Group node from " + prevIndex + " to " + indexNode);
    if (bb!= null && ba!=null && 
      bb.attributes.type == this.NODE_LAYER &&  
      ba.attributes.type == this.NODE_LAYER &&
      bb.attributes.OlLayer == ba.attributes.OlLayer) {
      // splitting bb-ba
      //console.log("splitting bb-ba");
      var index = ba.attributes.OlLayer.getSubLayerInfo(ba.attributes.layerName).index - 1;
      var newLayers = ba.attributes.OlLayer.split(index);
      this.linkLayerToNode(newLayers[1], newParent);
      var pos = this.map.getLayerIndex(bb.attributes.OlLayer);
      this.map.addLayer(newLayers[1], pos);
      newLayers[0].redraw();
      newLayers[1].setVisibility(true);
    }
        
    // looking for index of OL layer preceeding the insertion point  
    var layerBefore = this.findPreviousLayer(newParent, indexNode);
    var startIndex = layerBefore==null ? this.map.layers.length : this.map.getLayerIndex(layerBefore);
    
    layersToMove = Carmen.Util.set_from_array(node.collect('OlLayer',true));
    for (var i=0; i<layersToMove.length; i++) {
      //console.log('moving ' + layersToMove[i].name + ' from ' + this.map.getLayerIndex(layersToMove[i]).toString() + ' to ' + ((startIndex-1) - i).toString());
      this.map.setLayerIndex(layersToMove[i], (startIndex-1) - i); 
    }
    
    // updating index   
    //Carmen.Application.debugDD(this.layerTree, this.map);
    //obedel profiling
    //console.log("moveGroup Node");
    newParent.updateIndex(true);
  },

  getLastLayerFromGroupNode : function(node) {
    var res = node;
    if (node.hasChildNodes()) {
      var lastChild = node.childNodes[node.childNodes.length-1]; 
      if ('type' in lastChild.attributes) {
        switch(lastChild.attributes.type) {
          case this.NODE_GROUP :
            res = this.getLastLayerFromGroupNode(lastChild);
            break;
          case this.NODE_LAYER :
             res = lastChild.attributes.OlLayer;
             break;
          default:
            res = null;
            break;
        }
      }
    }
    return res;
  },

  getFirstLayerFromGroupNode : function(node) {
    var res = null;
    if (node.hasChildNodes()) {
      var firstChild = node.childNodes[0]; 
      if ('type' in firstChild.attributes) {
        switch(firstChild.attributes.type) {
          case this.NODE_GROUP :
            res = this.getFirstLayerFromGroupNode(firstChild);
            break;
          case this.NODE_LAYER :
             res = firstChild.attributes.OlLayer;
             break;
          default:
            res = null;
            break;  
        }
      }
    }
    return res;
  },

  // not necessary, I think
  updateNodeLayerControl : function(node) {
    var layer = node.attributes.OlLayer;

  },

  moveLayerNode : function(node , oldParent, newParent, indexNode) {
    listNodes = oldParent.childNodes;
    var prevIndex = node.attributes.index;
    
    // hack cos in extJs index Node provided is different 
    // if the node is inserted before or after its previous index  

    var descending = prevIndex<indexNode;
    indexNode = descending ?  indexNode-1 : indexNode;
    
    var bb = node.previousSibling; // bb : brother node before drop point 
    var ba = node.nextSibling;     // ba : brother node after drop point
    var pbb = null; // pbb : previous brother node before, i.e. node before initial position 
    var pba = null; // pb : previous brother node after, i.e. node after initial position
    var i=0;
    while (i<listNodes.length && (pba==null || pbb==null)) {
      if (listNodes[i].attributes.index==prevIndex-1) {
        pbb = listNodes[i];
      }
      else {
        if (listNodes[i].attributes.index==prevIndex+1)
          pba = listNodes[i];
      }
      i++;
    }

    var Lpos_node = node.attributes.layerIdx;
  
    // console.log("listNodes");console.log(listNodes);
    // console.log("node");console.log(node);
    // console.log("bb");console.log(bb);
    // console.log("ba");console.log(ba);
    // console.log("pbb");console.log(pbb);
    // console.log("pba");console.log(pba);
    // console.log("moveNode from " + prevIndex + " to " + indexNode);
      
    // splitting pbb-node if necessary
    if ((pbb != null) && (pbb.attributes.type == this.NODE_LAYER) &&  
      (pbb.attributes.OlLayer == node.attributes.OlLayer) && (
      (!descending && (ba!=null) && 
        ((ba.attributes.type == this.NODE_LAYER && ba.attributes.OlLayer != node.attributes.OlLayer) ||
          ba.attributes.type == this.NODE_GROUP)) ||
      (descending && (bb!=null) && 
        ((bb.attributes.type == this.NODE_LAYER && bb.attributes.OlLayer != node.attributes.OlLayer) ||
          bb.attributes.type == this.NODE_GROUP)))
      ) { 
      //console.log("splitting pbb-node");
      //console.log(node.attributes.OlLayer.subLayers);
      
      var indexSLn = node.attributes.OlLayer.getSubLayerInfo(node.attributes.layerName).index - 1;
      var newLayers = pbb.attributes.OlLayer.split(indexSLn);
      this.linkLayerToNode(newLayers[1], newParent);

      //console.log(node.attributes.OlLayer.subLayers);
      var indexLpbb = this.map.getLayerIndex(pbb.attributes.OlLayer);
      //console.log("indexLn : " + indexLpbb);
      this.map.addLayer(newLayers[1], indexLpbb);
      newLayers[0].redraw();
      newLayers[1].setVisibility(true);
    }
    // splitting node-pba if necessary
    if ((pba != null) && (pba.attributes.type == this.NODE_LAYER) &&  
      (pba.attributes.OlLayer == node.attributes.OlLayer) && ( 
      (!descending && (ba!=null) && 
        ((ba.attributes.type == this.NODE_LAYER && ba.attributes.OlLayer != node.attributes.OlLayer) ||
          ba.attributes.type == this.NODE_GROUP)) ||
      (descending && (bb!=null) && 
        ((bb.attributes.type == this.NODE_LAYER && bb.attributes.OlLayer != node.attributes.OlLayer) ||
          bb.attributes.type == this.NODE_GROUP)))
      ) { 
      //console.log("splitting node-pba");
      //console.log(node.attributes.OlLayer.subLayers);
      var indexSLn = node.attributes.OlLayer.getSubLayerInfo(node.attributes.layerName).index;
      var newLayers = node.attributes.OlLayer.split(indexSLn);
      this.linkLayerToNode(newLayers[1], newParent);
      
      //console.log(node.attributes.OlLayer.subLayers);
      var indexLn = this.map.getLayerIndex(node.attributes.OlLayer);
      //console.log("indexLn : " + indexLn);
      this.map.addLayer(newLayers[1], indexLn);
      newLayers[0].redraw();
      newLayers[1].setVisibility(true);
    }
    
    if (bb!=null) {
      //console.log('bb!=null');
      var Lpos_bb = bb.attributes.layerIdx;
      if (bb.attributes.type == this.NODE_LAYER) {
        if (bb.attributes.OlLayer == node.attributes.OlLayer) {
          // moving node sublayer just after bb sublayer 
          var indexSLbb = bb.attributes.OlLayer.getSubLayerInfo(bb.attributes.layerName).index;
          indexSLn = descending ? indexSLbb : indexSLbb + 1;  
          node.attributes.OlLayer.moveSubLayer(node.attributes.layerName, indexSLn);
        }
        else {// (bb.attributes.olLayer != node.attributes.olLayer)
          //console.log("bb.attributes.olLayer != node.attributes.olLayer");
          // splitting bb-ba if necessary 
          if (ba!=null) {
            if (ba.attributes.type == this.NODE_LAYER) {
              if (ba.attributes.OlLayer == bb.attributes.OlLayer) {
                //console.log("ba.attributes.olLayer == bb.attributes.olLayer");
                //console.log("splitting bb-ba");
                var indexSLba = ba.attributes.OlLayer.getSubLayerInfo(ba.attributes.layerName).index;
                
                //console.log("indexSLba " + indexSLba);
                //console.log(ba.attributes.OlLayer.subLayers);
                var newLayers = ba.attributes.OlLayer.split(indexSLba-1);
                this.linkLayerToNode(newLayers[1], newParent);
                //console.log(ba.attributes.OlLayer.subLayers);
                
                var indexLbb = this.map.getLayerIndex(bb.attributes.OlLayer);
                //console.log("indexLbb " + indexLbb);
                this.map.addLayer(newLayers[1], indexLbb);
                newLayers[0].redraw();
                newLayers[1].setVisibility(true);
                
                var indexLbb = this.map.getLayerIndex(bb.attributes.OlLayer);
                var indexLn = descending ? indexLbb : indexLbb -1;
                this.map.setLayerIndex(node.attributes.OlLayer, indexLn);
              }
              else { // ba.attributes.olLayer != bb.attributes.olLayer
                if (ba.attributes.OlLayer == node.attributes.OlLayer) {
                  //console.log("ba.attributes.olLayer == node.attributes.olLayer");
                  var indexSLba = ba.attributes.OlLayer.getSubLayerInfo(ba.attributes.layerName).index;
                  var indexSLn = descending ? indexSLba - 1 : indexSLba;  
                  node.attributes.OlLayer.moveSubLayer(node.attributes.layerName, indexSLn);
                }
                else {
                  //console.log("ba.attributes.olLayer != node.attributes.olLayer");
                  var indexLbb = this.map.getLayerIndex(bb.attributes.OlLayer);
                  var indexLn = descending ? indexLbb : indexLbb -1;
                  this.map.setLayerIndex(node.attributes.OlLayer, indexLn);
                }
              }
            }
            else { // ba == GROUP
              //console.log("ba==GROUP");
              var indexLbb = this.map.getLayerIndex(bb.attributes.OlLayer);
              var indexLn = descending ? indexLbb : indexLbb -1;
              this.map.setLayerIndex(node.attributes.OlLayer, indexLn);
            }
          } 
          else { // ba == null
            //console.log("ba==null");
            var indexLbb = this.map.getLayerIndex(bb.attributes.OlLayer);
            var indexLn = descending ? indexLbb : indexLbb -1;
            this.map.setLayerIndex(node.attributes.OlLayer, indexLn);
          }
        } 
      }
      else { // (bb.attributes.type == this.NODE_GROUP)
        //console.log("bb.attributes.type == this.NODE_GROUP");
        if (ba != null) {
          if (ba.attributes.type == this.NODE_LAYER) {
            if (ba.attributes.OlLayer == node.attributes.OlLayer) {
              //console.log("ba.attributes.OlLayer == node.attributes.OlLayer");
              var indexSLba = ba.attributes.OlLayer.getSubLayerInfo(ba.attributes.layerName).index;
              var indexSLn = descending ? indexSLba-1  : indexSLba;  
              node.attributes.OlLayer.moveSubLayer(node.attributes.layerName, indexSLn);
            }
            else { // ba.attributes.OlLayer != node.attributes.OlLayer
              //console.log("ba.attributes.OlLayer != node.attributes.OlLayer");
              var indexLba = this.map.getLayerIndex(ba.attributes.OlLayer);
              var indexLn = descending ? indexLba + 1 : indexLba;
              this.map.setLayerIndex(node.attributes.OlLayer, indexLn);
            } 
          }
          else { // (ba.attributes.type == this.NODE_GROUP)
            //console.log("ba.attributes.type == this.NODE_GROUP");
            var indexLbb  = this.map.getLayerIndex(this.getLastLayerFromGroupNode(bb));
            var indexLn = descending ? indexLbb : indexLbb -1;
            this.map.setLayerIndex(node.attributes.OlLayer, indexLn);
          } 
        }
        else { // ba == null && bb != null
          //console.log("ba == null");
          var indexSLbb  = this.map.getLayerIndex(this.getLastLayerFromGroupNode(bb));
          var indexLn = descending ? indexLbb : indexLbb - 1;
          this.map.setLayerIndex(node.attributes.OlLayer, indexLn);
        } 
      } 
    }
    else { // bb == null
      // ascending always
      //console.log("bb == null");
      if (ba != null) {
        if (ba.attributes.type == this.NODE_LAYER) {
          if (ba.attributes.OlLayer == node.attributes.OlLayer) {
            //console.log("ba.attributes.OlLayer == node.attributes.OlLayer");
            var indexSLba = ba.attributes.OlLayer.getSubLayerInfo(ba.attributes.layerName).index;
            //console.log("bb == null && ba.attributes.OlLayer == node.attributes.OlLayer");
            node.attributes.OlLayer.moveSubLayer(node.attributes.layerName, indexSLba);
          }
          else { // (ba.attributes.olLayer != node.attributes.olLayer) 
            //console.log("ba.attributes.OlLayer != node.attributes.OlLayer");
            var indexLba = this.map.getLayerIndex(ba.attributes.OlLayer);
            //var indexLn = indexLba + 1; //indexLba + 1 == indexLbb
            var indexLn = descending ? indexLba + 1 : indexLba;
            this.map.setLayerIndex(node.attributes.OlLayer, indexLn);
          }
        }
        else { // (ba.attributes.type == this.NODE_GROUP) 
          //console.log("ba.attributes.type == this.NODE_GROUP");
          var indexLba  = this.map.getLayerIndex(this.getFirstLayerFromGroupNode(ba));
          //var indexLn = indexLba + 1; //indexLba + 1 == indexLbb
          var indexLn = descending ? indexLba + 1 : indexLba;
          this.map.setLayerIndex(node.attributes.OlLayer, indexLn);
        }
      }
      else { // ba == null
        // the node does not move
      }
    }

    // if the sublayers ordering does not follow 
    // mapfile layers ordering, we need to split layers 
    if (bb != null && bb.attributes.type == this.NODE_LAYER &&
      bb.attributes.OlLayer == node.attributes.OlLayer && 
      bb.attributes.layerIdx < node.attributes.layerIdx) {
      //console.log("splitting bb-node cos mapfile ordering");
      var indexSLbb = bb.attributes.OlLayer.getSubLayerInfo(bb.attributes.layerName).index;
      var newLayers = node.attributes.OlLayer.split(indexSLbb);
      this.linkLayerToNode(newLayers[1], newParent);
      var indexLb = this.map.getLayerIndex(bb.attributes.OlLayer);
      this.map.addLayer(newLayers[1], indexLb);
      newLayers[0].redraw();
      newLayers[1].setVisibility(true);
    } 

    if (ba != null && ba.attributes.type == this.NODE_LAYER &&
      ba.attributes.OlLayer == node.attributes.OlLayer && 
      ba.attributes.layerIdx > node.attributes.layerIdx) {
      //console.log("splitting node-ba cos mapfile ordering");
      //console.log(ba.attributes.OlLayer.subLayers);
      var indexSLba = ba.attributes.OlLayer.getSubLayerInfo(node.attributes.layerName).index;
      //console.log("index : " + indexSLba);
      var newLayers = ba.attributes.OlLayer.split(indexSLba);
      //console.log(ba.attributes.OlLayer.subLayers);
      this.linkLayerToNode(newLayers[1], newParent);
      //console.log(ba.attributes.OlLayer);
      var indexLba = this.map.getLayerIndex(node.attributes.OlLayer);
      //console.log("indexLba : " + indexLba);
      this.map.addLayer(newLayers[1], indexLba);
      newLayers[0].redraw();
      newLayers[1].setVisibility(true);
    } 

    // upadting index   
    //Carmen.Application.debugDD(this.layerTree, this.map);
    // obedel profiling
    //console.log("moveLayer Node");
    newParent.updateIndex(true);
  }, // end of moveNode
  




  ////////////////////////////////////////////////
  // Adding layer nodes dynamically
  ////////////////////////////////////////////////
  
  addLayerNode : function(node, toLegend) {
    // inserting the node in the layer tree    
    var tree = this.getLayerTree();
    if (tree.hasChildNodes()) 
      node = tree.insertBefore(node, tree.childNodes[0]);
    else
      node = tree.appendChild(node);
    
    this.initNode(node);
    
    var legendControl = this.getLegendControl();
    if (toLegend && legendControl) {
      legendControl.addLegendNode(node);
    }
  },
 
  getLegendControl : function() {
     if (this.legendControl == null) {
      var controls = this.map.getControlsByClass('Carmen.Control.Legend');
      this.legendControl = controls.length==0 ? null : controls[0];
    }
    return this.legendControl;
  },
 
  buildLayerNode : function(cfg, atts) {
    var nodeCfg = OpenLayers.Util.extend({},
        OpenLayers.Util.extend(Carmen.Control.LayerTreeManager.LayerNodesConfig["default"].cfg,
        cfg));
    var attributes = OpenLayers.Util.extend({},
        OpenLayers.Util.extend(Carmen.Control.LayerTreeManager.LayerNodesConfig["default"].atts,
        atts));
    nodeCfg.id = Ext.id();
    var node = this.getLayerTree().createNode(nodeCfg);
    node.attributes = attributes;
    return node;  
  },
  
  //////////////////////////////////
  // Context serialization
  //////////////////////////////////
  
  serializeForContext : function(kmlAsString, overideLayerVisibility) {
    //obedel migration todo
    //this.updateNodeOpacityValues();
    var res = { LayerTree : { LayerTreeNode : this.serializeNodeForContext(this.layerTree, overideLayerVisibility) } };
    //var res = { LayerTree : { LayerTreeNode : this.serializeNodeForContext(this.layerTree) } };
    return res; 
  },
  
  serializeNodeForContext : function(node, overideLayerVisibility) {
    var layerTreeNode = {};
    switch (node.attributes.type) {
      case (this.NODE_GROUP) :
        layerTreeNode.attributes = {
          name : node.data.text,
          open : Carmen.Util.bool2IntStr(node.isExpanded()),
          depth : node.getDepth().toString(),
          type : "group",
          opacity : node.attributes.opacity.toString(),
          forceOpacityControl : node.attributes.forceOpacityControl ? "1" : "0"
        };
        // recursivity
        var subNodes = [];
        for (var i=0; i< node.childNodes.length; i++) {
          child = node.childNodes[i];
          //console.log(child.data.text, child.attributes.type);
          //subNodes.push(this.serializeNodeForContext(child));
          subNodes.push(this.serializeNodeForContext(child, overideLayerVisibility));
        }
        if (subNodes.length>0)
          layerTreeNode.LayerTreeNode = subNodes;
      break;
      case (this.NODE_LAYER) :

        var layerVisibility = false;
        if((overideLayerVisibility != null) && (overideLayerVisibility != undefined)) {
          layerVisibility = (overideLayerVisibility.indexOf(node.attributes.layerName) != -1) ? "1" : "0";
        } else {
          layerVisibility = Carmen.Util.bool2IntStr(node.attributes.visible);
        }

        layerTreeNode.attributes = {
          name : node.data.text,
          mapName : node.attributes.layerName,
          depth : node.getDepth().toString(),
          type : 'layer',
          //visible : Carmen.Util.bool2IntStr(layerVisibility),
          visible :layerVisibility,
          displayClass : Carmen.Util.bool2IntStr(node.hasChildNodes()),
          providerGrpId : (this.map.getNumLayers() - this.map.getLayerIndex(node.attributes.OlLayer)).toString(),
          layerId : node.attributes.layerIdx.toString(),
          layerIdx : node.attributes.layerIdx.toString(),
          opacity : node.attributes.opacity.toString(),
          forceOpacityControl : node.attributes.forceOpacityControl ? "1" : "0",
          addedByUser : 'addedByUser' in node.attributes ? "1" : "0",
          bottomLegend : node.attributes.bottomLegend,
          classLegend : node.attributes.classLegend
          // obedel no need for this
          //iconDisplayStrategy : ('iconDisplayStrategy' in node.attributes 
          //  && node.attributes.iconDisplayStrategy) ? "1" : "0"
        };
        if (node.attributes.iconStyle) {
          layerTreeNode.iconStyle = node.attributes.iconStyle;
        } 
      break;
      default:
      break;
    };
    return layerTreeNode;
  },

  getControlByName: function(ctlName) {
    var ctl =null;
    if (this.map) {
      var ctls = this.map.getControlsByClass(ctlName);
      if (ctls.length > 0) {
        ctl = ctls[0];
      }
    }
    return ctl;
  }

});


// handling of treeview 'nodedragover' event
// targetNode : the target node
// position : position among {'append', 'before', 'after'}
// dragData : data relating to the drag operation
// e  : event object for the drag, nor used here
// eOpts : options passed to the listeners, not used here
Carmen.Control.LayerTreeManager.isDropValid = function( targetNode, position, dragData, e, eOpts ) {
    // moving at the root level is not allowed
    if (targetNode.parentNode == null)
      return false;
    // moving is only available between brother nodes, and 
    // it is not allowed to place a node at the same place
    var srcNode = dragData.records[0];
    var indexSrc = srcNode.parentNode.indexOf(srcNode);
    var indexTarget = targetNode.parentNode.indexOf(targetNode);
    //console.log(position + " indexFrom : " + indexSrc + " indexTo : " + indexTarget + " same parent: " +  (srcNode.parentNode == targetNode.parentNode));
    var result = (srcNode.parentNode == targetNode.parentNode) && 
      ((position == 'before' && (indexTarget-indexSrc)!=1) || 
       (position == 'after' && (indexTarget-indexSrc)!=-1));
  return result;
};


//obdel : pb cos targetNode does not corresponds to a nodeInterface object but to a 'nodeView' object
/*
Carmen.Control.LayerTreeManager.handleDrop = function(targetNode, data, overModel, dropPosition, dropHandlers) {
  var node = data.records[0];
  var tree = node.getOwnerTree();
  var oldParent = node.parentNode;
  var newParent = targetNode.parentNode;
  var index = targetNode.parentNode.indexOf(targetNode);
  console.log('moving node beforedrop');
  switch(node.attributes.type) {
    case (this.NODE_GROUP) : 
      this.moveGroupNode(node , oldParent, newParent, index);
    case (this.NODE_LAYER) :
      this.moveLayerNode(node , oldParent, newParent, index);
      break;
    default :
      break;
  } 
};
*/

 
/*   
function(tree, node , oldParent, newParent, index) { 
              switch(node.attributes.type) {
                case (this.NODE_GROUP) : 
                  this.moveGroupNode(tree, node , oldParent, newParent, index);
                  break;
                case (this.NODE_LAYER) :
                  this.moveLayerNode(tree, node , oldParent, newParent, index);
                  break;
                default :
                  break;
              } 
            },
            scope: this,
            delay: 100
          }
          
          function(node, data, overModel, dropPosition, dropHandlers) {
            
          
          
          
          }
*/


Carmen.Control.LayerTreeManager.addTransparencySliderToNode = function(n, initialOpacity) {
  //console.log(node.attributes.text);
  
  var transparencySlider = Ext.create('Ext.Slider.Single', {
    id: node.id + '_slider',
    renderTo: node.id + '_ext',
    width: 75,
    minValue: 0,
    maxValue: 100,
    plugins : new Ext.ux.SliderTip()
  });
  
  transparencySlider.on('change',
    function (slider, newVal) {
      
      var opacity = 1.0 - (newVal/100);
      //node.attributes.opacity = opacity;
      var olLayers = Carmen.Util.set_from_array(this.collect('OlLayer', false));
      for (var i=0; i < olLayers.length; i++) { 
          olLayers[i].setOpacity(opacity);
      }
      
      var layerOrClassNodes = this.collectChildNodes(
        function (n) {
          return ('type' in n.attributes && 
            (n.attributes.type == Carmen.Control.LayerTreeManager.NODE_LAYER ||
             n.attributes.type == Carmen.Control.LayerTreeManager.NODE_CLASS));
        });
      
      var sliderNodes = this.collectChildNodes(
        function (n) {
          return n.attributes.forceOpacityControl;
        });
        
      for (var i=0; i < layerOrClassNodes.length; i++) { 
        var icon = layerOrClassNodes[i].getUI().getIconEl();
        OpenLayers.Util.modifyDOMElement(icon, null, null, null, null, null, null, opacity); 
      }
      
      for (var i=0; i < sliderNodes.length; i++) { 
        var gSlider = Ext.getCmp(sliderNodes[i].id + '_slider');
        gSlider.setValue(newVal,true);
      }
    }, node);
    
  //console.log(node.text + " " + initialOpacity);
  if (initialOpacity) {
    var val = 100 - (initialOpacity * 100);
    transparencySlider.setValue(val,true);
  }
};



Carmen.Control.LayerTreeManager.LayerNodesConfig = {
    'removable' : {
        cfg : {
              leaf : true,
              expanded : false,
              checked : true,
              iconCls : 'cmnLayerTreeLayerNodeIcon',
              visible : true,
              expanded : false,
              opacity : 1.0
        },
        atts : {
          visible: true,
          expanded: false,
          opacity: 1.0,
          forceOpacityControl: true,
          addedByUser: true,
          iconCls : 'cmnLayerTreeLayerNodeIcon',
          removable: true
        }
    },
    'default' : {
        cfg : {
              leaf : true,
              expanded : false,
              checked : true,
              iconCls : 'cmnLayerTreeLayerNodeIcon',
              visible : true,
              expanded : false,
              opacity : 1.0
        },
        atts : {
          visible: true,
          expanded: false,
          opacity: 1.0,
          forceOpacityControl: true,
          addedByUser: true,
          iconCls : 'cmnLayerTreeLayerNodeIcon',
          removable: true
        }
    }

  };
/*
Carmen.Control.LayerTreeManager.LayerNodesOptions = {
    'removable' : {
              rightIconTitle : 'Supprimer la couche',
              rightIconCallback : function(evt) {
                   // removing the layer from the map
                   var layer = this.attributes.OlLayer;
                   var map = layer.map;
                   map.removeLayer(layer);
                   // removing the node from the tree
                   this.remove();
                   evt.stopPropagation();
                   return false;       
                 }
    }
  };  
*/



Carmen.Control.LayerTreeManager.NODE_LAYER = 0;
Carmen.Control.LayerTreeManager.NODE_GROUP = 1;
Carmen.Control.LayerTreeManager.NODE_CLASS = 2;
Carmen.Control.LayerTreeManager.NODE_FEATURE = 3;
Carmen.Control.LayerTreeManager.TREE_NODE_ROOT_LABEL = "Tous les thèmes";

Carmen.Control.LocalStyler = new OpenLayers.Class(OpenLayers.Control, {

   vectorLayer : null,
   id : "LocalStyler",
   // dimension specific styles
   pointStyle : null,
   lineStyle : null,
   polygonStyle : null,

   // default style for init
   defaultStyle : OpenLayers.Feature.Vector.style['default'],

// style stores
   stylePointStore : {
     fields : ['styleDef', 'styleDesc', 'styleName', 'stylePreview'],
     data : [
       [ { graphicName : 'circle', ms_symbolName : 'ms_circle'}, 'disque plein', 'point_style_1', '/IHM/images/sym_annot_circle.png'],
       [ { graphicName : 'square', ms_symbolName : 'ms_square' }, 'carré plein', 'point_style_2', '/IHM/images/sym_annot_square.png'],
       [ { graphicName : 'triangle', ms_symbolName : 'ms_triangle'  }, 'triangle plein', 'point_style_3', '/IHM/images/sym_annot_triangle.png'],
       [ { graphicName : 'x', ms_symbolName : 'ms_cross' }, 'croix (multiplication)', 'point_style_4', '/IHM/images/sym_annot_x.png'],
       [ { graphicName : 'cross', ms_symbolName : 'ms_plus' }, 'croix (addition)', 'point_style_5', '/IHM/images/sym_annot_cross.png'],
       [ { graphicName : 'star', ms_symbolName : 'ms_star' }, 'étoile', 'point_style_6', '/IHM/images/sym_annot_star.png']
     ] },

   styleLineStore : {
     fields : ['styleDef', 'styleDesc', 'styleName', 'stylePreview'],
     data : [
       [ { strokeDashstyle : 'solid', ms_symbolName : 'ms_line_solid' }, 'trait plein', 'line_style_1', '/IHM/images/sym_annot_lineplain.png'],
       [ { strokeDashstyle : 'dot', ms_symbolName : 'ms_line_dot' }, 'pointillés', 'line_style_2', '/IHM/images/sym_annot_linedot.png'],
       [ { strokeDashstyle : 'longdash', ms_symbolName : 'ms_line_dash' }, 'tiretés', 'line_style_3', '/IHM/images/sym_annot_linedash.png'],
       [ { strokeDashstyle : 'dashdot', ms_symbolName : 'ms_line_dashdot' }, 'tiretés - pointillés', 'line_style_4', '/IHM/images/sym_annot_linedashdot.png']
     ] },

   stylePolygonStore : {
     fields : ['styleDef', 'styleDesc', 'styleName', 'stylePreview'],
     data : [
       [ { fillOpacity : 0.4, ms_symbolName : 'ms_area_fill' }, 'surface pleine', 'polygon_style_1', '/IHM/images/sym_annot_areaplain.png'],
       [ { fillOpacity : 0.0, ms_symbolName : 'ms_area_empty' }, 'contour', 'polygon_style_2', '/IHM/images/sym_annot_areaempty.png']
     ] },

   // createNewStyle : if set to true, work on a copy of dimension specific styles 
   // so that features previously drawn with these styles are not modified...
   createNewStyle : true, 
   
   styleRenderer : null,
   
 
   initialize: function(vectorLayer, options) {
     this.vectorLayer = vectorLayer;
     
     OpenLayers.Control.prototype.initialize.apply(this, [options]);
     
     this.pointStyle = OpenLayers.Util.extend({}, this.defaultStyle); 
     this.lineStyle = OpenLayers.Util.extend(
       OpenLayers.Util.extend({}, this.defaultStyle),
       { strokeWidth : 2 });
     this.polygonStyle = OpenLayers.Util.extend({}, this.defaultStyle);
     
     // built style renderer, depends on browser type (IE, gecko...)
     var renderer = OpenLayers.Util.getParameters(window.location.href).renderer;
     renderer = (renderer) ? [renderer] : OpenLayers.Layer.Vector.prototype.renderers; 
     this.styleRenderer = new OpenLayers.Class(OpenLayers.Renderer[renderer[0]], { 
       resolution : 1.0,
       left : 0.0,
       top : 10.0,
         getResolution : function() {
           return 1.0;
         }
      });
     this.initializeUIComponents();
   },

   initializeUIComponents : function() {

     Carmen.ComboStyle = Ext.extend(Ext.form.ComboBox, {
       mode: 'local',
       editable: false,
       typeAhead: true,
       listConfig : {
         loadingText: 'Chargement',
         getInnerTpl: function() {return '<tpl for="."><div id="" ext:qtip="{styleDesc}" class="x-combo-list-item"><img src="{stylePreview}" alt="{styleDesc}"/></div></tpl>';}
       },
       //displayTpl: Ext.create('Ext.XTemplate',
       //'<tpl for="."><div id="" ext:qtip="{styleDesc}" class="x-combo-list-item"><img src="{stylePreview}" alt="{styleDesc}"/></div></tpl>'),
       displayTpl: new Ext.XTemplate(
               '<tpl for=".">' +
                   '{[values["styleDesc"]]}' +
               '</tpl>'
           ),
       forceSelection: true,
       triggerAction: 'all',
       displayField: 'styleDesc',
       valueField: 'styleDef',
       lazyInit: false,
       width: 200,
       styleStoreCfg : null,
       styleRenderer : null,
       initComponent: function() {

       this.doQueryTask = Ext.create('Ext.util.DelayedTask', this.doRawQuery, this);
         this.id = Ext.id(); 
         this.tpl = '<tpl for="."><div id="tpl_' + this.id + '" ext:qtip="{styleDesc}" class="x-boundlist-item"><img src="{stylePreview}" alt="{styleDesc}"/></div></tpl>';

         //Ext.form.ComboBox.superclass.initComponent.call(this);

         this.store = new Ext.data.ArrayStore(this.styleStoreCfg);

         // setting initial combo value as the first one in the store
         this.on('afterrender', function() {
           this.setValue(this.store.getAt(0).data.styleDef); 

         }, this);

         // on change updating the linked style by extending it with combo selected style
         this.on('select', 
           function(c,r,i) {

             nv = r[0].get('styleDef');
             OpenLayers.Util.extend(this.linkedStyle, nv);
             /*if (this.styleRenderer)
               this.updateStylePreview();*/

           }, this);

       },

       updateStylePreview : function (){
         var styleBox = Ext.get('tpl_' + this.id);
         renderer = new this.styleRenderer(styleBox.id);
         var point = new OpenLayers.Geometry.Point(1.0,1.0);
         renderer.drawGeometry(point, OpenLayers.Feature.Vector.style['default'], Ext.id(null, 'style_geom_'));         
       }

      });

     // Point
     this.pointStyler = new Ext.form.FieldContainer({
       fieldLabel : 'Style',
       id: 'oPointShape',
       items : [new Carmen.ComboStyle({ 
         styleStoreCfg : this.stylePointStore, 
         linkedStyle : this.pointStyle,
         styleRenderer : this.styleRenderer })
         ] 
     });

     Carmen.PointColorChooserStyle = Ext.extend(Ext.ux.ColorChooser, {
       linkedStyle : null,
       listeners : {
            //on select, update linkedStyle color
            'select' : {
              fn : function(cp, colorStr) {
                OpenLayers.Util.extend(cp.linkedStyle, { strokeColor : '#' + colorStr, fillColor : '#' + colorStr});
              }
            }   
          },
        id : 'oPointColor'
    });

     this.pointStylerColor = new Carmen.PointColorChooserStyle({ linkedStyle : this.pointStyle });

     Carmen.PointSizeChooser = Ext.extend(Ext.form.TextField, {
       allowDecimals : true,
       decimalSeparator : ".",
       decimalPrecision : 2,
       allowNegative : false,
       minValue : 0,
       maxValue : 100,
       width : 275,
       fieldLabel: 'Taille',
       minText : "La valeur minimale pour ce champ est 0",
       maxText : "La valeur maximale pour ce champ est 100",
       nanText : "Ce champ nécessite un nombre",
       baseChars : "0123456789",
       emptyText : "Taille",
       id : 'oPointSize',
       maxLength : 3,
       maxLengthText : 'La valeur doit être compris entre 0 et 100'
     });
     this.pointSizeChooser = new Carmen.PointSizeChooser(); 

     // Line
     this.lineStyler = new Ext.form.FieldContainer({
       fieldLabel : 'Style',
       id: 'oLineShape',
       items : [new Carmen.ComboStyle({
         styleStoreCfg : this.styleLineStore, 
         linkedStyle : this.lineStyle,
         styleRenderer : this.styleRenderer })]
     });

     Carmen.LineColorChooserStyle = Ext.extend(Ext.ux.ColorChooser, {
        linkedStyle : null,
        listeners : {
             //on select, update linkedStyle color
             'select' : {
               fn : function(cp, colorStr) {
                 OpenLayers.Util.extend(cp.linkedStyle, { strokeColor : '#' + colorStr, fillColor : '#' + colorStr});
               }
             }   
           },
         id : 'oLineColor'
     });

    this.lineStylerColor = new Carmen.LineColorChooserStyle({ linkedStyle : this.lineStyle });

    Carmen.LineThicknessChooser = Ext.extend(Ext.form.TextField, {
      allowDecimals : true,
      decimalSeparator : ".",
      decimalPrecision : 2,
      allowNegative : false,
      fieldLabel : 'Epaisseur',
      width : 275,
      minValue : 0,
      maxValue : 100,
      minText : "La valeur minimale pour ce champ est 0",
      maxText : "La valeur maximale pour ce champ est 100",
      nanText : "Ce champ nécessite un nombre",
      baseChars : "0123456789",
      emptyText : "Epaisseur",
      id : 'oLineThickness',
      maxLength : 3,
      maxLengthText : 'La valeur doit être compris entre 0 et 100'
    });
    this.lineThicknessChooser = new Carmen.LineThicknessChooser();

    // Polygone
    this.polygonStyler = new Ext.form.FieldContainer({
      fieldLabel : 'Style',
      id : 'oPolygonShape',
      items : [new Carmen.ComboStyle({
        styleStoreCfg : this.stylePolygonStore, 
        linkedStyle : this.polygonStyle,
        styleRenderer : this.styleRenderer })]
    });

    Carmen.PolygonColorChooserStyle = Ext.extend(Ext.ux.ColorChooser, {
        linkedStyle : null,
        fieldLabel : 'Remplissage',
        listeners : {
             //on select, update linkedStyle color
             'select' : {
               fn : function(cp, colorStr) {
                 OpenLayers.Util.extend(cp.linkedStyle, { strokeColor : '#' + colorStr, fillColor : '#' + colorStr});
               }
             }   
           },
         id : 'oPolygonColor'
     });

    this.polygonStylerColor = new Carmen.PolygonColorChooserStyle({ linkedStyle : this.polygonStyle });
    this.polygonStylerColor.colorBtn.setTooltip('Modifier couleur du remplissage');
    
    Carmen.PolygonEdgeColorChooser = Ext.extend(Ext.ux.ColorChooser, {
      linkedStyle : null,
      fieldLabel : 'Contour',
      listeners : {
      'select' : {
        fn : function(cp, colorStr) {
        OpenLayers.Util.extend(cp.linkedStyle, { strokeColor : '#' + colorStr, fillColor : '#' + colorStr});
        }
      }
      },
      id: 'oPolygonEdgeColor'
    });

    this.polygonEdgeColorChooser = new Carmen.PolygonEdgeColorChooser({ linkedStyle : this.polygonStyle });
    this.polygonEdgeColorChooser.colorBtn.setTooltip('Modifier couleur du contour');

    Carmen.PolygonEdgeSizeChooser = Ext.extend(Ext.form.TextField, {
      allowDecimals : true,
      decimalSeparator : ".",
      decimalPrecision : 2,
      allowNegative : false,
      fieldLabel: 'Epaisseur',
      width : 275,
      minValue : 0,
      maxValue : 100,
      minText : "La valeur minimale pour ce champ est 0",
      maxText : "La valeur maximale pour ce champ est 100",
      nanText : "Ce champ nécessite un nombre",
      baseChars : "0123456789",
      emptyText : "Epaisseur",
      id : 'oPolygonEdgeSize',
      maxLength : 3,
      maxLengthText : 'La valeur doit être compris entre 0 et 100'

    }); 
    this.polygonEdgeSizeChooser = new Carmen.PolygonEdgeSizeChooser();

    // Build window
    this.styleWindow = new Ext.Window({
      id : 'styleWindow_' + this.displayClassName, 
      layout: 'fit',
      //width: 365,
      //height: 350,
      width: 515, //Modified
      height: 500, //Modified
      constrain : true,
      plain: true,
      title: 'Styles des annotations',
      modal:false,
      autoDestroy :true,
      resizable:true,
      closeAction: 'hide',
      shadow : false,
      hidden: true,
      autoScroll: true,
      items: [{
         xtype : 'form',
         fitToFrame: true,
         bodyStyle:'padding:10px 10px 10px 10px; color: #000000;',
         labelStyle: 'color: #000000;',
         monitorValid : true,
         defaults: {
           labelWidth: 200
         },
         items : [
         {
      xtype: 'fieldset',
      id: 'fieldsetRequestPoints',
      title: 'Points',
      items: [
        this.pointStyler,
        this.pointSizeChooser,
        this.pointStylerColor
      ]
     },
     {
      xtype: 'fieldset',
      id: 'fieldsetRequestLines',
      title: 'Lignes',
      items: [
        this.lineStyler,
        this.lineThicknessChooser,
        this.lineStylerColor
      ]
     },
     {
      xtype: 'fieldset',
      id: 'fieldsetRequestPolygones',
      title: 'Polygones',
      items: [
        this.polygonStyler,
        this.polygonEdgeSizeChooser,
        this.polygonStylerColor,
        this.polygonEdgeColorChooser
      ]
     }],
         buttons : [{
          id: 'btnStyleValid_' + this.displayClassName,
          formBind : true,
          text: 'Valider',
          listeners : {
            'click' : {
              fn : function() {
                //this.vectorLayer.updateGeometryDefaultStyle(this.pointStyle, this.lineStyle, this.polygonStyle);
                //this.styleWindow.hide();
                var layer = app.map.getLayersBy("name", actualNodeId)[0];
                var hexToRgb = /^([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i;

                //Point style
                var oPointShape      = Ext.getCmp("oPointShape").items.get(0).value.ms_symbolName;
                var oPointColor      = Ext.getCmp("oPointColor").color;
                oPointColor          = hexToRgb.exec(oPointColor);
                oPointColor          = parseInt(oPointColor[1], 16) + "," +
                                       parseInt(oPointColor[2], 16) + "," +
                                       parseInt(oPointColor[3], 16);
                var oPointSize       = Ext.getCmp("oPointSize").getValue();

                //Line style
                var oLineShape       = Ext.getCmp("oLineShape").items.get(0).value.ms_symbolName;
                var oLineColor       = Ext.getCmp("oLineColor").color;
                oLineColor           = hexToRgb.exec(oLineColor);
                oLineColor           = parseInt(oLineColor[1], 16) + "," +
                                       parseInt(oLineColor[2], 16) + "," +
                                       parseInt(oLineColor[3], 16);
                var oLineThickness   = Ext.getCmp("oLineThickness").getValue();

                //Polygon style
                var oPolygonShape     = Ext.getCmp("oPolygonShape").items.get(0).value.ms_symbolName;
                var oPolygonColor     = Ext.getCmp("oPolygonColor").color;
                oPolygonColor         = hexToRgb.exec(oPolygonColor);
                oPolygonColor         = parseInt(oPolygonColor[1], 16) + "," +
                                        parseInt(oPolygonColor[2], 16) + "," +
                                        parseInt(oPolygonColor[3], 16);
                var oPolygonEdgeColor = Ext.getCmp("oPolygonEdgeColor").color;
                oPolygonEdgeColor     = hexToRgb.exec(oPolygonEdgeColor);
                oPolygonEdgeColor     = parseInt(oPolygonEdgeColor[1], 16) + "," +
                                        parseInt(oPolygonEdgeColor[2], 16)+ "," +
                                        parseInt(oPolygonEdgeColor[3], 16);
                var oPolygonEdgeSize  = Ext.getCmp("oPolygonEdgeSize").getValue();

                var overwriteUrl = window.CARMEN_URL_SERVER_FRONT + '/services/AddLocalData/AddLocalData.php?';
               //Appel au service overwriteMapfile
                Ext.Ajax.request({
                  url: overwriteUrl,
                  method: "POST",
                  params: {
                    "service"           : "overwriteMapfile",
                    "oPointShape"       : oPointShape,
                    "oPointColor"       : oPointColor,
                    "oPointSize"        : oPointSize,
                    "oLineShape"        : oLineShape,
                    "oLineColor"        : oLineColor,
                    "oLineThickness"    : oLineThickness,
                    "oPolygonShape"     : oPolygonShape,
                    "oPolygonColor"     : oPolygonColor,
                    "oPolygonEdgeColor" : oPolygonEdgeColor,
                    "oPolygonEdgeSize"  : oPolygonEdgeSize,
                    "oMapName"          : actualNodeId
                  },
                  success: function(response){
                    this.vectorLayer.redraw(true);
                    this.layerTreeNode.data.icon +=  "?" + new Date().getTime();
                    var tree = this.layerTreeNode.getOwnerTree();
                    if (tree) {
                      var view = tree.getView();
                      view.refreshNode(this.layerTreeNode);
                     }
                    //var addedLayer = layer.map.layerTreeManager.layerTree.findChildByAttribute("layerName", actualNodeId);
                    //var addedLayerIcon = Ext.get(addedLayer.ui.iconNode.id);
                    //addedLayerIcon.dom.src = addedLayerIcon.dom.src + "?" + new Date().getTime();
                  },
                  failure: function(response){
                    console.log("failure");
                    console.log(response);
                  },
                  scope: this
                });
                this.styleWindow.hide();
              },
              scope: this
            } 
          }
         }] 
       }],
     listeners : {
       'hide' : {
         fn : function() { 
           this.deactivate(); 
         },
         scope : this
       }
     }
   });
   },

  activate : function(layer, node) {
    this.vectorLayer = layer;
    this.layerTreeNode = node;
    var state = OpenLayers.Control.prototype.activate.apply(this, arguments);
    if (state) {
      this.styleWindow.show();
    }
    return state;
  },
  
  deactivate : function() {
    var state = OpenLayers.Control.prototype.deactivate.apply(this, arguments);
    if (state)
      this.styleWindow.hide();
    return state;
  },
  displayClassName: "CmnControlLocalStyler",
  CLASS_NAME: "Carmen.Control.LocalStyler"
});
