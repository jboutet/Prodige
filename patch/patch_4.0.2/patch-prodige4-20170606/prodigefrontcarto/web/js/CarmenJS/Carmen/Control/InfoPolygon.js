  /**
 * @_requires OpenLayers/Controljs
 */

/**
 * Class: Carmen.Control.Info
 
 * Inherits from:
 *  - <OpenLayers.Control>
 */

Carmen.Control.InfoPolygon = new OpenLayers.Class(Carmen.Control.InfoCircle, {

  // UI components
  btn: new Ext.Button({
      tooltip: 'Information sur zone polygonale',      
      tooltipType: 'title',
      cls: 'x-btn-icon', /* cmn-tool-infopolygon',*/
      enableToggle: true,
      toggleGroup: 'mainMapControlInfo',
      disabled: false,
      text : '<span class="fa-stack fa-lg"><i class="fa icon-square fa-stack-2x"></i><i class="fa fa-info fa-stack-1x"></i></span>',
      width: ctrlWidth,
      height: ctrlHeight
  }),
  btnGrpDefaultActiv : false,
    /**
   * Method: draw
   */    
  draw: function() {
      this.handler = new OpenLayers.Handler.Polygon(this,
	  { 
		done: function(res) { 
		  //console.log("polygon finished"); 
		  //console.log(res);
		  this.info(res.toString());
		}
	  }, 
	  {keyMask: this.keyMask, persist: false} );
  },
  
  buildParams : function(nodes, position, fieldsFilter, mapfile) {
    
    var layerNames = [];
    var fieldsList = [];
    var briefFieldsList = [];
    var baseQueryURLList = [];
    
    for (var i=0; i<nodes.length; i++) {
      if(typeof(nodes[i].attributes.infoFields)!="undefined"){
        layerNames.push(nodes[i].attributes.layerName);
        if (typeof(nodes[i].attributes.briefFields)!="undefined" ){ 
          briefFieldsList.push(nodes[i].attributes.briefFields);
        }  
        fieldsList.push(nodes[i].attributes.infoFields);
        baseQueryURLList.push(nodes[i].attributes.baseQueryURL);
      }
    }
    
    var fieldsDesc = new Array();
    for (var i=0; i<fieldsList.length; i++) {
      var list = Carmen.Util.chompAndSplit(fieldsList[i],';');
      fieldsDesc[i] = new Array();
      for (var j=0; j<list.length; j++) {
        var desc  = Carmen.Util.decodeCarmenFieldDesc(list[j], baseQueryURLList[i]);
        fieldsDesc[i][j]=desc;   
      }
      if (fieldsFilter) 
        fieldsDesc[i] = Carmen.Util.array_filter(fieldsDesc[i], fieldsFilter);
    }

    var briefFieldsDesc = new Array();
    for (var i=0; i<briefFieldsList.length; i++) {
      var list = Carmen.Util.chompAndSplit(briefFieldsList[i],';');
      briefFieldsDesc[i] = new Array();
      for (var j=0; j<list.length; j++) {
        var desc  = Carmen.Util.decodeCarmenFieldDesc(list[j], baseQueryURLList[i]);
        briefFieldsDesc[i][j]=desc;   
      }
      if (fieldsFilter) 
		briefFieldsDesc[i] = Carmen.Util.array_filter(briefFieldsDesc[i], fieldsFilter);
    }

    // linking kept info fields and brief fields 
    // to the corresponding layer node in the tree
    for (var i=0; i<nodes.length; i++) {
   	  nodes[i].attributes.infoFieldsDesc = fieldsDesc[i];
   	  nodes[i].attributes.briefFieldsDesc = briefFieldsDesc[i];
    }

    var params = {
      layers : Ext.util.JSON.encode(layerNames),
      fields : Ext.util.JSON.encode(fieldsDesc),
      briefFields : Ext.util.JSON.encode(briefFieldsDesc),
      shape : position,
      map : mapfile,
      showGraphicalSelection : this.showGraphicalSelection,
      shapeType : 'polygon' }
      
    return params;    
  },
  
  
  CLASS_NAME: "Carmen.Control.InfoPolygon"

});
  

