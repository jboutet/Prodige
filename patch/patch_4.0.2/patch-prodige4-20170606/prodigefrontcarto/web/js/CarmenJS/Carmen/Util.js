Carmen = window.Carmen || {};
/**
 * Namespace: Util
 */
Carmen.Util = {};

/** 
 * Various Constants
 */

// force use of native json encoding to prevent ext unicode translation in JSON.encode
//Ext.USE_NATIVE_JSON = true;
Ext.JSON.encode = function(o) { if (JSON && JSON.stringify) { return JSON.stringify(o) } else { Ext.JSON.encodeValue(o); } };

// DOTS_PER_INCH
// constant transmitted to OpenLayers
Carmen.DOTS_PER_INCH = 96;
// constant transmitted to Mapserver
Carmen.Mapserver_DOTS_PER_INCH = 96;

// EROOR CODES (not used in final version or ?)
Carmen.CARMEN_ERROR = 256;
Carmen.CARMEN_ERROR_SESSION_EXPIRED = 257;
// size in pixels of a wms legend that may contains symbology and textual description 
Carmen.Util.WMS_FULL_LEGEND_MIN_WIDTH = 82;
Carmen.Util.WMS_FULL_LEGEND_MIN_HEIGHT = 40;

Carmen.Util.WMSC_RESOLUTIONS = [
  2113.267326,
  1056.633663,
  528.3168317,
  264.1584158,
  132.0792079,
  66.03960396,
  26.41584158,
  13.229166668,
  6.614583334,
  4.630208105,
  2.645833334,
  1.322916667,
  0.661458333];

Carmen.Util.MAP_RESOLUTIONS = [
   156543.03395208137,
  78271.516843749,
  39135.758554166176,
  19567.879277083088,
  9783.939506249877,
  4891.969885416605,
  2445.984810416636,
  1222.9925374999846,
  611.4962687499923,
  305.7480020833295,
  152.8741333333314,
  76.4370666666657,
  38.21853333333285,
  19.109266666666425,
  9.554633333333213,
  4.777316666666606,
  2.388658333333303,
  1.1943291666666516,
  0.5971645833333258,
  0.2984499999999962,
  0.1492249999999981,
  0.07461249999999905];
  

/**
 * hack for Openlayers.Util.getScaleFromResolution since factor doesn't match
 * 
 */
Carmen.Util.getScaleFromResolution = function(res, unit) {
  var scale = OpenLayers.Util.getScaleFromResolution(res, unit);
  if (scale != null) {
    //Hack based on experimentations
    scale = 1.0/(156543.0339 / 147923.76911893254) * scale; 
  }
  return scale;
}

Carmen.SpecialChars = {
  "around" : String.fromCharCode(0x02248) 
}; 
  
Carmen.Util.INFO_TOOL_XY_TOLERANCE = 5;  
Carmen.Util.initLegend = function() {  
  this.context = new Carmen.JSONContext(jsOws);
  var ctx = this.context;

  // Layer Tree Manager
  var layerTreeManager = new Carmen.Control.LayerTreeManager();
  var jsonLayerTree = ctx.mdataMap.LayerTree;
  var jsonLayerList = ctx.layer;
  layerTreeManager.buildTree(jsonLayerTree, jsonLayerList);
  
  // legend
  var legend = new Carmen.Control.Legend();
  legend.layerTreeManager = layerTreeManager;
  legend.renderHTMLto("legendDiv");
  submitForm();
}
/*
 * wrapping functions between OpenLayers control handlers and ExtJS button
 * handlers
 */
Carmen.Util.activeControl = null;
Carmen.Util.buildExt2olHandlerToggle = function(olControl) {
	return function(extBtn, state) {
    //console.log('State : ' + (state ? "activation" : "desactivation"));
    //console.log(olControl);
    //console.log(Carmen.Util.activeControl);
    
    if ( Carmen.Util.activeControl && Carmen.Util.activeControl!=olControl && !state){
      return;
    }
    
	  if ( Carmen.Util.activeControl && Carmen.Util.activeControl!=olControl ){
      //console.log('cas 1');
      Carmen.Util.activeControl.state = false;
      Carmen.Util.activeControl.deactivate();
    }
    if (state && Carmen.Util.activeControl!=olControl ){
      //console.log('cas 2');
      Carmen.Util.activeControl = olControl;
    }
    // desactivation volontaire de l'outil courant
    if ( Carmen.Util.activeControl && Carmen.Util.activeControl==olControl && !state){
      //console.log('cas 3');
      Carmen.Util.activeControl.deactivate();
      Carmen.Util.activeControl=null;
    }
    if ( Carmen.Util.activeControl ){
      //console.log('cas 4');
      Carmen.Util.activeControl.activate();
      Carmen.Util.activeControl.active = true;
    }
        
    //extBtn.pressed = state;
    //console.log('fin');
    //console.log(Carmen.Util.activeControl);
	}
};

Carmen.Util.buildExt2olHandlerButton = function(olControl) {
	return function() {
		olControl.trigger();
	}
};


/*
 * AJAX error handling function
 */ 
Carmen.Util.handleAjaxFailure = function(response, callerName, skipExceptSession) {
  callerName = Ext.valueFrom(callerName, "Carmen", false);
  skipExceptSession = Ext.valueFrom(skipExceptSession, false, false);
  
  if (response) {
    var response = response.responseText ? Ext.decode(response.responseText) : response;
    if (response.errmsg) {
      var failureType = Ext.valueFrom(response.failureType, 0, false);  
      // special handling for SESSION_EXPIRED error 
      if (failureType == Carmen.CARMEN_ERROR_SESSION_EXPIRED)
        Ext.Msg.alert(callerName, response.errmsg, 
          function() {
            top.window.location.reload();
          });
      else if (!skipExceptSession) 
        Ext.Msg.alert(callerName, response.errmsg + " (" + response.failureType + ")");
    }
    else if (!skipExceptSession)
      Ext.Msg.alert(callerName, 'Service indisponible');
  }
  else if (!skipExceptSession)
    Ext.Msg.alert(callerName, 'Service indisponible');
};







/*
 * layer listener to watch layer loading
 */
Carmen.Util.olRegisterProfilerEvents = function(layer) {
	layer.logEvent = function(event) {
	}

	layer.events.register("loadstart", layer, function() {
				this.logEvent("Load Start");
			});

	layer.events.register("tileloaded", layer, function() {
				this
						.logEvent("Tile loaded. " + this.numLoadingTiles
								+ " left.");
			});

	layer.events.register("loadend", layer, function() {
				this.logEvent("Load End. Grid:" + this.grid.length + "x"
						+ this.grid[0].length);
			});
};

Carmen.Util.getTimeStamp = function() {
	var date = new Date();
	var timeParts = [date.getHours(), date.getMinutes(), date.getSeconds()];
	var timeStamp = timeParts.join(":");
	return timeStamp;
};

/*
 * Miscellaneous function
 */

Carmen.Util.openHelp = function() {
	window.open('http://carmen.ecologie.gouv.fr', 'Help',
			"location=no, toolbar=no, scrollbars=yes");
}

Carmen.Util.openWindow = function(title, url) {
	window.open(url, title, "location=no, toolbar=no, scrollbars=yes");
};

Carmen.Util.openWindowExt = function(title, url, autosize) {
	// could not work when accessing page which are not under the same domain
	var win = new Ext.Window({
				layout : 'fit',
				width: autosize ? 'auto' : 550,
        height: autosize ? 'auto' : 500,
        constrain : true,
        plain : true,
        title : title,
        header : (title!=null) && (title!=''),
				modal : false,
				autoDestroy : true,
				resizable : true,
				loader	: {
            autoLoad	: true,
            url		: url,
            scripts	: false
        }
			});
	win.show();
};

// iframe extjs component taken from
// https://yui-ext.com/forum/showthread.php?p=298080
Ext.define('Ext.ux.IFrameComponent', {
    extend: 'Ext.Component',
    alias: 'Ext.ux.IFrameComponent',
    onRender: function(ct, position){
        this.el = ct.createChild({
            tag: 'iframe',
            id: 'iframe-' + this.id,
            name : this.name,
            width : '100%',
            height: '100%',
            frameBorder: 0,
            src: this.url
        });
    }
});
 
Carmen.Util.openWindowIframe = function(title, url, autosize, width, height) {
  var win = new Ext.Window({
        layout : 'fit',
        width: autosize ? 'auto' : ( width ? width : 550 ),
        height: autosize ? 'auto' : ( height ? height : 500 ),
        constrain : true,
        plain : true,
        title : title,
        header : (title!=null) && (title!=''),
        modal : false,
        autoDestroy : true,
        resizable : true,
				maximizable : true,        
        items : [ 
          
            { 
              xtype: 'uxiframe',
              src: url
              /*
              loader	: {
                  autoLoad	: true,
                  url		: url,
                  scripts	: false
              }*/
            }
          
        ]        
      });
   win.on('maximize',
    function (winElt) {
      //alert('here' + url + '  ' + title);
      winElt.close();
      window.open(url);
    },
    win);

  win.show();
};

// force an horizontal scrollbar on the toolbar tb 
// if necessary (tb content trunked) 
Carmen.Util.forceScrollOnToolbarInIE = function(tb) {
  var scrollHeight = tb.getEl().dom.scrollHeight;
  var clientHeight = tb.getEl().dom.clientHeight;
  var viewWidth = tb.getEl().getComputedWidth();
  var tbWidth = tb.getEl().first().getComputedWidth();
  var style = { 
    paddingBottom : "2px",
    overflowX: "auto" }; 
  if (scrollHeight>clientHeight || tbWidth>viewWidth) {
    style.overflowX = "scroll";
//    if (Ext.isIE7)
//      style.paddingBottom = "19px";
  }
  tb.getEl().applyStyles(style);
};

/*
 * Miscellaneous function
 */

Carmen.Util.openDownloadData = function() {
	winDownloadData = new Ext.Window( {
		layout :'fit',
		width : 770,
    constrain : true,
		plain :true,
		title :'Téléchargement',
		modal :false,
		x :150,
		y :100,
		id :'downloadWindow',
		autoDestroy :true,
		// monitorValid:true,
		resizable :true,
		autoLoad : {
			url :'/services/GetDownloadData/getLicence.php',
			scripts :true
		}
	});
	
	winDownloadData.show();

};

function myEditFunction (){
	alert("ok");
};


Carmen.Util.doAjax = function(url, MsgBoxTitle, useClassicAlert) {
	Ext.Ajax.request( {
		url :url,
		method : "GET",
		params : {
			action :'getMsg'
		},
		success : function(result, request) {
			response = Ext.decode(result.responseText);
			if (useClassicAlert)
				alert(response.msg);
			else
				Ext.MessageBox.alert(MsgBoxTitle, response.msg);
		},
		failure : function(result, request) {
			if (useClassicAlert)
				alert("Échec de la procédure");
			else
				Ext.MessageBox.alert('Erreur', 'Échec de la procédure');
		}
	});
}


/*
 * UTF-8 useful functions
 */
Carmen.Util.encode_utf8 = function(s) {
	return unescape(encodeURIComponent(s));
};

Carmen.Util.decode_utf8 = function(s) {
	return decodeURIComponent(escape(s));
};

Carmen.Util.array_find = function(arr, value) {
	var i = -1;
	while (i < arr.length && arr[i] != value) {
		i++;
	}
	return i;
};


Carmen.Util.json2xml = function(jsonObj, header) {
	return json2xml(jsonObj, null, header);
}

function json2xml(o, tab, header) {
	var toXml = function(v, name, ind) {
		var xml = "";
			if(typeof v != 'undefined'){
		if (v instanceof Array) {
			for ( var i = 0, n = v.length; i < n; i++)
				xml += ind + toXml(v[i], name, ind + "\t") + "\n";
		} else if (typeof (v) == "object") {
			var hasChild = false;
			xml += ind + "<" + name;
			for ( var m in v) {
				if (m.charAt(0) == "@")
					xml += " " + m.substr(1) + "=\"" + Carmen.Util.htmlspecialchars(v[m].toString()) + "\"";
				else if (m=="attributes") {
          for (n in v[m]) {
		        	if(typeof v[m][n] == 'undefined'){
		        		v[m][n]="";
		        	}
            xml += " " + n.toString() + '="' + Carmen.Util.htmlspecialchars(v[m][n].toString()) + '"';	
          }
        } else
					hasChild = true;
			}
			xml += hasChild ? ">" : "/>";
			if (hasChild) {
				for ( var m in v) {
					if (m == "#text")
						xml += v[m];
					else if (m == "#cdata")
						xml += "<![CDATA[" + v[m] + "]]>";
					else if (m.charAt(0) != "@" && m != "attributes")
						xml += toXml(v[m], m, ind + "\t");
				}
				xml += (xml.charAt(xml.length - 1) == "\n" ? ind : "") + "</"
						+ name + ">";
			}
		} else {
			xml += ind + "<" + name + ">" + Carmen.Util.htmlspecialchars(v.toString()) + "</" + name + ">";
		}
			}
		return xml;
	};
	var xml = header ? header : "";
	
	for ( var m in o)
		xml += toXml(o[m], m, "");
	return tab ? xml.replace(/\t/g, tab) : xml.replace(/\t|\n/g, "");
}

Carmen.Util.htmlspecialchars = function(str) {
	str = str.replace(/&/g,'&amp;');
	str = str.replace(/"/g,'&quot;');
	str = str.replace(/>/g,'&gt;');
	str = str.replace(/</g,'&lt;');
	return str;
}

Carmen.Util.unhtmlspecialchars = function(str) {
	str = str.replace(/&amp;/g,'&');
	str = str.replace(/&quot;/g, '"');
	str = str.replace(/&gt;/g, '>');
	str = str.replace(/&lt;/g, '<');
	return str;
}


// TODO a revoir
Carmen.Util.olInherits = function(daughter, mother) {

	var res = (daugther.prototype.CLASS_NAME == mother.prototype.CLASS_NAME);
	if ('parent' in daughter && !res) {
		res = Carmen.Util.olInherits(daughter.parent, mother);
	}
	return res;
};


Carmen.Util.chompAndSplit = function(str, sep) {
  // chomp
  //console.log('str in cas : ' + str);
  if ((typeof str !== "undefined") && str.length>0 && str.substr(str.length-1,1)==sep) {
   str = str.substr(0,str.length-1);
  }
  // and split
  return (str && str.split ? str.split(sep) : []);
};

Carmen.Util.decodeCarmenFieldDesc = function(fieldDesc, baseURL) {
  var arrFieldDesc = Carmen.Util.chompAndSplit(fieldDesc, '|');

  if (baseURL=="null")
    baseURL = '';
  
  return arrFieldDesc.length>=3 ? {
       name : arrFieldDesc[0],
       alias : arrFieldDesc[1],
       type : arrFieldDesc[2],
       // if no URL template (arrFieldDesc[4]) given,
       // putting entire field (arrFieldDesc[0]) in it 
       url : (arrFieldDesc[2] != 'URL' && arrFieldDesc[2] != 'IMG' && arrFieldDesc[2] != 'Image') ? null :
         (arrFieldDesc.length>3 ? baseURL + arrFieldDesc[3] : baseURL + '<'+ arrFieldDesc[0] +'>')
      } : null;
};                     

Carmen.Util.carmenTypeToExtType = function(carmenStr) {
  var res = 'auto';
  switch(carmenStr) {
    case 'TXT' :
      res = 'string';
      break;
    case 'URL','IMG','Image' : 
      res= 'auto';
      break;  
    case 'AAAAMMJJ', 'AAAAJJMM', 'MMJJAAAA','JJMMAAAA' :
      res = 'date';
        break;
      default :
        break;         
    }
    return res;
};

Carmen.Util.fieldFilter_URL = function(field) {
	return field.type=='URL';
};
  
Carmen.Util.fieldFilter_TXT = function(field) {
	return field.type=='TXT';
};

Carmen.Util.carmenDateTypeToExtDateFormat = function(carmenStr) {
  var res = 'd-m-Y';
  switch(carmenStr) {
    case 'AAAAMMJJ' :
      res = 'Y-m-d';
      break;
    case 'AAAAJJMM' :
      res = 'Y-d-m';
      break;        
    case 'MMJJAAAA' :
      res = 'm-d-Y';
      break;
    case 'JJMMAAAA' :
      res = 'd-m-Y';
        break;                
      default :
        break;         
    }
    return res;
};

Carmen.Util.textRenderer = function(v) {
  var html =  '<span>' + v + '</span>';
  return html;
};

Carmen.Util.URL2Html = function(v) {
  // url special handling to protect special chars éèâ... and avoid encoding pbs
  var url_info = Carmen.Util.parseURL(unescape(v.href || v.text));
  var url = url_info==null ? v.href || v.text : url_info.protocol + '://' + url_info.host + '/' + url_info.mid + url_info.end;
  
  /*
  var html =  '<u><span onclick="javascript:Carmen.Util.openWindowIframe(' +  
    '\'Informations détaillées\', \'' + 
    url + '\');" >' + v.text + '</span></u>';*/
  var html =  '<u><span onclick="javascript:window.open(\'' +  
    url + '\', \'_blank\');" >' + v.text + '</span></u>';
  return html;
};

Carmen.Util.URLRenderer = function(v) {
  var html =  '<span class="cmnLayerTreeLayerNodeMetadata">' + Carmen.Util.URL2Html(v) + '</span>';
  return html;
};

Carmen.Util.imgRenderer = function(src) {
  var html =  '<span>Aucune image associée</span>';
  if (src!=null && src!='')
    // url special handling to protct special chars éèâ... and avoid encoding pbs
    var url_info = Carmen.Util.parseURL(unescape(src));
	var url = url_info==null ? src : url_info.protocol + '://' + url_info.host + '/' + url_info.mid + url_info.end;
    html = '<span class="cmnImgRenderer"><img src="'+ 
      url + '" onclick="javascript:window.open(\'' +  
      url + '\', \'_blank\');" /></span>';
    return html;
};


Carmen.Util.extentRenderer = function(v) {
   return '<div class="cmnInfoGridZoomIcon"> </div>';
};

Carmen.Util.carmenTypeToExtRenderer = function(carmenStr) {
  var res = Carmen.Util.textRenderer;
  switch(carmenStr) {
    case 'AAAAMMJJ', 'AAAAJJMM', 'MMJJAAAA','JJMMAAAA' :
      res = Ext.util.Format.dateRenderer('d/m/Y');
      break;
    case 'URL' :
      res = Carmen.Util.URLRenderer;
      break;
    case 'IMG' :
    case 'Image' :
      res = Carmen.Util.imgRenderer;
      break;
    default :
      break;         
  }
  return res;
};


Carmen.Util.carmenFontDescToFontStyles = function(fontDesc) {
  var desc = {}; 
  desc.font = fontDesc;
  var t = fontDesc.match(/[^-]+/g); 
  if (t!=null) {
    desc.font =  t[0];
    for (var i=1; i<t.length; i++) {
	    switch(t[i].toLowerCase()) {
	    	case  "gras" : 
					desc.fontWeight = 'bold'
		   		break;
	     	case  "italique" :
	     		desc.fontStyle = 'italic'
	     		break; 
	    }
		}    
  }
  return desc;
}



Carmen.Util.clone = function(obj) {
  var cloned = {};
  for (a in obj) {
  	cloned[a] = obj[a];
  } 
  return cloned;
  // return eval(uneval(obj));
};


Carmen.Util.strExtentToOlBounds = function(extentStr, punctualBufferRadius) {
  var tabExtent = extentStr.substr(1,extentStr.length-1).split(",");
  for (var i=0; i<4; i++)
    tabExtent[i] = parseFloat(tabExtent[i]); 
  if (punctualBufferRadius) {  
    // in case of extent limited to a point, we extend the extent
	  if (tabExtent[1]==tabExtent[3]) {
	    tabExtent[1] = tabExtent[1] - punctualBufferRadius;
	    tabExtent[3] = tabExtent[3] + punctualBufferRadius;
	  }
	  if (tabExtent[2]==tabExtent[4]) {
	    tabExtent[2] = tabExtent[2] - punctualBufferRadius;
	    tabExtent[4] = tabExtent[4] + punctualBufferRadius;
	  }  
  }  
  return new OpenLayers.Bounds(tabExtent[0],
          tabExtent[1],
          tabExtent[2],
          tabExtent[3]
        );
};

Carmen.Util.positionToStr = function(position) {
  var str = "";
  if (position instanceof OpenLayers.Bounds) {
    str = '(' + position.left + ',' + position.bottom + ',' + position.right + ',' + position.top + ')';  
  }
  else if (position instanceof OpenLayers.LonLat) {
    str = '(' + position.lon + ',' + position.lat + ')';
  }
  else if (position instanceof OpenLayers.Pixel) {
    str = '(' + position.x + ',' + position.y + ')';
  }
  return str; 
};

Carmen.Util.circleToStr = function(circle) {
  var str = "";
  // test if we have a circle 
  if (circle.radius!=undefined && circle.origin!=undefined) {
    str = '(' + circle.origin.x + ',' + circle.origin.y + ',' + circle.radius + ')';
  }
  return str; 
};

Carmen.Util.emptyStore =  Ext.create('Ext.data.ArrayStore', {
          fields: [],
          data : []});


//obedel debug ext 2.2.1 -> 3.0.0
//Carmen.Util.emptyColumnModel = new Ext.grid.ColumnModel({
//        columns: []});
        
Carmen.Util.convertContextBBtoolBounds = function(bb) {
  
  return new OpenLayers.Bounds(bb.attributes.minx, bb.attributes.miny, bb.attributes.maxx, bb.attributes.maxy);
};


// Miscellaneous functions
// array_filter : return an array composed of 
// elements of aray satisfying filter test
Carmen.Util.array_filter = function (aray, filter) {
	var res = [];
	for(var i=0; i<aray.length;i++) {
		if (filter(aray[i])) {
			res.push(aray[i]);
		}
	}
	return res;
}


// array_map : return an array where f 
// has been applied to each elt of aray
Carmen.Util.array_map = function (aray, f) {
	var res = [];
	for(var i=0; i<aray.length;i++) {
		res.push(f(aray[i]));
	}
	return res;
}

// array_forEach : apply f to each elt of aray
Carmen.Util.array_forEach = function (aray, f) {
 var i = 0, l = this.length, j;
 for (i=0; i<l; i++) { 
   if ((j = this[i])) { 
     f(j); 
   } 
 }
}



Carmen.Util.buildColumnModel = function(fieldsDesc, withExtent, noSpecialRenderer) {
    var columns = new Array();
    columns.push(new Ext.grid.RowNumberer({header : 'N°'}));
    if (withExtent) {
	    columns.push({
	      id : 'extent',
	      header : '',
	      width : 20,
	      resizable: true,
	      renderer: Carmen.Util.extentRenderer,
	      dataIndex: 'extent'
	    });
		}    
    for (var i=0; i<fieldsDesc.length; i++) {
    	var desc = fieldsDesc[i];
    	var colDesc = {
    		id: desc.name + i.toString(),
    		header: desc.alias,
    		width: 200,
    		sortable: true,
    		resizable: true,
    		renderer: Carmen.Util.carmenTypeToExtRenderer(desc.type),
    		dataIndex: desc.name
    	};
    	columns.push(colDesc);
    }
    return columns; 
};

Carmen.Util.buildFieldsDesc = function(cmnFieldStr, baseQueryURL ) {
  var fieldsDesc = [];
  baseQueryURL = baseQueryURL ? baseQueryURL : "";
  var list = Carmen.Util.chompAndSplit(cmnFieldStr,';');
  for (var j=0; j<list.length; j++) {
    var desc  = Carmen.Util.decodeCarmenFieldDesc(list[j], baseQueryURL);
    fieldsDesc.push(desc);
  }
  return fieldsDesc;
}

// Miscellaneous functions
// set_add : add el to aray only if not already in
// optional parameter f_merge is used to compare the 
// element to be inserted with existing ones. 
// f_merge (e1,e2) -> bool
// in case f_merge return true, the element to be 
// inserted is skipped 
Carmen.Util.set_add = function (aray, el, f_merge) {
	var i=0;
	if (!f_merge)
	  f_merge = function(e1,e2) { return e1===e2; };
	while (i<aray.length && !(f_merge(aray[i],el))) 
		i++;
	if (i>=aray.length)
		aray.push(el);
	return aray;
}

// convert an array to a set
Carmen.Util.set_from_array = function (aray) {
	var set = [];
	for (var i=0; i<aray.length; i++) {
		Carmen.Util.set_add(set, aray[i]);
	}		
	return set;
}
/**
 * @param aray the set to consider
 * @param el : the search element
 * @return true if el is present in the set aray 
 */
Carmen.Util.set_member = function (aray, el) {
	var i=0;
	while (i<aray.length && (aray[i]!=el)) 
		i++;
	return i<aray.length;
}

Carmen.Util.loadPage = function(URL) {
	if (URL)
	  top.window.location.replace(URL);
	else {
		top.window.location.reload();
	}
	  
}


// inspired from https://yui-ext.com/forum/showthread.php?t=6016&page=2
Carmen.Util.loadXML = function(xmlStr) {
	var xmlDoc = {};
	// for firefox and friends
	if (document.implementation && document.implementation.createDocument) {
		  //console.log("Firefox");
		  xmlDoc = document.implementation.createDocument("", "", null);
		  //console.log(xmlDoc);
	}
	else if (window.ActiveXObject) {
		  //console.log("IE");
		  xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
		  xmlDoc.onreadystatechange = function () {
		  	if (xmlDoc.readyState == 4)  {}
		  };
		  //console.log(xmlDoc);
	} 
	else {
		xmlDoc = xmlStr;
		  //alert('i have no name :(');
		  alert('Your browser can\'t load the XML file: ' + e.toString());
	} //end else if
   
	xmlDoc.async="false";
	xmlDoc.load(xmlStr);
	//console.log(xmlDoc);
	return xmlDoc;
}

/**
 * @brief convert a boolean value to its corresponding string representation
 * @param bool : a boolean value
 * @return "1" for true, "0" for false or undefined 
 */
Carmen.Util.bool2IntStr = function(bool) {
	return bool ? "1" : "0";
};

// Misc math functions
// number descending order to be used with array.sort
Carmen.Util.descendingOrder= function(a, b) { return b - a; };

Carmen.Util.roundIntTo2Digits = function(n) {
  var res = new Number(n).toFixed();
  var n_str = res.toString();
  if (n_str.length>2) {
    var n_str_1 = n_str.substr(0,2);
    var n_str_2 = n_str.substr(2);
    var n2_str = n_str_1 + '.' + n_str_2;
    var n_dec_digits = n_str_2.length;
    var n2 = parseFloat(n2_str).toFixed();
    res = n2 * Math.pow(10, n_dec_digits);
  }
  return res; 
};

Carmen.Util.roundIntToNdigits = function(n,d) {
  var res = new Number(n).toFixed();
  res = Math.round(res/100)*100;
  var n_str = res.toString();
  if (n_str.length>d) {
    var n_str_1 = n_str.substr(0,d);
    var n_str_2 = n_str.substr(d);
    var n2_str = n_str_1 + '.' + n_str_2;
    var n_dec_digits = n_str_2.length;
    var n2 = parseFloat(n2_str).toFixed();
    res = n2 * Math.pow(10, n_dec_digits);
  }
  return res; 
};

Carmen.Util.roundIntToDigits = function(n) {
  var res = new Number(n).toFixed();
  var n_str = res.toString();
  // number of significant digits should be found another way, 
  // but have no more idea now...
  var n_digits = res>50000 ? 2 : 3;
  res = Carmen.Util.roundIntToNdigits(n, n_digits);

  return res; 
};


Carmen.Util.scaleTextFormater = function(scale) {
  var denom =  parseInt(scale).toString();
  
  var newDenom ='';
  for (var i=0; i<denom.length; i++) {
  	newDenom = denom.charAt(denom.length-1-i) + newDenom;
  	if ((i+1) % 3 == 0 && (i+1)<denom.length)
  		newDenom = ' ' + newDenom;  
  }
  return  ' 1 / ' + newDenom;
}; 

/*
 * Test if the coordinate system cs corresponds 
 * is geographic or projected
 */
Carmen.Util.isGeographicProjection = function(cs) {
  var prj = null;
  if (typeof cs == "string")
    prj = new OpenLayers.Projection(cs);
  else if (cs instanceof OpenLayers.Projection)
    prj = cs;
  return (prj!=null) ? (prj.getUnits()==null) : false;
}; 

/*
 * get the coordinate system units string, e.g. 'm', 'dd'...,
 * from the coordinate system cs. return 'm' if units can't 
 * be guessed from cs.   
 */
Carmen.Util.getProjectionUnits = function(cs) {
  var prj = null;
  if (typeof cs == "string")
    prj = new OpenLayers.Projection(cs);
  else if (cs instanceof OpenLayers.Projection)
    prj = cs;
    
  return (prj==null) ? 'dd' : prj.getUnits()== null ? 'dd' : prj.getUnits();
}; 

/**
 * check if resolution r1 is near resolution r2
 * return true if scale s1 (from r1) differs from 
 * less than 1% from scale s2 (from r2)
 */  
Carmen.Util.resNearTester = function (r1,r2) {
  var denom1 = OpenLayers.Util.getScaleFromResolution(r1, "m");
  var denom2 = OpenLayers.Util.getScaleFromResolution(r2, "m");
  var delta = Math.abs(denom1-denom2)/Math.max(denom1,denom2);
  return (delta<0.01);
}  
  




Carmen.Util.OLWMSGroup2CarmenOWSDesc = function (layer, nextLayerId, extraDesc) {
	var descs = [];
	var layersInfo = (!layer.hasSubLayers()) ? 
		[{ name: layer.name, 
			visible: layer.getVisibility(), 
			wmsName: layer.params.LAYERS, 
			index: 0
		}] : layer.subLayers;
	for (var i=0; i<layersInfo.length; i++) {
		var info = layersInfo[i];
		var desc = {
			Server : { 
				OnlineResource : { attributes : 
						{ href : layer.url }
				},
				attributes : {
					service : layer.params.SERVICE,
					version : layer.params.VERSION
				}
			},
			
			Title : info.name, 
			attributes : {
				
				hidden : Carmen.Util.bool2IntStr(!info.visible),
				name : info.name,
				queryable : Carmen.Util.bool2IntStr(true)
			},
			Name : info.wmsName,
			Extension : {
				wms_format : layer.params.FORMAT,
				//wms_name : info.wmsName,
				wms_onlineresource : layer.url,
				wms_server_version : layer.params.VERSION,
				wms_srs : Ext.valueFrom(layer.params.SRS, extraDesc.projection, false),
				haslabel : Carmen.Util.bool2IntStr(false),
				LAYER_TITLE : info.name,
				layerSettings_ActivThemes : "ON", 
				layerSettings_Tiled : "OFF", 
				layerSettings_download : "OFF", 
				layerSettings_group : Ext.valueFrom(extraDesc.group, "", true),
				layerSettings_order : nextLayerId.toString(), 
				wms_layer : true
			}
		};
		nextLayerId++;
		descs.push(desc);
	};
	return descs
};

Carmen.Util.OLWMTSGroup2CarmenOWSDesc = function (layer, nextLayerId, extraDesc) {
	var descs = [];
	var layersInfo = (!layer.hasSubLayers()) ? 
		[{ name: layer.name, 
			visible: layer.getVisibility(), 
			wmsName: layer.options.layer, 
			index: 0
		}] : layer.subLayers;
	for (var i=0; i<layersInfo.length; i++) {
		var matrixIdsString = "";
		var tileOrigineString = "";
		for(var j = 0;j<layer.matrixIds.length;j++){
			matrixIdsString += layer.matrixIds[j].identifier + " ";
			tileOrigineString += layer.matrixIds[j].topLeftCorner.lon  + ",";
			tileOrigineString += layer.matrixIds[j].topLeftCorner.lat  + " ";
		}
		matrixIdsString = matrixIdsString.slice(0,-1);
		tileOrigineString = tileOrigineString.slice(0,-1);
		var info = layersInfo[i];
		var desc = {
			Server : { 
				OnlineResource : { attributes : 
						{ href : layer.url }
				},
				attributes : {
					service : layer.options.SERVICE,
					version : layer.options.VERSION
				}
			},
			Title : info.name, 
			attributes : {
				haslabel : Carmen.Util.bool2IntStr(false),
				hidden : Carmen.Util.bool2IntStr(!info.visible),
				name : info.name,
				queryable : Carmen.Util.bool2IntStr(true)
			},
			Extension : {
				wmts_format : layer.format,
				wmts_name : layer.name,
				wmts_OnlineResource : layer.url,
				wmts_server_version : layer.version,
				wmts_srs : Ext.valueFrom(layer.projection.projCode, extraDesc.projection, false),	
				wmts_resolutions : layer.resolutions.join(' '),
				wmts_tileorigin :tileOrigineString,
				wmts_matrixids :matrixIdsString,
				wmts_style :layer.style,
				wmts_tileset :layer.matrixSet,
				wmts_wms_layer : layer.layer,				
				LAYER_TITLE : layer.name,
				layerSettings_ActivThemes : "ON", 
				layerSettings_Tiled : "ON", 
				layerSettings_download : "OFF", 
				layerSettings_group : Ext.valueFrom(extraDesc.group, "", true),
				layerSettings_isWmts : "YES",
				layerSettings_order : nextLayerId.toString(),
				wms_layer : true
			}
		};
		nextLayerId++;
		descs.push(desc);	
	};

	return descs
};


Carmen.Util.OLVector2CarmenOWSDesc = function (layer, nextLayerId, extraDesc) {
    var serialized_layer = layer.serializeForContext(true);
     
		var desc = {
			Document :  serialized_layer.kml,
			Title : layer.name, 
			attributes : {
				
				hidden : Carmen.Util.bool2IntStr(!layer.visible),
				name : layer.name,
				queryable : Carmen.Util.bool2IntStr(true)
			},
			Extension : {
				LAYER_TITLE : layer.name,
				haslabel : Carmen.Util.bool2IntStr(false),
				layerSettings_ActivThemes : "ON", 
				layerSettings_Tiled : "OFF", 
				layerSettings_download : "OFF", 
				layerSettings_group : Ext.valueFrom(extraDesc.group, "", true),
				layerSettings_order : nextLayerId.toString(),
				layerSettings_olStyle : serialized_layer.style  
			}
		};

	return [desc];
};










Carmen.Util.OLWFSGroup2CarmenOWSDesc = function (layer, nextLayerId, extraDesc) {
  var descs = [];
  var layersInfo = (!layer.hasSubLayers()) ? 
    [{ name: layer.name, 
      visible: layer.getVisibility(), 
      wmsName: layer.params.LAYERS, 
      index: 0
    }] : layer.subLayers;
  for (var i=0; i<layersInfo.length; i++) {
    var info = layersInfo[i]; 
    
    var desc = {
      Server : { 
        OnlineResource : { attributes : 
            { url : layer.url }
        }
      },
      StyleList : {
        Style : { 
            Name :  info.name,
            Title : "" ,
            LegendURL: {
              attributes : 
                        {
                          //server : extraDesc.server,
                          OnlineResource : 
                          {
                          href : extraDesc.server,
                          type : "simple"  
                          },
                          mapfile : extraDesc.mapfile,
                          layer : extraDesc.layerName,
                          classIdx : "0"
                        }
            }
          }
      },
      Title : info.name, 
      attributes : {
        
        hidden : Carmen.Util.bool2IntStr(!info.visible),
        name : info.name,
        queryable : Carmen.Util.bool2IntStr(true)
      },
      Extension : {
       /* wms_format : layer.params.FORMAT,
        wms_name : info.wmsName,
        wms_OnlineResource : layer.url,
        wms_server_version : layer.params.VERSION,,*/
        wms_title : info.name,
        haslabel : Carmen.Util.bool2IntStr(false),
        wms_srs : Ext.valueFrom(layer.params.SRS, extraDesc.projection, false),
        LAYER_TITLE : info.name,
        layerSettings_ActivThemes : "ON", //Carmen.Util.bool2IntStr(false),
        layerSettings_Tiled : "OFF", //Carmen.Util.bool2IntStr(false),
        layerSettings_download : "OFF", //Carmen.Util.bool2IntStr(false),
        layerSettings_group : Ext.valueFrom(extraDesc.group, "", true),
        layerSettings_order : nextLayerId.toString() 
      }
    };
    nextLayerId++;
    descs.push(desc); 
  };

  return descs
};

Carmen.Util.OLWFSCarmenOWSDesc = function (layer, nextLayerId, extraDesc) {
  var desc = {
    Server : { 
     OnlineResource : { 
       attributes : 
            { url : layer.url }
       }
     },
    StyleList : {
       Style : { 
         Name :  extraDesc.layerTitle,
         Title : "" ,
         LegendURL: {
           attributes : {
             //server : extraDesc.server,
             OnlineResource : 
               {
               href : extraDesc.server,
               type : "simple"  
               },
             mapfile : extraDesc.mapfile,
             layer : extraDesc.layerName,
             classIdx : "0"
           }
         }
       }
      },
    Title : extraDesc.layerName, 
      attributes : {
        
        hidden : Carmen.Util.bool2IntStr(!layer.getVisibility()),
        name : extraDesc.layerName,
        queryable : Carmen.Util.bool2IntStr(true)
      },
    Extension : {
        LAYER_TITLE : extraDesc.layerName,
        haslabel : Carmen.Util.bool2IntStr(false),
        layerSettings_ActivThemes : "ON", //Carmen.Util.bool2IntStr(false),
        layerSettings_Tiled : "OFF", //Carmen.Util.bool2IntStr(false),
        layerSettings_download : "OFF", //Carmen.Util.bool2IntStr(false),
        layerSettings_group : Ext.valueFrom(extraDesc.group, "", true),
        layerSettings_order : nextLayerId.toString(), 
        layerType : "mapfile_wfs",
        layerSettings_mapfile : layer.mapfile
      }
    };
    if (extraDesc.infoFields!=null) {
      desc.Extension.layerSettings_infoFields = extraDesc.infoFields;
	 	  desc.Extension.layerSettings_briefFields = extraDesc.briefFields;
	 	  desc.Extension.layerSettings_ToolTipFields = extraDesc.toolTipFields;
    }
  return [desc];
};

Carmen.Util.OLLOCALDATACarmenOWSDesc = function (layer, layerId, extraDesc) {
  var desc = {
	Server : {
	  OnlineResource : {
	    attributes : { url : layer.url }
	  }
	},
	StyleList : {
	  Style : {
		Name : extraDesc.layerTitle,
		Title : "",
		LegendURL : {
		  attributes : {
			//server   : extraDesc.server,
		  OnlineResource : 
        {
        href : extraDesc.server,
        type : "simple"  
        },
			mapfile  : extraDesc.mapfile,
			layer	 : extraDesc.layerName,
			classIdx : "0"
		  }
		}
	  }
	},
	Title : extraDesc.layerName,
	  attributes : {
		
		hidden   : Carmen.Util.bool2IntStr(!layer.getVisibility()),
		name	 : extraDesc.layerName,
		queryable: Carmen.Util.bool2IntStr(true)
	  },
	  Extension : {
		LAYER_TITLE : extraDesc.layerName,
		layerSettings_ActivThemes : "ON",
		haslabel : Carmen.Util.bool2IntStr(false),
		layerSettings_Tiles 	  : "OFF",
		layerSettings_download	  : "OFF",
		layerSettings_group		  : Ext.valueFrom(extraDesc.group, "", true),
		layerSettings_order		  : layerId.toString(),
		layerType				  : "mapfile_localdata",
		layerSettings_mapfile	  : layer.mapfile
	  }
  };
  if(extraDesc.infoFields != null){
	desc.Extension.layerSettings_infoFields    = extraDesc.infoFields;
	desc.Extension.layerSettings_briefFields   = extraDesc.briefFields;
	desc.Extension.layerSettings_ToolTipFields = extraDesc.toolTipFields; 
  }
  return [desc];
}

/* 
 * a backwards compatable implementation of postMessage
 * by Josh Fraser (joshfraser.com)
 * released under the Apache 2.0 license.  
 *
 * this code was adapted from Ben Alman's jQuery postMessage code found at:
 * http://benalman.com/projects/jquery-postmessage-plugin/
 * 
 * other inspiration was taken from Luke Shepard's code for Facebook Connect:
 * http://github.com/facebook/connect-js/blob/master/src/core/xd.js
 *
 * the goal of this project was to make a backwards compatable version of postMessage
 * without having any dependency on jQuery or the FB Connect libraries
 *
 * my goal was to keep this as terse as possible since my own purpose was to use this 
 * as part of a distributed widget where filesize could be sensative.
 * 
 */

// everything is wrapped in the XD function to reduce namespace collisions
var XD = function(){
  
    var interval_id,
    last_hash,
    cache_bust = 1,
    attached_callback,
    window = this;
    
    return {
        postMessage : function(message, target_url, target) {
            
            if (!target_url) { 
                return; 
            }
    
            target = target || parent;  // default to parent
    
            if (window['postMessage']) {
                // the browser supports window.postMessage, so call it with a targetOrigin
                // set appropriately, based on the target_url parameter.
                target['postMessage'](message, target_url.replace( /([^:]+:\/\/[^\/]+).*/, '$1'));

            } else if (target_url) {
                // the browser does not support window.postMessage, so set the location
                // of the target to target_url#message. A bit ugly, but it works! A cache
                // bust parameter is added to ensure that repeat messages trigger the callback.
                target.location = target_url.replace(/#.*$/, '') + '#' + (+new Date) + (cache_bust++) + '&' + message;
            }
        },
  
        receiveMessage : function(callback, source_origin) {
            
            // browser supports window.postMessage
            if (window['postMessage']) {
                // bind the callback to the actual event associated with window.postMessage
                if (callback) {
                    attached_callback = function(e) {
                        if ((typeof source_origin === 'string' && e.origin !== source_origin)
                        || (Object.prototype.toString.call(source_origin) === "[object Function]" && source_origin(e.origin) === !1)) {
                            return !1;
                        }
                        callback(e);
                    };
                }
                if (window['addEventListener']) {
                    window[callback ? 'addEventListener' : 'removeEventListener']('message', attached_callback, !1);
                } else {
                    window[callback ? 'attachEvent' : 'detachEvent']('onmessage', attached_callback);
                }
            } else {
                // a polling loop is started & callback is called whenever the location.hash changes
                interval_id && clearInterval(interval_id);
                interval_id = null;

                if (callback) {
                    interval_id = setInterval(function(){
                        var hash = document.location.hash,
                        re = /^#?\d+&/;
                        if (hash !== last_hash && re.test(hash)) {
                            last_hash = hash;
                            callback({data: hash.replace(re, '')});
                        }
                    }, 100);
                }
            }   
        }
    };
}();

var Url = {

  // public method for url encoding
  encode : function(string) {
    return escape(this._utf8_encode(string));
  },

  // public method for url decoding
  decode : function(string) {
    return this._utf8_decode(unescape(string));
  },

  // private method for UTF-8 encoding
  _utf8_encode : function(string) {
    string = string.replace(/\r\n/g, "\n");
    var utftext = "";

    for ( var n = 0; n < string.length; n++) {

      var c = string.charCodeAt(n);

      if (c < 128) {
        utftext += String.fromCharCode(c);
      } else if ((c > 127) && (c < 2048)) {
        utftext += String.fromCharCode((c >> 6) | 192);
        utftext += String.fromCharCode((c & 63) | 128);
      } else {
        utftext += String.fromCharCode((c >> 12) | 224);
        utftext += String.fromCharCode(((c >> 6) & 63) | 128);
        utftext += String.fromCharCode((c & 63) | 128);
      }

    }

    return utftext;
  },

  // private method for UTF-8 decoding
  _utf8_decode : function(utftext) {
    var string = "";
    var i = 0;
    var c = c1 = c2 = 0;

    while (i < utftext.length) {

      c = utftext.charCodeAt(i);

      if (c < 128) {
        string += String.fromCharCode(c);
        i++;
      } else if ((c > 191) && (c < 224)) {
        c2 = utftext.charCodeAt(i + 1);
        string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
        i += 2;
      } else {
        c2 = utftext.charCodeAt(i + 1);
        c3 = utftext.charCodeAt(i + 2);
        string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6)
            | (c3 & 63));
        i += 3;
      }

    }

    return string;
  }

};


Carmen.Util.addCssSheet = function(windowElt, filename, firstPos) {
  var css_style= windowElt.document.createElement("link");
  css_style.setAttribute("rel", "stylesheet");
  css_style.setAttribute("type", "text/css");
  css_style.setAttribute("href", filename);
  var head = windowElt.document.getElementsByTagName("head")[0]; 
  if (firstPos) 
    head.insertBefore(css_style, head.firstChild);
  else
    head.appendChild(css_style);
}

Carmen.Util.addCssClass = function(elt, className) {
  elt.className = elt.className + " " + className;
}



Carmen.Util.colorStr2css = function(colorStr) {
  var r,g,b;
  r = parseInt(colorStr.slice(0,2),16);
  g = parseInt(colorStr.slice(2,4),16);
  b = parseInt(colorStr.slice(4),16);
  return {r : r, g : g, b : b};
};


Carmen.Util.isComplexGeometry = function(geom) {
  return (geom.CLASS_NAME=="OpenLayers.Geometry.Collection" ||
    geom.CLASS_NAME=="OpenLayers.Geometry.MultiLineString" ||
    geom.CLASS_NAME=="OpenLayers.Geometry.MultiPolygon" ||
    geom.CLASS_NAME=="OpenLayers.Geometry.MultiPoint");
};
  

Carmen.Util.simplifyFeature = function(feature) {
  var newFeatures = [];
  var res = [];
  var geom = feature.geometry;
  if (Carmen.Util.isComplexGeometry(geom)) {
    //console.log(geom.CLASS_NAME +" found !")
    for (var j=0; j<geom.components.length; j++) {
      var f = feature.clone();
      f.geometry = f.geometry.components[j];
      newFeatures.push(f); 
    }
    res = res.concat([], newFeatures);
  }
  else
    res = [feature];
  if (newFeatures.length>0) {
    //alert("before relaunching simplification...");
    for (var i =0; i<newFeatures.length; i++ ) {
      var newFeatures2 = Carmen.Util.simplifyFeature(newFeatures[i]);
      if (newFeatures2.length>0)
        res = res.concat(newFeatures2);
    }
  }
  return res;
};      


// URL & path handling
Carmen.Util.parseURL =  function(url) {
  res = null;
  var url_pattern = url_pattern = /(\w+):\/\/([^\/]+)\/(.+\/)*([^\/]*)$/;
  var matched = url.match(url_pattern);
  if (matched != null) {
    res = {
      fullurl : matched[0],   
      protocol : matched[1],
      host : matched[2],  
      mid : matched[3] ? matched[3] : "",
      end : matched[4] };
    res.start = res.protocol + '://' + res.host + '/' + res.mid;
  }
  return res;
};

Carmen.Util.parsePath =  function(path) {
  var path_pattern = /(.*\/)*([^\/]+)$/;
  var matched = path.match(path_pattern);
  var res= null;
  if (matched != null) {
    res = {
      start : matched[1] ? matched[1] : "",   
      end : matched[2] ? matched[2] : ""
    };
  }
  return res;
};

Carmen.Util.parseFilename =  function(filename) {
  var file_pattern = /([^\.]*)\.?(.*)$/;
  var matched = filename.match(file_pattern);
  var res= null;
  if (matched != null) {
    res = {
      basename : matched[1] ? matched[1] : "",   
      suffix : matched[2] ? matched[2] : ""
    };
  }
  return res;
};


// Reuse of ext 2.2 code to make an upload to server and getting 
// back result to an iframe
// ext 3.0 version of form raises errors with ff 3.6.3 on windows  
Carmen.Util.doFormUpload = function (f, url, queryParams){
        
    // ExtJS bug Hack : forcing form encoding for upload
		// ref : http://extjs.com/forum/showthread.php?t=56864
		var form = f;

    var p =  Ext.urlEncode(queryParams);
        
    var id = Ext.id();
    var frame = document.createElement('iframe');
    frame.id = id;
    frame.name = id;
    frame.className = 'x-hidden';
    if(Ext.isIE){
        frame.src = Ext.SSL_SECURE_URL;
    }
	
	document.body.appendChild(frame);

    if(Ext.isIE){
       document.frames[id].name = id;
    }

    form.target = id;
    form.method = 'POST';
    form.enctype = form.encoding = 'multipart/form-data';
    if(url){
        form.action = url;
    }

    if(p){ // add dynamic params
        p = Ext.urlDecode(p, false);
        for(var k in p){
            if(p.hasOwnProperty(k)){
				form.add(Ext.create(
					"Ext.form.field.Hidden", 
					{
						name:k,
						value:p[k]
					})
				);
            }
        }
    }

    function cb(){
        var r = {  // bogus response object
            responseText : '',
            responseXML : null
        };
        try { //
            var doc;
            if(Ext.isIE){
                doc = frame.contentWindow.document;
            }else {
                doc = (frame.contentDocument || window.frames[id].document);
            }
            if(doc && doc.body){
                r.responseText = doc.body.innerHTML;
            }
            if(doc && doc.XMLDocument){
                r.responseXML = doc.XMLDocument;
            }else {
                r.responseXML = doc;
            }
        }
        catch(e) {
            // ignore
            //console.log("erreur");
        }

        
        setTimeout(function(){Ext.removeNode(frame);}, 100);
    }
	
	var frame_dom = Ext.get(frame.id);

	frame_dom.on('load', cb, this, {single: true});
	
    form.submit({
		url:url,
		standardSubmit: true
	});
};



Carmen.Util.computeLinearMidPoint = function(line) {
  var vertices = line.getVertices();
  var a = vertices[0]; 
  var x = a.x;
  var y = a.y;
  var lmid = line.getLength()/2.0;
  if (vertices.length>2) {
   var b = vertices[1];
   var ab = new OpenLayers.Geometry.Curve([a,b]);
   var lab = ab.getLength();
   var l = lab;
   var i = 0;
   while (!(l>=lmid)) {
    i++;
    a = b;
    b = vertices[i];
    ab = new OpenLayers.Geometry.Curve([a,b]);
    lab = ab.getLength();
    l = l + lab;  
   }
   ldelta = lab - l + lmid;
   x = a.x + (b.x-a.x) * (ldelta/lab);
   y = a.y + (b.y-a.y) * (ldelta/lab); 
  }
  return new OpenLayers.Geometry.Point(x,y);
};

// stats function
/**
 * compute statistics on values (array)
 * returns an object containing statistical results
 * a minima : { count 	: xxx }
 * a maxima : { count 	: xxx,
 * 				sum 	: xxx,
 * 				min		: xxx,
 * 				max		: xxx,
 * 				average	: xxx,
 * 				variance : 	xxx,
 * 				stddeviation : xxx }
 */
   
Carmen.Util.stats = function(values) {
 var res = {};
 res.count = values.length;
 if (values.length>0) {
	 var sum = 0;
	 var min = Number.MAX_VALUE;
	 var max = Number.MIN_VALUE;
	 var average = Number.MIN_VALUE;
	 for (var i=0; i <values.length; i++) {
		value = parseFloat(values[i]);
		sum = sum + value;
		min = min > value ? value : min;
		max = max < value ? value : max;
	 }
	 
	 if (!isNaN(sum)) {
		average = sum/values.length;
		res.sum = sum;
		res.min = min;
		res.max = max;
		res.average = average;
		if (values.length>1) {
			 var variance = 0.0
			 for (var i=0; i <values.length; i++) {
				value = parseFloat(values[i]);
				variance =  variance + ((value-average)*(value+average));
			 }
			 res.variance = variance / (values.length-1);
			 res.stddeviation = Math.sqrt(variance);
		}
	}	 
 }
 return res;
}


// miscellaneous functions to fit admin constraint
// should be made another way...
initBanner = function(){
  var frame = document.getElementById("iframe-banner").contentWindow;
  if(typeof(frame!="undefined") && frame!=null && frame.document.getElementById("CARTE_NOM")!=null) {
    frame.document.getElementById("CARTE_NOM").innerHTML = Url.decode(jsOws.ViewContext.General.Title);
    Carmen.Util.addCssSheet(frame, overloadExtTheme, true);
    Carmen.Util.addCssClass(frame.document.body, "x-window-mc");
  }
//  if(Ext.isIE && Ext.isIE8){
  if(typeof(frame!="undefined") && frame!=null && frame.document.querySelectorAll(".title").length==1){
	frame.document.querySelectorAll(".title")[0].innerHTML = Url.decode(jsOws.ViewContext.General.Title);
	Carmen.Util.addCssSheet(frame, overloadExtTheme, true);
    Carmen.Util.addCssClass(frame.document.body, "x-window-mc");
  }
  if(typeof(frame!="undefined") && frame!=null && frame.document.querySelectorAll(".commentaire").length==1){
	if(jsOws.ViewContext.General.Extension && jsOws.ViewContext.General.Extension.COMMENTAIRE)
	  frame.document.querySelectorAll(".commentaire")[0].innerHTML = Url.decode(jsOws.ViewContext.General.Extension.COMMENTAIRE);
	else
	  frame.document.querySelectorAll(".commentaire")[0].innerHTML = "";
  }
//  }
};

