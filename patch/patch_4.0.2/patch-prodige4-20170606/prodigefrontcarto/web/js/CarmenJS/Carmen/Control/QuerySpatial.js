  /**
 * @_requires OpenLayers/Controljs
 */

/**
 * Class: Carmen.Control.Info
 
 * Inherits from:
 *  - <OpenLayers.Control>
 */
 
Carmen.Control.QuerySpatial = new OpenLayers.Class(OpenLayers.Control, {
	
  // UI components
  btn: new Ext.Button({
      tooltip: 'Requêtes spatiales',      
      tooltipType: 'title',
      cls: 'x-btn-icon cmn-tool-spatialquery',
      disabled: false
  }), 
    
  // properties 
  /**
   * Property: type
   * {OpenLayers.Control.TYPE}
   */
  type: OpenLayers.Control.TYPE_TOOL,
  
  queryInProgress : false,

  layerTreeManager : null,

  layerTree : null,
  
  layerTreeRef : null,

  mapfile : null,
  
  serviceUrl : null,
  
  operatorSet: [
	["intersects", "s'intersectent avec", 0],
	["in", "sont entièrement contenus dans", 1],
	["contains", "contiennent entièrement", 2],
	["distance_within", "sont à une distance", 3]
	],

  // UI part..
  windowTitle : 'Requêtes spatiales',
  
  win : null,
  
  queryPanel : null,
  
  comboLayer : null,
  
  comboOperator : null,
  
  gridSelection : null,
  
  comboLayerRef : null,
  
  radiusField : null,
  
  gridInfoPanel : null, 
  
  gridInfoFormPanel : null,
  
  requestTimeout : 120000, //set to 120s for long requests
  
  
  // methods
  initialize: function(serviceUrl, mapfile, options) {
    OpenLayers.Control.prototype.initialize.apply(this, [options]);
    this.mapfile = mapfile;
    this.serviceUrl = serviceUrl;
  },

  initializeUIComponents : function() {

  	   this.gridSelection = new Ext.grid.GridPanel({
  	      title : 'Sélection d\'objets',
  	      width : 'auto',
  	      height : 150,
  	      store: Carmen.Util.emptyStore,
  	      cm: Carmen.Util.emptyColumnModel,
  	      hidden: true,
  	      disabled: true,
  	      //sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
  	      autoScroll: true,
  	      //forceLayout: true,
  	      //layout : 'fit',
  	      headerAsText:true,
  	      style: {
              width: '95%',
              marginBottom: '0px',
              marginTop: '10px'
          },
          tools:[{
			id:'all',
			qtip: 'Sélectionner tous les éléments de la sélection',
			scope : this,
			handler: function() {
				this.gridSelection.getSelectionModel().selectAll();
			}
			
		  },{
			id:'inv',
			qtip: 'Inverser la sélection',
			scope : this,
			handler: function() {
				var unselected= [];
				for (var i=0; i<this.gridSelection.getStore().getTotalCount(); i++)
					if (!this.gridSelection.getSelectionModel().isSelected(i))
						unselected.push(i)
				this.gridSelection.getSelectionModel().clearSelections();
				this.gridSelection.getSelectionModel().selectRows(unselected,false);
			}
		  }]  
  	    });
  
       this.comboLayerRef = new Ext.form.ComboBox({
        fieldLabel: 'Les entités de la couche',
        queryMode: 'local',
        editable: true,
        listConfig: {
          loadingText: 'Chargement',
          getInnerTpl: function() { return '<tpl for="."><div ext:qtip="{layerTitle}" class="x-combo-list-item">{layerTitle}</div></tpl>'; }
        },
        emptyText: 'Sélectionnez une couche',
        typeAhead : true,
        forceSelection: true,
        triggerAction: 'all',
        minChars: 1,
        queryDelay: 250,     
        store: new Ext.data.ArrayStore({
           fields: ['layer', 'layerTitle'],
           data : [[null, 'Sélectionnez une couche']] }
        ),
        validator: function(v) {
           return (v!="") ? true : "Choississez une couche de référence pour la condition spatiale";
          },   
        displayField: 'layerTitle',
        valueField: 'layer',
        width: 180,
        listeners : { 
          'select' : { 
            fn : function(f,r,i) {
                  if (i==0) {
                  	// vidage gridSelection
                  	this.gridSelection.disable();
                  	this.gridSelection.hide();
                  }
                  else {
                  	var obj = r.get('layer');
                  	var sl = obj.layer.getSelection(obj.name);
                  	this.gridSelection.reconfigure(sl.store, sl.cm);
                  	this.gridSelection.enable();
                  	this.gridSelection.show();
                  	this.gridSelection.getSelectionModel().selectAll();
                  	this.gridSelection.syncSize();
                  	this.gridInfoPanel.hide();
                  }	
                },
              scope : this 
           }
        }     	
      });

	this.comboLayer = new Ext.form.ComboBox({
        tpl: '<tpl for="."><div ext:qtip="{layerTitle}" class="x-combo-list-item">{layerTitle}</div></tpl>',
        fieldLabel: 'Je veux sélectionner les entités de la couche',
        queryMode: 'local',
        editable: true,
        listConfig : {loadingText: 'Chargement' },
        emptyText: 'Sélectionnez une couche',
        typeAhead : true,
        forceSelection: true,
        triggerAction: 'all',
        minChars: 1,
        queryDelay: 250,     
        store: new Ext.data.ArrayStore({
           fields: ['layer', 'layerTitle'],
           data : [[null, 'Sélectionnez une couche']] }
        ),
        validator: function(v) {
           return (v!="") ? true : "Choississez une couche dans laquelle seront sélectionnées les entités";
          },   
        displayField: 'layerTitle',
        valueField: 'layer',
        width: 180
   });


   this.comboOperator = new Ext.form.ComboBox({
    	tpl: '<tpl for="."><div ext:qtip="{operatorDesc}" class="x-combo-list-item">{operatorDesc}</div></tpl>',
    	fieldLabel: 'qui',
    	mode: 'local',
        emptyText: 'Sélectionnez un opérateur',
        typeAhead : true,
        forceSelection: true,
        triggerAction: 'all',
        minChars: 1,
        queryDelay: 250,     
        store: new Ext.data.ArrayStore({
           fields: ['operator', 'operatorDesc', 'operatorCode'],
           data : this.operatorSet 
        }),
        validator: function(v) {
           return (v!='Sélectionnez un opérateur');
          },   
        displayField: 'operatorDesc',
        valueField: 'operatorCode',
        width: 180,
        listeners : { 
          'select' : { 
            fn : function(f,r,i) {
                  var radiusFieldElt = Ext.get('radiusField_' + this.displayClassName);
                  var labelElt = null;
                  if (radiusFieldElt!=null)
					var labelElt = radiusFieldElt.findParent("div.x-form-item",null, true).first();
                  if (i<3) {
                  	// hiding radius field when distance operator not selected
                  	this.radiusField.disable();
                  	this.radiusField.hide();
                  	if (labelElt)
						labelElt.hide();
                  }
                  else {
                  	this.radiusField.enable();
                  	this.radiusField.show();
                  	if (labelElt)
						labelElt.show();
                  }	
                },
              scope : this 
           }
        }     	
      });

   this.radiusField = new Ext.form.NumberField({
    	id : 'radiusField_' + this.displayClassName,
    	fieldLabel: 'inférieure ou égale à (en ' + Carmen.Util.getProjectionUnits(this.map.projection) + ')',
    	mode: 'local',
    	allowBlank: false,
    	hidden: true,
    	width: 100,
    	value: 0
    });
   
   this.queryPanel = new Ext.form.FormPanel({
		fitToFrame: true,
        bodyStyle:'padding:10px 10px 10px 10px; color: #000000;',
        labelStyle: 'color: #000000;',
        labelWidth: 160,
        monitorValid : true,
	      items : [
	        this.comboLayer,
	        this.comboOperator,
	        this.radiusField,
	        this.comboLayerRef, 
	        this.gridSelection
		  ],
		  buttons : [{
          id: 'btnValid_' + this.displayClassName + this.mode,
          formBind : true,
          text: 'Interroger',
          listeners : {
            'click' : {
              fn : this.performQuery,                    
              scope : this
            } 
          }
         }]
   });
   
   // create the Grid
   this.gridInfoPanel = new Ext.grid.GridPanel({
    id : 'gridInfoPanel_' + this.displayClassName,
    store: new Ext.data.ArrayStore({
      fields: [],
      data: []
    }),
    columns: [],
    title:'Résultats sélectionnés',
    hidden: true,
    autoScroll: true,
    fitToFrame: true,
    border: false,
    iconCls : 'cmnInfoGridZoomIcon',
    tools:[{
			id:'xls',
			qtip: 'Export de la sélection au format Excel',
			scope : this
		},
		{
			id:'csv',
			qtip: 'Export de la sélection au format CSV',
			scope : this
		}]
   });
   this.gridInfoFormPanel = new Ext.form.FormPanel({
	id: 'gridInfoFormPanel_' + this.displayClassName,
	hidden : true,
	fileUpload : true,
	waitTitle : "Traitement en cours..."
   });
    
   this.win = new Ext.Window({
      id : 'window_' + this.displayClassName, 
      layout: 'form',
	    width:420,
	    height: 420,
      constrain : true,
	    plain: true,
	    title: this.windowTitle,
	    modal:false,
	    autoDestroy :true,
	    resizable:true,
	    closeAction: 'hide',
	    shadow : false,
	    hidden: true,
	    autoScroll: true,
	    items: [this.queryPanel, this.gridInfoPanel, this.gridInfoFormPanel]
	        
	  });
  },


  setMap: function(map) {
    OpenLayers.Control.prototype.setMap.apply(this, arguments);
    
    this.initializeUIComponents();
    // linking control activation to selection events
	// Not used anymore. btn always activ and showing 
	// a popup msg when no selection activ 
	/*  
	this.map.events.register('addSelection', this,
		function(evt) {
		  if (!this.active)
			this.btn.setDisabled(false);
			// updating here layers with selection list...
			 //if (!this.win.hidden)
			 //	this.updateWindow(); 
		});
  
    this.map.events.register('emptySelection', this,
        function(evt) {
			if (!this.queryInProgress) {
				this.deactivate(); 
				this.btn.setDisabled(true);
			}
        });
    */
	this.btn.addListener('click', Carmen.Util.buildExt2olHandlerButton(this));    
	var tb = this.map.app.ui.getTopToolBar();
	tb.add(this.btn);
	this.map.app.ui.doLayout();
  },
  
  activate: function() {
    if (this.getLayerTreeManager()==null) {
      Ext.MessageBox.alert("Warning", "The control " + 
       this. CLASS_NAME + 
       " requires the LayerTreeManager control to work.");
      return null;
    }
    OpenLayers.Control.prototype.activate.apply(this, arguments);
  },

  deactivate: function() {
  	OpenLayers.Control.prototype.deactivate.apply(this, arguments);
  	this.win.hide();
  },
  
  trigger : function() {
	// check that selection is not null
	if (this.map.getSelectionLayers().length==0) {
		Ext.MessageBox.alert(this.windowTitle, 
			"Pour effectuer une requête spatiale, vous devez au préalable sélectionner" + 
			" des objets via les outils d'information ou de requêtes attributaires");
      return null;
	}

    if (!this.win.rendered) {
	 this.win.show();
	 this.gridInfoPanel.view.hmenu.add({
		itemId : 'stats',
		text: 'Statistiques',
		iconCls:'cmn-tool-stats'
	  });
    }
    if (this.win.hidden)
      this.win.show();
    this.updateWindow();  
  },


  performQuery : function() {
    
     var queryParams = null;
	 var fidList = [];
	 var form = this.queryPanel.getForm();
	 if (!form.isValid()) {
		Ext.Msg.alert(this.windowTitle, 'Certains paramètres ne sont pas valides');
		return;
	 }
	
	this.gridSelection.getSelectionModel().each(
	 function(record) { fidList.push(record.get('fid')); });
	   if (fidList.length==0) {
		 Ext.Msg.alert(this.windowTitle, 'Vous devez sélectionner ' +
		   'au moins un objet dans la sélection');
	   }
	   else {
				
		  var lname = this.comboLayer.getValue().name;
		  var layerInfoFields = this.comboLayer.getValue().node.attributes.infoFields;
		  var layerBriefFields = this.comboLayer.getValue().node.attributes.briefFields;
		  var layerBaseQueryURL = this.comboLayer.getValue().node.attributes.baseQueryURL;
		  
		  // building fields and infoFields desc
		  var fieldsList = [ layerInfoFields ];
		  var briefFieldsList = [ layerBriefFields ];
		  var baseQueryURLList = [ layerBaseQueryURL ];
		  var fieldsDesc = new Array();
		  for (var i=0; i<fieldsList.length; i++) {
			  var list = Carmen.Util.chompAndSplit(fieldsList[i],';');
			  fieldsDesc[i] = new Array();
			  for (var j=0; j<list.length; j++) {
				var desc  = Carmen.Util.decodeCarmenFieldDesc(list[j], baseQueryURLList[i]);
				fieldsDesc[i][j]=desc;   
			  }
		  }
		  var briefFieldsDesc = new Array();
		  for (var i=0; i<briefFieldsList.length; i++) {
			  var list = Carmen.Util.chompAndSplit(briefFieldsList[i],';');
			  briefFieldsDesc[i] = new Array();
			  for (var j=0; j<list.length; j++) {
				var desc  = Carmen.Util.decodeCarmenFieldDesc(list[j], baseQueryURLList[i]);
				briefFieldsDesc[i][j]=desc;   
			  }
			  briefFieldsDesc[i] = Carmen.Util.array_filter(briefFieldsDesc[i], Carmen.Util.fieldFilter_TXT);
		  }
		  queryParams = {
			queryType : this.comboOperator.getValue(),
			radius : this.radiusField.getValue(),
			map : this.mapfile,
			layerRef : this.comboLayerRef.getValue().name,
			layers : Ext.encode([lname]),
			fidList : fidList.join(','),
			shape : " ",
			shapeType : " ",
			showGraphicalSelection : true,
			fields : Ext.util.JSON.encode(fieldsDesc),
			briefFields : Ext.util.JSON.encode(briefFieldsDesc)
		  };
		}
	 
	 
	 var received = function (response) {
      response = Ext.decode(response.responseText);
	  if (response.success!=null && response.success==false) {
		Carmen.Util.handleAjaxFailure(response, this.windowTitle, false); 
		return;
	  }
      if (response.totalCount == 0) {
        this.clearGridPanel();
      }
      else {
	      for(lname in response.results)  {
	        var node = this.layerTree.findChildByAttribute('layerName', lname);
	        if (node!=null) { 
	          var data = response.results[lname].data;
	          node.attributes.infoFieldDesc = Ext.decode(queryParams.fields)[0];
	          node.attributes.infoData = response.results[lname];
	          node.attributes.infoData.qfile = response.queryfile; 
	          this.updateGridPanel(node);
	        }
	      }
      }   
      this.updateSelectionCombo();
    }
	 	 
	 if (queryParams!=null) { 
	  // launching request... 
	  Ext.Ajax.timeout = this.requestTimeout;
	  Ext.Ajax.request({
		url: this.serviceUrl,
		//timeout: this.requestTimeout,
		success: received,
		failure: function (response) { 
		   Carmen.Util.handleAjaxFailure(response, control.windowTitle, true); 
		},
		scope : this,
		params: queryParams
	  });
	 }
  },
  

  updateWindow : function() {
      var mapserverLayerFilter = function(n) {
         var type = Ext.valueFrom(n.attributes.type, -1, false);
         var res = (type == Carmen.Control.LayerTreeManager.NODE_LAYER &&
  			   n.attributes.OlLayer instanceof Carmen.Layer.MapServerGroup);
         return res;
       };  
       
      this.layerTree = this.getLayerTreeManager().getLayerTree().clone();
	  this.layerTree.filter(this.filterNodeLayerQueryable);
      
      var layerNodes = this.layerTree.collectChildNodes(mapserverLayerFilter);
      var data = [[null,'Sélectionnez une couche']].concat(
       Carmen.Util.array_map(layerNodes,
       function (e) {
         return [ { node : e, layer : e.attributes.OlLayer, name : e.attributes.layerName}, e.attributes.text ]
       }));
    var nbLayerInSel = this.updateSelectionCombo();
    if (nbLayerInSel.length<=1) {
      this.win.hide();
      Ext.Msg.alert('Requêtes spatiales', 'Aucune sélection n\'est active');
    }
    else {
      this.comboLayer.getStore().removeAll();
      this.comboLayer.getStore().loadData(data);
      this.comboLayer.reset();
      this.radiusField.reset();
      
      // Ext hack... for hiding radius fieldlabel 
	  var radiusFieldElt = Ext.get('radiusField_' + this.displayClassName);
      if (radiusFieldElt!=null)
		radiusFieldElt.findParent("div.x-form-item",null, true).first().hide();
	  
	  this.gridInfoPanel.setWidth(this.win.getWidth()-15);
	  this.clearGridPanel();
	  this.gridInfoPanel.hide();
	  
	  
    }
  },
  
  updateSelectionCombo : function() {
	  var mapserverLayerFilter = function(n) {
         var type = Ext.valueFrom(n.attributes.type, -1, false);
         var res = (type == Carmen.Control.LayerTreeManager.NODE_LAYER &&
  			   n.attributes.OlLayer instanceof Carmen.Layer.MapServerGroup);
         return res;
       };
            
      this.layerTreeRef = this.getLayerTreeManager().getLayerTree().clone();
            
      this.layerTreeRef.filter(
       function (n) {
         var type = Ext.valueFrom(n.attributes.type, -1, false);
  		   var res = (type == Carmen.Control.LayerTreeManager.NODE_GROUP && n.hasChildNodes() && n.getDepth()>0) || (type == Carmen.Control.LayerTreeManager.NODE_LAYER &&
  			   n.attributes.OlLayer instanceof Carmen.Layer.MapServerGroup && 
  			   n.attributes.OlLayer.handleSelection(n.attributes.layerName) &&
  			   n.attributes.OlLayer.hasSelection(n.attributes.layerName));
  	     return res;
       });
           
      var layerNodesRef = this.layerTreeRef.collectChildNodes(mapserverLayerFilter);
      
      var dataRef = [[null,'Sélectionnez une couche']].concat(
       Carmen.Util.array_map(layerNodesRef,
       function (e) {
         return [ { layer : e.attributes.OlLayer, name : e.attributes.layerName}, e.attributes.text ]
       }));

	this.gridSelection.hide();
	this.comboLayerRef.getStore().removeAll();
	this.comboLayerRef.getStore().loadData(dataRef);
	this.comboLayerRef.reset();
	
	return dataRef.length;
  },
   
   
   clearGridPanel : function() {
  	var gPanel = this.gridInfoPanel;
  	gPanel.setTitle('Aucun résultat');
  	var xlsTool = this.gridInfoPanel.getTool('xls');
    xlsTool.hide();
    var csvTool = this.gridInfoPanel.getTool('csv');
    csvTool.hide();
  	gPanel.reconfigure(Carmen.Util.emptyStore, Carmen.Util.emptyColumnModel);
  	if (!gPanel.isVisible()){
      gPanel.show();
      gPanel.setWidth(this.win.getWidth()-15);
      this.gridInfoPanel.syncSize();
  	}

  },
  
  updateGridPanel : function(layerNode) {
    var control = this;
    var gPanel = this.gridInfoPanel; 
    
    // clearing selection
    this.queryInProgress = true;
    this.gridSelection.hide();
    this.map.clearSelection();
    this.queryInProgress = false;
    // removing listeners...
    gPanel.purgeListeners();    
    // updating panel title
    layerNode.attributes.layerAlias = layerNode.text;
    gPanel.setTitle(layerNode.text + ' ' + 
      layerNode.attributes.infoData.data.length + 
      ' résultats');
    
    var extentStr = layerNode.attributes.infoData.extent;
    var minScale = layerNode.attributes.minScale;
    var maxScale = layerNode.attributes.maxScale;
    var bounds = Carmen.Util.strExtentToOlBounds(extentStr, Carmen.Control.AdvancedQueryAttributes.PUNCTUAL_RADIUS);
    var lnode = layerNode;
    gPanel.header.on('click', 
        function() {
          control.map.zoomToSuitableExtent(bounds, minScale, maxScale);
          lnode.attributes.OlLayer.setDisplayMode(
             Carmen.Layer.MapServerGroup.DISPLAY_NQUERYMAP, 
             lnode.attributes.layerName, null, 
             lnode.attributes.infoData.qfile);
        });
    control.map.zoomToSuitableExtent(bounds, minScale, maxScale);
    layerNode.attributes.OlLayer.setDisplayMode(
       Carmen.Layer.MapServerGroup.DISPLAY_NQUERYMAP, 
       layerNode.attributes.layerName, null, 
       layerNode.attributes.infoData.qfile);
               
     // configuring and showing the result table panel
     var gConfig = this.buildGridConfig(layerNode.attributes.infoFields, layerNode.attributes.infoData);
     
     // configuring exports...
	 var xlsTool = gPanel.getTool('xls');
	 xlsTool.removeAllListeners();
	 xlsTool.show();
	 xlsTool.on('click',
	  function() {
	    var url = Routing.generate('frontcarto_export_xls');
          //PRODIGE40 '/services/GetXLS/index.php';
		var f = this.control.gridInfoFormPanel.getForm(); 
		queryParams = { 
		  field : Ext.encode(this.node.attributes.infoFieldDesc),
		  data : Ext.encode(this.node.attributes.infoData.data),
		  selectionName : this.node.attributes.layerAlias
		};
		Carmen.Util.doFormUpload(f, url, queryParams);
	  }, 
	  { control : control, node : layerNode } // function scope
	 );
    var csvTool = gPanel.getTool('csv');
	csvTool.removeAllListeners();
	csvTool.show();
	csvTool.on('click',
	  function() {
		var url = Routing.generate('frontcarto_export_xls');
          //PRODIGE40 '/services/GetXLS/index.php';
		var f = this.control.gridInfoFormPanel.getForm(); 
		queryParams = { 
		  field : Ext.encode(this.node.attributes.infoFieldDesc),
		  data : Ext.encode(this.node.attributes.infoData.data),
		  selectionName : this.node.attributes.layerAlias,
		  output : "CSV"
		};
		Carmen.Util.doFormUpload(f, url, queryParams);
	  }, 
	  { control : control, node : layerNode }
	);
     gPanel.reconfigure(gConfig.store, gConfig.columnModel);
     gPanel.on('rowclick', 
      function(grid, rowIndex, e){
        var rec = grid.store.getAt(rowIndex);
        // zooming to the right extent
        var extentStr = rec.get('extent');
        var bounds = Carmen.Util.strExtentToOlBounds(extentStr, Carmen.Control.AdvancedQueryAttributes.PUNCTUAL_RADIUS);
        control.map.zoomToSuitableExtent(bounds, minScale, maxScale);

        // highlighting the feature linked to the row
        var fid = rec.get('fid');
        lnode.attributes.OlLayer.setDisplayMode(
          Carmen.Layer.MapServerGroup.DISPLAY_QUERYMAP, 
          lnode.attributes.layerName, fid);
      });
      
      // registering selection
      olLayer = lnode.attributes.OlLayer;
      lname = lnode.attributes.layerName;
      if (olLayer.handleSelection(lname)) { 
  	    this.map.addToSelectionLayers(olLayer);
  	    olLayer.setSelectionStore(lname, 
  	      new Ext.data.SimpleStore({
            fields: lnode.attributes.infoData.fields,
            data :  lnode.attributes.infoData.data
          }));
      } 
    
    // configuring stats menu...
	var statsBtn = gPanel.view.hmenu.items.get('stats');
	statsBtn.purgeListeners()
	statsBtn.on('click',
		function() {
			var layerName = this.node.attributes.layerName;
			var layerAlias = this.node.attributes.layerAlias;
			var colIndex = this.gridPanel.view.hdCtxIndex;
			var cm = this.gridPanel.getColumnModel();
			var colName = cm.getDataIndex(colIndex);
			var realColName = colName.replace(/_[0-9]+$/,'');
			var colAlias = cm.getColumnHeader(colIndex);
		
			var store = this.gridPanel.getStore();
			
			// collect is a function used to get 
			// column values in a array
			var collect = function(str, fieldname) {
				var res = [];
				var browse = function (r) {
					this.res.push(r.get(this.fieldName));
					return true;
				}
				str.each(browse, 
					{ res : res, fieldName : fieldname });
				return res;
			}
			var data = collect(store, colName);
			var stats = Carmen.Util.stats(data);
			control.queryStatsOnColumn(stats, layerAlias, colAlias);	
		},
		{ node : this, gridPanel : gPanel, control : control }
	);
						 
     if (!gPanel.isVisible()) {
       //console.log('isnotvisible');
       gPanel.show();
       gPanel.syncSize();
       
       gPanel.setWidth(this.win.getWidth()-18);
       //resize grid
       var newHeight = eval(this.win.getEl().getHeight(true)-this.queryPanel.getHeight())-3;
       gPanel.setHeight(newHeight);
       
     }
     else{
       //console.log('isalreadyvisible');
       gPanel.syncSize();
       gPanel.setWidth(this.win.getWidth()-18);
       //resize grid
       var newHeight = eval(this.win.body.getStyle('max-height').replace(/px/,'')-this.queryPanel.getHeight())-3;
       gPanel.setHeight(newHeight);
      }
  }, 
  
  extentRenderer : function(v) {
  	return '<div class="cmnInfoGridZoomIcon"> </div>';
  },


  buildGridConfig: function (fieldsDesc, result) {
    var fields = new Array();
    var columns = new Array();
    
    columns.push(new Ext.grid.RowNumberer());
    columns.push({
      id : 'extent',
      header : '',
      width : 20,
      resizable: true,
      renderer: this.extentRenderer,
      dataIndex: 'extent'
    });
    
    var arrFieldsDesc =  Carmen.Util.chompAndSplit(fieldsDesc,';');
    //console.log(arrFieldsDesc);
    for (var i=0; i<arrFieldsDesc.length; i++) {
    	//console.log(arrFieldsDesc[i]);
    	var desc = Carmen.Util.decodeCarmenFieldDesc(arrFieldsDesc[i]);
    	//console.log(desc);
    	var fDesc = {name : desc.name + '_' + i.toString(), type : Carmen.Util.carmenTypeToExtType(desc.type)};
    	if (desc.type=='date') 
    	 fDesc['dateFormat'] = Carmen.Util.carmenDateTypeToExtDateFormat(desc.type);
    	fields.push(fDesc);
    	
    	var colDesc = {
    		id: Ext.id(), //desc.name + '_' + i.toString(),
    		header: desc.alias,
    		width: 200,
    		sortable: true,
    		resizable: true,
    		renderer: Carmen.Util.carmenTypeToExtRenderer(desc.type),
    		dataIndex: desc.name + '_' + i.toString()
    	};
    	columns.push(colDesc);
    }
    fields.push({name : 'extent'});
    fields.push({name : 'fid'});
    //console.log(fields);
    var config = {
	  	store : new Ext.data.SimpleStore({
	        fields: fields,
	        data : result.data
	    }),
	    columnModel : new Ext.grid.ColumnModel({
	      columns: columns 
	    })
	  };

	  return config;
  },

   
   
   getLayerTreeManager : function() {
    if (this.layertreeManager == null) {
      var controls = this.map.getControlsByClass('Carmen.Control.LayerTreeManager');
      this.layerTreeManager = controls.length==0 ? null : controls[0];
    }
    return this.layerTreeManager;
  },
   
  queryStatsOnColumn : function(stats, layerAlias, colAlias) {
	var labelMapping = {
		"count" : "Effectif",
		"min" : "Minimum",
		"max" : "Maximum",
		"sum" : "Somme",
		"variance" : "Variance",
		"average" : "Moyenne",
		"stddeviation" : "Ecart type"
	};
	var statWin = new Ext.Window({
		id : this.displayClass +'_' +'StatsWindow' + Ext.id(),
		layout:'anchor',
		width:340,
		height: 232,
    constrain : true,
		plain: true,
		title: 'Statistiques pour le champ ' + colAlias + ' de la couche ' + layerAlias,
		modal:false,
		autoDestroy :true,
		resizable:false,
		autoScroll: true,
		closeAction: 'hide',
		shadow : false
	  });
	  var infoPanel = new Ext.form.FormPanel({
		id: this.displayClass +'_' +'StatsInfoPanel' + Ext.id(),
		autoScroll: true,
		fitToFrame: true,
		defaults : {
			labelStyle: 'padding:5px;',
			style: 'font-weight:bold; padding:5px;'
		}
		//labelWidth: 80,
		//labelPad: 3
	  });
	  for (var field in stats) {
		if (labelMapping[field]) {
			var infoField = new Ext.form.DisplayField({
				fieldLabel : labelMapping[field],
				value : stats[field]
			});
			infoPanel.add(infoField);
		}
	  }
	statWin.add(infoPanel);
	statWin.show();
  },
 
  // Filter and transform function used on tree
  filterNodeLayerQueryable :  function(node) {
	var res = true;
	res = ('type' in node.attributes) && 
	  ((node.attributes.type == Carmen.Control.LayerTreeManager.NODE_GROUP && node.hasChildNodes()) ||
	   (node.attributes.type == Carmen.Control.LayerTreeManager.NODE_LAYER && node.attributes.OlLayer.getVisibility(node.attributes.layerName)
	   && node.attributes.infoFields && node.attributes.infoFields.length>0));
	return res;
  },
  
  CLASS_NAME: "Carmen.Control.QuerySpatial"

});




  

