<?php

namespace ProdigeFrontCarto\WebBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use JMS\SecurityExtraBundle\Annotation\Secure;
use JMS\Serializer\SerializationContext;
use Prodige\ProdigeBundle\Services\ConfigReader;
use Symfony\Component\HttpFoundation\Request;
use Prodige\ProdigeBundle\Controller\BaseController;
use ProdigeFrontCarto\HelpersBundle\MapfileToXML\CarmenMapfileToOWC;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author alkante
 */
class DefaultController extends BaseController
{
    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/{service_idx}/{mapfile}/{owslifetime}/{owscontext}", name="frontcarto_index", options={"expose"=true}, requirements={"mapfile"="^.+\.map$", "service_idx"="^\d+$"}, defaults={"service_idx"=1, "owslifetime"=null, "owscontext"=null})
     */
    public function indexAction(Request $request, $service_idx=1, $mapfile, $owslifetime=null, $owscontext=null)
    {
        $config_reader = $this->getConfigReader();
        $config_reader->setParameter("service_idx", $service_idx);
        
        
        $serializer = $this->get('jms_serializer');
        $serializerContext = new SerializationContext();
        $serializerContext->setSerializeNull(true);
        $user = $serializer->serialize($this->getUser(), 'json', $serializerContext);
        
        $fullMapfile = $config_reader->getParameter("PRO_MAPFILE_PATH").$mapfile;
        $htmlcontents = "";
        //TODO BFE check rights in php, not in ajax
        if(strpos($fullMapfile, "MODEL_")===false && strpos($fullMapfile, "TMP_")===false && strpos($fullMapfile, "layers/TMP_")===false && strpos($fullMapfile, "carteperso/")===false){ 
            $VERIFY_RIGHTS = true; 
        }else{
            $VERIFY_RIGHTS = false;
        }
        $CARMEN_URL_SERVER_FRONT = CARMEN_URL_SERVER_FRONT; 
     // look for params transmitted via GET or POST
        $callerParams = $request->get("callerParams", "");
        $strParamJs = explode(';', $callerParams);
        $CARMEN_PARAMS = array();
        foreach($strParamJs as $key=>$param) {
            if(empty($param)) 
                continue;
            list($id, $param) = explode('=', $param);
            $CARMEN_PARAMS[$id] = str_replace('"', '', $param);
        }
        
        $owscontext = implode("/", array_diff(array($owslifetime, $owscontext), array("", null)));
        
        $owsContent = $this->getJsOws($request, $fullMapfile, $owscontext, $htmlcontents, $service_idx);
        if ( !$owsContent ){
        	$this->getLogger()->error(__METHOD__, array("Aucun fichier Contexte trouvé pour la carte ".$fullMapfile));
        	return new Response("Carte non trouvée (Fichier de contexte manquant)", 404);
        }
        //loadContext as PHP XML Object
        $ContextDom = new \DOMDocument();
        $ContextDom->loadXML($owsContent);
        //load from context
        if($ContextDom){
          $ColorNode = $ContextDom->getElementsByTagName("EXT_THEME_COLOR");
          //$xpath = new \DOMXPath($ContextDom);
          //$ColorNode  = $xpath->query($query);
          if($ColorNode && $ColorNode->length>0){
            $overloadExtTheme = trim($ColorNode->item(0)->nodeValue);
          }
        }
        
        //contextual URLs check GET Parameters
        $extent = $request->get("extent", "");
        $group  = $request->get("group",  "");
        $layer  = $request->get("layer",  "");
        $object = $request->get("object", "");
        
        $extent = stripslashes(urldecode($extent));
        $group = stripslashes(urldecode($group));
        $layer = stripslashes(urldecode($layer));
        $object = stripslashes(urldecode($object));
        
        if ($extent!="" && strpos($extent,'(') === FALSE) {
            $extent = "(" . $extent . ")";
        }
        $getExtraParams = "";
        if ($extent!=null || $group!=null || $layer!=null || $object !=null){
            $xpath = new \DOMXPath($ContextDom);
            $query = "//BoundingBox/@minx";
            $minx  = $xpath->query($query)->item(0) ;
            $query = "//BoundingBox/@maxx";
            $maxx  = $xpath->query($query)->item(0) ;
            $query = "//BoundingBox/@miny";
            $miny  = $xpath->query($query)->item(0) ;
            $query = "//BoundingBox/@maxy";
            $maxy  = $xpath->query($query)->item(0) ;
            
            
            
            $extentObj =  $this->defineMapObj($fullMapfile, $extent, $group, $layer, $object);
            if($extentObj){
                $minx->value = $extentObj->minx;
                $maxx->value = $extentObj->maxx;
                $miny->value = $extentObj->miny;
                $maxy->value = $extentObj->maxy;
            }
            $getExtraParams = "&extent=".$extent."&group=".$group."&layer=".$layer."&object=".$object;
        }
        $owsContent = $ContextDom->saveXml();
        
        $strJsOws = xml2json($owsContent);
        $jsOws = json_decode($strJsOws, true);
        
        // layers or carteperso => check mapfile dirname first fragment


        //get uuid from mapfile
        $conn = $this->getProdigeConnection('carmen');
        
        $strSQL = " SELECT map_wmsmetadata_uuid from map where map_file=:mapfile limit 1;";
        $result = $conn->fetchAll($strSQL, array("mapfile"=>str_replace(".map", "", $mapfile)));
        $uuid = "";
        foreach($result as $row){
            $uuid = $row["map_wmsmetadata_uuid"];
        }
        
        $verifParams = array(
            "uuid"         => $uuid,
            "TRAITEMENTS" => "NAVIGATION|PUBLIPOSTAGE",
        );
        if( false === stripos($fullMapfile, 'layers/') ) {
            $verifParams["OBJET_TYPE"]  = "service";
        	  $verifParams["OBJET_STYPE"] = "invoke";
        } else {
            $verifParams["OBJET_TYPE"]  = "dataset";
        }
        
        $CARMEN_CONFIG = array_merge($config_reader->getAllParameters(), array(
          "jsOws" => $jsOws,
          "PRODIGE_VERIFY_RIGHTS_URL" => $this->generateUrl("prodige_verify_rights_url"),
        	"PRODIGE_VERIFY_RIGHTS_URL_PARAMS" => http_build_query($verifParams),
        	"mapfile" => $fullMapfile, // TODO @vlc use basename($fullMapfile) ?
        	"mapfile_name" => $mapfile,
        	"CARMEN_URL_SERVER_DATA" => preg_replace('!/$!', '', str_replace("app_dev.php", "", CARMEN_URL_SERVER_DATA)),
          "PRO_USER_INTERNET_USR_ID" => PRO_USER_INTERNET_USR_ID
        ));
        $mapfile_name = $mapfile;
        
        $BROADCAST = array(
            "CARMEN_URL_CATALOGUE"=>true,
            "CARMEN_URL_ADMINCARTO"=>false
        );
        $arguments = array("user", "BROADCAST", "VERIFY_RIGHTS", "CARMEN_CONFIG", "CARMEN_PARAMS", "mapfile_name", "owsTitle", "CARMEN_URL_SERVER_FRONT", "overloadExtTheme", 
                           "getExtraParams");
        return $this->render('ProdigeFrontCartoWebBundle:Default:index.html.twig', compact($arguments));
    }
    
    
    private function getJsOws(Request $request, $mapfile, $context=null, &$htmlcontents="", $service_idx=1)
    {
        $serviceUrl = $this->getPathToIHMDirectory();
        $serviceFullUrl = $this->getUrlToIHMDirectory();
        $xmlStringContents ="";        
        // checking for context file
        if ( $context ) {
          // yes, we get it...
            $contextPath = CARMEN_PATH_CONTEXT . '/' . $context;
            if (!file_exists($contextPath)) {
                $htmlcontents .= "<font color='red'>Erreur : Contexte introuvable : " . $contextPath . "</font>";
                return;
            }
            else {
                // merging context with original mapfile to take into accounts updates of map 
                $mtx = new CarmenMapfileToOWC($mapfile, $this->getUrlToMapserver(), $mapfile, $serviceFullUrl);
                  
                $xmlStringContents =  file_get_contents($contextPath);
                updateWfsDataSourceFromContext($xmlStringContents); 

                $owsFile = CARMEN_PATH_OWSCONTEXT."/".$service_idx."_".substr(str_replace("/", "_", $request->get("mapfile")), 0, -4).".ows";
                $mapStringContents = (file_exists($owsFile) ? file_get_contents($owsFile) :"");
                
                if($mapStringContents!=""){
                    $xmlStringContents = $mtx->mergeWithContext($xmlStringContents, $mapStringContents);
                }
            }
        }
        else {
            
            //no we build one from mapfile
            // translate mapfile in ows
            /*$mtx = new CarmenMapfileToOWC($mapfile, $this->getUrlToMapserver(), $mapfile, $serviceFullUrl,
                                          $extent, $group, $layer, $object);
            $xmlStringContents =  $mtx->buildXML();*/
            $contextFile = basename($mapfile, ".map");
            
            $owsFile = CARMEN_PATH_OWSCONTEXT."/".substr(str_replace("/", "_", $request->getPathInfo()), 1, -4).".ows";
            if(!file_exists($owsFile)){
              //For the moment ows file generated
              /*$mtx = new CarmenMapfileToOWC($mapfile, $CARMEN_URL_SERVER_DATA . $MAPSERV_URL_PATH, 
                                            $serviceFullUrl, $CARMEN_URL_SERVER_METADATA."/",
                                            $extent, $group, $layer, $object, $id);
              $xmlStringContents =  $mtx->buildXML();

              //obedel useful for debugging
              file_put_contents($owsFile, $xmlStringContents);*/
              $htmlcontents .= "<font color='red'>Erreur : Contexte manquant </font>";
              return;
            }else{
              //loading from ows file
              $xmlStringContents = file_get_contents($owsFile);
            }
        }
        
        //translate ows in json
        //$strJsOws =  xml2json::transformXmlStringToJson($xmlStringContents);
        
        return $xmlStringContents;
    }
    
    /**
     * brief : update layers status according to contextual parameters and
     * 	return an initial extent defined from parameter extent or calculated
     * 	from parameter object
     * @param extent : initial extent encoded as 'minx,miny,maxx,maxy"
     * @param group : ';' separated list of layer groups that should be set to ON
     * @param layer : ';' separated list of layers that should be set to ON
     * @param object : encoded string ("layer;field;value") describing object(s)
     * which enclosing bounding box will serve as initial extent view on the map
     *	@return the initial extent view.
     */
    private function defineMapObj($mapFile, $extent, $group, $layer, $object)
    {

        $oMap = ms_newMapObj($mapFile);
        $oExtentMax = ms_newRectObj();
        $oExtentMax->setExtent($oMap->extent->minx,
            $oMap->extent->miny,
            $oMap->extent->maxx,
            $oMap->extent->maxy);
        $oExtentInitial = NULL;
        
        if ($object!="") {
            //request
            $selExtentLayer = ms_newRectObj();
            $selExtentLayer->setExtent(MAX_DOUBLE, MAX_DOUBLE, MIN_DOUBLE, MIN_DOUBLE);
            $tabParamQuery = explode(";", $object);
            $layerName = trim($tabParamQuery[0]); 
            
            $qstring = $tabParamQuery[1] . "=" . '"' . $tabParamQuery[2] . '"';
            //for ($i = 0; $i < $oMap->numlayers; $i++){
            if( $msLayer = @$oMap->getLayerByName($layerName)){
                //$layerTitle = $msLayer->getMetadata("CARMEN_LAYER_TITLE");
                //if(strcmp(trim($layerTitle), mapfileEncode(trim($tabParamQuery[0])))==0){
                    // activate queried layer
                   
                $msLayer->set("status", MS_ON);
                switch($msLayer->connectiontype) {
                  case MS_SHAPEFILE :
                  case MS_TILED_SHAPEFILE :
                      // redefining the connection as an ogr connection
                      // to allow complex query expressions via DATA sql restrictions
                      $datasetName = datasetNameFromFilename(OWSEncode($msLayer->data));
                      $resSetConnection = @$msLayer->set("connection", mapfileEncode(forceShapefileExtension(OWSEncode($msLayer->data))));
                      @$msLayer->set("connectiontype", MS_OGR);
                      $sqlStr = 'SELECT * FROM ' . $datasetName . ' WHERE ' . stripslashes($qstring);
                      @$msLayer->set("data", mapfileEncode($sqlStr));
                      $queryMode = QM_OGR_FILTER;
                      $res = (@$msLayer->open()==MS_SUCCESS);
                      $debugQstring = $sqlStr . ' ' . $msLayer->connection;
                      break;
                  case MS_OGR :
                      // defining or completing the DATA sql restrictions
                      $datasetName = datasetNameFromFilename(OWSEncode($msLayer->connection));
                      $sqlStr = 'SELECT * FROM ' . $datasetName . ' WHERE ' . stripslashes($qstring);
                      @$msLayer->set("data", mapfileEncode($sqlStr));

                      //echo mapfileEncode($sqlStr) . "\n";

                      $queryMode = QM_OGR_FILTER;
                      $res = (@$msLayer->open()==MS_SUCCESS);
                      // todo : look if there is already a restriction
                      // but not simple to handle cos LEFT JOIN and so on...
                      // if (isset($msLayer->data) && strlen(($msLayer->data))>0)
                      $debugQstring = $sqlStr;
                      break;
                  case MS_POSTGIS :
                      $queryMode = QM_MAPSERVER_QUERY;
                      //$qstring = $tabParamQuery[1]. '::text' . '=' . '\'' . $tabParamQuery[2] . '\'';
                      $res = (@$msLayer->queryByAttributes(mapfileEncode($tabParamQuery[1]), mapfileEncode($tabParamQuery[2]) ,MS_MULTIPLE)==MS_SUCCESS);
                      if ($res)
                          $res = (@$msLayer->open()==MS_SUCCESS);
                      $debugQstring = $qstring;
                      break;
                  default:
                      // general case : handling query with mapscript queryByAttributes
                      $queryMode = QM_MAPSERVER_QUERY;
                      $qstring = $tabParamQuery[2];
                      $res = (@$msLayer->queryByAttributes(mapfileEncode($tabParamQuery[1]), mapfileEncode($tabParamQuery[2]) ,MS_MULTIPLE)==MS_SUCCESS);
                      if ($res)
                          $res = (@$msLayer->open()==MS_SUCCESS);
                      $debugQstring = $qstring;
                      break;
                }
                if ($res){
                    //echo " " . $res . "\n";
                    $count=0;
                    switch($queryMode) {
                      case QM_OGR_FILTER:
                          $status = @$msLayer->whichShapes($oMap->extent);
                          //print_r($oMap->extent);
                          //echo "layer status " . $msLayer->status . "\n";
                          //echo "query status " . $status . "\n";
                          //print_r($msLayer->getExtent());
                          $count=0;
                          while ($shape = @$msLayer->nextShape())
                          {

                              //print_r($selExtentLayer);
                              //print_r($feat->bounds);

                              $selExtentLayer = mergeRectObj($selExtentLayer, $shape->bounds);
                              //print_r($selExtentLayer);
                              $shape->free();
                              $count++;
                          }
                          //echo $count . "\n";
                          break;
                      case QM_MAPSERVER_QUERY:
                          $count = @$msLayer->getNumResults();
                          for ($j=0;$j<$count;$j++) {
                              $resObj = @$msLayer->getResult($j);
                              $feat = @$msLayer->getShape($resObj);
                              $selExtentLayer = mergeRectObj($selExtentLayer, $feat->bounds);
                              $feat->free();
                          }
                          break;
                    }
                    if ($count>0) {
                        $oExtentInitial = $selExtentLayer;
                    }
                    $msLayer->close();
                }
            }
        }
        else
        {
            if($extent!=""){
                $oExtentInitial = shapeStrToRectObj($extent);
            }
        }
    
    
        if ($group!="" || $layer!="") {
            //not done yet 
            return false;
            $tabGroups = explode(";", mapfileEncode($group));
            $tabLayers = explode(";", mapfileEncode($layer));
            for ($i = 0; $i < $oMap->numlayers; $i++){
                $msLayer = $oMap->getLayer($i);
                $giArboGroup = $msLayer->getMetadata("CARMEN_LAYER_GRP");
                $layerTitle = $msLayer->getMetadata("CARMEN_LAYER_TITLE");
                if ($group != "") {
                    foreach($tabGroups as $key => $value) {
                        //$group = trim(stripslashes(urldecode($group)));
                        //echo $group . "    ";
                        //echo $giArboGroup;
                        //echo '<br/>';
                        if(strpos($giArboGroup, trim($value))!==false) {
                            $msLayer->set("status", MS_ON);
                        }
                    }
                }
                if($layer!="") {
                    foreach($tabLayers as $key => $value){
                        //$value = trim(stripslashes(urldecode($value)));
                        //echo $value;
                        //echo $layerTitle . "   ";
                        //echo '<br/>';
                        if (strcmp(trim($layerTitle),trim($value))==0) {
                            $msLayer->set("status", MS_ON);
                        }
                    }
                }
            }
        }
        return $oExtentInitial;
    }
}
