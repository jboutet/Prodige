<?php
namespace ProdigeFrontCarto\HelpersBundle\Controller;

use Prodige\ProdigeBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\DBAL\Connection;
use Prodige\ProdigeBundle\Common\Mail\AlkMail;
use Prodige\ProdigeBundle\Controller\User;

/**
 * Publipostage class definition
 */
class PublipostageController extends BaseController
{
    const DEBUG_MAIL = false;
    const DEBUG_MAILTO = "m.artigny@alkante.com";
    /**
     * destructor
     */
    public function __destruct() 
    {
        //
    }
    
    /**
     * private methods
     */
    
    /**
     * Get the connection string
     * 
     * @return string : $conn_string
     */
    private function get_conn_string() 
    {
        $dbInfo = $_SESSION ["service"] ['dbInfo'];
        $conn_string = "no DB config " . print_r($_SESSION ["service"], true);
        
        if ($dbInfo != Null) {
            $conn_string = "";
            foreach($dbInfo as $key => $val) {
                if ($key != "name") {
                    $conn_string .= $key . "=" . $val . " ";
                }
            }
        }
        return $conn_string;
    }
    
    
    /**
     * Write statistique in csv file
     */
    private function writeCSVFile($filename, $row) {
        $exists = (file_exists($filename) && is_file($filename) ? true : false);
        $file = fopen($filename, 'a+');
        if (!$file) {
            return;
        }
        if (!$exists) {
            fwrite($file, '"Date/Heure","Adresse IP","Nom","Prénom","Login","Donnée concernée","Nombre de destinataires du message"' . "\n");
        }
        $line = "";
        $sep = "";
        foreach($row as $value) {
            $line .= $sep . '"' . str_replace('"', '', $value) . '"';
            $sep = ',';
        }
        fwrite($file, $line . "\n");
        fclose($file);
    }
    
    /**
     * adds a log when a mail is sent
     * 
     * @param string $adrIP
     *            ip address
     * @param string $name
     *            name of user who sends the mail
     * @param string $given
     *            given name of user who sends the mail
     * @param string $login
     *            login of user who sends the mail
     * @param string $layerName
     *            name of layer concerned by the mail
     * @param int $nbTo
     *            number of recipient
     */
    public function addLog($adrIP, $name, $given, $login, $layerName, $nbTo) {
    	
        $logFilename = $this->getPathToWebLogs()."statistiques.csv";
        $row = array (
                date("d/m/Y H:i:s"),
                $adrIP,
                $name,
                $given,
                $login,
                $layerName,
                $nbTo 
       );
        $this->writeCSVFile($logFilename, $row);
    }
    
    /**
     * Send mail by using AlkMail class
     *
     * //public function sendMails($msg_id, $from, $to_name, $to_mail, $subject, $txtBody) {
     * private function sendMails($from, $to_name, $to_mail, $subject, $body) {
     *
     * $oMail = new AlkMail();
     *
     * $oMail->setFrom($from[0], $from[1]);
     * $oMail->addTo($to_name, $to_mail);
     * $oMail->setSubject($subject);
     * $oMail->setBody($body); // html body
     * $oMail->transformBody(); // calcul de la version text/plain
     *
     * //$oMail->setAltBody($body); // text body
     * // for attachments files
     * //$oMail->addFile($strFiles[1], $strFiles[0]);
     * //foreach ($strfiles as $strFileName => $strPathFileName) {
     * // $oMail->addFile($strPathFileName, $strFileName);
     * //}
     * $iRes = $oMail->sendMailToQueue("-1");
     * return $iRes;
     * //echo "\nNb d'envoyer : ".$iRes."\n<br>";
     * //echo "php.ini - sendmail_path = ".ini_get("sendmail_path")."\n<br>";
     * }
     */
    
    /**
     * publics methods (suffix action .
     * ..)
     */
    
    /**
     * action : getJsonStore_Models
     * 
     * @return JsonStore for registred models
     */
    public function getJsonStore_ModelsAction(Request $request) 
    {
        $resultArray = array();
        $dbconn = $this->getProdigeConnection();
        
        if (!$dbconn) {
            // echo "t0";die();
        } else {
            $layerName = $request->get("layerTable", "");
            
            $schema = "parametrage";
            $tableName = "prodige_msg_template";
            $strSQL = " SELECT pk_modele_id, modele_name, msg_title, msg_body" . 
                      " FROM " . $schema . "." . $tableName . 
                      " WHERE layer_name like :layer_name ORDER BY modele_name";
            
            $result = $dbconn->fetchAll($strSQL, array("layer_name"=>$layerName));
            foreach($result as $row){
                $resultArray[] = $row; 
            }
        }
        
        return new JsonResponse($resultArray);
    }
    
    /**
     * action : supprimer
     * 
     * @return boolean true if supprimerAction was successful, false ifelse
     */
    public function supprimerAction() 
    {
        $modeleId = Request("modeleId", REQ_GET_POST, "-1", "is_numeric");
        $resultAction = array (
                "msg" => "",
                "success" => false 
       );
        if ($modeleId >= 0) {
            $dbconn = $this->getProdigeConnection();
            if (!$dbconn) {
                $resultAction ["msg"] = "Impossible de se connecter à la base.";
            } else {
                $strSQL = "DELETE FROM parametrage.prodige_msg_template WHERE pk_modele_id=" . $modeleId;
                $result = pg_query($dbconn, $strSQL);
                if (!$result) {
                    $resultAction ["msg"] = "Une erreur est survenue lors de la suppression du modèle.";
                } else {
                    $resultAction ["msg"] = "Le modèle a été supprimé avec succès.";
                    $resultAction ["success"] = true;
                }
            }
        } else {
            $resultAction ["msg"] = "Modèle non supprimé.<br>Des informations nécessaires à la suppression sont manquantes.";
        }
        echo json_encode($resultAction);
    }
    
    /**
     * action : tester
     * 
     * @return int number of sent emails
     */
    public function testerAction(Request $request) 
    {
        return $this->envoyerAction($request, true);
    }
    
    /**
     * action : envoyer
     * 
     * @return int : number of sent emails
     */
    public function envoyerAction(Request $request, $bTest = false) 
    {
        
        !defined("ALK_PATH_MAILING_QUEUE") && define("ALK_PATH_MAILING_QUEUE", $this->getParameter("PRODIGE_PATH_DATA")."/MAILQUEUE/");
        defined("ALK_PATH_MAILING_QUEUE") && ALK_PATH_MAILING_QUEUE && @mkdir(ALK_PATH_MAILING_QUEUE, 0770, true);
        
        $PRO_PUBLIPOSTAGE_AUTOMATIC_MESSAGE = $this->getProdigeConnection("parametrage")->fetchColumn("SELECT prodige_settings_value FROM parametrage.prodige_settings WHERE prodige_settings_constant = :prodige_settings_constant", array("prodige_settings_constant"=>"PRO_PUBLIPOSTAGE_AUTOMATIC_MESSAGE"));
        
        $fidsList      = trim($request->get("fidsList", ""), ' ,');
        $fieldMailTo   = $request->get("champCourrielDestinataire", "");
        $fieldNameTo   = $request->get("champNomDestinataire", "");
        $nameFrom      = $request->get("nomExpediteur", "");
        $mailFrom      = $request->get("courrielExpediteur", "");
        
        $objectMail    = $request->get("objectMessage", "");
        $bodyMail      = $request->get("bodyMessage", "");
        
        $schema        = "public";
        $tableName     = $request->get("layerTable", "");
        $modelName     = $request->get("nomModele", "");
        $userName      = $request->get("userName", "");
        $userGiven     = $request->get("userGiven", "");
        $userLogin     = $request->get("userLogin", "");
        $userSignature = $request->get("userSignature", User::GetUserSignature());
        $userIP        = (isset($_SERVER ["REMOTE_ADDR"]) ? $_SERVER ["REMOTE_ADDR"] : "");
        
        if ( $userSignature=="" ){
            $CATALOGUE = $this->getCatalogueConnection();
            $exists = $CATALOGUE->fetchColumn("select true from information_schema.schemata where schema_name='alkanet'");
            $user = User::getUser();
            if ( $exists!==false && $user ){
                $userSignature = $CATALOGUE->fetchColumn("select coalesce(nullif(agent_signature, ''), agent_prenom||' '||agent_nom) from alkanet.sit_agent where agent_id=:agent_id", array("agent_id"=>User::GetUserId()));
            }
        }
        $tableName = explode(".", $tableName);
        $tableName = end($tableName);
        
        $resultAction = array(
            "msg" => "",
            "success" => false 
        );
        
        if ($fidsList != "" && $fieldMailTo != "" && $fieldNameTo != "" && $nameFrom != "" && $mailFrom != "" && $objectMail != "" && $tableName != "") {
            
            $dbConn = $this->getProdigeConnection();
            if (!$dbConn) {
                $resultAction ["msg"] = "Impossible de se connecter à la base";
            } else {
                // search signature and appli name
                $appliName = "";
                $mailSignature = ($PRO_PUBLIPOSTAGE_AUTOMATIC_MESSAGE ? $PRO_PUBLIPOSTAGE_AUTOMATIC_MESSAGE : "");
                $userSignature = nl2br($userSignature);
                
                $tabAssoc = array(
                    "application" => $appliName,
                    "signature" => $userSignature,
                    "messageAuto" => $mailSignature,
                    "saveMsgSentParam" => '--ip="' . $userIP . '" --userName="' . $userName . '" --userGiven="' . $userGiven . '" --userLogin="' . $userLogin . '" --tableName="' . $tableName . '"' 
                );
                
                $oMail = new AlkMail();
                
                $oMail->setFrom($nameFrom, $mailFrom);
                $oMail->setTabBodyAssoc($tabAssoc);
                $oMail->setAcceptDoubloonRecipient(true);
                $oMail->setSubject($objectMail);
                $oMail->setBody($bodyMail); // html body
                $oMail->transformBody(); // calcul de la version text/plain
                                         
                // for attachments files
                                         // $oMail->addFile($strFiles[1], $strFiles[0]);
                                         // foreach ($strfiles as $strFileName => $strPathFileName) {
                                         // $oMail->addFile($strPathFileName, $strFileName);
                                         // }
                                         
                // finds all the keys/values usable
                $strSQL = "SELECT * FROM " . $schema . "." . $tableName . " WHERE gid IN( :fidsList )";
                $result = $dbConn->fetchAll(
                	$strSQL, 
                	array("fidsList"=>explode(",", $fidsList)), 
                	array("fidsList"=>\Doctrine\DBAL\Connection::PARAM_INT_ARRAY)
                );
                
                
                $nbAdd = 0;
                foreach($result as $row) {
                    $mailTo = ($bTest ? $mailFrom : (isset($row [$fieldMailTo]) ? $row [$fieldMailTo] : ""));
                    $nameTo = ($bTest ? $nameFrom : (isset($row [$fieldNameTo]) ? $row [$fieldNameTo] : ""));
                    
                    $tabAssoc = array();
                    foreach($row as $strKey => $strValue) {
                        if ($strKey != "the_geom") {
                            $tabAssoc [$strKey] = trim($strValue);
                        }
                    }
                
                    if ( self::DEBUG_MAIL ){
                        $nbAdd += $oMail->addTo($nameTo, self::DEBUG_MAILTO, $tabAssoc);
                    } else {
                        $nbAdd += $oMail->addTo($nameTo, $mailTo, $tabAssoc);
                    }
                    
                    if ($bTest && $nbAdd > 0) {
                        // sends only one message in test mode.
                        break;
                    }
                }
                
                $nbTo = $oMail->send("1"); // 1 to force the send of mail by the queue
                
                $addToQueue = false;
                if ($nbTo < - 10) {
                    $addToQueue = true;
                    $nbTo = - $nbTo - 10;
                }
                
                $msg = ($bTest ? "Le message de test a été envoyé avec succès à l'adresse de l'expéditeur." : "Le message a été envoyé avec succès à " . $nbTo . " destinataires.");
                if (!$addToQueue) {
                    if (!$bTest) {
                        $this->addLog($userIP, $userName, $userGiven, $userLogin, $tableName, $nbTo);
                    }
                } else {
                    $msg = "La préparation du message à envoyer s'est déroulée avec succès.<br>" . $nbTo . " destinataire" . ($nbTo > 1 ? "s ont" : " a") . " été trouvé" . ($nbTo > 1 ? "s" : "") . "<br>" . "Vous serez informé par messagerie lorsque l'envoi sera terminé.";
                }
                
                $resultAction = ($nbTo > 0 
                    ? array(
                        "msg" => $msg,
                        "success" => true 
                    ) 
                    : array(
                        "msg" => "Aucun destinataire n'a été trouvé. Message non envoyé.<br>" . "Veuillez vérifier votre sélection des champs nom et courriel destinataires.",
                        "success" => false 
                    )
                );
                
                if ($resultAction ["success"] && $modelName != "" && !$bTest) {
                    $parameters = array(
                      "modele_name" => $modelName,
                      "msg_title"   => $objectMail,
                      "msg_body"    => $bodyMail,
                      "layer_name"  => $tableName,
                    );
                    $strSQL = $dbConn->createQueryBuilder()->insert("parametrage.prodige_msg_template");
                    foreach ($parameters as $param=>$value){
                    	$strSQL->setValue($param, ":".$param)->setParameter(":".$param, $value);
                    }
                    $result = $strSQL->execute();
                    if (!$result) {
                        $resultAction ["msg"] .= "<br>Une erreur est survenue lors de l'enregistrement du modèle.";
                    } else {
                        $resultAction ["msg"] .= "<br>Le modèle a été enregistré avec succès.";
                    }
                }
            }
        } else {
            $resultAction ["msg"] = "Message non envoyé.<br>Des informations nécessaires à l'envoi sont manquantes.";
        }
        return new JsonResponse($resultAction);
    }
}
