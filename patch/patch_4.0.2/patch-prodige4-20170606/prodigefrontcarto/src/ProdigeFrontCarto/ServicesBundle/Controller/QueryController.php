<?php

namespace ProdigeFrontCarto\ServicesBundle\Controller;

use Prodige\ProdigeBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use ProdigeFrontCarto\ServicesBundle\Services\QueryByShape;
use Symfony\Component\HttpKernel\HttpKernel;

/**
 * @Route("/frontcarto/query")
 */
class QueryController extends BaseController 
{

    /**
     * TODO convertToSymfony2
     *
     * @Route("/advancedqueryattributes", name="frontcarto_query_advancedqueryattributes", options={"expose"=true})
     * @Method({"GET", "POST"})
     * 
     * @param Request $request  The request object.
     * 
     * @return JsonResponse Json object.
     *
     * @see /home/devperso/bfontaine/prodige3.4/prodigefrontcarto/services/AdvancedQueryAttributes/index.php
     *
     * #@used IHM/JavaScript/CarmenJS/Carmen/Application.js={426}
     * #@used IHM/JavaScript/CarmenJS/Carmen/Control/AdvancedQueryAttributes.js={651; 1147}
     */
     public function queryByAttributes(Request $request)
     {
       ?><?php

// /**
//  * QueryAttributes Service
//  */

// require_once("./../../lib/lib_session.php");
// require_once("./../../app_conf.php");
// require_once("./../../lib/lib_request.php");
// require_once("./../../lib/lib_service.php");
// require_once("./../util.php");


// set_time_limit(600);
// set_error_handler("handle_error", E_ALL | E_STRICT);
// if (!loadService())
//   trigger_error(CARMEN_ERROR_SESSION_EXPIRED . "|" . CARMEN_MSG_SESSION_EXPIRED . "|CARMEN_ERROR", E_USER_ERROR);


// Retrieving parameters
$mapfile = $request->get("map", "");
$layer =  $request->get("layer", "");
$qitem =  $request->get("qitem", "");
$qstring = $request->get("qstring", "");
$qOgrString = $request->get("qOgrString", "");
$op         = $request->get("op", "");
$pattern = $request->get("pattern", "");
$fieldsJSON = stripslashes($request->get("fields", ""));
$fieldsList = json_decode($fieldsJSON, true);
// nb limite lie à la cb
$valueLineLimit = $request->get("valueLineLimit", PRO_REQUETEUR_NB_LIMIT_OBJ) ?: PRO_REQUETEUR_NB_LIMIT_OBJ;

// listvalpossible : param ajax : pour obtenir une liste de valeur dans le combo
//$listvalpossible = $request->get("listvalpossible", "");
//echo $mapfile."_______".$layer."******".$qitem."---".$qstring; die();

$queryMode = "";

if ($mapfile==""
		|| $layer==""
		|| $qitem==""
		|| $qstring==""
		|| $op=="" 
		|| $pattern==""
		|| $fieldsList==false
) {
  trigger_error(CARMEN_ERROR . "|Missing or wrong parameters in queryAttributes call.|CARMEN_ERROR", E_USER_ERROR);
}

// building queryfilename
$queryfileName = datasetNameFromFilename($mapfile) . '_' . $_SESSION["service"]['idx'] . '_' . session_id() . '.qy';
$queryfileOgrName = datasetNameFromFilename($mapfile) . '_' . $_SESSION["service"]['idx'] . '_' . session_id() . '.map';
$queryfileFullPath = CARMEN_DATA_PATH_CACHE . $queryfileName;
$queryfileOgrFullPath = CARMEN_DATA_PATH_CACHE . $queryfileOgrName;



//debug
$debugQstring = "";
$comment = "";

$totalCount = 0;

$oMap = ms_newMapObj($mapfile);
$strObjJson = "{";

$selExtent = ms_newRectObj();
$selExtent->setExtent(MAX_DOUBLE, MAX_DOUBLE, MIN_DOUBLE, MIN_DOUBLE);


$layer = $oMap->getLayerByName($layer);
if(@$oMap->getLayerByName("layerOgr"))
  $layerOgr = $oMap->getLayerByName("layerOgr");
else
  $layerOgr = ms_newLayerObj($oMap, $layer);
$layerOgr->set("name", "layerOgr");

$layerOgr->updateFromString(
              "LAYER
              TYPE POINT    
              METADATA
              END # METADATA
              VALIDATION
                'labelrequires' '[01]'
              END
              END # LAYER");

$res = false;


$dataInEncode = 'MapfileEncode';
$dataOutEncode = 'OWSEncode';
//setting up the query
if ($layer) {
  // depending on the connection type the qstring does not play the same role
  // for postgis -> where clause
  // elsewhere -> regex to test qitem
  // so complex queries have to be passed as data filter for non postgis layer...
  
	// with mapserver client wfs layers, mapserv seems to automatically 
  // encode data from iso to utf8 in memory... so we we must not do an 
  // extra encoding utf8->iso transformation
  if ($layer->connectiontype == MS_WFS ||	$layer->getMetaData("LAYER_ENCODING")=="UTF-8") {
		$dataInEncode = 'NoEncode';
		$dataOutEncode = 'NoEncode';
  }  
  switch($layer->connectiontype) {
    case MS_SHAPEFILE :
    case MS_TILED_SHAPEFILE :
      // redefining the connection as an ogr connection
      // to allow complex query expressions via DATA sql restrictions
      if($queryMode == QM_OGR_FILTER){
        //Dans le cas d'une requete OGR on rajoute un novueau layer comportant la selection
        $layerData = (preg_match('/^'.preg_quote(CARMEN_URL_PATH_DATA, '/').'/', $layer->data) ? $layer->data : CARMEN_URL_PATH_DATA."cartes/Publication/wfs/temp/".substr($layer->data, 2));
      	$datasetName = datasetNameFromFilename($dataOutEncode($layerData));
      	$resSetConnection = $layer->set("data", $dataInEncode(forceShapefileExtension($dataOutEncode($layerData))));
      	$resSetConnection = $layerOgr->set("connection", $dataInEncode(forceShapefileExtension($dataOutEncode($layerData))));
      	$layerOgr->setConnectiontype(MS_OGR);
      	
      	
      	// building qstring
      	$qstring = str_replace("PATTERN", $pattern, $qstring);
      	$qstring = str_replace("FIELD", $qitem, $qstring);
		
      	$sqlStr = 'SELECT * FROM ' . $datasetName;
      	$layerOgr->set("data", $dataInEncode($sqlStr));
      	$layerOgr->setFilter($dataInEncode($qOgrString));

      	$queryMode = QM_OGR_FILTER;
      	$oClass = $layerOgr->getClass(0);
      	$oClass->deleteStyle(0);
      	$oStyle = ms_newStyleObj($oClass);
      	$oStyle->set("size", 5);
      	$oStyle->color->setRGB(255, 255, 0);
      	$oStyle->set("symbolname", "Cercle");
      	$oStyle->set("width", "1");
      	$oStyle->outlinecolor->setRGB(0, 0, 0);
        $res = ($layerOgr->open()==MS_SUCCESS);
      	$debugQstring = $sqlStr . ' ' . $layerOgr->connection;
      }else{
      	//Sinon rien ne change
        $datasetName = datasetNameFromFilename($dataOutEncode($layer->data));
      
        $resSetConnection = $layer->set("connection", $dataInEncode(forceShapefileExtension($dataOutEncode(CARMEN_URL_PATH_DATA."cartes/Publication/wfs/temp/".substr($layer->data, 2)))));
        $layer->setConnectiontype(MS_OGR);
      
        // building qstring
        $qstring = str_replace("PATTERN", $pattern, $qstring);
        $qstring = str_replace("FIELD", $qitem, $qstring);
      
        $sqlStr = 'SELECT * FROM ' . $datasetName . ' WHERE ' . stripslashes($qstring);

        $layer->set("data", $dataInEncode($sqlStr));
        $queryMode = QM_OGR_FILTER;

        $res = ($layer->open()==MS_SUCCESS);
        $debugQstring = $sqlStr . ' ' . $layer->connection;
      }
      //$comment = "\"SHP " .  $sqlStr . "\"";
      break;
    case MS_OGR :
      // defining or completing the DATA sql restrictions
      $datasetName = datasetNameFromFilename($dataOutEncode($layer->connection));
      //$qstring = 'Nombre_Chaussées like \'%' . utf8_decode('é') . '%\'';  
      
      // building qstring
      $qstring = str_replace("PATTERN", $pattern, $qstring);
      $qstring = str_replace("FIELD", $qitem, $qstring);
      
      $sqlStr = 'SELECT * FROM ' . $datasetName . ' WHERE ' . stripslashes($qstring);
      $comment = $sqlStr;
      $layer->set("data", $dataInEncode($sqlStr));
      $queryMode = QM_OGR_FILTER;
      $res = (@$layer->open()==MS_SUCCESS);
      $comment = "\"OGR " .  $res . "\"";
      // todo : look if there is already a restriction
      // but not simple to handle cos LEFT JOIN and so on...
      // if (isset($layer->data) && strlen(($layer->data))>0)
       $debugQstring = $sqlStr;
      break;
    case MS_POSTGIS :
      $queryMode = QM_MAPSERVER_QUERY;
      // cas de recherhce unbiqt sur la couche : on interroge toute la couche
  	  if($qitem==-1){
  		  $qitem="";
  	  }
//           if($qitem!="")
//       	$qstring = str_replace($qitem."]", $qitem.']::text', $qstring);
      $qstring = "(" . stripslashes($qstring) . ")"; 
      
      if ( $qitem ){
        $res = (@$layer->queryByAttributes($qitem, $qstring ,MS_MULTIPLE)==MS_SUCCESS);
        $res = $res && (@$layer->open()==MS_SUCCESS);
      } else {
        $layerOgr = $layer;
      	$res = (@$layer->open()==MS_SUCCESS);
      	
        $queryMode = QM_OGR_FILTER;
      }     
      
      $comment = "\"POSTGIS " .  $res . "\"";
      $debugQstring = $qstring;
      break;
    default:
      // general case : handling query with mapscript queryByAttributes
      $queryMode = QM_MAPSERVER_QUERY;      
      $qstring = str_replace($qitem.']', $qitem.']::text', $qstring);
      $qstring = "\"" . stripslashes($qstring) . "\"";
      $res = (@$layer->queryByAttributes($dataInEncode($qitem), $dataInEncode($qstring) ,MS_MULTIPLE)==MS_SUCCESS);
      if ($res)
        $res = (@$layer->open()==MS_SUCCESS);
      $debugQstring = $qstring;
      $comment = "\"OTHER " .  $res . "\"";
      break;
  }
}

$selExtentLayer = ms_newRectObj();
$selExtentLayer->setExtent(MAX_DOUBLE, MAX_DOUBLE, MIN_DOUBLE, MIN_DOUBLE);
  	
$strObjJson = array();
if ($res) {

  $fields = $fieldsList;
  $layerFields = $layer->getItems();
  
  $escapeFields = array_map("escape_quote", $fields);
  $quoteFields = str_replace('"', '', array_map("getName", $escapeFields));
  $strObjJson = array(
    "fields" => array_merge($quoteFields, array("extent", "fid")),
    "data" => array()
  );
  $keyField = $quoteFields[0]; 
  for($j=0; $j<count($quoteFields); $j++) {
      if ( strtolower($quoteFields[$j])==strtolower($qitem) ) {
      	$keyField = $quoteFields[$j];
      	break;
      }
  }
      

  switch($queryMode) { 
    case QM_OGR_FILTER:
      $debugString = 'mode_OGR' . $debugQstring;
      
      $status = $layerOgr->whichShapes($layerOgr->getExtent());
      $count=0;
      $selectedShapes = array();
      $selectedTiles = array();
      while ($shape = $layerOgr->nextShape())
      {
        $values = array();
        $dataKey = $shape->values[$keyField];
        
        for($j=0; $j<count($fields); $j++) {
          // TODO : strange behaviour with ogr data source
          // seems to need to force field name conversion to utf-8 to handle special chars éè...
          if ($fields[$j]["type"]=="URL") {
            $values[] = array("href" => fill_template($fields[$j]["url"], $shape->values, $layerFields),
            		           "text" => escape_quote($dataOutEncode($shape->values[$dataInEncode($fields[$j]["name"])])));
          }
          else if ($fields[$j]["type"]=="IMG" || $fields[$j]["type"]=="Image") {
			      $values[] = fill_template($fields[$j]["url"], $shape->values, $layerFields);
          }
          else {
            $values[] = escape_quote($dataOutEncode($shape->values[$dataInEncode($fields[$j]["name"])]));
          }       
        }

        $selExtentLayer = mergeRectObj($selExtentLayer, $shape->bounds);

        $values[] = $dataOutEncode(rectObjToStr($shape->bounds));
        $values[] = $dataOutEncode($shape->index);
        

        array_push($selectedShapes, $shape->index);
        array_push($selectedTiles, $shape->tileindex);
        $shape->free();
        
        if ( !array_key_exists($dataKey, $strObjJson["data"]) )
        	$count++;
        $strObjJson["data"][$dataKey] =  $values;   
        if ( $count>$valueLineLimit ) break;
      }
      $oMap->save($queryfileOgrFullPath);
      
      $layer->close();
      if ($layer!==$layerOgr) $layerOgr->close();
      
      break;
    case QM_MAPSERVER_QUERY:
      $count = 0;
      
      for ($i=0;$i<$layer->getNumResults();$i++) {
        $resObj = $layer->getResult($i);
        $feat = $layer->getShape($resObj);
        $dataKey = $feat->values[$keyField];
        
        $values = array();
        for($j=0; $j<count($fields); $j++) {
          if ($fields[$j]["type"]=="URL"){
            $values[] = array("href" => fill_template($fields[$j]["url"], $feat->values, $layerFields),
            		           "text" => escape_quote($dataOutEncode($feat->values[$dataInEncode($fields[$j]["name"])])));
          }
          else if ($fields[$j]["type"]=="IMG" || $fields[$j]["type"]=="Image"){
						$values[] = fill_template($fields[$j]["url"], $feat->values, $layerFields);
          }
		  		else{
            $values[] = escape_quote($dataOutEncode($feat->values[$dataInEncode($fields[$j]["name"])])) ;
		  		}
        }
          
        $selExtentLayer = mergeRectObj($selExtentLayer, $feat->bounds);

        $values[] = $dataOutEncode(rectObjToStr($feat->bounds));
        $values[] = $dataOutEncode($resObj->shapeindex);
        
        $feat->free();
        
        if ( !array_key_exists($dataKey, $strObjJson["data"]) )
        	$count++;
        $strObjJson["data"][$dataKey] = $values;
        if ( $count>$valueLineLimit ) break;

      }
      $debugString = 'modePostGIS';

      $oMap->saveQuery($queryfileFullPath);
      break;
  }

  $layer->close();
  if ( $layer!==$layerOgr ) $layerOgr->close();

  $totalCount = $totalCount + $count;

  $strObjJson["extent"] = $dataOutEncode(rectObjToStr($selExtentLayer));
  ksort($strObjJson["data"]);
  $strObjJson["data"] = array_values($strObjJson["data"]);
  $strObjJson = array($dataOutEncode($layer->name) => $strObjJson);

  $selExtent = $selExtentLayer;
}
$response =  array(
		  "totalCount" => $totalCount
		, "extent"=>rectObjToStr($selExtent)
		, "results" => $strObjJson
		, "queryfile" => $queryfileFullPath
		, "mapfileOgr" => ($queryMode == QM_OGR_FILTER ? $queryfileOgrFullPath : "")
);
// write json result
return new Response(json_encode($response));
//. ', "debugString" : "' . $debugQstring . '"

     }
    
    /**
     * CONFIRM convertToSymfony2
     * TODO WEBSERVICE:ADMIN_CARTO
     *
     * @Route("/getbyaddress", name="frontcarto_query_getbyaddress", options={"expose"=true})
     * @Method({"GET", "POST"})
     * 
     * @param Request $request  The request object.
     * @param string $service
     * @param string $callback
     * 
     * @return JsonResponse Json object.
     *
     * @see /home/devperso/bfontaine/prodige3.4/prodigefrontcarto/services/GetByAddress/index.php
     *
     * #@used IHM/JavaScript/CarmenJS/Carmen/Application.js={807}
     * #@used IHM/JavaScript/CarmenJS/Carmen/Control/LocateByAddress.js={154}
     * #@test "https://prodige40:15003/app_dev.php/frontcarto/query/getbyaddress";
     */
    public function queryByAddress(Request $request/*, $service, $callback*/)
    {

/*TODO convertToSymfony2
require_once("./../../lib/lib_session.php");

require_once("./../../app_conf.php");
require_once("./../../lib/lib_request.php");
require_once("./../../lib/lib_service.php");
require_once("./../util.php");
*/
        $config_reader = $this->getConfigReader();
        $service  = $request->get("service", "");
        $callback = $request->get("callback", "");
        
        if($service == "getconfig"){
        	$data = array();
        	$dao = $this->getDAO(self::CONNECTION_PRODIGE);
				  $query = "SELECT * from parametrage.prodige_settings where prodige_settings_constant = 'PRO_REQUETEUR_PARADRESSE' limit 1";
				  $rs = $dao->buildResultSet($query);
				  $rs->First();
				  $rs = $rs->GetCurrentRow();
				  foreach($rs as $key => $value){
				  		if(!is_numeric($key)){
				  			$data[$key] = urlencode($value);
				  		}
				  }
          return new Response($callback."(".json_encode($data).")");
        }
        if($service == "locate"){
            $tabResultat      = array();
            $nominatim_url    = $request->get("serviceurl", "");
            $nominatim_limit  = 5;
            $nominatim_format = "json";
            $adresse          = $request->get("adresse", "");
            $adresse          = utf8_encode(str_replace(" ", "+", $adresse));
            if($nominatim_url != "")
              $tabResultat      = file_get_contents($nominatim_url."?limit=".$nominatim_limit."&format=".$nominatim_format."&q=".$adresse."&addressdetails=0&polygon=0");
            header("HTTP/1.0 200 OK");  // useful for some browsers which seem to misinterpret header (error 500 found)
            return new Response($callback."(" . $tabResultat . ")");
        }

    }
     
    /**
     * PARTIAL convertToSymfony2 
     *
     * @Route("/getareas", name="frontcarto_query_getareas", options={"expose"=true})
     * /{mapfile}/{layerName}/{fieldName}/{fieldId}
     * @Method({"GET"})
     * 
     * @param Request $request  The request object.
     * @param string $callback
     * @param string $mapfile
     * @param string $layerName
     * @param string $fieldName
     * @param string $fieldId
     * @return JsonResponse Json object.
     * 
     * @see /home/devperso/bfontaine/prodige3.4/prodigefrontcarto/services/GetAreas/index.php
     *
     * #@used IHM/JavaScript/CarmenJS/Carmen/Application.js={552}
     */
    public function queryByAreas(Request $request/*, $mapfile, $layerName, $fieldName, $fielId*/)
    {
        $mapfile      = $request->get("map", "");
        //obedel removed utf8_decode, since Mapfiles are in utf8...
        $layerName    = mapfileEncode($request->get("layer", ""));
        $fieldName    = mapfileEncode($request->get("fieldName", ""));
        $fieldId      = mapfileEncode($request->get("fieldId", ""));
        $filter_item  = mapfileEncode($request->get("filter_item", ""));
        $filter_id    = mapfileEncode($request->get("filter_id", ""));
        $callback     = mapfileEncode($request->get("callback", ""));
        
        $count = 0;
        $jsonData = array();
        
        $oMap = ms_newMapObj($mapfile);
        
        $layer = @$oMap->getLayerByName($layerName);
        if ($layer){
          if ($filter_id != "") {
//                 switch($layer->connectiontype) {
//                     case MS_POSTGIS :
//                         $filter_id = '"' . $filter_item . '"::text = \'' . $filter_id . '\'::text';
//                     break;
//                     default:
//                         // need more test cos mapserver expressions are driver dependant
//                         $filter_id = $filter_id;
//                     break;
//                 }
                $layer->set("filteritem", $filter_item);
                $layer->setFilter($filter_id);
          }
          $layer->open();
          $status = $layer->whichShapes($oMap->extent);
          while ($shape = $layer->nextShape())
          {
              $count++;
              
              $extent = implode(", ", array(
                      OWSEncode($shape->bounds->minx), 
                      OWSEncode($shape->bounds->miny), 
                      OWSEncode($shape->bounds->maxx), 
                      OWSEncode($shape->bounds->maxy)
              ));
              $jsonData[$shape->values[$fieldName]] = array(
                "name"   => escape_quote(OWSEncode($shape->values[$fieldName])),
                "id"     => escape_quote(OWSEncode($shape->values[$fieldId])),
                "extent" => $extent
              );
        
          }
          $layer->close();
          ksort($jsonData);
          $jsonData = array_values($jsonData);
        }
        else {
          $count = 0;
          $jsonData[] = array("name"=>"Erreur de paramétrage ", "extent"=>"-1"); 
        }
        
        //write json result
        $jsonData = array("totalCount"=>$count, "Names"=>$jsonData);
        return ($callback ? new Response($callback."(" . json_encode($jsonData) . ")") : new JsonResponse($jsonData));

    }
        
    /**
     * TODO convertToSymfony2
     * TODO= Transmission des paramètres (tableName, queryMode, ...) 
     * TODO= Initialisation de $_SESSION["service"]['dbInfo']
     * 
     * @TODO duplicate code, see Prodige\ProdigeBundle\Controller\GetAreasFromDBController
     *
     * @Route("/getareasfromdb", name="frontcarto_query_getareasfromdb", options={"expose"=true})
     * @Method({"GET", "POST"})
     * 
     * @param Request $request  The request object.
     * 
     * @return JsonResponse Json object.
     *
     * @see /home/devperso/bfontaine/prodige3.4/prodigefrontcarto/services/GetAreasFromDB/index.php
     *
     * #@used IHM/JavaScript/CarmenJS/Carmen/Application.js={589}
     */
    public function queryByAreasFromDB(Request $request)
    {
        try {
            defined("CARMEN_QM_GETFIELDS") || define("CARMEN_QM_GETFIELDS", 0);
            defined("CARMEN_QM_GETDATA") || define("CARMEN_QM_GETDATA", 1);
            
            $tablename      = $request->get("tablename", "");
            $queryParams_pk = $request->get("queryParams_pk", "");
            $queryMode      = $request->get("queryMode", "");
            $queryParams_filterValue = $request->get("queryParams_filterValue", "");
            $callback       = $request->get("callback", "");
            if( $tablename == "" || $queryMode == "" ) {
                throw new \Exception("Missing or wrong parameters in getAreasFromDB call. {tablename=".($tablename==""?"KO":"OK").", queryMode=".($queryMode==""?"KO":"OK")."}");
            } else {
                $queryMode = strcmp($queryMode, "getFields")== 0 ? CARMEN_QM_GETFIELDS : CARMEN_QM_GETDATA;
                if( $queryMode == CARMEN_QM_GETDATA && $queryParams_pk == "" ) {
                    throw new \Exception("Missing or wrong parameters in getAreasFromDB call. {queryParams_pk=".($queryParams_pk==""?"KO":"OK").", queryMode=".($queryMode==""?"KO":"OK")."}");
                }
            }
            
            $conn = $this->getConnection("prodige");
            $queryParam1 = $conn->createQueryBuilder();
            $queryParam2 = $conn->createQueryBuilder();
            $querySelect = $conn->createQueryBuilder();
            
            $querySelect->select(
                "st_xmax(box2d(the_geom)) AS xmax",
                "st_xmin(box2d(the_geom)) AS xmin",
                "st_ymax(box2d(the_geom)) AS ymax",
                "st_ymin(box2d(the_geom)) AS ymin"
                );
            
            
            /*
             * set_error_handler("handle_error", E_ALL | E_STRICT);
             * if(!loadService())
             * trigger_error(CARMEN_ERROR_SESSION_EXPIRED . "|" . CARMEN_MSG_SESSION_EXPIRED . "|CARMEN_ERROR", E_USER_ERROR);
             */
            
            $queryParam1->select("pk_critere_moteur", "critere_moteur_nom", "critere_moteur_order", "critere_moteur_id_join")->from(" parametrage." . $tablename)->orderBy("critere_moteur_order");
            $queryParam2->select("pk_critere_moteur", "critere_moteur_nom", "critere_moteur_order", "critere_moteur_table", "critere_moteur_champ_id", "critere_moteur_champ_nom", "critere_moteur_id_join", "critere_moteur_champ_join")->from(" parametrage." . $tablename)->where("pk_critere_moteur=" . $queryParams_pk);
            if( $tablename == "prodige_download_param" ) {
                $queryParam1->andWhere("b_flux_atom=0");
                $queryParam2->andWhere("b_flux_atom=0");
            }
            
            switch($queryMode) {
                case CARMEN_QM_GETFIELDS :
                    try {
                        $result = $queryParam1->execute()->fetchAll();
                    } catch( \Exception $ex ){
                        throw new \Exception("Error while accessing param table.");
                    }
                    $res = array();
                    foreach($result as $row){
                        $field = array();
                        $field['field_pk'] = $row["pk_critere_moteur"];
                        $field['field_name'] = OWSEncode($row["critere_moteur_nom"]);
                        $field['field_order'] = $row["critere_moteur_order"];
                        $field['field_join'] = $row["critere_moteur_id_join"];
                        // only search criteria with field_name <>'' are valid
                        if(strlen($field['field_name'])> 0)
                            array_push($res, $field);
                    }
                    $json_res = json_encode($res);
                    
                    break;
                case CARMEN_QM_GETDATA :
                default :
                    try {
                        $result = $queryParam2->execute()->fetchAll();
                        $result = $result[0];
                    } catch( \Exception $ex ){
                        throw new \Exception("Error while accessing param table.");
                    }
                    
                    $area_tablename = $result["critere_moteur_table"];
                    $area_fieldname_id = $result["critere_moteur_champ_id"];
                    $area_fieldname_name = $result["critere_moteur_champ_nom"];
                    
                    $querySelect->addSelect($area_fieldname_id, $area_fieldname_name)->from("public." . $area_tablename)->orderBy($area_fieldname_name);
                    
                    if( $queryParams_filterValue != "" ) {
                        $area_fieldname_join = $result["critere_moteur_champ_join"];
                        $querySelect->where($area_fieldname_join . "::text=(:filter_value)::text")->setParameter("filter_value", $queryParams_filterValue);
                    }
                    
                    try {
                        $result = $querySelect->execute()->fetchAll();
                    } catch( \Exception $ex ){
                        throw new \Exception("Bad parametrage");
                    }
                    $res = array();
                    foreach($result as $row){
                        $area = array();
                        $area['id'] = $row[$area_fieldname_id];
                        $area['name'] = OWSEncode($row[$area_fieldname_name]);
                        $area['extent'] = $row['xmin'] . "," . $row['ymin'] . "," . $row['xmax'] . "," . $row['ymax'];
                        array_push($res, $area);
                    }
                    $json_res = json_encode(array(
                        "totalCount" => count($res),
                        "Names" => $res 
                    ));
                    break;
            }
            
            return new Response($callback . "(" . $json_res . ")");
        } catch( \Exception $exception ){
            return new Response($callback . "(" . json_encode(array("erreur" => $exception->getMessage())). ")", Response::HTTP_INTERNAL_SERVER_ERROR );
        }
    }
    
    /**
     * CONFIRM convertToSymfony2
     * TODO= Initialisation de $_SESSION["service"]['idx']
     * 
     *
     * @Route("/getinformation/{editableLayers}", name="frontcarto_query_getinformation", options={"expose"=true}, requirements={"editableLayers"="^.*$"}, defaults={"editableLayers"=null})
     * @Method({"GET", "POST"})
     * 
     * @param Request $request  The request object.
     * 
     * @return JsonResponse Json object.
     *
     * @see /home/devperso/bfontaine/prodige3.4/prodigefrontcarto/services/GetInformation/index.php
     *
     * #@used IHM/JavaScript/CarmenJS/Carmen/Ext/ResultPanel.js={216}
     * #@used IHM/JavaScript/CarmenJS/Carmen/Application.js={323; 330; 333; 336; 340; 343; 437; 758}
     * #@used IHM/JavaScript/CarmenJS/Carmen/Control/ToolTip.js={387}
     * #@used IHM/JavaScript/CarmenJS/Carmen/Control/InfoURL.js={92}
     * #@used_done IHM/JavaScript/CarmenJS/Carmen/Control/InfoPolyline.js={433}
     * #@used_done IHM/JavaScript/CarmenJS/Carmen/Control/Info.js={353}
     * #@used_done IHM/JavaScript/CarmenJS/Carmen/Control/InfoCircle.js={110}
     * 
     * #@test "https://prodige40:15003/app_dev.php/frontcarto/query/getinformation";
     */
    public function queryByShape(Request $request, $editableLayers=null)
    {
        if ( $editableLayers ) $editableLayers = explode("/", $editableLayers);
        else $editableLayers = array();
        // Retrieving parameters
        $mapfile                = $request->get("map",                    "");
        $layersJSON             = $request->get("layers",                 '[]');
        $shapeStr               = $request->get("shape",                  "");
        $fieldsJSON             = $request->get("fields",                 '[]');
        $briefFieldsJSON        = $request->get("briefFields",            '[]');
        $showGraphicalSelection = $request->get("showGraphicalSelection", false);
        $shapeType              = $request->get("shapeType",              "box");
        
        // arguments concerning spatial query mode
        $queryType              = $request->get("queryType",              QueryByShape::CARMEN_QUERY_INFORMATION);
        $radius                 = $request->get("radius",                 0);
        $fidListStr             = $request->get("fidList",                "") ;
        $layerRef               = $request->get("layerRef",               "") ;
    /*
        $editor = new EditionManagerController();
        $editor->setContainer($this->container);
        $editors = array();
        foreach($editableLayers as $layerName){
            $editor->_initialize($request, $mapfile, $layerName);
           
            $WKTs = $editor->getFiltersWKTGeom();
            if ( !empty($WKTs) ){
                foreach ($WKTs as $territoire=>$WKT){
                    $WKT = $WKT[0]["the_geom"];
                    $WKT = explode(";", $WKT);
                    $WKT = end($WKT);
                    $editors[$layerName][$territoire] = \ms_shapeObjFromWkt($WKT);
                }
            }
        }*/
    
        $editors = array();
        foreach($editableLayers as $layerName){
            $editor = new EditionManagerController();
            $editor->setContainer($this->container);
            $editor->_initialize($request, $mapfile, $layerName);
            $editors[$layerName] = $editor;
        }
        
        
        try {
            $strJSON = QueryByShape::execute(
                $mapfile                ,
                $layersJSON             ,
                $shapeStr               ,
                $fieldsJSON             ,
                $briefFieldsJSON        ,
                $showGraphicalSelection ,
                $shapeType              ,
                $queryType              ,
                $radius                 ,
                $fidListStr             ,
                $layerRef               ,
                $editors
            );
        } catch (\Exception $exception){
            return new JsonResponse(array("totalCount"=>0, "success"=>false, "msg"=>$exception->getMessage()));
        }
        return new Response($strJSON, 200, array("Content-Type"=>"text/json"));
    }
    
    /**
     * TODO convertToSymfony2
     *
     * @Route("/locatebyaddress/{proxy_url}", name="frontcarto_query_locatebyaddress_proxyhost", options={"expose"=true})
     * 
     * @param Request $request  The request object.
     * 
     * @return JsonResponse Json object.
     *
     * @see /home/devperso/bfontaine/prodige3.4/prodigefrontcarto/services/GetInformation/proxy.php
     *
     * #@used_done IHM/JavaScript/CarmenJS/Carmen/Ext/ResultPanel.js={377}
     * #@used_done IHM/JavaScript/CarmenJS/Carmen/Control/LocateByAddress.js={161}
     * #@used IHM/JavaScript/CarmenJS/Carmen/Control/AddOGC.js={919; 1201; 1358; 1400}
     * #@used IHM/JavaScript/CarmenJS/Carmen/Control/InfoPolyline.js={568}
     * #@used IHM/JavaScript/CarmenJS/Carmen/Control/Info.js={520}
     * #@used IHM/JavaScript/CarmenJS/Carmen/Control/InfoCircle.js={245}
     * #@used services/AddWMTS/addWMTS.js={252}
     * #@used services/AddWMS/addWMS.js={329}
     * #@used services/AddWMSC/addWMSC.js={332}
     */
    public function locateByAddressAction(Request $request, $proxy_url)
    {
        $proxy_url = str_replace("?", "", urldecode($proxy_url));
        $PAYLOAD = urldecode(file_get_contents('php://input'));
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
          CURLOPT_URL => $proxy_url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => $PAYLOAD,
          CURLOPT_HTTPHEADER => ($headers = array(
            "Cache-Control: no-cache",
            "Content-Type: application/xml",
            "referer: ".$this->getParameter("PRODIGE_URL_FRONTCARTO")
          )),
        ));
        
        $splitHeaders = array();
        foreach($headers as $header){
            $subheaders = explode(";", $header);
            foreach($subheaders as $subheader){
                $split = explode(':', $subheader, 2);
                $split = array_map("trim", $split);
                $splitHeaders[$split[0]] = $split[1];
            }
        }
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);
        
        if ($err) {
          return new Response("Unavailable to access service", Response::HTTP_SERVICE_UNAVAILABLE, $splitHeaders);
        } else {
          return new Response($response, Response::HTTP_OK, $splitHeaders);
        }
        
        return new Response("Unavailable to access service", Response::HTTP_SERVICE_UNAVAILABLE, $headers);
    }
        
    /**
     * TODO convertToSymfony2
     *
     * @Route("/getinformation_proxy/{proxy_url}", name="frontcarto_query_getinformation_proxyhost", options={"expose"=true})
     * 
     * @param Request $request  The request object.
     * 
     * @return JsonResponse Json object.
     *
     * @see /home/devperso/bfontaine/prodige3.4/prodigefrontcarto/services/GetInformation/proxy.php
     *
     * #@used_done IHM/JavaScript/CarmenJS/Carmen/Ext/ResultPanel.js={377}
     * #@used_done IHM/JavaScript/CarmenJS/Carmen/Control/LocateByAddress.js={161}
     * #@used IHM/JavaScript/CarmenJS/Carmen/Control/AddOGC.js={919; 1201; 1358; 1400}
     * #@used IHM/JavaScript/CarmenJS/Carmen/Control/InfoPolyline.js={568}
     * #@used IHM/JavaScript/CarmenJS/Carmen/Control/Info.js={520}
     * #@used IHM/JavaScript/CarmenJS/Carmen/Control/InfoCircle.js={245}
     * #@used services/AddWMTS/addWMTS.js={252}
     * #@used services/AddWMS/addWMS.js={329}
     * #@used services/AddWMSC/addWMSC.js={332}
     */
    public function redirectAsProxyHost(Request $request, $proxy_url)
    {
      if ( $request->getMethod()=="GET" ){
      	$request->query->set("proxy_url", $proxy_url);
      }
      else {
      	$request->request->set("proxy_url", $proxy_url);
      }
      return $this->executeProxyRequest($request);
    }
    
    /**
     * TODO convertToSymfony2
     *
     * @Route("/getinformation_proxy", name="frontcarto_query_getinformation_proxy", options={"expose"=true})
     * @Method({"GET", "POST"})
     * 
     * @param Request $request  The request object.
     * 
     * @return JsonResponse Json object.
     *
     * @see /home/devperso/bfontaine/prodige3.4/prodigefrontcarto/services/GetInformation/proxy.php
     *
     * #@used_done IHM/JavaScript/CarmenJS/Carmen/Ext/ResultPanel.js={377}
     * #@used_done IHM/JavaScript/CarmenJS/Carmen/Control/LocateByAddress.js={161}
     * #@used IHM/JavaScript/CarmenJS/Carmen/Control/AddOGC.js={919; 1201; 1358; 1400}
     * #@used IHM/JavaScript/CarmenJS/Carmen/Control/InfoPolyline.js={568}
     * #@used IHM/JavaScript/CarmenJS/Carmen/Control/Info.js={520}
     * #@used IHM/JavaScript/CarmenJS/Carmen/Control/InfoCircle.js={245}
     * #@used services/AddWMTS/addWMTS.js={252}
     * #@used services/AddWMS/addWMS.js={329}
     * #@used services/AddWMSC/addWMSC.js={332}
     */
    public function executeProxyRequest(Request $request)
    {
        $proxy_url = urldecode($request->get("proxy_url", false));
        $force_utf8_output = $request->get("force_utf8_output", false);
        $request->query->remove("proxy_url");
        $request->query->remove("force_utf8_output");
        if ( $proxy_url ){
            $query_string = parse_url($proxy_url, PHP_URL_QUERY);
            $proxy_url = str_replace("?".$query_string, "", $proxy_url);
            $query_string_a = array();
            parse_str($query_string, $query_string_a);
       	    $test_only = array(
                CURLOPT_NOBODY         => false, 
                CURLOPT_RETURNTRANSFER => true, 
                CURLOPT_CONNECTTIMEOUT => 5
            );
       	    $headers = array();
       	    $GET = ($request->getMethod()=="GET" ? array_merge($query_string_a, $_GET) : array());
       	    $POST = ($request->getMethod()!="GET" ? array_merge($query_string_a, $_POST) : array());
       	    
       	    $response = $this->curl($proxy_url, $request->getMethod(), $GET, $POST, $test_only, $headers);
            
       	    if ( is_array($response) && isset($response["success"]) && $response["success"]===false){
                 $response = "Unavailable to access service";
                 $headers["http_code"] = $headers["http_code"] ?: Response::HTTP_SERVICE_UNAVAILABLE;
            }
            $this->getLogger()->info(__METHOD__, array("response"=>$response, "headers"=>$headers));
            if ( $force_utf8_output && stripos($headers['content_type'],"UTF-8")===false){
                $response = utf8_encode($response);
            }
            return new Response($response, $headers["http_code"], $headers);
        }
        return new Response("No proxy_url", Response::HTTP_BAD_REQUEST);
    }
    /**
     * TODO convertToSymfony2
     *
     * @Route("/getstats", name="frontcarto_query_getstats", options={"expose"=true})
     * @Method({"GET"})
     * 
     * @param Request $request  The request object.
     * 
     * @return JsonResponse Json object.
     *
     * @see /home/devperso/bfontaine/prodige3.4/prodigefrontcarto/services/GetStats/index.php
     *
     * #@used_done IHM/JavaScript/CarmenJS/Carmen/Ext/ResultPanel.js={660}
     */
    public function getFieldStats(Request $request)
    {
        $mapfile = $request->get("map", "");
        $layerName =  mapfileEncode($request->get("layer", ""));
        $fieldName =  mapfileEncode($request->get("fieldName", ""));
        
        $callback = $request->get("callback", "");
        $tabResult = array();
        $count = 0;
        
        $oMap = ms_newMapObj($mapfile);
        
        $tabValues = array();
        $layer = @$oMap->getLayerByName($layerName);
        if ($layer){
          $layer->open();
          $status = $layer->whichShapes($oMap->extent);
          while ($shape = $layer->nextShape())
          {
            $count++;
            $tabValues[]=$shape->values[$fieldName];
          }
          //$count=count($tabResult);
          $layer->close();
          $tabStat = array(
            "count" => $count,
            "min" => min($tabValues),
            "max" => max($tabValues),
            "sum" => array_sum($tabValues),
            "variance" => variance_sample($tabValues),
            "average" => array_sum($tabValues)/$count,
            "stddeviation" =>standard_deviation_population($tabValues)
          );
        }
        else {
          $count = 0;
          $strObjJson.="{\"error\":\"". "Impossible de récupérer les statistiques de cette couche\"}"; 
          //TODO treat error
        }
        
        //write json result
        return new Response($callback."({\"tabStat\":".json_encode($tabStat)."})");

    }
}
