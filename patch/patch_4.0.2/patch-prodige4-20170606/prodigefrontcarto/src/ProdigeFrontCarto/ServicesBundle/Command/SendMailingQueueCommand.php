<?php

namespace ProdigeFrontCarto\ServicesBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Prodige\ProdigeBundle\Controller\BaseController;
use Prodige\ProdigeBundle\Services\LdapUtils;
use Prodige\ProdigeBundle\Common\AlkRequest;
use Doctrine\DBAL\Types\Type;
use ProdigeFrontCarto\ServicesBundle\Controller\EditionManagerController;
use Symfony\Component\Process\Process;
use Symfony\Component\Console\Input\InputArgument;

/**
 * Cette commande s'occupe de scanner les comptes utilisateur pour désactiver ceux dont le mot de passe a expiré
 */
class SendMailingQueueCommand extends ContainerAwareCommand {
    
    /**
     * Configures the command definition
     */
    protected function configure() {
        $this->setName ( 'prodigefrontcarto:sendmailingqueue' )
             ->addArgument("mailingQueue", InputArgument::OPTIONAL, "Fichier de mailing spécifique", null)
             ->setDescription ( "Cette commande s'occupe de traiter les files d'attente d'envois de mails" )
        ;
    }
    
    /**
     * Executes the command
     */
    protected function execute(InputInterface $input, OutputInterface $output) 
    {
        !defined("ALK_PATH_MAILING_QUEUE") && define("ALK_PATH_MAILING_QUEUE", $this->getContainer()->getParameter("PRODIGE_PATH_DATA")."/MAILQUEUE/");
        if ( !defined("ALK_PATH_MAILING_QUEUE") || !is_dir(ALK_PATH_MAILING_QUEUE) ) return;
        $ALK_PATH_MAILING_QUEUE = ALK_PATH_MAILING_QUEUE;
        
        $mailingQueue = $input->getArgument("mailingQueue");
        $pattern = ($mailingQueue ? $mailingQueue : "*.queue");
        $CMD = 
<<<CMD
#!/bin/bash
QUEUE_DIR="{$ALK_PATH_MAILING_QUEUE}"

if [ ! -d "\$QUEUE_DIR" ]; then
  exit 1;
fi

LOGFILE="\$QUEUE_DIR/queue.log"
echo `date` > "\$LOGFILE"
chmod 660 "\$LOGFILE"

cd "\$QUEUE_DIR"

QUEUE_LIST=`ls {$pattern}`
for QUEUE_FILE in \$QUEUE_LIST; do
  if [ ! -f \${QUEUE_FILE}.lock ]; then
    echo "Exécution de \${QUEUE_FILE}" >> "\$LOGFILE"
    /bin/sh \${QUEUE_FILE}
    rm "\${QUEUE_FILE}*"
  fi
done
CMD
        ;
        $process = new Process($CMD);
        $process->run(function ($type, $buffer) use($output){
            if (Process::ERR === $type) {
                $output->writeln('ERRORS : ');
                $output->writeln($buffer);
            } else {
                $output->writeln('SUCCESS : ');
                $output->writeln($buffer);
            }
        });
        
    }
}