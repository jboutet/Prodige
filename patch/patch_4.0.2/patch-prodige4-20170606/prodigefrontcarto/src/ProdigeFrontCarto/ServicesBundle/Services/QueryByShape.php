<?php
namespace ProdigeFrontCarto\ServicesBundle\Services;

use ProdigeFrontCarto\ServicesBundle\Controller\EditionManagerController;
class QueryByShape 
{

    // shape type used in information querying
    const CARMEN_SHAPE_QUERY_BOX      = 0;
    const CARMEN_SHAPE_QUERY_CIRCLE   = 1;
    const CARMEN_SHAPE_QUERY_POLYGON  = 2;
    const CARMEN_SHAPE_QUERY_POLYLINE = 3;
    const CARMEN_SHAPE_QUERY_POINT    = 4;
    
    // querying type : information/spatial query
    const CARMEN_QUERY_INFORMATION         = -1;
    const CARMEN_QUERY_ADVANCED_INTERSECTS = 0;
    const CARMEN_QUERY_ADVANCED_IN         = 1;
    const CARMEN_QUERY_ADVANCED_CONTAINS   = 2;
    const CARMEN_QUERY_ADVANCED_DWITHIN    = 3;
    
    public static function execute(
        $mapfile                ,
        $layersJSON             ,
        $shapeStr               ,
        $fieldsJSON             ,
        $briefFieldsJSON        ,
        $showGraphicalSelection ,
        $shapeType              ,
        $queryType              = self::CARMEN_QUERY_INFORMATION,
        $radius                 = 0,
        $fidListStr             = "",
        $layerRef               = "",
        $editableLayers         = array()
    ){

        $layerNames = json_decode ( $layersJSON, true );
        $fieldsList = json_decode ( $fieldsJSON, true );
        $briefFieldsList = json_decode ( $briefFieldsJSON, true );

        // TODO enhance parameters checking to include all cases
        
        if ($mapfile == "" || $layerNames == false || $shapeStr == "" || $fieldsList == false ||  $showGraphicalSelection == "") {
            throw new \Exception("Missing or wrong parameters in getInformation call." );
        }
        
        $shapeQueryType = self::CARMEN_SHAPE_QUERY_CIRCLE;
        if (strcmp ( $shapeType, "box" ) == 0)
            $shapeQueryType = self::CARMEN_SHAPE_QUERY_BOX;
        elseif (strcmp ( $shapeType, "circle" ) == 0)
            $shapeQueryType = self::CARMEN_SHAPE_QUERY_CIRCLE;
        elseif (strcmp ( $shapeType, "polygon" ) == 0)
            $shapeQueryType = self::CARMEN_SHAPE_QUERY_POLYGON;
        elseif (strcmp ( $shapeType, "polyline" ) == 0)
            $shapeQueryType = self::CARMEN_SHAPE_QUERY_POLYLINE;
        
        $showGraphicalSelection = ($showGraphicalSelection == "true");
        $fidList = explode ( ",", $fidListStr );
        // building queryfilename
        $queryfileName = datasetNameFromFilename ( $mapfile ) . '_' . $_SESSION ["service"] ['idx'] . '_' . session_id () . '_' . $queryType . '.qy';
        $queryfileFullPath = CARMEN_DATA_PATH_CACHE . $queryfileName;
        $selectedShapes = array ();
        
        $totalCount = 0;
        $selExtent = ms_newRectObj ();
        $selExtent->setExtent ( MAX_DOUBLE, MAX_DOUBLE, MIN_DOUBLE, MIN_DOUBLE );
        $params = array ();
        
        $dataInEncode = 'MapfileEncode';
        $dataOutEncode = 'OWSEncode';
        
        $oMap = ms_newMapObj ( $mapfile );
        // map extent
        $qRectQueryMapExtent = ms_newRectObj ();
        $qRectQueryMapExtent->setextent ( $oMap->extent->minx, $oMap->extent->miny, $oMap->extent->maxx, $oMap->extent->maxy );
        // map projection
        $projection = $oMap->getProjection ();
        $projection = ($projection === MS_FALSE) ? "2154" : $projection;
        // parsing projection string to retrieve epsg id
        if (! ($projection === MS_FALSE)) {
            $matched = array ();
            $found = preg_match ( "/^.*(epsg|EPSG):(\d+)$/", $projection, $matched );
            if ($found > 0) {
                $projection = $matched [2];
            }
        }
        // die($projection);
        switch ($queryType) {
            case self::CARMEN_QUERY_INFORMATION :
                switch ($shapeQueryType) {
                    case self::CARMEN_SHAPE_QUERY_BOX :
                        $coords = explode ( ',', MapfileEncode ( substr ( $shapeStr, 1, strlen ( $shapeStr ) - 2 ) ) );
                        if (count ( $coords ) == 4) {
                            $shapeWKT = "POLYGON ((" . $coords [0] . " " . $coords [1] . "," . $coords [2] . " " . $coords [1] . "," . $coords [2] . " " . $coords [3] . "," . $coords [0] . " " . $coords [3] . "," . $coords [0] . " " . $coords [1] . "))";
                            $params = array (
                                    "shapeWKT" => $shapeWKT 
                            );
                        } else if (count ( $coords ) == 2) {
                            $shapeWKT = "POINT (" . $coords [0] . " " . $coords [1] . ")";
                            $params = array (
                                    "shapeWKT" => $shapeWKT,
                                    "radius" => 1 
                            );
                            $shapeQueryType = self::CARMEN_SHAPE_QUERY_POINT;
                        }
                        break;
                    case self::CARMEN_SHAPE_QUERY_CIRCLE :
                        $coords = explode ( ',', MapfileEncode ( substr ( $shapeStr, 1, strlen ( $shapeStr ) - 2 ) ) );
                        $radius = array_pop ( $coords );
                        $params = array (
                                "shapeWKT" => "POINT (" . $coords [0] . " " . $coords [1] . ")",
                                "radius" => $radius 
                        );
                        break;
                    case self::CARMEN_SHAPE_QUERY_POLYGON :
                    case self::CARMEN_SHAPE_QUERY_POLYLINE :
                        $params = array (
                                "shapeWKT" => $shapeStr 
                        );
                        break;
                }
                break;
            case self::CARMEN_QUERY_ADVANCED_INTERSECTS :
            case self::CARMEN_QUERY_ADVANCED_CONTAINS :
            case self::CARMEN_QUERY_ADVANCED_IN :
            case self::CARMEN_QUERY_ADVANCED_DWITHIN :
                // die("ici " . -1);
                $oLayerRef = $oMap->getLayerByName ( $layerRef );
                if ($oLayerRef == FALSE)
                    throw new \Exception ( "Unable to access layer " . $layerRef );
                $params = array (
                        "fidList" => $fidList,
                        "layerRef" => $oLayerRef,
                        "radius" => $radius,
                        "map" => $oMap 
                );
                break;
        }
        
        $strObjJson = "{";
        $comment = "";
        for($l = 0; $l < count ( $layerNames ); $l ++) {
        
            $layerName = MapfileEncode ( $layerNames [$l] );
            $layer = @$oMap->getLayerByName ( $layerName );
            $res = false;
            if ($layer) {
                // if postgis layer, changing postgis data connection
                // in order to use postgis to retrieve data, instead of mapserver
                if ($layer->connectiontype == MS_POSTGIS && self::queryPostGISLayer ( $layer, $params, $queryType, $shapeQueryType, $projection )) {
                    $qRectQueryMapExtentObj = ms_shapeObjFromWkt ( $params ["shapeWKT"] );
                    $res = (@$layer->queryByShape ( $qRectQueryMapExtentObj ) == MS_SUCCESS);
                    //$res = (@$layer->queryByRect ( $qRectQueryMapExtent ) == MS_SUCCESS);
                } else {
                    if ($queryType != self::CARMEN_QUERY_INFORMATION) {
                        throw new \Exception ( "spatial query on non postgis layers not implemented yet" );
                    }
                    // not a postgis layer, using mapserver engine to compute request
                    switch ($shapeQueryType) {
                        case self::CARMEN_SHAPE_QUERY_BOX :
                            if (count ( $coords ) >= 2) {
                                if (count ( $coords ) == 4) {
                                    // querying by a rectangle
                                    $qRectQuery = ms_newRectObj ();
                                    $qRectQuery->setextent ( $coords [0], $coords [1], $coords [2], $coords [3] );
                                    $res = (@$layer->queryByRect ( $qRectQuery ) == MS_SUCCESS);
                                }
                            }
                            break;
                        case self::CARMEN_SHAPE_QUERY_POINT :
                            // querying by a point
                            $qPtQuery = ms_newPointObj ();
                            $qPtQuery->setXY ( $coords [0], $coords [1] );
                            // @todo See how to set the tolerance
                            $res = (@$layer->queryByPoint ( $qPtQuery, MS_MULTIPLE, 1 ) == MS_SUCCESS);
                            break;
                        case self::CARMEN_SHAPE_QUERY_CIRCLE :
                            $qPtQuery = ms_newPointObj ();
                            $qPtQuery->setXY ( $coords [0], $coords [1] );
                            $res = (@$layer->queryByPoint ( $qPtQuery, MS_MULTIPLE, $radius ) == MS_SUCCESS);
                            break;
                        case self::CARMEN_SHAPE_QUERY_POLYGON :
                        case self::CARMEN_SHAPE_QUERY_POLYLINE :
                            $shpObj = ms_shapeObjFromWkt ( $shapeStr );
                            if ($shpObj) {
                                $shpObj = self::CARMEN_SHAPE_QUERY_POLYLINE ? $shpObj->buffer ( 1 ) : $shpObj;
                                $res = (@$layer->queryByShape ( $shpObj ) == MS_SUCCESS);
                            } else
                                throw new \Exception ( "Problem with geometry " . $shapeStr );
                            break;
                    }
                }
            }
            
            if ($res) {
                $queryfileNameLayer = datasetNameFromFilename ( $mapfile ) . '_' . $l . '_' . $_SESSION ["service"] ['idx'] . '_' . session_id () . '_' . $queryType . '.qy';
                $queryfileFullPathLayer = CARMEN_DATA_PATH_CACHE . $queryfileNameLayer;
                if ($showGraphicalSelection) {
                    $oMap->saveQuery ( $queryfileFullPathLayer);
                }
                $selExtentLayer = ms_newRectObj ();
                $selExtentLayer->setExtent ( MAX_DOUBLE, MAX_DOUBLE, MIN_DOUBLE, MIN_DOUBLE );
                $count = $layer->getNumResults ();
                $totalCount = $totalCount + $count;
                
                // with mapserver client wfs layers, mapserv seems to automatically
                // encode data from iso to utf8 in memory... so we we must not do an
                // extra encoding utf8->iso transformation
                if ($layer->connectiontype == MS_WFS || $layer->getMetaData ( "LAYER_ENCODING" ) == "UTF-8") {
                    $dataInEncode = 'NoEncode';
                    $dataOutEncode = 'NoEncode';
                }
                
                @$layer->open();
                
                $fields = $fieldsList [$l];
                $briefFields = $briefFieldsList [$l];
                $layerFields = $layer->getItems ();
                
                // writing json step by step...
                $strObjJson .= "\"" . $layerName . "\"" . ': ';
                $escapeFields = array_map ( "escape_quote", $fields );
                $quoteFields = array_map ( "getName", $escapeFields );
                $escapeBriefFields = array_map ( "escape_quote", $briefFields );
                $quoteBriefFields = array_map ( "getName", $escapeBriefFields );
                $strObjJson .= '{ fields : [' . implode ( ",", $quoteFields ) . '] ,'; // , "extent", "fid"] ,';
                $strObjJson .= ' briefFields : [' . implode ( ",", $quoteBriefFields ) . '] ,'; // , "extent", "fid"] ,';
                $strObjJson .= ' data : [';
                $strBriefData = ' briefData : [';
                
                $nbEditable = 0;
                for($i = 0; $i < $count; $i ++) {
                    $resObj = $layer->getResult ( $i );            
                    $feat = $layer->getShape ( $resObj );
                    
                    if ($feat != "") {
                        $editable = false;
                        $matches = array();
                        if ( array_key_exists($layerName, $editableLayers) ){
                            $editor = $editableLayers[$layerName];
                            $editor instanceof EditionManagerController;
                            $editable = $editor->isShapeIntersectsFilters(preg_replace("!\D!", "", $layer->getProjection()), $feat->toWKT());
                        }
                        if ( $editable ) $nbEditable++;
                        $strObjJson .= '[';
                        for($j = 0; $j < count ( $fields ); $j ++) {
                            if ($fields [$j] ["type"] == "URL")
                                $value = '{ "href" : "' . fill_template ( $fields [$j] ["url"], $feat->values, $layerFields ) . '", "text" : "' . escape_quote ( $dataOutEncode ( $feat->values [$dataInEncode ( $fields [$j] ["name"] )] ) ) . '"},';
                            else if ($fields [$j] ["type"] == "IMG" || $fields[$j]["type"]=="Image") {
                                $value = '"' . fill_template ( $fields [$j] ["url"], $feat->values, $layerFields ) . '",';
                            } else
                                $value = '"' . escape_quote ( $dataOutEncode ( $feat->values [$dataInEncode ( $fields [$j] ["name"] )] ) ) . '",';
                            $strObjJson .= $value;
                        }
                        
                        $strBriefData .= '[';
                        for($j = 0; $j < count ( $briefFields ); $j ++) {
                            
                            if ($briefFields [$j] ["type"] == "URL")
                                $value = '{ "href" : "' . fill_template ( $briefFields [$j] ["url"], $feat->values, $layerFields ) . '", "text" : "' . escape_quote ( $dataOutEncode ( $feat->values [$dataInEncode ( $briefFields [$j] ["name"] )] ) ) . '"},';
                            else if ($briefFields [$j] ["type"] == "IMG" || $briefFields[$j]["type"]=="Image") {
                                $value = '"' . fill_template ( $briefFields [$j] ["url"], $feat->values, $layerFields ) . '",';
                            } else
                                $value = '"' . escape_quote ( $dataOutEncode ( $feat->values [$dataInEncode ( $briefFields [$j] ["name"] )] ) ) . '",';
                            $strBriefData .= $value;
                        }
                        $bds = $feat->bounds;
                        $selExtentLayer = mergeRectObj ( $selExtentLayer, $bds );
                        $bds = $feat->bounds;
                        $strObjJson .= '"' . $dataOutEncode ( rectObjToStr ( $bds ) ) . '"';
                        $strObjJson .= ', "' . $dataOutEncode ( $resObj->shapeindex ) . '"';
                        $strObjJson .= ", ".($editable ? "true" : "false");
                        $strObjJson .= '],';
                        
                        
                        $strBriefData .= '"' . $dataOutEncode ( rectObjToStr ( $bds ) ) . '"';
                        $strBriefData .= ', "' . $dataOutEncode ( $resObj->shapeindex ) . '"';
                        $strBriefData .= ", ".($editable ? "true" : "false");
                        $strBriefData .= '],';
                        
                        $selectedShapes [] = array (
                                $layer->index,
                                $resObj->tileindex,
                                $resObj->shapeindex 
                        );
                        
                        $feat->free ();
                    }
                }
                
                $selExtent = mergeRectObj ( $selExtent, $selExtentLayer );
                $strBriefData = ($count > 0) ? substr ( $strBriefData, 0, strlen ( $strBriefData ) - 1 ) : $strBriefData;
                $strBriefData .= '],';
                $strObjJson = ($count > 0) ? substr ( $strObjJson, 0, strlen ( $strObjJson ) - 1 ) : $strObjJson;
                $strObjJson .= '],';
                $strObjJson .= $strBriefData;
                $strObjJson .= '"extent" : "' . $dataOutEncode ( rectObjToStr ( $selExtentLayer ) ) . '",';
                //$strObjJson .= '"filters" : ' .( array_key_exists($layerName, $editableLayers) ? json_encode(array_map(function($shape){return rectObjToStr($shape->bounds);}, $editableLayers[$layerName])) : "null" ) . ',';
                $strObjJson .= '"nbEditable" : ' .$nbEditable . ',';
                $dataOutEncode ( rectObjToStr ( $selExtentLayer ) ) . '",';

                $strObjJson .= '"queryfile" : "' . (file_exists($queryfileFullPathLayer) && $layer->connectiontype != MS_WFS  ? $queryfileFullPathLayer : "") . '"';
                if ($layer->connectiontype == MS_POSTGIS) {
                  $strObjJson .= ',"layerTable" : "' . self::getTableNameFromDataStr( $layer->data ) . '"';
                }
                $strObjJson .= '},';
                
                if ($showGraphicalSelection) {
                    $oMap->saveQuery ( $queryfileFullPathLayer );
                }
                $layer->close ();
            }
           
        }
        $strObjJson = lastChar ( $strObjJson ) == ',' ? substr ( $strObjJson, 0, strlen ( $strObjJson ) - 1 ) : $strObjJson;
        
        $strObjJson .= '}';
        return '({"totalCount":"' . $totalCount . '","extent":"' . $dataOutEncode ( rectObjToStr ( $selExtent ) ) . '", "results":' . $strObjJson . ', "queryfile":"' . "" . '", "comment": "' . $comment . '"})';
    }
        


    /**
     * @param \layerObj $msLayer
     * @param array     $params
     * @param int       $queryType
     * @param int       $shapeQueryType
     * @param string    $projection
     * @return boolean
     */
    protected static function queryPostGISLayer(\layerObj $msLayer, $params, $queryType, $shapeQueryType, $projection) {
        // die("ici " . 1);
        $strSQL = self::sqlSrcFromPostGISLayerConnection ( $msLayer->data );
        // die("ici " . $strSQL . " " . $queryType);
        if ($strSQL != null) {
            switch ($queryType) {
                case self::CARMEN_QUERY_INFORMATION :
                    switch ($shapeQueryType) {
                        case self::CARMEN_SHAPE_QUERY_BOX :
                        case self::CARMEN_SHAPE_QUERY_POLYGON :
                        case self::CARMEN_SHAPE_QUERY_POLYLINE :
                            $strSQL = "the_geom FROM ( select * from ( " . $strSQL . ") sreq WHERE st_intersects(the_geom, st_geometryFromText('" . $params ["shapeWKT"] . "',$projection))) as foo using unique gid using SRID=$projection";
                            $msLayer->set ( "data", $strSQL );
                            break;
                        case self::CARMEN_SHAPE_QUERY_POINT :
                        case self::CARMEN_SHAPE_QUERY_CIRCLE :
                            $strSQL = "the_geom FROM ( select * from (" . $strSQL . ") sreq WHERE st_dwithin(the_geom, st_geometryFromText('" . $params ["shapeWKT"] . "',$projection)," . $params ["radius"] . ")) as foo using unique gid using SRID=$projection";
                            $msLayer->set ( "data", $strSQL );
                            break;
                    }
                    break;
                default :
                    $fidList = implode ( ",", $params ["fidList"] );
                    $oLayerRef = $params ["layerRef"];
                    $radius = $params ["radius"];
                    if ($oLayerRef->connectiontype == MS_POSTGIS) {
                        $layerRefSrc = self::sqlSrcFromPostGISLayerConnection ( $oLayerRef->data );
                        $strSQL_t2 = "SELECT st_union(the_geom) as the_geom_ref FROM (" . $layerRefSrc . ") as t WHERE gid IN (" . $fidList . ")";
                        switch ($queryType) {
                            case self::CARMEN_QUERY_ADVANCED_INTERSECTS :
                                $strSpatialPredicate = "st_intersects(t1.the_geom, t2.the_geom_ref)";
                                break;
                            case self::CARMEN_QUERY_ADVANCED_CONTAINS : // A contains B
                                $strSQL_t2 = "SELECT st_union(the_geom) as the_geom_ref FROM (" . $layerRefSrc . ") as t WHERE gid IN (" . $fidList . ")";
                                $strSpatialPredicate = "st_contains(t1.the_geom, t2.the_geom_ref)";
                                break;
                            case self::CARMEN_QUERY_ADVANCED_IN : // B contains A
                                $strSQL_t2 = "SELECT st_union(the_geom) as the_geom_ref FROM (" . $layerRefSrc . ") as t WHERE gid IN (" . $fidList . ")";
                                $strSpatialPredicate = "st_contains(t2.the_geom_ref, t1.the_geom)";
                                break;
                            case self::CARMEN_QUERY_ADVANCED_DWITHIN :
                                $strSpatialPredicate = "st_dwithin(t1.the_geom, t2.the_geom_ref, " . $radius . ")";
                                break;
                        }
                        
                        $strSQL_t1 = $strSQL;
                        $strSQL = "SELECT * FROM (" . $strSQL_t1 . ") as t1, (" . $strSQL_t2 . ") as t2 WHERE " . $strSpatialPredicate;
                        // die($strSQL);
                        $strSQL = "the_geom FROM (" . $strSQL . ") as foo using unique gid using SRID=$projection";
                    } else {
                        // todo make multi geom form fid -> shape desc
                        throw new \Exception ( "spatial query on non postgis layers not implemented yet" );
                        $shapesWKT = array ();
                        @$oLayerRef->open ();
                        $layerProj = getEPSGProjectionFromMsString ( $oLayerRef->getProjection () );
                        $mapProj = getEPSGProjectionFromMsString ( $params ["map"]->getProjection () );
                        // die("Projection " . getEPSGProjectionFromMsString($layerProj));
                        for($i = 0; $i < count ( $fidList ); $i ++) {
                            $rsltObj = new \resultObj(intval ( $fidList [$i] ) );
                            $shpObj = $oLayerRef->getShape ($rsltObj);
                            array_push ( $shapesWKT, $shpObj->toWkt () );
                        }
                        $strSpatialPredicate = "";
                        switch ($queryType) {
                            case self::CARMEN_QUERY_ADVANCED_INTERSECTS :
                                $strSpatialPredicate = "st_intersects(the_geom, %GEOM2%)";
                                break;
                            case self::CARMEN_QUERY_ADVANCED_CONTAINS : // A contains B
                                $strSpatialPredicate = "st_contains(the_geom, %GEOM2%)";
                                break;
                            case self::CARMEN_QUERY_ADVANCED_IN : // B contains A
                                $strSpatialPredicate = "st_contains(%GEOM2%,the_geom)";
                                break;
                            case self::CARMEN_QUERY_ADVANCED_DWITHIN :
                                $strSpatialPredicate = "st_dwithin(the_geom, %GEOM2%, " . $radius . ")";
                                break;
                        }
                        
                        $predicate = "";
                        for($i = 0; $i < count ( $shapesWKT ); $i ++) {
                            $strGeom = "st_geometryFromText('" . $shapesWKT [$i] . "'," . $layerProj . ")";
                            if ($mapProj != $layerProj)
                                $strGeom = "st_transform(" . $strGeom . ", " . $mapProj . ")";
                            $predicate .= str_replace ( "%GEOM2%", $strGeom, $strSpatialPredicate );
                            $predicate .= $i < (count ( $shapesWKT ) - 1) ? " OR " : "";
                        }
                        $strSQL = "the_geom FROM (" . $strSQL . " WHERE " . $predicate . " ) as foo using unique gid using SRID=$projection";
                        // die($strSQL);
                    }
                    
                    // die($strSQL);
                    $msLayer->set ( "data", $strSQL );
                    break;
            }
        }
        // die("ici " . $strSQL . " " . $queryType);
        // has datasource been changed
        return $strSQL != NULL;
    }
    /**
     * @param string $strLayerConnection
     * @return string|NULL
     * throws \Exception
     */
    protected static function sqlSrcFromPostGISLayerConnection($strLayerConnection) {
        // die("ici test regexp");
        $src = NULL;
        $matched = array ();
        $found = preg_match ( "!\s*\((.*)\)\s*!", $strLayerConnection, $matched );
        if ($found)
            $src = $matched [1];
        else
            throw new \Exception("Unable to redefine PostGIS datasource in the querying process." );
        return $src;
    }

    /**
     * 
     * @param string $dataStr
     * @return string
     */
    protected static function getTableNameFromDataStr($dataStr) {
        //global $dbconn;
        $tablename="";
        //"the_geom from (select * from tce_marais_bretons) as foo using unique gid using srid=2154"
        $matched =  array();
        // before modification: /^.*select\s+\*\s+from\s+([^\s]+)\).*$/
        $found = preg_match("/^.*select\s+.*\s+from\s+([^\s\(\)]+).*$/",$dataStr, $matched);
        if ($found>0) { 
            $tablename=$matched[1];
        }
        return $tablename;
    }
}
