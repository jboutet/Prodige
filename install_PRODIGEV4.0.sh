#!/bin/bash
#Installation complète Prodige 4.0
#28/04/2017
#http://adullact.net/projects/prodige/

#Definition du repertoire d'install source
SCRIPTDIR=`dirname $(readlink -f $0)`
DATEINST=`date '+%y%m%d-%H%M'`
Netapes="10"
#adresse du depot debian jessie
SOURCES_LIST1="deb http://ftp.fr.debian.org/debian/ jessie main contrib non-free"
SOURCES_LIST2="deb http://security.debian.org/ jessie/updates  main contrib non-free"
SOURCES_LIST3="deb http://ftp.fr.debian.org/debian/ jessie-updates main"
SOURCES_LIST4="deb http://ftp.fr.debian.org/debian jessie-backports main"

YEAR=`date '+%Y'`
DATE=`date '+%y%m%d'`
NUM="4.0.1"
VERSIONTXT="/home/sites/prodigecatalogue/web/version.txt"
PATCH_NAME="update_prodige${NUM}_26042017.tar.gz"
PATCH_TAG="prodige_${NUM}"
PATCH_DESC="Patch correctif prodige_${NUM}
- Installation de Prodige 4.
"
LDAP_DOMAIN="alkante.domprodige"
LDAP_ORGA="Prodige4"
OLDDNS_SUFFIX="-p4.alkante.com"
DB="CATALOGUE"


################################################
#options parsing
################################################

function usage(){
printf "Utilisation du script avec des options :\n"
printf "\t-n user_bdd\t : L'utilisateur de connexion postgres qui sera créé pour l'application, par exemple user_prodige\n" 
printf "\t-t THESAURUS_NAME\t : Le nom du thésaurus\n" 
printf "\t-c SSLDIR\t\t : Le répertoire des certificats SSL\n"
printf "\t-h\t\t\t : Affiche ce message.\n"
printf "Exemple : ./${SCRIPTNAME} -u www.georegion.fr -c /home/ssl\n"
printf "./${SCRIPTNAME} doit être lancé avec aucune option OU avec les 4 options\n"
}
error_exit()
{
echo "Annulation du script : $1" && exit 1
}

case $# in
    0) MODE=interactive;;
    8) MODE=noninteractive;;
    *) usage && exit 1;;
esac

OPTS=$( getopt -q -o n:,t:,u:,c: -- "$@" )
[ $? != 0 ] && usage && exit 1
eval set -- "$OPTS"

while true ; do
	case "$1" in
		-t) user_bdd="$2"
			shift 2;;
		-t) THESAURUS_NAME="$2"
			shift 2;;
		-u) WWWURL="$2"
			shift 2;;
		-c) SSLDIR="$2"
			shift 2;;
		--) shift; break;;
	esac
done

if [ $MODE == "noninteractive" ]; then
    for param in user_bdd THESAURUS_NAME WWWURL SSLDIR; do
        check "${!param}" "var" "Variable $param vide"
    done
fi



################################################
#Fonctions
################################################

ctl_saisie()
{
echo $1 | grep -Eq "$2" ; [ $? -eq 0 ] || error_exit "Format saisi \"$1\" : incorrect"
}

remove_trailing()
{
echo "$1"|sed "s/\(^.*\)\/$/\1/"
}

red='\033[31m';green='\033[32m';yellow='\033[33m';blue='\033[34m'
magenta='\033[35m';cyan='\033[36m';white='\033[37m'

color_echo()
{
message="$1";color="$2"
echo -en "$color";echo -e "$message"
tput sgr0
return
}  

echo_titre()
{
color_echo "\n\t${1}/$Netapes ${2}\n" "$cyan"
}

wait_exec()
{
rep="0"
[ "$rep" == "0" ]
while [ "$?" != "1" ] ; do
   echo -ne ". "
   sleep "$2"
   pgrep -f "$1" > /dev/null
done
echo
}

get_dom()
{
# WWWURL= www-domain.tld ou www.domain.tld ou www-xyz.domain.tld ou www.xyz.domain.tld
ctl_saisie "$WWWURL" "^www[\.-].*\..*$"
# DNS_SUFFIX= -domain.tld ou .domain.tld ou -xyz.domain.tld ou .xyz.domain.tld
DNS_SUFFIX=`echo "$WWWURL" | sed 's/^www//'`
# DOMAIN= domain.tld ou xyz.domain.tld
DOMAIN=`echo $DNS_SUFFIX | sed 's/^-\?[a-z0-9]*\.//'`
# DNS_PREFIX_SEP = . ou -
DNS_PREFIX_SEP=`echo $WWWURL | sed 's/\(www\)\([\.-]\).*$/\2/'`
}

check()
{
case "$2" in
    var) [ -z $1 ] && error_exit "$3";;
    file) [ -f $1 ] || error_exit "Le fichier $1 n'existe pas";;
    dir) [ -d $1 ] || error_exit "Le répertoire $1 n'existe pas";;
esac
}

change_mirror()
{
#Configurer les depôts Debian jessie
cp /etc/apt/sources.list /etc/apt/sources.list.bak.$DATEINST
echo "${SOURCES_LIST1}" > /etc/apt/sources.list
echo "${SOURCES_LIST2}" >> /etc/apt/sources.list
echo "${SOURCES_LIST3}" >> /etc/apt/sources.list
}

replace_postgres_conn()
{
echo -e "\tParamétrage de Postgres dans ${3}"
sed -i "
s|${1}|${user_bdd}|g
s|${2}|${pass_bdd}|g
" "${3}"
}

restart()
{
/etc/init.d/postgresql restart
/etc/init.d/apache2 restart
/etc/init.d/tomcat7 restart
/etc/init.d/jetty8 restart
}


################################################
#Verifications
################################################
verif()
{
echo
check "${SCRIPTDIR}" "dir"

#Verification version Debian
echo -ne "Version Debian Jessie "
if [ "`egrep "^8\." /etc/debian_version`" ] 
  then color_echo "OK" $green
  else color_echo "NOK, annulation de l'installation" $red;exit
fi

echo -ne "Architecture amd64 "
if [ "`dpkg --print-architecture |grep amd64`" ] 
  then color_echo "OK" $green
  else color_echo "NOK i386 détectée, annulation de l'installation" $red;exit
fi
echo

#Verification depot Debian
which wget > /dev/null 2>&1 || { color_echo "wget n'est pas installé, annulation de l'installation" $red;exit; }
SOURCES_LIST1_URL=`echo "${SOURCES_LIST1}" |awk '{ print $2 "dists/" $3 }'`
change_mirror
grep -q "jessie-backports" /etc/apt/sources.list || echo "${SOURCES_LIST4}" >> /etc/apt/sources.list
echo -ne "Vérification du dépôt Debian "
wget -O /dev/null -q "${SOURCES_LIST1_URL}"
if [ $? -eq 0 ]
 then color_echo "OK" $green
else color_echo "NOK
Veuillez mettre à jour manuellement l'adresse du dépôt dans la variable SOURCES_LIST1 de ce script
Puis relancez l'installation" $red;exit
fi
apt-get -qq update
apt-get install -qqy openssl pwgen ca-certificates

LDAP_PWD=`pwgen -s 10 1`
check "$LDAP_PWD" "var" "Le mot de passe LDAP n'a pu être généré"
SSL_PWD="$LDAP_PWD"
PHPCLI_DEFAULT_PWD=`pwgen -s 10 1`
check "$PHPCLI_DEFAULT_PWD" "var" "Le mot de passe PHPCLI_DEFAULT_PWD n'a pu être généré"
pass_bdd=`pwgen -s 10 1`
check "$pass_bdd" "var" "Le mot de passe postgres pour $user_bdd n'a pu être généré"

for dir in \
${SCRIPTDIR}/prodigeadmincarto \
${SCRIPTDIR}/prodigeadminsite \
${SCRIPTDIR}/prodigecatalogue \
${SCRIPTDIR}/prodigefrontcarto \
${SCRIPTDIR}/prodigetelecarto \
${SCRIPTDIR}/prodigedatacarto \
${SCRIPTDIR}/cas \
${SCRIPTDIR}/vendor \
${SCRIPTDIR}/prodigegeosource \
${SCRIPTDIR}/tasks \
;do check "$dir" "dir";done
for file in \
${SCRIPTDIR}/editables_parameters.yml \
${SCRIPTDIR}/cas/etc/cas/cas.properties \
${SCRIPTDIR}/cas/WEB-INF/classes/services/prodige4-proxy.json \
${SCRIPTDIR}/cas/etc/cas/log4j2.xml \
${SCRIPTDIR}/prodigegeosource/WEB-INF/config-overrides-prod.xml \
${SCRIPTDIR}/prodigegeosource/WEB-INF/config-db/jdbc.properties \
${SCRIPTDIR}/prodigegeosource/WEB-INF/config-security/config-security.properties \
${SCRIPTDIR}/prodigeadmincarto/carmenwsback/app/config/parameters.yml \
${SCRIPTDIR}/prodigegeosource/WEB-INF/classes/log4j.xml \
${SCRIPTDIR}/conf/cas.xml \
${SCRIPTDIR}/conf/geonetwork.xml \
${SCRIPTDIR}/conf/geonetwork_log4j.xml \
${SCRIPTDIR}/conf/cas_log4j2.xml \
${SCRIPTDIR}/conf/server.xml \
${SCRIPTDIR}/conf/prodige4.0.conf \
${SCRIPTDIR}/conf/alkante.ldif \
${SCRIPTDIR}/conf/p4.ldif \
${SCRIPTDIR}/conf/0.conf \
${SCRIPTDIR}/bases/PRODIGE.sql \
${SCRIPTDIR}/bases/CATALOGUE.sql \
${SCRIPTDIR}/rights.sh \
${SCRIPTDIR}/uprod.sh \
;do check "$file" file ;done
}



################################################
#Variables utilisateur
################################################

start()
{
echo "Europe/Paris" > /etc/timezone
dpkg-reconfigure -f noninteractive tzdata

echo -ne "Locales systême "
if [ "`egrep "^fr_FR ISO-8859-1" /etc/locale.gen`" ] && [ "`egrep "^fr_FR.UTF-8 UTF-8" /etc/locale.gen`" ] && [ `locale charmap` == "UTF-8" ]
    then color_echo "OK" $green
    else
         echo "Reconfiguration des paramétrages régionaux actifs"
         echo "Activation de FR utf8 et de FR latin1, avec utf8 par défaut"
         cp /etc/locale.gen /etc/locale.gen.$DATEINST
         echo "LANG=fr_FR.UTF-8" > /etc/default/locale 
         echo "fr_FR ISO-8859-1" > /etc/locale.gen
         echo "fr_FR.UTF-8 UTF-8" >> /etc/locale.gen
         locale-gen
         echo "La machine doit redémarrer, veuillez relancer l'install APRES REBOOT..."
         read -p "Appuyer sur Entrée pour redémarrer ou contrôle-C pour l'en empêcher"
         reboot &
	 exit 0
fi
}

ask()
{
if [ $MODE == "interactive" ]; then
    echo "Indiquer l'utilisateur de connexion postgres qui sera créé pour l'application, par exemple user_prodige ?"
    read user_bdd
    echo
    echo "Indiquer le nom du thésaurus ?"
    read THESAURUS_NAME
    echo
    echo "Indiquer l'url qui sera utilisée pour accéder au site prodige ?
Elle peut être sous une et une seule des formes suivantes:
- www.domaine.fr      => vous devez fournir un certificat SSL wildcard pour      *.domaine.fr
- www-test.domaine.fr => vous devez fournir un certificat SSL wildcard pour      *.domaine.fr
- www.test.domaine.fr => vous devez fournir un certificat SSL wildcard pour *.test.domaine.fr
Merci de saisir l'url en commençant par www. ou par www-
"
    read WWWURL
    echo
    get_dom
    echo "L'accès au site sera https://$WWWURL
Pour cela vous devez fournir un certificat wildcard pour *.$DOMAIN"
    echo "Indiquer le répertoire où se trouvent les fichiers du certificat SSL (wildcard.prodige4.crt, wildcard.prodige4.key et CA_intermediate.pem) ?"
    read SSLDIR
    echo
fi

check "$SSLDIR" "dir"
for cert in wildcard.prodige4.crt wildcard.prodige4.key CA_intermediate.pem; do
   check "$SSLDIR/$cert" "file"
done
openssl verify -verbose -CAfile "$SSLDIR/CA_intermediate.pem"  "$SSLDIR/wildcard.prodige4.crt"
[ $? -eq 0 ] || error_exit "Erreur dans la vérification de la chaine des CA"
(openssl x509 -noout -modulus -in "$SSLDIR/wildcard.prodige4.crt"| openssl md5 ; openssl rsa -noout -modulus -in "$SSLDIR/wildcard.prodige4.key" | openssl md5) | uniq
[ $? -eq 0 ] || error_exit "Erreur $SSLDIR/wildcard.prodige4.crt ne correspond pas à $SSLDIR/wildcard.prodige4.key"
echo

ctl_saisie "$SERVICE_PRODUCTEUR" "^[0-9]*$"
get_dom

for param in user_bdd pass_bdd DB ; do
     check "${!param}" "var" "Variable $param vide"
done

echo "
Le script est prêt pour lancer l'installation avec les paramètres suivants:
- Le nom du thésaurus est : $THESAURUS_NAME
- L'accès au site sera https://$WWWURL avec un certificat wildcard pour *.$DOMAIN présent dans $SSLDIR
- Paramètres postgres: bases PRODIGE et $DB  / user=$user_bdd pass=$pass_bdd
- Mot de passe admin LDAP : $LDAP_PWD
- Mot de passe java SSL : $SSL_PWD
- Mot de passe cli admin : $PHPCLI_DEFAULT_PWD"
read -p "Pour démarrer l'installation, appuyez sur Entrée!"
echo
}



################################################
#Install paquets
################################################
install_utils()
{
apt-get -qqy remove mailx exim4*
apt-get -qqy install openssh-server ssh ftp unzip zip screen ntpdate time vim wget make autoconf libstdc++6 gcc cpp gnu-standards g++ sendmail sendmail-base sensible-mda sudo rsync bzip2 ed pwgen ca-certificates sharutils psmisc openssl
 
cd /usr/local/src
apt-get -qqy install fontconfig libfontconfig
wget -nv https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-2.1.1-linux-x86_64.tar.bz2
tar -xjf phantomjs-2.1.1-linux-x86_64.tar.bz2
cp phantomjs-2.1.1-linux-x86_64/bin/phantomjs /usr/local/bin/
cd $SCRIPTDIR/files/
tar -xjf wkhtmltopdf-0.11.0_rc1-static-amd64.tar.bz2
cp wkhtmltopdf-amd64 /usr/bin/wkhtmltopdf
ln -s /usr/bin/wkhtmltopdf /usr/local/bin/wkhtmltopdf
cd /usr/lib/x86_64-linux-gnu
ln -s libcrypto.so.1.0.0 libcrypto.so > /dev/null 2>&1
ln -s libssl.so.1.0.0 libssl.so > /dev/null 2>&1
}

install_apache()
{
sed -i "s/^127.0.0.1.*$/127.0.0.1 localhost www$DNS_SUFFIX catalogue$DNS_SUFFIX adminsite$DNS_SUFFIX telecarto$DNS_SUFFIX datacarto$DNS_SUFFIX admincarto$DNS_SUFFIX mapserv$DNS_SUFFIX carto$DNS_SUFFIX/" /etc/hosts

apt-get -qqy install php5 \
php5-mcrypt php5-gd  php5-common \
php5-cli libapache2-mod-php5 \
php5-imagick php5-json php5-xsl \
php5-memcache memcached apache2 apache2.2-common php5-pgsql \
php-pear php5-curl python-numpy
}

conf_apache()
{
#Configuration de  php et d'Apache
cp /etc/php5/apache2/php.ini /etc/php5/apache2/php.ini.dist
cp /usr/share/php5/php.ini-production /etc/php5/apache2/php.ini
#memory_limit = 80M 
sed -i "s/^memory_limit = /;memory_limit = /g" /etc/php5/apache2/php.ini
sed -i "/^;memory_limit = / i\memory_limit = 512M" /etc/php5/apache2/php.ini
#post_max_size = 12M
sed -i "s/^post_max_size = /;post_max_size = /g" /etc/php5/apache2/php.ini
sed -i "/^;post_max_size = / i\post_max_size = 500M" /etc/php5/apache2/php.ini
#upload_max_filesize = 10M
sed -i "s/^upload_max_filesize = /;upload_max_filesize = /g" /etc/php5/apache2/php.ini
sed -i "/^;upload_max_filesize = / i\upload_max_filesize = 500M" /etc/php5/apache2/php.ini
#Reglage des sessions
sed -i "s/session.gc_maxlifetime = 1440/session.gc_maxlifetime = 3600/" /etc/php5/apache2/php.ini
#maxclient 512
sed -i 's/MaxRequestWorkers[[:blank:]]*150/ServerLimit 512\n\tMaxRequestWorkers 512/' /etc/apache2/mods-available/mpm_prefork.conf

#secu
sed -i "s/^expose_php = On/;expose_php = On\nexpose_php = Off/" /etc/php5/apache2/php.ini
sed -i "s/^ServerSignature.*$/ServerSignature Off/
s/^ServerTokens OS.*$/ServerTokens Prod/" /etc/apache2/conf-available/security.conf
echo 'umask 007' >> /etc/apache2/envvars
sed -i 's/^\([[:blank:]]*\)SSLProtocol.*$/\1SSLProtocol all -SSLv3\n\tSSLHonorCipherOrder On\n\tSSLCompression off/
s/^\([[:blank:]]*\)SSLCipherSuite.*$/\1SSLCipherSuite ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA:ECDHE-RSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-RSA-AES256-SHA256:DHE-RSA-AES256-SHA:ECDHE-ECDSA-DES-CBC3-SHA:ECDHE-RSA-DES-CBC3-SHA:EDH-RSA-DES-CBC3-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:DES-CBC3-SHA:!DSS/' /etc/apache2/mods-available/ssl.conf


#activation des modules et des sites
a2enmod proxy_http
a2enmod proxy
a2enmod rewrite
a2enmod ssl
a2enmod headers
a2enmod cgi
a2dissite 000-default.conf
echo "ulimit -n 16384" >> /etc/default/apache2

apt-get -qqy install php5-pecl-http
echo "extension=raphf.so
extension=propro.so
extension=http.so
" > /etc/php5/mods-available/pecl-http.ini
php5enmod pecl-http

}

install_tomcat()
{
[ -f /etc/insserv/overrides/tomcat7 ] && rm /etc/insserv/overrides/tomcat7
[ -f /etc/logrotate.d/tomcat7 ] && rm /etc/logrotate.d/tomcat7
[ -f /etc/insserv/overrides/tomcat7 ] && rm /etc/insserv/overrides/tomcat7
apt-get  -qqy install -t jessie-backports openjdk-8-jdk
apt-get -qqy install tomcat7
/etc/init.d/tomcat7 stop

sed -i 's/TOMCAT7_GROUP=tomcat7/TOMCAT7_GROUP=www-data/
s/TOMCAT7_USER=tomcat7/TOMCAT7_USER=www-data/
s/^JAVA_OPTS.*$/JAVA_OPTS=\"-Xms1024m -Xmx2048m -Xss2M -Xmn128M -XX:MaxPermSize=256m -XX:PermSize=256m -server -Djava.awt.headless=true -XX:+UseParallelGC -XX:+AggressiveOpts\"/' /etc/default/tomcat7
find -L /var/cache/tomcat7/ -user tomcat7 -exec chown www-data {} \;
find -L /var/lib/tomcat7/ -user tomcat7 -exec chown www-data {} \;
find -L /var/cache/tomcat7/ -group tomcat7 -exec chgrp www-data {} \;
find -L /var/lib/tomcat7/ -group tomcat7 -exec chgrp www-data {} \;
mkdir -p /usr/share/tomcat7/shared/classes
mkdir -p /usr/share/tomcat7/server/classes
sed -i '99i ulimit -n 16384' /usr/share/tomcat7/bin/catalina.sh

#sed -i 's/\(^JAVA_OPTS=\)\(.*\)\"$/\1\2 -Dgeosource.resources.dir=\/home\/sites\/prodigegeosource\/WEB-INF\/data"/' /etc/default/tomcat7


echo "Modification de logrotate"
cp $SCRIPTDIR/conf/tomcat7.logrotate /etc/logrotate.d/tomcat7
chown root: /etc/logrotate.d/tomcat7
chmod 640 /etc/logrotate.d/tomcat7

sed -i "$ i\* soft nofile 16384\n* hard nofile 16384" /etc/security/limits.conf
[ `egrep -c "^fs.file-max" /etc/sysctl.conf` -eq 0 ] && echo "fs.file-max = 16384" >> /etc/sysctl.conf && sysctl -p /etc/sysctl.conf && echo "16384" > /proc/sys/fs/file-max	

apt-get -qqy install jetty8
cp /etc/default/jetty8 /etc/default/jetty8.bak
sed -i 's/NO_START=1/NO_START=0/
s/^.*LOGFILE_DAYS.$/LOGFILE_DAYS=7/
s/^.*JETTY_PORT.*$/JETTY_PORT=8380/
s/^.*JETTY_HOST.*$/JETTY_HOST=0.0.0.0/
s/^.*JETTY_USER.*$/JETTY_USER=www-data/' /etc/default/jetty8
cd /etc/jetty8
ln -s /etc/tomcat7/keystore/prodige.keystore
sed -i 's/^# etc\/jetty-ssl.xml/etc\/jetty-ssl.xml/' start.ini
cp $SCRIPTDIR/conf/jetty-ssl.xml /etc/jetty8/jetty-ssl.xml
/etc/init.d/jetty8 stop
chown www-data: /var/lib/jetty8
mv /var/lib/jetty8/webapps /var/lib/jetty8/webapps.dist
mkdir /var/lib/jetty8/webapps && cd /var/lib/jetty8/webapps
ln -s /home/sites/cas
}

install_carto()
{
#install postgres
apt-get -qqy install  postgresql-9.4 postgresql-contrib postgis  postgresql-9.4-postgis-scripts postgresql-9.4-postgis-2.1
cp $SCRIPTDIR/conf/tomcat7.insserv /etc/insserv/overrides/tomcat7
insserv > /dev/null 2>&1
#Install paquets compiles par Alkante
#dep gdal
apt-get -qqy install curl libcurl3 libcurl3-gnutls libgeos-c1 libgif4 libjpeg62-turbo libpng12-0 python libsqlite3-0 libxerces-c3.1 libpq5 libxml2 libjasper1 libkml0 libfreexl1 libexpat1
#dep mapserver
apt-get -qqy install libcairo2 libcurl3-gnutls libfcgi0ldbl libgd3 proj-bin libapache2-mod-fcgid
[ -f  /etc/apache2/mods-available/fcgid.conf ] && mv /etc/apache2/mods-available/fcgid.conf /etc/apache2/mods-available/fcgid.conf.bak
cp $SCRIPTDIR/conf/fcgid.conf /etc/apache2/mods-available/fcgid.conf
a2enmod fcgid
#bug debian
ln -s /usr/lib/libproj.so.0 /usr/lib/libproj.so
#dep mapcache
apt-get -qqy install curl libcurl3 libcurl3-gnutls libpng12-0 libjpeg62-turbo libtiff5 libpixman-1-0 libpcre3 libfcgi0ldbl libapache2-mod-fcgid

echo
cd "${SCRIPTDIR}/deb"
dpkg -i libecwj2_3.3-1jessie_amd64.deb
echo
dpkg -i gdal_2.0.2-3jessie_amd64.deb
echo
dpkg -i mapserv_7.0.0-1jessie_amd64.deb
mkdir /usr/lib/cgi-bin/ > /dev/null 2>&1
cp /usr/local/bin/mapserv /usr/lib/cgi-bin/mapserv
cp /usr/local/bin/mapserv /usr/lib/cgi-bin/mapservwfs
cp /usr/local/bin/mapserv /usr/lib/cgi-bin/mapserv.fcgi
chmod 755 /usr/lib/cgi-bin/mapserv*
echo "extension=php_mapscript.so" > /etc/php5/mods-available/mapscript.ini
php5enmod mapscript
#php-ogr
dpkg -i php-ogr_1.1.1-1jessie_amd64.deb
php5enmod ogr
echo
dpkg -i mapcache_1.4.0-1jessie_amd64.deb
ldconfig
echo 'LoadModule mapcache_module    /usr/lib/apache2/modules/mod_mapcache.so' > /etc/apache2/mods-available/mapcache.load
a2enmod mapcache

#Epsg
echo "Install epsg"
cp -f $SCRIPTDIR/files/epsg /usr/share/proj/
}

install_ldap()
{
export DEBIAN_FRONTEND=noninteractive
sudo debconf-set-selections <<EOF
slapd slapd/internal/generated_adminpw password $LDAP_PWD
slapd slapd/password2 password $LDAP_PWD
slapd slapd/internal/adminpw password $LDAP_PWD
slapd slapd/password1 password $LDAP_PWD
slapd slapd/domain string $LDAP_DOMAIN
slapd shared/organization string $LDAP_ORGA
EOF
apt-get install -qqy slapd ldap-utils
echo
echo -e "\tImport du schéma alkante.ldif"
ldapadd -Q -Y EXTERNAL -H ldapi:/// -f ${SCRIPTDIR}/conf/alkante.ldif
sed -i 's|^SLAPD_SERVICES=.*$|SLAPD_SERVICES="ldap://127.0.0.1:389/ ldapi://127.0.0.1:389/"|' /etc/default/slapd
/etc/init.d/slapd restart
echo
echo -e "\tImport de p4.ldif"
PHPCLI_DEFAULT_PWD_SSHA=`slappasswd -h {SSHA} -s "${PHPCLI_DEFAULT_PWD}" | sed 's|/|\\\/|g'`
sed -i "s/#PHPCLI_DEFAULT_PWD_SSHA#/${PHPCLI_DEFAULT_PWD_SSHA}/" ${SCRIPTDIR}/conf/p4.ldif
ldapadd -x -H ldap://localhost/ -D "cn=admin,dc=alkante,dc=domprodige" -w "$LDAP_PWD" -f ${SCRIPTDIR}/conf/p4.ldif 
echo
}

install_casldap()
{
apt-get -qqy install php5-ldap php-cas
}

copy_sources()
{
echo "Copie des données à partir de $SCRIPTDIR"
mkdir /home/sites
check "/home/sites" "dir"
cd $SCRIPTDIR
for dir in prodigeadmincarto prodigeadminsite prodigecatalogue prodigefrontcarto prodigetelecarto prodigedatacarto cas vendor prodigegeosource
  do
    echo -e "\tInstallation de /home/sites/$dir ..."
    rsync -a $dir /home/sites/
  done
chmod -R 770 /home/sites
chown -R www-data: /home/sites
echo

cd $SCRIPTDIR/
echo "Copie de $SCRIPTDIR/prodige vers /home/"
cp -a prodige /home/
cd /home/sites/prodigefrontcarto/web/IHM
[ -d IHM ] || mkdir IHM
cd IHM
ln -s /home/prodige/cartes/IHM cartes
cd /home/prodige/cartes/IHM
ln -s /home/prodige/cartes/Publication/reference
echo

find /home/prodige/cartes/ \( -name "*.map" -o -name "*.xml" \) -print0 |xargs -0 sed -i "s/${OLDDNS_SUFFIX}/${DNS_SUFFIX}/g"
find /home/prodige/cartes/ -type f -print0 |xargs -0 sed -i "s/pass_prodige/$pass_bdd/g"
find /home/prodige/cartes/ -type f -print0 |xargs -0 sed -i "s/user_prodige/$user_bdd/g"
find /home/prodige/owscontext -type f -name "*.ows" -print0 |xargs -0 -r sed -i "s|$OLDDNS_SUFFIX|$DNS_SUFFIX|g"
find /home/prodige/cartes/Publication/ -type f -name "*.json" -print0 |xargs -0 -r sed -i "s|$OLDDNS_SUFFIX|$DNS_SUFFIX|g"
sed -i "s|p4.alkante.com|$DOMAIN|" /home/prodige/SYSTEM/services.xml
find /home/prodige/cartes/ -name "*.map" -exec sed -i '/LOG \"log_itx.txt\"/d' {} \;
[ -f /home/prodige/cartes/Publication/ms_error.txt ] && rm /home/prodige/cartes/Publication/ms_error.txt

echo "Edition des fichiers de configuration des sites"
echo "########################################################"
cp ${SCRIPTDIR}/favicon.ico /home/sites/
cp ${SCRIPTDIR}/logo-client.png /home/sites/
cp ${SCRIPTDIR}/editables_parameters.yml /home/sites/
cd /home/sites
for conffile in \
editables_parameters.yml \
cas/etc/cas/cas.properties \
cas/WEB-INF/classes/services/prodige4-proxy.json \
prodigeadmincarto/carmenwsback/app/config/parameters.yml \
prodigegeosource/WEB-INF/config-overrides-prod.xml \
prodigegeosource/WEB-INF/config-db/jdbc.properties \
prodigegeosource/WEB-INF/config-security/config-security.properties \
;do 
echo -e "\t$conffile"
sed -i "s/#DOMAIN#/$DNS_SUFFIX/g
s/#DB_PRODIGE_NAME#/PRODIGE/g
s/#DB_PRODIGE_USER#/$user_bdd/g
s/#DB_PRODIGE_PWD#/$pass_bdd/g
s/#DB_CATALOGUE_NAME#/$DB/g
s/#DB_CATALOGUE_USER#/$user_bdd/g
s/#DB_CATALOGUE_PWD#/$pass_bdd/g
s/#PHPCLI_DEFAULT_LOGIN#/admincli/g
s/#PHPCLI_DEFAULT_PWD#/${PHPCLI_DEFAULT_PWD}/g
s/#LDAP_PWD#/$LDAP_PWD/g
s/#PATH_CACERT_PEM#/\/etc\/apache2\/ssl-cert\/CA_intermediate.pem/g" $conffile
done

echo -e "\tprodigeadmincarto/carmenwsback/app/config/parameters.yml"
sed -i "s/dns_url_prefix_sep.*$/dns_url_prefix_sep: \"$DNS_PREFIX_SEP\"/" /home/sites/prodigeadmincarto/carmenwsback/app/config/parameters.yml 
echo

echo "Création des lien symboliques"
echo "########################################################"
for dir in prodigeadminsite prodigecatalogue prodigefrontcarto prodigetelecarto prodigedatacarto
  do
    echo -e "\t$dir"
    cd /home/sites
    cd $dir/vendor
    [ -d alkante ] && rm -rf alkante
    [ -d prodige ] && rm -rf prodige
    ln -s ../../vendor/alkante
    ln -s ../../vendor/prodige
  done
for dir in prodigeadmincarto/carmenwsback prodigeadmincarto/carmenwsmapserv
  do
    echo -e "\t$dir"
    cd /home/sites
    cd $dir/vendor
    [ -d alkante ] && rm -rf alkante
    [ -d prodige ] && rm -rf prodige
    ln -s ../../../vendor/alkante
    ln -s ../../../vendor/prodige
  done

echo "Installation des favicon.ico"
echo "########################################################"
for ico in \
/home/sites/cas/favicon.ico \
/home/sites/prodigegeosource/images/logos/favicon.ico \
/home/sites/prodigeadmincarto/carmenwsback/web/favicon.ico \
/home/sites/prodigeadminsite/web/favicon.ico \
/home/sites/prodigecatalogue/web/favicon.ico \
/home/sites/prodigedatacarto/web/favicon.ico \
/home/sites/prodigefrontcarto/web/favicon.ico \
/home/sites/prodigetelecarto/web/favicon.ico \
;do
ln -sf /home/sites/favicon.ico "${ico}"
done
echo
echo "Installation des logos"
echo "########################################################"
ln -s /home/sites/logo-client.png /home/sites/cas/images/logo-client.png
ln -s /home/sites/logo-client.png /home/sites/vendor/prodige/prodige/Prodige/ProdigeBundle/Resources/public/images/logo-client.png
echo

echo "Suppression de fichiers geonetwork posant problème"
echo "########################################################"
rm -f /home/sites/prodigegeosource/WEB-INF/data/config/schema_plugins/iso19139/schematron/schematron-rules-inspire-strict.sch
rm -f /home/sites/prodigegeosource/WEB-INF/data/config/schema_plugins/iso19139/schematron/schematron-rules-inspire-strict.xsl
rm -f /home/sites/prodigegeosource/WEB-INF/data/config/schema_plugins/iso19139/schematron/schematron-rules-inspire-sds.sch
rm -f /home/sites/prodigegeosource/WEB-INF/data/config/schema_plugins/iso19139/schematron/schematron-rules-inspire-sds.xsl
rm -f /home/sites/prodigegeosource/WEB-INF/data/config/schema_plugins/iso19139/schematron/schematron-rules-inspire-strict.disabled.sch
rm -f /home/sites/prodigegeosource/WEB-INF/data/config/schema_plugins/iso19139/schematron/schematron-rules-inspire-strict.disabled.xsl
rm -f /home/sites/prodigegeosource/WEB-INF/data/config/schema_plugins/iso19139/schematron/schematron-rules-inspire-sds.disabled.sch
rm -f /home/sites/prodigegeosource/WEB-INF/data/config/schema_plugins/iso19139/schematron/schematron-rules-inspire-sds.disabled.xsl
rm -f /home/sites/prodigegeosource/WEB-INF/data/config/schema_plugins/iso19139/schematron/schematron-rules-inspire.disabled.sch
rm -f /home/sites/prodigegeosource/WEB-INF/data/config/schema_plugins/iso19139/schematron/schematron-rules-inspire.disabled.xsl
echo

echo "Edition des fichiers de configuration tomcat"
echo "########################################################"
cp ${SCRIPTDIR}/conf/geonetwork_log4j.xml /home/sites/prodigegeosource/WEB-INF/classes/log4j.xml
cp ${SCRIPTDIR}/conf/geonetwork.xml /etc/tomcat7/Catalina/localhost/
chmod +r /etc/tomcat7/Catalina/localhost/*.xml
cp ${SCRIPTDIR}/conf/server.xml /etc/tomcat7/
sed -i "s/#SSL_PWD#/$SSL_PWD/g" /etc/tomcat7/server.xml
cp ${SCRIPTDIR}/conf/jetty-ssl.xml /etc/jetty8/
sed -i "s/#SSL_PWD#/$SSL_PWD/g" /etc/jetty8/jetty-ssl.xml
cp ${SCRIPTDIR}/conf/cas_log4j2.xml /home/sites/cas/WEB-INF/classes/log4j2.xml
chmod 750 /var/log/jetty8
chown www-data.adm /var/log/jetty8
echo



echo "Configuration des virtualhosts Apache"
echo "########################################################"
cp  ${SCRIPTDIR}/conf/prodige4.0.conf /etc/apache2/sites-available/
sed -i "s/#DOMAIN#/$DNS_SUFFIX/g" /etc/apache2/sites-available/prodige4.0.conf
a2ensite prodige4.0.conf
cp  ${SCRIPTDIR}/conf/0.conf /etc/apache2/sites-available/
sed -i "s/#DOMAIN#/$DNS_SUFFIX/g" /etc/apache2/sites-available/0.conf
[ -d /home/sites/nothing ] || mkdir -p /home/sites/nothing
a2ensite 0.conf
echo


echo "Installation des certificats SSL"
echo "########################################################"
cd /etc/apache2
[ -d ssl-cert ] || mkdir ssl-cert
cd ssl-cert
[ -d ssl.crt ] || mkdir ssl.crt
[ -d ssl.key ] || mkdir ssl.key
cp "$SSLDIR/wildcard.prodige4.crt" ssl.crt/wildcard.prodige4.crt.$YEAR
cd ssl.crt && ln -s wildcard.prodige4.crt.$YEAR wildcard.prodige4.crt
cd ..
cp "$SSLDIR/wildcard.prodige4.key" ssl.key/wildcard.prodige4.key.$YEAR
cd ssl.key && ln -s wildcard.prodige4.key.$YEAR wildcard.prodige4.key
cd ..
cp "$SSLDIR/CA_intermediate.pem" CA_intermediate.pem.$YEAR
ln -s CA_intermediate.pem.$YEAR CA_intermediate.pem
chmod 644 CA_intermediate.pem.$YEAR

#conversion ssl pour keystore tomcat
mkdir /etc/tomcat7/keystore
openssl pkcs12 -export -name wildcard.prodige4 -in "$SSLDIR/wildcard.prodige4.crt" -inkey "$SSLDIR/wildcard.prodige4.key" -out /etc/tomcat7/keystore/wildcard.prodige4.p12.$YEAR -password pass:$SSL_PWD
keytool -importkeystore -destkeystore /etc/tomcat7/keystore/prodige.keystore -srckeystore /etc/tomcat7/keystore/wildcard.prodige4.p12.$YEAR -srcstoretype pkcs12 -srcalias wildcard.prodige4 -srcstorepass $SSL_PWD -deststorepass $SSL_PWD -noprompt
keytool -import -trustcacerts  -keystore /etc/ssl/certs/java/cacerts -storepass changeit -noprompt -file "$SSLDIR/CA_intermediate.pem" -alias wildcard.prodige4CA
echo

/etc/init.d/apache2 restart
}

import_base()
{
echo
SOURCE="$SCRIPTDIR/bases/${1}.sql"
echo  "Re-création de la structure" | tee  -a "${2}" 
sudo -i -u postgres createdb -O "$user_bdd" ${3} "${1}" >> "${2}" 2>&1
echo -ne "Import de la base "  | tee  -a "${2}"
psql -U "${user_bdd}" -h localhost -d "${1}" -f "${SOURCE}" >> "${2}" 2>&1 &
wait_exec "${SOURCE}" "2"
sudo -u postgres psql -d "${1}" -c "
grant all on geometry_columns to ${user_bdd};
grant all on spatial_ref_sys to ${user_bdd};
grant all on geography_columns to ${user_bdd};
" >> "${2}" 2>&1
}

install_base()
{
echo "localhost:5432:PRODIGE:$user_bdd:$pass_bdd
localhost:5432:$DB:$user_bdd:$pass_bdd
localhost:5432:postgres:$user_bdd:$pass_bdd" >> /root/.pgpass
chmod 600 /root/.pgpass

echo "Paramétrage des scripts sql Postgres"
for i in www catalogue adminsite telecarto datacarto admincarto carto; do
   sed -i "s/${i}${OLDDNS_SUFFIX}/${i}${DNS_SUFFIX}/g" ${SCRIPTDIR}/bases/*.sql
done
sed -i "s/user_prodige/$user_bdd/g
s/pass_prodige/$pass_bdd/g" ${SCRIPTDIR}/bases/*.sql

md5pass=`echo -n "${pass_bdd}${user_bdd}"|md5sum|awk '{ print $1 }'`
sudo -i -u postgres psql -c "CREATE ROLE ${user_bdd} ENCRYPTED PASSWORD 'md5${md5pass}' SUPERUSER CREATEDB CREATEROLE INHERIT LOGIN;"
sudo -i -u postgres psql -c "CREATE ROLE user_admin ENCRYPTED PASSWORD 'md57cff8cc9d16b03d9dfcc7954b4662992' SUPERUSER CREATEDB CREATEROLE INHERIT LOGIN;"
su - postgres -c "psql -c \"ALTER ROLE $user_bdd SUPERUSER;\""

#Reglage de postgres pour la phase import
sed -i 's/#checkpoint_segments = 3/checkpoint_segments = 100/' /etc/postgresql/9.4/main/postgresql.conf
/etc/init.d/postgresql restart

LOG_CATALOGUE="$SCRIPTDIR/import_$DB.log"
import_base "${DB}" "$LOG_CATALOGUE" "--encoding=UTF8"
LOG_PRODIGE="$SCRIPTDIR/import_PRODIGE.log"
import_base "PRODIGE" "$LOG_PRODIGE" "--encoding=UTF8"

dns_tmp=`echo $DNS_SUFFIX | sed 's/^[\.-]//'`
su - postgres -c "psql -d PRODIGE -c \"set search_path to carmen; update lex_dns set dns_url = '$dns_tmp';\""

su - postgres -c "psql -d CATALOGUE -c \"INSERT INTO settings VALUES ('system/metadatacreate/generateUuid', 'true', 2, 9100, 'n');\""

su - postgres -c "psql -c \"ALTER ROLE $user_bdd NOSUPERUSER NOCREATEDB NOCREATEROLE INHERIT LOGIN;\""
sed -i 's/checkpoint_segments = 100/#checkpoint_segments = 3/' /etc/postgresql/9.4/main/postgresql.conf
/etc/init.d/postgresql restart
rm /root/.pgpass
}

install_tasks()
{
mkdir -p /home/tasks/log
cp $SCRIPTDIR/tasks/* /home/tasks/
chown root.root /home/tasks/*
chmod 700 /home/tasks/*
#taches planifiees
echo "
#tasks prodige4.0
00 03           * * *   www-data    cd /home/sites/prodigeadminsite/ && /usr/bin/php app/console adminsite:generate_mapcache > /dev/null 2>&1
00 23           * * *   www-data    cd /home/sites/prodigeadmincarto/carmenwsback && /usr/bin/php app/console carmen:importdata > /dev/null 2>&1
00 00           * * *   www-data    cd /home/sites/prodigecatalogue/ && /usr/bin/php app/console prodige:password:lockexpired > /dev/null 2>&1
00,30 *         * * *   www-data    cd /home/sites/prodigeadminsite/ && /usr/bin/php app/console adminsite:populate_session_logs > /dev/null 2>&1
00 01         * * 6   www-data    cd /home/sites/prodigefrontcarto &&  /usr/bin/php app/console prodigefrontcarto:alertedition > /dev/null 2>&1
10 01         * * 6   www-data    cd /home/sites/prodigefrontcarto &&  /usr/bin/php app/console prodigefrontcarto:sendmailingqueue > /dev/null 2>&1
*/5 *           * * *   root        /home/tasks/queue_download.sh
30 00           * * *   root        /home/tasks/clean.sh
#end tasks prodige4.0
" >> /etc/crontab
echo
}


echo "
#################################"
echo -e "Démarrage installation de Prodige 4.0"
read -p "Appuyez sur Entrée pour continuer
#################################"
echo

verif
start
ask

echo_titre "1" "INSTALL DES PAQUETS DEBIAN"
install_utils
echo_titre "2" "INSTALL APACHE-PHP"
install_apache
echo_titre "3" "CONFIG APACHE-PHP"
conf_apache
echo_titre "4" "INSTALL TOMCAT / JETTY"
install_tomcat
echo_titre "5" "INSTALL PAQUETS carto"
install_carto
echo_titre "6" "INSTALL CAS/LDAP"
install_ldap
install_casldap
echo_titre "7" "Installation des sources"
copy_sources
echo_titre "8" "Redémarrage des services"
restart
echo_titre "9" "Installation des bases de données"
install_base
echo_titre "10" "Inscription des tâches planifiées"
install_tasks
echo

/etc/init.d/tomcat7 stop
rm -rf /home/sites/prodigegeosource/WEB-INF/data/index/*
/etc/init.d/tomcat7 start
cp ${SCRIPTDIR}/rights.sh /home/
cp ${SCRIPTDIR}/uprod.sh /home/sites/
cd /home/sites

echo "Génération du fichiers version.txt"
echo "########################################################"
echo "Génération du fichier version.txt"
echo -e "Patch ${PATCH_NAME} (${PATCH_TAG}) applied `date`\n${PATCH_DESC}\n-----------------------------------------------" > ./current_version.txt
[ -f "${VERSIONTXT}" ] || touch "${VERSIONTXT}"
ed -s  "${VERSIONTXT}" <<< $'0 r ./current_version.txt\n,w'
rm ./current_version.txt

bash ./uprod.sh
bash /home/rights.sh

color_echo "L'installation est terminée" $yellow
echo
exit 0

