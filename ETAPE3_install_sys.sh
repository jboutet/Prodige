#!/bin/bash
#Installation Composants système pour Prodige 4
#12/07/2016
#http://adullact.net/projects/prodige/

#Definition du repertoire d'install source
SCRIPTDIR=`dirname $(readlink -f $0)`
DATEINST=`date '+%y%m%d-%H%M'`
Netapes="7"
#adresse du depot debian jessie
SOURCES_LIST1="deb http://ftp.fr.debian.org/debian/ jessie main contrib non-free"
SOURCES_LIST2="deb http://security.debian.org/ jessie/updates  main contrib non-free"
SOURCES_LIST3="deb http://ftp.fr.debian.org/debian/ jessie-updates main"
SOURCES_LIST4="deb http://ftp.fr.debian.org/debian jessie-backports main"

change_mirror()
{
#Configurer les depôts Debian jessie
cp /etc/apt/sources.list /etc/apt/sources.list.bak.$DATEINST
echo "${SOURCES_LIST1}" > /etc/apt/sources.list
echo "${SOURCES_LIST2}" >> /etc/apt/sources.list
echo "${SOURCES_LIST3}" >> /etc/apt/sources.list
}

################################################
#Verifications
################################################
verif()
{
echo
#Verification version Debian
echo -ne "Version Debian Jessie "
if [ "`egrep "^8\." /etc/debian_version`" ] 
  then color_echo "OK" $green
  else color_echo "NOK, annulation de l'installation" $red;exit
fi

echo -ne "Architecture amd64 "
if [ "`dpkg --print-architecture |grep amd64`" ] 
  then color_echo "OK" $green
  else color_echo "NOK i386 détectée, annulation de l'installation" $red;exit
fi
echo

#Verification depot Debian
SOURCES_LIST1_URL=`echo "${SOURCES_LIST1}" |awk '{ print $2 "dists/" $3 }'`
echo -ne "Vérification du dépôt Debian "
wget -O /dev/null -q "${SOURCES_LIST1_URL}"
if [ $? -eq 0 ]
 then color_echo "OK" $green
else color_echo "NOK
Veuillez mettre à jour manuellement l'adresse du dépôt dans la variable SOURCES_LIST1 de ce script
Puis relancez l'installation" $red;exit
fi
}


start()
{
apt-get -qq update

echo "Europe/Paris" > /etc/timezone
dpkg-reconfigure -f noninteractive tzdata

echo -ne "Locales systême "
if [ "`egrep "^fr_FR ISO-8859-1" /etc/locale.gen`" ] && [ "`egrep "^fr_FR.UTF-8 UTF-8" /etc/locale.gen`" ] && [ `locale charmap` == "UTF-8" ]
    then color_echo "OK" $green
    else
         echo "Reconfiguration des paramétrages régionaux actifs"
         echo "Activation de FR utf8 et de FR latin1, avec utf8 par défaut"
         cp /etc/locale.gen /etc/locale.gen.$DATEINST
         echo "LANG=fr_FR.UTF-8" > /etc/default/locale 
         echo "fr_FR ISO-8859-1" > /etc/locale.gen
         echo "fr_FR.UTF-8 UTF-8" >> /etc/locale.gen
         locale-gen
         echo "La machine doit redémarrer, veuillez relancer l'install APRES REBOOT..."
         read -p "Appuyer sur Entrée pour redémarrer ou contrôle-C pour l'en empêcher"
         reboot &
	 exit 0
fi
}

################################################
#Fonctions
################################################

red='\033[31m';green='\033[32m';yellow='\033[33m';blue='\033[34m'
magenta='\033[35m';cyan='\033[36m';white='\033[37m'

color_echo()
{
message="$1";color="$2"
echo -en "$color";echo -e "$message"
tput sgr0
return
}  
echo_titre()
{
color_echo "\n\t${1}/$Netapes ${2}\n" "$cyan"
}


################################################
#Install paquets
################################################
install_utils()
{
apt-get -qqy remove mailx exim4*
apt-get -qqy install openssh-server ssh ftp unzip zip screen ntpdate time vim wget make autoconf libstdc++6 gcc cpp gnu-standards g++ sendmail sendmail-base sensible-mda sudo rsync bzip2 ed pwgen ca-certificates sharutils psmisc openssl
 
cd /usr/local/src
apt-get -qqy install fontconfig libfontconfig
wget -nv https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-2.1.1-linux-x86_64.tar.bz2
tar -xjf phantomjs-2.1.1-linux-x86_64.tar.bz2
cp phantomjs-2.1.1-linux-x86_64/bin/phantomjs /usr/local/bin/
cd $SCRIPTDIR/files/
tar -xjf wkhtmltopdf-0.11.0_rc1-static-amd64.tar.bz2
cp wkhtmltopdf-amd64 /usr/bin/wkhtmltopdf
ln -s /usr/bin/wkhtmltopdf /usr/local/bin/wkhtmltopdf
cd /usr/lib/x86_64-linux-gnu
ln -s libcrypto.so.1.0.0 libcrypto.so > /dev/null 2>&1
ln -s libssl.so.1.0.0 libssl.so > /dev/null 2>&1
}

install_apache()
{
apt-get -qqy install php5 \
php5-mcrypt php5-gd  php5-common \
php5-cli libapache2-mod-php5 \
php5-imagick php5-json php5-xsl \
php5-memcache memcached apache2 apache2.2-common php5-pgsql \
php-pear php5-curl python-numpy
}

conf_apache()
{
#Configuration de  php et d'Apache
cp /etc/php5/apache2/php.ini /etc/php5/apache2/php.ini.dist
cp /usr/share/php5/php.ini-production /etc/php5/apache2/php.ini
#memory_limit = 80M 
sed -i "s/^memory_limit = /;memory_limit = /g" /etc/php5/apache2/php.ini
sed -i "/^;memory_limit = / i\memory_limit = 512M" /etc/php5/apache2/php.ini
#post_max_size = 12M
sed -i "s/^post_max_size = /;post_max_size = /g" /etc/php5/apache2/php.ini
sed -i "/^;post_max_size = / i\post_max_size = 500M" /etc/php5/apache2/php.ini
#upload_max_filesize = 10M
sed -i "s/^upload_max_filesize = /;upload_max_filesize = /g" /etc/php5/apache2/php.ini
sed -i "/^;upload_max_filesize = / i\upload_max_filesize = 500M" /etc/php5/apache2/php.ini
#Reglage des sessions
sed -i "s/session.gc_maxlifetime = 1440/session.gc_maxlifetime = 3600/" /etc/php5/apache2/php.ini
#maxclient 512
sed -i 's/MaxRequestWorkers[[:blank:]]*150/ServerLimit 512\n\tMaxRequestWorkers 512/' /etc/apache2/mods-available/mpm_prefork.conf

#secu
sed -i "s/^expose_php = On/;expose_php = On\nexpose_php = Off/" /etc/php5/apache2/php.ini
sed -i "s/^ServerSignature.*$/ServerSignature Off/
s/^ServerTokens OS.*$/ServerTokens Prod/" /etc/apache2/conf-available/security.conf
echo 'umask 007' >> /etc/apache2/envvars
sed -i 's/^\([[:blank:]]*\)SSLProtocol.*$/\1SSLProtocol all -SSLv3\n\tSSLHonorCipherOrder On\n\tSSLCompression off/
s/^\([[:blank:]]*\)SSLCipherSuite.*$/\1SSLCipherSuite ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA:ECDHE-RSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-RSA-AES256-SHA256:DHE-RSA-AES256-SHA:ECDHE-ECDSA-DES-CBC3-SHA:ECDHE-RSA-DES-CBC3-SHA:EDH-RSA-DES-CBC3-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:DES-CBC3-SHA:!DSS/' /etc/apache2/mods-available/ssl.conf


#activation des modules et des sites
a2enmod proxy_http
a2enmod proxy
a2enmod rewrite
a2enmod ssl
a2enmod headers
a2enmod cgi
a2dissite 000-default.conf
echo "ulimit -n 16384" >> /etc/default/apache2

apt-get -qqy install php5-pecl-http
echo "extension=raphf.so
extension=propro.so
extension=http.so
" > /etc/php5/mods-available/pecl-http.ini
php5enmod pecl-http

}

install_tomcat()
{
[ -f /etc/insserv/overrides/tomcat7 ] && rm /etc/insserv/overrides/tomcat7
[ -f /etc/logrotate.d/tomcat7 ] && rm /etc/logrotate.d/tomcat7
[ -f /etc/insserv/overrides/tomcat7 ] && rm /etc/insserv/overrides/tomcat7
apt-get  -qqy install -t jessie-backports openjdk-8-jdk
apt-get -qqy install tomcat7
/etc/init.d/tomcat7 stop

sed -i 's/TOMCAT7_GROUP=tomcat7/TOMCAT7_GROUP=www-data/
s/TOMCAT7_USER=tomcat7/TOMCAT7_USER=www-data/
s/^JAVA_OPTS.*$/JAVA_OPTS=\"-Xms1024m -Xmx2048m -Xss2M -Xmn128M -XX:MaxPermSize=256m -XX:PermSize=256m -server -Djava.awt.headless=true -XX:+UseParallelGC -XX:+AggressiveOpts\"/' /etc/default/tomcat7
find -L /var/cache/tomcat7/ -user tomcat7 -exec chown www-data {} \;
find -L /var/lib/tomcat7/ -user tomcat7 -exec chown www-data {} \;
find -L /var/cache/tomcat7/ -group tomcat7 -exec chgrp www-data {} \;
find -L /var/lib/tomcat7/ -group tomcat7 -exec chgrp www-data {} \;
mkdir -p /usr/share/tomcat7/shared/classes
mkdir -p /usr/share/tomcat7/server/classes
sed -i '99i ulimit -n 16384' /usr/share/tomcat7/bin/catalina.sh

#sed -i 's/\(^JAVA_OPTS=\)\(.*\)\"$/\1\2 -Dgeosource.resources.dir=\/home\/sites\/prodigegeosource\/WEB-INF\/data"/' /etc/default/tomcat7


echo "Modification de logrotate"
cp $SCRIPTDIR/conf/tomcat7.logrotate /etc/logrotate.d/tomcat7
chown root: /etc/logrotate.d/tomcat7
chmod 640 /etc/logrotate.d/tomcat7

sed -i "$ i\* soft nofile 16384\n* hard nofile 16384" /etc/security/limits.conf
[ `egrep -c "^fs.file-max" /etc/sysctl.conf` -eq 0 ] && echo "fs.file-max = 16384" >> /etc/sysctl.conf && sysctl -p /etc/sysctl.conf && echo "16384" > /proc/sys/fs/file-max	

apt-get -qqy install jetty8
cp /etc/default/jetty8 /etc/default/jetty8.bak
sed -i 's/NO_START=1/NO_START=0/
s/^.*LOGFILE_DAYS.$/LOGFILE_DAYS=7/
s/^.*JETTY_PORT.*$/JETTY_PORT=8380/
s/^.*JETTY_HOST.*$/JETTY_HOST=0.0.0.0/
s/^.*JETTY_USER.*$/JETTY_USER=www-data/' /etc/default/jetty8
cd /etc/jetty8
ln -s /etc/tomcat7/keystore/prodige.keystore
sed -i 's/^# etc\/jetty-ssl.xml/etc\/jetty-ssl.xml/' start.ini
cp $SCRIPTDIR/conf/jetty-ssl.xml /etc/jetty8/jetty-ssl.xml
/etc/init.d/jetty8 stop
chown www-data: /var/lib/jetty8
mv /var/lib/jetty8/webapps /var/lib/jetty8/webapps.dist
mkdir /var/lib/jetty8/webapps && cd /var/lib/jetty8/webapps
ln -s /home/sites/cas
}

install_carto()
{
[ -f /etc/apache2/mods-available/fcgid.conf ] && rm /etc/apache2/mods-available/fcgid.conf
#install postgres
apt-get -qqy install  postgresql-9.4 postgresql-contrib postgis postgresql-9.4-postgis-scripts postgresql-9.4-postgis-2.1
cp $SCRIPTDIR/conf/tomcat7.insserv /etc/insserv/overrides/tomcat7
insserv > /dev/null 2>&1
#Install paquets compiles par Alkante
#dep gdal
apt-get -qqy install curl libcurl3 libcurl3-gnutls libgeos-c1 libgif4 libjpeg62-turbo libpng12-0 python libsqlite3-0 libxerces-c3.1 libpq5 libxml2 libjasper1 libkml0 libfreexl1 libexpat1
#dep mapserver
apt-get -qqy install libcairo2 libcurl3-gnutls libfcgi0ldbl libgd3 proj-bin libapache2-mod-fcgid
[ -f  /etc/apache2/mods-available/fcgid.conf ] && mv /etc/apache2/mods-available/fcgid.conf /etc/apache2/mods-available/fcgid.conf.bak
cp $SCRIPTDIR/conf/fcgid.conf /etc/apache2/mods-available/fcgid.conf
a2enmod fcgid
#bug debian
ln -s /usr/lib/libproj.so.0 /usr/lib/libproj.so
#dep mapcache
apt-get -qqy install curl libcurl3 libcurl3-gnutls libpng12-0 libjpeg62-turbo libtiff5 libpixman-1-0 libpcre3 libfcgi0ldbl libapache2-mod-fcgid

echo
cd "${SCRIPTDIR}/deb"
dpkg -i libecwj2_3.3-1jessie_amd64.deb
echo
dpkg -i gdal_2.0.2-3jessie_amd64.deb
echo
dpkg -i mapserv_7.0.0-1jessie_amd64.deb
mkdir /usr/lib/cgi-bin/ > /dev/null 2>&1
cp /usr/local/bin/mapserv /usr/lib/cgi-bin/mapserv
cp /usr/local/bin/mapserv /usr/lib/cgi-bin/mapservwfs
cp /usr/local/bin/mapserv /usr/lib/cgi-bin/mapserv.fcgi
chmod 755 /usr/lib/cgi-bin/mapserv*
echo "extension=php_mapscript.so" > /etc/php5/mods-available/mapscript.ini
php5enmod mapscript
#php-ogr
dpkg -i php-ogr_1.1.1-1jessie_amd64.deb
php5enmod ogr
echo
dpkg -i mapcache_1.4.0-1jessie_amd64.deb
ldconfig
echo 'LoadModule mapcache_module    /usr/lib/apache2/modules/mod_mapcache.so' > /etc/apache2/mods-available/mapcache.load
a2enmod mapcache

#Epsg
echo "Install epsg"
cp -f $SCRIPTDIR/files/epsg /usr/share/proj/
}

install_casldap()
{
apt-get -qqy install php5-ldap php-cas
}

#Redemarrages
################################################
restart()
{
/etc/init.d/postgresql restart
/etc/init.d/apache2 restart
/etc/init.d/tomcat7 restart
}



echo "
#################################"
echo -e "Démarrage installation des composants systême pour Prodige 4"
read -p "Appuyez sur Entrée pour continuer
#################################"
echo

verif
change_mirror
grep -q "jessie-backports" /etc/apt/sources.list || echo "${SOURCES_LIST4}" >> /etc/apt/sources.list
start
read -ep "`color_echo "\nL'installation va démarrer
Appuyez sur Entrée pour continuer ou contrôle-C pour annuler" $yellow`"

echo_titre "1" "INSTALL DES PAQUETS DEBIAN"
install_utils
echo_titre "2" "INSTALL APACHE-PHP"
install_apache
echo_titre "3" "CONFIG APACHE-PHP"
conf_apache
echo_titre "4" "INSTALL TOMCAT"
install_tomcat
echo_titre "5" "INSTALL PAQUETS carto"
install_carto
echo_titre "6" "INSTALL CAS/LDAP"
install_casldap
echo_titre "7" "Redémarrage des services"
restart

color_echo "L'installation des composants systême est terminée" $yellow
echo
exit 0

