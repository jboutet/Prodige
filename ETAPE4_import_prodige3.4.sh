#!/bin/bash



#Definition du repertoire d'install source
SCRIPTDIR=`dirname $(readlink -f $0)`
DATEINST=`date '+%y%m%d'`
OLDIR="${SCRIPTDIR}/old"


################################################
#Fonctions
################################################

red='\033[31m';green='\033[32m';yellow='\033[33m';blue='\033[34m'
magenta='\033[35m';cyan='\033[36m';white='\033[37m'

color_echo ()
{
message="$1";color="$2"
echo -en "$color";echo -e "$message"
tput sgr0
return
}  
echo_titre()
{
color_echo "\n\t${1}/$Netapes ${2}\n" "$cyan"
}
wait_exec()
{
rep="0"
[ "$rep" == "0" ]
while [ "$?" != "1" ] ; do
   echo -ne ". "
   sleep "$2"
   pgrep -f "$1" > /dev/null
done
echo
}

################################################
#Verifications
################################################

verif()
{
#Verification version Debian
echo -ne "Version Debian Jessie "
if [ "`egrep "^8\." /etc/debian_version`" ] 
  then color_echo "OK" $green
  else color_echo "NOK, annulation de l'installation" $red;exit
fi

echo
echo -ne "Architecture amd64 "
if [ "`dpkg --print-architecture |grep amd64`" ] 
  then color_echo "OK" $green
  else color_echo "NOK i386 détectée, annulation de l'installation" $red;exit
fi


#Verification présence des donnees 3.4 à importer
for dir in $OLDIR/etc $OLDIR/postgres $OLDIR/cartes
do
  echo -ne "Vérification des données 3.4 à importer : $dir"
  if [ ! -d $dir ] 
    then color_echo " $dir n'existe pas, annulation de l'installation" $red ;exit
    else color_echo " OK" $green
  fi
done

#Récuperation des paramètres de prodige 3.4
cd /home/sites
echo
echo -ne "Vérification de l'url du site"
url=`awk -F "\"" '/PRODIGE_VERIFY_RIGHTS_URL/ { print $4 }' prodigefrontcarto/app_conf.php |awk -F "/" '{ print $3 }'`
if [ -z "$url" ] 
  then color_echo " Variable url vide, annulation de l'installation" $red && exit
  else color_echo " OK" $green
fi
dom=`echo $url | sed 's/^catalogue\.//
s/^www\.//'`

echo
echo -ne "Vérification du nom de la base Postgres"
DB=`awk '/define\(\"PRO_CATALOGUE_CONNEXION/ { print $4 }' prodigecatalogue/PRRA/parametrage.php|sed "s/dbname=//"`
if [ -z "$DB" ] 
  then color_echo " Variable DB vide, annulation de l'installation" $red && exit
  else color_echo " OK" $green
fi
echo -ne "Vérification de l'utilisateur Postgres"
user_bdd=`awk '/define\(\"PRO_CATALOGUE_CONNEXION/ { print $3 }' prodigecatalogue/PRRA/parametrage.php|sed "s/user=//"`
if [ -z "$user_bdd" ] 
  then color_echo " Variable user_bdd vide, annulation de l'installation" $red && exit
  else color_echo " OK" $green
fi

echo -ne "Vérification du mot de passe Postgres"
pass_bdd=`awk '/define\(\"PRO_CATALOGUE_CONNEXION/ { print $5 }' prodigecatalogue/PRRA/parametrage.php|sed "s/password=//"|sed 's/\".*//'`
if [ -z "$pass_bdd" ] 
  then color_echo " Variable pass_bdd vide, annulation de l'installation" $red && exit
  else color_echo " OK" $green
fi
#Vérification de la présence des bases 3.4
for dump in $OLDIR/postgres/PRODIGE.dmp $OLDIR/postgres/$DB.dmp
do
  echo -ne "Vérification de la base $dump"
  if [ ! -f $dump ] 
    then color_echo " $dump n'existe pas, annulation de l'installation" $red ;exit
    else color_echo " OK" $green
  fi
done

#Vérification du numéro de version de prodige3.4
echo
echo -ne "Vérification du numéro de version de prodige3.4"
if [ "`grep 3.4.17 /home/sites/prodigeadminsite/version.txt`" ] 
  then color_echo " 3.4.17 OK" $green
  else color_echo " Prodige n'est pas en version 3.4.17" $red ; exit
fi

/etc/init.d/postgresql restart || { echo "Erreur postgres n'a pu démarrer, le script ne peut être lancé"; exit; }
}



import_base()
{
echo
SOURCE="$OLDIR/postgres/${1}.dmp"
echo "Import de la base à partir de ${SOURCE}, cf log dans ${2}"
echo  "Re-création de la structure" | tee  -a "${2}" 
sudo -i -u postgres createdb -O "$user_bdd" ${3} "${1}" >> "${2}" 2>&1
echo  "Conversion postgis "  | tee  -a "${2}"
sudo -i -u postgres psql -f /usr/share/postgresql/9.4/contrib/postgis-2.1/postgis.sql "${1}" 1> /dev/null
sudo -i -u postgres /usr/share/postgresql/9.4/contrib/postgis-2.1/postgis_restore.pl "${SOURCE}" > "${SOURCE}.tmp"
echo -ne "Import de la base "  | tee  -a "${2}"
psql  -U "${user_bdd}" -h localhost -f "${SOURCE}.tmp" "${1}" >> "${2}" 2>&1 &
wait_exec "${SOURCE}.tmp" "5"
sudo -i -u postgres psql -d "${1}" -c "
grant all on geometry_columns to ${user_bdd};
grant all on spatial_ref_sys to ${user_bdd};
grant all on geography_columns to ${user_bdd};
" >> "${2}" 2>&1
}

postgres()
{
echo "localhost:5432:PRODIGE:$user_bdd:$pass_bdd
localhost:5432:$DB:$user_bdd:$pass_bdd
localhost:5432:postgres:$user_bdd:$pass_bdd" >> /root/.pgpass
chmod 600 /root/.pgpass

chmod 755 ${SCRIPTDIR}
chown -R postgres: "$OLDIR/postgres"
md5pass=`echo -n "${pass_bdd}${user_bdd}"|md5sum|awk '{ print $1 }'`
sudo -i -u postgres psql -c "CREATE ROLE ${user_bdd} ENCRYPTED PASSWORD 'md5${md5pass}' SUPERUSER CREATEDB CREATEROLE INHERIT LOGIN;"
sudo -i -u postgres psql -c "CREATE ROLE user_admin ENCRYPTED PASSWORD 'md57cff8cc9d16b03d9dfcc7954b4662992' SUPERUSER CREATEDB CREATEROLE INHERIT LOGIN;"
su - postgres -c "psql -c \"ALTER ROLE $user_bdd SUPERUSER;\""

#Reglage de postgres pour la phase import
sed -i 's/#checkpoint_segments = 3/checkpoint_segments = 100/' /etc/postgresql/9.4/main/postgresql.conf
/etc/init.d/postgresql restart

LOG_CATALOGUE="$SCRIPTDIR/import_$DB.log"
import_base "${DB}" "$LOG_CATALOGUE" "--encoding=UTF8"
LOG_PRODIGE="$SCRIPTDIR/import_PRODIGE.log"
import_base "PRODIGE" "$LOG_PRODIGE" "--encoding=UTF8"

su - postgres -c "psql -c \"ALTER ROLE $user_bdd NOSUPERUSER NOCREATEDB NOCREATEROLE INHERIT LOGIN;\""
rm /root/.pgpass
}

echo "
##################################################################"
echo -e "Démarrage de l'import des données Prodige3.4
##################################################################
"
echo


verif

read -ep "`color_echo "\nL'importation peut commencer
Appuyez sur Entrée pour continuer ou contrôle-C pour annuler" $yellow`"

echo_titre "1" "Import des bases de données"
postgres


color_echo "L'importation des données est terminée
Vous pouvez passer à l'étape suivante" $yellow
echo
exit 0
