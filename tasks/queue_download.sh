#!/bin/bash
DATE=`date '+%y%m%d'`
SCRIPTDIR=`dirname $(readlink -f $0)`
SCRIPTNAME=`basename $(readlink -f $0)`
LOGDIR="${SCRIPTDIR}/log"; [ -d ${LOGDIR} ] || mkdir -p "${LOGDIR}"
LOG="${LOGDIR}/${SCRIPTNAME%.*}_${DATE}.log"

echolog()
{
echo "`date '+%b %d %H:%M:%S'`  ${1}"
}

mainjob()
{
if /sbin/start-stop-daemon --test --start --pidfile "/var/run/queue.pid"  --user www-data --startas "/usr/bin/php"
    then
        ulimit -n 131072
        /sbin/start-stop-daemon  --start --pidfile "/var/run/queue.pid" --user "www-data" --exec "/home/tasks/queue_download_process.sh" --make-pidfile "/var/run/queue.pid" --chuid "www-data"
fi
}

exec 6>&1 7>&2 && exec >> ${LOG} 2>&1
echolog "Starting ${SCRIPTDIR}/${SCRIPTNAME}"
mainjob
find ${LOGDIR}/ -name "${SCRIPTNAME%.*}_*.log" -type f -mtime +10  -maxdepth 1 -delete
echolog "End of ${SCRIPTDIR}/${SCRIPTNAME}"
exec 1>&6 6>&- 2>&7 7>&-

