#!/bin/bash
DATE=`date '+%y%m%d'`
SCRIPTDIR=`dirname $(readlink -f $0)`
SCRIPTNAME=`basename $(readlink -f $0)`
LOGDIR="${SCRIPTDIR}/log"; [ -d ${LOGDIR} ] || mkdir -p "${LOGDIR}"
LOG="${LOGDIR}/${SCRIPTNAME%.*}_${DATE}.log"

echolog()
{
echo "`date '+%b %d %H:%M:%S'`  ${1}"
}


mainjob()
{
cd /home/sites/prodigetelecarto/ && /usr/bin/php app/console prodigetelecarto:treatQueue
}

#exec 6>&1 7>&2 && exec >> ${LOG} 2>&1
echolog "Starting ${SCRIPTDIR}/${SCRIPTNAME}"
mainjob
find ${LOGDIR}/ -name "${SCRIPTNAME%.*}_*.log" -type f -mtime +10  -maxdepth 1 -delete
echolog "End of ${SCRIPTDIR}/${SCRIPTNAME}"
#exec 1>&6 6>&- 2>&7 7>&-
