#!/bin/bash
DATE=`date '+%y%m%d'`
SCRIPTDIR=`dirname $(readlink -f $0)`
SCRIPTNAME=`basename $(readlink -f $0)`
LOGDIR="${SCRIPTDIR}/log"; [ -d ${LOGDIR} ] || mkdir -p "${LOGDIR}"
LOG="${LOGDIR}/${SCRIPTNAME%.*}_${DATE}.log"

echolog()
{
echo "`date '+%b %d %H:%M:%S'`  ${1}"
}

mainjob()
{
#3.4
find /home/prodige/cartes/Publication/ -type f -name "temp_admin_*" -mtime +7 -exec rm -f {} \;
find /home/prodige/cartes/Publication/temp/ -type f -mtime +30 -exec rm -f {} \;
find /home/prodige/mapimage/ -type f -mtime +1 -exec rm -f {} \;
find /home/sites/prodigefrontcarto/context/seven_days/ -type f -mtime +7 -exec rm -f {} \;
find /home/sites/prodigefrontcarto/context/one_month/ -type f -mtime +31 -exec rm -f {} \;
find /home/sites/prodigefrontcarto/context/one_day/ -type f -mtime +1 -exec rm -f {} \;
#TODO MIGRATION cd /home/sites/prodigeadmincarto/HTML_JOIN/lib && /usr/bin/php5 removeTemporaryView.php
find /home/sites/prodigetelecarto/DOWNLOAD/ -mindepth 1 -mtime +7 -exec rm -rf {} \;
find /home/sites/prodigetelecarto/DOWNLOAD/ -mindepth 1 -maxdepth 1 -type d -mtime +1 -exec rm -rf {} \;
find /home/sites/prodigetelecarto/DOWNLOAD/ -maxdepth 1 -type f -mtime +1 -name "*.zip.*" -delete
find /var/log/tomcat7/ -name "geonetwork.log.*" -type f -mtime +15 -delete
find /tmp -maxdepth 1 -regex '/tmp/PDF_Complet_[a-f0-9][a-f0-9]*.zip' -mtime +5 -delete
find /tmp -maxdepth 1 -name "*.tmp.gml" -mtime +5 -delete
find /tmp -maxdepth 1 -name "*._filter.map" -mtime +5 -delete
#NOT USED IN P4 find /home/prodige/cartes/IHM/LEGEND/ -type f -name "TMP_*" -delete
find /home/prodige/cartes/Publication/wfs/temp/ -type f -mtime +8 -delete
#TODO MIGRATION find /home/sites/prodigecatalogue/Logs/ -type f -mtime +8 -delete
#NOT USED IN P4 find /home/sites/prodigefrontcarto/templates_c/ -type f -mtime +8 -delete


#4.0
#/tmp/geometrytype_*
#/tmp/layerfieldvalues_*
#/tmp/*.pdf
}

exec 6>&1 7>&2 && exec >> ${LOG} 2>&1
echolog "Starting ${SCRIPTDIR}/${SCRIPTNAME}"
mainjob
find ${LOGDIR}/ -name "${SCRIPTNAME%.*}_*.log" -type f -mtime +10  -maxdepth 1 -delete
echolog "End of ${SCRIPTDIR}/${SCRIPTNAME}"
exec 1>&6 6>&- 2>&7 7>&-