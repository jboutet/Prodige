#!/bin/bash

echo "Ce script ne doit pas être exécuté
Lancer les commandes les unes après les autres"
exit

/etc/init.d/tomcat7 stop
pg_dropcluster --stop 9.1 main
apt-get remove --purge postgresql-9.1 postgresql-9.1-postgis postgresql-client-9.1 postgresql-contrib-9.1 postgresql-doc-9.1 libecwj2 gdal mapserv mapcache php5* apache2* tomcat* libtomcat7-java openjdk-7* postgis*  postgres*
rm -rf /etc/apache2 /etc/php5 /etc/tomcat7 /usr/lib/cgi-bin /usr/share/tomcat7 /var/lib/postgresql/
rm /usr/bin/wkhtmltopdf > /dev/null 2>&1
rm /usr/local/bin/wkhtmltopdf > /dev/null 2>&1
rm /usr/lib/libproj.so.0 /usr/lib/libproj.so > /dev/null 2>&1

echo '
deb http://ftp.fr.debian.org/debian jessie main contrib non-free
deb http://security.debian.org/ jessie/updates main' > /etc/apt/sources.list

apt-get update
apt-get upgrade
#Restart services during package upgrades without asking? > yes
#Configuring libc6:amd64 > yes
#You will need to start these manually by running '/etc/init.d/apache2' > OK

apt-get dist-upgrade
#Configuration de openssh-server Non
#/etc/sysctl.conf > Y
#/etc/security/limits.conf > Y
#/etc/securetty > Y
#/etc/apt/apt.conf.d/50unattended-upgrades > Y
apt-get autoremove


reboot

